import os
import json
import logging
import base64
import hashlib
import sys
import datetime
import tornado.process
import tornado.ioloop
from controller_enums import EventType
from controller_enums import TrackerPresets

log = logging.getLogger(__name__)
event_log = logging.getLogger('EventLog')

PROXY_SRV_USERNAME = 'rproxy'
SSH_CMD = '/usr/bin/ssh'
SSH_KNOWN_HOSTS = '/run/shm/known_hosts'
if sys.platform == 'win32':
    import argparse

    SSH_CMD = "MOCK"  # r'C:\Program Files\Git\usr\bin\ssh.exe'
    SSH_KNOWN_HOSTS = 'known_hosts'

    class SubprocessMock(object):
        STREAM = object()

        def __init__(self, *args, **kwargs):
            self.stderr = argparse.Namespace()  # Just using argparse.Namespace to hold stuff
            self.stderr.read_until_close = lambda **kwargs: None
            self.stdout = argparse.Namespace()
            self.stdout.read_until_close = lambda **kwargs: None
            self.stdout.set_close_callback = lambda func: tornado.ioloop.IOLoop.current().add_timeout(
                datetime.timedelta(seconds=120),
                func)
            self.proc = argparse.Namespace()
            self.proc.terminate = lambda: log.debug("Stopped sub process")


    tornado.process.Subprocess = SubprocessMock
    
class TerraSmartCloudListener(object):
    """
    Subscribes to MQTT topics in the TerraSmart cloud and dispatches received messages
    """

    def __init__(self, mqtt_client, https_port, http_port, site, asset_updater, asset_mgr, asset_broadcaster, persistent_service, cloud_updater, config):
        self.mqtt_client = mqtt_client
        self.aws_principal_id = None
        if os.path.exists(self.mqtt_client.ssl_options['certfile']):
            with open(self.mqtt_client.ssl_options['certfile'], 'r') as f:
                # The AWS certificate principal ID is the public certificate's "fingerprint"
                # This is calculated by taking the SHA-256 hash of the Base64 encoded bytes in the certificate file
                self.aws_principal_id = hashlib.sha256(base64.b64decode(f.read()[28:-27])).hexdigest()

            self.mqtt_client.on('connect', self._on_mqtt_connected)
        self.mqtt_client.on('message', self._on_mqtt_message)

        self._ssh_process = None
        self.timeout = None
        self.https_port = https_port
        self.http_port = http_port
        self.my_site = site
        self.asset_updater = asset_updater
        self.persistent_service = persistent_service
        self.asset_mgr = asset_mgr
        self.asset_broadcaster = asset_broadcaster
        self.cloud_updater = cloud_updater

        self._current_connect_info = None
        self.known_commands = ["start_proxy", "stop_proxy",
                               "control_operational_mode",
                               "control_uploads",
                               "force_stm32_upload",
                               "force_upload",
                               "clear_faults",
                               "manual_control",
                               "cloud_updater_settings"]

        self.topic_prefix = config['PROXY_PREFIX']
        self.proxy_server = config['PROXY_SRV_ADDR']

    def _on_timeout(self):
        log.debug("SSH proxy timeout reached")
        self.timeout = None
        if self._ssh_process:
            event_log.info("Remote proxy timeout reached. Disconnecting tunnel", extra=EventType.ACCESS_CONTROL._dict)
            self._ssh_process.proc.terminate()

    def _on_mqtt_connected(self):
        self.mqtt_client.subscribe('networkControllers/' + self.aws_principal_id)
        # event_log.debug("Connected to cloud with ID %s" % self.aws_principal_id)

    def _on_ssh_process_data(self, data):
        log.debug("Received unexpected SSH client message: %s" % data)

    def _on_ssh_closed(self, connect_info):
        log.debug("SSH closed called")
        if self.timeout is not None:
            tornado.ioloop.IOLoop.current().remove_timeout(self.timeout)
        self._ssh_process = None

        connect_info['timestamp'] = (datetime.datetime.utcnow() - datetime.datetime.utcfromtimestamp(0)).total_seconds()
        connect_info['timeout'] = 0
        self.mqtt_client.publish(topic=self.topic_prefix + '/disconnected',
                                 payload=json.dumps(connect_info))

    def _log_msg_error(self, msg):
        event_log.error("Received malformed command from the cloud", extra=EventType.ACCESS_CONTROL._dict)
        event_log.error("msg=" + str(msg), extra=EventType.ACCESS_CONTROL._dict)

    def _sanity_check_msg(self, msg):
        # The cloud must send us a dictionary
        if type(msg) != type({}):
            event_log.error("Message from cloud must be a dictionary!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        # That dictionary must contain a command field named "cmd"
        if "cmd" not in msg:
            event_log.error("Message from cloud must include a cmd field!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        # That cmd must be a string
        if (type(msg["cmd"]) != type("")) and (type(msg["cmd"]) != type(u"")):
            event_log.error("The cmd field in the message must be a string!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        # That dictionary must contain another dictionary named "args"
        if "args" not in msg:
            event_log.error("Message from cloud must include an args field!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        if type(msg["args"]) != type({}):
            event_log.error("The args field in the message must be a dictionary!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        return True

    def _control_operational_mode(self, msg):
        # msg is a dict containing "cmd" and "args"
        # "args" is itself a dict

        # Check the aspects common to all commands
        if not self._sanity_check_msg(msg):
            return

        # Now check portions of msg specific to this particular command

        args = msg["args"]

        # args must include a operational_mode
        if "operational_mode" not in args:
            event_log.error("A control_operational_mode command must include an operational_mode field!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        operational_mode = args["operational_mode"]

        # operational_mode must be an integer
        if type(operational_mode) != type(123):
            event_log.error("The operational_mode field in the args dictionary must be an integer!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        # operational_mode must be 0-15 (only 0-8 are used as of 09/10/2019, 9-15 are for future expansion)
        if (operational_mode < 0) or (operational_mode > 15):
            event_log.error("The operational_mode field in the args dictionary must be an integer 0-15!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        # args must include a tracking_enable
        if "tracking_enable" not in args:
            event_log.error("A control_operational_mode command must include a tracking_enable field!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        tracking_enable = args["tracking_enable"]

        # tracking_enable must be a boolean
        if type(tracking_enable) != type(True):
            event_log.error("The tracking_enable field in the args dictionary must be a boolean!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        # args must include a backtracking_enable
        if "backtracking_enable" not in args:
            event_log.error("A control_operational_mode command must include a backtracking_enable field!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        backtracking_enable = args["backtracking_enable"]

        # backtracking_enable must be a boolean
        if type(backtracking_enable) != type(True):
            event_log.error("The backtracking_enable field in the args dictionary must be a boolean!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        # OK, command seems valid. Log what we are being told to do by the cloud
        if tracking_enable:
            if backtracking_enable:
                event_log.info('The cloud has commanded the site to the "Normal" state with backtracking enabled', extra=EventType.ACCESS_CONTROL._dict)
            else:
                event_log.info('The cloud has commanded the site to the "Normal" state with backtracking disabled', extra=EventType.ACCESS_CONTROL._dict)
        else:
            event_log.info('The cloud has commanded the site to the "User Preset Angle" state, preset = "%s"' % TrackerPresets(operational_mode), extra=EventType.ACCESS_CONTROL._dict)

        # Do the requested command
        self.my_site.update(args)

    def _control_uploads(self, msg):
        # msg is a dict containing "cmd" and "args"
        # "args" is itself a dict

        # Check the aspects common to all commands
        if not self._sanity_check_msg(msg):
            return

        # Now check portions of msg specific to this particular command

        args = msg["args"]

        if len(args) == 0:
            event_log.error("A control_uploads command must include at least one field!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        supported_fields = ["do_upload_bridge_script", "do_upload_bridge_firmware",
                            "do_upload_asset_scripts", "do_upload_asset_radio_firmware",
                            "do_upload_asset_stm32_firmware", "do_upload_asset_configs"]

        for (key, value) in args.items():
            key = str(key) # get rid of unicode indicator
            if key not in supported_fields:
                event_log.error("The control_uploads command does not support a " + key + " field!" , extra=EventType.ACCESS_CONTROL._dict)
                self._log_msg_error(msg)
                return False

            # field values must be a boolean
            if type(value) != type(True):
                event_log.error("The " + key + " field in the args dictionary must be a boolean!", extra=EventType.ACCESS_CONTROL._dict)
                self._log_msg_error(msg)
                return False

        # OK, command seems valid. Log what we are being told to do by the cloud
        changes = ""
        for (key, value) in args.items():
            if changes != "":
                changes += ", "
            changes += key
            changes += "="
            if value:
                changes += "yes"
            else:
                changes += "no"

        event_log.info("Cloud has commanded new upload settings of " + changes, extra=EventType.ACCESS_CONTROL._dict)

        # Do the requested command
        self.asset_updater.update_config(args)

    # Address List validation was pulled into a subroutine when we added the clear_faults() command
    def _validate_and_extract_address_list(self, msg, args, list_name, command):
        # args must include an asset_list
        if list_name not in args:
            event_log.error("A " + command + " command must include an " + list_name + "!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return None

        address_list = args[list_name]
        if type(address_list) != type([]):
            event_log.error("The " + list_name + " field of a " + command + " command must be a list!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return None

        if len(address_list) == 0:
            event_log.error("The " + list_name + " field of a " + command + " command cannot be empty!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return None

        index = 0
        while index < len(address_list):
            address = address_list[index]
            if (type(address) != type("")) and (type(address) != type(u"")):
                event_log.error("Each entry in the " + list_name + " of a " + command + " command must be a string!", extra=EventType.ACCESS_CONTROL._dict)
                self._log_msg_error(msg)
                return None

            if len(address) != 6:
                event_log.error("Each entry in the " + list_name + " of a " + command + " command must be 6 characters long!", extra=EventType.ACCESS_CONTROL._dict)
                self._log_msg_error(msg)
                return None

            address = str(address) # get rid of unicode
            address = address.lower()

            for character in address:
                if character not in "0123456789abcdef":
                    event_log.error("Each entry in the " + list_name + " of a " + command + " command must be valid HEX-ASCII string!", extra=EventType.ACCESS_CONTROL._dict)
                    self._log_msg_error(msg)
                    return None

            asset = self.asset_mgr.get_asset_if_known(address)
            if asset is None:
                event_log.error("A " + command + " command was attempted to a non-existant device %s!" % address, extra=EventType.ACCESS_CONTROL._dict)
                self._log_msg_error(msg)
                return None

            address_list[index] = address
            index += 1

        return address_list


    def _force_stm32_upload(self, msg):
        # Check the aspects common to all commands
        if not self._sanity_check_msg(msg):
            return

        # Now check portions of msg specific to this particular command

        args = msg["args"]

        address_list = self._validate_and_extract_address_list(msg, args, "address_list", "force_stm32_upload")
        if address_list is None:
            return False

        # OK, command seems valid. Do it
        self.asset_updater.force_upload(address_list, TC_WX_FIRMWARE_UPLOAD)

    def _force_upload(self, msg):
        # Check the aspects common to all commands
        if not self._sanity_check_msg(msg):
            return

        # Now check portions of msg specific to this particular command

        args = msg["args"]

        address_list = self._validate_and_extract_address_list(msg, args, "address_list", "force_upload")
        if address_list is None:
            return False

        # args must include an upload_type
        if "upload_type" not in args:
            event_log.error("A force_upload command must include an upload_type field!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        upload_type = args["upload_type"]

        # upload_type must be an integer
        if type(upload_type) != type(123):
            event_log.error("The upload_type field in the args dictionary must be an integer!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        # upload_type must be 0-6 (only 0-4 and 6 are used as of 05/26/2019, 5 is for future expansion)
        if (upload_type < 0) or (upload_type > 6):
            event_log.error("The upload_type field in the args dictionary must be an integer 0-6!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        # OK, command seems valid. Do it
        self.asset_updater.force_upload(address_list, upload_type)

    def _clear_faults(self, msg):
        # Check the aspects common to all commands
        if not self._sanity_check_msg(msg):
            return

        # Now check portions of msg specific to this particular command

        args = msg["args"]

        address_list = self._validate_and_extract_address_list(msg, args, "address_list", "clear_faults")
        if address_list is None:
            return False

        # args must include a fault_bitmask
        if "fault_bitmask" not in args:
            event_log.error("A clear_faults command must include a fault_bitmask field!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        fault_bitmask = args["fault_bitmask"]

        # fault_bitmask must be an integer
        if type(fault_bitmask) != type(123):
            event_log.error("The fault_bitmask field in the args dictionary must be an integer!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        # fault_bitmask must be 0-255
        if (fault_bitmask < 0) or (fault_bitmask > 255):
            event_log.error("The fault_bitmask field in the args dictionary must be an integer 0-255!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        # OK, command seems valid. Do it
        count = len(address_list)
        if count == 1:
            event_log.info('The cloud has cleared faults on 1 unit', extra=EventType.ACCESS_CONTROL._dict)
        else:
            event_log.info('The cloud has cleared faults on %d units' % (count,), extra=EventType.ACCESS_CONTROL._dict)

        self.asset_broadcaster.request_clear_faults(address_list, fault_bitmask)


    def _cloud_updater_settings(self, msg):
        # msg is a dict containing "cmd" and "args"
        # "args" is itself a dict

        # Check the aspects common to all commands
        if not self._sanity_check_msg(msg):
            return

        # Now check portions of msg specific to this particular command

        args = msg["args"]

        if len(args) == 0:
            event_log.error("A cloud_updater_settings command must include at least one field!", extra=EventType.ACCESS_CONTROL._dict)
            self._log_msg_error(msg)
            return False

        supported_fields = ["wind_reporting_close_percentage", "wind_reporting_close_interval",
                            "wind_reporting_over_interval", "snow_reporting_close_percentage",
                            "snow_reporting_close_interval", "snow_reporting_over_interval"]

        for (key, value) in args.items():
            key = str(key) # get rid of unicode indicator
            if key not in supported_fields:
                event_log.error("The cloud_updater_settings command does not support a " + key + " field!" , extra=EventType.ACCESS_CONTROL._dict)
                self._log_msg_error(msg)
                return False

            # (To date) All field values must be an integer
            if type(value) != type(123):
                event_log.error("The " + key + " field in the args dictionary must be an integer!", extra=EventType.ACCESS_CONTROL._dict)
                self._log_msg_error(msg)
                return False

            # Apply some range checks
            if value < 1:
                event_log.error("The " + key + " field in the args dictionary must be >= 1!", extra=EventType.ACCESS_CONTROL._dict)
                self._log_msg_error(msg)
                return False

            max = 60 # for most of these settings, 60 (minutes) is the maximum
            if key == "wind_reporting_close_percentage":
                max = 100 # percent
            elif key == "snow_reporting_close_percentage":
                max = 100 # percent

            if value > max:
                event_log.error("The " + key + " field in the args dictionary must be <= " + str(max) + "!", extra=EventType.ACCESS_CONTROL._dict)
                self._log_msg_error(msg)
                return False

        # OK, command seems valid. Log what we are being told to do by the cloud
        changes = ""
        for (key, value) in args.items():
            if changes != "":
                changes += ", "
            changes += key
            changes += "="
            changes += str(value)

        event_log.info("Cloud has commanded new cloud updater settings of " + changes, extra=EventType.ACCESS_CONTROL._dict)

        # Do the requested command
        self.cloud_updater.update_config(args)

    def _on_mqtt_message(self, message):
        # print "_on_mqtt_message", message
        log.debug("Received cloud message %s" % message.payload)

        try:
            msg = json.loads(message.payload)
        except ValueError:
            log.error("Received bad JSON")

        try:
            command = msg["cmd"]
            if command not in self.known_commands:
                event_log.error("Received unsupported command " + str(command) + " from the cloud!" , extra=EventType.ACCESS_CONTROL._dict)
                return

            # We have started adding some new commands
            if command == "control_operational_mode":
                self._control_operational_mode(msg)
                return
            elif command == "manual_control":
                self._on_manual_control(msg)
                return
            elif command == "control_uploads":
                self._control_uploads(msg)
                return
            elif command == "force_stm32_upload":
                self._force_stm32_upload(msg)
                return
            elif command == "force_upload":
                self._force_upload(msg)
                return
            elif command == "clear_faults":
                self._clear_faults(msg)
                return
            elif command == "cloud_updater_settings":
                self._cloud_updater_settings(msg)
                return

            # The original MQTT commands were all about Proxy Tunnels
            local_port_name = None
            try:
                local_port = int(msg["args"]["access_port"])
            except ValueError:
                local_port = self.https_port
                local_port_name = 'HTTPS'
                if msg["args"]["access_port"].lower() == 'http':
                    local_port = self.http_port
                    local_port_name = 'HTTP'
                elif msg["args"]["access_port"].lower() == 'ssh':
                    local_port = 22
                    local_port_name = 'SSH'

            if self.timeout:
                tornado.ioloop.IOLoop.current().remove_timeout(self.timeout)
                if self._ssh_process:
                    if msg["cmd"] == "stop_proxy" or self._current_connect_info['access_port'] != local_port:
                        event_log.info("Terminating existing proxy tunnel early", extra=EventType.ACCESS_CONTROL._dict)
                        self._ssh_process.proc.terminate()
                        if msg["cmd"] == "stop_proxy":
                            event_log.info("Finished stopping proxy tunnel", extra=EventType.ACCESS_CONTROL._dict)
                            return
                    else:
                        self.timeout = tornado.ioloop.IOLoop.current().add_timeout(
                            datetime.timedelta(minutes=msg["args"]["timeout"]),
                            self._on_timeout)
                        event_log.info("Extending existing proxy tunnel session", extra=EventType.ACCESS_CONTROL._dict)
                        return
            elif msg["args"]["proxy_port"] is None:
                event_log.error("Received request to extend proxy session but we currently don't have one", extra=EventType.ACCESS_CONTROL._dict)
                return

            self.timeout = tornado.ioloop.IOLoop.current().add_timeout(
                datetime.timedelta(minutes=msg["args"]["timeout"]),
                self._on_timeout)

            with file(SSH_KNOWN_HOSTS, 'w') as f:
                f.write("%s %s" % (self.proxy_server, msg["args"]["server_pub_key"]))

            self._current_connect_info = {
                'proxy_port': msg["args"]["proxy_port"],
                'timestamp': (datetime.datetime.utcnow() - datetime.datetime.utcfromtimestamp(0)).total_seconds(),
                'timeout': msg["args"]["timeout"],
                'access_port': local_port,
                'access_port_name': local_port_name
            }

            event_log.info("Starting remote proxy tunnel", extra=EventType.ACCESS_CONTROL._dict)
            self._ssh_process = tornado.process.Subprocess(
                [SSH_CMD, '-oUserKnownHostsFile=/run/shm/known_hosts', '-N', '-R',
                 '0.0.0.0:%i:127.0.0.1:%i' % (msg["args"]["proxy_port"], local_port),
                 '%s@%s' % (PROXY_SRV_USERNAME, self.proxy_server), '-i', self.mqtt_client.ssl_options['keyfile']],
                stderr=tornado.process.Subprocess.STREAM,
                stdout=tornado.process.Subprocess.STREAM)
            self._ssh_process.stderr.read_until_close(streaming_callback=self._on_ssh_process_data)
            self._ssh_process.stdout.read_until_close(streaming_callback=self._on_ssh_process_data)
            self._ssh_process.stdout.set_close_callback(lambda: self._on_ssh_closed(self._current_connect_info))

            # There is no callback or message from the SSH client once it actually connects
            self.mqtt_client.publish(topic=self.topic_prefix + '/connected',
                                     payload=json.dumps(self._current_connect_info))
        except KeyError:
            log.error("Received unsupported or malformed message: %s" % msg)

    def _on_manual_control(self, msg):
        # msg is a dict containing "cmd" and "args"

        # Check the aspects common to all commands
        if not self._sanity_check_msg(msg):
            return

        # "args" is itself a dict
        args = msg["args"]

        # args must include a asset_id
        if "asset_id" not in args:
            event_log.error("A manual_control command must include an asset_id field!", extra=EventType.INDIVIDUAL_ASSET._dict)
            self._log_msg_error(msg)
            return False

        asset_id = args["asset_id"]

        if (type(asset_id) != type("")) and (type(asset_id) != type(u"")):
            event_log.error("The asset_id field in the args must be a string!", extra=EventType.INDIVIDUAL_ASSET._dict)
            self._log_msg_error(msg)
            return False

        if "cmd_state" not in args:
            event_log.error("The cmd_state field in the args is missing!", extra=EventType.INDIVIDUAL_ASSET._dict)
            self._log_msg_error(msg)
            return False

        cmd_state = args["cmd_state"]

        if (type(cmd_state) != type(1234)):
            event_log.error("The cmd_state field in the args must be a string!", extra=EventType.INDIVIDUAL_ASSET._dict)
            self._log_msg_error(msg)
            return False

        if (cmd_state < 0) or (cmd_state > 5):
            event_log.error("The cmd_state field in the args dictionary must be an integer 0-5!", extra=EventType.INDIVIDUAL_ASSET._dict)
            self._log_msg_error(msg)
            return False  
        
        if "param" in args:
            param = args["param"]
            if param is not None:
                if type(param) != type(123):
                    event_log.error("The type of 'param' field in the args must be an integer!", extra=EventType.INDIVIDUAL_ASSET._dict)
                    self._log_msg_error(msg)
                    return False

        if "username" not in args:
            event_log.error("The username field in the args is missing!", extra=EventType.INDIVIDUAL_ASSET._dict)
            self._log_msg_error(msg)
            return False

        self.persistent_service.add_command(args)


if __name__ == '__main__':
    from utils import SystemSettings

    from ts_common import tornado_paho

    import tornado.ioloop

    logging.basicConfig()

    mqtt_client = tornado_paho.TornadoPahoMqttClient(mqtt_broker='a2dlentyd00pdf.iot.us-east-2.amazonaws.com',
                                                     mqtt_port=8883,
                                                     ssl_options={
                                                         "certfile": os.path.join(
                                                             SystemSettings.SYSTEM_CONFIG_DIRECTORY, "my_aws_iot.pem"),
                                                         "keyfile": os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY,
                                                                                 "private.key"),
                                                         "ca_certs": r'VeriSign-Class 3-Public-Primary-Certification-Authority-G5.pem'
                                                     })
    TerraSmartCloudListener(mqtt_client, https_port=8883, http_port=8888)

    tornado.ioloop.IOLoop.current().start()
