"""
nc_update_manager.py - This file update NC to a new .whl file found in a specific
folder, by constantly watching it.
"""

from utils.PickleHelper import PickleHelper
import time
from os import listdir
from os.path import isfile, join
import os
import subprocess
from terrasmart_site import NC_SOFTWARE_VERSION as NC_Softwere_version
import logging
from controller_enums import EventType
import tornado.process
import tornado.ioloop
import sys
import pip

log = logging.getLogger(__name__)

WATCH_FILE_DIRECTORY = "/home/netctrl/nc_whl_watch_folder"  # system will be watching this directory see if .whl file exists.

class NetworkControllerUpdateManager(object):

    REFRESH_INTERVAL = 30  # seconds
    ENV_FILE_DIRECTORY = "/home/netctrl/ts-netctrl-venv/bin/python"
    PIP_FILE_DIRECTORY = "/home/netctrl/ts-netctrl-venv/bin/pip"
    HOME_FILE_DIRECTORY ="/home/netctrl/"
    SCRIPT_FILE_NAME = "install-whl2.sh"
    SYSTEM_PASS = "terrasmart"

    def __init__(self, schedule_func):
        """
        Constructor
        :param callable schedule_func: A function with the signature func(call_every_n_seconds, callback_func)
        """
        self._schedule_func = schedule_func
        self._schedule_func(NetworkControllerUpdateManager.REFRESH_INTERVAL, self._update_status)
        self.all_package_installed = False
        self._install_missing_package()
        self._software_upgrading = True

    def _update_status(self):
        log.debug(" I am alive ")
        if self.all_package_installed:
            self._set_NC_Version_update()
            file_check = self._check_NC_new_file()
            if file_check:
                if self._install_whl_file(file_check):
                    logging.getLogger('EventLog').log(logging.INFO, "Rebooting Network Controller", extra=EventType.FIRMWARE_UPDATE._dict)
                    time.sleep(2)
                    self.restart_NC()
                    time.sleep(10)
                    log.debug("First restart failed due to SSH issue, running restart again")
                    self.restart_NC()
        else:
            log.debug("some packages missing so can not run")

    def _install_missing_package(self):
        if not self.all_package_installed:
            try:
                self.all_package_installed = True
                import paramiko
                log.debug("package paramiko already installed ")
                return
            except ImportError, e:
                log.error(e)
                self.all_package_installed = False
                logging.getLogger('EventLog').log(logging.INFO, "Packege paramiko is not installed so existing NC Update itself module", extra=EventType.FIRMWARE_UPDATE._dict)
                logging.getLogger('EventLog').log(logging.INFO, "Package not found terminating NC update module, please install manually by adding in requirements.txt", extra=EventType.FIRMWARE_UPDATE._dict)

    def _install(self, package):
        installation_Package_result = False
        script = NetworkControllerUpdateManager.ENV_FILE_DIRECTORY + " " + NetworkControllerUpdateManager.PIP_FILE_DIRECTORY + " install " + package
        cmd3 = subprocess.Popen(script, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).communicate()
        for xxxx in cmd3:
            log.debug(xxxx)
            if xxxx.find("Successfully installed") != -1:
                installation_Package_result = True
            if xxxx.find("Requirement already satisfied:") != -1:
                installation_Package_result = True
        return installation_Package_result

    def _check_NC_new_file(self):
        file_directory = WATCH_FILE_DIRECTORY
        file_check = 0
        file_error = None

        if not os.path.exists(file_directory):
            os.makedirs(file_directory)

        onlyfiles = [f for f in listdir(file_directory) if isfile(join(file_directory, f))]
        if len(onlyfiles) > 0:
            if os.path.splitext(onlyfiles[0])[1] == ".filepart":
                return file_check
            else:
                cmd_copy = "cp " + NetworkControllerUpdateManager.HOME_FILE_DIRECTORY + "/" + NetworkControllerUpdateManager.SCRIPT_FILE_NAME + " " + WATCH_FILE_DIRECTORY + ";"
                subprocess.Popen([cmd_copy], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).communicate()
                time.sleep(1)
        if len(onlyfiles) > 1:
            print "Got files "
            extension1 = os.path.splitext(onlyfiles[0])[1]
            extension2 = os.path.splitext(onlyfiles[1])[1]
            wait_for_file = True

            while wait_for_file:
                try:
                    size_of_file1 = os.path.getsize(file_directory + "/" + onlyfiles[0])
                    size_of_file2 = os.path.getsize(file_directory + "/" + onlyfiles[1])
                    time.sleep(5)
                    if os.path.getsize(file_directory + "/" + onlyfiles[0]) != size_of_file1 or os.path.getsize(
                            file_directory + "/" + onlyfiles[1]) != size_of_file2:
                        continue
                except Exception as e:
                    log.error(e.args)
                    file_error = e.args
                except OSError as error:
                    log.error(error)
                    file_error = error
                wait_for_file = False

            if extension1 != ".whl" and extension1 != ".sh":
                try:
                    os.remove(file_directory + "/" + onlyfiles[0])
                except OSError as error:
                    file_error = error
                    log.error(error)

            if extension2 != ".whl" and extension2 != ".sh":
                try:
                    os.remove(file_directory + "/" + onlyfiles[1])
                except OSError as error:
                    log.error(error)
                    file_error = error
            else:
                if extension1 == ".whl":
                    script = "./install-whl2.sh " + onlyfiles[0]
                    file_check = 1
                else:
                    script = "./install-whl2.sh " + onlyfiles[1]
                    file_check = 2

            if file_error:
                log.debug("Firmware File Loading error %s" % file_error)
        
        return file_check

    def _install_whl_file(self, file_check):
        file_directory = WATCH_FILE_DIRECTORY
        permittion_result = False
        main_loop_enable = True
        installation_result = False
        continue_counter = 3

        while main_loop_enable:
            onlyfiles = [f for f in listdir(file_directory) if isfile(join(file_directory, f))]
            if len(onlyfiles) > 1:
                log.debug(" got files ")
                if file_check == 1:
                    script = "./install-whl2.sh " + onlyfiles[0]
                    if not permittion_result:
                        x = onlyfiles[0].split("+")
                        logging.getLogger('EventLog').log(logging.INFO, "Firmware %s Loaded" % x[0], extra=EventType.FIRMWARE_UPDATE._dict)
                        logging.getLogger('EventLog').log(logging.INFO, "Preparing to Update", extra=EventType.FIRMWARE_UPDATE._dict)
                else:
                    script = "./install-whl2.sh " + onlyfiles[1]
                    if not permittion_result:
                        x = onlyfiles[1].split("+")
                        logging.getLogger('EventLog').log(logging.INFO, "Firmware %s Loaded" % x[0], extra=EventType.FIRMWARE_UPDATE._dict)
                        logging.getLogger('EventLog').log(logging.INFO, "Preparing to Update", extra=EventType.FIRMWARE_UPDATE._dict)

                if not permittion_result:
                    logging.getLogger('EventLog').log(logging.INFO, "Installing Updates", extra=EventType.FIRMWARE_UPDATE._dict)
                permittion_result = False
                shellscript = subprocess.Popen([script], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, cwd=file_directory + "/").communicate()
                for x in shellscript:
                    log.debug(x)
                    if (x.find('Successfully installed ts-net-controller') != -1):
                        installation_result = True
                    if (x.find('Permission denied') != -1 and x.find('install-whl2.sh') != -1):
                        cmd2 = subprocess.Popen(['chmod 777 install-whl2.sh'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, cwd=file_directory + "/").communicate()
                        for xxx in cmd2:
                             log.debug(xxx)
                        permittion_result = True
                if permittion_result:
                    if continue_counter:
                        continue_counter = continue_counter - 1
                        continue

                if installation_result:
                    logging.getLogger('EventLog').log(logging.INFO, "Firmware Update Completed Successfully", extra=EventType.FIRMWARE_UPDATE._dict)
                else:
                    logging.getLogger('EventLog').log(logging.INFO, "Firmware Update Failed", extra=EventType.FIRMWARE_UPDATE._dict)
                time.sleep(2)
                try:
                    os.remove(file_directory + "/" + onlyfiles[0])
                    os.remove(file_directory + "/" + onlyfiles[1])
                except OSError as error:
                    log.error(error)
            main_loop_enable = False
        return installation_result


    def get_config_file(self):
        class ConfigFile(object):
            def __init__(self):
                self.software_version = "0.4.0"
                PickleHelper(self, None, 'site.pkl').load_config()
        return ConfigFile()

    def restart_NC(self):
        restart_results = False
        try:
            import paramiko
            #paramiko.util.log_to_file("/home/netctrl/test_folder_hayat/example_debug_4_.log")
            ssh = paramiko.SSHClient()

            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname='localhost', username='snap', password=NetworkControllerUpdateManager.SYSTEM_PASS)
            stdin, stdout, stderr = ssh.exec_command('sudo service ts-net-controller restart', get_pty=True)
            # you have to check if you really need to send password here
            stdin.write(NetworkControllerUpdateManager.SYSTEM_PASS + '\n')
            stdin.flush()

            # restarting secound try if first failed
            stdin, stdout, stderr = ssh.exec_command('sudo service ts-net-controller restart', get_pty=True)
            # you have to check if you really need to send password here
            stdin.write(NetworkControllerUpdateManager.SYSTEM_PASS + '\n')
            stdin.flush()
            # if restart is sucessfull then code below this will not execute
            restart_results = True
            ssh.close()

        except Exception as e:
            log.error(e.args)
            logging.getLogger('EventLog').log(logging.INFO, "NC rebooting failed. Error logs %s " % e.args, extra=EventType.FIRMWARE_UPDATE._dict)
        return restart_results

    def update_config_file(self, key, value):
        PickleHelper(self, None, 'site.pkl').update_single_entry_config(key, value)

    def _set_NC_Version_update(self):
        config = self.get_config_file()
        if config.software_version != NC_Softwere_version and self._software_upgrading:
            key = "software_version"
            value = NC_Softwere_version
            self.update_config_file(key, value)
            if config.software_version != "0.4.0":
                NC_SOFTWARE_VERSION_LOG = "Network Controller Firmware Upgraded from %s to %s " % (config.software_version, NC_Softwere_version)
            else:
                NC_SOFTWARE_VERSION_LOG = "Network Controller Firmware Upgraded to %s " % NC_Softwere_version
                log.debug("Did not get software version from config file")

            logging.getLogger('EventLog').log(logging.INFO, " %s " % NC_SOFTWARE_VERSION_LOG, extra=EventType.FIRMWARE_UPDATE._dict)
            self._software_upgrading = False

if __name__ == '__main__':
    import tornado.ioloop
    import datetime
    from tornado.ioloop import PeriodicCallback
    import tornado.ioloop
    logging.basicConfig(level=logging.DEBUG)


    def test_NC_Update_Itself_logs():
        logging.basicConfig(level=logging.INFO)

        def dummy_scheduler(seconds, func):
            pcb = PeriodicCallback(func, seconds * 100)
            pcb.start()
            return pcb

        def call_NC_update():
            NetworkControllerUpdateManager(dummy_scheduler)

        tornado.ioloop.IOLoop.current().add_timeout(datetime.timedelta(seconds=0), call_NC_update)
        tornado.ioloop.IOLoop.current().add_timeout(datetime.timedelta(seconds=5), tornado.ioloop.IOLoop.current().stop)
        tornado.ioloop.IOLoop.current().start()

    test_NC_Update_Itself_logs()