"""
To coordinate their values, I wanted to define all of these interrelated
priorities in the same file.
"""
#
# The reasoning behind the current set of assignments:
# Scripts and firmware are required to be able to perform ASSET_MOVING.
# So this puts ASSET_UPDATER higher in priority.
#
# Moving from one "network" to another is not expected to occur often,
# and is currently under user control (user must manually initiate).
# When it IS requested, we need to try and get the job done, so it goes
# next in the list.
#
# Since ASSET_COMMANDER handles things like E-STOP, it seemed reasonable
# to put it next.
#
# Some of these may get changed based on testing
#

ASSET_UPDATER_PRIORITY = 0
ASSET_MOVER_PRIORITY = 1
ASSET_COMMANDER_PRIORITY = 2
BRIDGE_MONITOR_PRIORITY = 3
ASSET_POLLER_PRIORITY = 4
ASSET_FINDER_PRIORITY = 5
LOG_RETRIEVER_PRIORITY = 6
