import cPickle
import os
import binascii

from Asset import Asset

from utils.SystemSettings import SYSTEM_CONFIG_DIRECTORY


DEFAULT_CONFIG_FILENAME = 'assets.pkl'


class AssetManager(object):
    def __init__(self, config_file=None):
        self.assets = {}  # {asset_id: Asset}
        self.discovered_assets = {}  # {asset_id: Asset}

        self.config_file = config_file or os.path.join(SYSTEM_CONFIG_DIRECTORY, DEFAULT_CONFIG_FILENAME)
        self._load_config()
        # Ensure no bad records (from an early engineering.spec related bug)
        fixups = False
        for asset in self.assets.values():
            if asset.id != 'serial':
                if asset.snap_address is None:
                    asset.snap_address = binascii.unhexlify(asset.id)
                    fixups = True
        if fixups:
            self.save_config()

    def _load_config(self):
        if os.path.exists(self.config_file):
            with open(self.config_file, 'rb') as f:
                assets = cPickle.load(f)
            self.assets.update(assets)

    def save_config(self):
        if not os.path.exists(os.path.dirname(self.config_file)):
            os.mkdir(os.path.dirname(self.config_file))

        with open(self.config_file, 'wb') as f:
            cPickle.dump(self.assets, f, protocol=cPickle.HIGHEST_PROTOCOL)

    def get_serial_asset(self):
        if 'serial' not in self.assets:
            self.assets['serial'] = Asset('serial')

        return self.assets['serial']

    def get_asset(self, id):
        if id not in self.assets:
            self.assets[id] = Asset(id)

        return self.assets[id]

    def get_asset_if_known(self, id):
        if id not in self.assets:
            return None
        else:
            return self.assets[id]

    def get_assets(self):
        return self.assets

    def get_discovered_asset(self, id):
        if id not in self.discovered_assets:
            self.discovered_assets[id] = Asset(id)

        return self.discovered_assets[id]

    def get_discovered_assets(self):
        return self.discovered_assets

    def clear_discovered_assets(self):
        self.discovered_assets.clear()

    def move_discovered_assets(self, discovered_asset_ids):
        """
        Move discovered assets into the (active) assets bucket
        :param list discovered_asset_ids: List of IDs to move
        :return: Assets IDs that were successfully moved
        :rtype: list
        """
        rtn_val = []
        discovered_assets = [self.discovered_assets.pop(_id, None) for _id in discovered_asset_ids]
        for asset in discovered_assets:
            if asset:
                assert asset.id not in self.assets, "Discovered asset already in provisioned asset list"
                self.assets[asset.id] = asset
                rtn_val.append(asset.id)

        self.save_config()

        return rtn_val

    # We later decided we wanted the ability to undo the above
    def move_back_to_discovered(self, asset_ids):
        """
        Move operational assets in the (active) assets bucket
        back into the discovered (unprovisioned) bucket.
        :param list asset_ids: List of IDs to move
        :return: Assets IDs that were successfully moved
        :rtype: list
        """
        rtn_val = []
        assets_to_be_moved = [self.assets.pop(_id, None) for _id in asset_ids]
        for asset in assets_to_be_moved:
            if asset:
                assert asset.id not in self.discovered_assets, "Active asset already in discovered asset list"
                self.discovered_assets[asset.id] = asset
                rtn_val.append(asset.id)

        self.save_config()

        return rtn_val

    def delete_discovered_assets(self, discovered_asset_ids):
        """
        Delete discovered assets (note that if they still exist they
        may be re-discovered via wireless communications)
        :param list discovered_asset_ids: List of IDs to delete
        :return: Assets IDs that were successfully deleted
        :rtype: list
        """
        rtn_val = []
        deleted_assets = [self.discovered_assets.pop(_id, None) for _id in discovered_asset_ids]
        for asset in deleted_assets:
            if asset:
                rtn_val.append(asset.id)

        self.save_config()

        return rtn_val
