"""
AssetPoller.py - The job of this module is to gather data from
COMMISSIONED and PROVISIONED units. This module learns about
these units from the AssetManager (who heard about them from
the AssetDiscovery module).
"""

import logging
import binascii  # For making SNAP Addresses human-readable
import datetime
import copy
import time

import Asset
import NetworkControllerDecoders as Decoders  # new code based on the original SNAPpy
from SnapComm import SnapComm
from PauseButtons import PauseButtonManager, PauseButton
from CommunicationsPriorities import *
from FormatHelpers import *
from controller_enums import EventType

log = logging.getLogger(__name__)
event_log = logging.getLogger('EventLog')

# Pick one! Usually we want this off/False
SHOULD_LOG_POLL_INFO = False
#SHOULD_LOG_POLL_INFO = True

# In an attempt to declare what should be pickle'd the "Pythonic" way I used the copy_reg module
# Apparently this also affects the copy module which I want to act in it's default way in this module
copy.dispatch_table = copy.dispatch_table.copy()
del copy.dispatch_table[Asset.Asset]


class CommStats(object):
    """This is a Data Object holding communications stats for a single object"""
    def __init__(self):
        """Constructor - just resets all stats"""
        self.total_attempts = 0
        self.total_responses = 0
        self.consecutive_responses = 0
        self.total_failures = 0
        self.consecutive_failures = 0


class AssetPoller(object):
    """
    The job of this class is to poll provisioned nodes
    by making over-the-air queries.
    """
    # We have changed the OFFLINE/ONLINE criteria, trying to reduce the
    # number of events logged about this
    CONSECUTIVE_FAILURES_UNTIL_OFFLINE = 12
    CONSECUTIVE_RESPONSES_UNTIL_ONLINE = 3
    POLLING_BATCH_SIZE = 10  # TODO - Decide on how many to try and poll at a time
    PANEL_CMD_MOVE_TO_PRESET = 5

    def __init__(self, asset_manager,
                 snap_connect, schedule_func,
                 max_hops=10, spread=10,
                 on_asset_changed=None,
                 on_assets_reporting_change=None,
                 on_report_default_status=None,
                 on_asset_reboot=None,
                 on_asset_preset_change=None):
        """
        Constructor.
        :param asset_manager: holder of ALL the Asset objects
        :param snap_connect: the object we use to do SNAP communications
        :param schedule_func: used for timing
        :param int max_hops: Optional. Maximum number of hops to send poll_report RPC
        :param int spread: Optional. Spread value to use in directed-multicast
        :param func on_asset_changed: Optional. Callable for when an asset is changed. Signature is func(asset_that_changed)
        :param func on_assets_reporting_change: Optional. Callable for when the number of reporting assets has changed. Signature is func(assets_reporting, total_assets, msg)
        """
        self._asset_manager = asset_manager
        self._snap_connect = snap_connect
        self._schedule_func = schedule_func
        self.on_asset_changed = on_asset_changed
        self._on_assets_reporting_change = on_assets_reporting_change
        self.on_report_default_status = on_report_default_status
        self.on_asset_reboot = on_asset_reboot
        self.on_asset_preset_change = on_asset_preset_change

        #self.poll_cycle = 3.0  # This is about not hogging the airwaves
        self.poll_cycle = 4.0  # This is about not hogging the airwaves
        #self.poll_cycle = 5.0  # This is about not hogging the airwaves

        self.max_hops = max_hops  # This is a config setting, not a status field!
        self.spread = spread

        # Added more fields on 06/15/2017 for display in the "Mesh Network"
        #  card in the SLUI
        self.assets_reporting = None
        self.total_assets = None
        self.latest_poll = None
        self.latest_response = None
        self.max_mesh_depth = None
        self.avg_mesh_depth = None
        self.report_default_status()

        # internal counter used to compute assets_responding
        self._reported_so_far = 0

        self._snap_connect.add_rpc_func("report", self.report)
        self._snap_connect.add_rpc_func("apcb_script_crc", self.apcb_script_crc)

        #self._blob_schedule = "srstysusvyswsxyOu"  # I will be tweaking this
        #self._blob_schedule = "sytsynsyusyvsynsywsyxsynsyO"  # per Kevin on 200721
        #self._blob_schedule = "ysytysynysyuysyvysywysynysyxO"  # 08/04/2020 proposal
        self._blob_schedule = "ysytysynysyuysyvysywysynysyxysyO"  # 08/04/2020 proposal 2

        self._blob_schedule_index = len(self._blob_schedule)  # Force restart of schedule

        self._offline_units = set()
        self._current_set = set()
        self._current_subset = set()
        self._special_subset = set() # Units in special modes that are worth focusing our attention on

        self._comm_stats = {}

        self._periodic = None
        self.schedule_poll(self.poll_cycle)

        self._schedule_func(60.0, self.dump_live)  # This is test code, and can later be removed

        self._serial_asset_copy = copy.copy(self._asset_manager.get_asset('serial'))

        pause_button_manager = PauseButtonManager.get_default_instance()
        self._pause_button = pause_button_manager.get_pause_button(ASSET_POLLER_PRIORITY)
        if SHOULD_LOG_POLL_INFO == True:
            self._last_poll_class = None
            self._last_poll_time = time.time()

        self.do_extra_status_poll = False # Used for units in special modes

    def schedule_poll(self, delay):
        if self._periodic is not None:
            self._periodic.stop()

        # Insert a longer pause between individual blob-gatherings...
        if len(self._current_set) == 0:
            self._periodic = self._schedule_func(self.poll_cycle, self.poll_units)
        else:
            # ... but within a blob gathering we pause less
            self._periodic = self._schedule_func(delay, self.poll_units)

    def analyze_mesh(self):
        """Computes stats like max_mesh_depth and avg_mesh_depth"""
        self.max_mesh_depth = 0
        total_mesh_depth = 0
        asset_count = 0

        assets = self._asset_manager.get_assets().values()
        for asset in assets:
            if asset.snap_address is not None:
                if self.max_mesh_depth < asset.radio_mesh_depth:
                    self.max_mesh_depth = asset.radio_mesh_depth
                if asset.radio_mesh_depth is not None:
                    total_mesh_depth += asset.radio_mesh_depth
                asset_count += 1
        if asset_count > 0:
            self.avg_mesh_depth = float(total_mesh_depth) / asset_count
            log.debug("max_mesh_depth=%d avg_mesh_depth=%f" % (self.max_mesh_depth, self.avg_mesh_depth))

    def report_default_status(self):
        if callable(self.on_report_default_status):
            try:
                current_set = set(self._asset_manager.get_assets().keys())
                copy_of_set = current_set.copy()
                for asset_key in copy_of_set:
                    asset = self._asset_manager.get_asset(asset_key)
                    if asset.snap_address is not None:
                        self.on_report_default_status(asset)
                current_set.clear()
            except Exception:
                log.debug("Error: unable to send default status")
        

    def determine_special_units(self):
        self._special_subset = set()
        asset_keys = self._asset_manager.get_assets().keys()
        MAX_SPECIAL_UNITS = 5 # If everybody is special, no one is...

        # In order to prioritize, we will make multiple passes
        # We care most about FastTrak mode
        for asset_key in asset_keys:
            if asset_key == 'serial':
                continue
            asset = self._asset_manager.get_asset(asset_key)
            # Is this asset in FastTrak mode?
            if (asset.tracking_status_bits & 0x20) != 0x00:
                self._special_subset.add(asset_key)
                if len(self._special_subset) >= MAX_SPECIAL_UNITS:
                    return

        # We care second about FCS mode
        for asset_key in asset_keys:
            if asset_key == 'serial':
                continue
            asset = self._asset_manager.get_asset(asset_key)
            # Is this asset in FCS mode?
            if ((asset.tracking_status_bits & 0x08) == 0x00) and (asset.offline_minutes != 0):
                self._special_subset.add(asset_key)
                if len(self._special_subset) >= MAX_SPECIAL_UNITS:
                    return

        # We care third about IRC mode
        for asset_key in asset_keys:
            if asset_key == 'serial':
                continue
            asset = self._asset_manager.get_asset(asset_key)
            # Is this asset in IRC mode?
            if (asset.tracking_status_bits & 0x08) != 0x00:
                self._special_subset.add(asset_key)
                if len(self._special_subset) >= MAX_SPECIAL_UNITS:
                    return

        return

    def poll_units(self):
        """Periodic poll, gathering data"""
        self._periodic.stop()

        # Are we done with the current batch of units?
        if len(self._current_set) == 0:
            # Compute some mesh stats for the SLUI
            self.analyze_mesh()

            self.determine_special_units()

            # this is also a good pausing point...
            # Get off the airwaves if something more important is going on
            if self._pause_button.paused:
                # check back in a minute to see if we can resume
                self.schedule_poll(60)
                return

            # Create a new batch
            self._current_set = set(self._asset_manager.get_assets().keys())

            # Due to the existence of the NCCB, plus various test data,
            # not every Asset that exists is one WE should be polling.
            # Filter the set.
            copy_of_set = self._current_set.copy()
            for asset_key in copy_of_set:
                asset = self._asset_manager.get_asset(asset_key)
                if asset.snap_address is None:
                    self._current_set.remove(asset_key)

            self.total_assets = len(self._current_set)
            # The following could be considered "unsmoothed" data.
            # We decided we did not like it on the UI.
            # self.assets_reporting = self._reported_so_far
            # The following represents a more "smoothed" view, since
            # we give units multiple tries before declaring them offline.
            self.assets_reporting = len(self._current_set) - len(self._offline_units)
            self._reported_so_far = 0

            # Advance to the next item on the schedule
            self._blob_schedule_index += 1
            if self._blob_schedule_index >= len(self._blob_schedule):
                self._blob_schedule_index = 0

            self._command = self._blob_schedule[self._blob_schedule_index]
            if self._command == 'y':  # 'y' as in BLOB_Y - WX data
                # Only ask units that have reported themselves as
                # "WX-equipped" for BLOB_Y data
                copy_of_set = self._current_set.copy()
                for asset_key in copy_of_set:
                    asset = self._asset_manager.get_asset(asset_key)
                    if not asset.has_weather_sensor:
                        self._current_set.remove(asset_key)
            elif self._command == 'O':  # 'O' as in 'O'ffline
                # We used to attempt an 's' command here, but if the STM32 is dead then
                # that command can fail. Switching to 'r' which only requires the RF220
                # to be working
                self._command = 'r'
                # Notice that here we did NOT filter out the offline units...
            # For now, we will try and poll units even if they seem offline
            #else:
            #    # Minimize the time we spend on offline units
            #    for asset_key in self._offline_units:
            #        if asset_key in self._current_set:
            #            self._current_set.remove(asset_key)

            # When a field is first being deployed, there may be a delay
            # before units actually start coming online.
            # Another scenario is this is the WX poll (blob 'y') but we don't
            # know who the weather stations are (yet) (or it is a site without them)
            if len(self._current_set) == 0:
                self.schedule_poll(0.1)
                return  # With no Assets, NC should not be stopped for reporting

            self.do_extra_status_poll = True

            if SHOULD_LOG_POLL_INFO == True:
                # see if a new set poll set is being used, if so create a
                # timestamped log entry
                if self._command != self._last_poll_class:
                    time_now = time.time()
                    log_entry = ',Polled asset class,%s,%s,%f,' % (self._last_poll_class, time_now, time_now-self._last_poll_time)
                    #print log_entry
                    event_log.info(log_entry,
                         extra=EventType.NETWORK_CONTROLLER._dict)
                    self._last_poll_class = self._command
                    self._last_poll_time = time_now

        # Build up a set and list of units to poll. Note that this might be made up of
        # only special units (every other time), only regular units (if there ARE NO specials),
        # or a mix of specials followed by enough regulars to fill up the current batch.

        self._current_subset = set() # This is used when processing responses
        addresses = "\x00\x00\x00"  # work-around for dmcast "slot 0" bug
        count = AssetPoller.POLLING_BATCH_SIZE

        special_units = self._special_subset.copy() # making a copy because we will empty this out
        # If no special units, we don't have to insert an extra 's'tatus poll for them
        if len(special_units) == 0:
            self.do_extra_status_poll = False
        # If this IS a 's'tatus poll already, we don't want to insert EXTRA 's'tatus polls
        if self._command == 's':
            self.do_extra_status_poll = False

        done = False
        while not done:
            if self.do_extra_status_poll:
                # Here we are doing an extra 's'tatus poll of JUST the special units
                if len(special_units) > 0:
                    asset_key = special_units.pop()  # grab an arbitrary unit from the set of special units
                else:
                    done = True
                    continue
            else:
                # Here we are doing the regularly scheduled poll, but giving PREFERENCE to special units
                # except that we are ignoring special units on weather polls
                if (self._command != 'y') and (len(special_units) > 0):
                    asset_key = special_units.pop()  # grab an arbitrary unit from the set of special units
                elif len(self._current_set) > 0:
                    asset_key = self._current_set.pop()  # grab an arbitrary unit from the standard set
                else:
                    done = True
                    continue

            # Make sure the polling of special units does not result in them being
            # polled TWICE in the same chunk
            if asset_key in self._current_subset:
                continue

            if asset_key not in self._comm_stats:
                self._comm_stats[asset_key] = CommStats()
            self._comm_stats[asset_key].total_attempts += 1

            self._current_subset.add(asset_key)

            # For a brief period, id and snap_address were the same.
            # Now we have to actually fetch the record and look up
            # the SNAP Address.
            asset = self._asset_manager.get_asset(asset_key)
            asset.radio_polls_sent += 1
            addresses += asset.snap_address
            count -= 1
            if count == 0:
                done = True

        if self.do_extra_status_poll:
            command = 's' # 's' stands for 's'tatus
        else:
            command = self._command

        self._snap_connect.dmcast_rpc(addresses, 0x8000, self.max_hops, 5,
                                      "poll_report",
                                      0, command, self.max_hops, self.spread)

        self.latest_poll = datetime.datetime.now()
        self.do_extra_status_poll = not self.do_extra_status_poll # toggle for next time

        # As other timings get adjusted, remember that this needs to be long
        # enough for all nodes in the subset to have time to respond!
        # We are trying out an increase in the timeout here based on the theory
        # that the units ARE responding but the NC is not waiting long enough
        #self._scheduled_timeout = self._schedule_func(0.5, self.timeout_handler)
        self._scheduled_timeout = self._schedule_func(1.0, self.timeout_handler)
        #self._scheduled_timeout = self._schedule_func(1.5, self.timeout_handler)
        #self._scheduled_timeout = self._schedule_func(2.0, self.timeout_handler)

    def notify_cloud(self, orig_asset, asset):
        if callable(self.on_asset_changed):
            try:
                self.on_asset_changed(orig_asset, asset)
            except (SystemExit, KeyboardInterrupt):
                raise
            except:
                log.exception("An error occurred while trying to notify that an asset changed")


    def notify_cloud_asset_reboot(self, previous_uptime, asset):
        if callable(self.on_asset_reboot):
            try:
                self.on_asset_reboot(previous_uptime, asset)
            except (SystemExit, KeyboardInterrupt):
                raise
            except:
                log.exception("An error occurred while trying to notify that an asset rebooted")


    def notify_cloud_asset_preset(self, previous_panel_index, previous_command_state, asset):
        if callable(self.on_asset_preset_change):
            try:
                self.on_asset_preset_change(previous_panel_index, previous_command_state, asset)
            except (SystemExit, KeyboardInterrupt):
                raise
            except:
                log.exception("An error occurred while trying to notify that an asset rebooted")

    def log_online_offline_event(self, asset):
        event_text = 'Asset ' + format_asset_identity(asset) + ' is now '
        if asset.offline:
            event_text += 'offline'
        else:
            event_text += 'online'
        event_log.info(event_text, extra=EventType.INDIVIDUAL_ASSET._dict)

    def timeout_handler(self):
        """Penalize units that do not respond in time"""
        self._scheduled_timeout.stop()
        self._scheduled_timeout = None

        assets_not_responding = []

        while len(self._current_subset) > 0:
            asset_key = self._current_subset.pop()

            self._comm_stats[asset_key].consecutive_responses = 0
            self._comm_stats[asset_key].total_failures += 1
            self._comm_stats[asset_key].consecutive_failures += 1

            if self._comm_stats[asset_key].consecutive_failures >= AssetPoller.CONSECUTIVE_FAILURES_UNTIL_OFFLINE:
                asset = self._asset_manager.get_asset(asset_key)
                # Handle the initial transition to OFFLINE
                if asset_key not in self._offline_units:
                    self._offline_units.add(asset_key)
                    if asset is not None:
                        orig_asset = copy.copy(asset)
                        asset.offline = True  # (for WeatherMonitor, possibly others in the future)
                        self.log_online_offline_event(asset)
                        # This is also now where we notify the cloud that the unit has gone offline
                        self.notify_cloud(orig_asset, asset)
                    assets_not_responding.append(asset_key)
                # Also make periodic attempts at handling units that have no script in them
                if (asset is not None) and (asset.radio_script_crc is None):
                    if (self._comm_stats[asset_key].consecutive_failures % AssetPoller.CONSECUTIVE_FAILURES_UNTIL_OFFLINE) == 0:
                        # Last-ditch-effort for the scenario of a unit that has lost it's script -
                        # No script == No blobs == no script CRC update
                        self._snap_connect.rpc(asset.snap_address, 'callback', 'apcb_script_crc', SnapComm.FunctionCode('loadNvParam'), 40)

        if assets_not_responding:
            self.assets_reporting = self.total_assets - len(self._offline_units)  # Update counts
            self._notify_assets_reporting_change("Asset %s has stopped sending data" % ', '.join(assets_not_responding))

        self._check_serial_asset_change()

        self.schedule_poll(0.1)

    def apcb_script_crc(self, script_crc):
        snap_address = self._snap_connect.rpc_source_addr()
        snap_address = binascii.hexlify(snap_address)
        asset = self._asset_manager.get_asset(snap_address)
        if asset is not None:
            asset.radio_script_crc = script_crc

    def report(self, response):
        """Response handler for the reply to poll_report()"""
        self.latest_response = datetime.datetime.now()

        snap_address = self._snap_connect.rpc_source_addr()
        snap_address = binascii.hexlify(snap_address)
        log.info("Live unit %s reported in", snap_address)

        asset_key = snap_address  # for now - we may come up with a different scheme

        self._comm_stats.setdefault(asset_key, CommStats()).consecutive_failures = 0
        self._comm_stats[asset_key].total_responses += 1
        self._comm_stats[asset_key].consecutive_responses += 1

        # The "if_known" check here is to prevent being confused by OVERHEARD conversations
        # between the FCS and an Asset that WE (the NC) have not been provisioned with yet
        # See TSTRAC-260 for more details...
        asset = self._asset_manager.get_asset_if_known(asset_key)
        if asset is None:
            return

        asset.radio_poll_responses += 1
        asset.last_reported = datetime.datetime.utcnow()

        orig_asset = copy.copy(asset)
        if asset_key in self._offline_units:
            if self._comm_stats[asset_key].consecutive_responses >= AssetPoller.CONSECUTIVE_RESPONSES_UNTIL_ONLINE:
                self._offline_units.remove(asset_key)
                self.assets_reporting += 1 # Required for self._notify_assets_reporting_change() to work
                self._notify_assets_reporting_change("Asset %s is now responding to data requests" % asset_key)
                asset.offline = False  # (for WeatherMonitor, possibly others in the future)
                self.log_online_offline_event(asset)
        else:
            asset.offline = False

        if len(response) >= 1:
            blob_id = response[0]
            if blob_id == 'n':
                Decoders.decode_blob_n(asset, response)
            elif blob_id == 'r':
                Decoders.decode_blob_r(asset, response)
            elif blob_id == 's':
                # We may move to something more explicit and/or sophisticated, but
                # for now I want to capture when units restart. I am looking at their
                # reported uptimes in order to detect this.
                previous_uptime = asset.uptime
                previous_panel_index = asset.panel_index
                previous_command_state = asset.panel_command_state
                previous_tracking_status = asset.tracking_status
                Decoders.decode_blob_s(asset, response)
                if asset.uptime < previous_uptime:
                    event_text = 'Asset ' + format_asset_identity(asset) + ' appears to have restarted -'
                    event_text += ' uptime was '
                    event_text += format_uptime_seconds(previous_uptime)
                    event_text += ', now '
                    event_text += format_uptime_seconds(asset.uptime)
                    event_log.info(event_text, extra=EventType.INDIVIDUAL_ASSET._dict)
                    self.notify_cloud_asset_reboot(previous_uptime, asset)
                try:
                    if (asset.panel_index != previous_panel_index or asset.panel_command_state != previous_command_state or previous_tracking_status != asset.tracking_status):
                        self.notify_cloud_asset_preset(previous_panel_index, previous_command_state, asset)
                except:
                    pass
            elif blob_id == 't':
                d = {}
                try:
                    orig_asset_segments = []
                    for j in xrange(2):
                        orig_asset_segments.append([])
                        orig_asset_segments[j] = copy.copy(asset.segments[j])
                    Decoders.decode_blob_t(asset, response)
                    for i in xrange(2):
                        asset_dif2 = {k:v for k, v in asset.segments[i].__dict__.iteritems() if orig_asset_segments[i].__dict__.get(k, None) != v}
                        d.update(asset_dif2)
                except Exception as e:
                    log.error(e.args)
                if len(d):
                    asset.segments_change = True
                else:
                    asset.segments_change = False
            elif blob_id == 'u':
                Decoders.decode_blob_u(asset, response)
            elif blob_id == 'v':
                Decoders.decode_blob_v(asset, response)
            elif blob_id == 'w':
                Decoders.decode_blob_w(asset, response)
            elif blob_id == 'x':
                orig_asset_preset_angles = []
                dd = {}
                try:
                    for j in xrange(16):
                        orig_asset_preset_angles.append([])
                        orig_asset_preset_angles[j] = copy.copy(asset.preset_angles[j])
                    Decoders.decode_blob_x(asset, response)
                    for i in xrange(16):
                        asset_dif2 = {k: v for k, v in asset.preset_angles[i].__dict__.iteritems() if
                                      orig_asset_preset_angles[i].__dict__.get(k, None) != v}
                        dd.update(asset_dif2)
                except Exception as e:
                    log.error(e.args)
                if len(dd):
                    asset.preset_angles_change = True
                else:
                    asset.preset_angles_change = False
            elif blob_id == 'y':
                Decoders.decode_blob_y(asset, response)

            self.notify_cloud(orig_asset, asset)

        # Since they DID respond, remove them from this batch
        # The extra check-and-return here is for the scenario
        #  where a response comes in RIGHT AFTER we have timed out
        if asset_key in self._current_subset:
            self._current_subset.remove(asset_key)
        else:
            return

        self._reported_so_far += 1

        # If all in this batch responded, cancel the timeout
        if len(self._current_subset) == 0:
            if self._scheduled_timeout is not None:
                self._scheduled_timeout.stop()
                self._scheduled_timeout = None

            self._check_serial_asset_change()

            self.schedule_poll(0.01)

    def dump_live(self):
        """Just some debug logging"""
        log.info("Live assets: " + str(self._asset_manager.get_assets().keys()))
        log.info("Offline assets: " + str(self._offline_units))
        if self.total_assets is not None:
            log.info("assets_reporting=%d total_assets=%d" % (self.assets_reporting, self.total_assets))
        # log.info("max_mesh_depth=%d avg_mesh_depth=%f" % (self.max_mesh_depth, self.avg_mesh_depth))
        # log.info("latest_poll=%r latest_response=%r" % (self.latest_poll, self.latest_response))

    def _notify_assets_reporting_change(self, msg):
        """
        Notify the subscriber that the number of assets reporting has changed
        :param str msg: The event log message string to log
        :return: None
        """
        if self._on_assets_reporting_change:
            try:
                self._on_assets_reporting_change(self.assets_reporting,
                                                 self.total_assets,
                                                 "Only %i assets are reporting out of %i" % (
                                                     self.assets_reporting, self.total_assets))
            except:
                pass

    def _check_serial_asset_change(self):
        if callable(self.on_asset_changed):
            serial_asset = self._asset_manager.get_serial_asset()
            try:
                self.on_asset_changed(self._serial_asset_copy, serial_asset)
            except (SystemExit, KeyboardInterrupt):
                raise
            except:
                log.exception("An error occurred while trying to notify that the serial asset changed")
            self._serial_asset_copy = copy.copy(serial_asset)

#
# Make it so running module standalone results in a functional test
#
if __name__ == "__main__":
    import tornado.ioloop
    from SnapComm import SnapComm
    import AssetDiscovery
    from asset_manager import AssetManager
    from Scheduler import Scheduler

    # logging.basicConfig(level=logging.WARNING)
    logging.basicConfig(level=logging.INFO)
    # logging.basicConfig(level=logging.DEBUG)

    snapcomm = SnapComm()

    asset_manager = AssetManager()

    asset_finder = AssetDiscovery.AssetFinder(asset_manager,
                                              snapcomm.sc,
                                              Scheduler.schedule_func,
                                              2,
                                              1000)

    asset_poller = AssetPoller(asset_manager,
                               snapcomm.sc,
                               Scheduler.schedule_func,
                               2,
                               1000)

    # In the real system, USERS trigger this move of assets from the
    # "unprovisioned"" bucket to the "live"" bucket
    def auto_provisioner():
        unprovisioned_assets = asset_manager.get_discovered_assets()
        for key in unprovisioned_assets:
            asset = unprovisioned_assets[key]
            # (Dodging some test data / dev code. Not every asset is REAL)
            if asset.snap_address is not None:
                # log.info("key=" + key + " snap_address=" + binascii.hexlify(asset.snap_address))
                # Important! Here we want to TRANSFER an Asset, not
                # make a new one (thus losing data). DO NOT CHANGE
                # the following to asset_manager.get_asset(asset.id)!
                asset_manager.get_assets()[asset.id] = asset
        asset_manager.clear_discovered_assets()

    Scheduler.schedule_func(10, auto_provisioner)

    tornado.ioloop.IOLoop.current().start()
