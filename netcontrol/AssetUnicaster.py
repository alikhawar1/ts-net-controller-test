"""
AssetUnicaster.py - The job of this module is to hide the OTA API from
the top-level code base, and replace it with a more generic one.

"""

import logging
import datetime
from controller_enums import CommandSource


class AssetUnicaster(object):
    """
    converts generic commands from the high-level code into specific
    SNAP Connect RPC calls (SNAPpy script dependent)
    """
    def __init__(self, snap_connect):
        """
        Constructor. Parameters include:
        snap_connect - the object we use to do SNAP radio communications
        asset_id - snap address of the asset to be commanded
        """
        if snap_connect is None:
            raise Exception("Error: AssetUnicaster requires a SNAP Connect!")
        else:
            self._snap_connect = snap_connect
    
    def _set_offline(self, asset_id, timeout=0x7fff):
        self._snap_connect.rpc(asset_id, "go_offline", timeout)

    def _set_online(self, asset_id):
        self._snap_connect.rpc(asset_id, "go_online")

    def stop(self, asset_id):
        """
        Stop any panel movement
        :return: None
        """
        self._set_offline(asset_id)
        self._snap_connect.rpc(asset_id, "stop", CommandSource.MC.value)

    def go_to_preset(self, asset_id, index):
        """Command the panel to go to a preconfigured preset angle"""
        self._set_offline(asset_id)
        self._snap_connect.rpc(asset_id, "move_to_preset", CommandSource.MC.value, index)

    def go_to_abs(self, asset_id, angle):
        """Command the panel to go to an absolute angle"""
        self._set_offline(asset_id)
        self._snap_connect.rpc(asset_id, "move_to_abs", CommandSource.MC.value, angle)
    
    def resume_normal(self, asset_id):
        """Command the panels to resume normal behavior from manual control mode"""
        self._set_online(asset_id)

if __name__ == "__main__":

    import tornado.ioloop
    from SnapComm import SnapComm
    from controller_enums import CommandSource, TrackerPresets
    from Scheduler import Scheduler

    logging.basicConfig(level=logging.DEBUG)

    snapcomm = SnapComm()
    asset_id = '13b4c3'.decode('hex')

    asset_unicaster = AssetUnicaster(snapcomm.sc, asset_id)

    stow_event_handle = None

    asset_unicaster.set_offline(40)
    def set_preset_test():
        asset_unicaster.go_to_preset(TrackerPresets.FLAT.value)

    stow_event_handle = Scheduler.schedule_func(5.0, set_preset_test)

    angle_event_handle = None
    tornado.ioloop.IOLoop.current().start()
