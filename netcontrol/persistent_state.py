"""
persistent_state.py - this module implements the PersistentState object,
which handles saving a small handful (initially just one) bits of
information across runs of the program.

This file is an application-specific layer on top of the more generic
nvram_storage module.
"""

import nvram_storage

class PersistentState(object):
    """
    This is application-specific code. It puts a high-level API on top
    of a low-level NVRAM storage capability.
    """

    NO_SUCH_VERSION = -1 # sentinel value, used when no config can be loaded

    # The low level code ensures it's really OUR data in the NVRAM.
    # This first byte is just to support future data VERSIONING
    PERSISTENCE_VERSION_INDEX = 0
    PERSISTENCE_VERSION = 1 # was 0 until 04/23/2019

    # Up through 04/23/2019, we were only persisting a single STATE byte
    # (and only using two bits out of the total of 8)
    # We are adding more bits and more distinct fields

    # Our first state will be a set of 8 bit flags
    # This data existed even in PERSISTENCE_VERSION 0
    STATE_BITS_0_TO_7_INDEX = 1

    SNOW_STOW_BIT = 0x01  # <- this one refers to snow on the ground...
    PANEL_SNOW_STOW_BIT = 0x02  # <- this one refers to snow up on the solar panel
    WIND_STOW_BIT = 0x04
    NIGHTLY_STOW_BIT = 0x08
    # The assumption is that other bits will be defined in the future
    UNUSED_4 = 0x10
    UNUSED_5 = 0x20
    UNUSED_6 = 0x40
    UNUSED_7 = 0x80

    # Next we are adding some "mode config" bits
    MODE_BITS_0_TO_7_INDEX = 2

    TRACKING_ENABLE_BIT = 0x01
    BACKTRACKING_ENABLE_BIT = 0x02
    MANUAL_CONTROL_BIT = 0x04 # <- Any manual commandes are active
    #UNUSED_2 = 0x04
    #UNUSED_3 = 0x08
    #UNUSED_4 = 0x10
    #UNUSED_5 = 0x20
    #UNUSED_6 = 0x40
    #UNUSED_7 = 0x80

    # Next we are adding which PRESET has been selected
    # Note that PRESET is a don't-care if the TRACKING_ENABLE_BIT is set
    PRESET_INDEX = 3
    # PRESET numbers are currently 0-15, but going ahead and allocating an entire byte

    # Finally we are adding a byte to hold the TrackingState of the site
    # Note that this is currently a little redundant with the STATE_BITS up above,
    # but will better allow future expansions.
    TRACKING_STATE_INDEX = 4
    # Another field that we are allocating an entire byte to, even though it only
    # uses a subset of the possible values at present.

    # This completes the "V1" expansion of the persistent data...

    def __init__(self):
        """Constructor"""
        self.nvram = nvram_storage.NvramStorage()
        self.state_string = self.nvram.get_state()

        # Will override these default if NVRAM data looks good
        self.state_bits = 0x00
        self.mode_bits = 0x00
        self.preset = 0x00
        self.tracking_state = 0x00
        self.loaded_version = PersistentState.NO_SUCH_VERSION

        if len(self.state_string) < 2:
            return

        self.loaded_version = ord(self.state_string[PersistentState.PERSISTENCE_VERSION_INDEX])

        # Handle transitioning from V0 to V1 NVRAM
        if self.loaded_version == 0:
            # Sanity check the string
            if len(self.state_string) != 2:
                self.loaded_version = PersistentState.NO_SUCH_VERSION
                return
            self.state_bits = ord(self.state_string[PersistentState.STATE_BITS_0_TO_7_INDEX])
            return

        # The normal case - loading new-format (V1) data
        if self.loaded_version != PersistentState.PERSISTENCE_VERSION:
            self.loaded_version = PersistentState.NO_SUCH_VERSION
            return

        # Sanity check the string
        if len(self.state_string) != 5: # see indexes above...
            self.loaded_version = PersistentState.NO_SUCH_VERSION
            return

        # Grab all the data
        self.state_bits = ord(self.state_string[PersistentState.STATE_BITS_0_TO_7_INDEX])
        self.mode_bits = ord(self.state_string[PersistentState.MODE_BITS_0_TO_7_INDEX])
        self.preset = ord(self.state_string[PersistentState.PRESET_INDEX])
        self.tracking_state = ord(self.state_string[PersistentState.TRACKING_STATE_INDEX])

    def get_state(self, which_bit):
        if self.state_bits & which_bit:
            return True
        else:
            return False

    def set_state(self, which_bit, new_value, save_now=True):
        if self.get_state(which_bit) == new_value:
            return  # no change, nothing to do

        if new_value:
            self.state_bits |= which_bit
        else:
            self.state_bits &= ~which_bit

        if save_now:
            self._persist_state()

    def get_mode(self, which_bit):
        if self.mode_bits & which_bit:
            return True
        else:
            return False

    def set_mode(self, which_bit, new_value, save_now=True):
        if self.get_mode(which_bit) == new_value:
            return  # no change, nothing to do

        if new_value:
            self.mode_bits |= which_bit
        else:
            self.mode_bits &= ~which_bit

        if save_now:
            self._persist_state()

    def get_preset(self):
        return self.preset

    def set_preset(self, new_value, save_now=True):
        if self.get_preset() == new_value:
            return  # no change, nothing to do

        self.preset = new_value

        if save_now:
            self._persist_state()

    def get_tracking_state(self):
        return self.tracking_state

    def set_tracking_state(self, new_value, save_now=True):
        if self.get_tracking_state() == new_value:
            return  # no change, nothing to do

        self.tracking_state = new_value

        if save_now:
            self._persist_state()

    def _persist_state(self):
        # Build up a new state_string.
        self.state_string = chr(PersistentState.PERSISTENCE_VERSION)

        self.state_string += chr(self.state_bits)
        self.state_string += chr(self.mode_bits)
        self.state_string += chr(self.preset)
        self.state_string += chr(self.tracking_state)

        self.nvram.set_state(self.state_string)


#
# Make it so running module standalone lets me step through
# a minimal functional test
#
if __name__ == "__main__":
    test = PersistentState()

    print test.get_state(PersistentState.WIND_STOW_BIT)
    wind_stow_needed = True
    test.set_state(PersistentState.WIND_STOW_BIT, wind_stow_needed)
    print test.get_state(PersistentState.WIND_STOW_BIT)
    wind_stow_needed = False
    test.set_state(PersistentState.WIND_STOW_BIT, wind_stow_needed)
    print test.get_state(PersistentState.WIND_STOW_BIT)
    wind_stow_needed = False
    test.set_state(PersistentState.WIND_STOW_BIT, wind_stow_needed)


    test2 = PersistentState()

    print test2.get_state(PersistentState.PANEL_SNOW_STOW_BIT)

    test2.set_state(PersistentState.PANEL_SNOW_STOW_BIT, False)

    print test2.get_state(PersistentState.PANEL_SNOW_STOW_BIT)
