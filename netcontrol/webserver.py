import os
from functools import partial
import json
import datetime
import calendar
import Cookie
import logging

import tornado.ioloop
import tornado.web
import tornado.httpserver

import graphql_server

from tornado_jwt_auth import jwt_authenticated, jwt_encode, jwt_decode

from pyfiria.gen import pass_lib
from controller_enums import EventType

from utils import SystemSettings


# Add new SameSite cookie attribute
Cookie.Morsel._reserved['samesite'] = 'SameSite'

# Code for generating jwt_secret:
# import base64
# from Crypto.Random import random
# base64.b64encode(''.join([chr(random.getrandbits(8)) for x in xrange(18)]))
settings = {
    "login_url": "/login",
    "xsrf_cookies": True,
    "autoreload": False,
    "serve_traceback": True,
    "jwt_secret": "LJntz9WYv0B+tbuSEyVuHB1q",
    "cookie_secret": 'Dph1TQHTsVHZglOiwgL2RjdY5XY4rC+nwqQCcNfafXPeCo/dA5oza64TfbR7Zmew0y6m'
}

log = logging.getLogger(__name__)
event_log = logging.getLogger('EventLog')


# noinspection PyAbstractClass
class GraphQLHandler(tornado.web.RequestHandler):
    pretty = False

    format_error = staticmethod(graphql_server.default_format_error)
    encode = staticmethod(graphql_server.json_encode)

    # noinspection PyMethodOverriding,PyAttributeOutsideInit
    def initialize(self, schema):
        self.schema = schema

    # noinspection PyBroadException
    def parse_body(self, request):
        content_type = self.request.headers.get('Content-type')
        if content_type == 'application/graphql':
            return {'query': request.body.decode('utf8')}
        elif content_type == 'application/json':
            return graphql_server.load_json_body(request.body.decode('utf8'))
        elif (('application/x-www-form-urlencoded' in content_type) or
                'multipart/form-data' in content_type):
            if 'operations' in request.arguments:
                _query = graphql_server.load_json_body(request.arguments['operations'][0])
                _query['variables'] = {}
                for var_name, _file in request.files.iteritems():
                    if var_name.startswith('variables.'):
                        _query['variables'][var_name[10:]] = _file[0]
                return _query

        return {}

    @jwt_authenticated
    def post(self):
        try:
            data = self.parse_body(self.request)
            pretty = self.pretty or self.request.arguments.get('pretty')
            # print data

            execution_results, all_params = graphql_server.run_http_query(
                self.schema,
                self.request.method.lower(),
                data,
                query_data=self.request.arguments,
                batch_enabled=False,
                catch=False,
                # Execute options
                return_promise=False,
                root_value=None,
                context_value={
                    'request': self.request.method,
                    'current_user': self.current_user
                },
                middleware=None,
                executor=None)
        except graphql_server.HttpQueryError as e:
            self.send_error(e.status_code, message=e.message)
            return

        result, status_code = graphql_server.encode_execution_results(
            execution_results,
            is_batch=isinstance(data, list),
            format_error=self.format_error,
            encode=partial(self.encode, pretty=pretty))

        self.set_status(status_code)
        self.write(result)
        self.set_header('Content-Type', 'application/json')


# noinspection PyAbstractClass
class LoginTokenHandler(tornado.web.RequestHandler):
    def check_xsrf_cookie(self):
        # The login page doesn't need to check for XSRF
        # It's secured with username and password
        pass

    def post(self):
        expires = datetime.datetime.utcnow() + datetime.timedelta(days=1)

        self.xsrf_token  # Make sure we set the cookie
        self.set_header("Content-Type", "application/json")
        login_args = {
            'username': ''
        }

        try:
            login_args = json.loads(self.request.body)
            with open(os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.PASSWD_FILENAME), 'r+') as fp:
                if not pass_lib.verify_user(fp, login_args.get('username', None), login_args.get('password', None)):
                    raise ValueError("Incorrect username or password")
            event_log.info("User '%s' logged in successfully" % login_args['username'], extra=EventType.ACCESS_CONTROL._dict)
        except ValueError:
            event_log.info("User '%s' attempted to login" % login_args['username'], extra=EventType.ACCESS_CONTROL._dict)
            self.write(json.dumps({
                "success": False,
            }))
            return

        self.write(json.dumps({
            "success": True,
            "expires": calendar.timegm(expires.utctimetuple()),
        }))

        self.set_cookie("access_token",
                        jwt_encode({
                            'user': login_args['username'],
                            'exp': expires
                        }, self.settings['jwt_secret']),
                        HttpOnly=True,
                        SameSite='strict')


# noinspection PyAbstractClass
class VerifyHandler(tornado.web.RequestHandler):
    # TODO Try to remove this
    # (What has to be added to the Javascript code so this is no longer needed?)
    def check_xsrf_cookie(self):
        pass

    @jwt_authenticated
    def post(self):
        encoded_cookie = self.get_cookie("access_token")
        decoded_cookie = jwt_decode(encoded_cookie, self.settings['jwt_secret'])
        username = decoded_cookie["user"]

        try:
            args = json.loads(self.request.body)
            with open(os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.PASSWD_FILENAME), 'r+') as fp:
                if not pass_lib.verify_user(fp, username, args.get('password', None)):
                    raise ValueError("Incorrect current password")
            #event_log.info("User '%s' entered their previous password successfully" % username, extra=EventType.ACCESS_CONTROL._dict)
        except ValueError:
            event_log.info("User '%s' entered their previous password incorrectly" % username, extra=EventType.ACCESS_CONTROL._dict)
            self.write(json.dumps({
                "success": False,
            }))
            return

        self.write(json.dumps({
            "success": True,
        }))


# noinspection PyAbstractClass
class ChangePasswordHandler(tornado.web.RequestHandler):
    # TODO Try to remove this
    # (What has to be added to the Javascript code so this is no longer needed?)
    def check_xsrf_cookie(self):
        pass

    @jwt_authenticated
    def post(self):
        encoded_cookie = self.get_cookie("access_token")
        decoded_cookie = jwt_decode(encoded_cookie, self.settings['jwt_secret'])
        username = decoded_cookie["user"]

        self.set_header("Content-Type", "application/json")

        try:
            args = json.loads(self.request.body)
            with open(os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.PASSWD_FILENAME), 'r+') as fp:
                #print "username="+username
                #print "new password="+str(args.get('password', None))
                pass_lib.add_user(fp, username, args.get('password', None))
            event_log.info("User '%s' changed password successfully" % username, extra=EventType.ACCESS_CONTROL._dict)
        except ValueError:
            event_log.info("User '%s' unable to change password" % username, extra=EventType.ACCESS_CONTROL._dict)
            self.write(json.dumps({
                "success": False,
            }))
            return

        self.write(json.dumps({
            "success": True,
        }))


# noinspection PyAbstractClass
class SignOutHandler(tornado.web.RequestHandler):
    def check_xsrf_cookie(self):
        # The sign out page doesn't need to check for XSRF
        # We are just clearing a cookie
        pass

    def post(self):
        self.clear_cookie('access_token')


# noinspection PyAbstractClass
class CommissioningManifestHandler(tornado.web.StaticFileHandler):
    def initialize(self, path, my_site, default_filename=None):
        self.root = path
        self.default_filename = default_filename

        my_site.generate_manifest()


# noinspection PyAbstractClass
class CompositeConfigHandler(tornado.web.StaticFileHandler):
    def initialize(self, path, my_site, default_filename=None):
        self.root = path
        self.default_filename = default_filename

        my_site.generate_composite_config()


# noinspection PyAbstractClass
class DefaultFileHandler(tornado.web.StaticFileHandler):
    def get_absolute_path(self, root, path):
        abspath = os.path.abspath(os.path.join(root, path))
        if os.path.exists(abspath):
            return abspath

        return os.path.abspath(os.path.join(root, self.default_filename))


# noinspection PyAbstractClass
class RedirectHttpHandler(tornado.web.RequestHandler):
    # noinspection PyMethodOverriding,PyAttributeOutsideInit
    def initialize(self, https_port):
        self.https_port = https_port

    def get(self):
        self.redirect("https://%s:%s%s" % (self.request.host.split(':')[0],
                                           self.https_port,
                                           self.request.path),
                      permanent=True)
        log.debug("Redirected request to HTTPS")


def make_redirect_app(https_port):
    return tornado.web.Application(
        [((r".*"), RedirectHttpHandler, {'https_port': https_port})]
    )


def make_app(schema, user_img_dir, my_site, ssl_options):
    static_path = os.path.join(os.path.dirname(__file__), "build")

    app = tornado.web.Application(
        [(r"/graphiql()", tornado.web.StaticFileHandler, {'path': os.path.join(os.path.dirname(__file__), "static", 'graphiql-workspace.html')}),
         (r"/graphql", GraphQLHandler, {'schema': schema}),
         (r"/token", LoginTokenHandler),
         (r"/verify", VerifyHandler),
         (r"/change", ChangePasswordHandler),
         (r"/signout", SignOutHandler),
         (r"/site-images", tornado.web.StaticFileHandler, {'path': user_img_dir}),
         (r"/commissioning\.manifest()", CommissioningManifestHandler, {'path': os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.MANIFEST_FILENAME),
                                                                        'my_site': my_site}),
         (r"/nc\.config()", CompositeConfigHandler, {'path': os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.COMPOSITE_NC_CONFIG_FILENAME),
                                                     'my_site': my_site}),
         (r"/(.*)", DefaultFileHandler, {"path": static_path,
                                         "default_filename": "index.html"}),
         ],
        **settings)

    server = tornado.httpserver.HTTPServer(app, ssl_options=ssl_options)

    log.debug("Webserver created")

    return server

if __name__ == '__main__':

    logging.basicConfig(level=logging.DEBUG)

    print "test_only"
    import tornado.wsgi, tornado.httpserver, tornado.ioloop

    class MainHandler(tornado.web.RequestHandler):
        def get(self):
            self.write("Hello,121 world")
    handlers1 = [ (r"/", MainHandler),  ]



    app1 = tornado.web.Application(handlers1)
    server = tornado.httpserver.HTTPServer(app1)
    server.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

