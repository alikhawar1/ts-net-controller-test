import logging
import os
import platform
import datetime
import cPickle
import monotonic
import copy

from utils.SystemSettings import SYSTEM_CONFIG_DIRECTORY
from utils.SystemSettings import MANIFEST_FILENAME, ENGINEERING_SPEC_FILENAME, COMPOSITE_NC_CONFIG_FILENAME

from ts_common import config_manifest
import ConfigManager

# 04/24/2019 - I think this code was originally intended to use TrackerPresets directly.
# Somewhere in the SLUI/GraphQL tie-in (or possibly due to the data pickling), the code
# ended up keeping (and PERSISTING) operational_mode as an integer instead of as an enum.
# Because of the persistence issue, we have to maintain this going forward (Search TrackerPresets)
from controller_enums import TrackerPresets

from persistent_state import PersistentState


log = logging.getLogger(__name__)

DEFAULT_CONFIG_FILENAME = 'site.pkl'

# I don't see any need for a fancier versioning scheme at this time
NC_SOFTWARE_VERSION = "0.7.4.2" 

class TerraSmartSite(object):
    """
    Describes a unique TerraSmart site
    """
    def __init__(self, site_img_dir, system_manager, persistent_state, config_file=None):
        """
        Instantiates a new site configuration instance
        :param str site_img_dir: The full absolute path to use for the site images directory
        :param str config_file: Optional. The full absolute path of the config file
        """
        config_file = config_file or os.path.join(SYSTEM_CONFIG_DIRECTORY, DEFAULT_CONFIG_FILENAME)

        # These defaults will get overwritten if we have a config file
        self.site_name = "Unknown Site Name"
        self.site_contact = "No contact"
        self.site_organization = "Unknown Org"
        self.gps_lat = 26.432691
        self.gps_lng = -81.799176
        self.gps_alt = 4.5
        self.enable_nightly_shutdown = True
        self.power_off = 30
        self.power_on = 30
        self.has_weather = False

        self.operational_mode = TrackerPresets.STOW.value
        self.tracking_enable = True
        self.backtracking_enable = True
        self.stow_avg_enable = False
        self.stow_avg_mph = 20
        self.stow_avg_duration = 5
        self.gust_enable = True
        self.gust_mph = 5
        self.resume_min = 30

        self.ntp = "time-c.nist.gov"
        self.mesh_channel = 9
        self.mesh_network_id = 0x7e2a
        self.mesh_key = None

        self.site_image = ''
        self.site_image_ul_lat = 26.433035
        self.site_image_ul_lng = -81.799833
        self.site_image_ll_lat = 26.432421
        self.site_image_ll_lng = -81.798836
        self.engineering_spec = None

        self.enable_low_power_shutdown = False
        self.cell_modem_warning_voltage = 12.5
        self.cell_modem_cutoff_voltage = 12.0
        self.cell_modem_cuton_voltage = 13.0  # only applies after cell modem cutoff!
        self.gateway_warning_voltage = 11.5
        self.gateway_cutoff_voltage = 11.0
        # We cannot turn ourselves back on...

        # Load config file
        if os.path.exists(config_file):
            with open(config_file, 'rb') as f:
                previous_config = cPickle.load(f)
                self.__dict__.update(previous_config.__dict__)

        # Overwrite these properties from the config file 
        self.site_img_dir = site_img_dir
        spec_dt = None
        if os.path.exists(os.path.join(SYSTEM_CONFIG_DIRECTORY, ENGINEERING_SPEC_FILENAME)):
            spec_dt = datetime.datetime.fromtimestamp(
                os.path.getmtime(os.path.join(SYSTEM_CONFIG_DIRECTORY, ENGINEERING_SPEC_FILENAME)))
        self.engineering_spec = spec_dt
        self.config_file = config_file

        self.software_version = NC_SOFTWARE_VERSION

        platform_info = platform.uname()  # returns a tuple of (mostly) ugly looking details
        self.gateway_platform = platform_info[0] + ' ' + platform_info[1]

        # Uncomment the following to see what it would be like with a real E12
        # self.gateway_os_version = '4.4.19-synapse2.0.0-3-gb4d9f13'
        self.gateway_os_version = platform_info[2]

        # Uncomment the following to see what it would be like with a real E12
        # self.gateway_os_build = '#1 SMP PREEMPT RT Mon Feb 6 17:50:31 UTC 2017'
        self.gateway_os_build = platform_info[3]

        # This helper object is used with config backup/restore
        self._config_manager = ConfigManager.ConfigManager()

        # This is used in computing uptime (see property below)
        self._startup_time = monotonic.monotonic()

        self._system_manager = system_manager

        self.persistent_state = persistent_state
        self.need_to_write_flash = False # various setters will update this...

        # Handle the transition from old code to new code
        if self.persistent_state.loaded_version == 0:
            # We loaded an old NVRAM image. Take what we had saved
            # out in FLASH and write it to the NVRAM
            self.persistent_state.set_mode(PersistentState.TRACKING_ENABLE_BIT, self.tracking_enable, save_now=False)
            self.persistent_state.set_mode(PersistentState.BACKTRACKING_ENABLE_BIT, self.backtracking_enable, save_now=False)
            self.persistent_state.set_preset(self.operational_mode, save_now=False)
            self.persistent_state._persist_state()
        else:
            # We have valid NVRAM data. Override what we got from FLASH
            self.tracking_enable = persistent_state.get_mode(PersistentState.TRACKING_ENABLE_BIT)
            self.backtracking_enable = persistent_state.get_mode(PersistentState.BACKTRACKING_ENABLE_BIT)
            self.operational_mode = persistent_state.get_preset()

    def __getstate__(self):
        state = copy.copy(self.__dict__)
        # SystemManager has a reference back to us. This caused a failure at
        # pickling time. He is also NOT "config data". So, easiest solution
        # is to exclude him from pickling.
        if '_system_manager' in state:
            del state['_system_manager']
        # May be worth filtering others here later...
        return state

    @property
    def uptime(self):
        return monotonic.monotonic() - self._startup_time

    def save(self):
        """
        Saves the current state to the config file
        :return: None
        """
        if self.need_to_write_flash:
            # main.py will have already created the SYSTEM_CONFIG_DIRECTORY if needed
            with open(self.config_file, 'wb') as f:
                cPickle.dump(self, f, protocol=cPickle.HIGHEST_PROTOCOL)
            self.need_to_write_flash = False # (we just did)

    def update(self, vals, save_now=True):
        """
        Updates the current state with the provided values
        :param dict vals: Values to use as the new state
        :return: None
        """

        # We used to keep ALL our config info in the FLASH file.
        # Now that we are keeping a few fields in RTC NVRAM, we
        # have to take some extra steps
        nvram_update_needed = False
        if 'tracking_enable' in vals:
            nvram_update_needed = True
            self.tracking_enable = vals.pop('tracking_enable')
            self.persistent_state.set_mode(PersistentState.TRACKING_ENABLE_BIT, self.tracking_enable, save_now=False)
        if 'backtracking_enable' in vals:
            nvram_update_needed = True
            self.backtracking_enable = vals.pop('backtracking_enable')
            self.persistent_state.set_mode(PersistentState.BACKTRACKING_ENABLE_BIT, self.backtracking_enable, save_now=False)
        if 'operational_mode' in vals:
            nvram_update_needed = True
            self.operational_mode = vals.pop('operational_mode')
            self.persistent_state.set_preset(self.operational_mode, save_now=False)
        if nvram_update_needed:
            self.persistent_state._persist_state()

        # Maybe we've dealt with everything already...
        if vals == {}:
            return

        # Anything else is data for the FLASH file
        self.__dict__.update(vals)
        self.need_to_write_flash = True
        if save_now:
            self.save()

    def replace_site_image(self, image_file, save_now=True):
        """
        Replaces the current site_image, if it exists, with a new one
        :param dict image_file: A dictionary containing the image file
        :return: None
        """
        if self.site_image and os.path.exists(os.path.join(self.site_img_dir, self.site_image)):
            os.remove(os.path.join(self.site_img_dir, self.site_image))

        with open(os.path.join(self.site_img_dir, image_file['filename']), 'wb') as f:
            f.write(image_file['body'])

        self.site_image = image_file['filename']
        self.need_to_write_flash = True
        if save_now:
            self.save()

    def replace_eng_spec_file(self, spec_file, save_now=True):
        """
        Replaces the current site_image, if it exists, with a new one
        :param dict image_file: A dictionary containing the image file
        :return: None
        """
        # TODO: These files will be encrypted and tied to the gateway's MAC address
        with open(os.path.join(SYSTEM_CONFIG_DIRECTORY, ENGINEERING_SPEC_FILENAME), 'wb') as f:
            f.write(spec_file['body'])

        self.engineering_spec = datetime.datetime.utcnow()
        self.need_to_write_flash = True
        self._config_manager.process_engineering_spec()
        if save_now:
            self.save()

    def generate_manifest(self):
        """
        Generates a manifest file and saves it to disk
        :return: None
        """
        cfg = config_manifest.ConfigManifest()
        cfg.set_id(self.site_name, self.site_name + ' Config')
        cfg.set_mesh(self.mesh_channel, self.mesh_network_id, self.mesh_key)
        cfg.set_loc(self.gps_lat, self.gps_lng)
        if self.site_image and os.path.exists(os.path.join(self.site_img_dir, self.site_image)):
            cfg.add_layer('Site Plan',
                          os.path.join(self.site_img_dir, self.site_image),
                          [[self.site_image_ul_lat, self.site_image_ul_lng],
                           [self.site_image_ll_lat, self.site_image_ll_lng]])
        cfg.pack(os.path.join(SYSTEM_CONFIG_DIRECTORY, MANIFEST_FILENAME))

    def generate_composite_config(self):
        """
        Generates a zip file containing all the individual config files and
        saves it to disk
        :return: None (but a zip file will have been created in SYSTEM_CONFIG_DIRECTORY)
        """
        self._config_manager.pack(SYSTEM_CONFIG_DIRECTORY, COMPOSITE_NC_CONFIG_FILENAME)

    def digest_composite_config(self, filename):
        """
        Gets the ConfigManager to process the received config file
        :return: True for success, False for failure (ex. bad zip file)
        """
        result = self._config_manager.unpack(SYSTEM_CONFIG_DIRECTORY, filename)

        if result:
            # TODO Should we go ahead and delete the file afterwards?
            # For now I am keeping it in case it is needed again.

            # For the new configuration to actually take effect, we need to restart
            # We are re-launched as-needed by external means, so all WE need to do
            # is schedule an exit. The delay is to give the return value time to make
            # it back up to the SLUI
            log.critical("Scheduling program exit so that uploaded configuration changes can take effect")

            self._system_manager.request_program_exit(3)

        return result
