"""
NCCBMonitor - Code to gather data from the Network Controller
Companion Board, over the USB CDC interface. This is the oddball
case - most of the data is gathered via SNAP radio / RPC.

Besides reporting how things are going with the PV panel,
battery, charger, and emergency stop button, the NCCB has
control of the Gateway and Cell Modem power, and can be
commanded to turn things on/off.

Note that code to make decisions regarding WHEN to do so
has been migrated to SystemManager.
"""

import logging

import serial

import Asset

import snappyImages.PacketSerial as PacketSerial  # repurposed SNAPpy
import snappyImages.HeartBeat as HeartBeat  # repurposed SNAPpy
import snappyImages.Responses as Responses  # repurposed SNAPpy
import snappyImages.BlobTracking as BlobTracking  # repurposed SNAPpy
import snappyImages.PKTCODEC as PKTCODEC  # repurposed SNAPpy
import snappyImages.TLVCODEC as TLVCODEC  # repurposed SNAPpy

import NetworkControllerDecoders  # new code based on the original SNAPpy


# I am linking these up manually to minimize changes to the original SNAPpy scripts
Responses.heartBeat = HeartBeat.heartBeat
Responses.extract_tlv = PKTCODEC.extract_tlv
BlobTracking.send_TLV_Packet = PacketSerial.send_TLV_Packet
BlobTracking.encode_tlv_str = TLVCODEC.encode_tlv_str
TLVCODEC.debug = NetworkControllerDecoders.debug

PORT = '/dev/ttyACM0'  # This is what appears on real E12

log = logging.getLogger(__name__)

DEVELOPMENT_MODE = False

class NCCBMonitor(object):
    """
    The "interface" in this case is the set of member variables
    that you can access directly (anything not _private), *PLUS*
    the following member functions:
        send_gateway_off(): pull the plug on ourself
        send_gateway_on(): somebody else might call this (we can't if we are OFF!)
        send_modem_off(): power down the cell modem
        send_modem_on(): power up the cell modem
        send_schedule_gateway_power(seconds_until_off,
                                    seconds_until_on): SCHEDULE power changes
        send_schedule_modem_power(seconds_until_off,
                                  seconds_until_on): SCHEDULE modem power changes
        send_schedule_power(seconds_until_gateway_off,
                            seconds_until_gateway_on,
                            seconds_until_modem_off,
                            seconds_until_modem_on): combo of the above two commands
    """

    def __init__(self, schedule_func, asset, port=PORT):
        """
        Constructor
        :param callable schedule_func: A function with the signature func(call_every_n_seconds, callback_func)
            to schedule a reoccurring callback
        :param object asset: An instance of an Asset class
        :param int port: the USB serial port to use
        """
        global DEVELOPMENT_MODE

        assert isinstance(asset, Asset.Asset)
        self.asset = asset
        if port:
            self._serial_port = serial.Serial(port, timeout=0)

            PacketSerial.sendPacket = self._send_packet

            schedule_func(1.0, self._every_second)
            schedule_func(0.1, self._every_100ms)
            schedule_func(0.001, self._every_1ms)
        else:
            DEVELOPMENT_MODE = True
            log.info("DEVELOPMENT MODE - NO NCCB")

    def _every_second(self):
        HeartBeat.checkHeartBeat()
        return True

    @staticmethod
    def _every_100ms():
        PacketSerial.receiveTick(100)
        BlobTracking.poll_for_blobs()
        return True

    def _every_1ms(self):
        incoming_data = self._serial_port.read(256)
        if incoming_data != '':
            PacketSerial.incomingDataHandler(self.asset, incoming_data)

        return True

    # Reminder - routines to BUILD packets are in PKTCODEC.py
    def _send_packet(self, packet):
        # Do packet book-keeping needed to support AssetUpdater
        self.asset.radio_polls_sent += 1
        self._serial_port.write(packet)

    def ensure_final_serial_transmits(self):
        if DEVELOPMENT_MODE:  # Developing on a PC with no NCCB attached...
            return
        self._serial_port.write_timeout = None  # we want blocking transmits now, since we about to exit


    @staticmethod
    def send_gateway_off():
        """Request immediate turn-off of Gateway power"""
        if DEVELOPMENT_MODE:  # Developing on a PC with no NCCB attached...
            return
        cmd = TLVCODEC.encode_tlv_str("gateway_off")
        PacketSerial.send_TLV_Packet(cmd)

    @staticmethod
    def send_gateway_on():
        """Request immediate turn-on of Gateway power"""
        if DEVELOPMENT_MODE:  # Developing on a PC with no NCCB attached...
            return
        cmd = TLVCODEC.encode_tlv_str("gateway_on")
        PacketSerial.send_TLV_Packet(cmd)

    @staticmethod
    def send_modem_off():
        """Request immediate turn-off of Cell Modem power"""
        if DEVELOPMENT_MODE:  # Developing on a PC with no NCCB attached...
            return
        cmd = TLVCODEC.encode_tlv_str("modem_off")
        PacketSerial.send_TLV_Packet(cmd)

    @staticmethod
    def send_modem_on():
        """Request immediate turn-on of Cell Modem power"""
        if DEVELOPMENT_MODE:  # Developing on a PC with no NCCB attached...
            return
        cmd = TLVCODEC.encode_tlv_str("modem_on")
        PacketSerial.send_TLV_Packet(cmd)

    @staticmethod
    def send_schedule_gateway_power(seconds_until_off,
                                    seconds_until_on):
        """Request FUTURE control of Gateway power"""
        if DEVELOPMENT_MODE:  # Developing on a PC with no NCCB attached...
            return
        cmd = TLVCODEC.encode_tlv_str("schedule_gateway_power")
        cmd += TLVCODEC.encode_tlv_int(int(seconds_until_off))
        cmd += TLVCODEC.encode_tlv_int(int(seconds_until_on))
        PacketSerial.send_TLV_Packet(cmd)

    @staticmethod
    def send_schedule_modem_power(seconds_until_off,
                                  seconds_until_on):
        """Request FUTURE control of Cell Modem power"""
        if DEVELOPMENT_MODE:  # Developing on a PC with no NCCB attached...
            return
        cmd = TLVCODEC.encode_tlv_str("schedule_modem_power")
        cmd += TLVCODEC.encode_tlv_int(int(seconds_until_off))
        cmd += TLVCODEC.encode_tlv_int(int(seconds_until_on))
        PacketSerial.send_TLV_Packet(cmd)

    @staticmethod
    def send_schedule_power(seconds_until_gateway_off,
                            seconds_until_gateway_on,
                            seconds_until_modem_off,
                            seconds_until_modem_on):
        """Request FUTURE control of Gateway and Cell Modem power"""
        if DEVELOPMENT_MODE:  # Developing on a PC with no NCCB attached...
            return
        cmd = TLVCODEC.encode_tlv_str("schedule_power")
        cmd += TLVCODEC.encode_tlv_int(int(seconds_until_gateway_off))
        cmd += TLVCODEC.encode_tlv_int(int(seconds_until_gateway_on))
        cmd += TLVCODEC.encode_tlv_int(int(seconds_until_modem_off))
        cmd += TLVCODEC.encode_tlv_int(int(seconds_until_modem_on))
        PacketSerial.send_TLV_Packet(cmd)

    @staticmethod
    def send_mcast_rpc(group, ttl, funcname, *args):
        """Generic handler to pass a rpc over the USB serial interface"""

        if DEVELOPMENT_MODE:  # Developing on a PC with no NCCB attached...
            return

        # Note that this function adapts mcast_rpc calls into non-multicast calls
        # For example, group and ttl are ignored
        cmd = TLVCODEC.encode_tlv_str(funcname)
        for arg in args:
            arg_type = type(arg)
            if arg_type == type(False):
                cmd += TLVCODEC.encode_tlv_bool(arg)
            elif arg_type == type(0):
                cmd += TLVCODEC.encode_tlv_int(arg)
            elif arg_type == type(""):
                cmd += TLVCODEC.encode_tlv_str(arg)
        PacketSerial.send_TLV_Packet(cmd)


if __name__ == '__main__':
    from tornado.ioloop import IOLoop
    from Scheduler import Scheduler

    # logging.basicConfig(level=logging.WARNING)
    # logging.basicConfig(level=logging.INFO)
    logging.basicConfig(level=logging.DEBUG)

    nccb_monitor = NCCBMonitor(schedule_func=Scheduler.schedule_func,
                               asset=Asset.Asset('serial'))

    IOLoop.current().start()
