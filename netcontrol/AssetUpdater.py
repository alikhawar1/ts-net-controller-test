"""
AssetUpdater.py - The original job of this module was to ensure each SNAP Module
was running the correct SNAPpy script for it's Asset Type (TX, WX, etc.)
This has since expanded to incorporate FIRMWARE and CONFIG update too:
    * STM32 Firmware on each TC/WX board (future work will expand to include NCCB)
    * Config Settings on each TC/WX board (from engineering.spec)
    * SNAP Firmware on each Radio Module
    * SNAPpy script on each Radio Module
"""

import logging
import os
import binascii
import ConfigParser
import csv
import time
import struct

from utils.PickleHelper import PickleHelper
from FormatHelpers import *

from pyfiria.snap import dmcast_uploader

from cStringIO import StringIO
from intelhex import IntelHex

from PauseButtons import PauseButtonManager, PauseButton
from CommunicationsPriorities import *
from utils import SystemSettings
from controller_enums import EventType

log = logging.getLogger(__name__)
ASSET_UPDATER_LOG_LEVEL = logging.INFO

# Upload Types - these became finer-grained when we added individual enable/disable of them
NO_UPLOAD_IN_PROGRESS = -1 # extra (sentinel) value

BRIDGE_SPY_FILE_UPLOAD = 0
ASSET_SPY_FILE_UPLOAD = 1
BRIDGE_RADIO_FIRMWARE_UPLOAD = 2
ASSET_RADIO_FIRMWARE_UPLOAD = 3
TC_WX_FIRMWARE_UPLOAD = 4
NCCB_FIRMWARE_UPLOAD = 5 # as of 03/31/2020, this is still unimplemented
TC_CONFIG_UPLOAD = 6

# See above
FIRST_UPLOAD_TYPE = 0
NUMBER_OF_UPLOAD_TYPES = 7

UPLOAD_TYPE_TEXT = [
    "Bridge Script",
    "Asset Script",
    "Bridge SNAP Firmware",
    "Asset SNAP Firmware",
    "Asset STM32 Firmware",
    "NCCB STM32 Firmware",
    "Asset Configuration",
]

# Upload triggers - added when we started changing the rules based on what initiated the download
MANUALLY_TRIGGERED = 0
AUTOMATICALLY_TRIGGERED = 1

SPY_FILE_EXT = '.spy'
SFI_FILE_EXT = '.sfi'
HEX_FILE_EXT = '.hex'
BLT_FILE_EXT = '.blt'

ASSET_UPDATER_CONFIG_FILE = 'asset_updater.pkl'

SITE_CONFIG_FILENAME = 'settings.conf' # in ConfigParser format
ROWS_CSV_FILENAME = 'rows.csv' # in comma Separated Value format

SIMULATED_NODE_FIRMWARE_VERSION = '2.8.1'
SIMULATED_NODE_FIRMWARE_NAME = 'RF200P81_AES128_SnapV2.8.1'
SIMULATED_NODE_SCRIPT_NAME = 'TrackerControllerSimulator'
SIMULATED_NODE_SCRIPT_CRC = 0x10DA
SIMULATED_NODES = [
    "\x5F\xCC\x98",  # Row 1 (7 nodes)
    "\x5F\xCC\xBB",
    "\x5F\xCB\x95",
    "\x5F\xCE\xE1",
    "\x5F\xCF\x4D",
    "\x5F\xCC\xF6",
    "\x5F\xCD\x52",
    "\x5F\xCD\x40",  # Row 2 (8 nodes)
    "\x5F\xCF\x00",
    "\x5F\xCD\x57",
    "\x5F\xCD\x00",
    "\x5F\xCE\x31",
    "\x5F\xCD\xC4",
    "\x5F\xCD\xC1",
    "\x5F\xCD\xB9",
    "\x5F\xCF\xA2",  # Row 3 (8 nodes)
    "\x5F\xCB\x65",
    "\x5F\xCE\x8E",
    "\x5F\xCE\x1E",
    "\x5F\xCD\x0A",
    "\x5F\xCC\xA0",
    "\x5F\xCD\xC9",
    "\x5F\xD1\x33",
    "\x5F\xCF\xF1",  # Row 4 (8 nodes)
    "\x5F\xCE\x60",
    "\x5F\xCE\xD8",
    "\x5F\xCC\x17",
    "\x5F\xCD\x63",
    "\x5F\xCB\xA6",
    "\x5F\xCE\xB2",
    "\x5F\xCF\xC8",
    "\x5F\xCC\x46",  # Row 5 (8 nodes)
    "\x5F\xCC\xA4",
    "\x5F\xCB\x33",
    "\x5F\xD0\x69",
    "\x5F\xCE\x4A",
    "\x5F\xCD\x9B",
    "\x5F\xCE\x03",
    "\x5F\xCE\x63",
    "\x5F\xCB\x3C",  # Row 6 (8 nodes)
    "\x5F\xCD\x32",
    "\x5F\xCE\xC0",
    "\x5F\xD1\x78",
    "\x5F\xD1\x3F",
    "\x5F\xD0\xCE",
    "\x5F\xCC\xA7",
    "\x5F\xCE\x4D",
    "\x5F\xCE\x86",  # Row 7 (8 nodes)
    "\x5F\xCF\xDA",
    "\x5F\xCD\xFC",
    "\x5F\xCC\x0C",
    "\x5F\xCD\x5B",
    "\x5F\xD0\x52",
    "\x5F\xCC\x09",
    "\x5F\xCD\x70",
    "\x5F\xCB\xB7",  # Row 8 (8 nodes)
    "\x5F\xCC\x39",
    "\x5F\xCF\xA7",
    "\x5F\xCD\x5A",
    "\x5F\xD0\xEC",
    "\x5F\xD1\x2A",
    "\x5F\xD1\x02",
    "\x5F\xD0\x44",
]

# Some default values (these are now overridable via the UI)
# These are indexed by "UPLOAD_TYPE"

# The values of 10 are the original default values
# The value of 4 for the STM32 firmware updates was determined from some PA Test site experiments
# The value of 1 for CONFIG uploads is because these have always had to be handled individually
DEFAULT_BATCH_SIZES = [10, 10, 10, 10, 4, 10, 1]

# The values of 3 are the original default values
# The value of 8 for the STM32 firmware upload came out of testing at Rio Arriba
DEFAULT_UPLOAD_RETRIES = [3, 3, 3, 3, 8, 3, 3]

# The values of 10 are just a starting guess
# 50 for STM32 upload postponement is at least based on field testing
# The same goes for 25 for upload postponement on config uploads
DEFAULT_UPLOAD_POLLS_REQUIRED = [10, 10, 10, 10, 50, 10, 25]

#
# This variant of the original DefaultUploadAdapter burns the first Dmcast
# timeslot to work around a bug in SNAP 2.8.1. This is an experiment to see
# if doing so will increase script upload robustness. If it does not help, then
# this code should be removed and the original DefaultUploadAdapter used instead.
#
class ExperimentalUploadAdapter(dmcast_uploader.DefaultUploadAdapter):
    """ This class provides the standard routines for a SNAPpy-style upload"""
    def send_erase(self, sconn, nodes, group, ttl, timeslot):
        """Send whatever is needed to begin the upload process and erase any prior image"""
        sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot, 'eraseImage')
        # The above automatically triggers a tellVmStat() response.
        # You might have to do one or more extra steps in your variant.

    def send_write_chunk(self, sconn, nodes, group, ttl, timeslot, offset, chunk):
        """Send a single CHUNK of the new image"""
        sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot, 'writeChunk', offset, chunk)
        # The above automatically triggers a tellVmStat() response.
        # You might have to do one or more extra steps in your variant.

    def send_reboot(self, sconn, nodes, group, ttl, timeslot):
        """Send whatever is needed to complete the upload process and run the new image"""
        sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot, 'reboot')

    def send_ping(self, sconn, nodes, group, ttl, timeslot):
        """Send whatever is needed to confirm the new image is running"""
        sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot, 'vmStat', 4, 40)  # read CRC

    def calc_dm_chunk_size(self, nominal_chunk_size, num_nodes):
        """Accommodate for dmcast address space in packet"""
        return nominal_chunk_size - 3 * (num_nodes + 1)


#
# This is a variant of the DefaultUploadAdapter. It generates the specific RPC calls
# needed to update the STM32 firmware THROUGH a SNAP Engine. Refer to SNAPpy script
# DmcastUploaderAdapter.py for the other side of this interface.
# 03/12/2019 update - this class has also been modified to skip the first address slot.
# (See ExperimentalUploadAdapter, above)
#
class STM32UploadAdapter(object):
    """ This class provides the standard routines for a SNAPpy-style upload"""
    def send_erase(self, sconn, nodes, group, ttl, timeslot):
        """Send whatever is needed to begin the upload process and erase any prior image"""
        group = 0x8000 # override group to be "data" group instead of broadcast group
        sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot, 'erase_image')

    def send_write_chunk(self, sconn, nodes, group, ttl, timeslot, offset, chunk):
        """Send a single CHUNK of the new image"""
        group = 0x8000 # override group to be "data" group instead of broadcast group
        # Handle more than 64K of image
        offset_high = (offset >> 16) & 0xFFFF
        offset_low = offset & 0xFFFF
        return sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot, 'write_chunk', offset_high, offset_low, chunk)

    def send_reboot(self, sconn, nodes, group, ttl, timeslot):
        """Send whatever is needed to complete the upload process and run the new image"""
        group = 0x8000 # override group to be "data" group instead of broadcast group
        sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot, 'reboot_STM32_afterwards')

    def send_ping(self, sconn, nodes, group, ttl, timeslot):
        """Send whatever is needed to confirm the new image is running"""
        pass  # Not used with firmware images, only for script uploads...

    def extract_image(self, filename):
        """Load image from hex file"""
        try:
            with open(filename, 'rb') as raw_file:
                core_hex = raw_file.read()
                core_stream = StringIO(core_hex)
                core_reader = IntelHex(core_stream)

                # Core end address will be <= the max. Send only the used range.
                core_end_addr = 0x08000000  # min for STM32F103RB
                segs = core_reader.segments()
                for i in xrange(len(segs)):
                    seg_end_addr = segs[i][1] - 1  # end of segment (inclusive)
                    if seg_end_addr > core_end_addr:
                        core_end_addr = seg_end_addr

                # STM32 image format (inclusive ranges)
                core_image = core_reader.tobinstr(0x08000000, core_end_addr)

            return core_image

        except:
            raise Exception("Error in HEX file")
            return None

    def calc_dm_chunk_size(self, nominal_chunk_size, num_nodes):
        """Accommodate for dmcast address space in packet"""
        # For this platform, it must also be a multiple of 4 bytes
        return (nominal_chunk_size - 3 * (num_nodes + 1)) & 0xfffc


#
# By the time we got around to adding "Config download", several types of SOFTWARE
# download had already been implemented. Because of this, a "config download"
# has been made to look and work similar to a software download.
#
# This is a variant of the DefaultUploadAdapter. It generates the multiple RPC calls
# needed to update the *Configuration* of a Row Box. Refer to SNAPpy script
# DmcastUploaderAdapter.py for the other side of this interface.
#

# Here are the fields of each row of the CSV file
CSV_SNAP_ADDRESS_INDEX = 0
CSV_CONFIG_LABEL_INDEX = 1
CSV_BLT_FILENAME_INDEX = 2 # BLT is Backtracking Lookup Table
# The "config angles"...
# Note that index here refers to their position in the rows.csv file
# In the actual "config angles" table, these are indices 0, 1, and 2
CSV_HORIZONTAL_OFFSET_ANGLE_INDEX = 3
CSV_MINIMUM_OFFSET_ANGLE_INDEX = 4
CSV_MAXIMUM_OFFSET_ANGLE_INDEX = 5
# PRESETS come from a separate file (instead of adding 32 (!) columns to this file)
CSV_PRESETS_FILENAME_INDEX = 6
CSV_INSTALL_FLAGS_INDEX = 7
CSV_WIND_DIR_OFFSET_INDEX = 8
CSV_SNOW_SENSOR_HEIGHT_INDEX = 9
CSV_SITE_LABEL_INDEX = 10
CSV_LATITUDE_INDEX = 11
CSV_LONGITUDE_INDEX = 12
CSV_LOCATION_INDEX = 13
CSV_BAP_FILENAME_INDEX = 14 # BAP is Backtracking Algorithm Parameters
# More will be added over time, and MUST go at the END of this list!
CSV_COLUMNS_COUNT = 15 # <- keep this up to date!


BLT_ANGLES = 181
BLT_CHUNK_SIZE = 32 # how many angles in each chunk (last one is a partial of 21)
NUM_BLT_CHUNKS = int(BLT_ANGLES / BLT_CHUNK_SIZE) + 1

NUM_CONFIG_ANGLES = 3

NUM_PRESETS = 16

NUM_CONFIG_NUMBERS = 3

NUM_CONFIG_STRINGS = 3 # really 5, but 2 are handled separately at the end


# These are the different "sections" of a config download...
FIRST_BLT_OFFSET = 0
FIRST_CONFIG_ANGLE_OFFSET = FIRST_BLT_OFFSET + NUM_BLT_CHUNKS
FIRST_PRESET_OFFSET = FIRST_CONFIG_ANGLE_OFFSET + NUM_CONFIG_ANGLES
FIRST_CONFIG_NUMBER_OFFSET = FIRST_PRESET_OFFSET + NUM_PRESETS
FIRST_CONFIG_STRING_OFFSET = FIRST_CONFIG_NUMBER_OFFSET + NUM_CONFIG_NUMBERS
# Keep this last one at the end (because it is optional)
FIRST_BAP_OFFSET = FIRST_CONFIG_STRING_OFFSET + NUM_CONFIG_STRINGS

class RowBoxConfigUploadAdapter(object):
    """ This class provides what LOOK like the standard routines for a
        SNAPpy-style script upload, but what they really do is set the
        CONFIGURATION of a Row Box"""
    def __init__(self, config_dir):
        self._config_dir = config_dir
        self.row_data = [] # caller fills this in before triggering us
        self.blt_chunks = [] # later gets filled in fom BLT file
        self.config_angles = [0,0,0] # gets overwritten from the rows.csv file
        self.presets = [] # later gets filled in from a separate CSV file
        self.config_numbers = [0,0,0] # gets overwritten from the rows.csv file
        self.config_strings = ['','',''] # gets overwritten from the rows.csv file

    def convert_angle(self, string_angle_in_float_degrees):
        temp = string_angle_in_float_degrees
        temp = float(temp)
        temp *= 10.0 # from degrees to tenths of a degree (ex 90 degrees maps to 900)
        # Round to the nearest tenth of a degree
        if (temp > 0.0):
            temp += 0.5
        else:
            temp -= 0.5
        # integer-ize
        temp = int(temp)
        return temp

    def convert_distance(self, string_distance_in_float_meters):
        temp = string_distance_in_float_meters
        temp = float(temp)
        temp *= 1000.0 # from meters to millimeters
        # Round to the nearest millimeter
        if (temp > 0.0):
            temp += 0.5
        else:
            temp -= 0.5
        # integer-ize
        temp = int(temp)
        return temp

    def convert_lat_lon(self, string_latitude, string_longitude):
        latitude = float(string_latitude)
        longitude = float(string_longitude)
        lat_lon = struct.pack('>dd', latitude, longitude)
        return lat_lon

    def send_erase(self, sconn, nodes, group, ttl, timeslot):
        """Send whatever is needed to begin the upload process and erase any prior configuration"""
        # Notice that in this case we are faking the operation
        sconn.dmcast_rpc(''.join(nodes), group, ttl, timeslot, 'fake_erase')

    def send_write_chunk(self, sconn, nodes, group, ttl, timeslot, offset, chunk):
        """Send a single CHUNK of the new config. Offset helps us keep our place"""

        if (offset >= FIRST_BLT_OFFSET) and (offset < FIRST_CONFIG_ANGLE_OFFSET):
            # Send down a chunk of the Peak Yield Backtracking Lookup Table
            sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot, 'swt_ltc', 0, offset, self.blt_chunks[offset])
        elif (offset >= FIRST_CONFIG_ANGLE_OFFSET) and (offset < FIRST_PRESET_OFFSET):
            config_angle_index = offset - FIRST_CONFIG_ANGLE_OFFSET
            # Send down one of the config angles
            if config_angle_index == 0: # this requires special handling
                # What we are doing is using a special index of -1 to bypass
                # the normal "calibration offset calculation" that a Row Box does.
                sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot,
                                 'swt_config_angle', 0, -1, self.config_angles[0], offset)
            else:
                sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot,
                                 'swt_config_angle', 0, config_angle_index, self.config_angles[config_angle_index], offset)
        elif (offset >= FIRST_PRESET_OFFSET) and (offset < FIRST_CONFIG_NUMBER_OFFSET):
            # Send down one of the presets
            preset_index = offset - FIRST_PRESET_OFFSET
            angle = self.presets[preset_index][0]
            nearest = self.presets[preset_index][1]
            sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot,
                             'swt_preset', 0, preset_index, angle, nearest, offset)
        elif (offset >= FIRST_CONFIG_NUMBER_OFFSET) and (offset < FIRST_CONFIG_STRING_OFFSET):
            config_number_index = offset - FIRST_CONFIG_NUMBER_OFFSET
            # Send down one of the config numbers
            sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot,
                             'swt_config_number', 0, config_number_index, self.config_numbers[config_number_index], offset)
        elif (offset >= FIRST_CONFIG_STRING_OFFSET) and (offset < FIRST_BAP_OFFSET):
            config_string_index = offset - FIRST_CONFIG_STRING_OFFSET
            # Send down one of the config strings. Note that the indexes are different in the unit
            unit_index = config_string_index + 2 # This is due to special handling of first 2 strings
            sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot,
                             'swt_config_string', 0, unit_index, self.config_strings[config_string_index], offset)
        else:
            # Send down a set of backtracking parameters
            bap_index = offset - FIRST_BAP_OFFSET
            sconn.dmcast_rpc('\x00\x00\x00' + ''.join(nodes), group, ttl, timeslot,
                             'swt_backtrack2', 0,
                             self.baps[bap_index][0], self.baps[bap_index][1], self.baps[bap_index][2],
                             self.baps[bap_index][3], self.baps[bap_index][4], self.baps[bap_index][5], offset)


    def send_reboot(self, sconn, nodes, group, ttl, timeslot):
        """Send whatever is needed to complete the config process"""
        now = time.time()
        # We are making a timestamp for use by Javascript, which wants milliseconds
        now *= 1000.0
        now_str = struct.pack('>d', now)

        sconn.dmcast_rpc(''.join(nodes), group, ttl, timeslot, 'save_config', 0, self.row_data[CSV_CONFIG_LABEL_INDEX], now_str)

    def send_ping(self, sconn, nodes, group, ttl, timeslot):
        """Send whatever is needed to confirm the new image is running"""
        pass  # Not used with config images, only for script uploads...

    def extract_image(self, filename):
        """Fake loading image from a single file"""

        # What we are doing here is making a FAKE binary image whose SIZE
        # in bytes indicates the number of discrete messages to be sent
        config_steps = 0
        # We actually need to load data from MULTIPLE files, and we store in pieces

        # We have a CSV file with data for each row and row box but
        # that data has already been preloaded by the caller
        # Here we are doing some pre-processing

        # Config Angles
        try:
            for index in xrange(0, NUM_CONFIG_ANGLES):
                self.config_angles[index] = self.convert_angle(self.row_data[CSV_HORIZONTAL_OFFSET_ANGLE_INDEX + index])
        except:
            raise Exception("Error in CSV file")
            return None
        config_steps += NUM_CONFIG_ANGLES # horizontal offset, min angle, max angle

        # Config Numbers
        try:
            self.config_numbers[0] = int(self.row_data[CSV_INSTALL_FLAGS_INDEX], 16)
            self.config_numbers[1] = self.convert_angle(self.row_data[CSV_WIND_DIR_OFFSET_INDEX])
            self.config_numbers[2] = self.convert_distance(self.row_data[CSV_SNOW_SENSOR_HEIGHT_INDEX])
        except:
            raise Exception("Error in CSV file")
            return None
        config_steps += NUM_CONFIG_NUMBERS # Install Flags, Wind Dir Offset, and Snow Sensor Height

        # Config Strings
        try:
            self.config_strings[0] = self.row_data[CSV_SITE_LABEL_INDEX]
            self.config_strings[1] = self.convert_lat_lon(self.row_data[CSV_LATITUDE_INDEX], self.row_data[CSV_LONGITUDE_INDEX])
            self.config_strings[2] = self.row_data[CSV_LOCATION_INDEX]
        except:
            raise Exception("Error in CSV file")
            return None
        config_steps += NUM_CONFIG_STRINGS # site label, lat/lon, location

        # We have a Backtracking Lookup Table (BLT) file
        # Convert that into binary chunks for transfer to the row box
        try:
            filename = self.row_data[CSV_BLT_FILENAME_INDEX]
            filename = os.path.join(self._config_dir, filename)
            with open(filename, 'r') as blt_file:
                entries = blt_file.readlines()
                if len(entries) < BLT_ANGLES:
                    raise Exception("Error in BLT file - incorrect number of entries")
                    return None
                entry_index = 0
                chunk_index = 0
                count = BLT_ANGLES
                self.blt_chunks = []
                while count > 0:
                    if entry_index % BLT_CHUNK_SIZE == 0:
                        self.blt_chunks.append('')
                    temp = entries[entry_index]
                    temp = self.convert_angle(temp)
                    # string-ize
                    msb = (temp >> 8) & 0xFF
                    lsb = temp & 0xFF
                    self.blt_chunks[chunk_index] += chr(msb)
                    self.blt_chunks[chunk_index] += chr(lsb)
                    entry_index += 1
                    if entry_index % BLT_CHUNK_SIZE == 0:
                        chunk_index += 1
                    count -= 1

        except:
            raise Exception("Error in BLT file")
            return None
        config_steps += NUM_BLT_CHUNKS # chunks of Backtracking Lookup Table

        # We have a separate PRESETS file (also CSV)
        # Convert that into Python data for transfer to the row box
        try:
            filename = self.row_data[CSV_PRESETS_FILENAME_INDEX]
            filename = os.path.join(self._config_dir, filename)
            with open(filename, 'rb') as csv_file:
                csv_reader = csv.reader(csv_file)
                self.presets = [(0, False)] * NUM_PRESETS
                for row_data in csv_reader:
                    if len(row_data) >= 3:
                        preset_index = int(row_data[0])
                        if (preset_index < NUM_PRESETS):
                            preset_angle = self.convert_angle(row_data[1])
                            preset_nearest = False
                            if row_data[2] in ["YES", "Yes", "yes", "Y", "y"]:
                                preset_nearest = True
                            self.presets[preset_index] = (preset_angle, preset_nearest)
        except:
            raise Exception("Error in PRESETS file")
            return None
        config_steps += NUM_PRESETS

        # More configuration is going to be added over time
        # config_steps += xxx

        # Because this next chunk of config data is OPTIONAL, keep it at the end
        # We MAY have an extra CSV file of parameters for the ORIGINAL Backtracking
        # algorithm.
        try:
            filename = self.row_data[CSV_BAP_FILENAME_INDEX]
            filename = os.path.join(self._config_dir, filename)
            with open(filename, 'rb') as csv_file:
                csv_reader = csv.reader(csv_file)
                self.baps = []
                for row_data in csv_reader:
                    if len(row_data) >= 6: # index, width, spacing_east, delta_height_east, spacing_west, delta_height_west
                        converted_row_data = []
                        for index in xrange(0, 6):
                            converted_row_data.append(int(row_data[index]))
                        self.baps.append(converted_row_data)
                        config_steps += 1

        except:
            # Because the system is eventually supposed to be migrating from the
            # original style of (algorithmic) backtracking to a new (lookup table)
            # based approach, we are considering this particular chunk of config
            # info to be optional
            pass

        return '#' * config_steps

    def calc_dm_chunk_size(self, nominal_chunk_size, num_nodes):
        """Accommodate for dmcast address space in packet"""
        # For this type of upload, we are not sending "real" chunks
        # Instead, the "file offset" is being used to track our
        # progress through a whole ASSORTMENT of config messages
        return 1


#
# Here we are subclassing an existing library class so we can customize it.
# The original customization was simply adding more/earlier Event Logging.
# The second customization added was the ability to abort early (but
# only AFTER completing the current chunk of nodes).
#
class MyDmcastBlockUploader(dmcast_uploader.DmcastBlockUploader):
    """We wanted a different reporting paradigm - log more events along
       the way instead of waiting until the end of the entire process.
       We later added the ability to abort the uploads (after the current block)
    """
    def __init__(self, snapconnect_inst, handler, dm_timeslot=50, retries=3, timeout_unit=100, adapter=None,
                 attribute_owner=None, attribute_name=''):
        super(MyDmcastBlockUploader, self).__init__(snapconnect_inst, handler, dm_timeslot, retries, timeout_unit, adapter)
        # Info for new summary report
        self.total_requested = 0
        self.total_attempted = 0 # can be higher than requested due to retries
        self.total_passed = 0
        self.total_failed = 0
        self.summary_report_needed = False
        self.attribute_owner = attribute_owner
        self.attribute_name = attribute_name
        self.trigger_type = AUTOMATICALLY_TRIGGERED # default to a legal value

    def report_current_block(self):
        first = (self.cur_block * self.block_size) + 1
        last = first + len(self.blocks[self.cur_block]) - 1
        temp_tuple = (first, last, self.total_node_count)
        log_summary_event("Uploading assets %d-%d of %d" % temp_tuple, EventType.FIRMWARE_UPDATE._dict)
        for address in self.blocks[self.cur_block]:
            log_event(address, "Starting upload to asset %s", EventType.FIRMWARE_UPDATE._dict)

    # Support for new summary report
    def reset_totals(self):
        self.total_attempted = 0
        self.total_passed = 0
        self.total_failed = 0
        self.summary_report_needed = False

    def init_requested(self):
        self.total_requested = 1

    def add_requested(self):
        self.total_requested += 1

    def remove_requested(self):
        self.total_requested -= 1

    def start(self, spyfile, nodelist, block_size=10, trigger_type=AUTOMATICALLY_TRIGGERED):
        super(MyDmcastBlockUploader, self).start(spyfile, nodelist, block_size)
        self.trigger_type = trigger_type
        self.report_current_block()

    def upload_complete(self, nodes_succeeded):
        attempted = set()
        attempted.update(self.blocks[self.cur_block])
        successes = set()
        successes.update(nodes_succeeded)
        failures = attempted.difference(successes)

        for address in successes:
            log_event(address, "Upload completed to asset %s", EventType.FIRMWARE_UPDATE._dict)
        for address in failures:
            log_event(address, "Upload failed to asset %s", EventType.FIRMWARE_UPDATE._dict)

        # Info for new summary report
        self.total_attempted += len(attempted)

        if self.trigger_type == AUTOMATICALLY_TRIGGERED:
            # Check for early abort, due to users disabling a category WHILE we were DOING that category...
            if self.cur_block < len(self.blocks) - 1: # Only check for abort if we are not already finishing up...
                if getattr(self.attribute_owner, self.attribute_name, True) == False:
                    units_so_far = (self.cur_block + 1) * self.block_size
                    temp_tuple = (units_so_far, self.total_node_count)
                    log_summary_event("User aborted uploads with only %d of %d assets processed" % temp_tuple, EventType.FIRMWARE_UPDATE._dict)
                    # "Trim" the collection of nodes to upload so that upload_complete() will wrap things up early...
                    self.blocks = self.blocks[0:self.cur_block+1]
                    self.total_node_count = units_so_far

        super(MyDmcastBlockUploader, self).upload_complete(nodes_succeeded)

        first = ((self.cur_block-1) * self.block_size) + 1
        last = first + len(self.blocks[self.cur_block-1]) - 1
        passed = len(successes)
        failed = len(failures)
        self.total_passed += passed
        self.total_failed += failed
        if self.cur_block < len(self.blocks):
            units_so_far = self.cur_block * self.block_size
            temp_tuple = (first, last, self.total_node_count, passed, failed, len(self.nodes_succeeded), units_so_far - len(self.nodes_succeeded))
            log_summary_event("%d-%d of %d assets processed, %d Pass / %d Fail, %d Pass / %d Fail total" % temp_tuple, EventType.FIRMWARE_UPDATE._dict)
            self.report_current_block()
        else:
            temp_tuple = (first, last, self.total_node_count, passed, failed, len(self.nodes_succeeded), self.total_node_count - len(self.nodes_succeeded))
            log_summary_event("%d-%d of %d assets processed, %d Pass / %d Fail, %d Pass / %d Fail Total" % temp_tuple, EventType.FIRMWARE_UPDATE._dict)

        if self.summary_report_needed:
            temp_tuple = (self.total_passed, self.total_requested, self.total_attempted, self.total_failed)
            log_summary_event("Summary: %d of %d assets uploaded, using %d attempts with %d failures" % temp_tuple, EventType.FIRMWARE_UPDATE._dict)
            self.summary_report_needed = False


class AssetUpdaterPersistableConfig(object):
    """
    The point of this class is to separate out the PERSISTED CONFIG
    fields from the remaining LIVE DATA fields. This allows us to
    only persist (pickle) those fields. See AssetUpdater.
    """
    def __init__(self):
        """Initializing these fields implicitly defines the "shape" of the object"""
        self.do_upload_scripts = None # Deprecated, keeping only for config file migration purposes
        self.configured_bridge_script_name = None
        self.configured_bridge_script_crc = None
        self.configured_bridge_radio_firmware_version = None
        self.configured_bridge_radio_firmware_filename = None
        # (no STM32 firmware for the Bridge...)

        self.configured_asset_script_name = None
        self.configured_asset_script_crc = None
        self.configured_asset_radio_firmware_version = None
        self.configured_asset_radio_firmware_filename = None
        self.configured_asset_stm32_firmware_version = None
        self.configured_asset_stm32_firmware_filename = None

        # These were added when adding support of DYNAMIC enable/disable of uploads
        self.do_upload_bridge_script = None
        self.do_upload_bridge_firmware = None
        self.do_upload_asset_scripts = None
        self.do_upload_asset_radio_firmware = None
        self.do_upload_asset_stm32_firmware = None
        self.do_upload_asset_configs = None

        # Another expansion as we made the upload process more and more configurable
        self.upload_batch_sizes = None
        self.upload_retries = None
        self.upload_polls_required = None

    def default_config(self):
        self.do_upload_scripts = True
        # REMINDER - These are just DEFAULTS! Used during initial development...
        self.configured_bridge_script_name = 'NetworkControllerBridge'
        self.configured_bridge_script_crc = 0x67BE
        self.configured_bridge_radio_firmware_version = '2.8.1'
        self.configured_bridge_radio_firmware_filename = 'RF220SU_AES128_SnapV2.8.1'

        self.configured_asset_script_name = 'TrackerControllerRadio'
        self.configured_asset_script_crc = 0x9B35
        self.configured_asset_radio_firmware_version = '2.8.1'
        self.configured_asset_radio_firmware_filename = 'RF220SU_AES128_SnapV2.8.1'
        self.configured_asset_stm32_firmware_version = '0.0'
        self.configured_asset_stm32_firmware_filename = 'TrackerControllerSTM32V0.0'

        # These were added when adding support of DYNAMIC enable/disable of uploads
        self.do_upload_bridge_script = True
        self.do_upload_bridge_firmware = True
        self.do_upload_asset_scripts = True
        self.do_upload_asset_radio_firmware = True
        self.do_upload_asset_stm32_firmware = True
        self.do_upload_asset_configs = True

        # Another expansion as we made the upload process more and more configurable
        self.upload_batch_sizes = DEFAULT_BATCH_SIZES
        self.upload_retries = DEFAULT_UPLOAD_RETRIES
        self.upload_polls_required = DEFAULT_UPLOAD_POLLS_REQUIRED

# Made into a subroutine to consolidate all formatting decisions
# Extracted from the AssetUpdater class when we needed to call it from multiple objects
def log_event(address, message, type):
    readable_snap_address = binascii.hexlify(address)
    logging.getLogger('EventLog').log(ASSET_UPDATER_LOG_LEVEL, message % (readable_snap_address,), extra=type)

def log_summary_event(message, type):
    logging.getLogger('EventLog').log(ASSET_UPDATER_LOG_LEVEL, message, extra=type)


class AssetUpdater(AssetUpdaterPersistableConfig, dmcast_uploader.DmcastUploadHandler):
    """
    Keeps SNAPpy scripts up to date (uploads them to radio modules as-needed
    """
    CONFIG_CHECK_INTERVAL = 30  # second

    def __init__(self, snap_connect, schedule_func, bridge_monitor, asset_mgr, image_dir, config_dir):
        """
        Constructor. Parameters include:
        snap_connect - the object we use to do SNAP communications
        schedule_func - used for timing
        """
        AssetUpdaterPersistableConfig.__init__(self)

        self._pickle_helper = PickleHelper(self, AssetUpdaterPersistableConfig, ASSET_UPDATER_CONFIG_FILE)
        if snap_connect is None:
            raise Exception("Error: AssetUpdater requires a SNAP Connect!")
        else:
            self._snap_connect = snap_connect

        if schedule_func is None:
            raise Exception("Error: AssetUpdater requires a schedule_func!")
        else:
            self._schedule_func = schedule_func

        if bridge_monitor is None:
            raise Exception("Error: AssetUpdater requires access to BridgeMonitor data!")
        else:
            self._bridge_monitor = bridge_monitor

        if asset_mgr is None:
            raise Exception("Error: AssetUpdater requires access to AssetManager data!")
        else:
            self._asset_mgr = asset_mgr

        self._image_dir = image_dir

        self._config_dir = config_dir

        if not self.load_config():
            self.default_config()
            self.save_config()

        # Manual Data Migration - one config setting was replaced with 6 finer-grained ones
        # This requires extra fixup steps IN CASE we load a config from an earlier version of the code
        save_needed = False
        new_attributes = ['do_upload_bridge_script',
                          'do_upload_bridge_firmware',
                          'do_upload_asset_scripts',
                          'do_upload_asset_radio_firmware',
                          'do_upload_asset_stm32_firmware',
                          'do_upload_asset_configs']
        for attribute in new_attributes:
            if getattr(self, attribute) is None:
                setattr(self, attribute, self.do_upload_scripts)
                save_needed = True

        # We later added some more, and there is no existing field to key off of...
        if getattr(self, 'upload_batch_sizes') is None:
            setattr(self, 'upload_batch_sizes', DEFAULT_BATCH_SIZES)
            save_needed = True

        if getattr(self, 'upload_retries') is None:
            setattr(self, 'upload_retries', DEFAULT_UPLOAD_RETRIES)
            save_needed = True

        if getattr(self, 'upload_polls_required') is None:
            setattr(self, 'upload_polls_required', DEFAULT_UPLOAD_POLLS_REQUIRED)
            save_needed = True

        if save_needed:
            self.save_config()

        self._upload_type = NO_UPLOAD_IN_PROGRESS

        # the original data structure, used for automatic uploads
        self._nodes_awaiting_upload = set()

        # we later added the ability to manually force batches of uploads too
        self._forced_updates = []
        self._uploader = []
        for index in xrange(FIRST_UPLOAD_TYPE, NUMBER_OF_UPLOAD_TYPES):
            self._forced_updates.append(set())
            self._uploader.append(None) # just sizing the list here, see below where we fill it in for real...

        # This one handles SPY file Updates to the Bridge Module
        self._uploader[BRIDGE_SPY_FILE_UPLOAD] = MyDmcastBlockUploader(snapconnect_inst=self._snap_connect,
                                                                       handler=self,
                                                                       timeout_unit=500,
                                                                       adapter=ExperimentalUploadAdapter(),
                                                                       attribute_owner = self,
                                                                       attribute_name = 'do_upload_bridge_script')

        # This one handles Firmware Updates to the Bridge Module
        self._uploader[BRIDGE_RADIO_FIRMWARE_UPLOAD] = MyDmcastBlockUploader(snapconnect_inst=self._snap_connect,
                                                                             handler=self,
                                                                             timeout_unit=500,
                                                                             adapter=ExperimentalUploadAdapter(),
                                                                             attribute_owner = self,
                                                                             attribute_name = 'do_upload_bridge_firmware')

        # This one handles SPY file Updates to the Radio Modules
        self._uploader[ASSET_SPY_FILE_UPLOAD] = MyDmcastBlockUploader(snapconnect_inst=self._snap_connect,
                                                                      handler=self,
                                                                      timeout_unit=500,
                                                                      adapter=ExperimentalUploadAdapter(),
                                                                      attribute_owner = self,
                                                                      attribute_name = 'do_upload_asset_scripts')

        # This one handles Firmware Updates to the Radio Modules
        self._uploader[ASSET_RADIO_FIRMWARE_UPLOAD] = MyDmcastBlockUploader(snapconnect_inst=self._snap_connect,
                                                                            handler=self,
                                                                            timeout_unit=500,
                                                                            adapter=ExperimentalUploadAdapter(),
                                                                            attribute_owner = self,
                                                                            attribute_name = 'do_upload_asset_radio_firmware')

        # This one handles STM32 Firmware Updates to Assets
        self._uploader[TC_WX_FIRMWARE_UPLOAD] = MyDmcastBlockUploader(snapconnect_inst=self._snap_connect,
                                                                      handler=self,
                                                                      timeout_unit=100,
                                                                      adapter=STM32UploadAdapter(),
                                                                      attribute_owner = self,
                                                                      attribute_name = 'do_upload_asset_stm32_firmware')
        # Because this adapter uses messages with more overhead, we also must reduce the max chunk size
        # The STM32 Boot Loader documentation also says it requires data to be in multiples of 4-bytes
        self._uploader[TC_WX_FIRMWARE_UPLOAD].set_chunk_size(76)  # 76 is max possible with current messaging

        # This one handles Configuration Update to the Row Boxes (engineering.spec support)
        self._uploader[TC_CONFIG_UPLOAD] = MyDmcastBlockUploader(snapconnect_inst=self._snap_connect,
                                                                 handler=self,
                                                                 timeout_unit=500,
                                                                 adapter=RowBoxConfigUploadAdapter(self._config_dir),
                                                                 attribute_owner = self,
                                                                 attribute_name = 'do_upload_asset_configs')

        # _init_firmware_update_check() launches the REAL update checks after a time delay
        self.pcb = self._schedule_func(300.0, self._init_firmware_update_check)

        pause_button_manager = PauseButtonManager.get_default_instance()
        self._pause_button = pause_button_manager.get_pause_button(ASSET_UPDATER_PRIORITY)

    def load_config(self):
        """Try to load config from disk (previously generated pickle file"""
        return self._pickle_helper.load_config()

    def save_config(self):
        """save config out to disk (pickle file)"""
        return self._pickle_helper.save_config()

    def update_config(self, new_config_dict, save_now=True):
        """update config based on user input (via SLUI)"""
        for key in new_config_dict:
            if hasattr(self, key):
                temp_tuple = (key, str(getattr(self, key)), str(new_config_dict[key]))
                setattr(self, key, new_config_dict[key])
                log_summary_event("Update setting %s changed was %s now %s" % temp_tuple, EventType.FIRMWARE_UPDATE._dict)

        if save_now:
            self.save_config()

    def fixup_script_name(self, filename):
        if filename.endswith(SPY_FILE_EXT):
            filename = filename[:-(len(SPY_FILE_EXT))]
        return filename

    def fixup_snap_name(self, filename):
        if filename.endswith(SFI_FILE_EXT):
            filename = filename[:-(len(SFI_FILE_EXT))]
        return filename

    def fixup_stm32_name(self, filename):
        if filename.endswith(HEX_FILE_EXT):
            filename = filename[:-(len(HEX_FILE_EXT))]
        return filename

    def determine_crc(self, script_image_dir, spyfile):
        spyfile = os.path.join(script_image_dir, spyfile)
        # We are leveraging the capabilities of an existing class...
        # (We are not going to actually upload the file)
        uploader = dmcast_uploader.DmcastUploader(None, None)
        uploader.extract_spy_image(spyfile)
        if uploader.spy_image is not None:
            return uploader.spy_crc
        else:
            raise # re-report the exception previously raised by the uploader

    def replace_bridge_script(self, bridge_script_file, save_now=True):
        """
        Replaces the current bridge script file, if it exists, with a new one
        :param dict bridge_script_file: A dictionary containing the new file AND it's filename
        :return: None
        """
        script_image_dir = os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.SOFTWARE_IMAGES_SUBDIRECTORY)
        script_filename = bridge_script_file['filename']
        script_filename = script_filename.encode('ascii', 'ignore')

        if not script_filename.endswith(SPY_FILE_EXT):
            log_summary_event("Requested Bridge Script file %s not a SPY file, ignoring" % script_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        if script_filename.startswith("TrackerControllerRadio"):
            log_summary_event("Requested Bridge Script file %s appears to be intended for assets instead, ignoring" % script_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        if not script_filename.startswith("NetworkControllerBridge"):
            log_summary_event("Requested Bridge Script file %s does not appear to be for the bridge radio, ignoring" % script_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        # We have to save the file so that we can check it
        # This doesn't mean we will INSTALL it
        with open(os.path.join(script_image_dir, script_filename), 'wb') as f:
            f.write(bridge_script_file['body'])

        try:
            self.configured_bridge_script_crc = self.determine_crc(script_image_dir, script_filename)
        except:
            log_summary_event("Requested Bridge Script file %s failed CRC test, ignoring" % script_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        log_summary_event("Requested Bridge Script file %s has been uploaded to the Network Controller" % script_filename, EventType.FIRMWARE_UPDATE._dict)
        script_filename = self.fixup_script_name(script_filename)
        self.configured_bridge_script_name = script_filename

        if save_now:
            self.save()


    def replace_bridge_firmware(self, bridge_firmware_file, save_now=True):
        """
        Replaces the current bridge firmware file, if it exists, with a new one
        :param dict bridge_firmware_file: A dictionary containing the new file AND it's filename
        :return: None
        """
        firmware_image_dir = os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.SOFTWARE_IMAGES_SUBDIRECTORY)
        firmware_filename = bridge_firmware_file['filename']
        firmware_filename = firmware_filename.encode('ascii', 'ignore')

        if not firmware_filename.endswith(SFI_FILE_EXT):
            log_summary_event("Requested Bridge Firmware file %s not a SFI file, ignoring" % firmware_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        if not firmware_filename.startswith("RF220SU_AES128_Snap"):
            log_summary_event("Requested Bridge Firmware file %s does not appear to be for the bridge radio, ignoring" % firmware_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        with open(os.path.join(firmware_image_dir, firmware_filename), 'wb') as f:
            f.write(bridge_firmware_file['body'])

        log_summary_event("Requested Bridge Firmware file %s has been uploaded to the Network Controller" % firmware_filename, EventType.FIRMWARE_UPDATE._dict)
        firmware_filename = self.fixup_snap_name(firmware_filename)
        self.configured_bridge_radio_firmware_filename = firmware_filename

        self.configured_bridge_radio_firmware_version = firmware_filename[20:]

        if save_now:
            self.save()


    def replace_asset_script(self, asset_script_file, save_now=True):
        """
        Replaces the current asset script file, if it exists, with a new one
        :param dict asset_script_file: A dictionary containing the new file AND it's filename
        :return: None
        """
        script_image_dir = os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.SOFTWARE_IMAGES_SUBDIRECTORY)
        script_filename = asset_script_file['filename']
        script_filename = script_filename.encode('ascii', 'ignore')

        if not script_filename.endswith(SPY_FILE_EXT):
            log_summary_event("Requested Asset Script file %s not a SPY file, ignoring" % script_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        if script_filename.startswith("NetworkControllerBridge"):
            log_summary_event("Requested Asset Script file %s appears to be intended for the bridge instead, ignoring" % script_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        if not script_filename.startswith("TrackerControllerRadio"):
            log_summary_event("Requested Asset Script file %s does not appear to be for the asset radios, ignoring" % script_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        # We have to save the file so that we can check it
        # This doesn't mean we will INSTALL it
        with open(os.path.join(script_image_dir, script_filename), 'wb') as f:
            f.write(asset_script_file['body'])

        try:
            self.configured_asset_script_crc = self.determine_crc(script_image_dir, script_filename)
        except:
            log_summary_event("Requested Asset Script file %s failed CRC test, ignoring" % script_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        log_summary_event("Requested Asset Script file %s has been uploaded to the Network Controller" % script_filename, EventType.FIRMWARE_UPDATE._dict)
        script_filename = self.fixup_script_name(script_filename)
        self.configured_asset_script_name = script_filename

        if save_now:
            self.save()


    def replace_asset_radio_firmware(self, asset_radio_firmware_file, save_now=True):
        """
        Replaces the current asset radio firmware file, if it exists, with a new one
        :param dict asset_radio_firmware_file: A dictionary containing the new file AND it's filename
        :return: None
        """
        firmware_image_dir = os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.SOFTWARE_IMAGES_SUBDIRECTORY)
        firmware_filename = asset_radio_firmware_file['filename']
        firmware_filename = firmware_filename.encode('ascii', 'ignore')

        if not firmware_filename.endswith(SFI_FILE_EXT):
            log_summary_event("Requested Asset SNAP Firmware file %s not a SFI file, ignoring" % firmware_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        if not firmware_filename.startswith("RF220SU_AES128_Snap"):
            log_summary_event("Requested Asset SNAP Firmware file %s does not appear to be for the asset radio, ignoring" % firmware_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        with open(os.path.join(firmware_image_dir, firmware_filename), 'wb') as f:
            f.write(asset_radio_firmware_file['body'])

        log_summary_event("Requested Asset SNAP Firmware file %s has been uploaded to the Network Controller" % firmware_filename, EventType.FIRMWARE_UPDATE._dict)
        firmware_filename = self.fixup_snap_name(firmware_filename)
        self.configured_asset_radio_firmware_filename = firmware_filename

        self.configured_asset_radio_firmware_version = firmware_filename[20:]

        if save_now:
            self.save()


    def replace_asset_stm32_firmware(self, asset_stm32_firmware_file, save_now=True):
        """
        Replaces the current asset STM32 firmware file, if it exists, with a new one
        :param dict asset_stm32_firmware_file: A dictionary containing the new file AND it's filename
        :return: None
        """
        firmware_image_dir = os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.SOFTWARE_IMAGES_SUBDIRECTORY)
        firmware_filename = asset_stm32_firmware_file['filename']
        firmware_filename = firmware_filename.encode('ascii', 'ignore')

        if not firmware_filename.endswith(HEX_FILE_EXT):
            log_summary_event("Requested Asset STM32 Firmware file %s not a HEX file, ignoring" % firmware_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        if not firmware_filename.startswith("TrackerControllerSTM32"):
            log_summary_event("Requested Asset STM32 Firmware file %s does not appear to be for the asset, ignoring" % firmware_filename, EventType.FIRMWARE_UPDATE._dict)
            return

        with open(os.path.join(firmware_image_dir, firmware_filename), 'wb') as f:
            f.write(asset_stm32_firmware_file['body'])

        log_summary_event("Requested Asset STM32 Firmware file %s has been uploaded to the Network Controller" % firmware_filename, EventType.FIRMWARE_UPDATE._dict)
        firmware_filename = self.fixup_stm32_name(firmware_filename)
        self.configured_asset_stm32_firmware_filename = firmware_filename

        self.configured_asset_stm32_firmware_version = firmware_filename[23:]

        if save_now:
            self.save()


    # This is our GraphQL tie-in point. SLUI user did XXX, this got called.
    # Also expect to hit this from the cloud UI at some point.
    def force_upload(self, asset_ids, upload_type):
        if (upload_type >= FIRST_UPLOAD_TYPE) and (upload_type < NUMBER_OF_UPLOAD_TYPES):
            upload_type_str = UPLOAD_TYPE_TEXT[upload_type]
        else:
            upload_type_str = "Unknown"

        if upload_type in [BRIDGE_SPY_FILE_UPLOAD, BRIDGE_RADIO_FIRMWARE_UPLOAD]:
            address = self._bridge_monitor.raw_bridge_address
            # Using existing button as a TOGGLE for now
            if address not in self._forced_updates[upload_type]:
                self._forced_updates[upload_type].add(address)
                log_event(address, upload_type_str + " Upload manually requested for bridge %s", EventType.FIRMWARE_UPDATE._dict)
                self._uploader[upload_type].init_requested()
            else:
                self._forced_updates[upload_type].remove(address)
                log_event(address, upload_type_str + " Upload request removed for bridge %s", EventType.FIRMWARE_UPDATE._dict)
                self._uploader[upload_type].remove_requested()

        elif upload_type in [ASSET_SPY_FILE_UPLOAD, ASSET_RADIO_FIRMWARE_UPLOAD,
                             TC_WX_FIRMWARE_UPLOAD, TC_CONFIG_UPLOAD]:
            empty_before = (len(self._forced_updates[upload_type]) == 0)
            for asset_id in asset_ids:
                empty_now = (len(self._forced_updates[upload_type]) == 0)
                # Treating NCCB as a special case
                if asset_id == 'serial':
                    log_summary_event(upload_type_str + " Upload to NCCB is currently not supported", EventType.FIRMWARE_UPDATE._dict)
                else:
                    address = binascii.unhexlify(asset_id)
                    # Using existing button as a TOGGLE for now
                    if address not in self._forced_updates[upload_type]:
                        self._forced_updates[upload_type].add(address)
                        log_event(address, upload_type_str + " Upload manually requested for asset %s", EventType.FIRMWARE_UPDATE._dict)
                        if empty_now:
                            self._uploader[upload_type].init_requested()
                        else:
                            self._uploader[upload_type].add_requested()
                    else:
                        self._forced_updates[upload_type].remove(address)
                        log_event(address, upload_type_str + " Upload request removed for asset %s", EventType.FIRMWARE_UPDATE._dict)
                        self._uploader[upload_type].remove_requested()
            empty_after = (len(self._forced_updates[upload_type]) == 0)
            if empty_before and not empty_after:
                self._uploader[upload_type].reset_totals()

        elif upload_type in [NCCB_FIRMWARE_UPLOAD]:
            log_summary_event(upload_type_str + " Upload to NCCB is currently not supported", EventType.FIRMWARE_UPDATE._dict)
        else:
            log_summary_event(upload_type_str + " upload type %d requested" % (upload_type,), EventType.FIRMWARE_UPDATE._dict)

        return asset_ids

    # Adhere to the interface of a DmcastUploadHandler
    def handle_upload_complete(self, t_elapsed, nodes_succeeded):
        """Called on completion of upload"""
        # Let base class log results to console
        super(AssetUpdater, self).handle_upload_complete(t_elapsed, nodes_succeeded)
        for address in nodes_succeeded:
            if address in self._nodes_awaiting_upload:
                #log_event(address, "Upload completed to asset %s", EventType.FIRMWARE_UPDATE._dict)
                self._nodes_awaiting_upload.remove(address)
                # Avoid back-to-back duplicate uploads due to lag in gathering data
                if address == self._bridge_monitor.raw_bridge_address:
                    self._bridge_monitor.regather_bridge_script_info()
                else:
                    readable_snap_address = binascii.hexlify(address)
                    asset = self._asset_mgr.get_asset(readable_snap_address)
                    if asset is not None:
                        # Brute force - clearing out ALL fields that relate to uploads,
                        # rather than try and figure out exactly WHAT just got uploaded.
                        # (Remember, the code already handles None values...)
                        asset.radio_script_crc = None
                        asset.radio_firmware_string = None
                        asset.firmware_rev_string = None
                        # Ensure we don't do this again too soon (need time to gather data from asset post-upgrade)
                        asset.radio_polls_sent_at_upload_time = asset.radio_polls_sent

                if (self._upload_type >= FIRST_UPLOAD_TYPE) and (self._upload_type < NUMBER_OF_UPLOAD_TYPES):
                    # NOW we can take it off the forced_uploads list
                    if address in self._forced_updates[self._upload_type]:
                        self._forced_updates[self._upload_type].remove(address)
                        if (len(self._forced_updates[self._upload_type]) == 0):
                            self._uploader[self._upload_type].summary_report_needed = True

        # Instead of trying to do something specific with the failing nodes,
        # I am going to let them be handled by the next "check" cycle
        self._nodes_awaiting_upload.clear()
        self._upload_type == NO_UPLOAD_IN_PROGRESS
    
    def _init_firmware_update_check(self):
        """ Purpose of this callback is to add some delay before start checking for resources' update  """
        self.pcb.stop()
        self._schedule_func(AssetUpdater.CONFIG_CHECK_INTERVAL, self.check_bridge_script)

    def check_bridge_script(self):
        """Runs periodically and checks to see if a bridge script upload is NEEDED"""
        # We support the notion of pausing some of the communications when critical
        # activities (like moving the entire network) are in progress
        if self._pause_button.paused:
            return

        # Don't bother checking upload triggers if we already have uploads in progress
        if len(self._nodes_awaiting_upload) != 0:
            return

        # On-demand uploads take precedence over the enable/disable feature
        if (len(self._forced_updates[BRIDGE_SPY_FILE_UPLOAD]) > 0):
            self._nodes_awaiting_upload.update(self._forced_updates[BRIDGE_SPY_FILE_UPLOAD])
            self.start_uploads(self.configured_bridge_script_name,
                               list(self._nodes_awaiting_upload),
                               BRIDGE_SPY_FILE_UPLOAD,
                               MANUALLY_TRIGGERED)
            return

        # This feature can be disabled on-the-fly via the Cloud or the SLUI
        if not self.do_upload_bridge_script:
            self.check_bridge_firmware()
            return

        # Enforce the "upload hold-off until sufficient polls have been made"
        asset = self._asset_mgr.get_serial_asset()
        polls_required = self.upload_polls_required[BRIDGE_SPY_FILE_UPLOAD]
        if (polls_required > 0):
            if ((asset.radio_polls_sent - asset.radio_polls_sent_at_upload_time) < polls_required):
                self.check_bridge_firmware()
                return

        # Has BridgeMonitor gathered the necessary data yet? If not, move on to next test
        current_script_name = self._bridge_monitor.bridge_script_name
        current_script_crc = self._bridge_monitor.bridge_script_crc
        if (current_script_name) is None or (current_script_crc is None):
            self.check_bridge_firmware()
            return

        # We know what the bridge HAS/what the bridge is running.
        # Is it CORRECT?
        if (current_script_name == self.configured_bridge_script_name) and (current_script_crc == self.configured_bridge_script_crc):
            # Since the Bridge SCRIPT looks good, go check on the Bridge FIRMWARE...
            self.check_bridge_firmware()
            return

        # It's not correct... get our helper object to take a shot at correcting the situation
        else:
            address = self._bridge_monitor.raw_bridge_address
            self._nodes_awaiting_upload.add(address)

            log_event(address, "Bridge Radio Script upload needed for asset %s", EventType.FIRMWARE_UPDATE._dict)

            self.start_uploads(self.configured_bridge_script_name, [address], BRIDGE_SPY_FILE_UPLOAD)

    def check_bridge_firmware(self):
        """Runs periodically and checks to see if a bridge FIRMWARE upload is NEEDED"""
        # Have we been configured with WHAT firmware image to enforce?
        # If not, skip ahead to next check
        if self.configured_bridge_radio_firmware_version is None:
            self.check_asset_scripts()
            return

        # On-demand uploads take precedence over the enable/disable feature
        if (len(self._forced_updates[BRIDGE_RADIO_FIRMWARE_UPLOAD]) > 0):
            self._nodes_awaiting_upload.update(self._forced_updates[BRIDGE_RADIO_FIRMWARE_UPLOAD])
            self.start_uploads(self.configured_bridge_radio_firmware_filename,
                               list(self._nodes_awaiting_upload),
                               BRIDGE_RADIO_FIRMWARE_UPLOAD,
                               MANUALLY_TRIGGERED)
            return

        # This feature can be disabled on-the-fly via the Cloud or the SLUI
        if not self.do_upload_bridge_firmware:
            self.check_asset_scripts()
            return

        # Enforce the "upload hold-off until sufficient polls have been made"
        asset = self._asset_mgr.get_serial_asset()
        polls_required = self.upload_polls_required[BRIDGE_RADIO_FIRMWARE_UPLOAD]
        if (polls_required > 0):
            if ((asset.radio_polls_sent - asset.radio_polls_sent_at_upload_time) < polls_required):
                self.check_asset_scripts()
                return

        # Has BridgeMonitor gathered the necessary data yet? If not, skip ahead to next check
        current_bridge_version = self._bridge_monitor.bridge_version
        if current_bridge_version is None:
            self.check_asset_scripts()
            return

        # We know what the bridge HAS/what the bridge is running.
        # Is it CORRECT?
        if (self.configured_bridge_radio_firmware_version in current_bridge_version):
            # Since the Bridge FIRMWARE looks good, go check on all the REMOTE assets...
            self.check_asset_scripts()
            return

        # It's not correct... get our helper object to take a shot at correcting the situation
        address = self._bridge_monitor.raw_bridge_address
        self._nodes_awaiting_upload.add(address)

        log_event(address, "Bridge Radio Firmware upload needed for asset %s", EventType.FIRMWARE_UPDATE._dict)

        self.start_uploads(self.configured_bridge_radio_firmware_filename, [address], BRIDGE_RADIO_FIRMWARE_UPLOAD)

    def check_asset_scripts(self):
        """Runs periodically and checks to see if an asset script upload is NEEDED"""

        # Ensure we are configured sufficiently to be ABLE to do Asset Script uploads
        if self.configured_asset_script_name is None:
            self.check_asset_radio_firmware()
            return

        # On-demand uploads take precedence over the enable/disable feature
        if (len(self._forced_updates[ASSET_SPY_FILE_UPLOAD]) > 0):
            self._nodes_awaiting_upload.update(self._forced_updates[ASSET_SPY_FILE_UPLOAD])
            self.start_uploads(self.configured_asset_script_name,
                               list(self._nodes_awaiting_upload),
                               ASSET_SPY_FILE_UPLOAD,
                               MANUALLY_TRIGGERED)
            return

        # This feature can be disabled on-the-fly via the Cloud or the SLUI
        if not self.do_upload_asset_scripts:
            self.check_asset_radio_firmware()
            return

        simulated_nodes_awaiting_upload = set()  # used in conjunction with a "black list" at top of file...
        temp_ids = self._asset_mgr.assets.copy()
        for id in temp_ids:
            asset = self._asset_mgr.get_asset(id)
            if asset.radio_script_crc is not None:
                # Make sure we have had time to gather the data before we try to act on it...
                polls_required = self.upload_polls_required[ASSET_SPY_FILE_UPLOAD]
                if (polls_required > 0):
                    if ((asset.radio_polls_sent - asset.radio_polls_sent_at_upload_time) < polls_required):
                        continue

                current_crc = force_16_bit_unsigned(asset.radio_script_crc)
                # Treat simulated nodes special, but still support auto-uploading them
                if asset.snap_address in SIMULATED_NODES:
                    if current_crc != SIMULATED_NODE_SCRIPT_CRC:
                        simulated_nodes_awaiting_upload.add(asset.snap_address)
                        log_event(asset.snap_address, "Asset Radio Script upload needed for simulated asset %s", EventType.FIRMWARE_UPDATE._dict)
                else:
                    if current_crc != self.configured_asset_script_crc:
                        self._nodes_awaiting_upload.add(asset.snap_address)
                        log_event(asset.snap_address, "Asset Radio Script upload needed for asset %s", EventType.FIRMWARE_UPDATE._dict)

        # We are prioritizing REAL nodes over SIMULATED nodes
        # (They cannot be done in parallel because they get different SPY files)
        if len(self._nodes_awaiting_upload) != 0:
            self.start_uploads(self.configured_asset_script_name, list(self._nodes_awaiting_upload), ASSET_SPY_FILE_UPLOAD)
        elif len(simulated_nodes_awaiting_upload) != 0:
            self._nodes_awaiting_upload = simulated_nodes_awaiting_upload
            self.start_uploads(SIMULATED_NODE_SCRIPT_NAME, list(self._nodes_awaiting_upload), ASSET_SPY_FILE_UPLOAD)
        else:
            self.check_asset_radio_firmware()

    def check_asset_radio_firmware(self):
        """Runs periodically and checks to see if an asset RADIO FIRMWARE upload is NEEDED"""

        # Ensure we are configured sufficiently to be ABLE to do Asset SNAP firmware uploads
        if self.configured_asset_radio_firmware_version is None:
            self.check_asset_stm32_firmware()
            return
        if self.configured_asset_radio_firmware_filename is None:
            self.check_asset_stm32_firmware()
            return

        # On-demand uploads take precedence over the enable/disable feature
        if (len(self._forced_updates[ASSET_RADIO_FIRMWARE_UPLOAD]) > 0):
            self._nodes_awaiting_upload.update(self._forced_updates[ASSET_RADIO_FIRMWARE_UPLOAD])
            self.start_uploads(self.configured_asset_radio_firmware_filename,
                               list(self._nodes_awaiting_upload),
                               ASSET_RADIO_FIRMWARE_UPLOAD,
                               MANUALLY_TRIGGERED)
            return

        # This feature can be disabled on-the-fly via the Cloud or the SLUI
        if not self.do_upload_asset_radio_firmware:
            self.check_asset_stm32_firmware()
            return

        simulated_nodes_awaiting_upload = set()  # used in conjunction with a "black list" at top of file...
        temp_ids = self._asset_mgr.assets.copy()
        for id in temp_ids:
            asset = self._asset_mgr.get_asset(id)

            # Make sure we have had time to gather the data before we try to act on it...
            polls_required = self.upload_polls_required[ASSET_RADIO_FIRMWARE_UPLOAD]
            if (polls_required > 0):
                if ((asset.radio_polls_sent - asset.radio_polls_sent_at_upload_time) < polls_required):
                    continue

            if asset.radio_firmware_string is not None:
                # Treat simulated nodes special, but still support auto-uploading them
                if asset.snap_address in SIMULATED_NODES:
                    if asset.radio_firmware_string != SIMULATED_NODE_FIRMWARE_VERSION:
                        simulated_nodes_awaiting_upload.add(asset.snap_address)
                        log_event(asset.snap_address, "Asset Radio Firmware upload needed for simulated asset %s", EventType.FIRMWARE_UPDATE._dict)
                else:
                    if asset.radio_firmware_string != self.configured_asset_radio_firmware_version:
                        self._nodes_awaiting_upload.add(asset.snap_address)
                        log_event(asset.snap_address, "Asset Radio Firmware upload needed for asset %s", EventType.FIRMWARE_UPDATE._dict)

        if len(self._nodes_awaiting_upload) != 0:
            self.start_uploads(self.configured_asset_radio_firmware_filename, list(self._nodes_awaiting_upload), ASSET_RADIO_FIRMWARE_UPLOAD)
        elif len(simulated_nodes_awaiting_upload) != 0:
            self._nodes_awaiting_upload = simulated_nodes_awaiting_upload
            self.start_uploads(SIMULATED_NODE_FIRMWARE_NAME, list(self._nodes_awaiting_upload), ASSET_RADIO_FIRMWARE_UPLOAD)
        else:
            self.check_asset_stm32_firmware()

    def check_asset_stm32_firmware(self):
        """Runs periodically and checks to see if an asset STM32 FIRMWARE upload is NEEDED"""
        # Ensure we are configured sufficiently to be ABLE to do STM32 firmware uploads
        if self.configured_asset_stm32_firmware_version is None:
            self.check_row_box_configs()
            return
        if self.configured_asset_stm32_firmware_filename is None:
            self.check_row_box_configs()
            return

        # On-demand uploads take precedence over the enable/disable feature
        if (len(self._forced_updates[TC_WX_FIRMWARE_UPLOAD]) > 0):
            self._nodes_awaiting_upload.update(self._forced_updates[TC_WX_FIRMWARE_UPLOAD])
            self.start_uploads(self.configured_asset_stm32_firmware_filename,
                               list(self._nodes_awaiting_upload),
                               TC_WX_FIRMWARE_UPLOAD,
                               MANUALLY_TRIGGERED)
            return

        # This feature can be disabled on-the-fly via the Cloud or the SLUI
        if not self.do_upload_asset_stm32_firmware:
            self.check_row_box_configs()
            return

        temp_ids = self._asset_mgr.assets.copy()
        for id in temp_ids:
            asset = self._asset_mgr.get_asset(id)

            # Make sure we have had time to gather the data before we try to act on it...
            polls_required = self.upload_polls_required[TC_WX_FIRMWARE_UPLOAD]
            if (polls_required > 0):
                if ((asset.radio_polls_sent - asset.radio_polls_sent_at_upload_time) < polls_required):
                    continue

            # Below - just added a manual test for snap_address to get around
            # the current issue with being unable to download firmware into the NCCB.
            # Longer term this needs to be handled differently, either through a new
            # USB serial path (requires additional hardware) or via the addition of a
            # second SNAP radio.
            if asset.snap_address is not None:
                if asset.snap_address not in SIMULATED_NODES:
                    if asset.firmware_rev_string is not None:
                        if asset.firmware_rev_string != self.configured_asset_stm32_firmware_version:
                            self._nodes_awaiting_upload.add(asset.snap_address)
                            log_event(asset.snap_address, "Asset Main Firmware upload needed for asset %s", EventType.FIRMWARE_UPDATE._dict)

        if len(self._nodes_awaiting_upload) != 0:
            self.start_uploads(self.configured_asset_stm32_firmware_filename,
                               list(self._nodes_awaiting_upload),
                               TC_WX_FIRMWARE_UPLOAD,
                               AUTOMATICALLY_TRIGGERED)
        else:
            self.check_row_box_configs()

    def check_row_box_configs(self):
        """Runs periodically and checks to see if an asset CONFIG upload is NEEDED"""
        try:
            filename = ROWS_CSV_FILENAME
            filename = os.path.join(self._config_dir, filename)
            with open(filename, 'rb') as csv_file:
                csv_reader = csv.reader(csv_file)
                for row_data in csv_reader:
                    if len(row_data) >= CSV_COLUMNS_COUNT:
                        snap_address = row_data[CSV_SNAP_ADDRESS_INDEX]
                        if len(snap_address) == 8:
                            snap_address = snap_address.replace('.', '')
                        if len(snap_address) != 6:
                            continue
                        asset = self._asset_mgr.get_asset_if_known(snap_address)
                        if asset is None:
                            continue

                        upload_needed = False

                        # This feature can be disabled on-the-fly via the Cloud or the SLUI
                        if self.do_upload_asset_configs:

                            if asset.snap_address in self._forced_updates[TC_CONFIG_UPLOAD]:
                                upload_needed = True
                            else:
                                # Make sure we have had time to gather the data before we try to act on it...
                                polls_required = self.upload_polls_required[TC_CONFIG_UPLOAD]
                                if (polls_required > 0):
                                    if ((asset.radio_polls_sent - asset.radio_polls_sent_at_upload_time) < polls_required):
                                        continue

                                if (asset.offline == False) and (asset.config_label != 'UNKNOWN'):
                                    if asset.config_label != row_data[CSV_CONFIG_LABEL_INDEX]:
                                        log_event(asset.snap_address, "Configuration upload needed for asset %s", EventType.FIRMWARE_UPDATE._dict)
                                        upload_needed = True

                        if upload_needed:
                            self._uploader[TC_CONFIG_UPLOAD].dm.adapter.row_data = row_data # remember the config settings from the CSV file
                            self._nodes_awaiting_upload.add(asset.snap_address)
                            self.start_uploads('dummy_filename', list(self._nodes_awaiting_upload), TC_CONFIG_UPLOAD)
                            return

        except:
            # Currently the use of a rows.csv file is optional
            # Future location of call to NEXT type of upload...
            return

    def start_uploads(self, base_filename, addresses, upload_type, trigger_type=AUTOMATICALLY_TRIGGERED):
        # Radio module has no concept of file extensions...
        if upload_type == TC_WX_FIRMWARE_UPLOAD:
            full_file_name = base_filename + HEX_FILE_EXT
        elif (upload_type == BRIDGE_RADIO_FIRMWARE_UPLOAD) or (upload_type == ASSET_RADIO_FIRMWARE_UPLOAD):
            full_file_name = base_filename + SFI_FILE_EXT
        elif (upload_type == BRIDGE_SPY_FILE_UPLOAD) or (upload_type == ASSET_SPY_FILE_UPLOAD):
            full_file_name = base_filename + SPY_FILE_EXT
        else:
            full_file_name = base_filename + SFI_FILE_EXT # Extension is important here! Do not change!
        # ...or directories for that matter...
        filename = os.path.join(self._image_dir, full_file_name)

        self._upload_type = upload_type

        # Always use the latest (user specified) retry values
        self._uploader[upload_type].dm.max_retries = self.upload_retries[upload_type]

        # NOTE - NCCB_FIRMWARE is still 100% TODO!
        self._uploader[upload_type].start(filename,
                                          addresses,
                                          self.upload_batch_sizes[upload_type],
                                          trigger_type)
