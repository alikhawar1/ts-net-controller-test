import logging
import datetime

import tornado.ioloop

from enum import Enum

import monotonic

from controller_enums import TrackerPresets

from PauseButtons import PauseButtonManager, PauseButton
from CommunicationsPriorities import *

from persistent_state import PersistentState
from controller_enums import EventType

log = logging.getLogger(__name__)
event_log = logging.getLogger('EventLog')


class CommandedPanelState(Enum):
    """
    Current commanded panel state
    """
    ESTOP = 0
    STOW_WEATHER = 1
    NORMAL = 2
    STOW_NIGHT = 3
    PRESET = 4

    @property
    def description(self):
        if self == CommandedPanelState.ESTOP:
            return "Emergency Stop"
        if self == CommandedPanelState.STOW_WEATHER:
            return "Stow Due to Weather"
        if self == CommandedPanelState.NORMAL:
            return "Normal"
        if self == CommandedPanelState.STOW_NIGHT:
            return "Nighttime Stow"
        if self == CommandedPanelState.PRESET:
            return "User Preset Angle"

    def __str__(self):
        return self.description


#
# Sometimes we need to test functionality related to sunrise or sunset,
# and it is not convenient to wait for the REAL sunrise or sunset.
# We sometimes patch in these fake objects during manual testing under
# the debugger.
#
class FakeResult(object):
    pass

class FakeSunTracker(object):
    FORECAST_SPACING = None
    def __init__(self, minutes_until_sunrise=1, minutes_until_sunset=5):
        self._minutes_until_sunrise = minutes_until_sunrise
        self._minutes_until_sunset = minutes_until_sunset

    def get_sun_rise_set(self):
        now = datetime.datetime.utcnow()
        fake_sunrise = now + datetime.timedelta(minutes = self._minutes_until_sunrise)
        fake_sunset = now + datetime.timedelta(minutes = self._minutes_until_sunset)
        result = FakeResult()
        result.sunrise = fake_sunrise
        result.sunset = fake_sunset
        return result

    def get_forecast(self, ignored1):
        return ""

class AssetCommander(object):
    ESTOP_SEND_FREQ = 5  # seconds
    STOW_SEND_FREQ = 30  # seconds
    NIGHTLY_STOW_SEND_FREQ = 30  # seconds
    PRESET_SEND_FREQ = 30  # seconds
    TRACK_SEND_FREQ = 5 * 60  # seconds (used to be longer before individual control modes added)
    LARGEST_SEND_FREQ = TRACK_SEND_FREQ
    REVEILLE_WINDOW = 60  # seconds
    # Reveille messages (reset_all_totals()) go out every second for the entire REVEILLE_WINDOW
    # To support the new Asset Real Time Clocks, we are replacing a percentage of these messages
    # with time_is() messages.
    TIME_INSTEAD_COUNTER = 6 # make every nth REVEILLE be a time_is() instead
    DEFAULT_PANEL_STATE = CommandedPanelState.NORMAL

    def __init__(self, sun_tracker, weather_mon, estop_handler, my_site, asset_broadcaster, persistent_state, on_panel_state_change=None):
        """
        In charge of telling the panel assets how they should be orientated
        :param sun_tracker: Sun tracker to get panel tracking angels
        :param weather_mon: Weather monitor to query for weather stow state
        :param estop_handler: Emergency stop handler to query emergency stop button state
        :param my_site: Site configuration to get desired operational mode
        :param asset_broadcaster: Mesh broadcaster to send commands to assets
        :param on_panel_state_change: Optional. Callable to notify when the commanded panel state is changed
        """
        self.estop_handler = estop_handler
        self.asset_broadcaster = asset_broadcaster
        self.weather_mon = weather_mon
        self.sun_tracker = sun_tracker  # Real code
        # self.sun_tracker = FakeSunTracker(3,10)  # Test code, never commit with this enabled!
        self.my_site = my_site
        self.persistent_state = persistent_state
        self.on_panel_state_change = on_panel_state_change

        # Handle the transition from old NVRAM content to new NVRAM content
        if self.persistent_state.loaded_version == 0:
            # We loaded an old NVRAM image. Use the original code's default value
            self._pstate = AssetCommander.DEFAULT_PANEL_STATE
            self.persistent_state.set_tracking_state(self._pstate.value)
            event_text = 'No saved info, panels defaulting initially to '
        else:
            # We have valid NVRAM data. Use what we saved out last instead of just a default value
            self._pstate = CommandedPanelState(persistent_state.get_tracking_state())
            event_text = 'Panel state reloaded, panels starting initially in '
        event_text += self.generate_state_text(self._pstate, self.my_site.operational_mode)
        event_log.info(event_text, extra=EventType.INDIVIDUAL_ASSET._dict)

        self.previous_panel_state = self._pstate
        self.previous_operational_mode = self.my_site.operational_mode
        self.previous_tracking_enable = self.my_site.tracking_enable
        self.previous_backtracking_enable = self.my_site.backtracking_enable

        self.last_broadcast_time = None
        self.current_rise_set = None
        self.last_reveille_broadcast_time = None
        self.reveille_counter = 0

        pause_button_manager = PauseButtonManager.get_default_instance()
        self._pause_button = pause_button_manager.get_pause_button(ASSET_COMMANDER_PRIORITY)

        # We used to schedule _update() right away, but this led to a glitch in the
        # E-STOP button handling... _update() would run before the button state had
        # been read in initially (via USB). Now we wait a few seconds for data to
        # come in before we start making decisions on it.
        self.startup_timer = tornado.ioloop.PeriodicCallback(self.start_polling, 5000)
        self.startup_timer.start()
        self._notify_state_change_cb(self._pstate)

    def start_polling(self):
        self.startup_timer.stop()
        tornado.ioloop.PeriodicCallback(self._update_state, 1000).start()

    @property
    def panel_state(self):
        return self._pstate

    def generate_state_text(self, state, preset):
        if state != CommandedPanelState.PRESET:
            text = '"%s" state' % (state,)
        else:
            text = '"%s" state, preset = "%s"' % (state, TrackerPresets(preset))
        return text

    def generate_mode_text(self, tracking_enable, backtracking_enable, preset):
        if tracking_enable:
            text = '"Tracking" mode with'
            if backtracking_enable:
                text += ' backtracking'
            else:
                text += 'out backtracking'
        else:
            text = '"%s" mode' % (TrackerPresets(preset))
        return text

    def log_transition(self, old_state, old_preset, new_state, new_preset):
        event_text = 'Panels will be commanded to be in '
        event_text += self.generate_state_text(new_state, new_preset)
        event_text += '. They were previously in '
        event_text += self.generate_state_text(old_state, old_preset)
        event_log.info(event_text, extra=EventType.INDIVIDUAL_ASSET._dict)

    @panel_state.setter
    def panel_state(self, state):
        if state != self._pstate:
            self.last_broadcast_time = 0
            self.log_transition(self._pstate, self.previous_operational_mode, state, self.my_site.operational_mode)

            # notice we save the numeric value, not the enum OBJECT!
            self.persistent_state.set_tracking_state(state.value)

            self.previous_panel_state = self._pstate
            self._notify_state_change_cb(state)
        self._pstate = state

    def _update_state(self):
        """
        Check the current state of the inputs in priority order and command the panels accordingly
        :return: None
        """
        if self._pause_button.paused:
            return

        now = datetime.datetime.utcnow()
        recalc = False
        if self.current_rise_set is None:
            recalc = True
        elif (now.day != self.current_rise_set.sunrise.day) and (now > self.current_rise_set.sunset):
            recalc = True
        if recalc:
            self.current_rise_set = self.sun_tracker.get_sun_rise_set()
            log.info("Sunrise=%s Sunset=%s" % (self.current_rise_set.sunrise, self.current_rise_set.sunset))

        saved_state = self._pstate # used to detect a corner case down below...

        if self.estop_handler.in_estop:  # Accounts for test mode
            self.panel_state = CommandedPanelState.ESTOP

        elif not self.my_site.tracking_enable: # Must be set to one of the many presets
            self.panel_state = CommandedPanelState.PRESET

        elif self.weather_mon.stow_needed:
            self.panel_state = CommandedPanelState.STOW_WEATHER

        elif now < self.current_rise_set.sunrise or now > self.current_rise_set.sunset:
            if self.is_weather_reporting:
                self.panel_state = CommandedPanelState.STOW_NIGHT

        elif self.my_site.tracking_enable:
            if self.is_weather_reporting:
                self.panel_state = CommandedPanelState.NORMAL
        
        # event_log.info("Site Uptime: " + str(self.my_site.uptime), extra=EventType.NETWORK_CONTROLLER._dict)

        # Handle situations where users are making changes to Operational Mode that
        # have no immediate effect because higher priority STATES are already in effect
        if self.panel_state in [CommandedPanelState.ESTOP, CommandedPanelState.STOW_WEATHER, CommandedPanelState.STOW_NIGHT]:
            mode_changed = False
            if self.previous_tracking_enable != self.my_site.tracking_enable:
                mode_changed = True
            if self.previous_backtracking_enable != self.my_site.backtracking_enable:
                mode_changed = True
            if self.previous_operational_mode != self.my_site.operational_mode:
                mode_changed = True
            if mode_changed:
                event_text = 'Operational mode has been changed from '
                event_text += self.generate_mode_text(self.previous_tracking_enable, self.previous_backtracking_enable, self.previous_operational_mode)
                event_text += ' to '
                event_text += self.generate_mode_text(self.my_site.tracking_enable, self.my_site.backtracking_enable, self.my_site.operational_mode)
                event_text += ' but the site is currently in "%s"' % (self._pstate,)
                event_log.info(event_text, extra=EventType.INDIVIDUAL_ASSET._dict)

        # Handle situation where users are changing between individual presets but remaining in PRESET State
        # (Because there is no STATE change in this scenario, the normal delta reporting does not fire)
        if (saved_state == CommandedPanelState.PRESET) and (self._pstate == CommandedPanelState.PRESET):
            if self.previous_operational_mode != self.my_site.operational_mode:
                self.log_transition(saved_state, self.previous_operational_mode, self._pstate, self.my_site.operational_mode)
                self._notify_state_change_cb(self._pstate)

        # Handle situation where users are changing the backtracking enable but remaining in NORMAL State
        # (Because there is no STATE change in this scenario, the normal delta reporting does not fire)
        if (saved_state == CommandedPanelState.NORMAL) and (self._pstate == CommandedPanelState.NORMAL):
            if self.previous_backtracking_enable != self.my_site.backtracking_enable:
                event_text = 'Operational mode has been changed to '
                event_text += self.generate_mode_text(self.my_site.tracking_enable, self.my_site.backtracking_enable, self.my_site.operational_mode)
                event_log.info(event_text, extra=EventType.INDIVIDUAL_ASSET._dict)
                self._notify_state_change_cb(self._pstate)

        self.previous_tracking_enable = self.my_site.tracking_enable
        self.previous_backtracking_enable = self.my_site.backtracking_enable
        self.previous_operational_mode = self.my_site.operational_mode

        self.command()

    def _notify_state_change_cb(self, new_state):
        try:
            if callable(self.on_panel_state_change):
                self.on_panel_state_change(self.previous_panel_state, new_state, self.my_site)
        except (SystemExit, KeyboardInterrupt):
            raise
        except:
            log.exception("An error occurred while notifying of a panel state change")

    def command(self):
        """
        Based on the current state send any necessary commands to the panels
        :return: None
        """
        sunrise_diff = datetime.datetime.utcnow() - self.current_rise_set.sunrise - datetime.timedelta(seconds=self.REVEILLE_WINDOW)
        monotime_now = monotonic.monotonic()
        if self.last_broadcast_time is None:
            monotime_diff = self.LARGEST_SEND_FREQ + 1
        else:
            monotime_diff = monotime_now - self.last_broadcast_time

        # "Reveille" (Accumulator Reset) is a "time-sensitive" command.
        # Because of this it was pulled out of the prioritized handling (below)
        # The assumption is that any critical "stow" or "stop" commands will
        # get sent both before and after the REVEILLE_WINDOW, plus they will
        # only suffer a performance hit during the window (they will still be sent)
        # (Prior to this change reveille would be blocked by ESTOP or WEATHER_STOW)
        if 0 < sunrise_diff.total_seconds() < self.REVEILLE_WINDOW:
            # When RTC support was added to the system, we decided the existing
            # once-a-day reveille window was a reasonable time to also set the clocks.
            self.reveille_counter += 1
            if self.reveille_counter >= self.TIME_INSTEAD_COUNTER:
                self.reveille_counter = 0
                self.asset_broadcaster.time_is()
            else:
                self.asset_broadcaster.reveille()
            self.last_reveille_broadcast_time = monotime_now

        if self.panel_state == CommandedPanelState.ESTOP:
            if monotime_diff >= self.ESTOP_SEND_FREQ:
                self.asset_broadcaster.stop()
                self.last_broadcast_time = monotime_now
            # See notes in EmergencyStopHandler.py for future ideas
        elif self.panel_state == CommandedPanelState.STOW_WEATHER:
            if monotime_diff >= self.STOW_SEND_FREQ:
                self.asset_broadcaster.stow()
                self.last_broadcast_time = monotime_now
        elif self.panel_state == CommandedPanelState.STOW_NIGHT:
            if monotime_diff >= self.NIGHTLY_STOW_SEND_FREQ:
                self.asset_broadcaster.nightly_stow()
                self.last_broadcast_time = monotime_now
        elif 0 < sunrise_diff.total_seconds() < self.REVEILLE_WINDOW:
            # This now gets handled up above, but I still want it to maintain
            # precedence over the remaining possibilities (below)
            pass  # The point here is to pause sending of sun_angles or PRESETS during reveille
        elif self.panel_state == CommandedPanelState.NORMAL:
            if monotime_diff >= self.TRACK_SEND_FREQ:
                if self.last_reveille_broadcast_time is None:
                    monotime_diff = 0
                else:
                    monotime_diff = monotime_now - self.last_reveille_broadcast_time
                minutes_since_accumulators_reset = int(monotime_diff / 60)
                flags = 0x0000
                if self.my_site.backtracking_enable:
                    flags |= 0x8000
                forecast_starting_time = datetime.datetime.utcnow() + datetime.timedelta(minutes = 2)
                self.asset_broadcaster.sun_angles(flags,
                                                  self.sun_tracker.FORECAST_SPACING,
                                                  self.sun_tracker.get_forecast(forecast_starting_time),
                                                  minutes_since_accumulators_reset)
                self.last_broadcast_time = monotime_now
        elif self.panel_state == CommandedPanelState.PRESET:
            if monotime_diff >= self.PRESET_SEND_FREQ:
                self.asset_broadcaster.go_to_preset(self.my_site.operational_mode)
                self.last_broadcast_time = monotime_now
    
    @property
    def is_weather_reporting(self):
        return self.my_site.uptime >= 240
    
    def skip_ahead(self):
        self.last_broadcast_time = None


if __name__ == '__main__':
    # Exercise specific test scenarios (a lot more could be added here later)
    # Currently exercising paths known to have an issue (test-driven bug fixing)

    # logging.basicConfig(level=logging.WARNING)
    #logging.basicConfig(level=logging.INFO)
    logging.basicConfig(level=logging.DEBUG)
    import random

    class FakeWeatherMon(object):
        def __init__(self):
            self.stow_needed = False

    class FakeEstopHandler(object):
        def __init__(self):
            self.in_estop = False

    class FakeSite(object):

        def __init__(self):
            self.operational_mode = TrackerPresets.FLAT.value
            #self.tracking_enable = True
            self.tracking_enable = False
            self.backtracking_enable = True

    class FakeAssetBroadcaster(object):

        def stop(self):
            print "stop"

        def stow(self):
            print "stow" # put away carefully - to stow a row is to place it in a safe position

        def reveille(self):
            print "reveille" # reveille signal is what tells units to restart their daily totals

        def nightly_stow(self):
             print  "nightly_stow triggered"

        def sun_angles(self, ignored1, ignored2, ignored3, ignored4):
            print "sun_angles"

        def go_to_preset(self, ignored1):
            print "go_to_preset"

        def time_is(self):
            t = datetime.datetime.utcnow()
            print "time_is %s %s %s %s %s %s" % (t.year, t.month, t.day, t.hour, t.minute, t.second)


    # Scenario 1 - unit starts up before sunrise, then time elapses
    sun_tracker = FakeSunTracker()
    weather_mon = FakeWeatherMon()
    estop_handler = FakeEstopHandler()
    my_site = FakeSite()
    asset_broadcaster = FakeAssetBroadcaster()

    class DummyObject(object):
        pass

    dummy_object = DummyObject()
    my_persistent_state = PersistentState()
    asset_commander = AssetCommander(sun_tracker = sun_tracker, weather_mon = weather_mon, estop_handler = estop_handler,
                                     asset_broadcaster = asset_broadcaster, my_site = my_site,
                                     persistent_state = my_persistent_state, on_panel_state_change=None)


    tornado.ioloop.IOLoop.current().start()
