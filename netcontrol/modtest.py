"""Test functions for MODBUS
Uses Python struct module to decode values from register-blocks.
See: https://docs.python.org/3/library/struct.html#format-strings

Run with Python3 in a virtualenv.

Dependency: PyModbusTCP
>> pip install pymodbustcp
"""
import struct
import datetime
from pyModbusTCP.client import ModbusClient


HOST_ADDR = 'localhost'
HOST_PORT = 5552
#HOST_ADDR = '192.168.1.133'
HOST_ADDR = '192.168.1.184'
HOST_PORT = 502

class Asciiz(object):
    def __init__(self, nregs):
        self.size = nregs
        self.decoder = struct.Struct(">%dH" % self.size)

    def get_reg16_seq(self, strval):
        """Convert native to reg16 sequence. (for MODBUS reads)"""
        n = self.size * 2
        strval = strval[:n-1].ljust(n, '\x00')
        return self.decoder.unpack(strval)

    def get_native(self, reg16_seq):
        """Convert reg16 sequence to native. (for MODBUS writes)"""
        return self.decoder.pack(*reg16_seq)

def time_string(timestamp):
    if timestamp == 0:
        return "(unspecified time)"
    return str(datetime.datetime.fromtimestamp(timestamp))

def read_asciiz(dev_id, addr, n_regs):
    #c = ModbusClient(host="localhost", unit_id=dev_id, auto_open=True)
    c = ModbusClient(host=HOST_ADDR, port=HOST_PORT, unit_id=dev_id, auto_open=True)

    regs = c.read_holding_registers(addr, n_regs)
    #print("Read %d regs=%s" % (len(regs), str(regs)))

    a = Asciiz(len(regs))
    #print("  native: '%s'" % a.get_native(regs))
    return a.get_native(regs)

def read_fmt(dev_id, addr, n_regs, fmt):
    decoder = struct.Struct(fmt)
    encoder = struct.Struct('>%dH' % (decoder.size // 2))
    c = ModbusClient(host=HOST_ADDR, port=HOST_PORT, unit_id=dev_id, auto_open=True)

    regs = c.read_holding_registers(addr, n_regs)
    #print("Read %d regs=%s" % (len(regs), str(regs)))

    bin = encoder.pack(*regs)  # Encode register block into binary string
    val = decoder.unpack(bin)[0]  # Decode per requested format
    #print("  native: '%s'" % str(val))
    return val


print("TC:")
print("System ID:", read_asciiz(0, 0, 3))
print("Site Name:", read_asciiz(0, 3, 10))
print("Organization:", read_asciiz(0, 13, 10))
print("Contact:", read_asciiz(0, 23, 10))
print("Uptime:", read_fmt(0, 33, 4, '>Q'))
print("Software Version:", read_asciiz(0, 37, 8))
print("Has WX:", read_fmt(0, 45, 1, '>H'))
print("E-Stop active:", read_fmt(0, 46, 1, '>H'))
print("Tracking enabled:", read_fmt(0, 47, 1, '>H'))
print("Backtracking enabled:", read_fmt(0, 48, 1, '>H'))
print("Calculated sun angle: ", read_fmt(0, 49, 2, '>f'))
print("Commanded Tracking State:", read_fmt(0, 51, 1, '>H'))
print("Temperature:", read_fmt(0, 52, 2, '>f'))

print("Battery Voltage:", read_fmt(0, 54, 2, '>f'))
print("Battery Current:", read_fmt(0, 56, 2, '>f'))
print("Battery Charge %:", read_fmt(0, 58, 1, '>H'))
print("Battery Health %:", read_fmt(0, 59, 1, '>H'))
print("Charger Voltage:", read_fmt(0, 60, 2, '>f'))
print("Charger Current:", read_fmt(0, 62, 2, '>f'))
print("Solar Voltage:", read_fmt(0, 64, 2, '>f'))
print("Solar Current:", read_fmt(0, 66, 2, '>f'))


total_num_assets = read_fmt(0, 68, 1, '>H')
print("Total assets:", total_num_assets)
print("Assets reporting:", read_fmt(0, 69, 1, '>H'))
print("Latest Poll Response:", time_string(read_fmt(0, 70, 4, '>Q')))
print("SNAP Bridge Address: %s" % (hex(read_fmt(0, 74, 2, '>I'))))
print("SNAP Bridge CRC: %s" % (hex(read_fmt(0, 76, 1, '>H'))))
print("SNAP Channel:", read_fmt(0, 77, 1, '>H'))
print("SNAP Net ID: %s" % (hex(read_fmt(0, 78, 1, '>H'))))

print("GPS Fix Acquired:", bool(read_fmt(0, 79, 1, '>H')))
print("GPS Fix timestamp:", time_string(read_fmt(0, 80, 4, '>Q')))
print("GPS Satellites:", read_fmt(0, 84, 1, '>H'))
print("GPS Latitude:", read_fmt(0, 85, 2, '>f'))
print("GPS Longitude:", read_fmt(0, 87, 2, '>f'))

print("Cell Link Status:", bool(read_fmt(0, 89, 1, '>H')))
print("Cell RSSI:", read_fmt(0, 90, 1, '>h'))
print("WAN IP Addr:", read_asciiz(0, 91, 8))
print("LAN IP Addr:", read_asciiz(0, 99, 8))
print("Modem Uptime:", read_fmt(0, 107, 2, '>I'))

print("NTP Sync Status:", bool(read_fmt(0, 109, 1, '>H')))
print("NTP Server:", read_asciiz(0, 110, 16))
print("NTP Offset:", read_fmt(0, 126, 2, '>f'))
print()

# Alerts
alert_index = 0
while alert_index <= 7:
    base_addr = alert_index * 25 + 128
    print("Alert %d -->" % (alert_index+1,))
    print("  Timestamp: ", time_string(read_fmt(0, base_addr, 4, '>Q')))
    print("  Type:", read_fmt(0, base_addr+4, 1, '>H'))
    print("  Message:", read_asciiz(0, base_addr+5, 20))
    alert_index += 1
    print()

# Poll remote devices
for i_dev in range(total_num_assets+1): # +1 for NCCB taking up a "slot"
    unit_id = 1 + (i_dev // 100)
    base_addr = (i_dev % 100) * 512
    print("Device-%d, Unit ID %d, Base Register %d:" % (i_dev, unit_id, base_addr))
    print("  Device type:", read_asciiz(unit_id, base_addr + 0, 8))
    print("  Last reported:", time_string(read_fmt(unit_id, base_addr + 8, 4, '>Q')))
    print("  Model:", read_asciiz(unit_id, base_addr + 12, 8))
    print("  HW rev:", read_asciiz(unit_id, base_addr + 20, 8))
    print("  FW rev:", read_asciiz(unit_id, base_addr + 28, 8))
    print("  Uptime:", read_fmt(unit_id, base_addr + 36, 2, '>I'))
    print("  SNAP Address: %s" % (hex(read_fmt(unit_id, base_addr + 38, 2, '>I'))))
    print("  RSSI:", read_fmt(unit_id, base_addr + 40, 1, '>h'))
    print("  Has Weather sensors:", read_fmt(unit_id, base_addr + 41, 1, '>H'))
    print("  Has Tracker HW:", read_fmt(unit_id, base_addr + 42, 1, '>H'))
    print("  Location Text:", read_asciiz(unit_id, base_addr + 43, 16))
    print("  Latitude:", read_fmt(unit_id, base_addr + 59, 2, '>f'))
    print("  Longitude:", read_fmt(unit_id, base_addr + 61, 2, '>f'))
    print("  Config Flags: %s" % (hex(read_fmt(unit_id, base_addr + 63, 2, '>I'))))
    print("  Battery Voltage:", read_fmt(unit_id, base_addr + 65, 2, '>f'))
    print("  Battery Current:", read_fmt(unit_id, base_addr + 67, 2, '>f'))
    print("  Battery Charge %:", read_fmt(unit_id, base_addr + 69, 1, '>H'))
    print("  Battery Health %:", read_fmt(unit_id, base_addr + 70, 1, '>H'))
    print("  Charger Voltage:", read_fmt(unit_id, base_addr + 71, 2, '>f'))
    print("  Charger Current:", read_fmt(unit_id, base_addr + 73, 2, '>f'))
    print("  Solar Voltage:", read_fmt(unit_id, base_addr + 75, 2, '>f'))
    print("  Solar Current:", read_fmt(unit_id, base_addr + 77, 2, '>f'))
    print("  Controller status bitfields: %s" % (hex(read_fmt(unit_id, base_addr + 79, 2, '>I'))))
    print("  Tracking Status enum: %s" % (hex(read_fmt(unit_id, base_addr + 81, 1, '>H'))))
    print("  Rack Current Angle:", read_fmt(unit_id, base_addr + 82, 2, '>f'))
    print("  Rack Requested Angle:", read_fmt(unit_id, base_addr + 84, 2, '>f'))
    print("  Motor Power prev hour:", read_fmt(unit_id, base_addr + 86, 2, '>f'))
    print("  Angular Error prev hour:", read_fmt(unit_id, base_addr + 88, 2, '>f'))
    print("  Wind Speed:", read_fmt(unit_id, base_addr + 90, 2, '>f'))
    print("  Wind Direction:", read_fmt(unit_id, base_addr + 92, 2, '>f'))
    print("  Average Wind Speed:", read_fmt(unit_id, base_addr + 94, 2, '>f'))
    print("  Peak Wind Speed:", read_fmt(unit_id, base_addr + 96, 2, '>f'))
    print("  Snow Sensor temperature:", read_fmt(unit_id, base_addr + 98, 2, '>f'))
    print("  Snow Sensor distance:", read_fmt(unit_id, base_addr + 100, 2, '>f'))
    print("  Snow depth:", read_fmt(unit_id, base_addr + 102, 2, '>f'))
    print()
