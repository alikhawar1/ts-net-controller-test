"""
EmergencyStopHandler.py - code to manage the Emergency Stop
button on the Network Controller (technically, it is on the
NCCB *attached to* the Network Controller).

At first glance, the requirements are pretty simple:
If the button is pressed, someone should broadcast the stop() command.

The reality is slightly more complicated, due to the following factors:
1) The desire for a "timed test mode", that lets the button be pushed
WITHOUT causing any actual stoppage
2) What else? (not yet addressed)
    2A) How long to broadcast the stop() commands?
    2B) Do we verify they were acted upon?
    2C) Etc.

Per convention, public API is anything not marked _private, but here it is:

Functions:

start_estop_test(seconds) - begin a test period

Data:

in_estop - a REAL Emergency Stop is in effect
    Can only occur when NOT in test mode
estop_is_pressed - in case the UI wants to show this
    This is a "live" button reading, and tracks regardless of test mode
estop_test_remaining - the seconds remaining (or 0)
estop_test_passed -  True or False
    Must call start_estop_test(seconds) first!
    estop_test_passed always set to False at start of test.
    estop_test_passed set to True if any button presses detected during the test.
    estop_test_passed==False should be ignored until estop_test_remaining == 0
"""


import logging


from controller_enums import TCStatusBits


log = logging.getLogger(__name__)


class EmergencyStopHandler(object):
    """Responsible for all Emergency Stop button handling"""
    def __init__(self, schedule, asset, on_estop_change):
        """
        Instantiates an instance of EmergencyStopHandler
        :param callable schedule: A function with the signature func(call_every_n_seconds, callback_func)
            to schedule reoccurring callbacks
        :param object asset: The asset object to update
        :param func on_estop_change: Optional. Callable for when the emergency stop state changes. Signature is func(state, msg)
        """
        self._asset = asset
        self._on_estop_change = on_estop_change

        self.in_estop = False
        self.estop_is_pressed = False
        self.estop_test_remaining = 0
        self.estop_test_passed = True

        schedule(1.0, self._every_second)
        schedule(1.0, self._every_100_milliseconds)

    def start_estop_test(self, seconds):
        """Begin a test period for the specified number of seconds"""
        # Don't allow a test if we are currently in emergency stop.
        # However, if a test is already running, allow it to be restarted
        if not self.in_estop or self.estop_test_remaining:
            self.estop_test_remaining = seconds
            self.estop_test_passed = False

    def _every_second(self):
        if self.estop_test_remaining:
            self.estop_test_remaining -= 1

        return True

    # Assumed - 10 times a second is often enough to check this
    def _every_100_milliseconds(self):
        self.estop_is_pressed = ((self._asset.status_bits & TCStatusBits.ESTOP_IS_PRESSED.value) != 0)

        if self.estop_test_remaining == 0:
            # Not in test mode...
            if self.estop_is_pressed and not self.in_estop:
                # log as critical AND report to the cloud
                self.on_estop_change("Emergency stop button is engaged and no test is active")
            elif self.in_estop and not self.estop_is_pressed:
                # log as critical AND report to the cloud
                self.on_estop_change("Emergency stop button was disengaged")

            self.in_estop = self.estop_is_pressed
        else:
            # Test mode is active...
            self.in_estop = False
            if self.estop_is_pressed:
                self.estop_test_passed = True
        return True

    def on_estop_change(self, msg):
        """
        Notify the subscriber that the state of the estop has changed
        :param str msg: The event log message string to log
        :return: None
        """
        log.critical(msg)
        if self._on_estop_change:
            try:
                self._on_estop_change(self.estop_is_pressed, msg)
            except:
                pass
