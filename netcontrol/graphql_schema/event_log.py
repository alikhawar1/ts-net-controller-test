import logging

import graphene
from graphene.types.datetime import DateTime
from graphene.types.resolver import dict_resolver


class EventLevel(graphene.Enum):
    CRITICAL = logging._levelNames.get(logging.CRITICAL)
    FATAL = logging._levelNames.get(logging.FATAL)
    ERROR = logging._levelNames.get(logging.ERROR)
    WARNING = logging._levelNames.get(logging.WARNING)
    WARN = logging._levelNames.get(logging.WARN)
    INFO = logging._levelNames.get(logging.INFO)
    DEBUG = logging._levelNames.get(logging.DEBUG)
    NOTSET = logging._levelNames.get(logging.NOTSET)

    @property
    def description(self):
        return logging._levelNames.get(self, str(self))


class EventLogEntry(graphene.ObjectType):
    id = graphene.ID()
    name = graphene.String()
    message = graphene.String()
    levelno = graphene.Int()
    levelname = graphene.Field(EventLevel)
    created = DateTime()
    cleared = DateTime(default_value=None)
    # event_type = graphene.String()


class ClearEventLogEntries(graphene.Mutation):
    class Input:
        ids = graphene.List(graphene.ID)
        cleared = graphene.Boolean(default_value=True)

    class Meta:
        default_resolver = dict_resolver

    event_log_entries = graphene.List(EventLogEntry)

    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError


class EventLog(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    event_log_entries = graphene.List(EventLogEntry,
                                      levelno=graphene.Argument(graphene.Int),
                                      created=graphene.Argument(DateTime),
                                      cleared=graphene.Argument(graphene.Boolean))
    entries_count = graphene.Int(levelno=graphene.Argument(graphene.Int),
                                 countCleared=graphene.Argument(graphene.Boolean))
