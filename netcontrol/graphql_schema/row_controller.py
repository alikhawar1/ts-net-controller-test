import graphene
from graphene.types.datetime import DateTime

from netcontrol import controller_enums


class TCSensor(graphene.ObjectType):
    sensor_num = graphene.Int(description="The asset's sensor number")
    sensor_adc_mode = graphene.Field(graphene.Enum.from_enum(controller_enums.SensorConfigMode), description="The sensor's ADC mode")
    sensor_flags = graphene.List(graphene.Enum.from_enum(controller_enums.SensorConfigFlags), description="The sensor's enabled flags")
    sensor_adc_value = graphene.Int(description="The last reported sensor ADC value")
    sensor_digital_input_value = graphene.Boolean(description="The last reported sensor digital input value")


class TCPresetAngle(graphene.ObjectType):
    preset_num = graphene.Int(description="This preset number")
    nearest_enabled = graphene.Boolean(description="Indicates if this preset is enabled")
    preset_angle = graphene.Float(description="This preset angle degrees")


class TCSegment(graphene.ObjectType):
    segment_num = graphene.Int(description="This segment number")
    panel_array_width = graphene.Float(description="The solar panel array width as measured in centimeters")
    spacing_to_east = graphene.Float(description="The east separation to the next solar panel array as measured in centimeters")
    spacing_to_west = graphene.Float(description="The west separation to the next solar panel array as measured in centimeters")
    delta_height_east = graphene.Float(description="The east delta height to the next solar panel array as measured in centimeters")
    delta_height_west = graphene.Float(description="The west delta height to the next solar panel array as measured in centimeters")


class TCAsset(graphene.ObjectType):
    """
    Describes a tracker controller asset
    """
    id = graphene.ID(description="The asset's unique identifier")
    last_reported = DateTime(description="The last time we heard from this asset")

    segments = graphene.List(TCSegment, description="The asset's configurable backtracking segments")

    sensors = graphene.List(TCSensor, description="The asset's connected sensors")

    preset_angles = graphene.List(TCPresetAngle, description="The asset's configured preset angles")

    # Config
    panel_horizontal_cal_angle = graphene.Float(description="The panel's horizontal angle calibration measurement")
    panel_min_cal_angle = graphene.Float(description="The panel's minimum allowed angle calibration measurement")
    panel_max_cal_angle = graphene.Float(description="The panel's maxiumum allowed angle calibration measurement")
    config_label = graphene.String(description="Descriptive label for the current configuration settings")
    config_timestamp = DateTime(description="The timestamp for the current configuration settings")
    site_name = graphene.String(description="A descriptive site name")
    location_lat = graphene.Float(description="The latitude location of the tracker")
    location_lng = graphene.Float(description="The longitude location of the tracker")
    location_text = graphene.String(description="A descriptive text location of the tracker")
    config_flags = graphene.Int(description="This is a bitmap of various config settings")
    wind_dir_offset = graphene.Int(description="Signed angular offset added to wind dir readings")
    snow_sensor_height = graphene.Float(description="The height of the snow sensor as measured in meters")
    model = graphene.String(description="This asset's model number")
    device = graphene.String(description="This asset's device type")
    hardware_rev = graphene.String(description="This asset's hardware revision")
    firmware_rev = graphene.String(description="This asset's firmware revision")
    has_weather_sensor = graphene.Boolean(description="If this asset has a weather station attached")
    has_tracker_hardware = graphene.Boolean(description="If this asset is a tracker controller")

    # Status
    offline = graphene.Boolean(description="Indicates if unit has been marked offline")
    statuses = graphene.List(graphene.Enum.from_enum(controller_enums.TCStatusBits), description="The currently reported statuses of this asset")
    uptime = graphene.Int(description="Total amount of time in seconds this asset has considered itself connected")
    downtime = graphene.Int(description="Total amount of time in seconds this asset has considered itself disconnected")
    offline_minutes = graphene.Int(description="Number of minutes this asset was in commissioning mode")
    commissioning_mode = graphene.Int(description="Row controller is in commissioning mode")
    current_angle = graphene.Float(description="The current angle of the panel in degrees")
    requested_angle = graphene.Float(description="The requested angle of the panel in degrees")
    battery_voltage = graphene.Float(description="Voltage of the battery")
    solar_voltage = graphene.Float(description="Voltage of the solar panel")
    charger_current = graphene.Float(description="Current in Amps used by the battery charger (0 is off)")
    unit_temperature = graphene.Float(description="Temperature read by the unit's temperature sensor in degrees fahrenheit")
    battery_temperature = graphene.Float(description="Temperature of the battery charger in degrees Fahrenheit")
    battery_current = graphene.Float(description="The output of the battery in Amps. If the battery is CHARGING this field will be 0")
    solar_current = graphene.Float(description="The output of the solar panel in Amps")
    charger_voltage = graphene.Float(description="The voltage of the battery charger. If the battery is NOT charging it will be 0")
    motor_current = graphene.Float(description="Current in Amps used by the motor")
    heater_temperature = graphene.Float(description="Temperature of the heater in degrees Fahrenheit")
    battery_charged = graphene.Int(description="Percentage of battery charged")
    battery_health = graphene.Int(description="Percentage of battery health")
    tracking_status = graphene.Field(graphene.Enum.from_enum(controller_enums.TCTrackingStatusBits), description="The currently reported tracking status")
    solar_power_current_hour = graphene.Float(description="The current hourly accumulated Watt-Hours of solar panel power used")
    solar_power_previous_hour = graphene.Float(description="The previous hourly accumulated Watt-Hours of solar panel power used")
    solar_power_current_day = graphene.Float(description="The current daily accumulated Watt-Hours of solar panel power used")
    solar_power_previous_day = graphene.Float(description="The previous daily accumulated Watt-Hours of solar panel power used")
    battery_power_current_hour = graphene.Float(description="The current hourly accumulated Watt-Hours of battery power used")
    battery_power_previous_hour = graphene.Float(description="The previous hourly accumulated Watt-Hours of battery power used")
    battery_power_current_day = graphene.Float(description="The current daily accumulated Watt-Hours of battery power used")
    battery_power_previous_day = graphene.Float(description="The previous daily accumulated Watt-Hours of battery power used")
    charger_power_current_hour = graphene.Float(description="The current hourly accumulated Watt-Hours of battery power used")
    charger_power_previous_hour = graphene.Float(description="The previous hourly accumulated Watt-Hours of battery power used")
    charger_power_current_day = graphene.Float(description="The current daily accumulated Watt-Hours of battery power used")
    charger_power_previous_day = graphene.Float(description="The previous daily accumulated Watt-Hours of battery power used")
    motor_power_current_hour = graphene.Float(description="The current hourly accumulated Watt-Hours of battery power used")
    motor_power_previous_hour = graphene.Float(description="The previous hourly accumulated Watt-Hours of battery power used")
    motor_power_current_day = graphene.Float(description="The current daily accumulated Watt-Hours of battery power used")
    motor_power_previous_day = graphene.Float(description="The previous daily accumulated Watt-Hours of battery power used")
    average_angular_error_current_hour = graphene.Float(description="The current hourly average angular error")
    average_angular_error_previous_hour = graphene.Float(description="The previous hourly average_angular_error")
    average_angular_error_current_day = graphene.Float(description="The current daily average_angular_error")
    average_angular_error_previous_day = graphene.Float(description="The previous daily average_angular_error")
    hours_since_accumulators_reset = graphene.Int(description="Hours since accumulators reset")
    # More status, added in 06/2020 for the MODEL 3 hardware (dual power monitoring)
    external_input_2_voltage = graphene.Float(description="Voltage of the second power input")
    external_input_2_current = graphene.Float(description="The output of the second power input in Amps")
    external_input_2_power_current_hour = graphene.Float(description="The current hourly accumulated Watt-Hours of external input 2 power used")
    external_input_2_power_previous_hour = graphene.Float(description="The previous hourly accumulated Watt-Hours of external input 2 power used")
    external_input_2_power_current_day = graphene.Float(description="The current daily accumulated Watt-Hours of external input 2 power used")
    external_input_2_power_previous_day = graphene.Float(description="The previous daily accumulated Watt-Hours of external input 2 power used")
    # More status, added 10/14/2020, initially in support of heater status reporting
    # This particular bitmap comes in BLOB_N, which does not get polled as often as BLOB_S
    misc_status_bits = graphene.Int(description="This is a bitmap of various status indicators")
    # More status, added 10/25/2020, initially in support of more motor current reporting
    peak_motor_inrush_current = graphene.Float(description="Peak inrush current in Amps used by the motor")
    peak_motor_current = graphene.Float(description="Peak current in Amps used by the motor")
    average_motor_current = graphene.Float(description="Average current in Amps used by the motor")
    ending_motor_current = graphene.Float(description="Current in Amps used by the motor at time of stop")
    # This particular bitmap comes in BLOB_S, which gets polled more often than BLOB_N
    # As of 10/25/2020, bit meanings are still TBD, but are intended for high resolution reporting...
    more_status_bits = graphene.Int(description="This is a bitmap of various status indicators")

    # Radio
    radio_link_quality = graphene.Int(description="The link quality in dBm of the most recently received radio packet")
    radio_mesh_depth = graphene.Int(description="The maximum mesh depth seen")
    radio_channel = graphene.Int(description="The radio channel")
    radio_network_id = graphene.Int(description="The radio network ID")
    radio_mac_addr = graphene.String(description="The radio MAC address")
    radio_script_version = graphene.String(description="The radio script version")
    radio_script_crc = graphene.Int(description="The radio script CRC")
    radio_firmware = graphene.String(description="The radio firmware version")
    radio_polls_sent = graphene.Int(description="The number of polls sent to this unit")
    radio_poll_responses = graphene.Int(description="The number of poll responses received back from this unit")
    radio_forwarding_mask = graphene.Int(description="The radio group forwarding bitmask")
    is_a_repeater = graphene.Boolean(description="Indicates if this radio is forwarding data packets")

    # Weather
    wind_speed = graphene.Float(description="The current wind speed in MPH")
    wind_direction = graphene.Float(description="The current wind direction in degrees")
    average_wind_speed = graphene.Float(description="The 1-minute average wind speed in MPH")
    peak_wind_speed = graphene.Float(description="The 1-hour peak wind speed in MPH")
    snow_sensor_temperature = graphene.Float(description="Temperature read by the snow sensor in degrees fahrenheit")
    snow_sensor_distance = graphene.Float(description="Distance to the ground measured by the snow sensor in meters")
    snow_depth = graphene.Float(description="Calculated Snow Depth in meters")
