import graphene
from graphene.types.datetime import DateTime


class GPSQuality(graphene.Enum):
    FIX_UNAVAILABLE = 0
    FIXED_SPS_MODE = 1
    FIXED_DIFFERENTIAL_MODE = 2
    FIXED_GPS_PPS_MODE = 3
    RTK = 4
    FLOAT_RTK = 5
    ESTIMATED_MODE = 6
    MANUAL_MODE = 7

    @property
    def description(self):
        if self == GPSQuality.FIX_UNAVAILABLE:
            return "Position fix unavailable"
        if self == GPSQuality.FIXED_SPS_MODE:
            return "Valid position fix, SPS mode"
        if self == GPSQuality.FIXED_DIFFERENTIAL_MODE:
            return "Valid position fix, differential GPS mode"
        if self == GPSQuality.FIXED_GPS_PPS_MODE:
            return "GPS PPS Mode, fix valid"
        if self == GPSQuality.RTK :
            return "Real Time Kinematic. System used in RTK mode with fixed integers"
        if self == GPSQuality.FLOAT_RTK:
            return "Float RTK. Satellite system used in RTK mode. Floating integers"
        if self == GPSQuality.ESTIMATED_MODE:
            return "Estimated (dead reckoning) Mode"
        if self == GPSQuality.MANUAL_MODE:
            return "Manual Input Mode"


class GPS(graphene.ObjectType):
    latitude = graphene.Float(description="The last reported latitude value from the GPS")
    longitude = graphene.Float(description="The last reported longitude value from the GPS")
    altitude = graphene.Float(description="The last reported altitude value from the GPS")
    num_sats = graphene.Int(description="The number of GPS satellites currently locked on")
    quality = graphene.Field(GPSQuality, description="GPS quality indicator")
    fix_time = DateTime(description="Time of last GPS report in UTC")
    is_responding = graphene.Boolean(description="Indicates of we are able to query the GPS")
    using_fallback = graphene.Boolean(description="Indicates that we are using the configured fallback coordinates")
    system_clock_questionable = graphene.Boolean(description="Indicates that system clock differs noticeably from GPS clock")
