"""
asset_updater.py - This module handles part of the GraphQL integration for AssetUpdater.py
There is more of the implementation over in graphql_handlers.py
"""

import graphene
from graphene.types.resolver import dict_resolver
from file_upload import *


class AssetUpdaterConfigFields(graphene.AbstractType):
    do_upload_bridge_script = graphene.Boolean(description="Enable upload of SNAPpy scripts into Network Controller Bridge")
    do_upload_bridge_firmware = graphene.Boolean(description="Enable upload of SNAP firmware into Network Controller Bridge")
    do_upload_asset_scripts = graphene.Boolean(description="Enable upload of SNAPpy scripts into Assets")
    do_upload_asset_radio_firmware = graphene.Boolean(description="Enable upload of SNAP firmware into Asset radios")
    do_upload_asset_stm32_firmware = graphene.Boolean(description="Enable upload of STM32 firmware into Assets")
    do_upload_asset_configs = graphene.Boolean(description="Enable upload of configuration settings into Assets")
    configured_bridge_script_name = graphene.String(description="The bridge SNAPpy script name (that will be loaded)")
    configured_bridge_script_crc = graphene.Int(description="The bridge script CRC")
    configured_bridge_radio_firmware_filename = graphene.String(description="The bridge SNAP firmware name (that will be loaded)")
    configured_bridge_radio_firmware_version = graphene.String(description="The bridge SNAP firmware version")
    configured_asset_script_name = graphene.String(description="The SNAPpy script that will be loaded into Assets")
    configured_asset_script_crc = graphene.Int(description="The asset script CRC")
    configured_asset_radio_firmware_filename = graphene.String(description="The asset SNAP firmware name (that will be loaded)")
    configured_asset_radio_firmware_version = graphene.String(description="The asset SNAP firmware version")
    configured_asset_stm32_firmware_filename = graphene.String(description="The asset STM32 firmware name (that will be loaded)")
    configured_asset_stm32_firmware_version = graphene.String(description="The asset STM32 firmware version")
    upload_batch_sizes = graphene.List(graphene.Int, description="How many units to attempt simultaneous upload to (indexed by type of upload)")
    upload_retries = graphene.List(graphene.Int, description="How many retries to use per packet when uploading (indexed by type of upload)")
    upload_polls_required = graphene.List(graphene.Int, description="How many polls to require before uploading (indexed by type of upload)")

class AssetUpdaterConfig(graphene.ObjectType, AssetUpdaterConfigFields):
    pass


# The above is enough to allow QUERYING the fields.
# This next chunk is to allow user update of the "config" portion.

class AssetUpdaterConfigInput(graphene.InputObjectType, AssetUpdaterConfigFields):
    pass


class UpdateAssetUpdaterConfig(graphene.Mutation):
    class Input:
        configData = AssetUpdaterConfigInput()
        bridgeScriptFile = FileUpload()
        bridgeFirmwareFile = FileUpload()
        assetScriptFile = FileUpload()
        assetRadioFirmwareFile = FileUpload()
        assetStm32FirmwareFile = FileUpload()

    class Meta:
        # If we don't do the following, the default resolver
        # will be the attr_resolver.
        default_resolver = dict_resolver

    # The following says we are going to be returned something CONTAINING
    # a field named "asset_updater" that is transformable into a
    # AssetUpdaterConfig object
    asset_updater = graphene.Field(AssetUpdaterConfig)

    # The following is to catch usage errors. The real implementation is
    # over in graphql_handlers.py
    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError
