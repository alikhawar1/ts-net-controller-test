"""
mesh_info.py - integration of SOME of the information gathered by the AssetPoller
into GraphQL. See also bridge.py and BridgeMonitor.py.
"""
import graphene
from graphene.types.datetime import DateTime


class MeshInfo(graphene.ObjectType):
    assets_reporting = graphene.Int(description="The number of assets reporting in")
    total_assets = graphene.Int(description="The total number of assets that could report in")
    latest_poll = DateTime(description="The timestamp of the latest asset poll")
    latest_response = DateTime(description="The timestamp of the latest asset response")
    max_mesh_depth = graphene.Int(description="The maximum mesh depth reported by any asset")
    avg_mesh_depth = graphene.Float(description="The average of all reported mesh depths")
