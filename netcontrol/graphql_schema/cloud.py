"""
cloud.py - this Python module is defining the GraphQL Schema
for interacting with the "cloud connectivity" code, but it does
not provide the actual implementation. For that, see graphql_handlers.py.
"""

import graphene
from graphene.types.datetime import DateTime
from graphene.types.resolver import dict_resolver


# When we talk about "Config" fields, we mean user settable.
class CloudConfigFields(graphene.AbstractType):
    wind_reporting_close_percentage = graphene.Int(description="What percentage of Wind Stow threshold to use as Increased Reporting threshold")
    wind_reporting_close_interval = graphene.Int(description="Reporting interval (in minutes) when close to Wind Stow threshold")
    wind_reporting_over_interval = graphene.Int(description="Reporting interval (in minutes) when at or above the Wind Stow threshold")
    snow_reporting_close_percentage = graphene.Int(description="What percentage of Snow Stow threshold to use as Increased Reporting threshold")
    snow_reporting_close_interval = graphene.Int(description="Reporting interval (in minutes) when close to Snow Stow threshold")
    snow_reporting_over_interval = graphene.Int(description="Reporting interval (in minutes) when at or above the Snow Stow threshold")
    increased_weather_reporting_timeout = graphene.Int(description="Reporting interval (in minutes) when below wind threshold")


class Cloud(graphene.ObjectType, CloudConfigFields):
    enabled = graphene.Boolean(description="Is cloud connectivity enabled")
    connected = graphene.Boolean(description="Is cloud connectivity working")
    last_publish = DateTime(description="The timestamp of the latest data push to the cloud")
    last_publish_response = DateTime(description="The timestamp of the latest push response from the cloud")


# The above is enough to allow QUERYING the fields.
# This next chunk is to allow user update of the "config" portion.

class CloudConfigInput(graphene.InputObjectType, CloudConfigFields):
    pass


class UpdateCloudConfig(graphene.Mutation):
    class Input:
        configData = CloudConfigInput()

    class Meta:
        # If we don't do the following, the default resolver
        # will be the attr_resolver.
        default_resolver = dict_resolver

    # The following says we are going to be returned something CONTAINING
    # a field named "cloud" that IS transformable into a Cloud object
    cloud = graphene.Field(Cloud)

    # The following is to catch usage errors. The real implementation is
    # over in graphql_handlers.py
    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError
