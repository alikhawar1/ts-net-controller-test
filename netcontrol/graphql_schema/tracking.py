import graphene
from graphene.types.resolver import dict_resolver

from netcontrol import asset_commander


class TrackingStatus(graphene.ObjectType):
    class Meta:
            default_resolver = dict_resolver

    calc_panel_angle = graphene.Float(description="The current solar panel elevation angle if tracking")
    commanded_state = graphene.Field(graphene.Enum.from_enum(asset_commander.CommandedPanelState), description="The state being commanded to the solar panels")
