"""
bridge.py - this Python module is defining the GraphQL Schema
for interacting with BridgeMonitor.py, but it does not provide
the actual implementation. For that, see graphql_handlers.py.
"""

import graphene
from graphene.types.resolver import dict_resolver


# When we talk about "Config" fields, we mean user settable.
class BridgeConfigFields(graphene.AbstractType):
    bridge_channel = graphene.Int(description="The bridge channel")
    bridge_network_id = graphene.Int(description="The bridge network ID")
    bridge_key = graphene.String(description="The bridge encryption key")


class Bridge(graphene.ObjectType, BridgeConfigFields):
    bridge_address = graphene.String(description="The bridge SNAP Address")
    bridge_version = graphene.String(description="The bridge module type and firmware version combined in a string")
    bridge_script_name = graphene.String(description="The bridge SNAPpy script name (if one is loaded)")
    bridge_script_version = graphene.String(description="The bridge radio script version (if one is loaded)")
    bridge_script_crc = graphene.Int(description="The bridge script CRC")


# The above is enough to allow QUERYING the fields.
# This next chunk is to allow user update of the "config" portion.

class BridgeConfigInput(graphene.InputObjectType, BridgeConfigFields):
    pass


class UpdateBridgeConfig(graphene.Mutation):
    class Input:
        configData = BridgeConfigInput()

    class Meta:
        # If we don't do the following, the default resolver
        # will be the attr_resolver.
        default_resolver = dict_resolver

    # The following says we are going to be returned something CONTAINING
    # a field named "bridge" that IS transformable into a Bridge object
    bridge = graphene.Field(Bridge)

    # The following is to catch usage errors. The real implementation is
    # over in graphql_handlers.py
    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError
