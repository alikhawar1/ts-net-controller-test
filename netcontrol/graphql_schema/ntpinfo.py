"""
ntpinfo.py - this Python module is defining the GraphQL Schema
for interacting with the NtpMonitor code, but it does not provide
the actual implementation. For that, see graphql_handlers.py.
"""

import graphene


class NtpInfo(graphene.ObjectType):
    time_synchronized = graphene.Boolean(description="Are we synchronized with a NTP server")
    time_server = graphene.String(description="If so, which one")
    time_offset = graphene.Float(description="How closely are we synchronized to the NTP server")
