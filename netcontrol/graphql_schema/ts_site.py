import graphene
from graphene.types.datetime import DateTime
from graphene.types.resolver import dict_resolver

import event_log
import row_controller
import gps
import modem
import bridge
import mesh_info
import ethernet
import weather_monitor
import asset_updater
import tracking
import cloud
import ntpinfo
import debug_mutations

from netcontrol import controller_enums

from file_upload import *

class SiteConfigFields(graphene.AbstractType):
    site_name = graphene.String(description="This site's descriptive name")
    site_contact = graphene.String()
    site_organization = graphene.String()
    gps_lat = graphene.Float()
    gps_lng = graphene.Float()
    gps_alt = graphene.Float()
    enable_nightly_shutdown = graphene.Boolean()
    power_off = graphene.Int()
    power_on = graphene.Int()
    has_weather = graphene.Boolean()

    operational_mode = graphene.Field(graphene.Enum.from_enum(controller_enums.TrackerPresets))
    tracking_enable = graphene.Boolean()
    backtracking_enable = graphene.Boolean()

    ntp = graphene.String()

    site_image = graphene.String()
    site_image_ul_lat = graphene.Float()
    site_image_ul_lng = graphene.Float()
    site_image_ll_lat = graphene.Float()
    site_image_ll_lng = graphene.Float()
    engineering_spec = DateTime()

    software_version = graphene.String(description="Version of the composite NC/SLUI software")

    gateway_platform = graphene.String(description="Platform (type of Operating System and computer)")
    gateway_os_version = graphene.String(description="Version of the NC's Operating System")
    gateway_os_build = graphene.String(description="Details about when the NC Operating system was built")

    uptime = graphene.Int(description="Total amount of time in seconds this NC has been running")

    enable_low_power_shutdown = graphene.Boolean(description="Enables voluntary shut-downs in low power scenarios")
    cell_modem_warning_voltage = graphene.Float(description="Voltage at which to start warning of potential cell modem cutoff")
    cell_modem_cutoff_voltage = graphene.Float(description="Voltage at which to voluntarily cutoff cell modem to conserve battery power")
    cell_modem_cuton_voltage = graphene.Float(description="Voltage at which to restore cell modem power after a voluntarily cutoff")
    gateway_warning_voltage = graphene.Float(description="Voltage at which to start warning of potential voluntary controller cutoff")
    gateway_cutoff_voltage = graphene.Float(description="Voltage at which to voluntarily cutoff controller, try again next morning")

class SiteStatusFields(graphene.AbstractType):
    cell_modem_power_low = graphene.Boolean(description="True when at risk for cell modem shutdown due to available power")
    cell_modem_powered_off = graphene.Boolean(description="True when cell modem has been switched off due to available power")
    gateway_power_low = graphene.Boolean(description="True when at risk for controller shutdown due to available power")

class EmergencyStopStatus(graphene.ObjectType):
    in_estop = graphene.Boolean()
    estop_is_pressed = graphene.Boolean()
    estop_test_passed = graphene.Boolean()
    estop_test_remaining = graphene.Int()


class EmergencyStopTest(graphene.Mutation):
    class Input:
        estop_test_start = graphene.Int()

    class Meta:
            default_resolver = dict_resolver

    emergency_stop = graphene.Field(EmergencyStopStatus)

    @staticmethod
    def mutate(*args):
        raise NotImplementedError


class SiteConfig(graphene.ObjectType, SiteConfigFields):
    pass


class SiteConfigInput(graphene.InputObjectType, SiteConfigFields):
    pass


class UpdateSiteConfig(graphene.Mutation):
    class Input:
        configData = SiteConfigInput()
        siteImageFile = FileUpload()
        engineeringSpec = FileUpload()

    config = graphene.Field(SiteConfig)

    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError


class SiteStatus(graphene.ObjectType, SiteStatusFields):
    pass


class ActivateDiscoveredAsset(graphene.Mutation):
    class Input:
        ids = graphene.List(graphene.ID)

    class Meta:
        default_resolver = dict_resolver

    activated = graphene.List(graphene.ID)

    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError


class DeactivateActiveAsset(graphene.Mutation):
    class Input:
        ids = graphene.List(graphene.ID)

    class Meta:
        default_resolver = dict_resolver

    deactivated = graphene.List(graphene.ID)

    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError


class DeleteDiscoveredAsset(graphene.Mutation):
    class Input:
        ids = graphene.List(graphene.ID)

    class Meta:
        default_resolver = dict_resolver

    deleted = graphene.List(graphene.ID)

    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError


class GetLogFromAsset(graphene.Mutation):
    class Input:
        ids = graphene.List(graphene.ID)

    class Meta:
        default_resolver = dict_resolver

    requested = graphene.List(graphene.ID)

    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError


class ClearFaults(graphene.Mutation):
    class Input:
        ids = graphene.List(graphene.ID)

    class Meta:
        default_resolver = dict_resolver

    requested = graphene.List(graphene.ID)

    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError


class ForceUpload(graphene.Mutation):
    class Input:
        ids = graphene.List(graphene.ID)
        uploadType = graphene.Int()

    class Meta:
        default_resolver = dict_resolver

    requested = graphene.List(graphene.ID)

    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError


class SystemUpgrade(graphene.Mutation):
    class Input:
        upgradeFile = FileUpload()

    received = graphene.Boolean()

    class Meta:
        default_resolver = dict_resolver

    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError


class ConfigImport(graphene.Mutation):
    class Input:
        configFile = FileUpload()

    received = graphene.Boolean()

    class Meta:
        default_resolver = dict_resolver

    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError


class ShutDownWifi(graphene.Mutation):
    class Input:
        dummy = graphene.Int()

    shutdown = graphene.Boolean()

    class Meta:
        default_resolver = dict_resolver

    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError


class CancelSnowStow(graphene.Mutation):
    class Input:
        dummy = graphene.Int()

    cancelled = graphene.Boolean()

    class Meta:
        default_resolver = dict_resolver

    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError


class TCSite(graphene.ObjectType):
    config = graphene.Field(SiteConfig)
    system_status = graphene.Field(SiteStatus)
    event_log = graphene.Field(event_log.EventLog)
    assets = graphene.List(row_controller.TCAsset,
                           id=graphene.Argument(graphene.ID),
                           has_weather_sensor=graphene.Argument(graphene.Boolean))
    discovered_assets = graphene.List(row_controller.TCAsset,
                                      id=graphene.Argument(graphene.ID))
    gps = graphene.Field(gps.GPS)
    cell_modem = graphene.Field(modem.TelitCellModem)
    bridge = graphene.Field(bridge.Bridge)
    mesh = graphene.Field(mesh_info.MeshInfo)
    ethernet = graphene.Field(ethernet.Ethernet)
    weather_monitor = graphene.Field(weather_monitor.WeatherMonitorConfig)
    asset_updater = graphene.Field(asset_updater.AssetUpdaterConfig)
    emergency_stop = graphene.Field(EmergencyStopStatus)
    tracking = graphene.Field(tracking.TrackingStatus)
    cloud = graphene.Field(cloud.Cloud)
    ntpinfo = graphene.Field(ntpinfo.NtpInfo)


class TSSiteMutations(graphene.ObjectType):
    clear_log_entries = event_log.ClearEventLogEntries.Field()
    update_config = UpdateSiteConfig.Field()
    activate_discovered_asset = ActivateDiscoveredAsset.Field()
    deactivate_active_asset = DeactivateActiveAsset.Field()
    delete_discovered_asset = DeleteDiscoveredAsset.Field()
    get_log_from_asset = GetLogFromAsset.Field()
    clear_faults = ClearFaults.Field()
    force_upload = ForceUpload.Field()
    emergency_stop = EmergencyStopTest.Field()
    update_bridge_config = bridge.UpdateBridgeConfig.Field()
    update_ethernet_config = ethernet.UpdateEthernetConfig.Field()
    update_weather_monitor_config = weather_monitor.UpdateWeatherMonitorConfig.Field()
    update_asset_updater_config = asset_updater.UpdateAssetUpdaterConfig.Field()
    update_cloud_config = cloud.UpdateCloudConfig.Field()
    cancel_snow_stow = CancelSnowStow.Field()
    upgrade = SystemUpgrade.Field()
    import_config = ConfigImport.Field()
    shut_down_wifi = ShutDownWifi.Field()
    debug = debug_mutations.DebugHelpers.Field()
