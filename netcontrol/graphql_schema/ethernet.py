"""
ethernet.py - this Python module is defining the GraphQL Schema
for interacting with Ethernet.py, but it does not provide
the actual implementation. For that, see graphql_handlers.py.
"""

import graphene
from graphene.types.resolver import dict_resolver


# When we talk about "Config" fields, we mean user settable.
class EthernetConfigFields(graphene.AbstractType):
    enable_static_ip_address = graphene.Boolean(description="Enable use of a second, static IP Address")
    requested_static_ip_address = graphene.String(description="The desired static IP Address (when enabled)")
    requested_static_subnet_mask = graphene.String(description="The desired static subnet mask (when enabled)")


# the composite object is then the combination of the settable fields and the live/dynamic fields
class Ethernet(graphene.ObjectType, EthernetConfigFields):
    dhcp_ip_address = graphene.String(description="IP Address selected via DHCP (when present)")
    dhcp_subnet_mask = graphene.String(description="Associated Subnet Mask via DHCP (when present)")
    static_ip_address = graphene.String(description="Static IP Address (when configured)")



# The above is enough to allow QUERYING the fields.
# This next chunk is to allow user update of the "config" portion.

class EthernetConfigInput(graphene.InputObjectType, EthernetConfigFields):
    pass


class UpdateEthernetConfig(graphene.Mutation):
    class Input:
        configData = EthernetConfigInput()

    class Meta:
        # If we don't do the following, the default resolver
        # will be the attr_resolver.
        default_resolver = dict_resolver

    # The following says we are going to be returned something CONTAINING
    # a field named "ethernet" that IS transformable into an Ethernet object
    ethernet = graphene.Field(Ethernet)

    # The following is to catch usage errors. The real implementation is
    # over in graphql_handlers.py
    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError
