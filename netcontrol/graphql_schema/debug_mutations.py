import graphene
from graphene.types.resolver import dict_resolver


class AssetResponse(graphene.InputObjectType):
    """
    Trigger a node response
    """
    response = graphene.String()
    addr = graphene.String(required=True)


class UpdateParam(graphene.InputObjectType):
    """
    A parameter to update
    """
    name = graphene.String(required=True)
    int_value = graphene.Int()
    str_value = graphene.String()


class UpdateAsset(graphene.InputObjectType):
    """
    Update an asset object parameter
    """
    addr = graphene.String(required=True)
    params = graphene.List(UpdateParam, required=True)


class DebugHelpers(graphene.Mutation):
    class Input:
        asset_response = graphene.Argument(AssetResponse)
        update_asset = graphene.Argument(UpdateAsset)

    class Meta:
        default_resolver = dict_resolver

    success = graphene.Boolean()

    @staticmethod
    def mutate(*args):
        raise NotImplementedError
