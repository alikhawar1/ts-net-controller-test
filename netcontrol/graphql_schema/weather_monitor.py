"""
weather_monitor.py - This module handles GraphQL integration for WeatherMonitor.py
"""

import graphene
from graphene.types.resolver import dict_resolver


class WeatherMonitorConfigFields(graphene.AbstractType):
    trigger_averaging = graphene.Boolean(description="Enabled use of averaged versus instantaneous values")

    enable_wind_speed_stow = graphene.Boolean(description="Enable one-minute averages as stow trigger")
    wind_speed_threshold = graphene.Float(description="Wind speed threshold in MPH to enable auto-stow timer")
    wind_speed_duration_required = graphene.Int(description="Seconds of continuous threshold crossing required to trigger auto-stow")

    enable_wind_gust_stow = graphene.Boolean(description="Enable 1-hour gusts as stow trigger")
    wind_gust_threshold = graphene.Float(description="Gust threshold in MPH to trigger auto-stow")

    enable_snow_depth_stow = graphene.Boolean(description="Enable ground snow depth as stow trigger")
    enable_snow_depth_averaging = graphene.Boolean(description="Enable use of average ground snow depth instead of peak")
    snow_depth_threshold = graphene.Float(description="Ground Snow Depth threshold in Meters to trigger auto-stow")
    snow_depth_low_threshold = graphene.Float(description="Ground Snow Depth low threshold in Meters to trigger auto-stow")
    enable_auto_resume_tracking_snow = graphene.Boolean(description="Enable use of auto resume tracking based on low threshold value")

    enable_panel_snow_depth_stow = graphene.Boolean(description="Enable panel snow depth as stow trigger")
    enable_panel_snow_depth_averaging = graphene.Boolean(description="Enable use of average panel snow depth instead of peak")
    panel_snow_depth_threshold = graphene.Float(description="Panel Snow Depth threshold in Meters to trigger auto-stow")
    panel_snow_depth_low_threshold = graphene.Float(description="Panel Snow Depth Low threshold in Meters to trigger auto-stow")
    enable_auto_resume_tracking_panel_snow = graphene.Boolean(description="Enable use of auto resume tracking based on low threshold value")

    resume_tracking_after_wind_timeout = graphene.Int(description="Seconds of non-stow wind conditions required to resume tracking")
    resume_tracking_after_snow_timeout = graphene.Int(description="Seconds of non-stow ground snow conditions required to resume tracking")
    resume_tracking_after_panel_snow_timeout = graphene.Int(description="Seconds of non-stow panel snow conditions required to resume tracking")

    minimum_stations_required = graphene.Int(description="Minimum number of weather sources required to allow tracking")

    stations_reporting = graphene.Int(description="Number of weather sources currently reporting in")
    total_stations = graphene.Int(description="Count of known weather sources")

#
# The following might appear unnecessary. However, when I added a GraphQL
# mutation to this code, I fell into "meta class h*ll". Since there was a
# high probability of WeatherMonitor gaining non-config data, the shortest
# path seemed to be mimicing what was done over in bridge.py.
#
class WeatherMonitorConfig(graphene.ObjectType, WeatherMonitorConfigFields):
    pass


# The above is enough to allow QUERYING the fields.
# This next chunk is to allow user update of the "config" portion.

class WeatherMonitorConfigInput(graphene.InputObjectType, WeatherMonitorConfigFields):
    pass


class UpdateWeatherMonitorConfig(graphene.Mutation):
    class Input:
        configData = WeatherMonitorConfigInput()

    class Meta:
        # If we don't do the following, the default resolver
        # will be the attr_resolver.
        default_resolver = dict_resolver

    # The following says we are going to be returned something CONTAINING
    # a field named "weather_monitor" that is transformable into a
    # WeatherMonitorConfig object
    weather_monitor = graphene.Field(WeatherMonitorConfig)

    # The following is to catch usage errors. The real implementation is
    # over in graphql_handlers.py
    @staticmethod
    def mutate(root, args, context, info):
        raise NotImplementedError
