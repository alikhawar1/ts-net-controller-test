import graphene


class TelitCellModem(graphene.ObjectType):
    rssi_dbm = graphene.Int(description="The received signal strength indication in dBm")
    imei = graphene.String(description="The radio IMEI number")
    roaming = graphene.Boolean(description="Indicates whether or not the radio is using the home network")
    mdn = graphene.String(description="The mobile directory number")
    lan_ip = graphene.String(description="The IP address assigned to the cellular modem interface")
    wan_ip = graphene.String(description="The externally facing ip address of the cellular modem")
    link_status = graphene.String(description="Status of the PPP link")
    uptime = graphene.Int(description="Number of minutes connection has been established")
    tx_data_usage = graphene.Int(description="The number of bytes received")
    rx_data_usage = graphene.Int(description="The number of bytes transmitted")
    tower_id = graphene.String(description="The tower identification number")
    is_responding = graphene.Boolean(description="Indicates of we are able to query the modem status")
