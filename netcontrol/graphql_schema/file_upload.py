import graphene

class FileUpload(graphene.InputObjectType):
    body = graphene.String(required=True)
    content_type = graphene.String(required=True, name='content_type')  # Don't camel case this property name
    filename = graphene.String(required=True)
