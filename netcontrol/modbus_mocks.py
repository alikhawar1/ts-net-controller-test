import time
import datetime
import terrasmart_cloud_pb2

class StructMock:
    pass


def alert_mock(alert_type):
    alert = terrasmart_cloud_pb2.Alert()
    alert.when.FromDatetime(datetime.datetime(2019, 12, 1))
    alert.active = True
    alert.type = alert_type
    alert.message = "Alert message test type %d" % alert_type
    return alert


class SiteMock:
    """Site object and other base objects"""
    def __init__(self):
        # my_site
        self.site_name = "Test site name"
        self.site_organization = "Test org"
        self.site_contact = "Test contact"
        self.software_version = "1.0"
        self.has_weather = True
        self.tracking_enable = True
        self.backtracking_enable = True

        # e_stop_mon
        self.in_estop = False

        # sun_tracker
        self.current_angle = 12.345

        # asset_commander
        self.panel_state = StructMock()
        self.panel_state.value = 2  # NORMAL

        # asset_poller
        self.total_assets = 4
        self.assets_reporting = 4
        #self.latest_response = datetime.datetime.now()
        self.latest_response = datetime.datetime(2019, 12, 1)

        # bridge_monitor
        self.bridge_address = "0a.0b.0c"
        self.bridge_script_crc = 0x1234
        self.bridge_channel = 9
        self.bridge_network_id = 0x7e2a

        # gps_mon
        self.num_sats = 3
        #self.fix_time = datetime.datetime.now()
        self.fix_time = datetime.datetime(2019, 12, 1)
        self.gps_latitude = 36.6431621
        self.gps_longitude = -116.397459

        # ntp_monitor
        self.time_synchronized = True
        self.time_server = 'time.tick.org'
        self.time_offset = 0.54321

        # active_alerts
        self._active_alerts = {
            terrasmart_cloud_pb2.Alert.ESTOP : alert_mock(terrasmart_cloud_pb2.Alert.ESTOP),
            terrasmart_cloud_pb2.Alert.ASSETS_REPORTING : alert_mock(terrasmart_cloud_pb2.Alert.ASSETS_REPORTING),
            terrasmart_cloud_pb2.Alert.GPS : alert_mock(terrasmart_cloud_pb2.Alert.GPS),
            terrasmart_cloud_pb2.Alert.CLOCK : alert_mock(terrasmart_cloud_pb2.Alert.CLOCK),
            terrasmart_cloud_pb2.Alert.MODEM : alert_mock(terrasmart_cloud_pb2.Alert.MODEM),
            terrasmart_cloud_pb2.Alert.NTP : alert_mock(terrasmart_cloud_pb2.Alert.NTP),
            terrasmart_cloud_pb2.Alert.UNKNOWN : alert_mock(terrasmart_cloud_pb2.Alert.UNKNOWN),
            terrasmart_cloud_pb2.Alert.UNKNOWN : alert_mock(terrasmart_cloud_pb2.Alert.UNKNOWN),
        }

    @property
    def uptime(self):
        #return int(time.clock())
        return 0x0102030405060708

class SiteMock2:
    """Second instance to handle naming collisions"""
    def __init__(self):
        # modem_mon
        self.ppp_up = True
        self.rssi_dbm = -85
        self.wan_ip = '24.1.2.3'
        self.lan_ip = '10.0.1.2'
        self.uptime = 60 * 60 * 24 * 365


class AssetMock:
    def __init__(self, num, device_type, has_wx, has_tracker):
        self.num = num  # PARAM

        # Device type
        self.device = device_type  # PARAM
        # System
        self.unit_temperature = 98.6
        #self.last_reported = datetime.datetime.now()
        self.last_reported = datetime.datetime(2019, 12, 1)
        self.model = "Model-" + chr(ord('A') + num)
        self.hardware_rev = 'HW v1.' + str(num)
        self.firmware_rev_string = 'FW v1.0'
        self.uptime = 25 * 365 * 24 * 3600  # 788,400,000
        self.snap_address = "\xAD\xD0" + chr(num)
        self.radio_link_quality = -85
        self.has_weather_sensor = has_wx  # PARAM
        self.has_tracker_hardware = has_tracker # PARAM
        self.location_text = "The Swamp"
        self.location_lat = 36.6431621
        self.location_lng = -116.397459
        self.config_flags = 0x01020304
        # Power
        self.battery_voltage = 18.5
        self.battery_current = 1.0
        self.battery_charged = 99
        self.battery_health = 99
        self.charger_voltage = 22.0
        self.charger_current = 0.5
        self.solar_voltage = 28.8
        self.solar_current = 0.88
        # Tracker
        self.status_bits = 0x00000000
        self.tracking_status_bits = 0x0001
        self.current_angle = 23.4
        self.requested_angle = 12.5
        self.motor_power_previous_hour = 5.6
        self.average_angular_error_previous_hour = 0.5
        # WX
        self.wind_speed = 2.3
        self.wind_direction = 45.6
        self.average_wind_speed = 2.0
        self.peak_wind_speed = 5.5
        self.snow_sensor_temperature = 1.2
        self.snow_sensor_distance = 3.4
        self.snow_depth = 5.6
        

class AssetManagerMock:
    def __init__(self):
        self.assets = {
            0 : AssetMock(0, 'TC', False, True),
            1 : AssetMock(1, 'TC', False, True),
            2 : AssetMock(2, 'NCCB', False, False),
            3 : AssetMock(3, 'TC', False, True),
            4 : AssetMock(4, 'WX', True, False),
        }

        self.nccb = AssetMock(0, 'NCCB', False, False)


    def get_serial_asset(self):
        return self.nccb 


site = SiteMock()
site2 = SiteMock2()
my_site = site
e_stop_mon = site
sun_tracker = site
asset_commander = site
asset_poller = site
bridge_monitor = site
gps_mon = site
modem_mon = site2
ntp_monitor = site
cloud_updater = site

asset_mgr = AssetManagerMock()
