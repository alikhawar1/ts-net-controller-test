"""
log_retriever.py - defines the LogRetriever class, whose job it is to
extract log data fom Assets. The log data gets written to a file.
"""

import logging
import os
import binascii
from datetime import datetime

from snapconnect import snap
from FormatHelpers import *

from PauseButtons import PauseButtonManager, PauseButton
from CommunicationsPriorities import *
from controller_enums import EventType

from utils.SystemSettings import SYSTEM_CONFIG_DIRECTORY

class LogRetriever(object):
    """
    This object does on-demand extraction of Log Data from Assets.
    """

    # TODO TUNE THESE!!! TODO
    REPLY_SPACING = 50  # copied from some similar code
    LOG_TIMEOUT = 2  # copied from some similar code (AssetMover)
    LOG_RETRIES = 10  # this is AFTER initial (unconditional) try ex. 1+2=3 attempt total

    def __init__(self, snap_connect, nccb_adapter, schedule_func, snap_bridge_address_func, max_hops, group=0x8000):
        """
        Constructor. Parameters include:
        snap_connect - the object we use to do SNAP communications
        nccb_adapter - the object we use to send the same communications to the NCCB (over USB serial)
        schedule_func - used for timing
        max_hops - controls the Time To Live (TTL) of the outbound messages
        group - specifies which multicast group to use (0x0001 = Broadcast, others are application-specific)
        """
        self._snap_connect = snap_connect
        self._nccb_adapter = nccb_adapter
        self._schedule_func = schedule_func
        self.get_snap_bridge_address = snap_bridge_address_func
        self.max_hops = max_hops
        self.group = group

        self._pacing = None
        self._scheduled_timeout = None

        # log_info() is the response to a log_query()
        self._snap_connect.add_rpc_func('log_info', self.log_info)
        # log_data() is the response to a log_read()
        self._snap_connect.add_rpc_func('log_data', self.log_data)

        self.target_address = None
        self.queued_targets = set()

        pause_button_manager = PauseButtonManager.get_default_instance()
        self._pause_button = pause_button_manager.get_pause_button(LOG_RETRIEVER_PRIORITY)


    def custom_format_snap_address(self, snap_address):
        if snap_address == 'serial':
            return 'NCCB ' + format_snap_address(self.get_snap_bridge_address())
        else:
            return 'Asset ' + format_snap_address(snap_address)


    # Helper function to reduce duplicated code. The deal here is the STM32 on the NCCB
    # currently can only be accessed via the USB serial interface. All other STM32s we
    # access via SNAP radio.
    def _send_rpc(self, target_address, group, max_hops, reply_spacing, func_name, *args):
        if target_address == 'serial':
            self._nccb_adapter.send_mcast_rpc(group, max_hops, func_name, *args)
        else:
            self._snap_connect.dmcast_rpc(target_address, group, max_hops, reply_spacing, func_name, *args)

    # This is our GraphQL tie-in point. SLUI user did XXX, this got called.
    # Also expect to hit this from the cloud UI at some point.
    def retrieve_logs(self, asset_ids):
        for asset_id in asset_ids:
            # Treating NCCB as a special case
            if asset_id == 'serial':
                self.retrieve_log('serial')
            else:
                snap_addr = binascii.unhexlify(asset_id)
                self.retrieve_log(snap_addr)

        return asset_ids

    def retrieve_log(self, snap_addr):
        """Try to retrieve a log file from the specified asset"""
        # Gracefully handle new requests made before previous request is completed
        if self.target_address is None:
            self.target_address = snap_addr
            # Disconnect the actual log retrieval from the UI
            self._pacing = self._schedule_func(1, self.request_log_retrieval)
        else:
            self.queued_targets.add(snap_addr)

    def request_log_retrieval(self):
        self._pacing.stop()

        if self._pause_button.paused:
            self._pacing = self._schedule_func(60, self.request_log_retrieval)
        else:
            if self._pause_button.attempt_to_pause_others():
                # Give everyone a chance to gracefully finish pausing before beginning the actual work
                self._pacing = self._schedule_func(5, self.start_log_retrieval)
            else:
                # Try again in a bit
                self._pacing = self._schedule_func(1, self.request_log_retrieval)

    def build_log_file_name(self):
        if self.target_address == 'serial':
            identity = binascii.hexlify(self.get_snap_bridge_address())
        else:
            identity = binascii.hexlify(self.target_address)
        timestamp = datetime.now().strftime('%Y%m%d_%H%M')
        filename = identity + '_' + timestamp + '.log'
        return filename

    def start_log_retrieval(self):
        self._pacing.stop()

        # Not sure which is worse - pausing the logging, or transferring file while it is changing...
        self._send_rpc(self.target_address, self.group, self.max_hops, LogRetriever.REPLY_SPACING, 'log_pause')

        msg = 'Starting Log File retrieval from ' + self.custom_format_snap_address(self.target_address)
        logging.getLogger('EventLog').log(logging.ERROR, msg, extra=EventType.INDIVIDUAL_ASSET._dict)

        # Open log file on disk to capture the retrieved log data
        self.log_file_name = os.path.join(SYSTEM_CONFIG_DIRECTORY, self.build_log_file_name())
        self.log_file = open(self.log_file_name, 'wb')

        self.current_offset = -1l
        self.retries = LogRetriever.LOG_RETRIES

        self.request_next_chunk()

    def request_next_chunk(self):
        # The original "single blob" approach was changed to a "two blob" approach.
        # This way we are not sending so much redundant data...
        if self.current_offset == -1: # we need to get the metadata first
            self._send_rpc(self.target_address, self.group, self.max_hops, LogRetriever.REPLY_SPACING, 'log_pause')
            self._send_rpc(self.target_address, self.group, self.max_hops, LogRetriever.REPLY_SPACING, 'log_query')
        else: # now we can get the actual log data
            # Convert our long-int offset into a "byte-string" that SNAPpy can handle
            offset_string = ''
            temp_offset = self.current_offset
            count = 8
            while count > 1:
                offset_string = chr(temp_offset & 0xFF) + offset_string
                temp_offset >>= 8
                count -= 1
            offset_string = chr(temp_offset & 0xFF) + offset_string
            self._send_rpc(self.target_address, self.group, self.max_hops, LogRetriever.REPLY_SPACING, 'log_read', offset_string)

        self._scheduled_timeout = self._schedule_func(LogRetriever.LOG_TIMEOUT, self.log_retrieval_timeout)

    def extract_long(self, str):
        result = 0l
        if len(str) == 8:
            while len(str) > 0:
                result <<= 8l
                result += ord(str[0])
                str = str[1:]
        return result

    # log_query() has a hardcoded callback of "log_info()"
    def log_info(self, blob_m):
        snap_address = self._snap_connect.rpc_source_addr()
        if snap_address == self.target_address:
            # We have a blob of info. Break it down
            # Note that if any of the message is bad, we essentially ignore
            # it and let the normal "timeout" handling take care of things...
            # The BLOB_M starts with an 'm' and then the BLOB version number,
            # then it has 8-byte log_size, log_write_offset, and settings.
            if len(blob_m) != 2 + 8 + 8 + 8:
                return

            if blob_m[0] != 'm':
                return
            if blob_m[1] != chr(0):
                return
            blob_m = blob_m[2:] # just to make the offsets easier in the followng code

            self.log_size = self.extract_long(blob_m[0:8])

            if self.log_size == 0:
                msg = 'Asset ' + self.custom_format_snap_address(self.target_address) + ' has no Log Storage, aborting'
                logging.getLogger('EventLog').log(logging.ERROR, msg, extra=EventType.INDIVIDUAL_ASSET._dict)
                self.done_retrieving_log()
                return

            self.log_write_offset = self.extract_long(blob_m[8:16])
            self.log_settings = self.extract_long(blob_m[16:24])

            self._scheduled_timeout.stop()
            self._scheduled_timeout = None

            self.current_offset = 0l
            self.retries = LogRetriever.LOG_RETRIES
            self.request_next_chunk()

    # log_read() has a hardcoded callback of "log_data()"
    def log_data(self, blob_l):
        snap_address = self._snap_connect.rpc_source_addr()
        if snap_address == self.target_address:
            # We have a blob of info. Break it down
            # Note that if any of the message is bad, we essentially ignore
            # it and let the normal "timeout" handling take care of things...
            # The BLOB_L starts with an 'l' and then the BLOB version number,
            # then it has the 8-byte log_chunk_offset, followed by 64 bytes
            # of actual data from the log (some of which might be 0x00)
            if len(blob_l) != 2 + 8 + 64:
                return

            if blob_l[0] != 'l':
                return
            if blob_l[1] != chr(0):
                return
            blob_l = blob_l[2:] # just to make the offsets easier in the followng code

            log_chunk_offset = self.extract_long(blob_l[0:8])

            if log_chunk_offset == self.current_offset:
                log_chunk = blob_l[8:]
                # Write this chunk of data to the file we started previously
                self.log_file.write(log_chunk)

                self._scheduled_timeout.stop()
                self._scheduled_timeout = None

                # Set up for next possible chunk, see if we are done
                done = False
                self.current_offset += 64l
                if self.current_offset >= self.log_size:
                    done = True

                # Optimization: Cut the process short if the Log is not full
                # (If it has not "wrapped around" yet)
                if "\x00\x00" in log_chunk:
                    done = True

                if done:
                    msg = 'Finished retrieving log from ' + self.custom_format_snap_address(self.target_address)
                    logging.getLogger('EventLog').log(logging.ERROR, msg, extra=EventType.INDIVIDUAL_ASSET._dict)
                    self.done_retrieving_log()
                else:
                    self.retries = LogRetriever.LOG_RETRIES
                    self.request_next_chunk()

    def log_retrieval_timeout(self):
        self._scheduled_timeout.stop()
        self._scheduled_timeout = None

        if self.retries > 0:
             self.retries -= 1
             self.request_next_chunk()
        else:
            if self.current_offset == -1l:
                msg = 'Unable to retrieve log metadata from ' + self.custom_format_snap_address(self.target_address)
            else:
                msg = 'Unable to retrieve full log from ' + self.custom_format_snap_address(self.target_address)
            logging.getLogger('EventLog').log(logging.ERROR, msg, extra=EventType.INDIVIDUAL_ASSET._dict)
            self.done_retrieving_log()

    def upload_log_to_S3(self, filename):
        # boto3 is the library we will use
        # TODO
        return False

    def resume_logging(self):
        self._send_rpc(self.target_address, self.group, self.max_hops, LogRetriever.REPLY_SPACING, 'log_resume')

    def done_retrieving_log(self):
        if self._scheduled_timeout is not None:
            self._scheduled_timeout.stop()
            self._scheduled_timeout = None

        self.resume_logging()

        # Close file (possibly empty)
        self.log_file.close()

        # Let the other system components make use of the wireless network again
        self._pause_button.unpause_others()

        self.resume_logging() # redundancy...

        # Send up to Amazon AWS S3
        if self.upload_log_to_S3(self.log_file_name):
            os.remove(self.log_file_name)

        self.resume_logging() # paranoia...

        self.target_address = None # Done with this one
        # Keep going if there are more queued-up requests...
        if len(self.queued_targets) != 0:
            self.retrieve_log(self.queued_targets.pop())
