import logging
import collections
import datetime
import itertools

import google.protobuf.message
from google.protobuf.internal.well_known_types import _NANOS_PER_SECOND

import terrasmart_cloud_pb2
from asset_commander import CommandedPanelState, AssetCommander
from Asset import TCTrackingStatusBits
import event_log
from utils.PickleHelper import PickleHelper

from controller_enums import EventType

log = logging.getLogger(__name__)

tc_angle_update = collections.namedtuple('tc_angle_update', ['current_angle', 'requested_angle', 'timestamp', 'commanded_state', 
                                            'tracking_status', 'commanded_state_detail', 'panel_command_state', 'panel_index', 'motor_current'])
tracking_change = collections.namedtuple('tracking_change', ['new_commanded_state', 'state_changed_at', 'commanded_preset'])
active_alert = collections.namedtuple('active_alert', ['when', 'msg'])

MANUAL_CONTROL_INDICATOR = 0x7fff

CLOUD_UPDATER_CONFIG_FILE = 'cloud_updater.pkl'

# The following are used with get_weather_threshold_trigger()
NO_TRIGGER = 0x00 # No need for Increased Reporting
AVG_WIND_CLOSE = 0x01
AVG_WIND_OVER = 0x02
SNOW_CLOSE = 0x04
SNOW_OVER = 0x08
WIND_GUST_CLOSE = 0x10
WIND_GUST_OVER = 0x20
PANEL_SNOW_CLOSE = 0x40
PANEL_SNOW_OVER = 0x80

class CloudUpdaterPersistableConfig(object):
    """
    The point of this class is to separate out the PERSISTED CONFIG
    fields from the remaining LIVE DATA fields. This allows us to
    only persist (pickle) those fields. See TerrasmartCloudUpdater.
    """
    def __init__(self):
        """Initializing these fields implicitly defines the "shape" of the object"""
        self.wind_reporting_close_percentage = None
        self.wind_reporting_close_interval = None
        self.increased_weather_reporting_timeout = None
        self.wind_reporting_over_interval = None
        self.snow_reporting_close_percentage = None
        self.snow_reporting_close_interval = None
        self.snow_reporting_over_interval = None

    def default_config(self):
        # These default values were chosed to match the behavior of the original
        # non-configurable implementation of the "increased reporting" feature
        self.wind_reporting_close_percentage = 75
        self.wind_reporting_close_interval = 15
        self.wind_reporting_over_interval = 15
        self.increased_weather_reporting_timeout = 0
        self.snow_reporting_close_percentage = 75
        self.snow_reporting_close_interval = 15
        self.snow_reporting_over_interval = 15

    # Handle situations where old configs don't have settings required by newer features
    def migrate_config_if_needed(self):
        changes_made = False

        # This whole routine is for future use

        return changes_made


class TerraSmartCloudUpdater(logging.Handler, CloudUpdaterPersistableConfig):
    BATCH_TRACKING_UPDATES_DEFAULT = False
    BATCH_ANGLE_UPDATES_DEFAULT = False
    BATCH_EVENT_LOG_DEFAULT = False
    MIN_SIZE = 0
    
    def __init__(self, mqtt_client, snap_bridge_address_func, sun_rise_set_func, my_site, mem_event_logger, config, enabled=False):
        logging.Handler.__init__(self, level=1)
        
        CloudUpdaterPersistableConfig.__init__(self)

        self._pickle_helper = PickleHelper(self, CloudUpdaterPersistableConfig, CLOUD_UPDATER_CONFIG_FILE)
        if not self.load_config():
            self.default_config()
            self.save_config()
        else:
            # We were able to load a config, but maybe it was out-of-date
            config_was_migrated = self.migrate_config_if_needed()
            if config_was_migrated:
                self.save_config()

        self.mem_event_logger = mem_event_logger
        self.enabled = enabled
        self.mqtt_client = mqtt_client

        assert callable(snap_bridge_address_func)
        assert callable(sun_rise_set_func)
        self.get_snap_bridge_address = snap_bridge_address_func
        self.get_sun_rise_set = sun_rise_set_func
        self.batch_tracking_updates = True  # Gets defaulted later
        self.angle_updates = {}
        self.tracking_updates = []
        self.cleared_records_updates = []
        self.battery_updates = []
        self.panel_update = []
        self.weather_updates = []
        self.config_updates = []
        self.asset_updates = []
        self.asset_radio_updates = []
        self.charger_updates = []
        self.commanded_state = terrasmart_cloud_pb2.UNKNOWN
        self.batch_tracking_updates = self.BATCH_TRACKING_UPDATES_DEFAULT
        self.batch_angle_updates = self.BATCH_ANGLE_UPDATES_DEFAULT
        self.batch_event_log = self.BATCH_EVENT_LOG_DEFAULT
        self._batched_log_count = 0
        self._active_alerts = {}
        # Create some dummy data to send in the case of "no alerts are active" but it
        # is time to make a report. See _send_batch_updates() for more about this...
        alert = terrasmart_cloud_pb2.Alert()
        alert.type = terrasmart_cloud_pb2.Alert.UNKNOWN
        # Just making the remaining fields VALID but not necessarily USEFUL
        alert.when.GetCurrentTime()
        alert.active = False
        alert.message = ''
        self._dummy_active_alerts = { terrasmart_cloud_pb2.Alert.UNKNOWN : alert }

        # Define how large to make the object.
        self.max_row_and_Weather_controller = 900
        sizey = 3
        # Build the two dimensional 20 rows and 3 columns to store snap_address, time and threshode and initilize it.
        self.weather_time_data = []
        for x in range(0, self.max_row_and_Weather_controller):
            self.weather_time_data.append([])
            for y in range(0, sizey):
                self.weather_time_data[x].append("")  # Fill with zeros.

        self.my_site = my_site
        logging.getLogger().addHandler(self)

        self.report_nc_battery_now = False
        self.reported_nc_battery_time = 0
        self.commanded_state_detail = 0
        self.topic = config['TOPIC_PREFIX'] + '/updates'

    def load_config(self):
        return self._pickle_helper.load_config()

    def save_config(self):
        return self._pickle_helper.save_config()

    def update_config(self, new_config_dict):
        """update config based on user input (via SLUI)"""
        for key in new_config_dict:
            if hasattr(self, key):
                setattr(self, key, new_config_dict[key])
        if self.save_config():
            self.on_config_update()

    def emit(self, record):
        """
        Determine if we should send this event log record to the cloud
        """
        if record.name == 'EventLog' or record.levelno >= event_log.MemoryEventLog.ALARM_LEVELNO:
            self._batched_log_count += 1

            if not self.batch_event_log:
                self._send_batch_updates()

    def _get_asset_snap_address(self, asset):
        return asset.snap_address if asset.id != 'serial' else self.get_snap_bridge_address()

    def asset_config_valid(self, asset):
        if asset.model_device is None:
            return False
        # May bring this check back later, but right now it is VERY
        # common for the GPS coordinates of units not to be set.
        #if (asset.location_lat == 0.0) and (asset.location_lng == 0.0):
        #    return False
        return True

    def asset_config_changed(self, asset_diff):
        if 'model_device' in asset_diff:
            return True
        if 'location_text' in asset_diff:
            return True
        if 'location_lat' in asset_diff:
            return True
        if 'location_lng' in asset_diff:
            return True
        if 'hardware_rev' in asset_diff:
            return True
        if 'firmware_rev' in asset_diff:
            return True
        if 'config_label' in asset_diff:
            return True
        if 'config_timestamp' in asset_diff:
            return True
        if 'site_name' in asset_diff:
            return True
        if 'panel_horizontal_cal_angle' in asset_diff:
            return True
        if 'panel_min_cal_angle' in asset_diff:
            return True
        if 'panel_max_cal_angle' in asset_diff:
            return True
        if 'config_flags' in asset_diff:
            return True
        if 'segments_change' in asset_diff:
            if asset_diff['segments_change']:
                return True
        if 'preset_angles_change' in asset_diff:
            return asset_diff['preset_angles_change']
        return False

    def get_weather_config(self):

        class WeatherConfig(object):
            def __init__(self):
                self.wind_speed_threshold = 35.0
                self.wind_gust_threshold = 50.0
                self.snow_depth_threshold = 1.0  # in meters
                self.panel_snow_depth_threshold = 1.0  # in meters
                self.enable_wind_speed_stow = False
                self.enable_wind_gust_stow = False
                self.enable_snow_depth_stow = False
                self.enable_panel_snow_depth_stow = False
                self.minimum_stations_required = 0 
                self.wind_speed_duration_required = 1 * 60
                self.resume_tracking_after_wind_timeout = 3 * 60
                self.resume_tracking_after_snow_timeout = 0
                self.resume_tracking_after_panel_snow_timeout = 0
                self.enable_snow_depth_averaging = False
                self.enable_panel_snow_depth_averaging = False

                PickleHelper(self, None, 'weathermon.pkl').load_config()

        return WeatherConfig()

    def get_site_config(self):

        class SiteConfig(object):
            def __init__(self):
                self.site_name = "Unknown Site Name"
                self.site_contact = "No contact"
                self.site_organization = "Unknown Org"
                self.gps_lat = 26.432691
                self.gps_lng = -81.799176
                self.gps_alt = 4.5
                self.enable_nightly_shutdown = True
                self.power_off = 30
                self.power_on = 30
                self.enable_low_power_shutdown = False
                self.cell_modem_warning_voltage = 12.5
                self.cell_modem_cutoff_voltage = 12.0
                self.cell_modem_cuton_voltage = 13.0  # only applies after cell modem cutoff!
                self.gateway_warning_voltage = 11.5
                self.gateway_cutoff_voltage = 11.0

                PickleHelper(self, None, 'site.pkl').load_config()

        return SiteConfig()

    def get_weather_threshold_trigger(self, asset_after):
        # NOTE! - because we have made WIND and SNOW reporting individually configurable,
        # this routine now returns a set of bits instead of a single value.

        threshold = self.get_weather_config()

        triggers = NO_TRIGGER

        if asset_after.average_wind_speed is not None and threshold.wind_speed_threshold is not None and threshold.enable_wind_speed_stow is not None:
            if threshold.enable_wind_speed_stow:
                if asset_after.average_wind_speed >= (threshold.wind_speed_threshold * (self.wind_reporting_close_percentage / 100.0)):
                    triggers |= AVG_WIND_CLOSE
                if asset_after.average_wind_speed > threshold.wind_speed_threshold:
                    triggers |= AVG_WIND_OVER

        if asset_after.peak_wind_speed is not None and threshold.wind_gust_threshold is not None and threshold.enable_wind_gust_stow is not None:
            if threshold.enable_wind_gust_stow:
                if asset_after.peak_wind_speed >= (threshold.wind_gust_threshold * (self.wind_reporting_close_percentage / 100.0)):
                    triggers |= WIND_GUST_CLOSE
                if asset_after.peak_wind_speed > threshold.wind_gust_threshold:
                    triggers |= WIND_GUST_OVER

        if asset_after.snow_depth is not None and threshold.snow_depth_threshold is not None and threshold.enable_snow_depth_stow is not None:
            if threshold.enable_snow_depth_stow:
                if asset_after.snow_depth >= (threshold.snow_depth_threshold * (self.snow_reporting_close_percentage / 100.0)):
                    triggers |= SNOW_CLOSE
                if asset_after.snow_depth > threshold.snow_depth_threshold:
                    triggers |= SNOW_OVER

        if asset_after.snow_depth is not None and threshold.panel_snow_depth_threshold is not None and threshold.enable_panel_snow_depth_stow is not None:
            if threshold.enable_panel_snow_depth_stow:
                if asset_after.snow_depth >= (threshold.panel_snow_depth_threshold * (self.snow_reporting_close_percentage / 100.0)):
                    triggers |= PANEL_SNOW_CLOSE
                if asset_after.snow_depth > threshold.panel_snow_depth_threshold:
                    triggers |= PANEL_SNOW_OVER
     
        return triggers

    def on_asset_changed(self, asset_before, asset_after):
        if self.enabled:
            now = datetime.datetime.utcnow() # compute this once upfront...
            updates_to_send = False
            send_battery_updates = False
            updates = self._create_new_updates()
            TerraSmartCloudUpdater.MIN_SIZE = len(updates.SerializeToString())

            asset_diff = {k:v for k, v in asset_after.__dict__.iteritems() if asset_before.__dict__.get(k, None) != v}
            # print asset_diff

            if self.asset_config_valid(asset_after):
                if self.asset_config_changed(asset_diff):
                    updates_to_send = True
                    self._add_config_hist(asset_after)

            if (('tracking_status_bits' in asset_diff) 
                or ('status_bits' in asset_diff) 
                or ('offline' in asset_diff) 
                or ('commissioning_mode' in asset_diff)):
                # We used to filter out the NCCB here but realized we DO
                # want to know how it's battery (etc.) is doing...
                updates_to_send = True
                tc_instant_update = updates.tc_updates.add()
                tc_instant_update.tc_snap_addr = self._get_asset_snap_address(asset_after)
                tc_instant_update.when.GetCurrentTime()  # Uses UTC time
                tc_instant_update.status_bits = asset_after.status_bits               
                if asset_after.offline:
                    tc_instant_update.asset_status = terrasmart_cloud_pb2.TrackerControllerInstantUpdates.OFFLINE
                elif asset_after.tracking_status == TCTrackingStatusBits.HANDHELD_ROW_CONTROL.value:
                    tc_instant_update.asset_status = terrasmart_cloud_pb2.TrackerControllerInstantUpdates.UNDER_HHC_CONTROL
                elif asset_after.offline_minutes == MANUAL_CONTROL_INDICATOR:
                    tc_instant_update.asset_status = terrasmart_cloud_pb2.TrackerControllerInstantUpdates.UNDER_MANUAL_CONTROL
                elif asset_after.offline_minutes > 0:
                    tc_instant_update.asset_status = terrasmart_cloud_pb2.TrackerControllerInstantUpdates.UNDER_FCS_CONTROL
                else:
                    tc_instant_update.asset_status = terrasmart_cloud_pb2.TrackerControllerInstantUpdates.ONLINE
                
                tc_instant_update.last_reported.FromDatetime(asset_after.last_reported)
                
            report_angle_update = False
            tracking_status = asset_after.tracking_status
            # Only check all this angle stuff when it is a Row Controller
            if asset_after.has_tracker_hardware:
                unit_is_in_special_mode = False
                if (asset_after.tracking_status & 0x20) != 0x00: # FastTrak Mode
                    unit_is_in_special_mode = True
                if ((asset_after.tracking_status & 0x08) == 0x00) and (asset_after.offline_minutes != 0):
                    unit_is_in_special_mode = True # FCS Mode
                if (asset_after.tracking_status & 0x08) != 0x00: # IRC Mode
                    unit_is_in_special_mode = True

                if 'panel_command_state' in asset_diff or 'panel_index' in asset_diff :
                    report_angle_update = True

                # Here we try to strike a balance between reporting too often, and
                # not reporting often enough.
                # ANY change in intended angle is a potential trigger...
                elif 'requested_angle' in asset_diff:
                    angle_difference = abs(asset_after.requested_angle - asset_after.requested_angle_last_reported)
                    if unit_is_in_special_mode and (angle_difference > 0):
                        report_angle_update = True
                    elif angle_difference >= 2.5:
                        report_angle_update = True
                    else:
                        time_since_last_report = now - asset_after.current_angle_last_reported_at
                        time_in_seconds = time_since_last_report.total_seconds()
                        if (angle_difference >= 1.0) and (time_in_seconds >= 30):
                            report_angle_update = True
                        elif (angle_difference >= 0.5) and (time_in_seconds >= 60):
                            report_angle_update = True
                        elif tracking_status == TCTrackingStatusBits.LOCAL_ESTOP.value:
                            if (angle_difference >= 0.0) and (time_in_seconds >= 3600):
                                report_angle_update = True
                        elif (angle_difference >= 0.2) and (time_in_seconds >= 120):
                            report_angle_update = True
                        elif (angle_difference >= 0.1) and (time_in_seconds >= 180):
                            report_angle_update = True
                        elif (angle_difference >= 0.0) and (time_in_seconds >= 3600):
                            report_angle_update = True
                else:
                    # We want to "see" the panel moving with reasonable granularity
                    angle_difference = abs(asset_after.current_angle - asset_after.current_angle_last_reported)
                    if unit_is_in_special_mode and (angle_difference > 0):
                        report_angle_update = True
                    elif angle_difference >= 2.5: # maybe raise this to 10 or higher?
                        report_angle_update = True
                    else:
                        # Here we are trying to gracefully handle the ends of movements...
                        time_since_last_report = now - asset_after.current_angle_last_reported_at
                        time_in_seconds = time_since_last_report.total_seconds()
                        if (angle_difference >= 1.0) and (time_in_seconds >= 30):
                            report_angle_update = True
                        elif (angle_difference >= 0.5) and (time_in_seconds >= 60):
                            report_angle_update = True
                        elif tracking_status == TCTrackingStatusBits.LOCAL_ESTOP.value:
                            if (angle_difference >= 0.0) and (time_in_seconds >= 3600):
                                report_angle_update = True
                        elif (angle_difference >= 0.2) and (time_in_seconds >= 120):
                            report_angle_update = True
                        elif (angle_difference >= 0.1) and (time_in_seconds >= 180):
                            report_angle_update = True
                        elif (angle_difference >= 0.0) and (time_in_seconds >= 3600):
                            report_angle_update = True
                            
                # 04/05/2019 - we realized that at times of day where the panel is at a motion
                # limit, tracking_status changes can go unreported because there is no corresponding
                # angle change. Making tracking_status changes an explicit trigger. One additional
                # wrinkle: tracking_status is a property of Asset, not a field, and so we have to
                # do the comparison explicity instead of looking in asset_diff
                #if 'tracking_status' in asset_diff: # <- this does not work!
                if asset_before.tracking_status != tracking_status:
                    report_angle_update = True
                # 05/03/2019 - we realized that the tracking_status property can mask out changes
                # because it "reports the first condition it finds". Because of this, adding an
                # explicit check for changes in the raw tracking_status_bits too.
                # This should HELP TSTRAC-375 but still will not cover all cases.
                if asset_before.tracking_status_bits != asset_after.tracking_status_bits:
                    report_angle_update = True
                if asset_after.site_commanded_state != self.commanded_state or asset_after.site_commanded_state_detail != self.commanded_state_detail:
                    report_angle_update = True
                    asset_after.site_commanded_state = self.commanded_state
                    asset_after.site_commanded_state_detail = self.commanded_state_detail
                    
            if (('radio_firmware' in asset_diff) or
                ('radio_script_version' in asset_diff) or 
                ('offline' in asset_diff and not asset_after.offline) or
                ('is_a_repeater' in asset_diff)):
                    self._add_asset_radio_update(asset_after)

            if report_angle_update:
                # Make notes for future "trigger" determinations (see above)
                asset_after.current_angle_last_reported = asset_after.current_angle
                asset_after.requested_angle_last_reported = asset_after.requested_angle
                asset_after.current_angle_last_reported_at = now
                # Convert between the TC/NC numbering and the Cloud enumeration
                converted_tracking_status = terrasmart_cloud_pb2.AngleUpdate.UNKNOWN
                # What looked like a field reference below was really a property reference.
                # The code has been changed to access a temporary variable instead, to
                # ensure we are not repeatedly doing the conversion (see property tracking_status)
                if tracking_status == TCTrackingStatusBits.MANUAL.value:
                    converted_tracking_status = terrasmart_cloud_pb2.AngleUpdate.MANUAL
                elif tracking_status == TCTrackingStatusBits.AUTO_TRACK_TRACKING.value:
                    converted_tracking_status = terrasmart_cloud_pb2.AngleUpdate.TRACKING_ONLY
                elif tracking_status == TCTrackingStatusBits.AUTO_TRACK_BACKTRACKING.value:
                    converted_tracking_status = terrasmart_cloud_pb2.AngleUpdate.TRACKING_WITH_BACKTRACKING
                elif tracking_status == TCTrackingStatusBits.LOW_BATTERY_AUTO_STOW_INITIATED.value:
                    converted_tracking_status = terrasmart_cloud_pb2.AngleUpdate.LOW_BATTERY_AUTO_STOW
                elif tracking_status == TCTrackingStatusBits.LOCAL_ESTOP.value:
                    converted_tracking_status = terrasmart_cloud_pb2.AngleUpdate.LOCAL_ESTOP
                elif tracking_status == TCTrackingStatusBits.INDIVIDUAL_ROW_CONTROL.value:
                    converted_tracking_status = terrasmart_cloud_pb2.AngleUpdate.INDIVIDUAL_ROW_CONTROL
                elif tracking_status == TCTrackingStatusBits.GROUP_ROW_CONTROL.value:
                    converted_tracking_status = terrasmart_cloud_pb2.AngleUpdate.GROUP_ROW_CONTROL
                elif tracking_status == TCTrackingStatusBits.HANDHELD_ROW_CONTROL.value:
                    converted_tracking_status = terrasmart_cloud_pb2.AngleUpdate.HANDHELD_ROW_CONTROL

                angles = self.angle_updates.setdefault(asset_after.snap_address, list())
                angles.append(tc_angle_update(asset_after.current_angle,
                                              asset_after.requested_angle,
                                              now,
                                              asset_after.site_commanded_state,
                                              converted_tracking_status,
                                              asset_after.site_commanded_state_detail,
                                              asset_after.panel_command_state,
                                              asset_after.panel_index,
                                              asset_after.motor_current))
                if not self.batch_angle_updates:
                    updates_to_send = True
                self._add_motor_current_hist(asset_after)

            send_weather = False
            if 'hours_since_accumulators_reset' in asset_diff:
                if asset_after.has_weather_sensor:
                    send_weather = True
                updates_to_send = True
                send_battery_updates = True
                try:
                    tc_hour_update = updates.tc_hour_updates.add()
                    if asset_after.hours_since_accumulators_reset == 0:
                        tc_hour_update.hour = 23
                    else:
                        tc_hour_update.hour = asset_after.hours_since_accumulators_reset - 1

                    day_dt = self.get_sun_rise_set().sunrise
                    if now < day_dt:
                        # The "tracking day" doesn't change until sunrise
                        yesterday = now - datetime.timedelta(days=1)
                        day_dt = day_dt.replace(year=yesterday.year, month=yesterday.month, day=yesterday.day)
                    day_dt = day_dt.replace(second=0, minute=0, microsecond=0, hour=0)
                    tc_hour_update.day.FromDatetime(day_dt)

                    tc_update = tc_hour_update.tc_update
                    tc_update.tc_snap_addr = self._get_asset_snap_address(asset_after)
                    tc_update.when.GetCurrentTime()  # Uses UTC time
                    tc_update.polls_sent = asset_after.radio_polls_sent
                    tc_update.polls_received = asset_after.radio_poll_responses

                    if not asset_after.radio_link_quality is None:
                        tc_update.link_quality = asset_after.radio_link_quality
                    if not asset_after.radio_mesh_depth is None:
                        tc_update.mesh_depth = asset_after.radio_mesh_depth

                    panel_pwr_update = tc_update.updates.add()
                    panel_pwr_update.type = terrasmart_cloud_pb2.AccumulatorUpdate.PANEL
                    panel_pwr_update.value = asset_after.solar_power_previous_hour

                    batt_pwr_update = tc_update.updates.add()
                    batt_pwr_update.type = terrasmart_cloud_pb2.AccumulatorUpdate.BATT
                    batt_pwr_update.value = asset_after.battery_power_previous_hour

                    motor_pwr_update = tc_update.updates.add()
                    motor_pwr_update.type = terrasmart_cloud_pb2.AccumulatorUpdate.MOTOR
                    motor_pwr_update.value = asset_after.motor_power_previous_hour

                    charger_pwr_update = tc_update.updates.add()
                    charger_pwr_update.type = terrasmart_cloud_pb2.AccumulatorUpdate.CHARGER
                    charger_pwr_update.value = asset_after.charger_power_current_hour

                    angle_err_update = tc_update.updates.add()
                    angle_err_update.type = terrasmart_cloud_pb2.AccumulatorUpdate.ANGULAR_ERROR
                    angle_err_update.value = asset_after.average_angular_error_previous_hour

                    external_input_2_pwr_update = tc_update.updates.add()
                    external_input_2_pwr_update.type = terrasmart_cloud_pb2.AccumulatorUpdate.EXTERNAL_INPUT_2
                    external_input_2_pwr_update.value = asset_after.external_input_2_power_previous_hour
                except:
                    log.exception('Unable to set cloud updates on accumulators reset')

            if 'hours_since_accumulators_reset' in asset_diff and asset_after.hours_since_accumulators_reset == 0:
                updates_to_send = True
                send_battery_updates = True
                try:
                    tc_update = updates.tc_day_updates.add()
                    tc_update.tc_snap_addr = self._get_asset_snap_address(asset_after)
                    tc_update.when.GetCurrentTime()  # Uses UTC time
                    tc_update.polls_sent = asset_after.radio_polls_sent
                    tc_update.polls_received = asset_after.radio_poll_responses
                    
                    if not asset_after.radio_link_quality is None:
                        tc_update.link_quality = asset_after.radio_link_quality
                    if not asset_after.radio_mesh_depth is None:
                        tc_update.mesh_depth = asset_after.radio_mesh_depth

                    panel_pwr_update = tc_update.updates.add()
                    panel_pwr_update.type = terrasmart_cloud_pb2.AccumulatorUpdate.PANEL
                    panel_pwr_update.value = asset_after.solar_power_previous_day

                    batt_pwr_update = tc_update.updates.add()
                    batt_pwr_update.type = terrasmart_cloud_pb2.AccumulatorUpdate.BATT
                    batt_pwr_update.value = asset_after.battery_power_previous_day

                    motor_pwr_update = tc_update.updates.add()
                    motor_pwr_update.type = terrasmart_cloud_pb2.AccumulatorUpdate.MOTOR
                    motor_pwr_update.value = asset_after.motor_power_previous_day

                    charger_pwr_update = tc_update.updates.add()
                    charger_pwr_update.type = terrasmart_cloud_pb2.AccumulatorUpdate.CHARGER
                    charger_pwr_update.value = asset_after.charger_power_current_day

                    angle_err_update = tc_update.updates.add()
                    angle_err_update.type = terrasmart_cloud_pb2.AccumulatorUpdate.ANGULAR_ERROR
                    angle_err_update.value = asset_after.average_angular_error_previous_day

                    external_input_2_pwr_update = tc_update.updates.add()
                    external_input_2_pwr_update.type = terrasmart_cloud_pb2.AccumulatorUpdate.EXTERNAL_INPUT_2
                    external_input_2_pwr_update.value = asset_after.external_input_2_power_previous_day
                except:
                    log.exception('Unable to set cloud updates on accumulators reset')
            if updates_to_send:
                if self._is_nc(asset_after):
                    if send_battery_updates:
                        self._add_asset_hist(asset_after)
                else:
                    self._add_asset_hist(asset_after)
                self._add_charger_hist(asset_after)
                self._add_panel_hist(asset_after)
                if send_weather and not self.get_weather_threshold_trigger(asset_after):
                    self._add_weather_hist(asset_after)

            if(asset_after.has_weather_sensor):
                self.Update_Cloud_for_Weather(asset_after, now, updates)

            if 'misc_status_bits' in asset_diff:
                updates_to_send = True

            self.Update_cloud_for_Bettery(asset_after, updates_to_send, send_battery_updates, updates)
            
            self._send_batch_updates(updates)

    def Update_cloud_for_Bettery(self, asset_after, updates_to_send, send_battery_updates, updates):
        if updates_to_send:
            if self._is_nc(asset_after):
                if send_battery_updates:
                    if not asset_after.battery_charged < 50:
                        self._add_battery_hist(asset_after)
            else:
                self._add_battery_hist(asset_after)

        if self._is_nc(asset_after):
            if self.report_nc_battery_now:
                self._add_battery_hist(asset_after)
                self.report_nc_battery_now = False
                self.reported_nc_battery_time = datetime.datetime.now()
            else:
                if asset_after.battery_charged < 50:
                    if not self.reported_nc_battery_time:
                        self._add_battery_hist(asset_after)
                        self.reported_nc_battery_time = datetime.datetime.now()
                    cc = datetime.datetime.now() - self.reported_nc_battery_time
                    if cc.seconds / 60.0 >= 15:
                        self._add_battery_hist(asset_after)
                        self.reported_nc_battery_time = datetime.datetime.now()

    def Update_Cloud_for_Weather(self, asset_after, now, updates):
        if not (asset_after.wind_speed or asset_after.average_wind_speed or asset_after.peak_wind_speed or asset_after.wind_direction or asset_after.snow_sensor_temperature or asset_after.snow_depth):
            return

        send_to_cloud_now = False

        sizex = self.max_row_and_Weather_controller
        index = self._get_index(self.weather_time_data, asset_after.snap_address, sizex)

        triggers = self.get_weather_threshold_trigger(asset_after)

        if index > 0:
            time_updated = self.weather_time_data[index - 1][1]
            time_since_last_report = datetime.datetime.now() - time_updated
            seconds_since_last_report = time_since_last_report.seconds
            minutes_since_last_report = seconds_since_last_report / 60.0
            old_triggers = self.weather_time_data[index - 1][2]
            # Report when we change behaviours, but even that has to be rate-limited
            if (old_triggers != triggers) and (minutes_since_last_report >= 1):
                send_to_cloud_now = True
            else: # check for the interval updates
                if (triggers & AVG_WIND_CLOSE) and (triggers & AVG_WIND_OVER) :
                    if minutes_since_last_report >= self.wind_reporting_over_interval:
                        send_to_cloud_now = True
                if (triggers & WIND_GUST_CLOSE) and (triggers & WIND_GUST_OVER):
                    if minutes_since_last_report >= self.wind_reporting_over_interval:
                        send_to_cloud_now = True
                if (triggers & SNOW_CLOSE) and (triggers & SNOW_OVER):
                    if minutes_since_last_report >= self.snow_reporting_over_interval:
                        send_to_cloud_now = True
                if (triggers & PANEL_SNOW_CLOSE) and (triggers & PANEL_SNOW_OVER):
                    if minutes_since_last_report >= self.snow_reporting_over_interval:
                        send_to_cloud_now = True
        else:
            send_to_cloud_now = True

        if send_to_cloud_now:
            self._set_update(self.weather_time_data, asset_after.snap_address, sizex,
                             datetime.datetime.now(), index, triggers)
            self._add_weather_hist(asset_after)

    def _add_asset_radio_update(self, asset):
        if asset.device == 'Companion':
            return
        radio_update = terrasmart_cloud_pb2.AssetRadioUpdate()
        radio_update.when.GetCurrentTime()  # Uses UTC time
        snap_addr = self._get_asset_snap_address(asset)
        if not snap_addr is None:
            radio_update.snap_addr = snap_addr
        radio_update.radio_mac_addr = str(asset.radio_mac_addr)
        radio_update.radio_channel = str(asset.radio_channel)
        radio_update.radio_network_id = str(asset.radio_network_id)
        radio_update.radio_firmware = str(asset.radio_firmware)
        radio_update.radio_script_version = str(asset.radio_script_version)
        radio_update.radio_script_crc = str(asset.radio_script_crc)
        radio_update.radio_link_quality = str(asset.radio_link_quality)
        radio_update.radio_mesh_depth = str(asset.radio_mesh_depth)
        radio_update.radio_polls_sent = str(asset.radio_polls_sent)
        radio_update.radio_poll_responses = str(asset.radio_poll_responses)
        if not asset.is_a_repeater is None:
            radio_update.is_a_repeater = asset.is_a_repeater
        self.asset_radio_updates.append(radio_update)

    def on_startup_update(self, sw_version, nccb_uptime, linux_uptime, app_uptime):
        if sw_version and nccb_uptime and linux_uptime and app_uptime:
            updates = self._create_new_updates()
            start_up_data = updates.start_up_data.add()
            start_up_data.when.GetCurrentTime()  # Uses UTC time
            start_up_data.FW_Version = sw_version
            start_up_data.NCCB_Uptime = nccb_uptime
            start_up_data.Linux_Uptime = linux_uptime
            start_up_data.Application_Uptime = app_uptime
            self.report_nc_battery_now = True
            self._send_batch_updates(updates)

    def on_cleared_records(self, cleared_records):
        self.cleared_records_updates.extend(cleared_records)

        if not self.batch_event_log:
            self._send_batch_updates()

    def on_state_change(self, snap_addr, command, status, error_code):
        updates = self._create_new_updates()
        updates.command_status.snap_addr = snap_addr
        updates.command_status.when.GetCurrentTime()
        updates.command_status.command = command.value
        updates.command_status.status = status.value
        updates.command_status.error_code = error_code.value
        self._send_batch_updates(updates)

    def _add_tracking_updates(self, cloud_update):
        for update in self.tracking_updates:
            tc = cloud_update.tracking_changes.add()
            tc.updated_state = update.new_commanded_state
            tc.state_changed_at.FromDatetime(update.state_changed_at)
            if update.commanded_preset is not None:
                tc.commanded_preset = update.commanded_preset
        del self.tracking_updates[:]

    def _add_angle_updates(self, cloud_update):
        for tc_snap_addr, angles in self.angle_updates.iteritems():
            if angles:
                rack_angles = cloud_update.rack_angles.add()
                rack_angles.tc_snap_addr = tc_snap_addr
                for angle_update in angles:
                    update = rack_angles.angles.add()
                    # TODO Figure out a way to make this less "manual" (and therefor error prone)
                    update.current_angle = angle_update.current_angle
                    update.requested_angle = angle_update.requested_angle
                    update.commanded_state = angle_update.commanded_state
                    update.tracking_status = angle_update.tracking_status
                    update.when.FromDatetime(angle_update.timestamp)
                    update.commanded_state_detail = angle_update.commanded_state_detail
                    update.panel_command_state = angle_update.panel_command_state
                    update.panel_index = angle_update.panel_index
                    update.motor_current = angle_update.motor_current
        self.angle_updates.clear()

    def _add_event_log_updates(self, cloud_update):
        if self._batched_log_count is not None:
            for record in itertools.islice(self.mem_event_logger.collection.itervalues(), 0, self._batched_log_count):
                cloud_log_entry = cloud_update.log_entries.add()
                cloud_log_entry.created.FromNanoseconds(int(record.created * _NANOS_PER_SECOND))
                cloud_log_entry.levelno = record.levelno
                cloud_log_entry.logger = record.name
                cloud_log_entry.message = record.message
                try:
                    cloud_log_entry.type = record.type.value
                except AttributeError:
                    cloud_log_entry.type = EventType.UNKNOWN.value

        for record in self.cleared_records_updates:
            cleared_log_entry = cloud_update.clear_log_entries.add()
            cleared_log_entry.created.FromNanoseconds(int(record.created * _NANOS_PER_SECOND))
            cleared_log_entry.cleared.FromNanoseconds(int(record.cleared * _NANOS_PER_SECOND))

        self._batched_log_count = 0
        del self.cleared_records_updates[:]

    def _send_update(self, update_pb):
        try:
            log.debug('Sending Cloud Update: %s' % update_pb)
            update_str = update_pb.SerializeToString()
        except google.protobuf.message.Error:
            log.critical('Unable to create cloud update message')
            return
        self.mqtt_client.publish(topic=self.topic, payload=bytearray(update_str))

    def on_panel_state_change(self, prev_state, new_state, my_site):
        
        self.commanded_state = terrasmart_cloud_pb2.UNKNOWN
        if new_state == CommandedPanelState.NORMAL:
            self.commanded_state = terrasmart_cloud_pb2.TRACKING_ONLY
            if my_site.backtracking_enable:
                self.commanded_state = terrasmart_cloud_pb2.TRACKING_WITH_BACKTRACKING
        elif new_state == CommandedPanelState.ESTOP:
            self.commanded_state = terrasmart_cloud_pb2.ESTOP
        elif new_state == CommandedPanelState.STOW_NIGHT:
            self.commanded_state = terrasmart_cloud_pb2.STOW_NIGHT
        elif new_state == CommandedPanelState.STOW_WEATHER:
            self.commanded_state = terrasmart_cloud_pb2.STOW_WEATHER
        elif new_state == CommandedPanelState.PRESET:
            self.commanded_state = terrasmart_cloud_pb2.PRESET
            self.tracking_updates.append(tracking_change(self.commanded_state, datetime.datetime.utcnow(), my_site.operational_mode))
            self.commanded_state_detail = my_site.operational_mode

        if new_state != CommandedPanelState.PRESET:
            self.tracking_updates.append(tracking_change(self.commanded_state, datetime.datetime.utcnow(), None))
            
        if not self.batch_tracking_updates:
            updates = terrasmart_cloud_pb2.CloudUpdates()
            try:
                updates.nc_snap_addr = self.get_snap_bridge_address()
            except ValueError:
                log.critical("Unable to determine SNAP bridge address for cloud update")
                return
            self._send_batch_updates(updates)

            self.report_nc_battery_now = True

    def _send_batch_updates(self, updates=None, send_current_alerts=False):
        updates = updates or self._create_new_updates()

        self._add_tracking_updates(updates)
        self._add_angle_updates(updates)
        self._add_event_log_updates(updates)
        updates.asset_radio_updates.extend(self.asset_radio_updates)
        del self.asset_radio_updates[:]
        updates.battery_updates.extend(self.battery_updates)
        del self.battery_updates[:]
        updates.weather_updates.extend(self.weather_updates)
        del self.weather_updates[:]
        updates.config_updates.extend(self.config_updates)
        del self.config_updates[:]
        updates.asset_updates.extend(self.asset_updates)
        del self.asset_updates[:]
        updates.panel_update.extend(self.panel_update)
        del self.panel_update[:]
        updates.charger_updates.extend(self.charger_updates)
        del self.charger_updates[:]

        if send_current_alerts:
            # Because we don't ALWAYS report this section, the cloud was
            # unable to distinguish between the following two cases:
            # 1) "list is empty because there ARE NO active alerts"
            # 2) "list is empty because I am not reporting alerts THIS TIME"
            # To get around this, we now force the list to be non-empty when
            # we DO bother to report it. There was already an UNKNOWN alert defined
            if self._active_alerts == {}:
                updates.active_alerts.extend(self._dummy_active_alerts.itervalues())
            else:
                updates.active_alerts.extend(self._active_alerts.itervalues())
        if len(updates.SerializeToString()) > TerraSmartCloudUpdater.MIN_SIZE:
            self._send_update(updates)

    # This is how the SystemManager tries to get a few more reports out asset_before
    # powering down the unit and/or the cell modem
    def flush_cloud_updates(self):
        self._send_batch_updates(updates=None, send_current_alerts=True)

    def on_estop_change(self, pressed, msg):
        updates = terrasmart_cloud_pb2.CloudUpdates()
        alert = terrasmart_cloud_pb2.Alert()
        alert.when.GetCurrentTime()  # Uses UTC time
        alert.active = pressed
        alert.type = terrasmart_cloud_pb2.Alert.ESTOP
        alert.message = msg
        updates.alert_updates.extend([alert])

        self._active_alerts.pop(terrasmart_cloud_pb2.Alert.ESTOP, None)
        if alert.active:
            self._active_alerts[terrasmart_cloud_pb2.Alert.ESTOP] = alert

        self._send_batch_updates(updates, send_current_alerts=True)

    def on_assets_reporting_change(self, assets_reporting, total_assets, msg):
        updates = terrasmart_cloud_pb2.CloudUpdates()
        alert = terrasmart_cloud_pb2.Alert()
        alert.when.GetCurrentTime()  # Uses UTC time
        alert.active = assets_reporting != total_assets
        alert.type = terrasmart_cloud_pb2.Alert.ASSETS_REPORTING
        alert.message = msg
        updates.alert_updates.extend([alert])

        self._active_alerts.pop(terrasmart_cloud_pb2.Alert.ASSETS_REPORTING, None)
        if alert.active:
            self._active_alerts[terrasmart_cloud_pb2.Alert.ASSETS_REPORTING] = alert

        self._send_batch_updates(updates, send_current_alerts=True)

    def on_gps_change(self, state, msg):
        updates = terrasmart_cloud_pb2.CloudUpdates()
        alert = terrasmart_cloud_pb2.Alert()
        alert.when.GetCurrentTime()  # Uses UTC time
        alert.active = not state
        alert.type = terrasmart_cloud_pb2.Alert.GPS
        alert.message = msg
        updates.alert_updates.extend([alert])

        self._active_alerts.pop(terrasmart_cloud_pb2.Alert.GPS, None)
        if alert.active:
            self._active_alerts[terrasmart_cloud_pb2.Alert.GPS] = alert

        self._send_batch_updates(updates, send_current_alerts=True)

    def on_clock_questionable(self, is_questionable, msg):
        updates = terrasmart_cloud_pb2.CloudUpdates()
        alert = terrasmart_cloud_pb2.Alert()
        alert.when.GetCurrentTime()  # Uses UTC time
        alert.active = is_questionable
        alert.type = terrasmart_cloud_pb2.Alert.CLOCK
        alert.message = msg
        updates.alert_updates.extend([alert])

        self._active_alerts.pop(terrasmart_cloud_pb2.Alert.CLOCK, None)
        if alert.active:
            self._active_alerts[terrasmart_cloud_pb2.Alert.CLOCK] = alert

        self._send_batch_updates(updates, send_current_alerts=True)

    def on_send_gps_update(self, obj):
        updates = self._create_new_updates()
        updates.gps_updates.snap_addr = updates.nc_snap_addr
        updates.gps_updates.when.GetCurrentTime()
        updates.gps_updates.latitude = obj.latitude
        updates.gps_updates.longitude = obj.longitude
        updates.gps_updates.altitude = obj.altitude
        updates.gps_updates.num_sats = obj.num_sats and obj.num_sats or 0
        updates.gps_updates.quality = obj.quality and obj.quality or 0
        updates.gps_updates.fix_time.FromDatetime(obj.fix_time and obj.fix_time or datetime.datetime(1979, 1, 1, 1))
        updates.gps_updates.is_responding = obj.is_responding
        updates.gps_updates.altitude_units = obj.altitude_units and obj.altitude_units or ""
        updates.gps_updates.is_clock_questionable = obj.system_clock_questionable

        self._send_batch_updates(updates)

    def on_cell_state_change(self, state, msg):
        updates = terrasmart_cloud_pb2.CloudUpdates()
        alert = terrasmart_cloud_pb2.Alert()
        alert.when.GetCurrentTime()  # Uses UTC time
        alert.active = not state
        alert.type = terrasmart_cloud_pb2.Alert.MODEM
        alert.message = msg
        updates.alert_updates.extend([alert])

        self._active_alerts.pop(terrasmart_cloud_pb2.Alert.MODEM, None)
        if alert.active:
            self._active_alerts[terrasmart_cloud_pb2.Alert.MODEM] = alert

        self._send_batch_updates(updates, send_current_alerts=True)

    def on_ntp_sync_change(self, time_synchronized, msg):
        updates = terrasmart_cloud_pb2.CloudUpdates()
        alert = terrasmart_cloud_pb2.Alert()
        alert.when.GetCurrentTime()  # Uses UTC time
        alert.active = not time_synchronized
        alert.type = terrasmart_cloud_pb2.Alert.NTP
        alert.message = msg
        updates.alert_updates.extend([alert])

        self._active_alerts.pop(terrasmart_cloud_pb2.Alert.NTP, None)
        if alert.active:
            self._active_alerts[terrasmart_cloud_pb2.Alert.NTP] = alert

        self._send_batch_updates(updates, send_current_alerts=True)
           
    def on_update_cell_stats(self, rssi_dbm, uptime):
        updates = self._create_new_updates()
        updates.cell_updates.nc_snap_addr = updates.nc_snap_addr
        updates.cell_updates.when.GetCurrentTime()

        if rssi_dbm is None:
            updates.cell_updates.rssi_dbm = 0    
        else:
            updates.cell_updates.rssi_dbm = int(rssi_dbm)
                       
        if uptime is None:
            updates.cell_updates.uptime = 0
        else:
            updates.cell_updates.uptime = int(uptime)
        self._send_batch_updates(updates)
    
    def on_daily_update_cell_stats(self, modem):
        updates = self._create_new_updates()
        try:
            updates.cell_daily_updates.nc_snap_addr = updates.nc_snap_addr
            updates.cell_daily_updates.when.GetCurrentTime()
            updates.cell_daily_updates.imei = modem.imei
            updates.cell_daily_updates.roaming = modem.roaming
            updates.cell_daily_updates.mdn = modem.mdn
            updates.cell_daily_updates.lan_ip = modem.lan_ip
            updates.cell_daily_updates.wan_ip = modem.wan_ip
            updates.cell_daily_updates.link_status = modem.link_status
            updates.cell_daily_updates.tx_data_usage = modem.tx_data_usage
            updates.cell_daily_updates.rx_data_usage = modem.rx_data_usage
            updates.cell_daily_updates.tower_id = modem.tower_id
        except(ValueError, TypeError):
            log.error("[on_daily_update_cell_stats]: Type mismatch can't assign None")
        self._send_batch_updates(updates)
   
    def on_weather_stow(self, stow_type, value, threshold, asset=None):
        updates = self._create_new_updates()
        snap_addr = None
        try:
            if asset is None:
                snap_addr = updates.nc_snap_addr
            else:
                snap_addr = asset.snap_address
                self._add_weather_hist(asset)
            updates.weather_stow_updates.snap_addr = snap_addr
            updates.weather_stow_updates.when.GetCurrentTime()
            updates.weather_stow_updates.stow_type = stow_type
            updates.weather_stow_updates.value = value
            updates.weather_stow_updates.threshold = threshold
        except(ValueError, TypeError):
            log.error("[on_weather_stow]: Type mismatch can't assign None")
        self._send_batch_updates(updates)
    
    def on_solar_info_update(self, sunrise, sunset, wakeup_time):
        try:
            updates = self._create_new_updates()
            updates.solar_info_updates.snap_addr = updates.nc_snap_addr
            updates.solar_info_updates.when.GetCurrentTime()
            updates.solar_info_updates.sunrise.FromDatetime(sunrise)
            updates.solar_info_updates.sunset.FromDatetime(sunset)
            updates.solar_info_updates.wakeup_time = str(wakeup_time)
            self._send_batch_updates(updates)
        except Exception as e:
            log.error('Exception: (%s) while sending solar info update' % (e.message))
    
    def on_config_update(self):
        updates = self._create_new_updates()
        try:
            self._add_site_config_update(updates)         
            self._send_batch_updates(updates)
        except (ValueError, TypeError):
            log.error("[on_config_update]: Type mismatch can't assign None")

    def on_nc_bridge_update(self, script_version, bridge_version):
        updates = self._create_new_updates()
        updates.bridge_updates.snap_addr = updates.nc_snap_addr
        updates.bridge_updates.when.GetCurrentTime()
        updates.bridge_updates.script_version = str(script_version)
        updates.bridge_updates.firmware_version = bridge_version

        self._send_batch_updates(updates)   

    def _add_site_config_update(self, updates):
        config = self.get_site_config()
        updates.site_config_updates.snap_addr = updates.nc_snap_addr
        updates.site_config_updates.when.GetCurrentTime()
        updates.site_config_updates.site_name = config.site_name
        updates.site_config_updates.site_contact = config.site_contact
        updates.site_config_updates.site_organization = config.site_organization
        updates.site_config_updates.gps_lat = config.gps_lat
        updates.site_config_updates.gps_lng = config.gps_lng
        updates.site_config_updates.gps_alt = config.gps_alt
        updates.site_config_updates.enable_nightly_shutdown = config.enable_nightly_shutdown
        updates.site_config_updates.power_off = config.power_off
        updates.site_config_updates.power_on = config.power_on
        updates.site_config_updates.enable_low_power_shutdown = config.enable_low_power_shutdown
        updates.site_config_updates.cell_modem_warning_voltage = config.cell_modem_warning_voltage           
        updates.site_config_updates.cell_modem_cutoff_voltage = config.cell_modem_cutoff_voltage
        updates.site_config_updates.cell_modem_cuton_voltage = config.cell_modem_cuton_voltage
        updates.site_config_updates.gateway_warning_voltage = config.gateway_warning_voltage
        updates.site_config_updates.gateway_cutoff_voltage = config.gateway_cutoff_voltage

        config = self.get_weather_config()
        updates.site_config_updates.wind_speed_threshold = config.wind_speed_threshold
        updates.site_config_updates.wind_gust_threshold = config.wind_gust_threshold
        updates.site_config_updates.snow_depth_threshold = config.snow_depth_threshold
        updates.site_config_updates.panel_snow_depth_threshold = config.panel_snow_depth_threshold
        updates.site_config_updates.enable_wind_speed_stow = config.enable_wind_speed_stow
        updates.site_config_updates.enable_wind_gust_stow = config.enable_wind_gust_stow
        updates.site_config_updates.enable_snow_depth_stow = config.enable_snow_depth_stow
        updates.site_config_updates.enable_panel_snow_depth_stow = config.enable_panel_snow_depth_stow
        updates.site_config_updates.minimum_stations_required = config.minimum_stations_required
        updates.site_config_updates.wind_speed_duration_required = config.wind_speed_duration_required
        updates.site_config_updates.resume_tracking_after_wind_timeout = config.resume_tracking_after_wind_timeout
        updates.site_config_updates.resume_tracking_after_snow_timeout = config.resume_tracking_after_snow_timeout
        updates.site_config_updates.resume_tracking_after_panel_snow_timeout = config.resume_tracking_after_panel_snow_timeout
        updates.site_config_updates.enable_snow_depth_averaging = config.enable_snow_depth_averaging
        updates.site_config_updates.enable_panel_snow_depth_averaging = config.enable_panel_snow_depth_averaging

        # We need to report some of our own conficuration, because the cloud logs some events about Increased Reporting
        updates.site_config_updates.wind_reporting_close_percentage = self.wind_reporting_close_percentage
        updates.site_config_updates.wind_reporting_close_interval = self.wind_reporting_close_interval
        updates.site_config_updates.wind_reporting_over_interval = self.wind_reporting_over_interval
        updates.site_config_updates.snow_reporting_close_percentage = self.snow_reporting_close_percentage
        updates.site_config_updates.snow_reporting_close_interval = self.snow_reporting_close_interval
        updates.site_config_updates.snow_reporting_over_interval = self.snow_reporting_over_interval


    def _create_new_updates(self):
        updates = terrasmart_cloud_pb2.CloudUpdates()
        try:
            updates.nc_snap_addr = self.get_snap_bridge_address()
        except (ValueError, TypeError):
            log.error("Unable to determine SNAP bridge address for cloud update")
            updates.nc_snap_addr = '\xFF\xFF\xFF'
        return updates

    def _add_battery_hist(self, asset):
        if asset.device:
            batt_update = terrasmart_cloud_pb2.BatteryUpdate()
            batt_update.when.GetCurrentTime()  # Uses UTC time
            snap_addr = self._get_asset_snap_address(asset)
            if not snap_addr is None:
                batt_update.tc_snap_addr = snap_addr
            batt_update.voltage = asset.battery_voltage
            batt_update.current = asset.battery_current
            batt_update.charged = asset.battery_charged
            batt_update.health = asset.battery_health
            batt_update.batt_temp = asset.battery_temperature
            batt_update.heater_temp = asset.heater_temperature
            batt_update.misc_status_bits = asset.misc_status_bits
            self.battery_updates.append(batt_update)

    def _add_charger_hist(self, asset):
        if asset.device:
            charger_update = terrasmart_cloud_pb2.ChargerUpdate()
            charger_update.when.GetCurrentTime()  # Uses UTC time
            snap_addr = self._get_asset_snap_address(asset)
            if not snap_addr is None:
                charger_update.snap_addr = snap_addr
            charger_update.voltage = asset.charger_voltage
            charger_update.current = asset.charger_current
            self.charger_updates.append(charger_update)

    def _add_panel_hist(self, asset):
        if asset.device:
            panel_update = terrasmart_cloud_pb2.PanelUpdate()
            panel_update.when.GetCurrentTime()  # Uses UTC time
            snap_addr = self._get_asset_snap_address(asset)
            if not snap_addr is None:
                panel_update.tc_snap_addr = snap_addr
            # External Input 1 - originally ALWAYS had a solar PANEL connected to it
            panel_update.solar_voltage = asset.solar_voltage
            panel_update.solar_current = asset.solar_current
            # External Input 2 - added in the Model 3 Row Boxes
            panel_update.external_input_2_voltage = asset.external_input_2_voltage
            panel_update.external_input_2_current = asset.external_input_2_current
            self.panel_update.append(panel_update)
        
    def _add_weather_hist(self, asset):
        weather_update = terrasmart_cloud_pb2.WeatherUpdate()
        weather_update.when.GetCurrentTime()  # Uses UTC time
        snap_addr = self._get_asset_snap_address(asset)
        if not snap_addr is None:
            weather_update.snap_addr = snap_addr
        weather_update.wind_speed = asset.wind_speed
        weather_update.wind_direction = asset.wind_direction
        weather_update.average_wind_speed = asset.average_wind_speed
        weather_update.peak_wind_speed = asset.peak_wind_speed
        weather_update.temperature = asset.snow_sensor_temperature
        weather_update.snow_depth = asset.snow_depth
        self.weather_updates.append(weather_update)

    def _add_config_hist(self, asset):
        config_update = terrasmart_cloud_pb2.ConfigUpdate()
        config_update.when.GetCurrentTime()  # Uses UTC time
        snap_addr = self._get_asset_snap_address(asset)
        if snap_addr is not None:
            config_update.snap_addr = snap_addr
        if asset.model_device is not None:
            config_update.model_device = asset.model_device
        config_update.location_lat = asset.location_lat
        config_update.location_lng = asset.location_lng
        config_update.location_text = asset.location_text
        config_update.hardware_rev = asset.hardware_rev
        config_update.firmware_rev = asset.firmware_rev
        config_update.config_label = asset.config_label
        config_update.site_name = asset.site_name
        config_update.panel_horizontal_cal_angle = asset.panel_horizontal_cal_angle
        config_update.panel_min_cal_angle = asset.panel_min_cal_angle
        config_update.panel_max_cal_angle = asset.panel_max_cal_angle
        config_update.config_flags = asset.config_flags
        config_update.segments_0_panel_array_width = asset.segments[0].panel_array_width
        config_update.segments_0_spacing_to_east = asset.segments[0].spacing_to_east
        config_update.segments_0_spacing_to_west = asset.segments[0].spacing_to_west
        config_update.segments_0_delta_height_east = asset.segments[0].delta_height_east
        config_update.segments_0_delta_height_west = asset.segments[0].delta_height_west
        config_update.segments_1_panel_array_width = asset.segments[1].panel_array_width
        config_update.segments_1_spacing_to_east = asset.segments[1].spacing_to_east
        config_update.segments_1_spacing_to_west = asset.segments[1].spacing_to_west
        config_update.segments_1_delta_height_east = asset.segments[1].delta_height_east
        config_update.segments_1_delta_height_west = asset.segments[1].delta_height_west
        if asset.config_timestamp:
            config_update.config_timestamp = int(asset.config_timestamp)
        if asset.preset_angles[0].preset_angle is not None:
            config_update.preset_angles_0_preset_angle = asset.preset_angles[0].preset_angle
        if asset.preset_angles[0].preset_angle is not None:
            config_update.preset_angles_0_nearest_enabled = asset.preset_angles[0].nearest_enabled
        if asset.preset_angles[1].preset_angle is not None:
            config_update.preset_angles_1_preset_angle = asset.preset_angles[1].preset_angle
        if asset.preset_angles[1].preset_angle is not None:
            config_update.preset_angles_1_nearest_enabled = asset.preset_angles[1].nearest_enabled
        if asset.preset_angles[2].preset_angle is not None:
            config_update.preset_angles_2_preset_angle = asset.preset_angles[2].preset_angle
        if asset.preset_angles[2].preset_angle is not None:
            config_update.preset_angles_2_nearest_enabled = asset.preset_angles[2].nearest_enabled
        if asset.preset_angles[3].preset_angle is not None:
            config_update.preset_angles_3_preset_angle = asset.preset_angles[3].preset_angle
        if asset.preset_angles[3].preset_angle is not None:
            config_update.preset_angles_3_nearest_enabled = asset.preset_angles[3].nearest_enabled
        if asset.preset_angles[4].preset_angle is not None:
            config_update.preset_angles_4_preset_angle = asset.preset_angles[4].preset_angle
        if asset.preset_angles[4].preset_angle is not None:
            config_update.preset_angles_4_nearest_enabled = asset.preset_angles[4].nearest_enabled
        if asset.preset_angles[5].preset_angle is not None:
            config_update.preset_angles_5_preset_angle = asset.preset_angles[5].preset_angle
        if asset.preset_angles[5].preset_angle is not None:
            config_update.preset_angles_5_nearest_enabled = asset.preset_angles[5].nearest_enabled
        if asset.preset_angles[6].preset_angle is not None:
            config_update.preset_angles_6_preset_angle = asset.preset_angles[6].preset_angle
        if asset.preset_angles[6].preset_angle is not None:
            config_update.preset_angles_6_nearest_enabled = asset.preset_angles[6].nearest_enabled
        if asset.preset_angles[7].preset_angle is not None:
            config_update.preset_angles_7_preset_angle = asset.preset_angles[7].preset_angle
        if asset.preset_angles[7].preset_angle is not None:
            config_update.preset_angles_7_nearest_enabled = asset.preset_angles[7].nearest_enabled
        if asset.preset_angles[8].preset_angle is not None:
            config_update.preset_angles_8_preset_angle = asset.preset_angles[8].preset_angle
        if asset.preset_angles[8].preset_angle is not None:
            config_update.preset_angles_8_nearest_enabled = asset.preset_angles[8].nearest_enabled
        if asset.preset_angles[9].preset_angle is not None:
            config_update.preset_angles_9_preset_angle = asset.preset_angles[9].preset_angle
        if asset.preset_angles[9].preset_angle is not None:
            config_update.preset_angles_9_nearest_enabled = asset.preset_angles[9].nearest_enabled
        if asset.preset_angles[10].preset_angle is not None:
            config_update.preset_angles_10_preset_angle = asset.preset_angles[10].preset_angle
        if asset.preset_angles[10].preset_angle is not None:
            config_update.preset_angles_10_nearest_enabled = asset.preset_angles[10].nearest_enabled
        if asset.preset_angles[11].preset_angle is not None:
            config_update.preset_angles_11_preset_angle = asset.preset_angles[11].preset_angle
        if asset.preset_angles[11].preset_angle is not None:
            config_update.preset_angles_11_nearest_enabled = asset.preset_angles[11].nearest_enabled
        if asset.preset_angles[12].preset_angle is not None:
            config_update.preset_angles_12_preset_angle = asset.preset_angles[12].preset_angle
        if asset.preset_angles[12].preset_angle is not None:
            config_update.preset_angles_12_nearest_enabled = asset.preset_angles[12].nearest_enabled
        if asset.preset_angles[13].preset_angle is not None:
            config_update.preset_angles_13_preset_angle = asset.preset_angles[13].preset_angle
        if asset.preset_angles[13].preset_angle is not None:
            config_update.preset_angles_13_nearest_enabled = asset.preset_angles[13].nearest_enabled
        if asset.preset_angles[14].preset_angle is not None:
            config_update.preset_angles_14_preset_angle = asset.preset_angles[14].preset_angle
        if asset.preset_angles[14].preset_angle is not None:
            config_update.preset_angles_14_nearest_enabled = asset.preset_angles[14].nearest_enabled
        if asset.preset_angles[15].preset_angle is not None:
            config_update.preset_angles_15_preset_angle = asset.preset_angles[15].preset_angle
        if asset.preset_angles[15].preset_angle is not None:
            config_update.preset_angles_15_nearest_enabled = asset.preset_angles[15].nearest_enabled
        self.config_updates.append(config_update)

    def on_report_default_status(self, asset_after):
        updates = self._create_new_updates()
        tc_updates = updates.tc_updates.add()
        tc_updates.tc_snap_addr = self._get_asset_snap_address(asset_after)
        tc_updates.when.GetCurrentTime()  # Uses UTC time
        tc_updates.status_bits = asset_after.status_bits
        tc_updates.asset_status = terrasmart_cloud_pb2.TrackerControllerInstantUpdates.UNKNOWN
        tc_updates.last_reported.GetCurrentTime() 
        self._send_update(updates)

    def _add_asset_hist(self, asset):
        if asset.device:
            asset_update = terrasmart_cloud_pb2.AssetUpdate()
            snap_addr = self._get_asset_snap_address(asset)
            if snap_addr is not None:
                asset_update.snap_addr = snap_addr
            asset_update.when.GetCurrentTime()  # Uses UTC time
            asset_update.unit_temperature = asset.unit_temperature
            asset_update.up_time = asset.uptime
            self.asset_updates.append(asset_update)

    def _get_index(self, memory, data, sizex):
        got_index = 0
        for x in range(0, sizex):
            if memory[x][0] == data:
                got_index = x+1
        return got_index
    
    def _is_nc(self, asset):
        return (asset.model_device is not None and asset.model_device & 01)

    def _set_update(self, memory, snap_add, sizex, now_time, update_at_index, threshold):
        if update_at_index > 0:
            memory[update_at_index - 1][1] = now_time
            memory[update_at_index - 1][2] = threshold
            return 1

        for x in range(0, sizex):
            if x >= sizex - 1:
                log.exception('Too many weather stations. Update coding')
                memory[x][0] = snap_add
                memory[x][1] = now_time
                memory[x][2] = threshold
                return 0

            if len(memory[x][0]) < 3:
                memory[x][0] = snap_add
                memory[x][1] = now_time
                memory[x][2] = threshold
                return x

        return 0

    def on_asset_reboot(self, previous_uptime, asset):
        updates = self._create_new_updates()
        updates.asset_restarted.snap_addr = self._get_asset_snap_address(asset)
        updates.asset_restarted.when.GetCurrentTime()
        updates.asset_restarted.up_time = asset.uptime

        self._send_batch_updates(updates)
    
    def on_asset_preset_change(self, previous_panel_index, previous_command_state, asset):
        updates = self._create_new_updates()
        updates.asset_preset_changed.snap_addr = self._get_asset_snap_address(asset)
        updates.asset_preset_changed.when.GetCurrentTime()
        updates.asset_preset_changed.panel_index = asset.panel_index
        updates.asset_preset_changed.panel_command_state = asset.panel_command_state

        self._send_batch_updates(updates)

    def _add_motor_current_hist(self, asset):
        updates = self._create_new_updates()
        updates.motor_current_update.snap_addr = self._get_asset_snap_address(asset)
        updates.motor_current_update.when.GetCurrentTime()
        updates.motor_current_update.peak_motor_inrush_current = asset.peak_motor_inrush_current
        updates.motor_current_update.peak_motor_current = asset.peak_motor_current
        updates.motor_current_update.average_motor_current = asset.average_motor_current
        updates.motor_current_update.ending_motor_current = asset.ending_motor_current

        self._send_batch_updates(updates)


    def send_increase_reporting(self, increase_avg_wind_reporting, increase_wind_gust_reporting, increase_avg_snow_reporting, increase_panel_snow_reporting, asset_source = None):
        updates = self._create_new_updates()
        updates.weather_reporting_update.snap_addr = updates.nc_snap_addr
        updates.weather_reporting_update.when.GetCurrentTime()  # Uses UTC time
        if asset_source is not None:
            updates.weather_reporting_update.asset_snap_addr = self._get_asset_snap_address(asset_source)
        updates.weather_reporting_update.increase_avg_wind_reporting = increase_avg_wind_reporting
        updates.weather_reporting_update.increase_wind_gust_reporting = increase_wind_gust_reporting
        updates.weather_reporting_update.increase_avg_snow_reporting = increase_avg_snow_reporting
        updates.weather_reporting_update.increase_panel_snow_reporting = increase_panel_snow_reporting
        self._send_batch_updates(updates)

    def send_weather_hist(self, asset):
        self._add_weather_hist(asset)


if __name__ == '__main__':
    import os
    import copy
    from ts_common import tornado_paho
    from netcontrol import Asset, SystemSettings
    import pvlib
    from SunPositionTracker import SunriseSunset
    import tornado.ioloop
    from controller_enums import TrackerPresets

    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    a = Asset.Asset(asset_id=0)
    a.last_reported = datetime.datetime(2015, 7, 15, 9, 44, 32, 599823)
    b = copy.copy(a)
    b.hours_since_accumulators_reset = 23
    a.snap_address = "\x01\x02\x03"
    a.current_angle = -5.2
    a.requested_angle = -5.3
    a.status_bits = 2
    a.hours_since_accumulators_reset = 0
    a.solar_power_previous_hour = 1.0
    a.battery_power_previous_hour = 2.0
    a.motor_power_previous_hour = 3.0
    a.charger_power_current_hour = 4.0
    a.average_angular_error_previous_hour = 5.0
    a.solar_power_previous_day = 6.0
    a.battery_power_previous_day = 7.0
    a.motor_power_previous_day = 8.0
    a.charger_power_current_day = 9.0
    a.average_angular_error_previous_day = 10.0
    a.solar_voltage = 12
    a.solar_current = 1.5
    a.external_input_2_voltage = 24
    a.external_input_2_current = 1.0
    a.battery_current = 1.7
    a.battery_voltage = 11.7
    a.radio_link_quality = 1
    a.radio_mesh_depth = 1
    b = copy.copy(a)
    b.hours_since_accumulators_reset = 23
    a.status_bits = 1
    a.has_weather_sensor = True
    a.wind_speed = 80
    a.average_wind_speed = 90
    a.unit_temperature = 20
    a.device = "test_device"
    #a.radio_mac_addr = "macadd"
    a.radio_channel = "radio_channel"
    a.radio_network_id = "networkid"
    a.radio_firmware = "firmW"
    a.radio_script_version = "0.777"
    a.radio_script_crc = "bb"
    a.radio_link_quality = 100
    a.radio_mesh_depth = 22
    a.radio_polls_sent = 33


    def get_sun_rise_set(dt=datetime.datetime.utcnow(), latitude=26.432691, longitude=-81.799176):
        result = pvlib.solarposition.get_sun_rise_set_transit(dt, latitude, longitude)

        return SunriseSunset(sunrise=result.sunrise[0].to_pydatetime(),
                             sunset=result.sunset[0].to_pydatetime())


    class FakeSite(object):
        def __init__(self):
            self.operational_mode = TrackerPresets.FLAT.value
            self.tracking_enable = True
            # self.tracking_enable = False
            self.backtracking_enable = True


    my_site = FakeSite()

    event_log_handler = event_log.MemoryEventLog(capacity=500)
    mqtt_client = tornado_paho.TornadoPahoMqttClient(mqtt_broker='a2dlentyd00pdf.iot.us-east-2.amazonaws.com',
                                                     mqtt_port=8883,
                                                     ssl_options={
                                                         "certfile": os.path.join(
                                                             SystemSettings.SYSTEM_CONFIG_DIRECTORY, "my_aws_iot.pem"),
                                                         "keyfile": os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY,
                                                                                 "private.key"),
                                                         "ca_certs": r'VeriSign-Class 3-Public-Primary-Certification-Authority-G5.pem'
                                                     })
    cloud_updater = TerraSmartCloudUpdater(mqtt_client,
                                           lambda: '\xFF\xFE\xFD',
                                           lambda *args, **kwargs: get_sun_rise_set(*args, **kwargs),
                                           my_site,
                                           event_log_handler,
                                           enabled=True)


    
    tornado.ioloop.IOLoop.current().add_timeout(datetime.timedelta(seconds=2),
                                                cloud_updater.on_asset_changed, b, a)
    tornado.ioloop.IOLoop.current().add_timeout(datetime.timedelta(seconds=4),
                                                tornado.ioloop.IOLoop.current().stop)
    tornado.ioloop.IOLoop.current().start()
