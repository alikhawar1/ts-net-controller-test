import os
import json
import boto3
import logging
import monotonic
from enum import Enum
from botocore.client import Config
from botocore.exceptions import NoCredentialsError, EndpointConnectionError, ClientError
from controller_enums import CommandedRowState, TrackerPresets, EventType, CommandStatus
from utils import config_parser

log = logging.getLogger(__name__)
event_log = logging.getLogger('EventLog')


class BotoClient(object):
    """  """
    def __init__(self, config):
        try:
            self.bucket = config['BUCKET']
            self.command_file = config['HOST_ENV'] + config['FILE_NAME']
            self.s3 = boto3.resource('s3',
                    aws_access_key_id=config['AWS_ACCESS_KEY_ID'],
                    aws_secret_access_key=config['AWS_SECRET_ACCESS_KEY'],
                    config=Config(connect_timeout=5, read_timeout=30, retries={'max_attempts': 0})
                    )

        except Exception as ex:
            event_log.error("Cloud config file not found", extra=EventType.INDIVIDUAL_ASSET._dict)

    def download_file(self):
        try:
            response = self.s3.Object(bucket_name=self.bucket, key=self.command_file).get()
            result = response['Body'].read()
            data = self.json_to_dict(json.loads(result))
            event_log.info("Command file download completed", extra=EventType.INDIVIDUAL_ASSET._dict)
            return data
        except EndpointConnectionError as ex:
            event_log.error("Connection Error: " + ex.message, extra=EventType.INDIVIDUAL_ASSET._dict)
            return None
        except ClientError as ex:
            event_log.error("File Not Found: File is missing or file name is incorrect", extra=EventType.INDIVIDUAL_ASSET._dict)
            return None
        except Exception as ex:
            event_log.error('Error while reading resource from cloud: ' + ex.message, extra=EventType.INDIVIDUAL_ASSET._dict)
            return None
    
    def upload_to_s3(self, local_file):
        try:
            if os.path.exists(local_file):
                data = open(local_file, 'rb')
                self._upload_data(self.cloud_file_name, data)
                return True
            return False
        except NoCredentialsError:
            log.error("Credentials not available")
            return False
        except:
            log.exception("Exception while uploading data to cloud")
            return False

    def _upload_data(self, file, data):
        self.s3.Bucket(self.bucket).put_object(Key=file, Body=data)
    
    def json_to_dict(self, json_object):
        list = json_object['commands']
        if len(list) == 0:
            return {}
        dict = {}
        for cmd in list:
            command_obj = self.to_object(cmd)
            dict.update(command_obj)
        return dict
    
    def to_object(self, cmd):
        try:
            if len(cmd) == 0:
                return {}
            dict = {}
            asset_id = cmd['asset_id']
            cmd_state = CommandedRowState(cmd['cmd_state'])
            last_state = CommandedRowState(cmd['last_state'])
            last_preset = cmd['last_preset']
            arg = cmd['param']
            if last_state == CommandedRowState.PANEL_CMD_MOVE_TO_PRESET:
                last_preset = TrackerPresets(last_preset)
                
            if cmd_state == CommandedRowState.PANEL_CMD_MOVE_TO_PRESET:
                arg = TrackerPresets(arg)
                    
            user_name = cmd['username']
            email = cmd['email']
            dict.update({
                asset_id: { 
                    'last_requested_time': monotonic.monotonic(),
                    'last_preset': last_preset,
                    'last_state': last_state,
                    'cmd_state': cmd_state,
                    'username': user_name,
                    'status': CommandStatus.NEW,
                    'email': email,
                    'arg': arg,
                } 
            })
            return dict
        except KeyError:
            print("Unable to parse response json")
            return {}
       

if __name__ == "__main__":
    boto_client = BotoClient()
    # boto_client.upload_to_s3(LOCAL_FILE)
    result = boto_client.download_file()
    
    print result
