"""
Ethernet.py - added to the codebase when we started supporting STATIC IP
addresses on the USB Ethernet adaptor.

The implementation currently seems overkill, but the assumption is that
additional fields and capabilities will be added in the future, and THEN
we will be glad we separated this out from terrasmart_site.py / ts_site.py.
"""


import logging

from utils.PickleHelper import PickleHelper

import gateway_utils


ETHERNET_CONFIG_FILE = 'ethernet.pkl'

log = logging.getLogger(__name__)


class EthernetPersistableConfig(object):
    """
    The point of this class is to separate out the PERSISTED CONFIG
    fields from the remaining LIVE DATA fields. This allows us to
    only persist (pickle) those fields. See Ethernet.
    """
    def __init__(self):
        """Initializing these fields implicitly defines the "shape" of the object"""
        self.enable_static_ip_address = None
        self.requested_static_ip_address = None
        self.requested_static_subnet_mask = None

    def default_config(self):
        self.enable_static_ip_address = False
        self.requested_static_ip_address = ''
        self.requested_static_subnet_mask = ''


class Ethernet(EthernetPersistableConfig):
    """Support for the (optional) USB Ethernet Adaptor"""
    def __init__(self):
        EthernetPersistableConfig.__init__(self)

        self._pickle_helper = PickleHelper(self, EthernetPersistableConfig, ETHERNET_CONFIG_FILE)

        if not self.load_config():
            self.default_config()
            self.save_config()

        self.apply_static_ip_address_settings()

    def load_config(self):
        """Try to load config from disk (previously generated pickle file"""
        return self._pickle_helper.load_config()

    def save_config(self):
        """save config out to disk (pickle file)"""
        return self._pickle_helper.save_config()

    def update_config(self, new_config_dict):
        """update config based on user input"""
        for key in new_config_dict:
            if hasattr(self, key):
                setattr(self, key, new_config_dict[key])
        self.save_config()
        self.apply_static_ip_address_settings()

    def apply_static_ip_address_settings(self):
        if self.enable_static_ip_address == True:
            gateway_utils.configure_static_ip(self.requested_static_ip_address,
                                              self.requested_static_subnet_mask)
        else:
            gateway_utils.configure_static_ip()

    @property
    def dhcp_ip_address(self):
        return gateway_utils.get_dhcp_ip_address()

    @property
    def dhcp_subnet_mask(self):
        return gateway_utils.get_dhcp_subnet_mask()

    @property
    def static_ip_address(self):
        return gateway_utils.get_static_ip_address()

