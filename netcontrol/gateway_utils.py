import sys

if sys.platform == 'linux2':
    import subprocess


def halt():
    """
    Calls halt on the system
    Note you must have set the appropriate permissions to perform this action
    :return: None
    """
    # The following added for when doing development/testing on a Windows PC
    if sys.platform == 'win32':
        exit()

    if sys.platform != 'linux2':
        raise SystemExit("Halt not supported on this platform")

    # See file history for a previous version that used ConsoleKit to
    # request the shutdown. This was not usable on the E12 gateways
    # because of the "power down == reboot" issue.
    subprocess.Popen(['sudo', '/sbin/shutdown', '-H', '+1'])

def shutdown_wifi():
    """
    In testing, we discovered that removal of the USB WiFi dongle would
    lockup the E12 gateway 10-20% of the time. This routine uses standard
    Linux commands to put the USB WiFi dongle into an inactive state so
    that it can be removed without locking up the system.
    :return: None
    """
    if sys.platform != 'linux2':
        raise SystemExit("WiFi shutdown not supported on this platform")

    subprocess.Popen(['sudo', '/usr/sbin/service', 'hostapd', 'stop'])
    subprocess.Popen(['sudo', '/sbin/ifdown', 'wlan0'])

def silent_command(command):
    """Issue command to shell, capture/return output."""
    output = ''
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    for c in iter(lambda: process.stdout.read(1), ''):
        output += c
    # If there was no stdout output, try stderr
    if len(output) == 0:
        for c in iter(lambda: process.stderr.read(1), ''):
            output += c
    return output

def get_ip_parameter(which_interface, search_key, text_when_missing):
    output = silent_command("ifconfig %s" % (which_interface,))
    if output.find("not found") != -1:
        return "Not present"
    if output.find(search_key) == -1:
        return text_when_missing
    output = output.partition(search_key)
    output = output[2]
    output = output.partition(" ")
    output = output[0]
    return output

def get_ip_address(which_interface, text_when_missing):
    output = get_ip_parameter(which_interface, "inet addr:", text_when_missing)
    return output

def get_subnet_mask(which_interface, text_when_missing):
    output = get_ip_parameter(which_interface, "Mask:", text_when_missing)
    return output

def get_dhcp_ip_address():
    result = get_ip_address("eth1", "Pending DHCP")
    return result

def get_dhcp_subnet_mask():
    result = get_subnet_mask("eth1", "Pending DHCP")
    return result

def get_static_ip_address():
    result = get_ip_address("eth1:0", "Not configured")
    return result

#
# NOTE! - Changing network configuration using ifconfig requires
# sudoer priviledges! This routine cannot do it's job without
# additional Linux configuration beign done first
#
def configure_static_ip(requested_ip_address="", requested_subnet_mask=""):
    command = "sudo ifconfig eth1:0 "
    if requested_ip_address != "":
        command += requested_ip_address
        if requested_subnet_mask != "":
            command += " netmask " + requested_subnet_mask
    else:
        command += "down"
    try:
      output = silent_command(command)
      if output != "":
          return False
    except Exception, e:
        return False
    return True
