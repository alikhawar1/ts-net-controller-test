"""
command_handler.py - the purpose of this module is to decompose larger function
blocks into smaller, separate and easy to maintainable function blocks. It only 
serves row_commander.py module to help carry out its responsibility

"""

import math
import logging
import monotonic
from controller_enums import EventType, CommandedRowState, CommandStatus, TCTrackingStatusBits, CommandErrorCodes

log = logging.getLogger(__name__)
event_log = logging.getLogger('EventLog')

class CommandHandler(object):
    REQUEST_RESEND_TIME_MAX = 5.0 * 60 # 5 Minutes timeout to resend a command for backup.
    REQUEST_RESEND_TIME_MIN = 1.0 * 60 # 1 Minutes timeout to resend a command if asset has not yet acked.

    def __init__(self, unicaster, asset_manager, persistent_service, asset_commander, on_state_change):
        if unicaster is None:
            raise Exception("Error: RowCommander requires an Asset Unicaster!")
        else:
            self._asset_unicaster = unicaster
        self._asset_manager = asset_manager
        self.persistent_service = persistent_service
        self._asset_commander = asset_commander
        self.on_state_change = on_state_change


    def assert_asset_online_back(self, asset_id):
        """ Checks whether RowBox has returned from IRC to normal """
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            return asset.tracking_status != TCTrackingStatusBits.INDIVIDUAL_ROW_CONTROL.value
        return False
    
    def local_estop_pressed(self, asset_id):
        """ Is RB in LOCAL_ESTOP mode"""
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            return asset.panel_command_state == CommandedRowState.PANEL_CMD_LOCAL_ESTOP.value
        return False
    
    def nccb_estop_pressed(self, asset_id):
        """ Is RB in LOCAL_ESTOP mode"""
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            return asset.panel_command_state == CommandedRowState.PANEL_CMD_NCCB_STOP.value
        return False
    
    def under_hhc(self, asset_id):
        """ Is RB in HANDHELD_ROW_CONTROL mode"""
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            return asset.tracking_status == TCTrackingStatusBits.HANDHELD_ROW_CONTROL.value
        return False

    def assert_angles_match(self, asset_id, arg):
        """ Is current angle in specified delta of requested angle """
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            return abs(arg - asset.current_angle) <= 0.5
        return False
    
    def assert_preset_angle_changed(self, asset_id):
        """ Is current angle in specified delta of target angle """
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            return abs(asset.requested_angle - asset.current_angle) <= 0.5
        return False

    def assert_asset_vestoped(self, asset_id):
        """ Checks asset is in V-ESTOP commanded state """
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            return asset.panel_command_state == CommandedRowState.PANEL_CMD_V_ESTOP.value
        return False
    
    def assert_preset_state_changed(self, asset_id, arg):
        """ Assets reported panel index is equal to requested panel index """
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            return asset.panel_index == arg.value
        return False

    def assert_asset_acked_stop_cmd(self, asset_id):
        """ RB has acknowledged to have receided STOP command 
            and stopping any panel movement
        """
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            return asset.tracking_status == TCTrackingStatusBits.INDIVIDUAL_ROW_CONTROL.value
        return False
    
    def assert_preset_angles_fetched(self, asset_id, index):
        """ RB has reported panel indices to angles mapping """
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            preset_angle = asset.preset_angles[index]
            return preset_angle.preset_angle is not None
        return False
    
    def assert_asset_acked_abs_cmd(self, asset_id, angle):
        """ RB has acknowledged the MOVE_TO_ABS command in IRC mode """
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            return asset.requested_angle == angle
        return False
    
    def assert_asset_acked_preset_cmd(self, asset_id, index):
        """ RB has acknowledged the MOVE_TO_PRESET command in IRC mode """
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            preset_angle = asset.preset_angles[index.value]
            index_match = (asset.panel_index == index.value) # index match
            angles_match = (asset.requested_angle * 10 == preset_angle.preset_angle) # angles match
            nearest_enabled = preset_angle.nearest_enabled # nearest is enabled
            inverse_comp = (asset.requested_angle * 10 == -preset_angle.preset_angle)
            return (index_match and (angles_match or (nearest_enabled and inverse_comp)))
        else:
            return False
    
    def assert_asset_received_resume_cmd(self, asset_id):
        """ Has RB received resume normal command """
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            return asset.offline_minutes == 0
        return False

    def is_asset_offline(self, asset_id):
        """ Checks is asset offline or not """
        asset = self._asset_manager.get_asset_if_known(asset_id)
        if asset:
            return asset.offline
        return True

    def get_post_exec_msg(self, last_state, last_preset):
        if last_state == CommandedRowState.PANEL_CMD_MOVE_TO_PRESET:
            return ' It was previously in - %s %s' % (last_state.description, last_preset.description)
        elif last_state == CommandedRowState.PANEL_CMD_MOVE_TO_ABS:
            return ' It was previously in - %s %i degree' % (last_state.description, last_preset)
        else:
            return ' It was previously in - %s state' % last_state.description

    def get_pre_exec_msg(self, cmd_state, param):
        if cmd_state == CommandedRowState.PANEL_CMD_MOVE_TO_PRESET:
            return param.description
        elif cmd_state == CommandedRowState.PANEL_CMD_MOVE_TO_ABS:
           return '%i degree' %param
        else:
            return ''

    def on_new_command(self, asset_id, user, cmd_state, arg):
        """ Command handler when a new command is received  """
        try:
            vals = {}
            snap_addr = asset_id.decode('hex')
            self.on_state_change(snap_addr, cmd_state, CommandStatus.NEW, CommandErrorCodes.CLEAR)
            msg = '%s commanded asset (%s) to - %s ' % (user, asset_id, cmd_state.description)
            msg += self.get_pre_exec_msg(cmd_state, arg)
            event_log.info(msg, extra=EventType.INDIVIDUAL_ASSET._dict)
            event_log.info('NC is commanding asset (%s)' %asset_id, extra=EventType.INDIVIDUAL_ASSET._dict)
            vals['status'] = CommandStatus.UNKNOWN
            if len(vals) > 0:
                self.persistent_service.update(asset_id, vals)
        except Exception as e:
            log.error('Error serving new_command - callback')
    
    def on_local_estop(self, asset_id, cmd_state):
        """ Handler to handle IRC command in case asset is in LOCAL_ESTOP mode """
        try:
            snap_addr = asset_id.decode('hex')
            self.persistent_service.remove(asset_id)
            msg = 'Unable to serve the command. Asset (%s) is in - Local Emergency Stop.' % asset_id
            event_log.info(msg, extra=EventType.INDIVIDUAL_ASSET._dict)
            self.on_state_change(snap_addr, cmd_state, CommandStatus.FAILED, CommandErrorCodes.LOCAL_ESTOP_ENGAGED)
        except Exception as e:
            log.error('Error serving new command, asset is in local_estop ')
    
    def on_hhc(self, asset_id, cmd_state):
        """ Handler to handle IRC command in case asset is in LOCAL_ESTOP mode """
        try:
            snap_addr = asset_id.decode('hex')
            self.persistent_service.remove(asset_id)
            msg = 'Unable to serve the command. Asset (%s) is in - HANDHELD_ROW_CONTROL mode.' % asset_id
            event_log.info(msg, extra=EventType.INDIVIDUAL_ASSET._dict)
            self.on_state_change(snap_addr, cmd_state, CommandStatus.FAILED, CommandErrorCodes.UNDER_HHC_CONTROL)
        except Exception as e:
            log.error('Error serving new command, asset is in local_estop ')
    
    def on_nccb_estop(self, asset_id, cmd_state):
        """ Handler to handle IRC command in case asset is in NCCB_ESTOP mode """
        try:
            snap_addr = asset_id.decode('hex')
            self.persistent_service.remove(asset_id)
            msg = 'Unable to serve the command. NC Emergency Stop button engaged.'
            event_log.info(msg, extra=EventType.INDIVIDUAL_ASSET._dict)
            self.on_state_change(snap_addr, cmd_state, CommandStatus.FAILED, CommandErrorCodes.NCCB_ESTOP_ENGAGED)
        except Exception as e:
            log.error('Error serving new command, asset is in nc estop pressed')

    def on_asset_offline(self, asset_id, cmd_state):
        """ Handler to handle IRC command in case asset is offline """
        try:
            vals = {}
            snap_addr = asset_id.decode('hex')
            self.persistent_service.remove(asset_id)
            msg = 'Unable to serve the command. Asset (%s) is offline.' % asset_id
            event_log.info(msg, extra=EventType.INDIVIDUAL_ASSET._dict)
            self.on_state_change(snap_addr, cmd_state, CommandStatus.FAILED, CommandErrorCodes.ASSET_OFFLINE)
        except Exception as e:
            log.error('Error serving asset_offlin callback')
    
    def on_stop_command(self, status, asset_id, cmd_state, last_state, last_preset, last_requested_time):
        """ IRC STOP command handler """
        try:
            vals = {}
            snap_addr = asset_id.decode('hex')
            if status == CommandStatus.UNKNOWN:
                self._asset_unicaster.stop(snap_addr)
                self.on_state_change(snap_addr, cmd_state, CommandStatus.REQUESTED, CommandErrorCodes.CLEAR)
                monotime_now = monotonic.monotonic()
                vals['last_requested_time'] = monotime_now
                vals['status'] = CommandStatus.REQUESTED

            elif status == CommandStatus.REQUESTED:
                if self.assert_asset_acked_stop_cmd(asset_id):
                    self.on_state_change(snap_addr, cmd_state, CommandStatus.ACKNOWLEDGED, CommandErrorCodes.CLEAR)
                    vals['status'] = CommandStatus.ACKNOWLEDGED
                else:
                    monotime_now = monotonic.monotonic()
                    time_diff = monotime_now - last_requested_time
                    if time_diff >= CommandHandler.REQUEST_RESEND_TIME_MIN:
                        self._asset_unicaster.stop(snap_addr)
                        last_requested_time = monotime_now
                        vals['last_requested_time'] = last_requested_time

            elif status == CommandStatus.ACKNOWLEDGED:
                if self.assert_asset_vestoped(asset_id):
                    vals['status'] = CommandStatus.COMPLETED

            elif status == CommandStatus.COMPLETED:
                msg = 'Asset (%s) Virtual E-STOP engaged.' % asset_id
                msg += self.get_post_exec_msg(last_state, last_preset)
                event_log.info(msg, extra=EventType.INDIVIDUAL_ASSET._dict)
                self.on_state_change(snap_addr, cmd_state, CommandStatus.COMPLETED, CommandErrorCodes.CLEAR)
                vals['status'] = CommandStatus.DONE
                
            if status != CommandStatus.NEW and status != CommandStatus.UNKNOWN:
                monotime_now = monotonic.monotonic()
                time_diff = monotime_now - last_requested_time
                if time_diff >= CommandHandler.REQUEST_RESEND_TIME_MAX:
                    self._asset_unicaster.stop(snap_addr)
                    vals['last_requested_time'] = monotime_now
            
            if len(vals) > 0:
                self.persistent_service.update(asset_id, vals)
        except Exception as e:
            log.error('Error serving stop_command command')

    def on_move_to_abs(self, status, asset_id, cmd_state, last_state, last_preset, arg, last_requested_time):
        """ IRC MOVE_TO_ABS command handler """
        try:
            vals = {}
            snap_addr = asset_id.decode('hex')
            if status == CommandStatus.UNKNOWN:
                self._asset_unicaster.go_to_abs(snap_addr, arg * 10)
                self.on_state_change(snap_addr, cmd_state, CommandStatus.REQUESTED, CommandErrorCodes.CLEAR)
                monotime_now = monotonic.monotonic()
                vals['last_requested_time'] = monotime_now
                vals['status'] = CommandStatus.REQUESTED

            elif status == CommandStatus.REQUESTED:
                if self.assert_asset_acked_abs_cmd(asset_id, arg):
                    self.on_state_change(snap_addr, cmd_state, CommandStatus.ACKNOWLEDGED, CommandErrorCodes.CLEAR)
                    vals['status'] = CommandStatus.ACKNOWLEDGED
                else:
                    monotime_now = monotonic.monotonic()
                    time_diff = monotime_now - last_requested_time
                    if time_diff >= CommandHandler.REQUEST_RESEND_TIME_MIN:
                        self._asset_unicaster.go_to_abs(snap_addr, arg * 10)
                        last_requested_time = monotime_now
                        vals['last_requested_time'] = last_requested_time

            elif status == CommandStatus.ACKNOWLEDGED:
                if self.assert_angles_match(asset_id, arg):
                    vals['status'] = CommandStatus.COMPLETED

            elif status == CommandStatus.COMPLETED:
                msg = 'Asset (%s) moved to %i degree.' % (asset_id , arg)
                msg += self.get_post_exec_msg(last_state, last_preset)
                event_log.info(msg, extra=EventType.INDIVIDUAL_ASSET._dict)
                self.on_state_change(snap_addr, cmd_state, CommandStatus.COMPLETED, CommandErrorCodes.CLEAR)
                vals['status'] = CommandStatus.DONE
                
            if status != CommandStatus.NEW and status != CommandStatus.UNKNOWN:
                monotime_now = monotonic.monotonic()
                time_diff = monotime_now - last_requested_time
                if time_diff >= CommandHandler.REQUEST_RESEND_TIME_MAX:
                    self._asset_unicaster.go_to_abs(snap_addr, arg * 10)
                    vals['last_requested_time'] = monotime_now

            if len(vals) > 0:
                self.persistent_service.update(asset_id, vals)

        except Exception as e:
            log.error('Error serving move_to_abs command')

    def on_move_to_preset(self, status, asset_id, cmd_state, last_state, last_preset, arg, last_requested_time):
        """ IRC MOVE_TO_PRESET command handler """
        try:
            vals = {}
            snap_addr    = asset_id.decode('hex')
            if status == CommandStatus.UNKNOWN:
                if self.assert_preset_angles_fetched(asset_id, arg.value):
                    self._asset_unicaster.go_to_preset(snap_addr, arg.value)
                    self.on_state_change(snap_addr, cmd_state, CommandStatus.REQUESTED, CommandErrorCodes.CLEAR)
                    monotime_now = monotonic.monotonic()
                    vals['last_requested_time'] = monotime_now
                    vals['status'] = CommandStatus.REQUESTED

            elif status == CommandStatus.REQUESTED:
                if self.assert_asset_acked_preset_cmd(asset_id, arg):
                    self.on_state_change(snap_addr, cmd_state, CommandStatus.ACKNOWLEDGED, CommandErrorCodes.CLEAR)
                    vals['status'] = CommandStatus.ACKNOWLEDGED
                else:
                    monotime_now = monotonic.monotonic()
                    time_diff = monotime_now - last_requested_time
                    if time_diff >= CommandHandler.REQUEST_RESEND_TIME_MIN:
                        self._asset_unicaster.go_to_preset(snap_addr, arg.value)
                        last_requested_time = monotime_now
                        vals['last_requested_time'] = last_requested_time

            elif status == CommandStatus.ACKNOWLEDGED:
                if self.assert_preset_angle_changed(asset_id):
                    vals['status'] = CommandStatus.COMPLETED
                    
            elif status == CommandStatus.COMPLETED:
                msg = 'Asset (%s) preset state changed to - %s %s.' % (asset_id, cmd_state.description, arg.description)
                msg += self.get_post_exec_msg(last_state, last_preset)
                event_log.info(msg, extra=EventType.INDIVIDUAL_ASSET._dict)
                self.on_state_change(snap_addr, cmd_state, CommandStatus.COMPLETED, CommandErrorCodes.CLEAR)
                vals['status'] = CommandStatus.DONE
                
            if status != CommandStatus.NEW and status != CommandStatus.UNKNOWN:
                monotime_now = monotonic.monotonic()
                time_diff = monotime_now - last_requested_time
                if time_diff >= CommandHandler.REQUEST_RESEND_TIME_MAX:
                    self._asset_unicaster.go_to_preset(snap_addr, arg.value)
                    vals['last_requested_time'] = monotime_now

            if len(vals) > 0:
                self.persistent_service.update(asset_id, vals)

        except Exception as e:
            log.error('Error serving move_to_preset command')

    def on_resume_normal(self, status, asset_id, cmd_state, last_state, last_preset, last_requested_time):
        """ IRC MOVE_TO_NONE (resume normal) command handler """
        try:
            vals = {}
            snap_addr    = asset_id.decode('hex')
            if status == CommandStatus.UNKNOWN:
                self._asset_unicaster.resume_normal(snap_addr)
                self.on_state_change(snap_addr, cmd_state, CommandStatus.REQUESTED, CommandErrorCodes.CLEAR)
                vals['status'] = CommandStatus.REQUESTED

            elif status == CommandStatus.REQUESTED:
                if self.assert_asset_received_resume_cmd(asset_id):
                    self._asset_commander.skip_ahead()
                    self.on_state_change(snap_addr, cmd_state, CommandStatus.ACKNOWLEDGED, CommandErrorCodes.CLEAR)
                    vals['status'] = CommandStatus.ACKNOWLEDGED
                else:
                    monotime_now = monotonic.monotonic()
                    time_diff = monotime_now - last_requested_time
                    if time_diff >= CommandHandler.REQUEST_RESEND_TIME_MIN:
                        self._asset_unicaster.resume_normal(snap_addr)
                        last_requested_time = monotime_now
                        vals['last_requested_time'] = last_requested_time

            elif status == CommandStatus.ACKNOWLEDGED:
                if self.assert_asset_online_back(asset_id):
                    vals['status'] = CommandStatus.COMPLETED
                    
            elif status == CommandStatus.COMPLETED:
                self.persistent_service.remove(asset_id)
                msg = 'Asset (%s) resumed tracking.' % asset_id
                msg += self.get_post_exec_msg(last_state, last_preset)
                event_log.info(msg, extra=EventType.INDIVIDUAL_ASSET._dict)
                self.on_state_change(snap_addr, cmd_state, CommandStatus.COMPLETED, CommandErrorCodes.CLEAR)

            if len(vals) > 0:
                self.persistent_service.update(asset_id, vals)

        except Exception as e:
            log.error('Error serving resume_normal command')


if __name__ == "__main__":
    import logging
    from enum import Enum
    from persistent_state import PersistentState
    from controller_enums import EventType, CommandedRowState, TrackerPresets

    logging.basicConfig(level=logging.DEBUG)