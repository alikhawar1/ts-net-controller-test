"""
Asset.py - a data object for TC, NCCB, and possibly WX info.
"""

import binascii
import copy_reg
import datetime

from controller_enums import TCStatusBits, TCTrackingStatusBits, SensorConfigFlags


class AssetSensor(object):
    """
    A sensor connected to an asset
    """

    def __init__(self, num):
        self.sensor_num = num
        self.flags = None
        self.sensor_adc_mode = None
        self.sensor_adc_value = None
        self.sensor_digital_input_value = None

    @property
    def sensor_flags(self):
        flags = []

        if self.flags:
            if self.flags & SensorConfigFlags.EXTERNAL_POWER.value:
                flags.append(SensorConfigFlags.EXTERNAL_POWER.value)
            if self.flags & SensorConfigFlags.ENABLE_DIN.value:
                flags.append(SensorConfigFlags.ENABLE_DIN.value)

        return flags


class AssetPresetAngle(object):
    """
    A preset angle
    """

    def __init__(self, num):
        self.preset_num = num
        self.nearest_enabled = None
        self.preset_angle = None


class AssetSegment(object):
    """
    A segment is a portion of a row that has independent backtracking parameters
    """

    def __init__(self, num):
        self.segment_num = num
        self.panel_array_width = None
        self.spacing_to_east = None
        self.spacing_to_west = None
        self.delta_height_east = None
        self.delta_height_west = None

# NOTE! - in the following, "solar" could also be considered "external_input_1"
# (The power inputs became more "general purpose" over time)
class Asset(object):
    """The application maintains one of these objects for every TC, WX, and NCCB on the network"""
    MAX_SENSORS = 8
    MAX_PRESETS = 16
    MAX_SEGMENTS = 2  # (used with backtracking)

    def __init__(self, asset_id):
        """Constructor"""
        self.id = asset_id
        self.last_reported = None
        self.snap_address = None
        self.config_timestamp = None
        self.offline = None  # Will be updated by the AssetPoller

        #
        # Decoded version of "r" - radio information from the Radio Module
        #
        self.radio_link_quality = None
        self.radio_mesh_depth = None
        self.radio_channel = None
        self.radio_network_id = None
        self.mac_addr = None  # See also property radio_mac_addr
        self.radio_script_version = None
        self.radio_script_crc = None
        self.radio_firmware = None
        self.radio_firmware_string = None
        self.radio_forwarding_mask = None
        self.is_a_repeater = None

        # Filled in by the Asset Poller
        self.radio_polls_sent = 0
        self.radio_poll_responses = 0

        # Used by the Asset Updater to prevent acting too soon
        self.radio_polls_sent_at_upload_time = 0

        #
        # Decoded version of "n" - the new data added 06/2020
        #
        self.external_input_2_voltage = 0
        self.external_input_2_current = 0

        self.external_input_2_power_current_hour = 0
        self.external_input_2_power_previous_hour = 0
        self.external_input_2_power_current_day = 0
        self.external_input_2_power_previous_day = 0
        # The following was added to blob "n" for blob version 1
        self.misc_status_bits = 0  # individual status bits
        # The following was added to blob "n" for blob version 2
        self.ending_motor_current = 0

        #
        # Decoded version of "s" - status (dynamic data) from the STM32
        #
        self.status_bits = 0

        self.uptime_days = 0
        self.uptime_hours = 0
        self.uptime_minutes = 0
        self.uptime_seconds = 0

        self.downtime_days = 0
        self.downtime_hours = 0
        self.downtime_minutes = 0
        self.downtime_seconds = 0

        self.offline_minutes = 0
        self.commissioning_mode = None

        self.current_angle = 0
        self.requested_angle = 0

        self.battery_voltage = 0
        self.solar_voltage = 0
        self.charger_current = 0

        self.unit_temperature = 0
        self.battery_temperature = 0

        self.battery_current = 0
        self.solar_current = 0
        self.charger_voltage = 0
        self.motor_current = 0
        self.heater_temperature = 0

        self.battery_charged = 0
        self.battery_health = 0

        self.tracking_status_bits = 0  # individual status bits

        self.solar_power_current_hour = 0
        self.solar_power_previous_hour = 0
        self.solar_power_current_day = 0
        self.solar_power_previous_day = 0

        self.battery_power_current_hour = 0
        self.battery_power_previous_hour = 0
        self.battery_power_current_day = 0
        self.battery_power_previous_day = 0

        self.charger_power_current_hour = 0
        self.charger_power_previous_hour = 0
        self.charger_power_current_day = 0
        self.charger_power_previous_day = 0

        self.motor_power_current_hour = 0
        self.motor_power_previous_hour = 0
        self.motor_power_current_day = 0
        self.motor_power_previous_day = 0

        self.average_angular_error_current_hour = 0
        self.average_angular_error_previous_hour = 0
        self.average_angular_error_current_day = 0
        self.average_angular_error_previous_day = 0

        self.hours_since_accumulators_reset = 0

        # These were added in BLOB_S version 1
        self.panel_index = 16
        self.panel_command_state = 0

        # These were added in BLOB_S version 2
        self.peak_motor_inrush_current = 0
        self.peak_motor_current = 0
        self.average_motor_current = 0
        self.more_status_bits = 0 # as of 10/25/2020, bit meanings still to be assigned

        #
        # Decoded version of "t" - user configurable data from the STM32
        #
        self.panel_horizontal_cal_angle = 0
        self.panel_min_cal_angle = 0
        self.panel_max_cal_angle = 0

        # Because we now support automated config download, we need a distinct value
        # to indicate we "have not heard gotten this value from the unit yet"
        self.config_label = 'UNKNOWN'
        self.site_name = ''
        self.location_lat = 0.0
        self.location_lng = 0.0
        self.location_text = ''

        self.segments = [AssetSegment(i) for i in xrange(Asset.MAX_SEGMENTS)]
        self.segments_change = False

        self.config_flags = 0
        self.wind_dir_offset = 0
        self.snow_sensor_height = 0

        # These were added in BLOB_T version 1
        self.battery_capacity = 0

        #
        # Decoded version of "u" - static information from the STM32
        #
        self.model_device = None # as reported by the asset
        self.device = '' # as generated from the above to use in the SLUI
        self.model = '' # as generated from the above to use in the SLUI
        self.hardware_rev = 0
        self.firmware_rev = 0
        self.firmware_rev_string = None
        self.has_tracker_hardware = False  # overridden if unit reports itself as a "Tracker"
        self.has_weather_sensor = False  # overridden if unit reports itself as "+WX"

        #
        # Decoded version of "v" - sensor configuration from the STM32
        # Decoded version of "w" - sensor data from the STM32
        #
        self.sensors = [AssetSensor(i) for i in xrange(Asset.MAX_SENSORS)]

        #
        # Decoded version of "x" - the current presets, and their "nearest" flags
        #
        self.preset_angles = [AssetPresetAngle(i) for i in xrange(Asset.MAX_PRESETS)]
        self.preset_angles_change = False

        #
        # Decoded version of "y" - the current weather data
        #
        self.has_reported_weather = False
        self.wind_speed = 0
        self.wind_direction = 0
        self.average_wind_speed = 0
        self.peak_wind_speed = 0
        self.snow_sensor_temperature = 0
        self.snow_sensor_distance = 0
        self.snow_depth = 0

        # Hold some data on behalf of the ts_cloud_updater
        self.requested_angle_last_reported = -1.0
        self.current_angle_last_reported = 181.0 # forcing a delta - our angles are 0-180!
        self.current_angle_last_reported_at = datetime.datetime.utcnow()
        self.site_commanded_state = None
        self.site_commanded_state_detail = None

    @property
    def radio_mac_addr(self):
        if self.mac_addr is None:
            return ''
        else:
            return binascii.hexlify(self.mac_addr)

    @property
    def uptime(self):
        return ((self.uptime_days * 86400) +
                (self.uptime_hours * 3600) +
                (self.uptime_minutes * 60) +
                self.uptime_seconds)

    @property
    def downtime(self):
        return ((self.downtime_days * 86400) +
                (self.downtime_hours * 3600) +
                (self.downtime_minutes * 60) +
                self.downtime_seconds)

    @property
    def statuses(self):
        """Returns a list of status enums"""
        statuses = []

        # TODO: Why does Graphene want me to return the value instead of the ENUM?

        # This first one actually comes from a different field -
        # In Asset objects, status_bits is not the same as tracking_status_bits
        # The reason we are doing this little bit of data migration is so that
        # a HIGH TEMP MOTOR CUTOFF will appear on the Asset List -> Status page
        if self.tracking_status_bits & TCTrackingStatusBits.HIGH_TEMPERATURE_MOTOR_CUTOFF.value:
            statuses.append(TCStatusBits.HIGH_TEMPERATURE_MOTOR_CUTOFF.value)

        if self.status_bits & TCStatusBits.OFFLINE.value:
            statuses.append(TCStatusBits.OFFLINE.value)
        if self.status_bits & TCStatusBits.OVER_CURRENT.value:
            statuses.append(TCStatusBits.OVER_CURRENT.value)
        if self.status_bits & TCStatusBits.MOTOR_TIMEOUT.value:
            statuses.append(TCStatusBits.MOTOR_TIMEOUT.value)
        if self.status_bits & TCStatusBits.CHARGER_FAULT.value:
            statuses.append(TCStatusBits.CHARGER_FAULT.value)
        if self.status_bits & TCStatusBits.MOTOR_CURRENT_LIMIT.value:
            statuses.append(TCStatusBits.MOTOR_CURRENT_LIMIT.value)
        if self.status_bits & TCStatusBits.BATTERY_LOW_MOVEMENT_RESTRICTED.value:
            statuses.append(TCStatusBits.BATTERY_LOW_MOVEMENT_RESTRICTED.value)
        if self.status_bits & TCStatusBits.MOTOR_TEMPERATURE_LIMIT.value:
            statuses.append(TCStatusBits.MOTOR_TEMPERATURE_LIMIT.value)
        if self.status_bits & TCStatusBits.ESTOP_IS_PRESSED.value:
            statuses.append(TCStatusBits.ESTOP_IS_PRESSED.value)

        return statuses

    @property
    def tracking_status(self):
        """Returns the current tracking status ENUM"""
        # TODO: Why does Graphene want me to return the value instead of the ENUM?

        # This first one actually comes from a different field -
        # In Asset objects, status_bits is not the same as tracking_status_bits
        # The following was done so that a LOCAL_ESTOP could be seen on the Asset List -> Tracking page
        if self.status_bits & TCStatusBits.ESTOP_IS_PRESSED.value:
            return TCTrackingStatusBits.LOCAL_ESTOP.value

        if self.tracking_status_bits == TCTrackingStatusBits.MANUAL.value:
            return TCTrackingStatusBits.MANUAL.value
        if self.tracking_status_bits == TCTrackingStatusBits.AUTO_TRACK_BACKTRACKING.value:
            return TCTrackingStatusBits.AUTO_TRACK_BACKTRACKING.value
        if self.tracking_status_bits == TCTrackingStatusBits.AUTO_TRACK_TRACKING.value:
            return TCTrackingStatusBits.AUTO_TRACK_TRACKING.value
        if self.tracking_status_bits == TCTrackingStatusBits.LOW_BATTERY_AUTO_STOW_INITIATED.value:
            return TCTrackingStatusBits.LOW_BATTERY_AUTO_STOW_INITIATED.value
        if self.tracking_status_bits == TCTrackingStatusBits.HIGH_TEMPERATURE_MOTOR_CUTOFF.value:
            return TCTrackingStatusBits.HIGH_TEMPERATURE_MOTOR_CUTOFF.value
        if self.tracking_status_bits == TCTrackingStatusBits.HANDHELD_ROW_CONTROL.value:
            return TCTrackingStatusBits.HANDHELD_ROW_CONTROL.value
        if self.tracking_status_bits == TCTrackingStatusBits.INDIVIDUAL_ROW_CONTROL.value:
            return TCTrackingStatusBits.INDIVIDUAL_ROW_CONTROL.value
        if self.tracking_status_bits == TCTrackingStatusBits.GROUP_ROW_CONTROL.value:
            return TCTrackingStatusBits.GROUP_ROW_CONTROL.value
        # Not sure of all the usage scenarios, so leaving the above code as-is. However, we
        # have seen issues with MORE THAN ONE CONDITION being active AT THE SAME TIME, so we
        # are adding support for some combinations as well. Notice that priority is being given
        # to the ERROR CONDITIONS. For example, if in IRC and you trigger a LOW BATTERY AUTO-STOW,
        # then the LOW BATTERY AUTO STOW gets displayed.
        if self.tracking_status_bits & TCTrackingStatusBits.LOW_BATTERY_AUTO_STOW_INITIATED.value:
            return TCTrackingStatusBits.LOW_BATTERY_AUTO_STOW_INITIATED.value
        if self.tracking_status_bits & TCTrackingStatusBits.HIGH_TEMPERATURE_MOTOR_CUTOFF.value:
            return TCTrackingStatusBits.HIGH_TEMPERATURE_MOTOR_CUTOFF.value

    # Yes static even though I use a parameter called self
    @staticmethod
    def what_to_pickle(self):
        """
        Should be called by the copy_reg for us to specify the properties we want pickled
        :param Asset self: Asset object instance
        :return: Arguments needed by pickle
        :rtype: tuple
        """
        return (Asset,
                (self.id,),
                {'last_reported': self.last_reported,
                 'snap_address': self.snap_address
                 }
                )


copy_reg.pickle(Asset, Asset.what_to_pickle)
