"""
AssetDiscovery.py - The job of this module is to find UNPROVISIONED units.
Unprovisioned units are one that have been COMMISSIONED (by the FCS)
but the NC does not (yet) know he is supposed to be polling them.

The assets discovered will get stored in the AssetManager.
"""

import logging
import binascii
import logging

import Asset
from PauseButtons import PauseButtonManager, PauseButton
from CommunicationsPriorities import *


log = logging.getLogger(__name__)


class AssetFinder(object):
    """
    The job of this class is to discover unprovisioned nodes
    by making over-the-air queries.
    """
    def __init__(self, asset_manager, snap_connect, schedule_func,
                 max_hops=10, spread=2000):
        """
        Constructor. Parameters include:
        asset_manager - holder of ALL the Asset objects
        snap_connect - the object we use to do SNAP communications
        (This may be replaced in the future with a more generic interface)
        schedule_func - used for timing
        """
        if asset_manager is None:
            raise Exception("Error: AssetFinder requires an AssetManager!")
        else:
            self._asset_manager = asset_manager

        if snap_connect is None:
            raise Exception("Error: AssetFinder requires a SNAP Connect!")
        else:
            self._snap_connect = snap_connect

        if schedule_func is None:
            raise Exception("Error: AssetFinder requires a schedule_func!")

        self.max_hops = max_hops
        self.spread = spread

        self._snap_connect.add_rpc_func("unprovisioned", self.unprovisioned)

        # Will likely increase all these delays post-development
        schedule_func(15.0, self.poll_for_unprovisioned)
        schedule_func(20.0, self.dump_unprovisioned)
        schedule_func(60.0, self.forget_unprovisioned)

        pause_button_manager = PauseButtonManager.get_default_instance()
        self._pause_button = pause_button_manager.get_pause_button(ASSET_FINDER_PRIORITY)

    def poll_for_unprovisioned(self):
        """Periodic poll, seeking out unprovisioned units"""
        if self._pause_button.paused:  # If something more important is going on...
            return  # ...bail
        self._snap_connect.mcast_rpc(1, self.max_hops, "poll_unprovisioned",
                                     0, 'i', self.max_hops, self.spread)

    def unprovisioned(self, response):
        """Response handler for the reply to poll_unprovisioned()"""
        # print binascii.hexlify(response) # temp test code

        snap_address = self._snap_connect.rpc_source_addr()
        readable_snap_address = binascii.hexlify(snap_address)
        log.info("Unprovisioned unit %s reported in", readable_snap_address)

        id = readable_snap_address  # for now - we may come up with a different scheme

        asset = self._asset_manager.get_discovered_asset(id)
        asset.snap_address = snap_address

    def dump_unprovisioned(self):
        """Just some debug logging"""
        log.info("Unprovisioned assets: " +
                     str(self._asset_manager.get_discovered_assets().keys()))
        # for key in self._asset_manager.get_discovered_assets().keys():
        #     asset = self._asset_manager.get_discovered_asset(key)
        #     if asset.snap_address is None:
        #         log.info("key=" + key + " snap_address=" + str(asset.snap_address))
        #     else:
        #        log.info("key=" + key + " snap_address=" + binascii.hexlify(asset.snap_address))

    # Assumed - better to have to periodically rediscover,
    # than to remember them forever
    def forget_unprovisioned(self):
        """We periodically flush the collection of unprovisioned assets"""
        log.info("Clearing unprovisioned assets collection")
        self._asset_manager.clear_discovered_assets()


#
# Make it so running module standalone results in a functional test
#
if __name__ == "__main__":
    import tornado.ioloop
    from SnapComm import SnapComm
    from asset_manager import AssetManager
    from Scheduler import Scheduler

    # logging.basicConfig(level=logging.WARNING)
    logging.basicConfig(level=logging.INFO)
    # logging.basicConfig(level=logging.DEBUG)

    snapcomm = SnapComm()

    asset_manager = AssetManager()

    asset_finder = AssetFinder(asset_manager,
                               snapcomm.sc,
                               Scheduler.schedule_func,
                               2,
                               1000)

    tornado.ioloop.IOLoop.current().start()
