"""
Scheduler.py - the purpose of this module is to put the scheduling
code in a single place. Note that the various implementations were
not 100% interchangable. The intent is to migrate over to using the
Tornado functions for this, and to not drag in Tornado everywhere.
"""

from tornado.ioloop import PeriodicCallback


class Scheduler(object):
    """
    Just trying to put this code in one place
    """

    @staticmethod
    def schedule_func(seconds, func):
        periodic_handle = PeriodicCallback(func, seconds * 1000)
        periodic_handle.start()
        return periodic_handle


if __name__ == '__main__':
    from tornado.ioloop import IOLoop

    def star():
        print '*'

    io_loop = IOLoop.current()

    Scheduler.schedule_func(5.0, star)

    io_loop.start()
