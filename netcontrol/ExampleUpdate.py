"""
ExampleUpdate.py - an early prototype for planned future Update.py work

The point here is that in a Python "update script" you can do anything you need to do.

Used during initial testing...
"""

import logging
import os

from utils.SystemSettings import *
from Scheduler import Scheduler
from pyfiria.snap import dmcast_uploader
from AssetUpdater import AssetUpdater

# logging.basicConfig(level=logging.WARNING)
# logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.DEBUG)

class DummyObject(object):
    pass


#
# Used with more than one script, so extracted into a subroutine
#
def determine_crc(spyfile):
    spyfile += '.spy'
    path = os.path.join(SYSTEM_CONFIG_DIRECTORY, 'script_images')  # TODO this is currently hard-coded in main.py and matched here... how to improve?
    spyfile = os.path.join(path, spyfile)
    # We are leveraging the capabilities of an existing class...
    # (We are not going to actually upload the file)
    uploader = dmcast_uploader.DmcastUploader(None, None)
    uploader.extract_spy_image(spyfile)
    return uploader.spy_crc


#
# Network Controller Bridge Script
#

CORRECT_BRIDGE_SCRIPT_NAME = 'NetworkControllerBridgeV0.1'  # Started incorporating a version number
# Corresponding script CRC used to be specified here too. NOW we extract it from the SPY file

# TODO Probably have to COPY the bridge script (mentioned above) to <config_dir>/script_images TOO

#
# Configure AssetUpdater to use the new bridge script
#
asset_updater = AssetUpdater(DummyObject(), Scheduler.schedule_func, DummyObject(), DummyObject(), '', '')

asset_updater.configured_bridge_script_name = CORRECT_BRIDGE_SCRIPT_NAME
asset_updater.configured_bridge_script_crc = determine_crc(CORRECT_BRIDGE_SCRIPT_NAME)

#
# Configure AssetUpdater to use the old BRIDGE Radio Firmware (SNAP Firmware, not STM32 Firmware!)
# What we are doing here is matching what the E12s came loaded with from the Factory.
# I did not upgrade Selkirk's E12 Bridge before I sent it out into the field.
#
# The following is currently commented out because of we are trying to advance to version 2.8.1
#asset_updater.configured_bridge_radio_firmware_version = '2.7.2'
#asset_updater.configured_bridge_radio_firmware_filename = 'RF220SU_AES128_SnapV2.7.2'

#
# Configure AssetUpdater to use the new BRIDGE Radio Firmware (SNAP Firmware, not STM32 Firmware!)
#
asset_updater.configured_bridge_radio_firmware_version = '2.8.1'
asset_updater.configured_bridge_radio_firmware_filename = 'RF220SU_AES128_SnapV2.8.1'

#
# Configure AssetUpdater to use the new Asset script (works in TCs and WXs)
#
#CORRECT_ASSET_SCRIPT_NAME = 'TrackerControllerRadioV0.5'  # Config Upload Support
#CORRECT_ASSET_SCRIPT_NAME = 'TrackerControllerRadioV0.6'  # STM32 upload improvements
#CORRECT_ASSET_SCRIPT_NAME = 'TrackerControllerRadioV0.7'  # More STM32 upload improvements
CORRECT_ASSET_SCRIPT_NAME = 'TrackerControllerRadioV1.0'

asset_updater.configured_asset_script_name = CORRECT_ASSET_SCRIPT_NAME
asset_updater.configured_asset_script_crc = determine_crc(CORRECT_ASSET_SCRIPT_NAME)

#
# Configure AssetUpdater to use the new Radio Firmware (SNAP Firmware, not STM32 Firmware!)
#
asset_updater.configured_asset_radio_firmware_version = '2.8.1'
asset_updater.configured_asset_radio_firmware_filename = 'RF220SU_AES128_SnapV2.8.1'

#
# Configure AssetUpdater to use the new Asset Firmware (works in TCs and WXs)
#
asset_updater.configured_asset_stm32_firmware_version = '1.8'
asset_updater.configured_asset_stm32_firmware_filename = 'TrackerControllerSTM32V1.8'

# This field was used before we added fine-grained control (see below)
#asset_updater.do_upload_scripts = True
#asset_updater.do_upload_scripts = False

# Fine-grained upload control. Note that these can also be changed on-the-fly,
# via the SLUI and/or the cloud. Uncomment alternate settings if needed.
#asset_updater.do_upload_bridge_script = True
#asset_updater.do_upload_bridge_script = False

#asset_updater.do_upload_bridge_firmware = True
#asset_updater.do_upload_bridge_firmware = False

#asset_updater.do_upload_asset_scripts = True
#asset_updater.do_upload_asset_scripts = False

#asset_updater.do_upload_asset_radio_firmware = True
#asset_updater.do_upload_asset_radio_firmware = False

#asset_updater.do_upload_asset_stm32_firmware = True
#asset_updater.do_upload_asset_stm32_firmware = False

#asset_updater.do_upload_asset_configs = True
#asset_updater.do_upload_asset_configs = False

asset_updater.save_config()
