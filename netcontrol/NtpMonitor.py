"""
NtpMonitor.py - THIS FILE DOEs NOT DO NTP!
What it DOES do is check on the status of the existing NTP
daemon that should be running in the background.
"""

import logging
import sys

if sys.platform == 'linux2':
    import subprocess

log = logging.getLogger(__name__)


class NtpMonitor(object):
    """
    Periodically does "command-line scraping" to see how NTP is doing
    """

    REFRESH_INTERVAL = 60  # seconds

    def __init__(self, schedule_func, on_ntp_sync_change=None):
        """
        Constructor
        :param callable schedule_func: A function with the signature func(call_every_n_seconds, callback_func)
        :param func on_ntp_sync_change: Optional. Callable for when the NTP server synchronization changes. Signature is func(synced, msg)
        """
        self._schedule_func = schedule_func
        self._on_ntp_sync_change = on_ntp_sync_change

        self.time_synchronized = True  # Start True is the first time through we log the correct alert
        self.time_server = None
        self.time_offset = None

        if sys.platform == 'linux2':
            self._update_status()
            self._schedule_func(NtpMonitor.REFRESH_INTERVAL, self._update_status)
        else:
            self.time_synchronized = False
            self._notify_ntp_sync_change("Network Controller is NOT time synchronized. Unsupported platform.")

    def _update_status(self):
        proc = subprocess.Popen(['ntpq', '-p'], stdout=subprocess.PIPE)
        stdout_value = proc.communicate()[0]

        previous_sync_state = self.time_synchronized

        self.time_synchronized = False
        self.time_server = None
        self.time_offset = None
        lines = stdout_value.split('\n')
        # Sanity test of output
        if len(lines) > 2:
            if lines[1].find('-----'):
                # Now go look for a time server that we are synchronized with (look for leading *)
                index = 2
                while index < len(lines):
                    if (lines[index] != '') and (lines[index][0] == '*'):
                        all_columns = lines[index].split(' ')
                        useful_columns = []
                        for column in all_columns:
                            if column != '':
                                useful_columns.append(column)
                        if len(useful_columns) == 10:
                            self.time_synchronized = True
                            self.time_server = useful_columns[0][1:]
                            self.time_offset = round(float(useful_columns[8]) * 0.001, 6) # <- converting from milliseconds to seconds
                        break
                    else:
                        index += 1

        if self.time_synchronized:
            if not previous_sync_state:
                self._notify_ntp_sync_change("Network Controller time synchronized to NTP Server")
        else:
            if previous_sync_state:
                self._notify_ntp_sync_change("Network Controller no longer time synchronized to NTP Server")

    def _notify_ntp_sync_change(self, msg):
        """
        Notify the subscriber that the state of time synchronization has changed
        :param str msg: The event log message string to log
        :return: None
        """
        log.critical(msg)
        if self._on_ntp_sync_change:
            try:
                self._on_ntp_sync_change(self.time_synchronized, msg)
            except:
                log.exception('An error occurred while notifying a callback')
