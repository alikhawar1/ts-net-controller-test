import os
import copy
import logging
import cPickle
from persistent_state import PersistentState
from controller_enums import CommandedRowState, TrackerPresets, EventType

log = logging.getLogger(__name__)
event_log = logging.getLogger('EventLog')

class RowPersistentService(object):
    
    def __init__(self, persistent_state, s3_client, schedule_func):
        self.command_cache = {}
        self.s3_client = s3_client
        self._persistent_state = persistent_state
        self._schedule_func = schedule_func
        
        # Introduced to limit the number of retries in case of failure
        # each try after a timeout of 20 seconds and a total of 6 retries.
        self.retry_count = 5
        self.pcb = None
        self.load_commands()

    def load_commands(self):
        event_log.info("NC is downloading Command File", extra=EventType.INDIVIDUAL_ASSET._dict)
        self.pcb = self._schedule_func(20.0, self.fetch_data)
    
    def fetch_data(self):
        if self.pcb:
            self.pcb.stop()
            commands = self.s3_client.download_file()
            if commands is None:
                if self.retry_count == 0:
                    event_log.info("NC can not download command file", extra=EventType.INDIVIDUAL_ASSET._dict)
                else:
                    self.retry_count -= 1
                    self.pcb.start()
            else:
                if len(commands) == 0:
                    event_log.info("No Active Commands", extra=EventType.INDIVIDUAL_ASSET._dict)
                self.command_cache = commands

    def add_command(self, vals):
        try:
            command_obj = self.s3_client.to_object(vals)
            self.command_cache.update(command_obj)
        except KeyError as ke:
            log.error('Key not found: ' + ke.message)
        except Exception as ex:
            log.error('Error updating command list: ' + ex.message)
        
    def update(self, key, vals):
        cmd = self.command_cache.pop(key)
        cmd.update(vals)
        self.command_cache.update({key: cmd})

    def get_list(self):
        return copy.copy(self.command_cache)
    
    def remove(self, key):
        self.command_cache.pop(key)
        
if __name__ == '__main__':
    pass
