"""
BridgeMonitor.py - the responsibilities if this module have waxed and waned
over time.

(The "bridge" is the "serial on one side, wireless on the other side" device
that allows us to talk to the wireless Assets out in the field.)

Currently it only does two jobs:

1) Holds the configuration settings for the bridge, which at present are also
considered to be the settings for the entire wireless network.

2) Gathers data about the bridge module in the background.
See also AssetPoller - it also gathers info that becomes part of the
"Mesh Network" card in the SLUI.

For a period of time, this module contained code to "move" the bridge from
one set of network settings to another. That responsibility has been taken
over by the AssetMover module, who coordinates the bridge settings as it
handles the much harder job of moving ALL the wireless Assets in the field
from one group of network settings to another.
"""


import logging

from snapconnect import snap

from utils.PickleHelper import PickleHelper
from SnapComm import SnapComm
from FormatHelpers import *
from PauseButtons import PauseButtonManager, PauseButton
from CommunicationsPriorities import *
import tornado.ioloop


BRIDGEMON_CONFIG_FILE = 'bridgemon.pkl'

log = logging.getLogger(__name__)


class BridgeMonitorPersistableConfig(object):
    """
    The point of this class is to separate out the PERSISTED CONFIG
    fields from the remaining LIVE DATA fields. This allows us to
    only persist (pickle) those fields. See BridgeMonitor.
    """
    def __init__(self):
        """Initializing these fields implicitly defines the "shape" of the object"""
        self.bridge_channel = None
        self.bridge_network_id = None
        self.bridge_key = None

    def default_config(self):
        # TODO Decide on default values. The current ones just match
        # my testbed
        self.bridge_channel = 7
        self.bridge_network_id = 0xBEEF
        # self.bridge_key = 'Synapse!Synapse!'
        self.bridge_key = ''


class BridgeMonitor(BridgeMonitorPersistableConfig):
    """Gatherer of information about the SNAP Bridge"""
    def __init__(self, snap_connect, schedule_func):
        """
        Constructor. Parameters include:
        snap_connect - the object we use to do SNAP communications
        schedule_func - used for timing
        """
        BridgeMonitorPersistableConfig.__init__(self)

        self._pickle_helper = PickleHelper(self, BridgeMonitorPersistableConfig, BRIDGEMON_CONFIG_FILE)

        if snap_connect is None:
            raise Exception("Error: BridgeMonitor requires a SNAP Connect!")
        else:
            self._snap_connect = snap_connect

        if schedule_func is None:
            raise Exception("Error: BridgeMonitor requires a schedule_func!")
        else:
            self._schedule_func = schedule_func

        if not self.load_config():
            self.default_config()
            self.save_config()

        # Contrast these next three fields with their persisted counterparts
        self._current_bridge_channel = None
        self._current_bridge_network_id = None
        self._current_bridge_key = None

        self._current_bridge_key_type = None
        self._prev_bridge_script_version = None

        self.bridge_address = None
        self.raw_bridge_address = None
        self.bridge_version = None  # This is actually module type + firmware version info
        self.bridge_script_name = None
        self.bridge_script_version = None
        self.bridge_script_crc = None
        self.on_bridge_update = None

        # In the following, "bmcb" stands for Bridge Monitor Call Back
        self._snap_connect.add_rpc_func("bmcb_channel", self.bmcb_channel)
        self._snap_connect.add_rpc_func("bmcb_network_id", self.bmcb_network_id)
        self._snap_connect.add_rpc_func("bmcb_key_type", self.bmcb_key_type)
        self._snap_connect.add_rpc_func("bmcb_key", self.bmcb_key)
        self._snap_connect.add_rpc_func("bmcb_module", self.bmcb_module)
        self._snap_connect.add_rpc_func("bmcb_major", self.bmcb_major)
        self._snap_connect.add_rpc_func("bmcb_minor", self.bmcb_minor)
        self._snap_connect.add_rpc_func("bmcb_build", self.bmcb_build)
        self._snap_connect.add_rpc_func("bmcb_script_name", self.bmcb_script_name)
        self._snap_connect.add_rpc_func("bmcb_script_version", self.bmcb_script_version)
        self._snap_connect.add_rpc_func("bmcb_script_crc", self.bmcb_script_crc)
        self._snap_connect.register_callback('next_hop_addr', self._on_sc_next_hop_addr)

        self._scheduler_handle = self._schedule_func(10.0, self.poll_bridge_for_data)

        pause_button_manager = PauseButtonManager.get_default_instance()
        self._pause_button = pause_button_manager.get_pause_button(BRIDGE_MONITOR_PRIORITY)

        self.pcb = self._schedule_func(300.0, self.on_startup)

    def on_startup(self):
        if self.on_bridge_update:
            try:
                self.pcb.stop()
                self._send_version()
                self.pcb = self._schedule_func(30.0, self.send_bridge_update)
            except Exception, e:
                log.exception(e)

    def send_bridge_update(self):
        try:
            self._send_version()
        except Exception, e:
            log.exception(e)

    def _send_version(self):
        if self.on_bridge_update:
            if self._prev_bridge_script_version != self.bridge_script_version:
                self._prev_bridge_script_version = self.bridge_script_version
                self.on_bridge_update(self.bridge_script_version, self.bridge_version)

    def _on_sc_next_hop_addr(self, addr, intf):
        if intf.intf_type in (snap.INTF_TYPE_SERIAL, snap.INTF_TYPE_SILABS_USB, snap.INTF_TYPE_SNAPSTICK):
            self.bridge_address = format_snap_address(addr)
            self.raw_bridge_address = addr

    def regather_bridge_script_info(self):
        # We gather the data repeatedly anyway...
        # the following just ensures no one mistakes old data for new data
        self.bridge_script_name = None
        self.bridge_script_version = None
        self.bridge_script_crc = None

    def load_config(self):
        """Try to load config from disk (previously generated pickle file"""
        return self._pickle_helper.load_config()

    def save_config(self):
        """save config out to disk (pickle file)"""
        return self._pickle_helper.save_config()

    # This used to be more direct. SLUI said change settings, and this got called.
    # Now when SLUI says change it, AssetMover TRIES to do so, and only calls
    # this function upon success. To say it another way, what used to be only
    # a config update from the SLUI become a (rather involved) action/process.
    def update_config(self, new_config_dict):
        """update config based on user input and subsequent asset movement"""
        for key in new_config_dict:
            if hasattr(self, key):
                setattr(self, key, new_config_dict[key])
        self.save_config()

    def _conditionally_send_multicast(self, *args, **kwargs):
        """
        The point of this routine is to reduce redundant code. Now that we try to
        use the communications link more cooperatively, we wound up with a LOT
        of checks for self._pause_button.paused. This subroutine was created in
        response to that.
        """
        if self._pause_button.paused:
            return
        self._snap_connect.mcast_rpc(*args, **kwargs)

    def poll_bridge_for_data(self):
        self._conditionally_send_multicast(1, 1, 'callback', 'bmcb_channel', SnapComm.FunctionCode('getChannel'))

    def bmcb_channel(self, channel):
        # Make a note of the current channel. It may or may not match the persisted channel.
        # This is dealt with after we finish gathering all of the other information.
        self._current_bridge_channel = channel

        snap_address = self._snap_connect.rpc_source_addr()
        # Since we are gathering this SOLELY for the SLUI's benefit,
        # we go ahead and store it in the exact format it needs.
        # If we were making use of this field ourselves, we would
        # store it in "native"" format and handle the translation via
        # a Python property.
        self.bridge_address = format_snap_address(snap_address)
        # It later turned out some other code needed the raw address too
        self.raw_bridge_address = snap_address
        self._conditionally_send_multicast(1, 1, 'callback', 'bmcb_network_id', SnapComm.FunctionCode('getNetId'))

    def bmcb_network_id(self, network_id):
        # Make a note of the current NID. It may or may not match the persisted NID.
        # This is dealt with after we finish gathering all of the other information.
        self._current_bridge_network_id = force_16_bit_unsigned(network_id)
        self._conditionally_send_multicast(1, 1, 'callback', 'bmcb_key_type', SnapComm.FunctionCode('loadNvParam'), 50)

    def bmcb_key_type(self, key_type):
        # Make a note of the current encryption type. See comments on channel and NID.
        self._current_bridge_key_type = key_type
        self._conditionally_send_multicast(1, 1, 'callback', 'bmcb_key', SnapComm.FunctionCode('loadNvParam'), 51)

    def bmcb_key(self, key):
        # Make a note of the current encryption key. See comments on channel and NID.
        if key is None:
            self._current_bridge_key = ''
        else:
            self._current_bridge_key = key
        self._conditionally_send_multicast(1, 1, 'callback', 'bmcb_module', SnapComm.FunctionCode('getInfo'), 3)

    def bmcb_module(self, module_code):
        if module_code == 14:
            self._module_name = 'RF200'
        elif module_code == 17:
            self._module_name = 'SM200'
        elif module_code == 19:
            self._module_name = 'RF266'
        elif module_code in [27, 30, 31]:
            self._module_name = 'RF220'
        else:
            self._module_name = 'UNKNOWN'
        self._conditionally_send_multicast(1, 1, 'callback', 'bmcb_major', SnapComm.FunctionCode('getInfo'), 5)

    def bmcb_major(self, major):
        self._major = major
        self._conditionally_send_multicast(1, 1, 'callback', 'bmcb_minor', SnapComm.FunctionCode('getInfo'), 6)

    def bmcb_minor(self, minor):
        self._minor = minor
        self._conditionally_send_multicast(1, 1, 'callback', 'bmcb_build', SnapComm.FunctionCode('getInfo'), 7)

    def bmcb_build(self, build):
        self.bridge_version = self._module_name + '-'
        self.bridge_version += str(self._major) + '.'
        self.bridge_version += str(self._minor) + '.'
        self.bridge_version += str(build)
        self._conditionally_send_multicast(1, 1, 'callback', 'bmcb_script_name', SnapComm.FunctionCode('imageName'))

    def bmcb_script_name(self, script_name):
        self.bridge_script_name = script_name
        self._conditionally_send_multicast(1, 1, 'callback', 'bmcb_script_version', SnapComm.FunctionCode('get_version'))

    def bmcb_script_version(self, script_version):
        self.bridge_script_version = script_version
        self._conditionally_send_multicast(1, 1, 'callback', 'bmcb_script_crc', SnapComm.FunctionCode('loadNvParam'), 40)

    def bmcb_script_crc(self, script_crc):
        self.bridge_script_crc = force_16_bit_unsigned(script_crc)
        self.check_bridge_config()

        # TODO Enable the following when we care...
        # If we get to here, we successfully gathered all the information we cared about.
        # Change the scheduling of poll_bridge_for_data to be less often
        # self._scheduler_handle.stop()
        # self._scheduler_handle = self._schedule_func(60.0, self.poll_bridge_for_data)
        # The periodic timer will fire later and re-invoke the whole sequence...

    #
    # We know what has been requested (the persisted config).
    # We potentially know what the bridge is currently set to.
    # We HAD to gather this data for display in the SLUI.
    # TODO Decide if there is anything else worth doing with the
    # data.
    # (Originally this is the spot in the code that would AUTOCORRECT)
    #
    def check_bridge_config(self):
        if self._pause_button.paused:
            return

        # TODO As stated above, decide IF we want to bother checking
        # the data that we have gathered, and WHAT we would do with the anwer.
        return

        # (Keeping the original checks for now)
        # Do we have all the information needed to make a decision?
        # (This code is using the "early bail-out model")
        if self._current_bridge_channel is None:
            return
        if self._current_bridge_network_id is None:
            return

        # We are using an IMPLICIT encryption TYPE, based on an explicit KEY
        if len(self.bridge_key) == 16:
            desired_key_type = 1
        else:
            desired_key_type = 0

        # Are the current settings compatible with the requested settings?
        mismatch = False

        if self._current_bridge_channel != self.bridge_channel:
            mismatch = True

        if self._current_bridge_network_id != self.bridge_network_id:
            mismatch = True

        if self._current_bridge_key_type != desired_key_type:
            mismatch = True

        if self._current_bridge_key != self.bridge_key:
            mismatch = True

        # If so, bail out (nothing needs to be done)
        if not mismatch:
            return

        # TODO Again, have to decide what we would do with a mismatch
        return
#
# Make it so running module standalone results in a functional test
#
if __name__ == "__main__":
    import tornado.ioloop
    from SnapComm import SnapComm
    from Scheduler import Scheduler

    # logging.basicConfig(level=logging.WARNING)
    # logging.basicConfig(level=logging.INFO)
    logging.basicConfig(level=logging.DEBUG)

    snapcomm = SnapComm()

    bridge_monitor = BridgeMonitor(snapcomm.sc,
                                   Scheduler.schedule_func)

    tornado.ioloop.IOLoop.current().start()
