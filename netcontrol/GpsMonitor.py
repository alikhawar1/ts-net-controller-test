"""
The cellular modem we are using also has a built-in GPS.
This module is responsible for capturing NMEA sentences
sent by that GPS over a dedicated TCP/IP port, and
then translating them into the desired lat, lon, and
altitude variables needed by the rest of the system.

As a bonus feature, this module compares the GPS time
to the System time and alerts if the delta is too big.
"""


import logging
import datetime

import socket
import tornado.ioloop
import tornado.iostream
import pynmea2


log = logging.getLogger(__name__)


class GpsMonitor(object):
    """
    The "interface" in this case if the set of member variables
    that you can access directly (anything not _private).
    """
    DEFAULT_TCP_IP = '192.168.2.1'
    DEFAULT_TCP_PORT = 5445
    DEFAULT_BUFFER_SIZE = 1024
    UPDATE_INTERVAL = 3.6*10e6

    RECONNECT_DELAY = 60  # 1 minute

    GRACE_PERIOD = 10  # seconds before we "time out" the GPS

    ALLOWED_SYSTEM_TIME_DELTA = datetime.timedelta(minutes=5)

    def __init__(self,
                 my_site,
                 ip_address=DEFAULT_TCP_IP,
                 port=DEFAULT_TCP_PORT,
                 buffer_size=DEFAULT_BUFFER_SIZE,
                 io_loop=None,
                 on_clock_questionable=None,
                 on_gps_change=None,
                 on_send_gps_update=None):
        """
        Constructor
        :param my_site: The TerraSmart site object to use for fallback GPS coordinates
        :param str ip_address: the ip_address of the device to be monitored.
        :param int port: the TCP/IP port of the device to be monitored.
        :param int buffer_size: currently unused (leftover from raw socket version)
        :param io_loop: Optional. A Tornado IOLoop instance
        :param func on_clock_questionable: Optional. Callable for when the system clock becomes questionable. Signature is func(system_clock_questionable, msg)
        :param func on_gps_change: Optional. Callable for when the GPS connection state changes. Signature is func(connected, msg)
        """
        self.my_site = my_site
        self._ip_address = ip_address
        self._port = port
        #self._buffer_size = buffer_size
        self._socket = None
        self.on_clock_questionable = on_clock_questionable
        self.on_gps_change = on_gps_change

        self.io_loop = io_loop or tornado.ioloop.IOLoop.current()

        self.gps_latitude = None
        self.gps_longitude = None
        self.gps_altitude = None
        self.altitude_units = None
        self.num_sats = None
        self.quality = None
        self.fix_time = None
        self.is_connected = False
        self.report_connect_failure = True # flag used to prevent reporting repeatedly
        self.was_connected = False
        self.using_fallback = True
        self.system_clock_questionable = False

        self._reconnect_delay = 0
        self._disconnect_delay = 0
        # In the real world, the GPS should always be connected.
        # In our development environments, not so much...
        self._attempt_connection()

        self.on_send_gps_update = on_send_gps_update
        self._streamreader = pynmea2.NMEAStreamReader()

        tornado.ioloop.PeriodicCallback(self._periodic_polling, 1000).start()
        tornado.ioloop.PeriodicCallback(self.send_gps_update, self.UPDATE_INTERVAL).start()

    @property
    def latitude(self):
        return self.gps_latitude or self.my_site.gps_lat

    @property
    def longitude(self):
        return self.gps_longitude or self.my_site.gps_lng

    @property
    def altitude(self):
        return self.gps_altitude or self.my_site.gps_alt

    @property
    def is_responding(self):
        return self.is_connected

    def send_gps_update(self):
        if self.on_send_gps_update:
            try:
                self.on_send_gps_update(self)
            except Exception as ex:
                log.error("Unable to update cloud with GPS information")

    def _data_received(self, data):
        #print "len(data) = " + str(len(data))
        #print "data = " + data
        #print "grace time left = " + str(self._disconnect_delay)
        try:
            for msg in self._streamreader.next(data):
                log.debug(msg)
                # The sentences vary in the additional detail they provide
                # Based on this we standardized on GGA
                if isinstance(msg, pynmea2.types.talker.GGA):
                    self.num_sats = msg.num_sats
                    log.info("num_sats=" + str(self.num_sats))
                    self.quality = msg.gps_qual
                    log.info("quality=" + str(self.quality))
                    if int(self.num_sats) >= 3:
                        self.gps_latitude = msg.latitude
                        self.gps_longitude = msg.longitude
                        self.gps_altitude = msg.altitude
                        self.altitude_units = msg.altitude_units
                        self.fix_time = datetime.datetime.combine(datetime.datetime.utcnow().date(), msg.timestamp)
                        log.info("Latitude=" + str(self.gps_latitude) +
                                 " Longitude=" + str(self.gps_longitude))
                        log.info("altitude=" + str(self.gps_altitude) + " units=" + str(self.altitude_units))
                        log.info("fix_time=" + str(self.fix_time))
                        self.using_fallback = False

                        # Extra step added due to rare issue with E12 losing it's time and date
                        try:
                            current_time = datetime.datetime.utcnow()
                            delta_time = abs(current_time - self.fix_time)
                            if delta_time > GpsMonitor.ALLOWED_SYSTEM_TIME_DELTA:
                                if not self.system_clock_questionable:
                                    self.system_clock_questionable = True
                                    self._notify_clock_questionable("Linux System Clock is questionable compared to GPS clock")
                            else:
                                if self.system_clock_questionable:
                                    self.system_clock_questionable = False
                                    self._notify_clock_questionable("Linux System Clock is now within a reasonable delta of the GPS clock")
                        except Exception as ex:
                            #print "GpsMonitor had problem doing system clock check"
                            log.debug("GpsMonitor had problem doing system clock check")
                            #print ex
                            log.debug(ex)

            self._disconnect_delay = GpsMonitor.GRACE_PERIOD
        except Exception as ex:
            #print "GpsMonitor choking on some data"
            log.debug("GpsMonitor choking on some data")
            #print "len(data) = " + str(len(data))
            log.debug("len(data) = " + str(len(data)))
            #print "data = " + data
            log.debug("data = " + data)
            #print ex
            log.debug(ex)

    def _connected(self):
        #print "_connected()"
        if self.was_connected:
            self._notify_gps_change(True, "Reconnected to the GPS")
            self.report_connect_failure = True # re-arm the reporting
        # Use the code below to report every connect
        #else:
        #    self._notify_gps_change(True, "Connected to the GPS")
        # Use the code below to only report it if we had a previous failure
        elif not self.report_connect_failure:
            self._notify_gps_change(True, "Connected to the GPS")
        self._reconnect_delay = 0
        self._read_future = self._stream.read_until_close(callback=None,
                                                          streaming_callback=self._data_received)
        self.is_connected = self.was_connected = True
        self._disconnect_delay = GpsMonitor.GRACE_PERIOD

    def _closing_actions(self):
        if self.is_connected:
            self._notify_gps_change(False, "Disconnected from the GPS")
        elif self.report_connect_failure:
            self._notify_gps_change(False, "Failed to connect to GPS")
            self.report_connect_failure = False
        self.is_connected = False
        self._socket = None
        self._reconnect_delay = GpsMonitor.RECONNECT_DELAY
        self.num_sats = None
        self.quality = None
        self._disconnect_delay = 0

    def _closed(self):
        #print "_closed()"
        self._closing_actions()

    def _attempt_connection(self):
        # Use of delay between attempts is to keep the system
        # usable even if no GPS is connected.
        if self._reconnect_delay:
            self._reconnect_delay -= 1
            if self._reconnect_delay > 0:
                return

        #print "now attempting a connect()"
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._stream = tornado.iostream.IOStream(self._socket)
        self._stream.set_close_callback(self._closed)
        self._stream.connect((self._ip_address, self._port), self._connected)

    def _periodic_polling(self):
        if self._socket is None:
            self._attempt_connection()
        # On the actual E12 we found that the _closed() call was not happening :(
        # so we added a backup timeout
        elif self._disconnect_delay > 0:
            self._disconnect_delay -= 1
            if self._disconnect_delay == 0:
                #print "GPS stream forced closed due to timeout"
                self._stream.close()

        return True  # Yes, we would like to be invoked again

    def _notify_clock_questionable(self, msg):
        """
        Notify the subscriber that the state of the system clock has changed
        :param str msg: The event log message string to log
        :return: None
        """
        log.critical(msg)
        if self.on_clock_questionable:
            try:
                self.on_clock_questionable(self.system_clock_questionable, msg)
            except:
                pass

    def _notify_gps_change(self, state, msg):
        """
        Notify the subscriber that the state of the GPS has changed
        :param bool state: Indicates the new state
        :param str msg: The event log message string to log
        :return: None
        """
        log.critical(msg)
        if self.on_gps_change:
            try:
                self.on_gps_change(state, msg)
            except:
                pass


if __name__ == '__main__':
    from terrasmart_site import TerraSmartSite

    # logging.basicConfig(level=logging.WARNING)
    logging.basicConfig(level=logging.INFO)
    # logging.basicConfig(level=logging.DEBUG)

    my_site = TerraSmartSite('', None)
    gps_mon = GpsMonitor(my_site)

    # At beginning of time we should be using fallback params
    assert gps_mon.altitude == my_site.gps_alt
    assert gps_mon.longitude == my_site.gps_lng
    assert gps_mon.latitude == my_site.gps_lat

    def check_vals():
        assert gps_mon.num_sats
        # Should now be using GPS values
        assert gps_mon.altitude == gps_mon.gps_altitude
        assert gps_mon.longitude == gps_mon.gps_longitude
        assert gps_mon.latitude == gps_mon.gps_latitude

    def dump_info():
        print str(gps_mon.num_sats) + ' ' + str(gps_mon.gps_longitude) + ' ' + str(gps_mon.gps_latitude) + ' ' + str(gps_mon.longitude) + ' ' + str(gps_mon.latitude)

    # tornado.ioloop.PeriodicCallback(dump_info, 1000).start()
    tornado.ioloop.PeriodicCallback(check_vals, 5000).start()
    tornado.ioloop.IOLoop.current().start()
