"""
AssetMover.py - the purpose of this module is to reliably move (wireless) Assets
from one set of network settings (including encryption settings) to another.

It was based on earlier work done in the original BridgeMonitor.py.

Note that the technique being used requires helper scripts on each Radio Module.
"""

import logging
import binascii

from snapconnect import snap
from FormatHelpers import *

from PauseButtons import PauseButtonManager, PauseButton
from CommunicationsPriorities import *
from controller_enums import EventType


log = logging.getLogger(__name__)
ASSET_MOVER_LOG_LEVEL_PROGRESS = logging.INFO
ASSET_MOVER_LOG_LEVEL_PROBLEM = logging.ERROR


class DmcastHelper(object):
    """
    If able to make this generic enough, will factor it out and re-use it elsewhere
    """
    def __init__(self, schedule_func):
        """
        Constructor. Parameters include:
        schedule_func - used for timing
        """
        self._schedule_func = schedule_func

        self._scheduled_timeout = None
        self._current_subset = None

    def apply_dmcast(self, set_of_assets, batch_size, batch_timeout, retries, command_sender, completion_callback):
        """
        Whatever dmcast message command_sender generates will be applied to all of
        the assets in the set_of_assets, using the specified batch_size and batch_timeout.
        If not all of the replies are good (see response_callin() below), retries for
        the stragglers will be attempted (again honoring batch_size and batch_timeout)
        up to the limit specified by the retries parameter
        """
        self._remaining_set = set_of_assets.copy()
        self._batch_size = batch_size
        self._batch_timeout = batch_timeout
        self._retries = retries
        self._command_sender = command_sender
        self._completion_callback = completion_callback

        self._straggler_set = set()

        self._do_next_batch()

    def _do_next_batch(self):
        """
        Builds up the address string for the next batch of Assets to get a
        directed multicast. Note that we callout to a previously specified
        function in order to generate the actual dmcast_rpc().
        This routine also takes care of detecting the end of a cycle, and
        the end of the entire process.
        """

        # Handle the case where we heard back from ALL units in the previous batch
        # before the timeout expired
        if self._scheduled_timeout is not None:
            self._scheduled_timeout.stop()
            self._scheduled_timeout = None

        self._current_subset = set()
        addresses = "\x00\x00\x00"  # work-around for dmcast "slot 0" bug
        count = self._batch_size
        while (count > 0) and (len(self._remaining_set) > 0):
            asset_key = self._remaining_set.pop()  # grab an arbitrary unit from the set

            self._current_subset.add(asset_key)

            asset_snap_address = binascii.unhexlify(asset_key)
            addresses += asset_snap_address
            count -= 1

        if addresses != "\x00\x00\x00":  # if there is actually something to do...
            self._command_sender(addresses)  # must actually send a dmcast_rpc()!
            self._scheduled_timeout = self._schedule_func(self._batch_timeout, self._timeout_handler)
        elif len(self._straggler_set) == 0:
            self._completion_callback(True)
        elif self._retries > 0:
            self._retries -= 1
            self._remaining_set = self._straggler_set
            self._straggler_set = set()
            self._do_next_batch()
        else:
            self._completion_callback(False)

    def response_callin(self, snap_address, asset_responded_correctly):
        asset_key = binascii.hexlify(snap_address)

        # Since they DID respond, remove them from this batch
        # The extra check-and-return here is for the scenario
        #  where a response comes in RIGHT AFTER we have timed out
        if asset_key in self._current_subset:
            self._current_subset.remove(asset_key)
        else:
            return

        if not asset_responded_correctly:
            self._straggler_set.add(asset_key)

        # If all in this batch responded (possibly incorrectly), go do another batch
        if len(self._current_subset) == 0:
            self._do_next_batch()

    def _timeout_handler(self):
        """
        This timeout gets cancelled if all of the nodes in the batch respond
        within the batch_timeout. If it fires, one or more Assets in the batch
        did not respond in time.
        """
        self._scheduled_timeout.stop()
        self._scheduled_timeout = None

        # Anything left in the current subset did not respond in time
        self._straggler_set |= self._current_subset
        self._do_next_batch()


class AssetMover(object):
    """
    This feature requires the appropriate helper scripts to be loaded into the
    Assets first. This enables the "arm", "apply", "confirm" paradigm that makes
    this possible.
    """

    # TODO TUNE THESE!!! TODO
    MAX_HOPS = 2  # 10
    REPLY_SPACING = 50  # does this matter???? Did seem to reduce the red in the sniffer TODO
    BATCH_SIZE = 10  # see how many can fit at one time
    BATCH_TIMEOUT = 2  # Hard to see the delays at 3
    BATCH_RETRIES = 2  # this is AFTER initial (unconditional) try ex. 1+2=3

    def __init__(self, snap_connect, schedule_func, asset_manager, bridge_monitor, system_manager):
        """
        Constructor. Parameters include:
        snap_connect - the object we use to do SNAP communications
        schedule_func - used for timing
        asset_mgr - knows all of the Assets AND their SNAP Addresses
        bridge_monitor - knows who the bridge node is
        nccb_monitor - knows when the next nightly shutdown will occur
        """
        self._snap_connect = snap_connect
        self._schedule_func = schedule_func
        self._asset_manager = asset_manager
        self._bridge_monitor = bridge_monitor
        self._system_manager = system_manager

        self._bridge_address = None  # will retrieve later, when needed
        # Extra flags for the bridge, since it requires special handling
        self._bridge_pending = False
        self._bridge_confirmed = False

        self._helper = DmcastHelper(schedule_func)

        self._snap_connect.add_rpc_func("amcb_arm_netcfg", self.amcb_arm_netcfg)
        self._snap_connect.add_rpc_func("proposed_netcfg", self.amcb_proposed_netcfg)
        self._snap_connect.add_rpc_func("current_netcfg", self.amcb_current_netcfg)
        self._snap_connect.add_rpc_func("amcb_confirm_netcfg", self.amcb_confirm_netcfg)

        pause_button_manager = PauseButtonManager.get_default_instance()
        self._pause_button = pause_button_manager.get_pause_button(ASSET_MOVER_PRIORITY)

        # These will get updated on user demand from the SLUI
        self._proposed_channel = None
        self._proposed_network_id = None
        self._proposed_key = None
        self._proposed_key_type = None


    # This is our GraphQL tie-in point. SLUI said change settings, this got called.
    def update_config(self, new_config_dict):
        """Try to move assets (including bridge), update config upon success"""

        self._new_config_dict = new_config_dict
        # Complication - GraphQL is optimal in that it only sends you what got edited by the user.
        # This means anything else must be pulled from BridgeMonitor's CURRENT configuration.
        if 'bridge_channel' in new_config_dict:
            self._proposed_channel = new_config_dict['bridge_channel']
        else:
            self._proposed_channel = self._bridge_monitor.bridge_channel
        if 'bridge_network_id' in new_config_dict:
            self._proposed_network_id = new_config_dict['bridge_network_id']
        else:
            self._proposed_network_id = self._bridge_monitor.bridge_network_id
        if 'bridge_key' in new_config_dict:
            # Compensate for UI dealing in unicode
            temp = new_config_dict['bridge_key']
            temp = temp.encode('ascii','replace')
            # In case we succeed, we will be wanting to save this out too
            self._new_config_dict['bridge_key'] = temp  # (gets used at the end of the process)
            self._proposed_key = temp
        else:
            self._proposed_key = self._bridge_monitor.bridge_key
        # We infer the key type.
        if len(self._proposed_key) == 16:  # being strict here because the wireless nodes are
            self._proposed_key_type = 1
        else:
            self._proposed_key_type = 0

        # Sanity check (only meant to catch mistakes during development)
        if (self._proposed_channel is None) or (self._proposed_network_id is None) or (self._proposed_key is None) or (self._proposed_key_type is None):
            return  # TODO this represents a coding error, put a breakpoint here
        else:
            self._pacing = self._schedule_func(1, self.request_move_assets)

    def request_move_assets(self):
        self._pacing.stop()

        # Check for real-world situations that can occur
        # 1) We don't attempt a network reconfiguration when running from low batteries
        nccb = self._asset_manager.get_asset("serial")
        if nccb is None:
            logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROBLEM, "Cannot verify battery power sufficient to reconfigure all assets, aborting", extra=EventType.INDIVIDUAL_ASSET._dict)
            return
        # if running from battery power and battery at less than 25%
        if (nccb.battery_current > nccb.solar_current) and (nccb.battery_charged < 25):
            logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROBLEM, "Insufficient battery power to attempt reconfiguring all assets, aborting", extra=EventType.INDIVIDUAL_ASSET._dict)
            return

        # 2) We don't allow the network reconfiguration attempt if there are any nodes offline
        # (In the process of making this check, we also build up the list of assets to be moved)
        self._nodes_to_be_moved = set(self._asset_manager.get_assets().keys())

        # Due to the existence of the NCCB, plus various test data,
        # not every Asset that exists is one that has a radio.
        # Filter the set down to just the real wireless nodes.
        copy_of_set = self._nodes_to_be_moved.copy()
        for asset_key in copy_of_set:
            asset = self._asset_manager.get_asset(asset_key)
            if asset.snap_address is None:
                self._nodes_to_be_moved.remove(asset_key)
            elif asset.offline == True:
                logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROBLEM, "All assets must be online before attempting network reconfiguration, aborting", extra=EventType.INDIVIDUAL_ASSET._dict)
                return

        # 2.5) Although technically not an asset, the radio module in the bridge has to be communicating too
        self._bridge_address = self._bridge_monitor.raw_bridge_address
        if self._bridge_address is None:
            logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROBLEM, "Bridge Node unknown, cannot move all assets (try again later)", extra=EventType.INDIVIDUAL_ASSET._dict)
            return

        # 3) It cannot be too close to nightly shutdown
        # Future TODO - maybe extend nightly shutdown instead?
        if self._system_manager.minutes_until_nightly_shutdown() < 15:
            logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROBLEM, "Too close to nightly shutdown, will not attempt to move all assets", extra=EventType.INDIVIDUAL_ASSET._dict)
            return

        # TODO What else should we check here?

        if self._pause_button.paused:
            self._pacing = self._schedule_func(60, self.request_move_assets)
        else:
            if self._pause_button.attempt_to_pause_others():
                # Give everyone a chance to gracefully finish pausing before beginning the actual work
                self._pacing = self._schedule_func(5, self.move_assets1)
            else:
                # Try again in a bit
                self._pacing = self._schedule_func(1, self.request_move_assets)

    #
    #
    # Step one - ARM everybody for a network change-over
    #
    #

    # DmcastHelper self._helper calls out to this routine
    def send_arm_netcfg(self, addresses):
        # Note that the following RPC call does not change any settings, it is merely
        # ENABLING a future change...
        self._snap_connect.dmcast_rpc(addresses, 1, AssetMover.MAX_HOPS, AssetMover.REPLY_SPACING,
                                      'callback', 'amcb_arm_netcfg',
                                      'arm_netcfg', self._proposed_channel, self._proposed_network_id, self._proposed_key_type, self._proposed_key, 60)

    # This gets invoked OTA due to the use of callback() up above
    def amcb_arm_netcfg(self, dummy):
        snap_address = self._snap_connect.rpc_source_addr()
        # This first message is trivial - we aren't actually verifying anything
        # (We just wanted confirmation they heard us...)
        self._helper.response_callin(snap_address, True)

    # DmcastHelper self._helper calls out to this routine
    def send_arm_netcfg_complete(self, result):
        if result:
            # Everything went well with the "arm", let's verify everyone got the correct settings
            logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROGRESS, "All assets armed, now reading back and confirming their proposed future settings", extra=EventType.INDIVIDUAL_ASSET._dict)
            self.move_assets2()
        else:
            logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROBLEM, "Was unable to communicate with all assets (arming phase) - network move aborted", extra=EventType.INDIVIDUAL_ASSET._dict)
            self.done_moving_assets()

    # It appears the Bridge Node will not respond to dmcasts received serially. This is a work-around...
    def send_arm_netcfg_to_bridge(self):
        """Sends an arm_netcfg() message to the bridge"""
        self._snap_connect.rpc(self._bridge_address, 'arm_netcfg', self._proposed_channel, self._proposed_network_id, self._proposed_key_type, self._proposed_key, 60)
        # We don't bother with a callback here because the results are going to be verified anyway...

    def move_assets1(self):
        self._pacing.stop()

        logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROGRESS, "Arming all assets for a potential switch to new network settings", extra=EventType.INDIVIDUAL_ASSET._dict)
        self.send_arm_netcfg_to_bridge()

        self._helper.apply_dmcast(self._nodes_to_be_moved, AssetMover.BATCH_SIZE, AssetMover.BATCH_TIMEOUT, AssetMover.BATCH_RETRIES,
                                  self.send_arm_netcfg, self.send_arm_netcfg_complete)
        # On success, move_assets2() will automatically be invoked (see callbacks up above)

    #
    #
    # Step two - Make sure everybody got the correct settings (everyone is armed)
    #
    #

    # DmcastHelper self._helper calls out to this routine
    def send_get_proposed_netcfg(self, addresses):
        # has a hardcoded callback of "proposed_netcfg()"
        self._snap_connect.dmcast_rpc(addresses, 1, AssetMover.MAX_HOPS, AssetMover.REPLY_SPACING, 'get_proposed_netcfg')

    # OTA "proposed_netcfg()"" mapped to this routine up in __init__()
    def amcb_proposed_netcfg(self, channel, network_id, key_type, key, timeout):
        snap_address = self._snap_connect.rpc_source_addr()
        success = True
        if channel != self._proposed_channel:
            success = False
        if force_16_bit_unsigned(network_id) != self._proposed_network_id:
            success = False
        if key_type != self._proposed_key_type:
            success = False
        if key != self._proposed_key:
            success = False

        # Due to the bridge not responding to serially received dmcasts, we are having
        # to give it special treatment.
        if snap_address == self._bridge_address:
            self._bridge_pending = False
            self._bridge_confirmed = success
        else:
            self._helper.response_callin(snap_address, success)

    # DmcastHelper self._helper calls out to this routine
    def send_get_proposed_netcfg_complete(self, result):
        if result and self._bridge_confirmed:
            logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROGRESS, "All assets correctly reported back the proposed new settings, now applying them", extra=EventType.INDIVIDUAL_ASSET._dict)
            # Everything went well with the "arm" (we even verified it), let's tell everyone to "apply" the new settings
            self.move_assets3()
        else:
            logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROBLEM, "Was unable to communicate with all assets (post-arming phase) - network move aborted", extra=EventType.INDIVIDUAL_ASSET._dict)
            self.done_moving_assets()

    # It appears the Bridge Node will not respond to dmcasts received serially. This is a work-around...
    def send_get_proposed_netcfg_to_bridge(self):
        """Sends a get_proposed_netcfg() message to the bridge"""
        # Callback not needed here because get_proposed_netcfg() ALWAYS calls back with proposed_netcfg()...
        self._snap_connect.rpc(self._bridge_address, 'get_proposed_netcfg')

    def move_assets2(self):
        self._bridge_pending = True
        self._bridge_confirmed = False

        self.send_get_proposed_netcfg_to_bridge()

        self._helper.apply_dmcast(self._nodes_to_be_moved, AssetMover.BATCH_SIZE, AssetMover.BATCH_TIMEOUT, AssetMover.BATCH_RETRIES,
                                  self.send_get_proposed_netcfg, self.send_get_proposed_netcfg_complete)
        # On success, move_assets3() will automatically be invoked (see callbacks up above)

    #
    #
    # Step three - Everybody is armed and confirmed correct - APPLY!
    #
    #

    # this is done as a blind multicast - see Step four for the verification aspects
    def move_assets3(self):
        # Have the bridge shout it out first
        self._snap_connect.mcast_rpc(1, 1, 'mcastRpc', 1, AssetMover.MAX_HOPS, 'apply_netcfg')
        self._snap_connect.mcast_rpc(1, 1, 'mcastRpc', 1, AssetMover.MAX_HOPS, 'apply_netcfg')
        # Tell the bridge to do the same
        self._snap_connect.rpc(self._bridge_address, 'apply_netcfg')
        self._snap_connect.rpc(self._bridge_address, 'apply_netcfg')

        # Because the above RPC actually changes settings, we could not use a simple callback.
        # Change settings to match and schedule outselves!
        self._previous_key = self._snap_connect.load_nv_param(51)
        self._previous_key_type = self._snap_connect.load_nv_param(50)

        self._snap_connect.save_nv_param(51, self._proposed_key)
        self._snap_connect.save_nv_param(50, self._proposed_key_type)

        self._pacing = self._schedule_func(5.0, self.move_assets4)  # let them all finish applying...

    #
    #
    # Step four - Make sure everybody is now on the new network (everyone did the apply)
    #
    #

    # DmcastHelper self._helper calls out to this routine
    def send_get_current_netcfg(self, addresses):
        # has a hardcoded callback of "current_netcfg()"
        self._snap_connect.dmcast_rpc(addresses, 1, AssetMover.MAX_HOPS, AssetMover.REPLY_SPACING, 'get_current_netcfg')

    # OTA "current_netcfg()"" mapped to this routine up in __init__()
    def amcb_current_netcfg(self, channel, network_id, key_type, key, timeout):
        snap_address = self._snap_connect.rpc_source_addr()
        success = True
        if channel != self._proposed_channel:
            success = False
        if force_16_bit_unsigned(network_id) != self._proposed_network_id:
            success = False
        if key_type != self._proposed_key_type:
            success = False
        if key != self._proposed_key:
            success = False

        # Due to the bridge not responding to serially received dmcasts, we are having
        # to give it special treatment.
        if snap_address == self._bridge_address:
            self._bridge_pending = False
            self._bridge_confirmed = success
        else:
            self._helper.response_callin(snap_address, success)

    # DmcastHelper self._helper calls out to this routine
    def send_get_current_netcfg_complete(self, result):
        if result and self._bridge_confirmed:
            logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROGRESS, "All assets confirmed switched over to the new network settings, now locking it in", extra=EventType.INDIVIDUAL_ASSET._dict)
            # Everything went well with the "apply", let's tell everyone to "confirm" the new settings
            self.move_assets5()
        else:
            # One or more nodes did not follow along. Revert back
            self._snap_connect.save_nv_param(51, self._previous_key)
            self._snap_connect.save_nv_param(50, self._previous_key_type)

            logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROBLEM, "Was unable to confirm all assets made the move (post-apply phase) - will fallback to original network settings", extra=EventType.INDIVIDUAL_ASSET._dict)
            self.done_moving_assets()

    # It appears the Bridge Node will not respond to dmcasts received serially. This is a work-around...
    def send_get_current_netcfg_to_bridge(self):
        """Sends a get_current_netcfg() message to the bridge"""
        # Callback not needed here because get_current_netcfg() ALWAYS calls back with current_netcfg()...
        self._snap_connect.rpc(self._bridge_address, 'get_current_netcfg')

    def move_assets4(self):
        self._pacing.stop()  # because we got here via a scheduled delay

        self._bridge_pending = True
        self._bridge_confirmed = False

        self.send_get_current_netcfg_to_bridge()

        self._helper.apply_dmcast(self._nodes_to_be_moved, AssetMover.BATCH_SIZE, AssetMover.BATCH_TIMEOUT, AssetMover.BATCH_RETRIES,
                                  self.send_get_current_netcfg, self.send_get_current_netcfg_complete)
        # On success, move_assets5() will automatically be invoked (see callbacks up above)

    #
    #
    # Step five - everyone made the jump, now we just have to keep them there.
    # Note that in this step we are not changing settings, we are just making
    # them persistent. This is the riskiest step, and the only one that could
    # require manual recovery.
    #
    #

    # DmcastHelper self._helper calls out to this routine
    def send_confirm_netcfg(self, addresses):
        # has a hardcoded callback of "current_netcfg()"
        self._snap_connect.dmcast_rpc(addresses, 1, AssetMover.MAX_HOPS, AssetMover.REPLY_SPACING,
                                      'callback', 'amcb_confirm_netcfg', 'confirm_netcfg')

    def amcb_confirm_netcfg(self, dummy):
        snap_address = self._snap_connect.rpc_source_addr()

        # Due to the bridge not responding to serially received dmcasts, we are having
        # to give it special treatment.
        if snap_address == self._bridge_address:
            self._bridge_pending = False
            self._bridge_confirmed = True
        else:
            self._helper.response_callin(snap_address, True)

    # DmcastHelper self._helper calls out to this routine
    def send_confirm_netcfg_complete(self, result):
        if result and self._bridge_confirmed:
            # Everything went well with the "confirm"
            # Since the move to the new group of network settings worked,
            # tell BridgeMonitor, keeper of the network settings
            self._bridge_monitor.update_config(self._new_config_dict)

            logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROGRESS, "All assets successfully moved to the new network settings", extra=EventType.INDIVIDUAL_ASSET._dict)
            self.done_moving_assets()
        else:
            # TODO Would need to log a MASSIVE failure here!!! TODO
            # This is bad... at this point we have fractured the network in two...
            logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROBLEM, "Was unable to lock-in all assets on the new network settings - use FCS to recover any lost nodes", extra=EventType.INDIVIDUAL_ASSET._dict)
            self.done_moving_assets()

    # It appears the Bridge Node will not respond to dmcasts received serially. This is a work-around...
    def send_confirm_netcfg_to_bridge(self):
        """Send a confirm_netcfg() to the bridge node"""
        self._snap_connect.rpc(self._bridge_address, 'callback', 'amcb_confirm_netcfg', 'confirm_netcfg')

    def move_assets5(self):
        self._bridge_pending = True
        self._bridge_confirmed = False

        self.send_confirm_netcfg_to_bridge()

        self._helper.apply_dmcast(self._nodes_to_be_moved, AssetMover.BATCH_SIZE, AssetMover.BATCH_TIMEOUT, AssetMover.BATCH_RETRIES + 2,
                                  self.send_confirm_netcfg, self.send_confirm_netcfg_complete)
        # On success, done_moving_assets will automatically be invoked (see callbacks up above)

    def done_moving_assets(self):
        # Let the other system components make use of the wireless network again
        logging.getLogger('EventLog').log(ASSET_MOVER_LOG_LEVEL_PROGRESS, "Resuming normal wireless network communications", extra=EventType.INDIVIDUAL_ASSET._dict)
        self._pause_button.unpause_others()
