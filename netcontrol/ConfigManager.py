"""
ConfigManager.py - originally responsible for two jobs:
1) ON DEMAND, create a composite file containing the complete config
of this Network Controller. These are kept by the user for restoral
purposes (example - a NC has to be replaced. The new NC needs the old
NCs configuration so it can take the original unit's place.)
2) ON DEMAND, overwrite any existing configuration with the contents
of a previously saved configuration (see item #1 above)
When Engineering Spec support was added in late 2019, ConfigManager took
on some additional duties:
A) When creating the composite config file, include the Engineering Spec
(if present)
B) When accepting a composite config file, trigger processing of the
Engineering Spec file (if present)
C) Validate/expand Engineering Spec files
"""

import logging
import os
import glob
import zipfile
import hashlib
import csv

from utils import SystemSettings
from controller_enums import EventType

log = logging.getLogger(__name__)

class ConfigManager(object):

    def __init__(self):
        pass

    def _add(self, zipfile, path, extension):
        """Helper function to add files of a given type (file extension)"""
        match_criteria = '*' + '.' + extension
        if path:
            match_criteria = os.path.join(path, match_criteria)
        files = glob.glob(match_criteria)
        for filename in files:
            zipfile.write(filename, filename)

    def pack(self, path, filename):
        """pack up all of the individual config files into one big zip file"""
        original_cwd = os.getcwd()
        os.chdir(path)  # We can now work relative to the root config directory

        full_filename = os.path.join(path, filename)
        with zipfile.ZipFile(full_filename, 'w') as zf:
            # Grabbing the actual config files is easy
            self._add(zf, '', 'pkl')
            # We also have different types of data files in various
            # subdirectories under the root config directory
            self._add(zf, SystemSettings.SITE_IMAGES_SUBDIRECTORY, 'jpg')
            self._add(zf, SystemSettings.SITE_IMAGES_SUBDIRECTORY, 'png')
            self._add(zf, SystemSettings.SITE_IMAGES_SUBDIRECTORY, 'bmp')

            self._add(zf, SystemSettings.SOFTWARE_IMAGES_SUBDIRECTORY, 'spy')
            self._add(zf, SystemSettings.SOFTWARE_IMAGES_SUBDIRECTORY, 'sfi')
            self._add(zf, SystemSettings.SOFTWARE_IMAGES_SUBDIRECTORY, 'hex')

            # IF we have an Engineering Spec, archive that too
            if os.path.isfile(SystemSettings.ENGINEERING_SPEC_FILENAME):
                zf.write(SystemSettings.ENGINEERING_SPEC_FILENAME, SystemSettings.ENGINEERING_SPEC_FILENAME)

        os.chdir(original_cwd)

    def unpack(self, path, filename):
        """unpack the contents of a previously saved config back into it's component pieces"""
        result = True
        original_cwd = os.getcwd()
        os.chdir(path)  # We can now work relative to the root config directory

        full_filename = os.path.join(path, filename)
        try:
            zf = zipfile.ZipFile(full_filename, 'r')
            if zf.testzip() is None:  # testzip returns first bad filename, None is a GOOD result...
                zf.extractall()
            else:
                result = False
            zf.close()
        except zipfile.BadZipfile, err:
            result = False

        # If the new config included an Engineering Spec, process that now
        if os.path.isfile(SystemSettings.ENGINEERING_SPEC_FILENAME):
            self.process_engineering_spec()

        os.chdir(original_cwd)
        return result

    def process_engineering_spec(self):
        result = True

        original_cwd = os.getcwd()
        os.chdir(SystemSettings.SYSTEM_CONFIG_DIRECTORY)  # We can now work relative to the root config directory

        TEMP_DIR = 'temp_files'

        # Make sure we have a temporary direcotry for validation
        if not os.path.isdir(TEMP_DIR):
            os.mkdir(TEMP_DIR)

        # Unzip the engineering spec into the temp directory
        try:
            zf = zipfile.ZipFile(SystemSettings.ENGINEERING_SPEC_FILENAME, 'r')
            if zf.testzip() is None:  # testzip returns first bad filename, None is a GOOD result...
                zf.extractall(TEMP_DIR)
            else:
                error_msg = "The %s file is corrupt, cannot use!" % (SystemSettings.ENGINEERING_SPEC_FILENAME,)
                logging.getLogger('EventLog').log(logging.CRITICAL, error_msg, extra=EventType.NETWORK_CONTROLLER._dict)
                result = False
            zf.close()
        except zipfile.BadZipfile, err:
            error_msg = "The %s file is invalid, cannot use!" % (SystemSettings.ENGINEERING_SPEC_FILENAME,)
            logging.getLogger('EventLog').log(logging.CRITICAL, error_msg, extra=EventType.NETWORK_CONTROLLER._dict)
            result = False

        if result == True: # No errors so far
            try:
                # Check the files against the manifest
                full_filename = os.path.join(TEMP_DIR, 'manifest')
                with open(full_filename, 'rb') as csv_file:
                    csv_reader = csv.reader(csv_file)
                    for entry in csv_reader:
                        if len(entry) == 2:
                            filename = entry[0]
                            specified_hashcode = entry[1]
                            full_filename = os.path.join(TEMP_DIR, filename)

                            # 1) The file must exist
                            if os.path.isfile(full_filename):
                                # 2) The file must have the specified hashcode
                                computed_hashcode = self.compute_hashcode(full_filename)
                                if computed_hashcode != specified_hashcode:
                                    error_msg = "File %s in the %s file is corrupt, cannot use!" % (filename, SystemSettings.ENGINEERING_SPEC_FILENAME)
                                    logging.getLogger('EventLog').log(logging.CRITICAL, error_msg, extra=EventType.NETWORK_CONTROLLER._dict)
                                    result = False
                            else:
                                error_msg = "The %s file is missing file %s, cannot use!" % (SystemSettings.ENGINEERING_SPEC_FILENAME, filename)
                                logging.getLogger('EventLog').log(logging.CRITICAL, error_msg, extra=EventType.NETWORK_CONTROLLER._dict)
                                result = False

                        else:
                            error_msg = "Invalid manifest file in %s, cannot use!" % (SystemSettings.ENGINEERING_SPEC_FILENAME,)
                            logging.getLogger('EventLog').log(logging.CRITICAL, error_msg, extra=EventType.NETWORK_CONTROLLER._dict)
                            result = False
            except Exception, e:
                error_msg = "Missing or invalid manifest file in %s, cannot use!" % (SystemSettings.ENGINEERING_SPEC_FILENAME,)
                logging.getLogger('EventLog').log(logging.CRITICAL, error_msg, extra=EventType.NETWORK_CONTROLLER._dict)
                result = False

        # Get rid of temp files
        match_criteria = '*.*'
        match_criteria = os.path.join(TEMP_DIR, match_criteria)
        files = glob.glob(match_criteria)
        for filename in files:
            os.remove(filename)

        match_criteria = '*'
        match_criteria = os.path.join(TEMP_DIR, match_criteria)
        files = glob.glob(match_criteria)
        for filename in files:
            os.remove(filename)

        if result == True:
            # Engineering Spec file was good, unzip it into the real destination
            try:
                zf = zipfile.ZipFile(SystemSettings.ENGINEERING_SPEC_FILENAME, 'r')
                zf.extractall() # Notice an alternate PATH was NOT specified this time...
                zf.close()
            except zipfile.BadZipfile, err:
                error_msg = "Unable to use %s file even though it passed validation!" % (SystemSettings.ENGINEERING_SPEC_FILENAME,)
                logging.getLogger('EventLog').log(logging.CRITICAL, error_msg, extra=EventType.NETWORK_CONTROLLER._dict)
                result = False

        os.chdir(original_cwd)

        if result == True:
            info_msg = "The %s file was validated and expanded successfully" % (SystemSettings.ENGINEERING_SPEC_FILENAME,)
            logging.getLogger('EventLog').log(logging.INFO, info_msg, extra=EventType.NETWORK_CONTROLLER._dict)

        return result

    def compute_hashcode(self, filename):
        try:
            with open(filename, 'rb') as f:
                hash_md5 = hashlib.md5()
                for chunk in iter(lambda: f.read(4096), b""):
                    hash_md5.update(chunk)
                result = hash_md5.hexdigest()
        except Exception, e:
            result = 'this file cannot be good'
        return result
