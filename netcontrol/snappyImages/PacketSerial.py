# As can be seen by the original comments, this file
# is a hacked up version of the original SNAPpy code
# TODO - Cleanup or REPLACE completely

#
# PacketSerial.py - a simple serial driver to communicate to the STM32 inside the Tracker Controller
#
# This file is more focused on the "serial" and "timing" aspects, see also PKTCODEC.py
#

from PKTCODEC import *
from Responses import *

PACKET_TIMEOUT = 5000 # Temp value, used so I can manually input packets from a terminal program
# TODO Shorten this timeout!

received_data = ''
receive_timeout = 0

# I may introduce receive STATES later.
# For now, starting with ONLY received_data and receive_timeout.
# The "state" is implicit in them.

# Convenience function
def send_TLV_Packet(tlv):
    sendPacket(build_packet(tlv))

# You must call this routine from one of the timer hooks!
def receiveTick(milliseconds):
    global receive_timeout, received_data

    if receive_timeout == 0:
        return

    if receive_timeout > milliseconds:
        receive_timeout -= milliseconds
        return
    else:
        debug("timeout") # debug code, remove!
        receive_timeout = 0
        received_data = ''
        
# Process all of the incoming bytes, which may contain
# one or more valid packets. It is also possible to receive
# partial packets, this code buffers as needed in received_data,
# and enforces a timeout too.
def incomingDataHandler(dest_asset, incoming_data):
    global receive_timeout, received_data

    #debug("heard: " + incoming_data) # debug code, remove!!!
    while incoming_data != '':
        if received_data == '':
            # Search for a PACKET_START character
            while incoming_data != '':
                if incoming_data[0] == PACKET_START:
                    receive_timeout = PACKET_TIMEOUT
                    break
                else:
                    incoming_data = incoming_data[1:]
                    if incoming_data == '':
                        return

        # If we get to here, we have seen a valid start character
        # Now we are gathering incoming characters until we see
        # a complete packet, the POSSIBILITY of which is indicated
        # by the PACKET_END delimiter
        next_byte = incoming_data[0]
        incoming_data = incoming_data[1:]
        received_data += next_byte
        receive_timeout = PACKET_TIMEOUT

        if next_byte == PACKET_END:
            # Might be the end of a packet, might be a PACKET_END
            # byte that occurred naturally WITHIN the packet
            if (packet_is_valid(received_data)):
                processReceivedPacket(dest_asset, received_data)
                received_data = ''
                receive_timeout = 0

        # SNAPpy strings can only hold so much...
        if len(received_data) == 255:
            received_data = ''
            receive_timeout = 0

        # We are still in the overall while loop...
