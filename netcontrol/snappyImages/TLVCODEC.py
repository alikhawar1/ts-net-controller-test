# As can be seen by the original comments, this file
# is a hacked up version of the original SNAPpy code
# TODO - Cleanup or REPLACE completely

#
# TLVCODEC.py - COder/DECoder routines for a Type/Length/Value scheme
#

#
# Individual Types, and their format
#
TLV_BOOL = 1
TLV_BOOL_LEN = 1 # Single byte, 0 means False, anything else means True

TLV_INT = 2
TLV_INT_LEN = 2 # Two bytes, LSB first

TLV_STR = 3
# For this type, the length is variable (including 0)
# If the string is not '', then <length> characters follow

# More types will be added, on an as-needed basis

def encode_tlv_bool(val):
    result = chr(TLV_BOOL) + chr(TLV_BOOL_LEN)
    if val:
        result += chr(1)
    else:
        result += chr(0)
    return result

def encode_tlv_int(val):
    result = chr(TLV_INT) + chr(TLV_INT_LEN)
    result += chr(val & 0xFF)
    result += chr((val >> 8) & 0xFF)
    return result

def encode_tlv_str(val):
    result = chr(TLV_STR) + chr( len(val) )
    result += val
    return result

def encode_tlv(val):
    val_type = type(val)
    if val_type == TC_BOOL:
        return encode_tlv_bool(val)
    elif val_type == TC_INT:
        return encode_tlv_int(val)
    elif val_type == TC_STRING:
        return encode_tlv_str(val)
    else:
        debug("programmer error!")

# We normally do not like global variables. However, in this case we want to return
# more information than can be handled as a single return value.
tlv_error = True
tlv_temp_type = None
tlv_temp_value = None

def tlv_decode_error():
    global tlv_error, tlv_temp_type, tlv_temp_value
    tlv_error = True
    tlv_temp_type = None
    tlv_temp_value = None
    debug("tlv decoding error!") # Debug code, remove!

def decode_tlv_bool(tlv_string):
    global tlv_temp_type, tlv_temp_value

    if len(tlv_string) < 3:
        tlv_decode_error()
        return

    if ord( tlv_string[0] ) != TLV_BOOL:
        tlv_decode_error()
        return

    if ord( tlv_string[1] ) != TLV_BOOL_LEN:
        tlv_decode_error()
        return

    tlv_temp_type = TLV_BOOL

    if tlv_string[2] == '\x00':
        tlv_temp_value = False
    else:
        tlv_temp_value = True

    tlv_string = tlv_string[3:]
    return tlv_string

def decode_tlv_int(tlv_string):
    global tlv_temp_type, tlv_temp_value

    if len(tlv_string) < 4:
        tlv_decode_error()
        return

    if ord( tlv_string[0] ) != TLV_INT:
        tlv_decode_error()
        return

    if ord( tlv_string[1] ) != TLV_INT_LEN:
        tlv_decode_error()
        return

    tlv_temp_type = TLV_INT

    tlv_temp_value = ord( tlv_string[3] )
    tlv_temp_value <<= 8
    tlv_temp_value += ord( tlv_string[2] )

    tlv_string = tlv_string[4:]
    return tlv_string

def decode_tlv_str(tlv_string):
    global tlv_temp_type, tlv_temp_value

    if len(tlv_string) < 2:
        tlv_decode_error()
        return

    if ord( tlv_string[0] ) != TLV_STR:
        tlv_decode_error()
        return

    string_length = ord( tlv_string[1] )
    if string_length > len(tlv_string) - 2:
        tlv_decode_error()
        return

    tlv_temp_type = TLV_STR

    tlv_temp_value = tlv_string[2:2+string_length]

    tlv_string = tlv_string[2+string_length:]
    return tlv_string

# We normally do not like global variables. However, in this case we want to return
# more information than can be handled as a single return value.

tlv_count = 0

tlv1_type = None
tlv1_value = None

tlv2_type = None
tlv2_value = None

tlv3_type = None
tlv3_value = None

tlv4_type = None
tlv4_value = None

tlv5_type = None
tlv5_value = None

tlv6_type = None
tlv6_value = None

tlv7_type = None
tlv7_value = None

tlv8_type = None
tlv8_value = None

tlv9_type = None
tlv9_value = None

# TODO Add more as needed

# Decode a string consisting of multiple individual TLV substrings
# Returns success or failure, with the details stashed away in the
# global variables up above
def decode_tlv_array(tlv_string):
    global tlv_count, tlv_error
    global tlv1_type, tlv1_value, tlv2_type, tlv2_value, tlv3_type, tlv3_value
    global tlv4_type, tlv4_value, tlv5_type, tlv5_value, tlv6_type, tlv6_value
    global tlv7_type, tlv7_value, tlv8_type, tlv8_value, tlv9_type, tlv9_value

    tlv_count = 0
    tlv_error = False
    
    while len(tlv_string) > 0 and (not tlv_error):
        tlv_type = ord( tlv_string[0] )
        if tlv_type == TLV_BOOL:
            tlv_string = decode_tlv_bool(tlv_string)
        elif tlv_type == TLV_INT:
            tlv_string = decode_tlv_int(tlv_string)
        elif tlv_type == TLV_STR:
            tlv_string = decode_tlv_str(tlv_string)
        else:
            tlv_decode_error()
            return False

        if not tlv_error:
            tlv_count += 1
            
            # Poor-man's arrays
            if tlv_count == 1:
                tlv1_type = tlv_temp_type
                tlv1_value = tlv_temp_value
            elif tlv_count == 2:
                tlv2_type = tlv_temp_type
                tlv2_value = tlv_temp_value
            elif tlv_count == 3:
                tlv3_type = tlv_temp_type
                tlv3_value = tlv_temp_value
            elif tlv_count == 4:
                tlv4_type = tlv_temp_type
                tlv4_value = tlv_temp_value
            elif tlv_count == 5:
                tlv5_type = tlv_temp_type
                tlv5_value = tlv_temp_value
            elif tlv_count == 6:
                tlv6_type = tlv_temp_type
                tlv6_value = tlv_temp_value
            elif tlv_count == 7:
                tlv7_type = tlv_temp_type
                tlv7_value = tlv_temp_value
            elif tlv_count == 8:
                tlv8_type = tlv_temp_type
                tlv8_value = tlv_temp_value
            elif tlv_count == 9:
                tlv9_type = tlv_temp_type
                tlv9_value = tlv_temp_value
            # TODO expand this as needed

    return ((len(tlv_string) == 0) and (tlv_error == False), tlv_error, tlv_count, tlv1_type, tlv1_value, tlv2_type, tlv2_value, tlv3_type, tlv3_value, tlv4_type, tlv4_value, tlv5_type, tlv5_value, tlv6_type, tlv6_value, tlv7_type, tlv7_value, tlv8_type, tlv8_value, tlv9_type, tlv9_value)

def dump_tlv_decode_results(): # debug code
    print "tlv_count = ", tlv_count, " tlv_error = ", tlv_error, 
    print " tlv_temp_type = ", tlv_temp_type, " tlv_temp_val = ", tlv_temp_value,
    print " tlv1 = ", tlv1_value, " tlv2 = ", tlv2_value, " tlv3 = ", tlv3_value,
    print " tlv4 = ", tlv4_value, " tlv5 = ", tlv5_value, " tlv6 = ", tlv6_value,
    print " tlv7 = ", tlv7_value, " tlv8 = ", tlv8_value, " tlv9 = ", tlv9_value,
    print
