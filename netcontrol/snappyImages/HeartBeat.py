# As can be seen by the original comments, this file
# is a hacked up version of the original SNAPpy code
# TODO - Cleanup or REPLACE completely

#
# HeartBeat.py - Monitors the STM32, makes sure it is still alive
#
# (you can think of this as a serial port watchdog)

from TLVCODEC import *
from PacketSerial import *

# We will fine-tune this value over time. Right now I can't think
# of anything the STM32 could be busy doing (including FLASH upgrades)
# that should take over a minute
STM32_TIMEOUT = 60 # Set this to 0 to disable this feature (for example, when debugging on the STM32 side)
STM32_QUERY = STM32_TIMEOUT - 3 # If incidental traffic doesn't give us confirmation, do an explicit "ping"

stm32_counter = 0

def send_query():
    query = encode_tlv_str("?")
    send_TLV_Packet(query)

def reset_STM32():
    print "NCCB is offline!"

# You must call this from the HOOK_1S handler
def checkHeartBeat():
    global stm32_counter

    if STM32_TIMEOUT == 0: # disabled
        return

    stm32_counter += 1
    if stm32_counter == STM32_QUERY:
        send_query()
    elif stm32_counter >= STM32_TIMEOUT:
        stm32_counter = 0
        reset_STM32()

# Call this every time you hear from the STM32
def heartBeat():
    global stm32_counter

    stm32_counter = 0

    #indicate_heartBeat() # possibly a stub in the final system...

