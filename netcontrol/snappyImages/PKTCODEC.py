#
# PKTCODEC.py - COde/DECode routines for a simple serial packet format
#

PACKET_START = '\x11'
PACKET_END = '\x22'

def compute_PIC_byte(data):
    checksum = 0;
    count = len(data)
    index = 0
    while index < count:
        byte = ord( data[index] )
        checksum += byte
        index += 1
    checksum ^= 0xFFFF
    checksum += 1
    checksum &= 0xFF
    return checksum

def compute_PIC_char(data):
    return chr( compute_PIC_byte(data) )

def build_packet(tlv_str):
    return PACKET_START + tlv_str + compute_PIC_char(tlv_str) + PACKET_END

def extract_tlv(packet):
    # REQUIRED - call packet_is_valid() first!
    return packet[1:-2]

def packet_is_valid(packet):
    if len(packet) < 3:
        return False
    packet_length = len(packet)
    if packet[0] != PACKET_START:
        return False
    if packet[packet_length-1] != PACKET_END:
        return False
    packet_PIC = packet[packet_length-2]
    computed_PIC = compute_PIC_char( extract_tlv(packet) )
    if packet_PIC != computed_PIC:
        return False
    return True
