#
# Responses.py - The RPC API that we synthesize from the TLV commands we get from the STM32
#


import datetime

from TLVCODEC import *
from netcontrol.NetworkControllerDecoders import *


SYSTEM_STATUS_REPORT = "rs"
# Eventually we had to stop adding data onto the end of BLOB_S (see below)
MORE_STATUS_REPORT = "rn"  # "n"ew data? Don't have a good name for this, but it is additional data

SYSTEM_CONFIG_REPORT = "rt"
SYSTEM_STATIC_REPORT = "ru"

SENSOR_CONFIG_REPORT = "rv"
SENSOR_STATUS_REPORT = "rw"

PRESET_CONFIG_REPORT = "rx"
WEATHER_STATUS_REPORT = "ry"


def processReceivedPacket(dest_asset, packet):
    """Process a packet received from the STM32 over the serial link"""

    heartBeat() # Receipt of any valid message convinces us that the STM32 is alive and well

    #print 'Received packet: ', packet,
    tlv = extract_tlv(packet)
    #print 'Received TLV: ', tlv,

    # The following worked in SNAPpy but not in Python. Changed to returning a huge tuple
    #decode_tlv_array(tlv) # fills in lots of global variables for us
    (decode_result, tlv_error, tlv_count, tlv1_type, tlv1_value, tlv2_type, tlv2_value, tlv3_type, tlv3_value, tlv4_type, tlv4_value, tlv5_type, tlv5_value, tlv6_type, tlv6_value, tlv7_type, tlv7_value, tlv8_type, tlv8_value, tlv9_type, tlv9_value) = decode_tlv_array(tlv)

    #print "tlv_error="+str(tlv_error)
    #print "tlv_count="+str(tlv_count)
    #print "tlv1_type="+str(tlv1_type)
    if (tlv_error == False) and (tlv_count > 0) and (tlv1_type == TLV_STR):
        dest_asset.last_reported = datetime.datetime.utcnow()

        # TBD - do we do a lot of validation and processing here,
        # or do we just pass these blindly through?
        # Going with blind pass-thru for starters...
        # Also have to revisit the use of multicast versus unicast
        # The current multicasts are enough to get the RPC to *Portal*
        if (tlv_count == 1):
            # Is it for us? Or the FCS/NC?
            if tlv1_value == '.': # Heartbeat reply from the STM32
                pass # heartBeat() is called on ALL messages, not just this one...
            else:
                mcastRpc(1, 1, tlv1_value)
        elif (tlv_count == 2):
            # Is it for us? Or the FCS/NC?
            if tlv1_value == SYSTEM_STATUS_REPORT:
                if tlv2_type == TLV_STR:
                    decode_blob_s(dest_asset, tlv2_value)
                else:
                    debug("Bad SYSTEM_STATUS_REPORT from STM32")
            elif tlv1_value == MORE_STATUS_REPORT:
                if tlv2_type == TLV_STR:
                    decode_blob_n(dest_asset, tlv2_value)
                else:
                    debug("Bad MORE_STATUS_REPORT from STM32")
            elif tlv1_value == SYSTEM_CONFIG_REPORT:
                if tlv2_type == TLV_STR:
                    decode_blob_t(dest_asset, tlv2_value)
                else:
                    debug("Bad SYSTEM_CONFIG_REPORT from STM32")
            elif tlv1_value == SYSTEM_STATIC_REPORT:
                if tlv2_type == TLV_STR:
                    decode_blob_u(dest_asset, tlv2_value)
                else:
                    debug("Bad SYSTEM_STATIC_REPORT from STM32")
            elif tlv1_value == SENSOR_CONFIG_REPORT:
                if tlv2_type == TLV_STR:
                    decode_blob_v(dest_asset, tlv2_value)
                else:
                    debug("Bad SENSOR_CONFIG_REPORT from STM32")
            elif tlv1_value == SENSOR_STATUS_REPORT:
                if tlv2_type == TLV_STR:
                    decode_blob_w(dest_asset, tlv2_value)
                else:
                    debug("Bad SENSOR_STATUS_REPORT from STM32")
            elif tlv1_value == PRESET_CONFIG_REPORT:
                if tlv2_type == TLV_STR:
                    decode_blob_x(dest_asset, tlv2_value)
                else:
                    debug("Bad PRESET_CONFIG_REPORT from STM32")
            elif tlv1_value == WEATHER_STATUS_REPORT:
                if tlv2_type == TLV_STR:
                    decode_blob_y(dest_asset, tlv2_value)
                else:
                    debug("Bad WEATHER_STATUS_REPORT from STM32")
            # More TODO here as I add blob types, message types
            else:
                mcastRpc(1, 1, tlv1_value, tlv2_value)
        elif (tlv_count == 3):
            mcastRpc(1, 1, tlv1_value, tlv2_value, tlv3_value)
        elif (tlv_count == 4):
            mcastRpc(1, 1, tlv1_value, tlv2_value, tlv3_value, tlv4_value)
        elif (tlv_count == 5):
            mcastRpc(1, 1, tlv1_value, tlv2_value, tlv3_value, tlv4_value, tlv5_value)
        elif (tlv_count == 6):
            mcastRpc(1, 1, tlv1_value, tlv2_value, tlv3_value, tlv4_value, tlv5_value, tlv6_value)
        elif (tlv_count == 7):
            mcastRpc(1, 1, tlv1_value, tlv2_value, tlv3_value, tlv4_value, tlv5_value, tlv6_value, tlv7_value)
        elif (tlv_count == 8):
            mcastRpc(1, 1, tlv1_value, tlv2_value, tlv3_value, tlv4_value, tlv5_value, tlv6_value, tlv7_value, tlv8_value)
        elif (tlv_count == 9):
            mcastRpc(1, 1, tlv1_value, tlv2_value, tlv3_value, tlv4_value, tlv5_value, tlv6_value, tlv7_value, tlv8_value, tlv9_value)
