# As can be seen by the original comments, this file
# is a hacked up version of the original SNAPpy code
# TODO - Cleanup or REPLACE completely

#
# BlobTracking.py - code related to keeping "blobs" (opaque binary strings)
# of data that we receive from the STM32, for later FCS and NC retrieval.
#
# Since some of the queries are actually for the RADIO MODULE, we also
# build a few blobs of our own.
#

N_POLL_RELOAD = 10 # Once a second
n_poll_counter = 6 # (trying to stagger this with the original blob s)

S_POLL_RELOAD = 10 # Once a second
s_poll_counter = 1

T_POLL_RELOAD = 30 # Every 3 seconds
t_poll_counter = 3

U_POLL_RELOAD = 100 # Every 10 seconds
u_poll_counter = 5

V_POLL_RELOAD = 100 # Every 10 seconds
v_poll_counter = 7

W_POLL_RELOAD = 10 # Once a second
w_poll_counter = 9

X_POLL_RELOAD = 100 # Every 10 seconds
x_poll_counter = 11

Y_POLL_RELOAD = 10 # Once a second for now, TBD
y_poll_counter = 0 # Starts out disabled, enabled when a unit reports it HAS weather sensors

def poll_s_blob_next():
    global s_poll_counter
    s_poll_counter = 1

def poll_t_blob_next():
    global t_poll_counter
    t_poll_counter = 1

def poll_u_blob_next():
    global u_poll_counter
    u_poll_counter = 1

def poll_v_blob_next():
    global v_poll_counter
    v_poll_counter = 1

def poll_w_blob_next():
    global w_poll_counter
    w_poll_counter = 1

def poll_x_blob_next():
    global x_poll_counter
    x_poll_counter = 1

def poll_y_blob_next():
    global y_poll_counter
    y_poll_counter = 1

def poll_for_blobs():
    global s_poll_counter, t_poll_counter, u_poll_counter
    global v_poll_counter, w_poll_counter, x_poll_counter
    global y_poll_counter, n_poll_counter

    n_poll_counter -= 1
    if n_poll_counter <= 0:
        n_poll_counter = N_POLL_RELOAD
        send_TLV_Packet(encode_tlv_str("qn"))

    s_poll_counter -= 1
    if s_poll_counter <= 0:
        s_poll_counter = S_POLL_RELOAD
        send_TLV_Packet(encode_tlv_str("qs"))

    t_poll_counter -= 1
    if t_poll_counter <= 0:
        t_poll_counter = T_POLL_RELOAD
        send_TLV_Packet(encode_tlv_str("qt"))

    u_poll_counter -= 1
    if u_poll_counter <= 0:
        u_poll_counter = U_POLL_RELOAD
        send_TLV_Packet(encode_tlv_str("qu"))

    v_poll_counter -= 1
    if v_poll_counter <= 0:
        v_poll_counter = V_POLL_RELOAD
        send_TLV_Packet(encode_tlv_str("qv"))

    w_poll_counter -= 1
    if w_poll_counter <= 0:
        w_poll_counter = W_POLL_RELOAD
        send_TLV_Packet(encode_tlv_str("qw"))

    x_poll_counter -= 1
    if x_poll_counter <= 0:
        x_poll_counter = X_POLL_RELOAD
        send_TLV_Packet(encode_tlv_str("qx"))

    if y_poll_counter:
        y_poll_counter -= 1
        if y_poll_counter <= 0:
            y_poll_counter = Y_POLL_RELOAD
            send_TLV_Packet(encode_tlv_str("qy"))

def encode_blob_word(word):
    blob = chr(word >> 8)
    blob += chr(word & 0xFF)
    return blob

def dumpHexDebug(bytes):
    result = ''
    count = len(bytes)
    index = 0
    while index < count:
        result += hexByte(ord(bytes[index]))
        index += 1
        if (index & 15) == 0:
            debug(result)
            result = ''
        else:
            result += ' '
    debug(result)
