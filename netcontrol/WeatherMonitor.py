"""
WeatherMonitor.py - This module is responsible for keeping any eye on the
weather (and the weather stations), and making that data available to other code.

Dependencies:

The "weather data" fields are contained in to Asset objects, which
WeatherMonitor gets from the AssetManager.

WeatherMonitor also needs a PersistentState helper so it can remember the status
of "snow stow" even through power outages/reboots.
"""

import logging
from enum import Enum

from utils.PickleHelper import PickleHelper
from FormatHelpers import format_snap_address
from FormatHelpers import format_asset_identity

import Asset
from persistent_state import PersistentState
from controller_enums import EventType
import datetime

WEATHERMON_CONFIG_FILE = 'weathermon.pkl'

log = logging.getLogger(__name__)
WEATHER_STOW_LOG_LEVEL = logging.INFO


class StowType:
    HIGH_WIND_GUST = 1
    HIGH_AVG_WIND = 2
    DEEP_SNOW = 3
    DEEP_PANEL_SNOW = 4
    LACK_WEATHER_INFO = 5

class WeatherMonitorPersistableConfig(object):
    """
    The point of this class is to separate out the PERSISTED CONFIG
    fields from the remaining LIVE DATA fields. This allows us to
    only persist (pickle) those fields. See WeatherMonitor.
    """
    def __init__(self):
        """Initializing these fields implicitly defines the "shape" of the object"""
        self.trigger_averaging = None

        self.enable_wind_speed_stow = None
        self.wind_speed_threshold = None
        self.wind_speed_duration_required = None

        self.enable_wind_gust_stow = None
        self.wind_gust_threshold = None

        self.resume_tracking_after_wind_timeout = None
        self.resume_tracking_after_snow_timeout = None
        self.resume_tracking_after_panel_snow_timeout = None

        self.enable_snow_depth_stow = None
        self.enable_snow_depth_averaging = None  # otherwise uses peak value
        self.snow_depth_threshold = None  # in meters
        self.snow_depth_low_threshold = None
        self.enable_auto_resume_tracking_snow = None

        # The original "snow depth" was implicitly "snow depth on the ground"
        # In 01/2019 we started adding support for "snow depth on the panels"
        self.enable_panel_snow_depth_stow = None
        self.enable_panel_snow_depth_averaging = None  # otherwise uses peak value
        self.panel_snow_depth_threshold = None  # in meters
        self.panel_snow_depth_low_threshold = None  # in meters
        self.enable_auto_resume_tracking_panel_snow = None

        self.minimum_stations_required = 0

        # Future TODO - Once we start polling NOAA for data, there is
        # another enable and parameter needed here.

    def default_config(self):
        # TODO Decide on default values. The current ones were taken
        # directly from the SLUI screen mockups, except the times
        # were reduced by a factor of 10 to speed up testing.

        self.trigger_averaging = False  # use instantaneous values instead of averaged

        self.enable_wind_speed_stow = True
        self.wind_speed_threshold = 35.0
        self.wind_speed_duration_required = 1 * 60  # 10 * 60

        self.enable_wind_gust_stow = True
        self.wind_gust_threshold = 50.0

        self.resume_tracking_after_wind_timeout = 30 * 60 # 30 minutes
        self.resume_tracking_after_snow_timeout = 0
        self.resume_tracking_after_panel_snow_timeout = 0

        self.enable_snow_depth_stow = True
        self.enable_snow_depth_averaging = False  # uses peak value
        self.snow_depth_threshold = 1.0  # in meters
        self.snow_depth_low_threshold = 3.333

        # The original "snow depth" was implicitly "snow depth on the ground"
        # In 01/2019 we started adding support for "snow depth on the panels"
        self.enable_panel_snow_depth_stow = True
        self.enable_panel_snow_depth_averaging = False  # uses peak value
        self.panel_snow_depth_threshold = 1.0  # in meters
        self.panel_snow_depth_low_threshold = 2.222  # in meters

        self.minimum_stations_required = 0  # Feature is disabled until user tells us the required quantity

        # Future TODO - Once we start polling NOAA for data, there is
        # another enable and parameter needed here.


class WeatherMonitor(WeatherMonitorPersistableConfig):
    """
    The job of this class is to aggregate the data from any WX-equipped
    Assets and make decisions regarding stow/no stow, etc.
    """
    def __init__(self, asset_manager, persistent_state, schedule_func, my_site, on_weather_stow, on_config_update, send_increase_reporting, send_weather_hist):
        """
        Constructor. Parameters include:
        asset_manager - holder of ALL the Asset objects
        persistent_state - holds a small amount of data in NVRAM
        schedule_func - used for timing
        my_site - knows our uptime
        """
        WeatherMonitorPersistableConfig.__init__(self)

        self.assets_active = {}

        self._pickle_helper = PickleHelper(self, WeatherMonitorPersistableConfig, WEATHERMON_CONFIG_FILE)

        if asset_manager is None:
            raise Exception("Error: WeatherMonitor requires an AssetManager!")
        else:
            self._asset_manager = asset_manager

        if persistent_state is None:
            raise Exception("Error: WeatherMonitor requires a means to save PersistentState!")
        else:
            self._persistent_state = persistent_state

        if schedule_func is None:
            raise Exception("Error: WeatherMonitor requires a schedule_func!")
        else:
            self._schedule_func = schedule_func

        if on_weather_stow is None:
            raise Exception("Error: WeatherMonitor requires a on_weather_stow callback")
        else:
            self.on_weather_stow = on_weather_stow

        if on_config_update is None:
            raise Exception("Error: WeatherMonitor requires a on_config_update callback!")
        else:
            self.on_config_update = on_config_update
            self.on_config_update()


        if send_increase_reporting is None:
            raise Exception("Error: WeatherMonitor requires a send_increase_reporting callback")
        else:
            self.send_cloud_increase_reporting_update = send_increase_reporting

        if send_weather_hist is None:
            raise Exception("Error: WeatherMonitor requires a send_weather_hist callback")
        else:
            self.send_cloud_weather_hist = send_weather_hist

        self._my_site = my_site

        if not self.load_config():
            self.default_config()
            self.save_config()

        self.stations_reporting = None
        self.total_stations = None

        self._total_temperature = None
        self.average_temperature = None

        self._total_wind_speed = None
        self.average_wind_speed = None
        self.max_wind_speed = None

        self._total_gust_speed = None
        self.average_gust_speed = None
        self.max_gust_speed = None

        self._total_snow_depth = None
        self.average_snow_depth = None
        self.max_snow_depth = None

        # The original "snow depth" was implicitly "snow depth on the ground"
        # In 01/2019 we started adding support for "snow depth on the panels"
        self._total_panel_snow_depth = None
        self.average_panel_snow_depth = None
        self.max_panel_snow_depth = None

        self._wind_speed_duration_so_far = 0
        self.resume_tracking_after_wind_timer = 0
        self.resume_tracking_after_snow_timer = 0
        self.resume_tracking_after_panel_snow_timer = 0

        
        # They hold snap addresses of assets to keep track of actual asset causing stow.
        self.max_panel_snow_depth_source = None
        self.max_wind_speed_source = None
        self.max_gust_speed_source = None
        self.max_snow_depth_source = None
        self.snap_addr = None

        # for increased avg wind and wind gust reporting
        self.increase_avg_wind_reporting = 0
        self.increase_wind_gust_reporting = 0
        self.increase_avg_snow_reporting = 0
        self.increase_panel_snow_reporting = 0
        self.increase_panel_snow_reported = False
        self.increase_avg_wind_reporting_reported = 3 
        self.increase_wind_gust_reporting_reported = 3 
        self.increase_avg_snow_reporting_reported = 3 
        self.increase_panel_snow_reporting_reported = 3 

        self.timer_for_increase_reporting_avg_wind = None
        self.start_timer_for_increase_reporting_avg_wind = False
        self.timer_for_increase_reporting_wind_gust = None
        self.start_timer_for_increase_reporting_wind_gust = False
        self.timer_for_increase_reporting_avg_snow = None
        self.start_timer_for_increase_reporting_avg_snow = False
        self.timer_for_increase_reporting_panel_snow = None
        self.start_timer_for_increase_reporting_panel_snow = False
        self.triggers_AVG_WIND_CLOSE = False
        self.triggers_WIND_GUST_CLOSE = False
        self.triggers_SNOW_CLOSE = False
        self.triggers_PANEL_SNOW_CLOSE = False
        self.avg_wind_source = None
        self.wind_gust_source = None
        self.avg_snow_source = None
        self.panel_snow_source = None
        self.on_startup_send_increase_reporting_update = True

        self.time_updated = datetime.datetime.now()


        # As of 01/10/2018 we now require hearing from a (configurable) minimum
        # number of weather stations before we allow tracking to resume.
        self.precautionary_stow_needed = False

        self.wind_stow_needed = False
        self.snow_stow_needed = False
        self.panel_snow_stow_needed = False
        if not self.enable_auto_resume_tracking_snow:
            # Snow on the ground...
            self.snow_stow_needed = self._persistent_state.get_state(PersistentState.SNOW_STOW_BIT)
            if self.snow_stow_needed:
                logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL, "A GROUND SNOW STOW condition is waiting to be manually cleared", extra=EventType.INDIVIDUAL_ASSET._dict)
        if not self.enable_auto_resume_tracking_panel_snow:
            # Snow on the solar panels...
            self.panel_snow_stow_needed = self._persistent_state.get_state(PersistentState.PANEL_SNOW_STOW_BIT)
            if self.panel_snow_stow_needed:
                logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL, "A PANEL SNOW STOW condition is waiting to be manually cleared", extra=EventType.INDIVIDUAL_ASSET._dict)
        self.stow_needed = self.precautionary_stow_needed or self.wind_stow_needed or self.snow_stow_needed or self.panel_snow_stow_needed

        self.POLLING_INTERVAL = 1.0
        self._schedule_func(self.POLLING_INTERVAL, self.weather_check)
        self.snow_stow_low_threshold_Enable = False
        self.panel_snow_stow_low_threshold_Enable = False
        self.start_up_data = True

    def load_config(self):
        return self._pickle_helper.load_config()

    def save_config(self):
        return self._pickle_helper.save_config()

    def update_config(self, new_config_dict):
        """update config based on user input (via SLUI)"""
        for key in new_config_dict:
            if hasattr(self, key):
                setattr(self, key, new_config_dict[key])
        if self.save_config():
            self.on_config_update()

    def get_wind_reporting_values(self):

        class Wind_reporting_values(object):
            def __init__(self):
                self.wind_reporting_close_percentage = 75
                self.wind_reporting_close_interval = 15
                self.snow_reporting_close_percentage = 75
                self.snow_reporting_close_interval = 15
                self.increased_weather_reporting_timeout = None

                PickleHelper(self, None, 'cloud_updater.pkl').load_config()

        return Wind_reporting_values()

    def aggregate_data(self):
        """Inspect our list of Assets and see what WX data has been reported"""
        self.stations_reporting = 0
        trackers_reporting = 0  # Row Boxes can now be equipped with PANEL SNOW SENSORS...

        self._total_temperature = 0

        self._total_wind_speed = 0
        self.max_wind_speed = 0

        self._total_gust_speed = 0
        self.max_gust_speed = 0

        self._total_snow_depth = 0
        self.max_snow_depth = 0
        self._total_panel_snow_depth = 0
        self.max_panel_snow_depth = 0

        # TODO decide on paradigm for missing data... should it be None or 0?
        self.average_temperature = 0
        self.average_wind_speed = 0
        self.average_gust_speed = 0
        self.average_snow_depth = 0
        self.average_panel_snow_depth = 0

        # for increased avg wind and wind gust reporting
        triggers_AVG_WIND_CLOSE = False
        triggers_WIND_GUST_CLOSE = False
        triggers_SNOW_CLOSE = False
        triggers_PANEL_SNOW_CLOSE = False
        avg_wind_source = None
        wind_gust_source = None
        avg_snow_source = None
        panel_snow_source = None
        reporting_values = self.get_wind_reporting_values()

        assets = self._asset_manager.get_assets().values()
        for asset in assets:
            # has_weather_sensor, has_reported_weather are triggered by two separate messages...
            if (asset.has_weather_sensor and asset.has_reported_weather) and not asset.offline:
                # TODO Consider checking on the "freshness" of the data too?

                # Row Boxes have started reporting SOME weather data (panel snow depth),
                # but it is not enough data to count them as Weather stations.
                # The reason we care is they would throw off all of the averages,
                # possibly even preventing a "stow on high winds"...
                if asset.device == 'Tracker':
                    trackers_reporting += 1

                    panel_snow_depth = asset.snow_depth
                    self._total_panel_snow_depth += panel_snow_depth

                    if self.max_panel_snow_depth < panel_snow_depth:
                        self.max_panel_snow_depth = panel_snow_depth
                        self.max_panel_snow_depth_source = asset.snap_address

                    # Check increasing panel snow
                    if self.enable_panel_snow_depth_stow:
                        if asset.snow_depth >= (self.panel_snow_depth_threshold * (reporting_values.snow_reporting_close_percentage / 100.0)):
                            if not triggers_PANEL_SNOW_CLOSE:
                                triggers_PANEL_SNOW_CLOSE = True
                                panel_snow_source = asset.snap_address
                else:
                    self.stations_reporting += 1

                    self._total_temperature += (asset.unit_temperature)

                    wind_speed = asset.average_wind_speed
                    self._total_wind_speed += wind_speed

                    if self.max_wind_speed < wind_speed:
                        self.max_wind_speed = wind_speed
                        self.max_wind_speed_source = asset.snap_address

                    gust_speed = asset.peak_wind_speed
                    self._total_gust_speed += gust_speed

                    if self.max_gust_speed < gust_speed:
                        self.max_gust_speed = gust_speed
                        self.max_gust_speed_source = asset.snap_address

                    snow_depth = asset.snow_depth
                    self._total_snow_depth += snow_depth

                    if self.max_snow_depth < snow_depth:
                        self.max_snow_depth = snow_depth
                        self.max_snow_depth_source = asset.snap_address

                    #Check increasing avg wind
                    if self.enable_wind_speed_stow:
                        if asset.average_wind_speed >= (self.wind_speed_threshold * (reporting_values.wind_reporting_close_percentage / 100.0)):
                            if not triggers_AVG_WIND_CLOSE:
                                triggers_AVG_WIND_CLOSE = True
                                avg_wind_source = asset.snap_address

                    #check increased wind gust
                    if self.enable_wind_gust_stow:
                        if asset.peak_wind_speed >= (self.wind_gust_threshold * (reporting_values.wind_reporting_close_percentage / 100.0)):
                            if not triggers_WIND_GUST_CLOSE:
                                triggers_WIND_GUST_CLOSE = True
                                wind_gust_source = asset.snap_address

                    # Check increasing snow
                    if self.enable_snow_depth_stow:
                        if asset.snow_depth >= (self.snow_depth_threshold * (reporting_values.snow_reporting_close_percentage / 100.0)):
                            if not triggers_SNOW_CLOSE:
                                triggers_SNOW_CLOSE = True
                                avg_snow_source = asset.snap_address

                self.assets_active[asset.snap_address] = asset

        if self.triggers_AVG_WIND_CLOSE != triggers_AVG_WIND_CLOSE:
            self.triggers_AVG_WIND_CLOSE = triggers_AVG_WIND_CLOSE
            if avg_wind_source is not None:
                self.avg_wind_source = avg_wind_source

        if self.triggers_WIND_GUST_CLOSE != triggers_WIND_GUST_CLOSE:
            self.triggers_WIND_GUST_CLOSE = triggers_WIND_GUST_CLOSE
            if wind_gust_source is not None:
                self.wind_gust_source = wind_gust_source

        if self.triggers_SNOW_CLOSE != triggers_SNOW_CLOSE:
            self.triggers_SNOW_CLOSE = triggers_SNOW_CLOSE
            if avg_snow_source is not None:
                self.avg_snow_source = avg_snow_source

        if self.triggers_PANEL_SNOW_CLOSE != triggers_PANEL_SNOW_CLOSE:
            self.triggers_PANEL_SNOW_CLOSE = triggers_PANEL_SNOW_CLOSE
            if panel_snow_source is not None:
                self.panel_snow_source = panel_snow_source

        if self.stations_reporting > 0:
            self.average_temperature = self._total_temperature / self.stations_reporting
            self.average_wind_speed = self._total_wind_speed / self.stations_reporting
            self.average_gust_speed = self._total_gust_speed / self.stations_reporting
            self.average_snow_depth = self._total_snow_depth / self.stations_reporting
            log.debug("avg temp=%f avg_wind_speed=%f max_wind_speed=%f avg_gust_speed=%f max_gust_speed=%f" %
                          (self.average_temperature,
                           self.average_wind_speed, self.max_wind_speed,
                           self.average_gust_speed, self.max_gust_speed))
            log.debug("avg_snow_depth=%f max_snow_depth=%f" %
                          (self.average_snow_depth, self.max_snow_depth))
        if trackers_reporting > 0:
            self.average_panel_snow_depth = self._total_panel_snow_depth / trackers_reporting
            log.debug("avg_panel_snow_depth=%f max_panel_snow_depth=%f" %
                          (self.average_panel_snow_depth, self.max_panel_snow_depth))

        if self.total_stations < self.stations_reporting:
            self.total_stations = self.stations_reporting

    def make_decisions(self):
        """Based on aggregated data and configured settings, decide to stow or not to stow"""

        stow_needed = False

        #
        # Mostly calculation of triggers here...
        #

        wind_stow_needed = False
        wind_gust = False
        wind_speed_trigger = False

        if self.enable_wind_speed_stow:
            if self.trigger_averaging:
                wind_speed = self.average_wind_speed
            else:
                wind_speed = self.max_wind_speed

            if wind_speed > self.wind_speed_threshold:
                self._wind_speed_duration_so_far += self.POLLING_INTERVAL
                if self._wind_speed_duration_so_far >= self.wind_speed_duration_required:
                    wind_stow_needed = True
                    wind_speed_trigger = True
            else:
                self._wind_speed_duration_so_far = 0
            if self.start_up_data:
                if not wind_speed_trigger:
                    wind_speed_trigger = self._persistent_state.get_state(PersistentState.WIND_STOW_BIT)
                    if wind_speed_trigger:
                        wind_stow_needed = True
                        self.wind_stow_needed = True

        if self.enable_wind_gust_stow:
            if self.trigger_averaging:
                gust_speed = self.average_gust_speed
            else:
                gust_speed = self.max_gust_speed
            if gust_speed > self.wind_gust_threshold:
                wind_stow_needed = True
                wind_gust = True
            if self.start_up_data:
                if not wind_gust:
                    wind_gust = self._persistent_state.get_state(PersistentState.WIND_STOW_BIT)
                    if wind_gust:
                        wind_stow_needed = True
                        self.wind_stow_needed = True

        # This is for snow on the ground
        snow_stow_needed = False
        if self.enable_snow_depth_stow:
            if self.enable_snow_depth_averaging:
                snow_depth = self.average_snow_depth
            else:
                snow_depth = self.max_snow_depth
            if snow_depth > self.snow_depth_threshold:
                snow_stow_needed = True
                if self.enable_auto_resume_tracking_snow:
                    self.snow_stow_low_threshold_Enable = True
                else:
                    self.snow_stow_low_threshold_Enable = False
            if self.snow_stow_low_threshold_Enable and self.enable_auto_resume_tracking_snow:
                if snow_depth > self.snow_depth_low_threshold:
                    snow_stow_needed = True

            if self.start_up_data:
                if not snow_stow_needed:
                    snow_stow_needed = self._persistent_state.get_state(PersistentState.SNOW_STOW_BIT)
                    if snow_stow_needed:
                        self.snow_stow_needed = True
                        if self.enable_auto_resume_tracking_snow:
                            self.snow_stow_low_threshold_Enable = True
                        else:
                            self.snow_stow_low_threshold_Enable = False



        # This is for snow on the solar panels
        panel_snow_stow_needed = False
        if self.enable_panel_snow_depth_stow:
            if self.enable_panel_snow_depth_averaging:
                panel_snow_depth = self.average_panel_snow_depth
            else:
                panel_snow_depth = self.max_panel_snow_depth
            if panel_snow_depth > self.panel_snow_depth_threshold:
                panel_snow_stow_needed = True
                if self.enable_auto_resume_tracking_panel_snow:
                    self.panel_snow_stow_low_threshold_Enable = True
                else:
                    self.panel_snow_stow_low_threshold_Enable = False
            if self.panel_snow_stow_low_threshold_Enable and self.enable_auto_resume_tracking_panel_snow:
                if panel_snow_depth > self.panel_snow_depth_low_threshold:
                    panel_snow_stow_needed = True
            if self.start_up_data:
                if not panel_snow_stow_needed:
                    panel_snow_stow_needed = self._persistent_state.get_state(PersistentState.PANEL_SNOW_STOW_BIT)
                    if panel_snow_stow_needed:
                        self.panel_snow_stow_needed = True
                        if self.enable_auto_resume_tracking_panel_snow:
                            self.panel_snow_stow_low_threshold_Enable = True
                        else:
                            self.panel_snow_stow_low_threshold_Enable = False

        precautionary_stow_needed = False
        if (self._my_site.uptime >= 600): # 10 minutes grace period...
            if self.stations_reporting < self.minimum_stations_required:
                precautionary_stow_needed = True
                if self.start_up_data:
                    self.start_up_data = False

        # TODO NOAA checks go here in the future...

        #
        # Mostly "delta-logging" here
        #

        if wind_stow_needed:
            self.resume_tracking_after_wind_timer = self.resume_tracking_after_wind_timeout
            if wind_stow_needed != self.wind_stow_needed:
                if wind_gust:
                    asset = self.get_source_asset(self.max_gust_speed_source)
                    logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL,
                                                          "High Wind Gust Weather Stow was triggered by %s due to a %.2f MPH wind gust. Threshold is %.2f MPH." %
                                                          (format_asset_identity(asset), gust_speed, self.wind_gust_threshold), extra=EventType.INDIVIDUAL_ASSET._dict)  
                    self.on_weather_stow(StowType.HIGH_WIND_GUST, gust_speed, self.wind_gust_threshold, asset)
                    
                if wind_speed_trigger:
                    asset = self.get_source_asset(self.max_wind_speed_source)
                    logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL,
                                                      "High Wind Speed Weather Stow was triggered by %s due to %.2f MPH average wind  speed. Threshold is %.2f MPH." %
                                                      (format_asset_identity(asset), wind_speed, self.wind_speed_threshold), extra=EventType.INDIVIDUAL_ASSET._dict)
                    self.on_weather_stow(StowType.HIGH_AVG_WIND, wind_speed, self.wind_speed_threshold, asset)
                self._persistent_state.set_state(PersistentState.WIND_STOW_BIT, True)
            self.wind_stow_needed = True
        elif self.resume_tracking_after_wind_timer > 0:
            if self.resume_tracking_after_wind_timer >= self.POLLING_INTERVAL:
                self.resume_tracking_after_wind_timer -= self.POLLING_INTERVAL
            else:
                self.resume_tracking_after_wind_timer = 0
            if self.resume_tracking_after_wind_timer == 0:
                logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL, "%d minutes of non-STOW wind conditions has occurred" % (self.resume_tracking_after_wind_timeout / 60,), extra=EventType.INDIVIDUAL_ASSET._dict)
                self.wind_stow_needed = False
                self._persistent_state.set_state(PersistentState.WIND_STOW_BIT, False)

        # Snow on the ground...
        if snow_stow_needed:
            self.resume_tracking_after_snow_timer = self.resume_tracking_after_snow_timeout
            if snow_stow_needed != self.snow_stow_needed:
                asset = self.get_source_asset(self.max_snow_depth_source)
                logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL, "Deep Snow Weather Stow was triggered by %s due to %.2f meter of snow. Threshold is %.2f meter." %
                                                                          (format_asset_identity(asset), snow_depth, self.snow_depth_threshold), extra=EventType.INDIVIDUAL_ASSET._dict)
                self.on_weather_stow(StowType.DEEP_SNOW, snow_depth, self.snow_depth_threshold, asset)                             
                self.snow_stow_needed = True
                self._persistent_state.set_state(PersistentState.SNOW_STOW_BIT, True)

        # Auto-resume support
        elif self.enable_auto_resume_tracking_snow:
            if self.resume_tracking_after_snow_timer >= self.POLLING_INTERVAL:
                self.resume_tracking_after_snow_timer -= self.POLLING_INTERVAL
            else:
                self.resume_tracking_after_snow_timer = 0
            if self.resume_tracking_after_snow_timer == 0 and self.snow_stow_needed:
                logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL, "%d minutes of non-STOW ground snow conditions has occurred" % (self.resume_tracking_after_snow_timeout / 60,), extra=EventType.INDIVIDUAL_ASSET._dict)
                self.snow_stow_needed = False
                self._persistent_state.set_state(PersistentState.SNOW_STOW_BIT, False)
                if self.snow_stow_low_threshold_Enable and self.enable_auto_resume_tracking_snow:
                    self.snow_stow_low_threshold_Enable = False

        # Snow on the solar panels...
        if panel_snow_stow_needed:
            self.resume_tracking_after_panel_snow_timer = self.resume_tracking_after_panel_snow_timeout
            if panel_snow_stow_needed != self.panel_snow_stow_needed:
                asset = self.get_source_asset(self.max_panel_snow_depth_source)
                logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL, "Deep Panel Snow Weather Stow was triggered by %s due to %.2f meter of snow. Threshold is %.2f meter." %
                                                  (format_asset_identity(asset), panel_snow_depth, self.panel_snow_depth_threshold), extra=EventType.INDIVIDUAL_ASSET._dict)
                self.on_weather_stow(StowType.DEEP_PANEL_SNOW, panel_snow_depth, self.panel_snow_depth_threshold, asset)
                self.panel_snow_stow_needed = True
                self._persistent_state.set_state(PersistentState.PANEL_SNOW_STOW_BIT, True)

        # Auto-resume support
        elif self.enable_auto_resume_tracking_panel_snow:
            if self.resume_tracking_after_panel_snow_timer >= self.POLLING_INTERVAL:
                self.resume_tracking_after_panel_snow_timer -= self.POLLING_INTERVAL
            else:
                self.resume_tracking_after_panel_snow_timer = 0
            if self.resume_tracking_after_panel_snow_timer == 0 and self.panel_snow_stow_needed:
                logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL, "%d minutes of non-STOW panel snow conditions has occurred" % (self.resume_tracking_after_panel_snow_timeout / 60,), extra=EventType.INDIVIDUAL_ASSET._dict)
                self.panel_snow_stow_needed = False
                self._persistent_state.set_state(PersistentState.PANEL_SNOW_STOW_BIT, False)
                if self.panel_snow_stow_low_threshold_Enable and self.enable_auto_resume_tracking_panel_snow:
                    self.panel_snow_stow_low_threshold_Enable = False

        if precautionary_stow_needed != self.precautionary_stow_needed:
            if precautionary_stow_needed:
                logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL, "Lack of weather info has triggered a STOW condition", extra=EventType.INDIVIDUAL_ASSET._dict)
                self.on_weather_stow(StowType.LACK_WEATHER_INFO, 0, 0)
            else:
                logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL, "Lack of weather info no longer triggering a STOW condition", extra=EventType.INDIVIDUAL_ASSET._dict)
            self.precautionary_stow_needed = precautionary_stow_needed


        # Final aggregation here (We want AssetCommander to be able to check a single source)
        self.stow_needed = self.precautionary_stow_needed or self.wind_stow_needed or self.snow_stow_needed or self.panel_snow_stow_needed

        # For Increased weather Reporting
        send_to_cloud_now = False
        reporting_values = self.get_wind_reporting_values()

        time_since_last_report = datetime.datetime.now() - self.time_updated
        seconds_since_last_report = time_since_last_report.total_seconds()
        minutes_since_last_report = seconds_since_last_report / 60.0
        
        if self.on_startup_send_increase_reporting_update:
            if self.stations_reporting > 0:
                send_to_cloud_now = True

        if reporting_values.increased_weather_reporting_timeout is not None:  # case : if some value saved on SLUI
            # case: for avg wind
            if (self.triggers_AVG_WIND_CLOSE):
                if self.increase_avg_wind_reporting == 0:
                    self.increase_avg_wind_reporting = 1
                if self.start_timer_for_increase_reporting_avg_wind: # wind got increased again cancel timer
                    self.start_timer_for_increase_reporting_avg_wind = False
                    send_to_cloud_now = True
                if minutes_since_last_report >= reporting_values.wind_reporting_close_interval:
                    send_to_cloud_now = True
            else:
                if self.increase_avg_wind_reporting == 1:
                    if (self.start_timer_for_increase_reporting_avg_wind == False): # wind got low so start timer
                        self.timer_for_increase_reporting_avg_wind = datetime.datetime.now()
                        self.start_timer_for_increase_reporting_avg_wind = True
                        send_to_cloud_now = True
                    if (self.start_timer_for_increase_reporting_avg_wind == True): # timer already started
                        elapsed_time_avg_wind = datetime.datetime.now() - self.timer_for_increase_reporting_avg_wind
                        elapsed_time_avg_wind_in_minutes = elapsed_time_avg_wind.total_seconds() / 60.0
                        if elapsed_time_avg_wind_in_minutes >= reporting_values.increased_weather_reporting_timeout:
                            # case: timer  timeout / ended.
                            self.start_timer_for_increase_reporting_avg_wind = False
                            send_to_cloud_now = True
                            self.increase_avg_wind_reporting = 0
                        else:
                            if minutes_since_last_report >= reporting_values.wind_reporting_close_interval:
                                send_to_cloud_now = True

            # case: for wind gust
            if (self.triggers_WIND_GUST_CLOSE) :
                if (self.increase_wind_gust_reporting == 0):
                    self.increase_wind_gust_reporting = 1
                if self.start_timer_for_increase_reporting_wind_gust:
                    self.start_timer_for_increase_reporting_wind_gust = False
                    send_to_cloud_now = True
                if minutes_since_last_report >= reporting_values.wind_reporting_close_interval:
                    send_to_cloud_now = True
            else:
                if self.increase_wind_gust_reporting == 1:
                    if (self.start_timer_for_increase_reporting_wind_gust == False):
                        self.timer_for_increase_reporting_wind_gust = datetime.datetime.now()
                        self.start_timer_for_increase_reporting_wind_gust = True
                        send_to_cloud_now = True
                    if (self.start_timer_for_increase_reporting_wind_gust == True): # timer already started
                        elapsed_time_wind_gust = datetime.datetime.now() - self.timer_for_increase_reporting_wind_gust
                        elapsed_time_wind_gust_in_minutes = elapsed_time_wind_gust.total_seconds() / 60.0
                        if elapsed_time_wind_gust_in_minutes >= reporting_values.increased_weather_reporting_timeout:
                            # case: timer  timeout / ended.
                            self.start_timer_for_increase_reporting_wind_gust = False
                            send_to_cloud_now = True
                            self.increase_wind_gust_reporting = 0
                        else:
                            if minutes_since_last_report >= reporting_values.wind_reporting_close_interval:
                                send_to_cloud_now = True

            # case: for avg snow
            if (self.triggers_SNOW_CLOSE):
                if self.increase_avg_snow_reporting == 0:
                    self.increase_avg_snow_reporting = 1
                if self.start_timer_for_increase_reporting_avg_snow: # snow got increased again now cancel timer
                    self.start_timer_for_increase_reporting_avg_snow = False
                    send_to_cloud_now = True
                if minutes_since_last_report >= reporting_values.snow_reporting_close_interval:
                    send_to_cloud_now = True
            else:
                if self.increase_avg_snow_reporting == 1:
                    if  (self.start_timer_for_increase_reporting_avg_snow == False): # snow git low so start timer
                        self.timer_for_increase_reporting_avg_snow = datetime.datetime.now()
                        self.start_timer_for_increase_reporting_avg_snow = True
                        send_to_cloud_now = True
                    if (self.start_timer_for_increase_reporting_avg_snow == True): #timer already started
                        elapsed_time_avg_snow = datetime.datetime.now() - self.timer_for_increase_reporting_avg_snow
                        elapsed_time_avg_snow_in_minutes = elapsed_time_avg_snow.total_seconds() / 60.0
                        if elapsed_time_avg_snow_in_minutes >= reporting_values.increased_weather_reporting_timeout:
                            # case: timer  timeout / ended.
                            self.start_timer_for_increase_reporting_avg_snow = False
                            send_to_cloud_now = True
                            self.increase_avg_snow_reporting = 0
                        else:
                            if minutes_since_last_report >= reporting_values.wind_reporting_close_interval:
                                send_to_cloud_now = True

            # case: panel snow
            if (self.triggers_PANEL_SNOW_CLOSE):
                if self.increase_panel_snow_reporting == 0:
                    self.increase_panel_snow_reporting = 1
                if self.start_timer_for_increase_reporting_panel_snow:  #panel snow got increased again now cancel timer
                    self.start_timer_for_increase_reporting_panel_snow = False
                    send_to_cloud_now = True
                if minutes_since_last_report >= reporting_values.snow_reporting_close_interval:
                    send_to_cloud_now = True
            else:
                if (self.increase_panel_snow_reporting == 1):
                    if (self.start_timer_for_increase_reporting_panel_snow == False): # panel snow got low so start timer
                        self.timer_for_increase_reporting_panel_snow = datetime.datetime.now()
                        self.start_timer_for_increase_reporting_panel_snow = True
                        send_to_cloud_now = True
                    if (self.start_timer_for_increase_reporting_panel_snow == True): # timer already started
                        elapsed_time_p_snow = datetime.datetime.now() - self.timer_for_increase_reporting_panel_snow
                        elapsed_time_p_snow_in_minutes = elapsed_time_p_snow.total_seconds() / 60.0
                        if elapsed_time_p_snow_in_minutes >= reporting_values.increased_weather_reporting_timeout:
                            # case: timer  timeout / ended.
                            self.start_timer_for_increase_reporting_panel_snow = False
                            send_to_cloud_now = True
                            self.increase_panel_snow_reporting = 0
                        else:
                            if minutes_since_last_report >= reporting_values.snow_reporting_close_interval:
                                send_to_cloud_now = True
        else:
            if (self.triggers_WIND_GUST_CLOSE):
                if self.increase_wind_gust_reporting == 0:
                    self.increase_wind_gust_reporting = 1
                if minutes_since_last_report >= reporting_values.wind_reporting_close_interval:
                    send_to_cloud_now = True
            else:
                if self.increase_wind_gust_reporting == 1:
                    self.increase_wind_gust_reporting = 0

            if (self.triggers_AVG_WIND_CLOSE):
                if self.increase_avg_snow_reporting == 0:
                    self.increase_avg_snow_reporting = 1
                if minutes_since_last_report >= reporting_values.wind_reporting_close_interval:
                    send_to_cloud_now = True
            else:
                if self.increase_avg_snow_reporting == 1:
                    self.increase_avg_snow_reporting = 0

            if (self.triggers_SNOW_CLOSE):
                if self.increase_avg_snow_reporting == 0:
                    self.increase_avg_snow_reporting = 1
                if minutes_since_last_report >= reporting_values.snow_reporting_close_interval:
                    send_to_cloud_now = True
            else:
                if self.increase_avg_snow_reporting == 1:
                    self.increase_avg_snow_reporting = 0

            if (self.triggers_PANEL_SNOW_CLOSE):
                if self.increase_panel_snow_reporting == 0:
                    self.increase_panel_snow_reporting = 1
                if minutes_since_last_report >= reporting_values.snow_reporting_close_interval:
                    send_to_cloud_now = True
            else:
                if self.increase_panel_snow_reporting == 1:
                    self.increase_panel_snow_reporting = 0

        if send_to_cloud_now:
            self.time_updated = datetime.datetime.now()

            #send increasing update for cloud
            reporting_source = None
            avg_wind_source = None
            wind_gust_source = None
            avg_snow_source = None
            panel_snow_source = None
            if self.avg_wind_source is not None:
                avg_wind_source = self.get_source_asset(self.avg_wind_source)
                reporting_source = avg_wind_source
            if self.wind_gust_source is not None:
                wind_gust_source = self.get_source_asset(self.wind_gust_source)
                reporting_source = wind_gust_source
            if self.avg_snow_source is not None:
                avg_snow_source = self.get_source_asset(self.avg_snow_source)
                reporting_source = avg_snow_source
            if self.panel_snow_source is not None:
                panel_snow_source = self.get_source_asset(self.panel_snow_source)
                reporting_source = panel_snow_source

            if self.on_startup_send_increase_reporting_update:
                self.on_startup_send_increase_reporting_update = False
                self.increase_avg_wind_reporting_reported = self.increase_avg_wind_reporting
                self.increase_wind_gust_reporting_reported = self.increase_wind_gust_reporting
                self.increase_avg_snow_reporting_reported = self.increase_avg_snow_reporting
                self.increase_panel_snow_reporting_reported = self.increase_panel_snow_reporting
                self.send_cloud_increase_reporting_update(self.increase_avg_wind_reporting_reported, self.increase_wind_gust_reporting_reported, self.increase_avg_snow_reporting_reported, self.increase_panel_snow_reporting_reported, reporting_source)

            if self.increase_avg_wind_reporting != self.increase_avg_wind_reporting_reported:
                if (avg_wind_source == wind_gust_source):
                    self.increase_wind_gust_reporting_reported = self.increase_wind_gust_reporting
                if (avg_wind_source == avg_snow_source):
                    self.increase_avg_snow_reporting_reported = self.increase_avg_snow_reporting
                self.increase_avg_wind_reporting_reported = self.increase_avg_wind_reporting
                self.send_cloud_increase_reporting_update(self.increase_avg_wind_reporting_reported,
                                                          self.increase_wind_gust_reporting_reported,
                                                          self.increase_avg_snow_reporting_reported,
                                                          self.increase_panel_snow_reporting_reported,
                                                          avg_wind_source)

            if self.increase_wind_gust_reporting != self.increase_wind_gust_reporting_reported:
                if (wind_gust_source == avg_snow_source):
                    self.increase_avg_snow_reporting_reported = self.increase_avg_snow_reporting
                self.increase_wind_gust_reporting_reported = self.increase_wind_gust_reporting
                self.send_cloud_increase_reporting_update(self.increase_avg_wind_reporting_reported,
                                                          self.increase_wind_gust_reporting_reported,
                                                          self.increase_avg_snow_reporting_reported,
                                                          self.increase_panel_snow_reporting_reported,
                                                          wind_gust_source)


            if self.increase_avg_snow_reporting != self.increase_avg_snow_reporting_reported:
                self.increase_avg_snow_reporting_reported = self.increase_avg_snow_reporting
                self.send_cloud_increase_reporting_update(self.increase_avg_wind_reporting_reported,
                                                          self.increase_wind_gust_reporting_reported,
                                                          self.increase_avg_snow_reporting_reported,
                                                          self.increase_panel_snow_reporting_reported,
                                                          avg_snow_source)

            if self.increase_panel_snow_reporting != self.increase_panel_snow_reporting_reported:
                self.increase_panel_snow_reporting_reported = self.increase_panel_snow_reporting
                self.send_cloud_increase_reporting_update(self.increase_avg_wind_reporting_reported,
                                                          self.increase_wind_gust_reporting_reported,
                                                          self.increase_avg_snow_reporting_reported,
                                                          self.increase_panel_snow_reporting_reported,
                                                          panel_snow_source)

            #send increasing updates for all weather stations
            assets = self._asset_manager.get_assets().values()

            for asset in assets:
                # has_weather_sensor, has_reported_weather are triggered by two separate messages...
                if (asset.has_weather_sensor and asset.has_reported_weather) and not asset.offline:
                    if asset.device == 'Tracker':
                        if (self.increase_panel_snow_reporting == 1) or (self.increase_panel_snow_reported == True):
                            self.send_cloud_weather_hist(asset)
                            self.increase_panel_snow_reported = True
                            if self.increase_panel_snow_reporting != 1:
                                self.increase_panel_snow_reported = False
                    else:
                        self.send_cloud_weather_hist(asset)

    def weather_check(self):
        self.aggregate_data()
        self.make_decisions()
        if self.stations_reporting > 0:
            if self.start_up_data:
                self.start_up_data = False

    def cancel_snow_stow(self, who):
        if self.snow_stow_needed:
            logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL, "Stow on snow depth was manually cleared by %s" % (who,), extra=EventType.INDIVIDUAL_ASSET._dict)
            self.snow_stow_needed = False
            # Manual intervention cancels auto-resume timer...
            self.resume_tracking_after_snow_timer = 0
            # "snow stow" is such a big deal we even remember it across power outages/reboots
            self._persistent_state.set_state(PersistentState.SNOW_STOW_BIT, self.snow_stow_needed)
            return True  # yes we took some action
        else:
            return False  # meaning we did nothing because no "snow stow" was in effect

    def cancel_panel_snow_stow(self, who):
        if self.panel_snow_stow_needed:
            logging.getLogger('EventLog').log(WEATHER_STOW_LOG_LEVEL, "Stow on panel snow depth was manually cleared by %s" % (who,), extra=EventType.INDIVIDUAL_ASSET._dict)
            self.panel_snow_stow_needed = False
            # Manual intervention cancels auto-resume timer...
            self.resume_tracking_after_panel_snow_timer = 0
            self._persistent_state.set_state(PersistentState.PANEL_SNOW_STOW_BIT, self.panel_snow_stow_needed)
            return True  # yes we took some action
        else:
            return False  # meaning we did nothing because no "panel snow stow" was in effect
    
    def get_source_asset(self, id):
        if id not in self.assets_active:
            self.assets_active[id] = Asset(id)
        return self.assets_active[id]



if __name__ == "__main__":
    from tornado.ioloop import IOLoop, PeriodicCallback
    import tornado.ioloop
    import random
    import datetime
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


    def test_weather_logs():
        logging.basicConfig(level=logging.INFO)
        from controller_enums import TrackerPresets
        import monotonic


        def dummy_scheduler(seconds, func):
            pcb = PeriodicCallback(func, seconds * 100)
            pcb.start()
            return pcb

        def dummy_on_weather_stow(HIGH_AVG_WIND, wind_speed, wind_speed_threshold, asset):
            return None

        def dummy_on_weather_config_update():
            return None


        class FakeSite(object):

            def __init__(self):
                self.operational_mode = 1#TrackerPresets.FLAT.value
                self.tracking_enable = True
                # self.tracking_enable = False
                self.backtracking_enable = True
                # This is used in computing uptime (see property below)
                self._startup_time = monotonic.monotonic()

            def uptime(self):
                return monotonic.monotonic() - self._startup_time


        class FakeAsset1(object):

            def __init__(self):
                a = Asset.Asset(asset_id=0)
                a.has_weather_sensor = True
                a.has_reported_weather =True
                a.average_wind_speed = 10.0 # + random.randint(1, 30)
                a.peak_wind_speed = 10.0
                a.snow_depth = 0
                address = "0ac3b7"
                #address = "0ac44a"
                #a.device = 'Tracker'
                a.snap_address = address.decode("hex")

                #a.peak_wind_speed = 55.0

                self.assets = {"lion": a}  # {asset_id: Asset} favorite_color = {"lion": "yellow", "kitty": "red"}
                self.operational_mode = TrackerPresets.FLAT.value
                self.tracking_enable = True
                # self.tracking_enable = False
                self.backtracking_enable = True
                # This is used in computing uptime (see property below)
                self._startup_time = monotonic.monotonic()

            def get_assets(self):

                return self.assets


        class FakeAsset2(object):

            def __init__(self):
                a = Asset.Asset(asset_id=0)
                a.has_weather_sensor = True
                a.has_reported_weather =True
                a.average_wind_speed = 40.0
                address = "0a1d6a"
                address = "0ac44a"
                a.device = 'Tracker'
                a.snap_address = address.decode("hex")

                #a.peak_wind_speed = 21.0

                self.assets = {"lion": a}  # {asset_id: Asset} favorite_color = {"lion": "yellow", "kitty": "red"}
                self.operational_mode = TrackerPresets.FLAT.value
                self.tracking_enable = True
                # self.tracking_enable = False
                self.backtracking_enable = True
                # This is used in computing uptime (see property below)
                self._startup_time = monotonic.monotonic()

            def get_assets(self):

                return self.assets



        asset_manager1 = FakeAsset1()
        asset_manager2 = FakeAsset2()

        my_site = FakeSite()

        my_persistent_state = PersistentState()

        def from_asset1():
            WeatherMonitor(asset_manager1, my_persistent_state, dummy_scheduler, my_site, dummy_on_weather_stow, dummy_on_weather_config_update)

        def from_asset2():
            WeatherMonitor(asset_manager2, my_persistent_state, dummy_scheduler, my_site, dummy_on_weather_stow, dummy_on_weather_config_update)


        #from_asset1()
        tornado.ioloop.IOLoop.current().add_timeout(datetime.timedelta(seconds=0), from_asset1)
        #tornado.ioloop.IOLoop.current().add_timeout(datetime.timedelta(seconds=6), from_asset2)
        #tornado.ioloop.IOLoop.current().add_timeout(datetime.timedelta(seconds=15), from_asset1)
        #tornado.ioloop.IOLoop.current().add_timeout(datetime.timedelta(seconds=25), tornado.ioloop.IOLoop.current().stop)
        tornado.ioloop.IOLoop.current().start()

    test_weather_logs()
