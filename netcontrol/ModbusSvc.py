"""ModbusSvc - provide MODBUS/TCP access to NC data"""

import struct
import logging
import time
import datetime
import tornado
from pyfiria.net import modbus_tcp_tornado
import bisect
import binascii
import terrasmart_cloud_pb2

log = logging.getLogger(__name__)


#--- Define CODEC types used to map system values to 16-bit MODBUS register blocks ---
class Asciiz(object):
    def __init__(self, nregs):
        self.size = nregs
        self.decoder = struct.Struct(">%dH" % self.size)

    def get_reg16_seq(self, strval):
        """Convert native to reg16 sequence. (for MODBUS reads)"""
        n = self.size * 2
        strval = strval[:n-1].ljust(n, '\x00')
        return self.decoder.unpack(strval)

    def get_native(self, reg16_seq):
        """Convert reg16 sequence to native. (for MODBUS writes)"""
        return self.decoder.pack(*reg16_seq)


def mk_codec(struct_encoder):
    """Create simple codec class for given struct format string"""
    class T(object):
        def __init__(self, nregs):
            self.dec = struct.Struct(">%dH" % nregs)
            self.enc = struct.Struct(struct_encoder)

        def get_reg16_seq(self, val):
            """Convert native to reg16 sequence. (for MODBUS reads)"""
            fbin = self.enc.pack(val)
            return self.dec.unpack(fbin)

    return T

S16 = Bool = mk_codec(">h")
U16 = Bool = mk_codec(">H")
U32 = mk_codec(">I")
U64 = mk_codec(">Q")
Float32 = mk_codec(">f")

class Reg(object):
    """Modbus parameter definition, representing a block of 16-bit registers"""
    def __init__(self, addr, size, codec, getter, setter=None):
        self.addr = addr
        self.size = size
        self.codec = codec
        self.getter = getter
        self.setter = setter

    def __cmp__(self, other):
        """Sort by address to support operations spanning multiple parameters"""
        return self.addr - other.addr

    def get_val(self, svc, dev_id):
        """Return this parameter's value, given the ModbusSvc instance and requested device ID"""
        if callable(self.getter):
            return self.getter(svc, dev_id)
        else:
            return self.getter


def datetime_to_timestamp(utc_dt):
    """Convert UTC DateTime object to timestamp (epoch seconds)."""
    timestamp = (utc_dt - datetime.datetime(1970, 1, 1)).total_seconds()
    return timestamp


tc_regs =  [  # Reg(addr, size, codec, item)
    Reg(0, 3, Asciiz, "TRAK"),  # System ID
    # System
    Reg(3, 10, Asciiz, lambda svc, id: svc.site.site_name),  # Site Name
    Reg(13, 10, Asciiz, lambda svc, id: svc.site.site_organization),  # Organization
    Reg(23, 10, Asciiz, lambda svc, id: svc.site.site_contact),  # Contact
    Reg(33, 4, U64, lambda svc, id: svc.site.uptime),  # System Uptime
    Reg(37, 8, Asciiz, lambda svc, id: svc.site.software_version),  # SW Version
    Reg(45, 1, Bool, lambda svc, id: svc.site.has_weather),  # Has WX
    Reg(46, 1, Bool, lambda svc, id: svc.e_stop_mon.in_estop),  # E-Stop Active
    Reg(47, 1, Bool, lambda svc, id: svc.site.tracking_enable),  # Tracking Enabled
    Reg(48, 1, Bool, lambda svc, id: svc.site.backtracking_enable),  # Backtracking Enabled
    Reg(49, 2, Float32, lambda svc, id: svc.sun_tracker.current_angle), # Calculated panel angle
    Reg(51, 1, U16, lambda svc, id: svc.asset_commander.panel_state.value),  # Commanded tracking state
    Reg(52, 2, Float32, lambda svc, id: svc.nccb.unit_temperature),  # Unit temperature
    # Power
    Reg(54, 2, Float32, lambda svc, id: svc.nccb.battery_voltage),  # Battery voltage
    Reg(56, 2, Float32, lambda svc, id: svc.nccb.battery_current),  # Battery current
    Reg(58, 1, U16, lambda svc, id: svc.nccb.battery_charged),  # Batt charge %
    Reg(59, 1, U16, lambda svc, id: svc.nccb.battery_health),  # Batt health %
    Reg(60, 2, Float32, lambda svc, id: svc.nccb.charger_voltage),  # Charger voltage
    Reg(62, 2, Float32, lambda svc, id: svc.nccb.charger_current),  # Charger current
    Reg(64, 2, Float32, lambda svc, id: svc.nccb.solar_voltage),  # Solar voltage
    Reg(66, 2, Float32, lambda svc, id: svc.nccb.solar_current),  # Solar current
    # Mesh
    Reg(68, 1, U16, lambda svc, id: svc.asset_poller.total_assets),  # Total num assets
    Reg(69, 1, U16, lambda svc, id: svc.asset_poller.assets_reporting),  # Num assets reporting
    Reg(70, 4, U64, lambda svc, id: datetime_to_timestamp(svc.asset_poller.latest_response)),  # Time of latest poll response
    Reg(74, 2, U32, lambda svc, id: int(svc.bridge.bridge_address.replace('.',''), 16)),  # SNAP bridge ADDR
    Reg(76, 1, U16, lambda svc, id: svc.bridge.bridge_script_crc),  # SNAP bridge CRC
    Reg(77, 1, U16, lambda svc, id: svc.bridge.bridge_channel),  # SNAP bridge channel
    Reg(78, 1, U16, lambda svc, id: svc.bridge.bridge_network_id),  # SNAP bridge NET_ID
    # GPS
    Reg(79, 1, Bool, lambda svc, id: int(svc.gps.num_sats) >= 3),  # Fix acquired
    Reg(80, 4, U64, lambda svc, id: datetime_to_timestamp(svc.gps.fix_time)),  # Fix timestamp
    Reg(84, 1, U16, lambda svc, id: int(svc.gps.num_sats)),  # Num satellites
    Reg(85, 2, Float32, lambda svc, id: svc.gps.gps_latitude),  # Latitude
    Reg(87, 2, Float32, lambda svc, id: svc.gps.gps_longitude),  # Longitude
    # Cell Modem
    Reg(89, 1, Bool, lambda svc, id: svc.modem.ppp_up),  # Link status
    Reg(90, 1, S16, lambda svc, id: int(svc.modem.rssi_dbm)),  # RSSI
    Reg(91, 8, Asciiz, lambda svc, id: svc.modem.wan_ip),  # WAN IP addr
    Reg(99, 8, Asciiz, lambda svc, id: svc.modem.lan_ip),  # LAN IP addr
    Reg(107, 2, U32, lambda svc, id: svc.modem.uptime),  # Modem uptime
    # NTP
    Reg(109, 1, Bool, lambda svc, id: svc.ntp.time_synchronized),  # NTP syc status
    Reg(110, 16, Asciiz, lambda svc, id: svc.ntp.time_server),  # NTP Server addr
    Reg(126, 2, Float32, lambda svc, id: svc.ntp.time_offset),  # NTP offset
    # Alerts
    Reg(128, 4, U64, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.ESTOP)[0]),  # Alert 1: Timestamp
    Reg(132, 1, U16, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.ESTOP)[1]),  # Type
    Reg(133, 20, Asciiz, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.ESTOP)[2]),  # Message
    Reg(153, 4, U64, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.ASSETS_REPORTING)[0]),  # Alert 2: Timestamp
    Reg(157, 1, U16, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.ASSETS_REPORTING)[1]),  # Type
    Reg(158, 20, Asciiz, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.ASSETS_REPORTING)[2]),  # Message
    Reg(178, 4, U64, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.GPS)[0]),  # Alert 3: Timestamp
    Reg(182, 1, U16, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.GPS)[1]),  # Type
    Reg(183, 20, Asciiz, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.GPS)[2]),  # Message
    Reg(203, 4, U64, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.CLOCK)[0]),  # Alert 4: Timestamp
    Reg(207, 1, U16, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.CLOCK)[1]),  # Type
    Reg(208, 20, Asciiz, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.CLOCK)[2]),  # Message
    Reg(228, 4, U64, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.MODEM)[0]),  # Alert 5: Timestamp
    Reg(232, 1, U16, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.MODEM)[1]),  # Type
    Reg(233, 20, Asciiz, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.MODEM)[2]),  # Message
    Reg(253, 4, U64, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.NTP)[0]),  # Alert 6: Timestamp
    Reg(257, 1, U16, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.NTP)[1]),  # Type
    Reg(258, 20, Asciiz, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.NTP)[2]),  # Message
    # Alerts 7 and 8 are currently spares
    Reg(278, 4, U64, lambda svc, id: 0),  # Alert 7: Timestamp
    Reg(282, 1, U16, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.UNKNOWN)),  # Type
    # We could choose to instead more closely match the original 6 alerts
    #Reg(282, 1, U16, lambda svc, id: 0xffff),  # Type
    Reg(283, 20, Asciiz, lambda svc, id: ''),  # Message
    Reg(303, 4, U64, lambda svc, id: 0),  # Alert 8: Timestamp
    Reg(307, 1, U16, lambda svc, id: svc.get_alert(terrasmart_cloud_pb2.Alert.UNKNOWN)),  # Type
    # We could choose to instead more closely match the original 6 alerts
    #Reg(307, 1, U16, lambda svc, id: 0xffff),  # Type
    Reg(308, 20, Asciiz, lambda svc, id: ''),  # Message
]

remote_regs =  [  # Reg(addr, size, codec, item)
    Reg(0, 8, Asciiz, lambda svc, id: svc.get_asset(id, 'device')),  # Device Type
    # System
    Reg(8, 4, U64, lambda svc, id: datetime_to_timestamp(svc.get_asset(id, 'last_reported'))),  # Timestamp last reported
    Reg(12, 8, Asciiz, lambda svc, id: svc.get_asset(id, 'model')),  # Model Num
    Reg(20, 8, Asciiz, lambda svc, id: str(svc.get_asset(id, 'hardware_rev'))),  # HW Rev 
    Reg(28, 8, Asciiz, lambda svc, id: svc.get_asset(id, 'firmware_rev_string')),  # FW Rev 
    Reg(36, 2, U32, lambda svc, id: svc.get_asset(id, 'uptime')),  # Uptime 
    Reg(38, 2, U32, lambda svc, id: int(binascii.hexlify(svc.get_asset(id, 'snap_address')), 16)),  # SNAP Address
    Reg(40, 1, S16, lambda svc, id: svc.get_asset(id, 'radio_link_quality')),  # RSSI
    Reg(41, 1, Bool, lambda svc, id: svc.get_asset(id, 'has_weather_sensor')),  # Has WX 
    Reg(42, 1, Bool, lambda svc, id: svc.get_asset(id, 'has_tracker_hardware')),  # Has Tracker 
    Reg(43, 16, Asciiz, lambda svc, id: svc.get_asset(id, 'location_text')),  # Location text 
    Reg(59, 2, Float32, lambda svc, id: svc.get_asset(id, 'location_lat')),  # Latitude 
    Reg(61, 2, Float32, lambda svc, id: svc.get_asset(id, 'location_lng')),  # Longitude 
    Reg(63, 2, U32, lambda svc, id: svc.get_asset(id, 'config_flags')),  # Config flags 
    # Power
    Reg(65, 2, Float32, lambda svc, id: svc.get_asset(id, 'battery_voltage')),  # Battery voltage 
    Reg(67, 2, Float32, lambda svc, id: svc.get_asset(id, 'battery_current')),  # Battery current 
    Reg(69, 1, U16, lambda svc, id: svc.get_asset(id, 'battery_charged')),  # Batt charge % 
    Reg(70, 1, U16, lambda svc, id: svc.get_asset(id, 'battery_health')),  # Batt health % 
    Reg(71, 2, Float32, lambda svc, id: svc.get_asset(id, 'charger_voltage')),  # Charger voltage 
    Reg(73, 2, Float32, lambda svc, id: svc.get_asset(id, 'charger_current')),  # Charger current 
    Reg(75, 2, Float32, lambda svc, id: svc.get_asset(id, 'solar_voltage')),  # Solar voltage 
    Reg(77, 2, Float32, lambda svc, id: svc.get_asset(id, 'solar_current')),  # Solar current 
    # Tracker
    Reg(79, 2, U32, lambda svc, id: svc.get_asset(id, 'status_bits')),  # Controller status bitfields 
    Reg(81, 1, U16, lambda svc, id: svc.get_asset(id, 'tracking_status_bits')),  # Tracking status enum 
    Reg(82, 2, Float32, lambda svc, id: svc.get_asset(id, 'current_angle')),  # Rack current angle 
    Reg(84, 2, Float32, lambda svc, id: svc.get_asset(id, 'requested_angle')),  # Rack requested angle 
    Reg(86, 2, Float32, lambda svc, id: svc.get_asset(id, 'motor_power_previous_hour')),  # Motor power prev hr 
    Reg(88, 2, Float32, lambda svc, id: svc.get_asset(id, 'average_angular_error_previous_hour')),  # Avg angular err prev hr 
    # WX
    Reg(90, 2, Float32, lambda svc, id: svc.get_asset(id, 'wind_speed')),  # Wind speed 
    Reg(92, 2, Float32, lambda svc, id: svc.get_asset(id, 'wind_direction')),  # Wind direction 
    Reg(94, 2, Float32, lambda svc, id: svc.get_asset(id, 'average_wind_speed')),  # Avg wind speed 
    Reg(96, 2, Float32, lambda svc, id: svc.get_asset(id, 'peak_wind_speed')),  # Peak wind speed 
    Reg(98, 2, Float32, lambda svc, id: svc.get_asset(id, 'snow_sensor_temperature')),  # Snow sensor temperature 
    Reg(100, 2, Float32, lambda svc, id: svc.get_asset(id, 'snow_sensor_distance')),  # Snow sensor distance 
    Reg(102, 2, Float32, lambda svc, id: svc.get_asset(id, 'snow_depth')),  # Snow depth 
]


class ModbusSvc(object):
    def __init__(self, site, asset_mgr, e_stop_mon, bridge_mon, asset_poller, gps_mon,
                 modem_mon, ntp_mon, sun_tracker, asset_commander, cloud_updater):
        self.site = site  # TerraSmartSite
        self.asset_mgr = asset_mgr  # AssetManager
        self.e_stop_mon = e_stop_mon # EmergencyStopHandler
        self.bridge = bridge_mon
        self.asset_poller = asset_poller
        self.nccb = asset_mgr.get_serial_asset()
        self.gps = gps_mon
        self.modem = modem_mon
        self.ntp = ntp_mon
        self.sun_tracker = sun_tracker
        self.asset_commander = asset_commander
        self.cloud_updater = cloud_updater
        self.t_last_asset_access = 0
        self.t_asset_reload_holdoff = 3  # (seconds) Asset list will be reloaded if access time interval exceeds this.
        self.cached_asset_keys = []

        self.srv = modbus_tcp_tornado.ModbusTcpServer(self, port=5552)

    def get_asset(self, id, field=None):
        """Return Asset instance for specified sequential integer ID.
           If field is provided, returns named field of Asset.
           Returns None if asset or field not found.
        """
        if id == -1:
            if self.site.has_weather:
                return self.nccb
            else:
                return None
        else:
            # Reload asset list if needed. Else return cached asset at index.
            # This allows time for MODBUS clients to traverse list without keys moving around.
            now = time.time()
            if (now - self.t_last_asset_access) > self.t_asset_reload_holdoff or (not self.cached_asset_keys):
                self.t_last_asset_access = now
                self.cached_asset_keys = self.asset_mgr.assets.keys()

            if 0 <= id < len(self.cached_asset_keys):
                k = self.cached_asset_keys[id]
                a = self.asset_mgr.assets.get(k)
                # DEBUG
                # if a:
                #     print "Asset id=%s, attr(%s)=%s" % (str(a.id), field, str(getattr(a, field, None)))
                return getattr(a, field, None) if field else a

            return None

    def get_alert(self, type):
        """Currently fetching alerts from CloudUpdater, since there doesn't appear to be
           a more native service managing them. As such there appears to be only one
           'active' alert per type being kept.
        """
        alerts = self.cloud_updater._active_alerts
        a = alerts.get(type, None)
        if a:
            return (a.when.ToSeconds(), a.type, a.message)
        else:
            return (0, 0xFFFF, '')  # Empty slot

    def read_span_regs(self, regs, id, addr, num):
        """Read that can span across multiple parameters.
        Args:
            regs(list): Sorted list of Reg objects
            id(int): Device ID of target (0=TC, Remote=1-25300)
            addr(int): Start address for read. Not required to be on parameter boundary.
            num(int): Number of 16-bit registers to read.

        Returns:
            list: 16-bit registers in span.
        """
        left = Reg(addr, 0, None, None)
        right = Reg(addr + num, 0, None, None)
        i1 = bisect.bisect_right(regs, left)
        if i1 > 0:
            i1 -= 1
        i2 = bisect.bisect_left(regs, right)

        # Invoke Reg items in range, decode and accumulate span of 16-bit data results.
        span_regs = []
        for i in range(i1, i2):
            reg = regs[i]
            native_val = None
            try:
                # Invoke getter
                native_val = reg.get_val(self, id)
                val = reg.codec(reg.size).get_reg16_seq(native_val)
            except:
                # Some values may not be populated with correct types at times, e.g. pre-initialization
                log.debug("Unfilled MODBUS register: addr=%d, native val=%s" % (reg.addr, native_val))
                # log.exception("MODBUS Stack Trace")   ### DEBUG
                val = [0] * reg.size   # Respond with zero-fill values

            span_regs.extend(val)

        # Trim if requested range starts on a mid-Reg boundary.
        ofs = addr - regs[i1].addr
        val = span_regs[ofs : ofs + num]

        # Pad with zeros if request exceeds available data range.
        if len(val) < num:
            val += [0] * (num - len(val))

        return val

    def tc_read(self, addr, num):
        log.debug("tc_read(%d, %d)" % (addr, num))
        return self.read_span_regs(tc_regs, 0, addr, num)

    def remote_read(self, id, addr, num):
        log.debug("remote_read(%d, %d, %d)" % (id, addr, num))
        return self.read_span_regs(remote_regs, id, addr, num)

    #---- ModbusClient interface ----
    def mb_read(self, unit_id, addr, num):
        """Return list of 16-bit register values from specified unit_id at start address.
        """
        addr -= 1  # We'll work with zero-based addresses
        log.debug("mb_read(%d, %d, %d)" % (unit_id, addr, num))

        # Interpret unit_id as "block" id
        if unit_id == 0:
            # TC itself. Check address to see if addressing a local WX.
            if addr < 25600:
                return self.tc_read(addr, num)
            else:
                # Flag local WX as device_id = -1
                return self.remote_read(-1, addr - 25600, num)
        else:
            # Ex: unit_id=1 addresses remote_id 0-99 based on 512 address count per device
            remote_id = ((unit_id - 1) * 100) + (addr // 512)
            remote_base_addr = addr % 512
            return self.remote_read(remote_id, remote_base_addr, num)
            # return self.remote_read(unit_id - 1, remote_base_addr, num)


    def mb_write(self, unit_id, addr, register_list):
        """
        Write list of 16-bit register values to specified unit_id at start_address.
        Return True if parameters are in range, False if not.
        """
        log.debug("mb_write(%d, %d, %s)" % (unit_id, addr, str(register_list)))
        # Writing not supported yet for NC
        return False

if __name__ == '__main__':
    from modbus_mocks import *
    logging.basicConfig(level=logging.DEBUG)
    log.debug("--- Starting test server ---")

    mb = ModbusSvc(my_site, asset_mgr, e_stop_mon, bridge_monitor, asset_poller, gps_mon, modem_mon,
                  ntp_monitor, sun_tracker, asset_commander, cloud_updater)

    tornado.ioloop.IOLoop.instance().start()
