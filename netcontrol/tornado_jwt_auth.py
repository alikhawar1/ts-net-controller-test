import functools

from jose import jwt, JWTError

from tornado.web import HTTPError


def jwt_authenticated(method):
    """
    Decorate methods with this to require that the user be logged in.
    """
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        jwt_cookie = self.get_cookie('access_token', None)
        if jwt_cookie:
            try:
                claims = jwt.decode(jwt_cookie, self.settings['jwt_secret'], algorithms=['HS256'])
                self.current_user = claims['user']
                return method(self, *args, **kwargs)
            except JWTError:
                raise HTTPError(403, "access_token is invalid")

        # TODO: What is the correct WWW-Authenticate value to return for cookies?
        self.set_header('WWW-Authenticate', 'Bearer token_type="JWT"')
        self.set_status(401, 'Unauthorized')

    return wrapper


def jwt_encode(claims, secret):
    return jwt.encode(claims, secret, algorithm='HS256')

def jwt_decode(claims, secret):
    return jwt.decode(claims, secret, algorithms='HS256')
