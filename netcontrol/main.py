import logging
import os
import optparse
import imp
import json
import ssl

from tornado.ioloop import IOLoop, PeriodicCallback

import graphene

from utils import SystemSettings
import terrasmart_site
import webserver
import event_log
import asset_manager
import GpsMonitor
import ModemMonitor
import NCCBMonitor
import SnapComm
import Ethernet
from asset_commander import AssetCommander
from AssetDiscovery import AssetFinder
from AssetUpdater import AssetUpdater
from AssetPoller import AssetPoller
from BridgeMonitor import BridgeMonitor
from AssetMover import AssetMover
from log_retriever import LogRetriever
from AssetBroadcaster import AssetBroadcaster
from SunPositionTracker import SunPositionTracker
from WeatherMonitor import WeatherMonitor
from EmergencyStopHandler import EmergencyStopHandler
from ts_common import tornado_paho
import graphql_handlers
from graphql_schema import ts_site
import ts_cloud_updater
import SystemManager
import NtpMonitor
import ts_cloud_listener
from persistent_state import PersistentState
from FormatHelpers import *
from ModbusSvc import ModbusSvc
from nc_update_manager import NetworkControllerUpdateManager

from pyfiria.gen import pass_lib
from controller_enums import EventType

from s3_client import BotoClient
from AssetUnicaster import AssetUnicaster
from RowPersistentService import RowPersistentService
from row_commander import RowCommander
from utils import config_parser

# Maybe later we will make this changeable (SLUI? CPUI?)
# For now, at least consolidating some separate hard-coded values
# is a step in the right direction
DEFAULT_MAX_HOPS = 4

USER_LOGGING_CONFIGFILENAME = 'logging_config.py'

log_system_startup_event = None
cloud_updater = None
asset_mgr = None
my_site = None


def sked_func(seconds, func):
    """
    Adapts the APy style callback scheduler function to Tornado's IOLoop style
    :param float seconds: Number of seconds to periodically call func
    :param callable func: A callable function that takes no arguments
    :return: PeriodicCallback
    """
    pcb = PeriodicCallback(func, seconds * 1000)
    pcb.start()
    return pcb


def config_logging():
    # Log format
    formatter = logging.Formatter('%(asctime)-15s %(levelname)-8s %(name)-8s %(message)s')

    # Console log handler
    chandler = logging.StreamHandler()
    chandler.setFormatter(formatter)

    # Configure root logger
    root_logger = logging.getLogger()
    root_logger.addHandler(chandler)

    # Setting the root logger or handler to a specific level (like WARN)
    # doesn't allow you to set different levels for individual components

    # Set the defaults to something fairly quiet
    logging.getLogger('ModemMonitor').setLevel(logging.WARN)
    logging.getLogger('GpsMonitor').setLevel(logging.WARN)
    logging.getLogger('snap').setLevel(logging.INFO)
    logging.getLogger('snaplib').setLevel(logging.INFO)
    logging.getLogger('NetworkControllerDecoders').setLevel(logging.INFO)
    logging.getLogger('AssetDiscovery').setLevel(logging.WARN)
    logging.getLogger('AssetPoller').setLevel(logging.WARN)
    logging.getLogger('tornado').setLevel(logging.WARN)
    logging.getLogger('WeatherMonitor').setLevel(logging.INFO)
    logging.getLogger('SunPositionTracker').setLevel(logging.INFO)
    logging.getLogger('AssetUpdater').setLevel(logging.WARN)
    logging.getLogger('SystemManager').setLevel(logging.WARN)
    logging.getLogger('NtpMonitor').setLevel(logging.WARN)
    logging.getLogger('RowCommander').setLevel(logging.WARN)

def get_uptime():
    try:
        f = open( "/proc/uptime" )
        contents = f.read().split()
        f.close()
        total_seconds = float(contents[0])
    except:
        total_seconds = 0
    return total_seconds

def log_system_startup():
    log_system_startup_event.stop()

    if asset_mgr is None:
        return

    if my_site is None:
        return

    nccb = asset_mgr.get_serial_asset()
    if nccb is None:
        return # must be running on a PC or something...

    nccb_uptime = format_piecewise_uptime(nccb.uptime_days, nccb.uptime_hours, nccb.uptime_minutes, nccb.uptime_seconds)

    linux_uptime = format_uptime_seconds(get_uptime())

    app_uptime = format_uptime_seconds(my_site.uptime)

    event_text = 'Network Controller version ' + terrasmart_site.NC_SOFTWARE_VERSION + ' startup - '
    event_text += 'NCCB Uptime: ' + nccb_uptime + ' '
    event_text += 'Linux Uptime: ' + linux_uptime + ' '
    event_text += 'Application Uptime: ' + app_uptime

    logging.getLogger('EventLog').info(event_text, extra=EventType.NETWORK_CONTROLLER._dict)
    cloud_updater.on_startup_update(terrasmart_site.NC_SOFTWARE_VERSION, nccb_uptime, linux_uptime, app_uptime)

def main():
    global log_system_startup_event, asset_mgr, my_site, cloud_updater

    parser = optparse.OptionParser()
    parser.add_option("-c", "--config_dir", dest="config_dir",
                      help="sets the config directory to DIR", metavar="DIR")
    parser.add_option("-n", "--nccb_port", dest="nccb_port", default=NCCBMonitor.PORT,
                      help="specifies the NCCB serial port")
    parser.add_option("-t", "--sc_port_type", dest="sc_port_type", default=SnapComm.DEFAULT_SERIAL_TYPE,
                      help="specifies the SNAPconnect bridge port type")
    parser.add_option("-p", "--sc_port", dest="sc_port", default=SnapComm.DEFAULT_SERIAL_PORT,
                      help="specifies the SNAPconnect bridge port")
    # Note that default ports are "high" to avoid priviledge issues
    # On the target, we use iptables to do port forwarding from
    # port 80 to port 8888 and from port 443 to port 8883
    parser.add_option("-s", "--tls_port", dest="tls_port", default=8883, type="int",
                      help="specifies the HTTPS/TLS port to listen on")
    parser.add_option("-w", "--http_port", dest="http_port", default=8888, type="int",
                      help="specifies the HTTP port to listen on")
    parser.add_option("-m", "--disable_mqtt", dest="mqtt_enabled", action="store_false", default=True,
                      help="specifies the HTTP port to listen on")

    (options, args) = parser.parse_args()
    if options.sc_port == "None":
        options.sc_port = None
    if options.sc_port_type == "None":
        options.sc_port_type = None
    if options.nccb_port == "None":
        options.nccb_port = None

    event_log_handler = event_log.MemoryEventLog(capacity=500)
    logging.getLogger().addHandler(event_log_handler)
    # Set the root handler to lowest level otherwise messages don't get to other handlers to filter on their own
    logging.getLogger().setLevel(logging.DEBUG)

    if options.config_dir:
        SystemSettings.SYSTEM_CONFIG_DIRECTORY = options.config_dir
    if not os.path.exists(SystemSettings.SYSTEM_CONFIG_DIRECTORY):
        os.makedirs(SystemSettings.SYSTEM_CONFIG_DIRECTORY)

    site_img_dir = os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.SITE_IMAGES_SUBDIRECTORY)
    if not os.path.exists(site_img_dir):
        os.mkdir(site_img_dir)

    script_img_dir = os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.SOFTWARE_IMAGES_SUBDIRECTORY)
    if not os.path.exists(script_img_dir):
        os.mkdir(script_img_dir)

    if not os.path.exists(os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.PASSWD_FILENAME)):
        logging.getLogger('EventLog').critical('Could not find existing passwd file, creating default', extra=EventType.ACCESS_CONTROL._dict)
        with open(os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, SystemSettings.PASSWD_FILENAME), 'w+') as f:
            json.dump({}, f)
            f.seek(0)
            #pass_lib.add_user(f, "admin", "terrasmart")
            pass_lib.add_user(f, "TTService2019", "$8Q6,E:9^y7zUW9y")

    if os.path.exists(os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, USER_LOGGING_CONFIGFILENAME)):
        imp.load_source('logging_config',
                        os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, USER_LOGGING_CONFIGFILENAME))

    # As of 01/10/2018 we persist a small amount of state across executions
    # by squirelling the data away in the NVRAM of the RTC (56 bytes TOTAL!)
    my_persistent_state = PersistentState()

    # System-wide concerns
    system_manager = SystemManager.SystemManager(schedule_func=sked_func)
    my_site = terrasmart_site.TerraSmartSite(site_img_dir, system_manager, my_persistent_state)
    # We had a little bit of "chicken and egg" syndrome up above.
    # Now that my_site has been created, tell system_manager about it.
    # (Several other pieces get handed to system_manager in a similar fashion)
    system_manager.set_site(my_site)

    # Interacting with the SNAP bridge
    snapcomm = SnapComm.SnapComm(serial_conn=options.sc_port_type, serial_port=options.sc_port)
    bridge_monitor = BridgeMonitor(snapcomm.sc,
                                   sked_func)  # could also use Scheduler.schedule_func,

    # Interacting with USB Ethernet
    ethernet = Ethernet.Ethernet()

    # Interacting with our Cell Modem
    gps_mon = GpsMonitor.GpsMonitor(my_site)

    sun_tracker = SunPositionTracker(gps_mon, my_site)
    system_manager.set_sun_tracker(sun_tracker)

    parser = config_parser.ConfigFileParser(os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, "s3-options.conf"))
    config = parser.get_config()
    if config is None:
        raise Exception("Error: Config file needed to connect cloud!")

    # Interacting with the cloud
    mqtt_client = tornado_paho.TornadoPahoMqttClient(mqtt_broker='a2dlentyd00pdf.iot.us-east-2.amazonaws.com',
                                                     mqtt_port=8883,
                                                     ssl_options={
                                                         "cert_reqs": ssl.CERT_REQUIRED,
                                                         "certfile": os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY,
                                                                                  "my_aws_iot.pem"),
                                                         "keyfile": os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY,
                                                                                 "private.key"),
                                                         "ca_certs": os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY,
                                                                                  'VeriSign-Class-3-Public-Primary-Certification-Authority-G5.pem')
                                                     })
    cloud_updater = ts_cloud_updater.TerraSmartCloudUpdater(mqtt_client,
                                                            lambda: bridge_monitor.raw_bridge_address,
                                                            lambda *args, **kwargs: sun_tracker.get_sun_rise_set(*args, **kwargs),
                                                            my_site,
                                                            event_log_handler,
                                                            config=config,
                                                            enabled=options.mqtt_enabled)
    gps_mon.on_gps_change = cloud_updater.on_gps_change
    gps_mon.on_clock_questionable = cloud_updater.on_clock_questionable
    gps_mon.on_send_gps_update = cloud_updater.on_send_gps_update
    gps_mon.send_gps_update() # Send update once on startup.
    
    modem_mon = ModemMonitor.ModemMonitor(on_state_change=cloud_updater.on_cell_state_change,
                                          on_update_cell_stats=cloud_updater.on_update_cell_stats, 
                                          on_daily_update_cell_stats=cloud_updater.on_daily_update_cell_stats,
                                          modem_power_cycle=system_manager.request_modem_power_cycle,
                                          connected_to_cloud = lambda: mqtt_client.connected)
    
    system_manager.set_cloud_updater(cloud_updater)

    bridge_monitor.on_bridge_update = cloud_updater.on_nc_bridge_update

    # See how NTP is doing
    ntp_monitor = NtpMonitor.NtpMonitor(schedule_func=sked_func, on_ntp_sync_change=cloud_updater.on_ntp_sync_change)

    # Maintains an in-memory database of all Assets
    asset_mgr = asset_manager.AssetManager()
    system_manager.set_asset_manager(asset_mgr)

    # Polls the Network Controller Companion Board over a USB CDC interface
    nccb_mon = NCCBMonitor.NCCBMonitor(schedule_func=sked_func,
                                       asset=asset_mgr.get_serial_asset(),
                                       port=options.nccb_port)
    system_manager.set_nccb_monitor(nccb_mon)

    # Discovers unprovisioned (but commissioned) Assets
    asset_finder = AssetFinder(asset_mgr,
                               snapcomm.sc,
                               sked_func,  # could also use Scheduler.schedule_func,
                               DEFAULT_MAX_HOPS,
                               1000)

    # Gathers data from wireless Assets (see also nccb_mon)
    asset_poller = AssetPoller(asset_mgr,
                               snapcomm.sc,
                               sked_func,  # could also use Scheduler.schedule_func,
                               DEFAULT_MAX_HOPS,
                               1000,
                               on_asset_changed=cloud_updater.on_asset_changed,
                               on_assets_reporting_change=cloud_updater.on_assets_reporting_change,
                               on_report_default_status=cloud_updater.on_report_default_status,
                               on_asset_reboot=cloud_updater.on_asset_reboot,
                               on_asset_preset_change=cloud_updater.on_asset_preset_change)

    asset_broadcaster = AssetBroadcaster(snapcomm.sc, nccb_mon, sked_func)

    asset_unicaster = AssetUnicaster(snapcomm.sc)

    weather_monitor = WeatherMonitor(asset_mgr, my_persistent_state, 
                                     sked_func,
                                     my_site, 
                                     cloud_updater.on_weather_stow,
                                     cloud_updater.on_config_update,
                                     cloud_updater.send_increase_reporting,
                                     cloud_updater.send_weather_hist)

    e_stop_mon = EmergencyStopHandler(sked_func,
                                      asset_mgr.get_serial_asset(),
                                      on_estop_change=cloud_updater.on_estop_change)
    # Maybe we should call this PanelCommander?
    asset_commander = AssetCommander(sun_tracker=sun_tracker,
                                     weather_mon=weather_monitor,
                                     estop_handler=e_stop_mon,
                                     asset_broadcaster=asset_broadcaster,
                                     my_site=my_site,
                                     persistent_state = my_persistent_state,
                                     on_panel_state_change=cloud_updater.on_panel_state_change)

    # Updates scripts and firmware on Assets
    asset_updater = AssetUpdater(snapcomm.sc,
                                 sked_func,
                                 bridge_monitor,
                                 asset_mgr,
                                 script_img_dir,
                                 SystemSettings.SYSTEM_CONFIG_DIRECTORY)

    # Moves all Assets gracefully from one "network" (set of config settings) to another
    asset_mover = AssetMover(snapcomm.sc,
                             sked_func,
                             asset_mgr,
                             bridge_monitor,
                             system_manager)

    # Retrieves Log Data from Assets
    asset_log_retriever = LogRetriever(snapcomm.sc,
                                       nccb_mon,
                                       sked_func,
                                       lambda: bridge_monitor.raw_bridge_address,
                                       DEFAULT_MAX_HOPS)

    # Hook in MODBUS/TCP server
    ModbusSvc(my_site, asset_mgr, e_stop_mon, bridge_monitor, asset_poller, gps_mon, modem_mon,
              ntp_monitor, sun_tracker, asset_commander, cloud_updater)
    
    # Interacting with the cloud to fetch active commands
    s3_client = BotoClient(config)

    # As of 04/29/2020, we persist rows under manual control along with previous state
    # and preset angle to have better reporting.
    row_persistent_service = RowPersistentService(my_persistent_state, s3_client, sked_func)

    # Commands individual row-boxes.
    row_commander = RowCommander(asset_unicaster,
                                 row_persistent_service,
                                 asset_mgr, 
                                 asset_commander,
                                 cloud_updater.on_state_change,
                                 sked_func)

    # Listen for MQTT commands from the cloud
    ts_cloud_listener.TerraSmartCloudListener(mqtt_client, https_port=options.tls_port, http_port=options.http_port,
                                              site=my_site, asset_updater=asset_updater, asset_mgr=asset_mgr,
                                              asset_broadcaster=asset_broadcaster,
                                              persistent_service=row_persistent_service,
                                              cloud_updater=cloud_updater,
                                              config=config)
    if options.mqtt_enabled:
        mqtt_client.connect()

    schema = graphene.Schema(query=ts_site.TCSite,
                             mutation=ts_site.TSSiteMutations)
    graphql_handlers.GraphQLHandler(schema, my_site, event_log_handler, asset_mgr,
                                    gps_mon, modem_mon, bridge_monitor, asset_poller,
                                    weather_monitor, e_stop_mon, asset_commander, sun_tracker,
                                    mqtt_client, cloud_updater, ntp_monitor, system_manager,
                                    asset_mover, asset_log_retriever, asset_updater,
                                    asset_broadcaster, ethernet)

    main_port = options.http_port
    ssl_options = None
    if options.tls_port > 0:
        ssl_options = {
            "certfile": os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, "fullchain.pem"),
            "keyfile": os.path.join(SystemSettings.SYSTEM_CONFIG_DIRECTORY, "private.pem"),
        }
        webserver.make_redirect_app(options.tls_port).listen(options.http_port)
        main_port = options.tls_port

    tornado_app = webserver.make_app(schema, site_img_dir, my_site, ssl_options=ssl_options)
    tornado_app.listen(main_port)

    NC_update_manager = NetworkControllerUpdateManager(schedule_func=sked_func)

    log_system_startup_event = sked_func(5.0, log_system_startup)

    IOLoop.current().start()


if __name__ == '__main__':
    config_logging()
    main()
