import logging
from controller_enums import EventType, CommandStatus
from netcontrol.command_handler import CommandHandler, CommandedRowState

log = logging.getLogger(__name__)
event_log = logging.getLogger('EventLog')

class RowCommander(object):

    def __init__(self, unicaster, persistent_service, asset_manager, asset_commander, on_state_change, schedule_func):
        """
        In charge of executing commands on the row boxes individually
        :param unicaster: Mesh unicaster to send commands to individual asset
        :param persistent_service: Manages manually commanded rows
        :param schedule_func - used for timing
        :param asset_manager: holder of ALL the Asset objects

        """
        if persistent_service is None:
            raise Exception("Error: RowCommander requires RowPersistentService object!")
        else:
            self.persistent_service = persistent_service

        self._schedule_func = schedule_func
        self.command_handler = CommandHandler(unicaster, asset_manager, persistent_service, asset_commander, on_state_change)
        
        self.pcb = self._schedule_func(1.0, self._update_state)

    
    def start_update_state(self):
        if self.pcb:
            self.pcb.stop()
            self.pcb = self._schedule_func(2.0, self._update_state)

    def _update_state(self):
        """ 
        Visits manually-commanded-row list periodically till commands get executed successfully,
        and updates the state of each row as being commanded by the user.
        """
        command_list = self.persistent_service.get_list()
        if len(command_list) == 0:
            return
        self.pcb.stop()
        for (asset_id, cmd) in command_list.iteritems():   
            last_requested_time = cmd['last_requested_time']       
            snap_addr           = asset_id.decode('hex')
            last_preset         = cmd['last_preset']
            last_state          = cmd['last_state']
            cmd_state           = cmd['cmd_state']
            user                = cmd['username']
            status              = cmd['status']
            arg                 = cmd['arg']

            if status == CommandStatus.NEW:
                self.command_handler.on_new_command(asset_id, user, cmd_state, arg)

                if self.command_handler.is_asset_offline(asset_id):
                    self.command_handler.on_asset_offline(asset_id, cmd_state)
                
                elif self.command_handler.local_estop_pressed(asset_id):
                    self.command_handler.on_local_estop(asset_id, cmd_state)

                elif self.command_handler.under_hhc(asset_id):
                    self.command_handler.on_hhc(asset_id, cmd_state)

                elif self.command_handler.nccb_estop_pressed(asset_id):
                    self.command_handler.on_nccb_estop(asset_id, cmd_state)

            elif cmd_state == CommandedRowState.PANEL_CMD_V_ESTOP:
                self.command_handler.on_stop_command(status, asset_id, cmd_state, last_state, last_preset, last_requested_time)
            
            elif cmd_state == CommandedRowState.PANEL_CMD_MOVE_TO_ABS:
                self.command_handler.on_move_to_abs(status, asset_id, cmd_state, last_state, last_preset, arg, last_requested_time)
    
            elif cmd_state == CommandedRowState.PANEL_CMD_MOVE_TO_PRESET:
                self.command_handler.on_move_to_preset(status, asset_id, cmd_state, last_state, last_preset, arg, last_requested_time)

            elif cmd_state == CommandedRowState.PANEL_CMD_NONE:
                self.command_handler.on_resume_normal(status, asset_id, cmd_state, last_state, last_preset, last_requested_time)

        self.pcb.start()

             
if __name__ == '__main__':
    import os
    import logging
    from enum import Enum
    from persistent_state import PersistentState
    from controller_enums import EventType, CommandedRowState, TrackerPresets

    logging.basicConfig(level=logging.DEBUG)
    
