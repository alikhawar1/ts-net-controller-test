"""
NetworkControllerDecoders.py - a repurposing of the
original (debug only) code from the Radio Module.
The original Decoders.py knew how to decode "blobs",
but all it did was assign their contents to individual
global variables and optionally "print" (sent to Portal)
some of them. This variant of that original code stores
the decoded values into a Asset class (a data object)
"""


import datetime
import logging
import struct
from controller_enums import EventType

import Asset  # someplace to put the data


log = logging.getLogger(__name__)


def debug(msg):
    log.debug(msg)
    # print msg


def decode_byte(bytes):
    return ord(bytes[0])


def chop_byte(s):
    return s[1:]


def decode_word(bytes):
    temp = (ord(bytes[0]) << 8) + ord(bytes[1])
    # The above was sufficient for SNAPpy, because ints are only 16-bits
    # Here we must do extra work to handle negative numbers
    temp &= 0xFFFF
    if (temp & 0x8000) == 0x8000:
        temp = temp - 65536
    return temp


def decode_word_unsigned(bytes):
    temp = (ord(bytes[0]) << 8) + ord(bytes[1])
    return temp


def chop_word(s):
    return s[2:]


def decode_string(bytes):
    length = decode_byte(bytes)

    return bytes[1:length + 1]


def chop_string(bytes, string):
    return bytes[1 + len(string):]


def decode_fixed_length_string(bytes, length):

    return bytes[0:length]


def chop_fixed_length_string(bytes, length):
    return bytes[length:]


N_VERSION = 2
S_VERSION = 2
T_VERSION = 1
U_VERSION = 0
V_VERSION = 0
W_VERSION = 0
X_VERSION = 0
Y_VERSION = 0

SNAP_FIRMWARE_BASE_REV = 281  # We report SNAP versions as an offset from this so they can fit in one byte
log_once = True

def decode_version_number_to_string(number):
    version_string = ''
    if number > 100:
        version_string += str(number / 100)
        version_string += '.'
        number %= 100
    version_string += str(number / 10)
    version_string += '.'
    number %= 10
    version_string += str(number)
    return version_string

def decode_blob_n(dest, n):
    if len(n) < 2:
        return
    if n[0] != 'n':
        return
    version = ord(n[1])
    n = n[2:]

    # Blob "n" version 0 data is always present
    # Brute force decoding our way across the blob
    # External Input 2 Voltage (16-bit) in tenths of a Volt
    if len(n) < 2:
        return
    dest.external_input_2_voltage = round(decode_word(n) * 0.1, 1)
    n = chop_word(n)

    # External Input 2 Panel Current (16-bit)
    if len(n) < 2:
        return
    dest.external_input_2_current = round(decode_word(n) * 0.01, 2)
    n = chop_word(n)

    # external_input_2_power_current_hour (16-bit unsigned)
    if len(n) < 2:
        return
    dest.external_input_2_power_current_hour = round(decode_word_unsigned(n) * 0.01, 2)
    n = chop_word(n)

    # external_input_2_power_previous_hour (16-bit unsigned)
    if len(n) < 2:
        return
    dest.external_input_2_power_previous_hour = round(decode_word_unsigned(n) * 0.01, 2)
    n = chop_word(n)

    # external_input_2_power_current_day (16-bit unsigned)
    if len(n) < 2:
        return
    dest.external_input_2_power_current_day = round(decode_word_unsigned(n) * 0.01, 2)
    n = chop_word(n)

    # external_input_2_power_previous_day (16-bit unsigned)
    if len(n) < 2:
        return
    dest.external_input_2_power_previous_day = round(decode_word_unsigned(n) * 0.01, 2)
    n = chop_word(n)

    # End of Blob "n" version 0 data, start of version 1 data
    if version >= 1:
        if len(n) < 2:
            return
        dest.misc_status_bits = decode_word_unsigned(n)
        n = chop_word(n)

        # End of Blob "n" version 1 data

    # End of Blob "n" version 1 data, start of version 2 data
    if version >= 2:
        # Motor Current at the end of the previous movement (16-bit)
        if len(n) < 2:
            return
        dest.ending_motor_current = round(decode_word(n) * 0.01, 2)
        n = chop_word(n)

        # End of Blob "n" version 2 data

    if n != '':
        debug(str(len(n)) + " bytes of BLOB_N unaccounted for")


def decode_blob_r(dest, r):
    if len(r) < 2:
        return
    if r[0] != 'r':
        return
    blob_version = ord(r[1])
    r = r[2:]

    # Brute force decoding our way across the blob
    # Link Quality
    if len(r) < 1:
        return
    dest.radio_link_quality = decode_byte(r)
    r = chop_byte(r)

    # Mesh Depth
    if len(r) < 1:
        return
    dest.radio_mesh_depth = decode_byte(r)
    r = chop_byte(r)

    # Channel
    if len(r) < 1:
        return
    dest.radio_channel = decode_byte(r)
    r = chop_byte(r)

    # Network ID
    if len(r) < 2:
        return
    dest.radio_network_id = decode_word(r)
    r = chop_word(r)

    # MAC Address
    if len(r) < 8:
        return
    dest.mac_addr = decode_fixed_length_string(r, 8)
    r = chop_fixed_length_string(r, 8)

    # Script Version
    if len(r) < 1:
        return
    temp = decode_byte(r)
    dest.radio_script_version = temp
    dest.radio_script_version_string = decode_version_number_to_string(temp)
    r = chop_byte(r)

    # Script CRC
    if len(r) < 2:
        return
    dest.radio_script_crc = decode_word(r)
    r = chop_word(r)

    # Radio Firmware Version
    if len(r) < 1:
        return
    temp = decode_byte(r)
    dest.radio_firmware = temp
    dest.radio_firmware_string = decode_version_number_to_string(temp + SNAP_FIRMWARE_BASE_REV)
    r = chop_byte(r)

    #
    # End of version 0 data, start of (optional) version 1 data
    #
    if blob_version >= 1:
        # Radio Multitcast Forwarding settings
        if len(r) < 2:
            return
        dest.radio_forwarding_mask = decode_word(r)
        r = chop_word(r)
        # For convenience - go ahead and determine if the radio is acting as a "Terrasmart Data Group(s) Repeater"
        if (dest.radio_forwarding_mask & 0xFF00) != 0x0000:
            dest.is_a_repeater = True
        else:
            dest.is_a_repeater = False
    #
    # End of version 1 data
    #

    if r != '':
        debug(str(len(r)) + " bytes of BLOB_R unaccounted for")


def convert_angular_error(raw):
    # Convert from "integer tenths of a degree"" to "floating point degrees"
    return round(raw * 0.1, 1)

def decode_blob_s(dest, s):
    if len(s) < 2:
        return
    if s[0] != 's':
        return
    got_version = ord(s[1])
    if got_version >= 0:
        s = s[2:]

        # Brute force decoding our way across the blob
        # Status bits
        if len(s) < 1:
            return
        dest.status_bits = decode_byte(s)
        s = chop_byte(s)

        # Uptime Days (16-bit)
        if len(s) < 2:
            return
        dest.uptime_days = decode_word(s)
        s = chop_word(s)

        # Uptime Hours
        if len(s) < 1:
            return
        dest.uptime_hours = decode_byte(s)
        s = chop_byte(s)

        # Uptime Minutes
        if len(s) < 1:
            return
        dest.uptime_minutes = decode_byte(s)
        s = chop_byte(s)

        # Uptime Seconds
        if len(s) < 1:
            return
        dest.uptime_seconds = decode_byte(s)
        s = chop_byte(s)

        # Downtime Days (16-bit)
        if len(s) < 2:
            return
        dest.downtime_days = decode_word(s)
        s = chop_word(s)

        # Downtime Hours
        if len(s) < 1:
            return
        dest.downtime_hours = decode_byte(s)
        s = chop_byte(s)

        # Downtime Minutes
        if len(s) < 1:
            return
        dest.downtime_minutes = decode_byte(s)
        s = chop_byte(s)

        # Downtime Seconds
        if len(s) < 1:
            return
        dest.downtime_seconds = decode_byte(s)
        s = chop_byte(s)

        # Offline Minutes (16-bit)
        if len(s) < 2:
            return
        dest.offline_minutes = decode_word(s)
        s = chop_word(s)

        # FCS control status
        if dest.offline_minutes > 0:
            dest.commissioning_mode = True
        else:
            dest.commissioning_mode = False

        # Current Panel Angle (16-bit)
        if len(s) < 2:
            return
        dest.current_angle = round(decode_word(s) * 0.1, 1)
        s = chop_word(s)

        # Requested Panel Angle (16-bit)
        if len(s) < 2:
            return
        dest.requested_angle = round(decode_word(s) * 0.1, 1)
        s = chop_word(s)

        # Battery Voltage (16-bit) in tenths of a Volt
        if len(s) < 2:
            return
        dest.battery_voltage = round(decode_word(s) * 0.1, 1)
        s = chop_word(s)

        # Solar Voltage (16-bit) in tenths of a Volt
        if len(s) < 2:
            return
        dest.solar_voltage = round(decode_word(s) * 0.1, 1)
        s = chop_word(s)

        # Charger Current (16-bit) in hundredths of Amp - 0 when not charging
        if len(s) < 2:
            return
        dest.charger_current = round(decode_word(s) * 0.01, 2)
        s = chop_word(s)

        # Unit Temperature (16-bit) in tenths of degree F
        if len(s) < 2:
            return
        dest.unit_temperature = round(decode_word(s) * 0.1, 1)
        s = chop_word(s)

        # Battery Temperature (16-bit) in tenths of degree F
        if len(s) < 2:
            return
        dest.battery_temperature = round(decode_word(s) * 0.1, 1)
        s = chop_word(s)

        # Battery Current (16-bit) in hundredths of Amp - Output of battery only!
        # If battery is CHARGING, this field is 0
        if len(s) < 2:
            return
        dest.battery_current = round(decode_word(s) * 0.01, 2)
        s = chop_word(s)

        # Solar Panel Current (16-bit)
        if len(s) < 2:
            return
        dest.solar_current = round(decode_word(s) * 0.01, 2)
        s = chop_word(s)

        # Charger Voltage (16-bit)
        if len(s) < 2:
            return
        dest.charger_voltage = round(decode_word(s) * 0.1, 1)
        s = chop_word(s)

        # Motor Current (16-bit)
        if len(s) < 2:
            return
        dest.motor_current = round(decode_word(s) * 0.01, 2)
        s = chop_word(s)

        # Heater Temperature (16-bit)
        if len(s) < 2:
            return
        dest.heater_temperature = round(decode_word(s) * 0.1, 1)
        s = chop_word(s)

        # Battery Percent Charged (8-bit)
        if len(s) < 1:
            return
        dest.battery_charged = decode_byte(s)
        s = chop_byte(s)

        # Battery Percent Health (8-bit)
        if len(s) < 1:
            return
        dest.battery_health = decode_byte(s)
        s = chop_byte(s)

        # Tracking Status (bits)
        if len(s) < 1:
            return
        dest.tracking_status_bits = decode_byte(s)
        s = chop_byte(s)

        # solar_power_current_hour (16-bit unsigned)
        if len(s) < 2:
            return
        dest.solar_power_current_hour = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # solar_power_previous_hour (16-bit unsigned)
        if len(s) < 2:
            return
        dest.solar_power_previous_hour = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # solar_power_current_day (16-bit unsigned)
        if len(s) < 2:
            return
        dest.solar_power_current_day = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # solar_power_previous_day (16-bit unsigned)
        if len(s) < 2:
            return
        dest.solar_power_previous_day = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # battery_power_current_hour (16-bit unsigned)
        if len(s) < 2:
            return
        dest.battery_power_current_hour = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # battery_power_previous_hour (16-bit unsigned)
        if len(s) < 2:
            return
        dest.battery_power_previous_hour = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # battery_power_current_day (16-bit unsigned)
        if len(s) < 2:
            return
        dest.battery_power_current_day = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # battery_power_previous_day (16-bit unsigned)
        if len(s) < 2:
            return
        dest.battery_power_previous_day = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # charger_power_current_hour (16-bit unsigned)
        if len(s) < 2:
            return
        dest.charger_power_current_hour = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # charger_power_previous_hour (16-bit unsigned)
        if len(s) < 2:
            return
        dest.charger_power_previous_hour = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # charger_power_current_day (16-bit unsigned)
        if len(s) < 2:
            return
        dest.charger_power_current_day = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # charger_power_previous_day (16-bit unsigned)
        if len(s) < 2:
            return
        dest.charger_power_previous_day = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # motor_power_current_hour (16-bit unsigned)
        if len(s) < 2:
            return
        dest.motor_power_current_hour = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # motor_power_previous_hour (16-bit unsigned)
        if len(s) < 2:
            return
        dest.motor_power_previous_hour = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # motor_power_current_day (16-bit unsigned)
        if len(s) < 2:
            return
        dest.motor_power_current_day = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # motor_power_previous_day (16-bit unsigned)
        if len(s) < 2:
            return
        dest.motor_power_previous_day = round(decode_word_unsigned(s) * 0.01, 2)
        s = chop_word(s)

        # average_angular_error_current_hour (16-bit)
        if len(s) < 2:
            return
        dest.average_angular_error_current_hour = convert_angular_error(decode_word(s))
        s = chop_word(s)

        # average_angular_error_previous_hour (16-bit)
        if len(s) < 2:
            return
        dest.average_angular_error_previous_hour = convert_angular_error(decode_word(s))
        s = chop_word(s)

        # average_angular_error_current_day (16-bit)
        if len(s) < 2:
            return
        dest.average_angular_error_current_day = convert_angular_error(decode_word(s))
        s = chop_word(s)

        # average_angular_error_previous_day (16-bit)
        if len(s) < 2:
            return
        dest.average_angular_error_previous_day = convert_angular_error(decode_word(s))
        s = chop_word(s)

        # hours_since_accumulator_reset (8-bit)
        if len(s) < 1:
            return
        dest.hours_since_accumulators_reset = decode_byte(s)
        s = chop_byte(s)

        if len(s) < 1:
            return

    # Fields that are in BLOB_S version 1 and above
    if got_version >= 1:
        # panel_index (8-bit)
        dest.panel_index = decode_byte(s)
        s = chop_byte(s)

        # command_state (8-bit)
        if len(s) < 1:
            return
        dest.panel_command_state = decode_byte(s)
        s = chop_byte(s)

    # Fields that are in BLOB_S version 2 and above
    if got_version >= 2:
        # Peak Motor Inrush Current (16-bit)
        if len(s) < 2:
            return
        dest.peak_motor_inrush_current = round(decode_word(s) * 0.01, 2)
        s = chop_word(s)

        # Peak Motor Current (16-bit)
        if len(s) < 2:
            return
        dest.peak_motor_current = round(decode_word(s) * 0.01, 2)
        s = chop_word(s)

        # Average Motor Current (16-bit)
        if len(s) < 2:
            return
        dest.average_motor_current = round(decode_word(s) * 0.01, 2)
        s = chop_word(s)

        # (currently spare) status byte (8-bit)
        if len(s) < 1:
            return
        dest.more_status_bits = decode_byte(s) # as of 10/25/2020, bit meanings still to be assigned
        s = chop_byte(s)

    global log_once
    if got_version > S_VERSION and log_once:
        logging.getLogger('EventLog').log(logging.INFO, "From blob_s version number > 2 is seen ", extra=EventType.INDIVIDUAL_ASSET._dict)
        log_once = False

    if s != '':
        debug(str(len(s)) + " bytes of BLOB_S unaccounted for")


def decode_blob_t(dest, t):
    if len(t) < 2:
        return
    if t[0] != 't':
        return
    version = ord(t[1])
    t = t[2:]

    # Blob "t" version 0 data is always present
    # Brute force decoding our way across the blob
    # Horizontal Calibration Angle (16-bit)
    if len(t) < 2:
        return
    dest.panel_horizontal_cal_angle = decode_word(t)
    t = chop_word(t)

    # Minimum Calibration Angle (16-bit)
    if len(t) < 2:
        return
    dest.panel_min_cal_angle = decode_word(t)
    t = chop_word(t)

    # Maximum Calibration Angle (16-bit)
    if len(t) < 2:
        return
    dest.panel_max_cal_angle = decode_word(t)
    t = chop_word(t)

    # Config Label (variable length string)
    if len(t) < 1:
        return
    dest.config_label = decode_string(t)
    t = chop_string(t, dest.config_label)

    # Timestamp (variable length string)
    if len(t) < 1:
        return
    temp = decode_string(t)
    try:
        dest.config_timestamp = struct.unpack('>d', temp)[0]
    except struct.error:
        # It is common that a BRAND NEW box could be talking to us BEFORE
        # it has ever been configured using an FCS. In this scenario, it
        # WON'T HAVE a config_timestamp yet. So, commenting out this log.error().
        #log.error("Unable to parse config_timestamp")
        # Re-enable the above message IF you need to debug something related
        # AND you know that ALL of your Assets have in fact been configured.
        dest.config_timestamp = 0
    t = chop_string(t, temp)

    # Site Label (variable length string)
    if len(t) < 1:
        return
    dest.site_name = decode_string(t)
    t = chop_string(t, dest.site_name)

    # Location in lat/lon (variable length string)
    if len(t) < 1:
        return
    temp = decode_string(t)
    t = chop_string(t, temp)

    try:
        dest.location_lat, dest.location_lng = struct.unpack('>dd', temp)
    except struct.error, e:
        pass

    # Location in text form (variable length string)
    if len(t) < 1:
        return
    dest.location_text = decode_string(t)
    t = chop_string(t, dest.location_text)

    #
    # Backtracking Config (part 1)
    #
    # Panel Array Width (16-bit)
    if len(t) < 2:
        return
    dest.segments[0].panel_array_width = decode_word(t)
    t = chop_word(t)

    # Spacing to East (16-bit)
    if len(t) < 2:
        return
    dest.segments[0].spacing_to_east = decode_word(t)
    t = chop_word(t)

    # Spacing to West (16-bit)
    if len(t) < 2:
        return
    dest.segments[0].spacing_to_west = decode_word(t)
    t = chop_word(t)

    # Delta Height to East (16-bit)
    if len(t) < 2:
        return
    dest.segments[0].delta_height_east = decode_word(t)
    t = chop_word(t)

    # Delta Height to West (16-bit)
    if len(t) < 2:
        return
    dest.segments[0].delta_height_west = decode_word(t)
    t = chop_word(t)

    # Config Flags (16-bit)
    if len(t) < 2:
        return
    dest.config_flags = decode_word(t)
    t = chop_word(t)

    # Wind Direction Offset (16-bit)
    if len(t) < 2:
        return
    # This comes in as tenths of a degree, converting to degrees
    dest.wind_dir_offset = round(decode_word(t) * 0.1, 1)
    t = chop_word(t)

    # Snow Sensor Height (16-bit)
    if len(t) < 2:
        return
    # This comes in as millimeters, converting to meters
    dest.snow_sensor_height = round(decode_word(t) * 0.001, 3)
    t = chop_word(t)

    #
    # Backtracking Config (part 2)
    #
    # Panel Array Width (16-bit)
    if len(t) < 2:
        return
    dest.segments[1].panel_array_width = decode_word(t)
    t = chop_word(t)

    # Spacing to East (16-bit)
    if len(t) < 2:
        return
    dest.segments[1].spacing_to_east = decode_word(t)
    t = chop_word(t)

    # Spacing to West (16-bit)
    if len(t) < 2:
        return
    dest.segments[1].spacing_to_west = decode_word(t)
    t = chop_word(t)

    # Delta Height to East (16-bit)
    if len(t) < 2:
        return
    dest.segments[1].delta_height_east = decode_word(t)
    t = chop_word(t)

    # Delta Height to West (16-bit)
    if len(t) < 2:
        return
    dest.segments[1].delta_height_west = decode_word(t)
    t = chop_word(t)
    
    # End of Blob "t" version 0 data, start of version 1 data 
    if version >= 1:
        # Battery Capacity (16-bit)
        if len(t) < 2:
            return
        dest.battery_capacity = decode_word(t)
        t = chop_word(t)

    if t != '':
        debug(str(len(t)) + " bytes of BLOB_T unaccounted for")


def decode_blob_u(dest, u):
    if len(u) < 2:
        return
    if u[0] != 'u':
        return
    if u[1] != chr(U_VERSION):
        return
    u = u[2:]

    # Brute force decoding our way across the blob

    # Device, Model are combined into a single byte
    if len(u) < 1:
        return
    dest.model_device = decode_byte(u)
    u = chop_byte(u)

    temp = dest.model_device & 0x03
    if temp == 0:
        dest.device = 'Tracker'
        dest.model = 'TTC-'
    elif temp == 1:
        dest.device = 'Companion'
        dest.model = 'NCCB-'
    elif temp == 2:
        dest.device = 'Weather'
        dest.model = 'WX-'
    else:
        dest.device = 'Unknown'
        dest.model = '???-'

    model_offset = ((dest.model_device >> 4) & 0x0F)
    dest.model += chr(ord('1') + model_offset)

    # (does it have a motor for moving a rack of solar panels?)
    if dest.model_device & 0x04:
        dest.has_tracker_hardware = True
    else:
        dest.has_tracker_hardware = False

    # (Weather sensors option)
    if dest.model_device & 0x08:
        dest.has_weather_sensor = True
        if dest.model[0:3] != "WX-":
            dest.model += "+WX"
    else:
        dest.has_weather_sensor = False

    # Hardware Revision
    if len(u) < 1:
        return
    dest.hardware_rev = decode_byte(u)
    u = chop_byte(u)

    # firmware Revision
    if len(u) < 1:
        return
    temp = decode_byte(u)
    dest.firmware_rev = temp
    dest.firmware_rev_string = decode_version_number_to_string(temp)
    u = chop_byte(u)

    if u != '':
        debug(str(len(u)) + " bytes of BLOB_U unaccounted for")

    debug("Device=" + dest.device +
          " Model=" + dest.model +
          " HW Rev=" + str(dest.hardware_rev) +
          " FW Rev=" + str(dest.firmware_rev))


def decode_blob_v(dest, v):
    if len(v) < (2 + 16):  # 2 byte header, 2 bytes per sensor
        return
    if v[0] != 'v':
        return
    if v[1] != chr(V_VERSION):
        return
    v = v[2:]

    # Brute force decoding our way across the blob
    dest.sensor_modes = []
    dest.sensor_flags = []
    index = 0
    while index < 8:
        dest.sensors[index].sensor_adc_mode = decode_byte(v)
        v = chop_byte(v)
        dest.sensors[index].flags = decode_byte(v)
        v = chop_byte(v)
        index += 1

    if v != '':
        debug(str(len(v)) + " bytes of BLOB_V unaccounted for")

    debug("Sensor Modes=" + str(dest.sensor_modes) +
          " Sensor Flags=" + str(dest.sensor_flags))


def decode_blob_w(dest, w):
    if len(w) < 2:
        return
    if w[0] != 'w':
        return
    if w[1] != chr(W_VERSION):
        return
    w = w[2:]

    # Brute force decoding our way across the blob
    for i in xrange(8):
        if len(w) < 2:
            return
        dest.sensors[i].sensor_adc_value = decode_word(w)
        w = chop_word(w)

    # Digital Inputs
    if len(w) < 1:
        return
    sensor_dins = decode_byte(w)
    for i in xrange(8):
        bit = 1 << i + 1
        dest.sensors[i].sensor_digital_input_value = bool(sensor_dins & bit)
    w = chop_byte(w)

    if w != '':
        debug(str(len(w)) + " bytes of BLOB_W unaccounted for")

    # debug("Sensor 0="+str(sensor_data_0) + " Sensor 1="+str(sensor_data_1) + " Sensor 2="+str(sensor_data_2) + " Sensor 3="+str(sensor_data_3))
    # debug("Sensor 4="+str(sensor_data_4) + " Sensor 5="+str(sensor_data_5) + " Sensor 6="+str(sensor_data_6) + " Sensor 7="+str(sensor_data_7))
    # debug("Sensor DINS=" + str(sensor_dins))


def decode_blob_x(dest, x):
    if len(x) < 2:
        return
    if x[0] != 'x':
        return
    if x[1] != chr(X_VERSION):
        return
    x = x[2:]

    # Brute force decoding our way across the blob
    for i in xrange(16):
        if len(x) < 2:
            return
        dest.preset_angles[i].preset_angle = decode_word(x)
        x = chop_word(x)

    # The "nearest" flags for the above presets
    if len(x) < 2:
        return
    preset_flags = decode_word(x)
    for i in xrange(16):
        bit = 1 << i
        dest.preset_angles[i].nearest_enabled = bool(preset_flags & bit)
    x = chop_word(x)

    if x != '':
        debug(str(len(x)) + " bytes of BLOB_X unaccounted for")


def decode_blob_y(dest, y):
    if len(y) < 2:
        return
    if y[0] != 'y':
        return
    if y[1] != chr(Y_VERSION):
        return
    y = y[2:]

    # Brute force decoding our way across the blob
    # Wind Speed
    if len(y) < 2:
        return
    dest.wind_speed = round(decode_word(y) * 0.1, 1)
    y = chop_word(y)

    # Wind Direction
    if len(y) < 2:
        return
    dest.wind_direction = round(decode_word(y) * 0.1, 1)
    y = chop_word(y)

    # Average Wind Speed (1 minute average)
    if len(y) < 2:
        return
    dest.average_wind_speed = round(decode_word(y) * 0.1, 1)
    y = chop_word(y)

    # Peak Wind Speed (previous hour)
    if len(y) < 2:
        return
    dest.peak_wind_speed = round(decode_word(y) * 0.1, 1)
    y = chop_word(y)

    # Snow Sensor Temperature (16-bit) in tenths of degree F
    if len(y) < 2:
        return
    dest.snow_sensor_temperature = round(decode_word(y) * 0.1, 1)
    y = chop_word(y)

    # Snow Sensor Measured Distance (16-bit)
    if len(y) < 2:
        return
    # This comes in as millimeters, converting to meters
    dest.snow_sensor_distance = round(decode_word(y) * 0.001, 3)
    y = chop_word(y)

    # Snow Sensor Computed Snow Depth (16-bit)
    if len(y) < 2:
        return
    # This comes in as millimeters, converting to meters
    dest.snow_depth = round(decode_word(y) * 0.001, 3)
    y = chop_word(y)

    if y != '':
        debug(str(len(y)) + " bytes of BLOB_Y unaccounted for")

    dest.has_reported_weather = True  # For WeatherMonitor

    debug("Wind Speed=" + str(dest.wind_speed) + " Wind Direction=" + str(dest.wind_direction) + " Avg Wind Speed=" + str(dest.average_wind_speed) + " Peak Wind Speed=" + str(dest.peak_wind_speed))
