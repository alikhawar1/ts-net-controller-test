import logging
import uuid
import itertools
import time
import collections
from controller_enums import EventType

class EventsOrderedDict(collections.OrderedDict):
    """Returns items in reverse order"""
    def keys(self):
        return list(reversed(self))

    def values(self):
        return [self[key] for key in reversed(self)]

    def items(self):
        return [(key, self[key]) for key in reversed(self)]

    def iterkeys(self):
        return reversed(self)

    def itervalues(self):
        for k in reversed(self):
            yield self[k]

    def iteritems(self):
        for k in reversed(self):
            yield (k, self[k])


class EventLogEntry(object):
    __slots__ = ('id', 'name', 'message', 'levelno', 'created', 'cleared', 'type')

    def __init__(self, record):
        self.id = uuid.uuid4().hex
        self.name = record.name
        self.message = record.message
        self.levelno = record.levelno
        self.created = record.created + time.timezone
        self.cleared = None
        try:
            self.type = record.type
        except AttributeError:
            self.type = EventType.UNKNOWN
        

    def __getstate__(self):
        return (None, {slot: getattr(self, slot) for slot in self.__slots__})

    @property
    def levelname(self):
        return logging.getLevelName(self.levelno)


# From: https://stackoverflow.com/questions/3345785/getting-number-of-elements-in-an-iterator-in-python
def count_iter_items(iterable):
    """
    Consume an iterable not reading it into memory; return the number of items.
    """
    counter = itertools.count()
    collections.deque(itertools.izip(iterable, counter), maxlen=0)  # (consume at C speed)
    return next(counter)


class MemoryEventLog(logging.Handler):
    """
    A handler class which buffers logging records in memory.
    """
    ALARM_LEVELNO = logging.CRITICAL

    def __init__(self, capacity=500):
        """
        Initialize the handler with the buffer size.
        """
        logging.Handler.__init__(self, level=1)
        self._capacity = capacity
        self.collection = EventsOrderedDict()

    def emit(self, record):
        """
        Emit a record which in our cases just add the record to the memory buffer
        """
        if record.name == 'EventLog' or record.levelno >= self.ALARM_LEVELNO:
            self.format(record)
            entry = EventLogEntry(record)
            if len(self.collection) >= self._capacity:
                entries = self._get_non_critical_events()
                try:
                    _entry = next(entries)
                    self.collection.pop(_entry.id)
                except StopIteration:
                    # Everything is critical but go ahead and make room anyways
                    self.collection.popitem(last=False)
            self.collection[entry.id] = entry

    def set_record_cleared_flag(self, record_ids, cleared):
        """
        Sets or removes the 'cleared' property on a set of records
        :param list record_ids: A list of record_ids to modify
        :param bool cleared: Indicates if the records should be marked as cleared
        :return: Success
        :rtype: list
        """
        now = time.time() + time.timezone if cleared else None
        return [(record, setattr(record, 'cleared', now))[0] for record in self.collection.itervalues() if record.id in record_ids and record.levelno >= self.ALARM_LEVELNO]

    def get_record_count(self, level=logging.CRITICAL, count_cleared=False):
        """
        Get the number of records that are the specified level or above
        :param int level: The logging level to filter by
        :return: The number of log records
        :rtype: int
        """
        records_iter = itertools.ifilter(lambda record:
                                         record.levelno >= level and (count_cleared or record.cleared is None),
                                         self.collection.itervalues())
        return count_iter_items(records_iter)  # Count items in iterator

    def get_events(self):
        """
        Gets an iterator to all of the event log entries
        :return: Event log entries
        :rtype: iter
        """
        return self.collection.itervalues()

    def _get_non_critical_events(self):
        """
        Gets an iterator to all the events that are non-critical or have been cleared
        :return: Non-critical events
        :rtype: iter
        """
        return itertools.ifilter(lambda record: record.levelno < logging.CRITICAL or record.cleared is not None, self.collection.itervalues())
