"""
ModemMonitor - Responsible for gathering information ABOUT our
cellular connectivity via a ReST API provided by the modem.
The "interface" in this case is the set of member variables
that you can access directly (anything not _private).
"""


import logging

import tornado.httpclient
import tornado.ioloop
import datetime


log = logging.getLogger(__name__)

MINUTES = 60 # 60 seconds in one minute, used for readability down below

class ModemMonitor(object):
    """
    The "interface" in this case is the set of member variables
    that you can access directly (anything not _private).
    """

    # Time duration in millis used to schedule on_update_cell_stats callback.
    HOURLY_CELL_STATS_REPORT_INTERVAL = 1000*60*60

    def __init__(self, ip_address='192.168.2.1', io_loop=None,
                 on_state_change=None, on_update_cell_stats=None, on_daily_update_cell_stats=None,
                 modem_power_cycle=None, connected_to_cloud=None):
        """
        Constructor. Parameters include:
        :param str ip_address: the ip_address of the device to be monitored.
        :param io_loop: Optional. A Tornado IOLoop instance
        :param func on_state_change: Optional. Callable for when the cell modem connection state changes. Signature is func(connected, msg)
        :param func on_update_cell_stats: Optional. Callable for when cell modem stats pushed to cloud. Signature is func(rssi_dbm, uptime)
        :param func on_daily_update_cell_stats: Optional. Callable for daily cell modem stats pushed to cloud. Signature is func(cell_modem)
        :param func modem_power_cycle: Optional. Callable to power-cycle the cell modem. Signature is func()
        :param func connected_to_cloud: Optional. Callable to get boolean status. Signature is bool func()
        """
        self._ip_address = ip_address

        self.io_loop = io_loop or tornado.ioloop.IOLoop.current()

        self.on_state_change = on_state_change

        self.on_update_cell_stats = on_update_cell_stats

        self.on_daily_update_cell_stats = on_daily_update_cell_stats

        self.modem_power_cycle = modem_power_cycle
        self.connected_to_cloud = connected_to_cloud

        self.login_counter = 0 # used to alternate between the possible passwords...
        self._token = None  # This becomes known AFTER we login to the ReST API
        self.rest_api_error = False
        self.comm_error = False

        self._comm_timeouts = 0
        self._MAX_COMM_TIMEOUTS = 5

        self.ppp_up = None  # will become True/False once we get a report from the cell modem

        self._query_index = 0  # which query to send next
        self._MAX_QUERIES = 2  # when to wrap back around to the start of the sequence

        # We will learn the following over the ReST API (via ongoing polling)
        self.rssi_dbm = 0
        self.imei = "Unknown"
        self.roaming = False
        self.mdn = "Unknown"
        self.lan_ip = "Unknown"
        self.wan_ip = "Unknown"
        self.link_status = "Unknown"
        self.uptime = 0
        self.tx_data_usage = "0"
        self.rx_data_usage = "0"
        self.tower_id = "Unknown"

        self._cellular_reboots = 0
        self._cellular_offline_seconds = 0
        self._cellular_offline_threshold = 10 * MINUTES

        # You might think once a second would be slow enough, but there are pretty
        # big JSON responses coming back, and each query takes around a second to
        # complete.
        self._timer = tornado.ioloop.PeriodicCallback(self._every_three_seconds, 3000)
        self._timer.start()

        self._timer2 = tornado.ioloop.PeriodicCallback(self.on_startup, 60000*3)
        self._timer2.start()

        self._timer3 = tornado.ioloop.PeriodicCallback(self._every_second, 1000)
        self._timer3.start()

        self.schedule_daily_update()

    def force_reconnect(self):
        self._token = None

    @property
    def is_responding(self):
        return not self.comm_error

    def _build_url(self, snippet):
        url = "https://" + self._ip_address + "/api/"
        url += snippet + "?token=" + self._token
        return url

    def _response_received(self):
        self._comm_timeouts = 0
        if self.comm_error:
            self._notify_modem_change(True, "Cell modem has started responding")
        self.comm_error = False

    def _every_second(self):
        connectivity_good = True # will override this if disproven...
        if self.connected_to_cloud is not None:
            connectivity_good = self.connected_to_cloud()
        connectivity_good = connectivity_good and self.ppp_up
        # If cell modem and connection to cloud are good
        if connectivity_good:
            self._cellular_reboots = 0
            self._cellular_offline_seconds = 0
            self._cellular_offline_threshold = 5 * MINUTES
        else:
            self._cellular_offline_seconds += 1
            if self._cellular_offline_seconds > self._cellular_offline_threshold:
                if self.modem_power_cycle is not None:
                    log.critical("Cellular connectivity has been down for %d minutes, power-cycling the cell modem" % (self._cellular_offline_seconds / MINUTES))
                    self.modem_power_cycle()
                self.force_reconnect()  # Force another login attempt...
                self._cellular_reboots += 1
                self._cellular_offline_seconds = 0
                # Compute a longer threshold for next time
                if self._cellular_reboots == 1:
                    self._cellular_offline_threshold = 20 * MINUTES
                elif self._cellular_reboots == 2:
                    self._cellular_offline_threshold = 40 * MINUTES
                elif self._cellular_reboots == 3:
                    self._cellular_offline_threshold = 60 * MINUTES
                elif self._cellular_reboots == 4:
                    self._cellular_offline_threshold = 120 * MINUTES # 2 hours
                else:
                    self._cellular_offline_threshold = 240 * MINUTES # 4 hours

    def _every_three_seconds(self):
        #print "tick"
        # Tornado's PeriodicCallbacks can result in the NEXT interval being "short" if
        # we were delayed in getting to this callback, or if it takes very long to run.
        # We are restarting the timer manually each time, to ensure we get a full interval.
        self._timer.stop()
        if self._comm_timeouts < self._MAX_COMM_TIMEOUTS:
            self._comm_timeouts += 1
            if self._comm_timeouts > self._MAX_COMM_TIMEOUTS:
                self.comm_error = True
                self._notify_modem_change(False, "Cell modem is not responding!")
                # Handle the case of a rebooted cell modem...
                self._token = None  # Force another login attempt...

        if self._token is None:
            self._login()
        else:
            # The pieces of information we want are scattered across
            # multiple ReST API calls. Two at present, structuring
            # the code to allow for more in the future.
            if self._query_index == 0:
                self._get_radio_stats()
            else:
                self._get_ppp_stats()

            self._query_index += 1
            if self._query_index >= self._MAX_QUERIES:
                self._query_index = 0

        self._timer = tornado.ioloop.PeriodicCallback(self._every_three_seconds, 3000)
        self._timer.start()

    def _login(self):
        #print "L?"
        http = tornado.httpclient.AsyncHTTPClient()
        url = "https://" + self._ip_address
        # Quick hack to get around there now being two default passwords in play...
        self.login_counter += 1
        if self.login_counter & 1:
            url += "/api/login?username=admin&password=admin" # original Multitech default
        else:
            url += "/api/login?username=admin&password=Admin1234%" # new Multitech has no default, AND enforces password complexity
        log.debug("attempting to fetch " + url)
        http.fetch(url, callback=self._on_login_response, ssl_options={})

    def _on_login_response(self, response):
        #print "L."
        # log.debug("reached on_login_response()")
        if response.error:
            #print "L!"
            if response.code != 599:  # timeout
                log.warning("Response error from cell modem %s" % response.error)
            else:
                log.debug(response.error)
            self.rest_api_error = True
            return
        else:
            self.rest_api_error = False
            self._response_received()

        try:
            json = tornado.escape.json_decode(response.body)
            log.debug(json)
            result = json["result"]
            self._token = result["token"]
            log.debug("token=%s", self._token)
        except Exception, e:
            self.rest_api_error = True

    def _get_radio_stats(self):
        #print "R?"
        http = tornado.httpclient.AsyncHTTPClient()
        url = self._build_url("stats/radio")
        log.debug("attempting to fetch " + url)
        http.fetch(url, callback=self._on_radio_stats_response, ssl_options={})

    def _on_radio_stats_response(self, response):
        #print "R."
        log.debug("reached on_radio_stats_response()")
        if response.error:
            #print "R!"
            self.rest_api_error = True
            self.force_reconnect()  # Force another login attempt...
            return
        else:
            self.rest_api_error = False
            self._response_received()
        try:
            json = tornado.escape.json_decode(response.body)
            log.debug(json)
            # Extract the data we are interested in from the json result
            result = json["result"]
            self.rssi_dbm = result.get("rssidBm", None)
            self.imei = result["imei"]
            self.roaming = result["roaming"]
            self.mdn = result["mdn"]
            log.info("RSSI=" + str(self.rssi_dbm) + " dBm")
            log.info("IMEI=" + self.imei)
            log.info("roaming=" + str(self.roaming))
            log.info("mdn=" + self.mdn)
        except Exception, e:
            self.rest_api_error = True

    def _get_ppp_stats(self):
        #print "P?"
        http = tornado.httpclient.AsyncHTTPClient()
        url = self._build_url("stats/ppp")
        log.debug("attempting to fetch " + url)
        http.fetch(url, callback=self._on_ppp_stats_response, ssl_options={})

    def _on_ppp_stats_response(self, response):
        #print "P."
        log.debug("reached on_ppp_stats_response()")
        if response.error:
            #print "P!"
            self.rest_api_error = True
            self.force_reconnect()  # Force another login attempt...
            return
        else:
            self.rest_api_error = False
            self._response_received()
        try:
            json = tornado.escape.json_decode(response.body)
            log.debug(json)
            # Extract the data we are interested in from the json result
            result = json["result"]
            self.link_status = result["link"]
            self.uptime = result["uptime"]
            self.lan_ip = result["localIp"]
            self.wan_ip = result["remoteIp"]
            self.tower_id = result["tower"]

            # I am curious how reliable the celluar connection is
            # TBD if we promote this to a full-fledged "system status" or not
            if self.link_status == "PPP Link is up":
                if self.ppp_up is not None:
                    if self.ppp_up == False:
                        log.critical("Cellular connectivity has been established")
                self.ppp_up = True

            if self.link_status != "PPP Link is down":
                tx = result["tx"]
                self.tx_data_usage = tx["bytes"]

                rx = result["rx"]
                self.rx_data_usage = rx["bytes"]
            else: # it IS down...
                if self.ppp_up is not None:
                    if self.ppp_up == True:
                        log.critical("Cellular connectivity has been lost!")
                self.ppp_up = False

            log.info("link_status=" + self.link_status)
            log.info("uptime=" + str(self.uptime))
            log.info("lan_ip=" + self.lan_ip)
            log.info("wan_ip=" + self.wan_ip)
            log.info("tower_id=" + self.tower_id)
            log.info("tx_data_usage=" + str(self.tx_data_usage))
            log.info("rx_data_usage=" + str(self.rx_data_usage))
        except Exception, e:
            self.rest_api_error = True

    def _notify_modem_change(self, state, msg):
        """
        Notify the subscriber that the state of the connection has changed
        :param bool state: Indicates the new state
        :param str msg: The event log message string to log
        :return: None
        """
        log.critical(msg)
        if self.on_state_change:
            try:
                self.on_state_change(state, msg)
            except:
                pass

    def _send_cell_stats(self, rssi_dbm, uptime):
        """
        Updates the subscriber with cell data periodically with a delay of 1 hour.
        :param rssi_dbm: Indicates the received signal strength of modem.
        :param uptime: Time since cellular module is active.
        """
        if self.on_update_cell_stats:
            try:
                self.on_update_cell_stats(rssi_dbm, uptime)
            except:
                log.exception('An error occurred while executing a callback [on_update_cell_stats()]')

    def _after_every_hour(self):
        self._send_cell_stats(self.rssi_dbm, self.uptime)

    def schedule_daily_update(self, days=1, hours=8, minutes=0):
        """
        Reports the subscriber with cell data every day at 8:00 AM - a default time period.
        :param days: Indicates no. of days after them data should be reported.
        :param hours: Hour of the day at which data is reported.
        :param minutes: Minutes at which data is reported to cloud.
        """
        time_delta = self.schedule_interval(days, hours, minutes)
        def wrapper():
            if self.on_daily_update_cell_stats:
                try:
                    self.on_daily_update_cell_stats(self)
                    self.schedule_daily_update()
                except Exception, e:
                    log.exception('An error occurred while executing a callback [schedule_update][on_daily_update_cell_stats()]')
                    log.exception(e)

        tornado.ioloop.IOLoop.current().add_timeout(time_delta, wrapper)


    def schedule_interval(self, days, hours, minutes):
        """
        Calculates a `datetime.timedelta` corresponding to input. Note that if `days == 0`,
        then it only returns duration not the specific timestamp in next day
        :param days: Indicates no. of days after them data should be reported.
        :param hours: Hour of the day at which data is reported.
        :param minutes: Minutes at which data is reported to cloud.
        """
        if days == 0:
            return datetime.timedelta(hours=hours, minutes=minutes)
        current_datetime = datetime.datetime.today()

        if current_datetime.hour < hours:
            return datetime.timedelta(hours=hours-current_datetime.hour,
                                      minutes=minutes-current_datetime.minute)

        tomorrow = current_datetime + datetime.timedelta(days=days)
        midnight = datetime.datetime.combine(tomorrow, datetime.time.min)
        actual_datetime = midnight + datetime.timedelta(hours=hours, minutes=minutes)
        time_delta = actual_datetime - current_datetime

        return time_delta

    def on_startup(self):
        """
        Executes `on_daily_update_cell_stats` and `on_update_cell_stats` callbacks 1x time after some seconds the NC started
        so that the cell stats get published once at startup if NC started after the specified timestamp.
        """
        # if self.link_status is not None:
        if self.on_daily_update_cell_stats:
            try:
                self._timer2.stop()
                self.on_daily_update_cell_stats(self)
                self._send_cell_stats(self.rssi_dbm, self.uptime)
                tornado.ioloop.PeriodicCallback(self._after_every_hour, ModemMonitor.HOURLY_CELL_STATS_REPORT_INTERVAL).start()
            except Exception, e:
                log.exception('An error occurred while executing a callback [on_startup]')
                log.exception(e)


if __name__ == '__main__':
    logging.basicConfig(level=logging.WARNING)
    # logging.basicConfig(level=logging.INFO)
    # logging.basicConfig(level=logging.DEBUG)

    def on_update_cell_stats(obj):
        print "[" + str(datetime.datetime.utcnow()) + "] link_status:   " + obj.link_status

    mod_mon = ModemMonitor('192.168.2.1')
    mod_mon.on_daily_update_cell_stats = on_update_cell_stats

    tornado.ioloop.IOLoop.current().start()
