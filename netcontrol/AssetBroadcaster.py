"""
AssetBroadcaster.py - The job of this module is to hide the OTA API from
the top-level code base, and replace it with a more generic one.

This module does not decide what to do, nor does it remember what has been done.
For those aspects, please refer to AssetCommander.py.

In the original system, all the commands implemented here were "site-wide".
We have since started adding commands that only apply to a specific list of assets,
with plans to also support GROUPS of assets as well as true INDIVIDUAL control.

TODO - Keep an eye on the size of this file. At some point it may be worth splitting
AssetBroadcaster up into finer-grained files/classes.
"""

import logging
import datetime
import binascii


from controller_enums import TrackerPresets
from controller_enums import EventType

event_log = logging.getLogger('EventLog')

SNAP_ADDRESS_SIZE = 3

class AssetBroadcaster(object):
    """
    convert generic commands from the high-level code into specific
    SNAP Connect RPC calls (SNAPpy script dependent)
    """
    def __init__(self, snap_connect, nccb_adapter, schedule_func, max_hops=10):
        """
        Constructor. Parameters include:
        snap_connect - the object we use to do SNAP radio communications
        nccb_adapter - the object we use to send the same communications to the NCCB
        max_hops - how many times the command should be repeated by the network
        """
        if snap_connect is None:
            raise Exception("Error: AssetBroadcaster requires a SNAP Connect!")
        else:
            self._snap_connect = snap_connect

        if nccb_adapter is None:
            raise Exception("Error: AssetBroadcaster requires a nccb_adapter!")
        else:
            self._nccb_adapter = nccb_adapter

        self._schedule_func = schedule_func

        self.max_hops = max_hops

        # We use these to support sending a dmcast_rpc() function call to more devices
        # than can fit into a single packet
        self._next_dmcast_timer_cb = None
        self._next_dmcast_addresses = []
        self._next_dmcast_args = ()

        # We use this to separate command processing from the Web UI
        # It is intended to contain tuples, each of which is made up of:
        # the timer callback control (so we can stop the timer from firing AGAIN),
        # the function and the parameters of that function
        self._work_queue = []
        # See also push_to_work_queue() and pull_from_work_queue()

    def pull_from_work_queue(self):
        job = self._work_queue.pop()
        timer_callback_control = job[0]
        timer_callback_control.stop()
        func = job[1]
        args = job[2]
        func(*args)

    def push_to_work_queue(self, func, *args):
        timer_callback_control = self._schedule_func(0.001, self.pull_from_work_queue)
        job = (timer_callback_control, func, args)
        # Do not use self._work_queue.append() here! We want the job to go on the FRONT of the list
        self._work_queue = [job] + self._work_queue

    # Use this function for broadcasting unless you are certain the NCCB doesn't need to hear it
    def send_mcast_rpc(self, *args):
        """The point of this function is to unify the radio and USB-serial communications"""
        self._snap_connect.mcast_rpc(*args)
        self._nccb_adapter.send_mcast_rpc(*args)

    #
    # This next group of commands is "site-wide", and uses multicasts (currently on group 1)
    # TODO Revisit the group number sometime... maybe these should now be sent on the DATA group?
    #

    def stow(self):
        """Stow the panel racks (for example - high wind)"""
        # Manual optimization - don't bother sending this to the NCCB
        self._snap_connect.mcast_rpc(1, self.max_hops, "move_to_preset", 0, TrackerPresets.STOW.value)

    def nightly_stow(self):
        """Stow the panel racks overnight (typically this means "to the East" but all presets are programmable)"""
        # Manual optimization - don't bother sending this to the NCCB
        self._snap_connect.mcast_rpc(1, self.max_hops, "move_to_preset", 0, TrackerPresets.STOW_NIGHT.value)

    def go_to_preset(self, preset):
        """Command the panels to go to a preconfigured preset angel"""
        # Manual optimization - don't bother sending this to the NCCB
        self._snap_connect.mcast_rpc(1, self.max_hops, "move_to_preset", 0, preset)

    def sun_angles(self, flags, dwell_seconds, angles_string, minutes_since_accumulators_reset):
        """Forecasts of upcoming sun positions"""
        # We have to combine flags and dwell_seconds in order to fit within a single packet
        # (Raising MAX_ANGLES from 15 to 30 put us on the edge)
        FLAGS_MASK = 0x8000
        if (flags & ~FLAGS_MASK) != 0x0000:
            raise Exception("Error: A new flag has been defined without being accounted for here!")
        if (dwell_seconds & (FLAGS_MASK-1)) != dwell_seconds:
            raise Exception("Error: dwell_seconds too large to be merged with flags!")
        flags_and_dwell_seconds = (flags & FLAGS_MASK) | (dwell_seconds & (FLAGS_MASK-1))
        # Manual optimization - don't bother sending this to the NCCB
        self._snap_connect.mcast_rpc(1, self.max_hops, "sun_angles", flags_and_dwell_seconds, angles_string, minutes_since_accumulators_reset)

    def stop(self):
        """
        Stop any panel movement
        :return: None
        """
        # Manual optimization - don't bother sending this to the NCCB
        self._snap_connect.mcast_rpc(1, self.max_hops, "stop", 0)

    def reveille(self):
        """Reset all of your accumulated totals for the day"""
        seq = datetime.datetime.utcnow().day  # Use the day for now as the sequence number
        # Notice we are sending to everyone including the NCCB
        self.send_mcast_rpc(1, self.max_hops, "reset_all_totals", seq)

    def time_is(self):
        """Broadcast the time so devices can set their internal clocks"""
        t = datetime.datetime.utcnow()
        # Notice we are sending to everyone including the NCCB
        self.send_mcast_rpc(1, self.max_hops, "time_is", t.year, t.month, t.day, t.hour, t.minute, t.second, t.isoweekday())

    #
    # Here are some routines for supporting commands to a specified RANDOM SUBSET of the assets
    # Note that this is not the same thing as GROUPS!
    # GROUPS are pre-defined, this is more ad-hoc
    #

    #
    # Helper routines
    #
    def send_dmcast_rpc(self, binary_address_list, *args):
        """The point of this function is to send the command to as many addresses AS WILL FIT"""
        address_string = ''.join(binary_address_list)
        successfully_sent = False
        while not successfully_sent:
            try:
                self._snap_connect.dmcast_rpc(address_string, *args)
                # If we get to here, everything fit into the packet
                successfully_sent = True
            except Exception, e:
                # If we get to here, the address_list was probably too large to encode
                # Worse case, we can try to chop off one address
                addresses_to_subtract = 1
                # Try to be smarter, by examining the actual exception text
                msg = str(e)
                prefix = "Function/Args too large to encode ("
                if msg.startswith(prefix):
                    msg = msg.replace(prefix, "")
                    suffix = " bytes, max is 123)"
                    if msg.endswith(suffix):
                        msg = msg.replace(suffix, "")
                        try:
                            temp = int(msg)
                            if temp > 123:
                                # Carefully compute how much we need to chop off
                                temp = temp - 123
                                temp -= 1
                                temp = temp / SNAP_ADDRESS_SIZE
                                temp += 1
                                addresses_to_subtract = temp
                        except:
                            pass
                # addresses_to_subtract is either 1 or a computed optimum value
                if len(address_string) > (SNAP_ADDRESS_SIZE * addresses_to_subtract):
                    address_string = address_string[:-(SNAP_ADDRESS_SIZE * addresses_to_subtract)]
                    # and we will try again because of the while loop
                else:
                    return 0
        # If we get to here, we were able to transmit the packet to the current address_string
        # Tell the caller HOW MANY addresses we were able to include
        return len(address_string) / SNAP_ADDRESS_SIZE

    def send_dmcast_rpcs(self, binary_address_list, *args):
        """The point of this function is to send the command to all the
           addresses, even if it takes multiple packets to do so"""
        total_addresses = len(binary_address_list)
        addresses_actually_sent_to = self.send_dmcast_rpc(binary_address_list, *args)
        if (addresses_actually_sent_to != total_addresses):
            if self._next_dmcast_timer_cb is not None:
                self._next_dmcast_timer_cb.stop()

            self._next_dmcast_addresses = binary_address_list[addresses_actually_sent_to:]
            self._next_dmcast_args = args

            # This doesn't use the work_queue because we actually want a delay too
            self._next_dmcast_timer_cb = self._schedule_func(0.25, self.dmcast_timer_cb)

    def dmcast_timer_cb(self):
        """Function send_dmcast_rpcs() uses this timer callback to re-invoke himself when needed"""
        self._next_dmcast_timer_cb.stop()
        self._next_dmcast_timer_cb = None
        self.send_dmcast_rpcs(self._next_dmcast_addresses, *self._next_dmcast_args)

    #
    # Specific commands
    #
    def log_clear_faults(self, asset_id):
        event_text = 'Clear Faults command sent to '
        if asset_id == 'serial':
            event_text += 'the NCCB'
        else:
            event_text += 'asset ' + asset_id
        event_log.info(event_text, extra=EventType.INDIVIDUAL_ASSET._dict)

    def clear_faults(self, asset_ids, fault_bitmask):
        # Deal with the NCCB first
        if 'serial' in asset_ids:
            asset_ids.remove('serial')
            self._nccb_adapter.send_mcast_rpc(1, 1, "clear_faults", 0, fault_bitmask)
            self.log_clear_faults('serial')

        # Then deal with the over-the-air devices
        binary_address_list = []
        for asset_id in asset_ids:
            snap_addr = binascii.unhexlify(asset_id)
            binary_address_list.append(snap_addr)
            self.log_clear_faults(asset_id)
        if len(binary_address_list) > 0:
            self.send_dmcast_rpcs(binary_address_list, 1, self.max_hops, 0, "clear_faults", 0, fault_bitmask)

    # This is our GraphQL tie-in point. SLUI user did XXX, this got called.
    # Also expect to hit this from the cloud UI at some point.
    def request_clear_faults(self, asset_ids, fault_bitmask=0xff):
        # Keep the UIs responsive by deferring the actual work...
        self.push_to_work_queue(self.clear_faults, asset_ids, fault_bitmask)

        return asset_ids


#
# Make it so running module standalone results in a functional test
#
if __name__ == "__main__":
    import tornado.ioloop
    from SnapComm import SnapComm
    from Scheduler import Scheduler

    # logging.basicConfig(level=logging.WARNING)
    # logging.basicConfig(level=logging.INFO)
    logging.basicConfig(level=logging.DEBUG)

    snapcomm = SnapComm()

    class FakeNccbAdapter(object):
        def send_mcast_rpc(self, *args):
            pass

    fake_nccb_adapter = FakeNccbAdapter()

    asset_broadcaster = AssetBroadcaster(snapcomm.sc, fake_nccb_adapter)

    stow_event_handle = None

    def stow_test():
        asset_broadcaster.stow()
        stow_event_handle.stop()

    stow_event_handle = Scheduler.schedule_func(5.0, stow_test)

    angle_event_handle = None

    def angle_test():
        asset_broadcaster.sun_angles(60, "\x00\x00\x00\x0a", 0)
        angle_event_handle.stop()

    angle_event_handle = Scheduler.schedule_func(10.0, angle_test)

    tornado.ioloop.IOLoop.current().start()
