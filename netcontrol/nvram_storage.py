"""
nvram_storage.py, NvramStorage class - This file lets us leverage
the spare 56 bytes of NVRAM in the DS1307 RTC of the E12.BaseException

Note that on Windows this file simulates the NVRAM using a normal file.BaseException

Note that on Linux this file requires user netctrl to have sudoers permission
for the command /usr/bin/tee /sys/class/rtc/rtc1/device/nvram.

This is currently done via file /etc/sudoers.d/netctrl (see Manufacturing script(s))

NOTE - This feature is NOT for "config" information! Those should go in FLASH.
"""

import logging
import sys
import binascii
from controller_enums import EventType

if sys.platform == 'linux2':
    import subprocess

log = logging.getLogger(__name__)

class NvramStorage(object):
    """
    Using either real or simulated NVRAM, support persistent state between
    runs of the Network Controller application.
    """

    if sys.platform == 'linux2':
        PERSISTENT_STATUS_FILE = '/sys/class/rtc/rtc1/device/nvram'
    else:
        PERSISTENT_STATUS_FILE = 'simulated.nvram'

    TOTAL_NVRAM_SPACE = 56  # Limitation of the DS1307 RTC chip
    USABLE_NVRAM_SPACE = 48  # because we prepend an 8-byte header (see below)

    # Here is the low-level structure of the NVRAM
    PERSISTENT_STATUS_HEADER = 'FS'
    PERSISTENT_STATUS_VERSION = 0
    # A varying LENGTH byte comes next
    # A 4-byte CRC32 of the next LENGTH bytes (after the CRC) comes next
    # LENGTH bytes of data (possibly 0, up to a maximum of USABLE_NVRAM_SPACE)

    def __init__(self):
        """Constructor - attempt to read in and validate a previously saved state from NVRAM"""
        try:
            file_object = open(NvramStorage.PERSISTENT_STATUS_FILE, 'rb')
            self.nvram = file_object.read()
            file_object.close()
            if self.nvram == '':
                self.nvram = '\x00' * NvramStorage.TOTAL_NVRAM_SPACE
            if len(self.nvram) > NvramStorage.TOTAL_NVRAM_SPACE:
                self.nvram = self.nvram[:NvramStorage.TOTAL_NVRAM_SPACE]
        except:
            self.nvram = '\x00' * NvramStorage.TOTAL_NVRAM_SPACE

        # See if this is a valid, previously saved, state image
        self.state = ''  # guilty until proven innocent
        if self.nvram[0:2] == NvramStorage.PERSISTENT_STATUS_HEADER:
            if ord(self.nvram[2]) <= NvramStorage.PERSISTENT_STATUS_VERSION:
                possible_state_length = ord(self.nvram[3])
                if (possible_state_length > 0) and (possible_state_length <= NvramStorage.USABLE_NVRAM_SPACE):
                    # We stored the CRC-32 little-endian
                    stored_crc = ord(self.nvram[4])
                    stored_crc += ord(self.nvram[5]) << 8
                    stored_crc += ord(self.nvram[6]) << 16
                    stored_crc += ord(self.nvram[7]) << 24
                    possible_state = self.nvram[8:8 + possible_state_length]
                    computed_crc = binascii.crc32(possible_state) & 0xFFFFFFFF
                    if computed_crc == stored_crc:
                        self.state = possible_state

    def get_state(self):
        """Returns the currently loaded state string"""
        return self.state

    def set_state(self, new_state):
        """Write a new state string into NVRAM (does nothing if unchanged since last write)"""
        if new_state == self.state:
            return
        if len(new_state) > NvramStorage.USABLE_NVRAM_SPACE:
            return
        # build up new NVRAM string
        self.nvram = NvramStorage.PERSISTENT_STATUS_HEADER
        self.nvram += chr(NvramStorage.PERSISTENT_STATUS_VERSION)
        self.nvram += chr(len(new_state))
        computed_crc = binascii.crc32(new_state) & 0xFFFFFFFF

        # Storing the CRC-32 little-endian
        self.nvram += chr(computed_crc & 0xFF)
        computed_crc >>= 8
        self.nvram += chr(computed_crc & 0xFF)
        computed_crc >>= 8
        self.nvram += chr(computed_crc & 0xFF)
        computed_crc >>= 8
        self.nvram += chr(computed_crc & 0xFF)

        self.nvram += new_state
        self._write_nvram()
        self.state = new_state

    def _write_nvram(self):
        if sys.platform == 'linux2':
            formatted_data = ''
            for character in self.nvram:
                formatted_data += '\\x%02X' % (ord(character),)

            command = "/usr/bin/printf '" + formatted_data + "' | sudo tee " + NvramStorage.PERSISTENT_STATUS_FILE

            # print self.nvram
            # print formatted_data
            # print command

            proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
            stdout_value = proc.communicate()[0]

            # print stdout_value
        else:
            try:
                file_object = open(NvramStorage.PERSISTENT_STATUS_FILE, 'wb')
                file_object.write(self.nvram)
                file_object.close()
            except:
                logging.getLogger('EventLog').log(logging.CRITICAL, "Unable to persist system state to NVRAM - do not power off!", extra=EventType.NETWORK_CONTROLLER._dict)

#
# Make it so running module standalone lets me step through
# a minimal functional test
#
if __name__ == "__main__":

    test = NvramStorage()

    print binascii.hexlify(test.get_state())

    test.set_state('This is just test data!')

    print binascii.hexlify(test.get_state())

    test2 = NvramStorage()

    print binascii.hexlify(test2.get_state())

    test2.set_state('')

    print binascii.hexlify(test2.get_state())
