"""
FormatHelpers.py - the purpose of this module is to break out
some common formatting routines for easier re-use
"""

import binascii
import Asset

def format_snap_address(snap_address):
    """Convert a SNAP address into AA.BB.CC format"""

    # What I am doing here is supporting BOTH 3-byte binary strings
    # and 6 character HEX-ASCII strings
    if snap_address == 'serial':
        return 'NCCB'

    if len(snap_address) == 3:
        snap_address = binascii.hexlify(snap_address)

    if len(snap_address) != 6:
        return "invalid Snap Address"

    snap_address = snap_address.upper()

    snap_address = snap_address[0:2] + '.' + snap_address[2:4] + '.' + snap_address[4:]

    return snap_address

def format_asset_identity(asset):
    identity = binascii.hexlify(asset.snap_address) # We may switch to format_snap_address() here...
    if (asset.location_text is not None) and (asset.location_text != ''):
        identity += ' [' + asset.location_text + ']'
    else:
        identity += ' (location unknown)'
    return identity

# This format was requested by Terrasmart (I would have said " days" instead of "D")

def format_piecewise_uptime(days, hours, minutes, seconds):
    result = str(days) + 'D '
    result += '%02d:%02d:%02d' % (hours, minutes, seconds)
    return result

def format_uptime_seconds(seconds):
    # I found a suitable conversion routine in a quick google...
    MINUTE = 60
    HOUR = MINUTE * 60
    DAY = HOUR * 24

    days = int(seconds / DAY)
    hours = int((seconds % DAY) / HOUR)
    minutes = int((seconds % HOUR) / MINUTE)
    seconds = int(seconds % MINUTE)
    result = format_piecewise_uptime(days, hours, minutes, seconds)
    return result


# SNAPpy uses SIGNED 16-bit integers, but Python integers are bigger than that.
# Sometimes we would rather treat them as unsigned 16-bit integers.
# This routine helps with that.
def force_16_bit_unsigned(value):
    return value & 0xFFFF
