import struct
import binascii
import time

def format_for_portal_use(raw_string):
    expanded1 = binascii.hexlify(now_str)
    print expanded1
    expanded2 = "'"
    while expanded1 != '':
        expanded2 += '\\x'
        expanded2 += expanded1[0:2]
        expanded1 = expanded1[2:]
    expanded2 += "'"
    print expanded2
    return expanded2

now = time.time()
print now
# We are making a timestamp for use by Javascript, which wants milliseconds
now *= 1000.0
print now

#convert to string
now_str = struct.pack('>d', now)

print len(now_str)
print now_str

format_for_portal_use(now_str)

now2 = struct.unpack('>d', now_str)[0]
print now2

# Work around issue with NUL bytes in strings from Portal
now_str = now_str[0:7] + chr( ord(now_str[7]) + 1 )

format_for_portal_use(now_str)

now2 = struct.unpack('>d', now_str)[0]
print now2
