"""
SystemManager.py - System-wide concerns like OVERNIGHT POWERDOWN
and PROGRAM EXIT are addressed in this module.

The intent is to consolidate the handling of such things to
keep the remaining code cleaner.

TODO OVERNIGHT POWERDOWN code currently in NCCBMonitor needs to be
migrated into this module at some point.
"""

import logging
import datetime

import gateway_utils
from controller_enums import EventType


log = logging.getLogger(__name__)


class SystemManager(object):
    """Handler of system-wide concerns, such as program shutdown"""

    MINIMUM_AWAKE_TIME = 5 * 60  # seconds before the "overnight shutdown" code is ALLOWED to run

    GATEWAY_POWER_OFF_DELAY = 2 * 60  # seconds to wait before having NCCB cut power
    CELL_MODEM_POWER_ON_DELAY = 10  # seconds to wait between E12 and Cell Modem power UP

    def __init__(self, schedule_func):
        """
        Constructor
        :param callable schedule_func: A function with the signature func(call_every_n_seconds, callback_func)
        """
        self._schedule_func = schedule_func

        self._my_site = None  # See set_site()
        self._sun_tracker = None  # See set_sun_tracker()
        self._asset_manager = None  # See set_asset_manager()
        self._nccb_monitor = None  # See set_nccb_monitor()
        self._cloud_updater = None  # See set_cloud_updater()

        self.sun_rise_set = None
        self._wifi_shutdown_timer = None
        self.minimum_awake_countdown = self.MINIMUM_AWAKE_TIME

        self.cell_modem_power_low = False
        self.cell_modem_powered_off = False
        self.gateway_power_low = False
        self._cell_modem_thrash_inhibitor = 0
        self._modem_shutdown_timer = None
        self._low_power_shutdown_needed = False

        self._periodic_poll_timer = self._schedule_func(1, self.periodic_poll)
        self._power_on_seconds = 0

    def set_site(self, my_site):
        """fills in missing info (info not known at constructor time)"""
        self._my_site = my_site

    def set_sun_tracker(self, sun_tracker):
        """fills in missing info (info not known at constructor time)"""
        self._sun_tracker = sun_tracker

    def set_asset_manager(self, asset_manager):
        """fills in missing info (info not known at constructor time)"""
        self._asset_manager = asset_manager

    def set_nccb_monitor(self, nccb_monitor):
        """fills in missing info (info not known at constructor time)"""
        self._nccb_monitor = nccb_monitor

    def set_cloud_updater(self, cloud_updater):
        """fills in missing info (info not known at constructor time)"""
        self._cloud_updater = cloud_updater

    # The following was added to support the new AssetMover.
    # He tries not to reconfigure the network at a bad time,
    # in this case "not when we are about to shutdown for the night".
    # Simply returns "24 hours" when not even close...
    def minutes_until_nightly_shutdown(self):
        twenty_four_hours = 24 * 60
        if self._my_site.enable_nightly_shutdown:
            now = datetime.datetime.utcnow()
            if self.sun_rise_set is None:
                # Real code
                self.sun_rise_set = self._sun_tracker.get_sun_rise_set()
            shutdown_time = self.sun_rise_set.sunset + datetime.timedelta(minutes=self._my_site.power_off)
            if now >= shutdown_time:
                return 0
            else:
                result = (shutdown_time - now)
                result = result.total_seconds() / 60
                return result
        else:
            return twenty_four_hours

    def check_power_levels(self):
        """
        Check cell modem power scenarios, like "battery low enough to warn users",
        "battery low enough to preemptively power down the cell modem", etc.
        """
        # Is this feature even enabled?
        if not self._my_site.enable_low_power_shutdown:
            return

        # Is it configured sensibly?
        if self._my_site.cell_modem_cuton_voltage <= self._my_site.cell_modem_cutoff_voltage:
            return

        # Do we have the necessary facts to make a decision?
        # (This is a system startup issue - we have to poll the NCCB for this data)
        nccb = self._asset_manager.get_serial_asset()
        if nccb is None:
            return

        if (nccb.battery_current is None) or (nccb.battery_voltage is None):
            return

        if (nccb.solar_current is None) or (nccb.solar_voltage is None):
            return

        # We only have to check the power source we are currently using...
        # For example, we don't care the battery is currently low if we
        # are running from the solar panel(s).
        if nccb.battery_current > 0:
            compared_voltage = nccb.battery_voltage
        elif nccb.solar_current > 0:
            compared_voltage = nccb.solar_voltage
        else:
            compared_voltage = 24.0  # default to "a voltage that would be OK" (AUX power cannot be measured today)

        #
        # Entire controller (this is the most drastic measure)
        #

        # Is the power level starting to be of concern?
        if compared_voltage <= self._my_site.gateway_warning_voltage:
            new_status = True
        else:
            new_status = False
        if self.gateway_power_low != new_status:
            self.gateway_power_low = new_status
            if self.gateway_power_low:
                logging.getLogger('EventLog').log(logging.CRITICAL, "Power is low enough the entire controller may power-down soon", extra=EventType.NETWORK_CONTROLLER._dict)
            else:
                logging.getLogger('EventLog').log(logging.INFO, "Power sufficient for controller operation", extra=EventType.NETWORK_CONTROLLER._dict)

        # Is the power level so low we need to turn off EVERYTHING?
        if compared_voltage <= self._my_site.gateway_cutoff_voltage:
            if not self._low_power_shutdown_needed:
                self.minimum_awake_countdown = 10
                self._low_power_shutdown_needed = True  # Existing code in check_for_nightly_shutdown() will take it from here...
                logging.getLogger('EventLog').log(logging.CRITICAL, "Power low enough controller is powering itself down!", extra=EventType.NETWORK_CONTROLLER._dict)

        #
        # Cell Modem as a sub-system (it can be independently powered down)
        #

        # Is the power level starting to be of concern?
        if compared_voltage <= self._my_site.cell_modem_warning_voltage:
            new_status = True
        else:
            new_status = False
        if self.cell_modem_power_low != new_status:
            self.cell_modem_power_low = new_status
            if self.cell_modem_power_low:
                logging.getLogger('EventLog').log(logging.CRITICAL, "Power is low enough cell modem may have to be powered down soon", extra=EventType.NETWORK_CONTROLLER._dict)
            else:
                logging.getLogger('EventLog').log(logging.INFO, "Power sufficient for cell modem operation", extra=EventType.NETWORK_CONTROLLER._dict)

        # We are yanking/restoring the power on a cell modem that is really a linux computer.
        # This inhibitor has been put in place so we at least don't do it a LOT
        if self._cell_modem_thrash_inhibitor > 0:
            self._cell_modem_thrash_inhibitor -= 1
            if self._cell_modem_thrash_inhibitor > 0:
                return

        if not self.cell_modem_powered_off:
            # Is the power level so low we need to turn off the cell modem?
            if compared_voltage <= self._my_site.cell_modem_cutoff_voltage:
                new_status = True
            else:
                new_status = False
            if self.cell_modem_powered_off != new_status:
                if self._cloud_updater is not None:
                    self._cloud_updater.flush_cloud_updates()
                    # Notice we are allowing time for final reports to go out
                    self.request_modem_shutdown(5)
                else:
                    self.request_modem_shutdown(0)
                self._cell_modem_thrash_inhibitor = 60
                self.cell_modem_powered_off = new_status
                logging.getLogger('EventLog').log(logging.CRITICAL, "Power low enough cell modem has been powered down", extra=EventType.NETWORK_CONTROLLER._dict)
        else:  # cell modem is currently powered down
            # Has the power level improved enough we can turn the cell modem back on?
            if compared_voltage > self._my_site.cell_modem_cuton_voltage:
                new_status = False
            else:
                new_status = True
            if self.cell_modem_powered_off != new_status:
                self._nccb_monitor.send_modem_on()
                self._cell_modem_thrash_inhibitor = 60
                self.cell_modem_powered_off = new_status
                logging.getLogger('EventLog').log(logging.INFO, "Power recovered enough to power up cell modem", extra=EventType.NETWORK_CONTROLLER._dict)

    # Migrated over from NCCBMonitor who originally performed Nightly Shutdown
    def check_for_nightly_shutdown(self):
        if self.minimum_awake_countdown > 0:
            self.minimum_awake_countdown -= 1
        if (self._my_site.enable_nightly_shutdown or self._low_power_shutdown_needed) and (self.minimum_awake_countdown == 0):
            now = datetime.datetime.utcnow()
            local_time = self._sun_tracker.get_local_time()
            if self.sun_rise_set is None:
                # Real code
                if datetime.datetime.utcnow().date().day > local_time.date().day:
                    self.sun_rise_set = self._sun_tracker.get_sun_rise_set(datetime.datetime.utcnow() - datetime.timedelta(days=1))
                elif datetime.datetime.utcnow().date().day < local_time.date().day:
                    self.sun_rise_set = self._sun_tracker.get_sun_rise_set(datetime.datetime.utcnow() + datetime.timedelta(days=1))
                else:
                    self.sun_rise_set = self._sun_tracker.get_sun_rise_set()
                # Test code
                # class FakeObj1:
                #     pass
                # self.sun_rise_set = FakeObj1()
                # self.sun_rise_set.sunset = now + datetime.timedelta(minutes=1)
                # end Test code
            after_sunset = self.sun_rise_set.sunset + datetime.timedelta(minutes=self._my_site.power_off)
            if (now > after_sunset) or self._low_power_shutdown_needed:
                # Real code
                next_sun_rise_set = self._sun_tracker.get_sun_rise_set(self.sun_rise_set.sunrise + datetime.timedelta(days=1))
                if self.sun_rise_set.sunrise.date().day == next_sun_rise_set.sunrise.date().day:
                    next_sun_rise_set = self._sun_tracker.get_sun_rise_set(self.sun_rise_set.sunrise + datetime.timedelta(days=2))
                # Test code
                # class FakeObj2:
                #     pass
                # next_sun_rise_set = FakeObj2()
                # next_sun_rise_set.sunrise = now + datetime.timedelta(minutes=10)
                # end Test code
                seconds_till_sunrise = int(round((next_sun_rise_set.sunrise - now).total_seconds()))
                before_sunset = seconds_till_sunrise - (self._my_site.power_on * 60)  # power_on is in minutes, we need seconds
                self._power_on_seconds = min(0xFFFF - self.CELL_MODEM_POWER_ON_DELAY, before_sunset)
                # In case if time calculation went wrong system should not go in negative value
                if self._power_on_seconds <= 0:
                    self._power_on_seconds = (self._power_on_seconds * -1) + 300
                if self._cloud_updater is not None:
                    self._cloud_updater.on_solar_info_update(next_sun_rise_set.sunrise, next_sun_rise_set.sunset, self._power_on_seconds)
                    
                log.critical("Powering down system to conserve battery per user settings. Scheduled to turn back on in %i seconds" % (self._power_on_seconds))

                self._nccb_monitor.ensure_final_serial_transmits()
                # TODO The following two commands could be accomplished with one command
                # Notice we are delaying the E12 power up a little to give the modem time to crank up
                self._nccb_monitor.send_schedule_gateway_power(self.GATEWAY_POWER_OFF_DELAY, self._power_on_seconds + self.CELL_MODEM_POWER_ON_DELAY)
                self._nccb_monitor.send_schedule_modem_power(self.GATEWAY_POWER_OFF_DELAY, self._power_on_seconds)

                # We used to simply halt here, but decided to give cloud updates a change to make it out first...
                self.request_system_halt(5)
        else:
            # Always clear when off so we don't have stale sunrise and sunset
            self.sun_rise_set = None

    def periodic_poll(self):
        """Perform system-level processing (like power management)"""
        # There is a very brief period at startup where we don't have everything we need yet
        if self._my_site is None:
            return
        if self._sun_tracker is None:
            return
        if self._asset_manager is None:
            return
        if self._nccb_monitor is None:
            return

        self.check_power_levels()
        self.check_for_nightly_shutdown()

    def _attempt_program_exit(self):
        """Gracefully exit the program"""

        # Future TODO point
        # In the near future we expect more logic to get added here
        # For example, we might need to notify the Cloud Backend that
        # we are about to go away.

        raise SystemExit("The program is deliberately shutting itself down")

    def request_program_exit(self, delay_seconds=0):
        if delay_seconds == 0:
            self._attempt_program_exit()
        else:
            self._schedule_func(delay_seconds, self._attempt_program_exit)

    def _attempt_system_halt(self):
        """Callback function used by request_system_halt()"""
        gateway_utils.halt()

        raise SystemExit("Powering down system to conserve battery per user settings. Scheduled to turn back on in %i seconds" % (self._power_on_seconds, ))

    def request_system_halt(self, delay_seconds=0):
        """Halt the system, possibly after a delay"""
        if delay_seconds == 0:
            self._attempt_system_halt()
        else:
            # Try to get a few final words out...
            if self._cloud_updater is not None:
                self._cloud_updater.flush_cloud_updates()
            # Make sure WE don't do any more processing...
            self._periodic_poll_timer.stop()
            self._periodic_poll_timer = None
            # Allow those final cloud updates time to get out...
            self._schedule_func(delay_seconds, self._attempt_system_halt)

    def _wifi_shutdown_callback(self):
        """Callback function used by request_wifi_shutdown()"""
        if self._wifi_shutdown_timer is not None:
            self._wifi_shutdown_timer.stop()  # This scheduler defaults to REPEATED events...
            self._wifi_shutdown_timer = None
        gateway_utils.shutdown_wifi()

    def request_wifi_shutdown(self, delay_seconds=0):
        """Gracefully shut down the WiFi (so it can be unplugged)"""
        if delay_seconds == 0:
            gateway_utils.shutdown_wifi()
        else:
            # VERY important to hang onto the PeriodocCallBack returned by _schedule_func!
            # These timers default to "auto-repeat" and must be explicitly turned off when
            # you want a "one-shot" behavior (like in this case).
            self._wifi_shutdown_timer = self._schedule_func(delay_seconds, self._wifi_shutdown_callback)

    def _modem_shutdown_callback(self):
        """Callback function used by request_modem_shutdown()"""
        if self._modem_shutdown_timer is not None:
            self._modem_shutdown_timer.stop()  # This scheduler defaults to REPEATED events...
            self._modem_shutdown_timer = None
        self._nccb_monitor.send_modem_off()

    def request_modem_shutdown(self, delay_seconds=0):
        """Shut down the modem after a delay to allow final reports to go out"""
        if delay_seconds == 0:
            self._modem_shutdown_callback()
        else:
            # VERY important to hang onto the PeriodocCallBack returned by _schedule_func!
            # These timers default to "auto-repeat" and must be explicitly turned off when
            # you want a "one-shot" behavior (like in this case).
            self._modem_shutdown_timer = self._schedule_func(delay_seconds, self._modem_shutdown_callback)

    # This is a pretty severe measure to take, but we are seeing sporadic cell modem failures
    # out in the field.
    def request_modem_power_cycle(self):
        self._nccb_monitor.send_schedule_modem_power(10, 40)
