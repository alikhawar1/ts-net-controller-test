"""
DumpUpdateSettings.py - used to check configuration of running units
"""

import logging
import os

from utils.SystemSettings import *
from Scheduler import Scheduler
from AssetUpdater import AssetUpdater

# logging.basicConfig(level=logging.WARNING)
# logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.DEBUG)

class DummyObject(object):
    pass


asset_updater = AssetUpdater(DummyObject(), Scheduler.schedule_func, DummyObject(), DummyObject(), '', '')

#
# Network Controller Bridge Script
#
print "asset_updater.configured_bridge_script_name = " + asset_updater.configured_bridge_script_name
print "asset_updater.configured_bridge_script_crc = " + "0x%04x" % (asset_updater.configured_bridge_script_crc,)

#
# BRIDGE Radio Firmware (SNAP Firmware, not STM32 Firmware!)
#
print "asset_updater.configured_bridge_radio_firmware_version = " + asset_updater.configured_bridge_radio_firmware_version
print "asset_updater.configured_bridge_radio_firmware_filename = " + asset_updater.configured_bridge_radio_firmware_filename

#
# Asset script (works in TCs and WXs)
#

print "asset_updater.configured_asset_script_name = " + asset_updater.configured_asset_script_name
print "asset_updater.configured_asset_script_crc = " + "0x%04x" % (asset_updater.configured_asset_script_crc,)

#
# Radio Firmware (SNAP Firmware, not STM32 Firmware!)
#
print "asset_updater.configured_asset_radio_firmware_version = " + asset_updater.configured_asset_radio_firmware_version
print "asset_updater.configured_asset_radio_firmware_filename = " + asset_updater.configured_asset_radio_firmware_filename

#
# Asset Firmware (works in TCs and WXs)
#
print "asset_updater.configured_asset_stm32_firmware_version = " + asset_updater.configured_asset_stm32_firmware_version
print "asset_updater.configured_asset_stm32_firmware_filename = " + asset_updater.configured_asset_stm32_firmware_filename

print "asset_updater.do_upload_scripts = " + str(asset_updater.do_upload_scripts)
print "asset_updater.do_upload_bridge_script = " + str(asset_updater.do_upload_bridge_script)
print "asset_updater.do_upload_bridge_firmware = " + str(asset_updater.do_upload_bridge_firmware)
print "asset_updater.do_upload_asset_scripts = " + str(asset_updater.do_upload_asset_scripts)
print "asset_updater.do_upload_asset_radio_firmware = " + str(asset_updater.do_upload_asset_radio_firmware)
print "asset_updater.do_upload_asset_stm32_firmware = " + str(asset_updater.do_upload_asset_stm32_firmware)
print "asset_updater.do_upload_asset_configs = " + str(asset_updater.do_upload_asset_configs)
