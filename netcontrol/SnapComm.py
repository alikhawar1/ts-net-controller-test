"""
SnapCom.py - The purpose of this module is to create and
initialize the SNAP Connect instance that we will be
using for communication with the remote Assets (TC, WX)
"""


import sys
import logging
from apy import ioloop_scheduler
import monotonic
import tornado.ioloop

from snapconnect import snap
import time

# We default to setting up to run on a SNAP Connect Gateway (E10/E20/E12/etc.)
DEFAULT_SERIAL_TYPE = snap.SERIAL_TYPE_RS232
# DEFAULT_SERIAL_PORT = '/dev/ttyS1'  # E10 gateway
DEFAULT_SERIAL_PORT = '/dev/snap1'  # E12 gateway

log = logging.getLogger(__name__)

def dummy_poll_function():
    return None

class SnapComm(object):
    """
    Creating a SNAP Connect instance is trivial, but this class also
    takes care of programming the various settings.
    """
    def __init__(self,
                 serial_conn=DEFAULT_SERIAL_TYPE,
                 serial_port=DEFAULT_SERIAL_PORT):
        """
        Constructor - comm settings have useful defaults,
        but can be overriden too
        """
        SNAPCONNECT_POLL_INTERVAL = 1  # ms

        # Install monotonic timer for Tornado
        tornado.ioloop.IOLoop.configure(tornado.ioloop.IOLoop.configured_class(), time_func=monotonic.monotonic)

        #if sys.platform != 'win32':
        #    snap.use_tornado_ioloop = True

        # SNAP Connect requires any scheduler provided to it have a poll() method
        # Create one if necessary
        scheduler = ioloop_scheduler.IOLoopScheduler.instance()
        poll_method = getattr(scheduler, 'poll', None)
        if (poll_method is None) or not callable(poll_method):
            scheduler.poll = dummy_poll_function

        self.sc = snap.Snap(funcs={}, scheduler=scheduler)

        # Tell the Tornado scheduler to call SNAP Connect's internal poll function.
        #tornado.ioloop.PeriodicCallback(self.sc.poll_internals, SNAPCONNECT_POLL_INTERVAL).start()
        #tornado.ioloop.PeriodicCallback(self.sc.poll, SNAPCONNECT_POLL_INTERVAL).start()
        tornado.ioloop.PeriodicCallback(self.my_poll, SNAPCONNECT_POLL_INTERVAL).start()

        # Enable second RPC CRC
        self.sc.save_nv_param(snap.NV_FEATURE_BITS_ID, 0x0100)
        # TODO turn on PACKET_CRC too
        # TODO at some point we want to enable encryption

        # The following settings are needed by the AssetPoller,
        # but I wanted all the config done in one place. So, here.
        self.sc.save_nv_param(snap.NV_GROUP_INTEREST_MASK_ID, 0x5501)
        self.sc.save_nv_param(snap.NV_GROUP_FORWARDING_MASK_ID, 0xAA01)

        if serial_conn is not None and serial_port is not None:
            self.sc.open_serial(serial_conn, serial_port)
        else:
            log.error("DEVELOPMENT MODE - No SNAPconnect serial connection")

        self.sc.accept_tcp()


    def my_poll(self):
        if not self.sc.poll():
            time.sleep(0.001)


    @staticmethod
    # SNAP Nodes that do not have a script loaded are also missing the lookup table
    # that enables "function lookup by name-string". Instead they can only invoke
    # functions by their "numerical code object". This routine bypasses this restriction.
    def FunctionCode(str):
        temp = snap.RpcCodec.RpcFunc(str)
        if temp is not None:
            if temp.code is not None:
                return temp
        return str  # could not perform the conversion, they keep what they had...


if __name__ == "__main__":

    # logging.basicConfig(level=logging.WARNING)
    # logging.basicConfig(level=logging.INFO)
    logging.basicConfig(level=logging.DEBUG)

    snapcomm = SnapComm()

    io_loop = tornado.ioloop.IOLoop.current()
    io_loop.start()
