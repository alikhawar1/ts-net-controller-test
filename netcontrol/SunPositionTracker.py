"""
SunPositionTracker - The job of this module is to periodically broadcast
"Sun Position Reports" which the TCs use to help them decide where to "aim"
the solar panel racks.
"""

import logging
import datetime
import math
from collections import namedtuple
import timezonefinderL, pytz

import pvlib
# pvlib creates a root logger when you import it's module :(
# remove the last logger it added
logging.getLogger().removeHandler(logging.getLogger().handlers[-1])

import pandas as pd


log = logging.getLogger(__name__)

SunriseSunset = namedtuple('SunriseSunset', ['sunrise', 'sunset'])


class SunPositionTracker(object):
    """
    The job of this class is to periodically create Sun Position Reports.
    """
    # The real constants
    FORECASTS_PER_BROADCAST = 15  # As of 08/21/2017 this CAN be increased to a max of 30
    # The following works out to data for up to an hour, sent 4 times per hour
    FORECAST_SPACING = 4 * 60  # in seconds, sun moves approx. 1 degree every 4 minutes

    # The constants when I am desk-checking the math using the debugger
    # FORECASTS_PER_BROADCAST = 70
    # FORECAST_SPACING = 15 * 60  # in seconds, so quarter hour

    def __init__(self, gps_monitor, my_site):
        """
        Constructor
        :param gps_monitor: GPS instance to get actual lat/lon/altitude from
        :param my_site: Site configuration to get fallback lat/lon/altitude from
        """
        if gps_monitor is None:
            raise Exception("Error: SunPositionTracker requires a GpsMonitor!")
        else:
            self._gps_monitor = gps_monitor

        self.my_site = my_site

    def compute_sun_angle(self, azimuth, elevation):
        """Convert from azimuth and elevation to sun_angle of 180 (due East) to 0 (due West)"""

        # The math in this next stretch of code comes from "Derivation of the solar
        # geometric relationships using vector analysis" by Alistair B. Sproul.
        # Equation (13) page 1194 - Polar to Cartesian

        # Convert from degrees to radians for use with the standard math library
        azimuth_rads = math.radians(azimuth)
        elevation_rads = math.radians(elevation)

        sun_east = math.cos(elevation_rads) * math.sin(azimuth_rads)
        sun_z = math.sin(elevation_rads)
        # sun_north = math.cos(elevation_rads) * math.cos(azimuth_rads)  # only computed for print statement below
        # print "*** sun x,y,z=" + str(sun_east) + ", " + str(sun_north) + ", " + str(sun_z)

        # We "virtually project" the above point onto the x (East/West) / z plane by ignoring
        # the y (North/South) component. The remainder of this routine is plane geometry, not spherical

        # Compute the length of the hypotenuse
        hypotenuse_squared = sun_east * sun_east + sun_z * sun_z
        hypotenuse = math.sqrt(hypotenuse_squared)

        # Compute the sine of the angle based on the triangle definition
        sin_sun_angle = sun_z / hypotenuse

        # From that we can compute the angle we seek. Note that here we get "90 down to 0 back up to 90"
        sun_angle_rads = math.asin(sin_sun_angle)
        sun_angle = math.degrees(sun_angle_rads)

        # We want a coordinate system like the following:
        # Imagine facing due south, and holding a protractor in front of you.
        # If the sun was flat on the western horizon, you would read 0 degrees.
        # Straight overhead would be 90 degrees. Flat on the eastern horizon
        # would read 180 degrees.
        if azimuth <= 180.0:  # sun is in the East
            sun_angle = 180.0 - sun_angle

        return sun_angle

    @property
    def current_angle(self):
        """
        Returns the angle the panel should currently be at if tracking
        :return: The current panel angle
        :rtype: float
        """
        return self.forecast(datetime.datetime.utcnow(), self._gps_monitor.latitude, self._gps_monitor.longitude, self._gps_monitor.altitude)

    def get_sun_rise_set(self, dt=None, latitude=None, longitude=None):
        """
        Calculates the sunrise and sunset
        :param datetime.datetime dt: The moment in time to use for the calculation (defaults to now)
        :param float latitude: The GPS latitude to use for the calculation (defaults to GPS)
        :param float longitude: The GPS longitude to use for the calculation (defaults to GPS)
        :return: The sunrise and sunset
        :rtype: SunriseSunset
        """
        dt = dt or datetime.datetime.utcnow()
        latitude = latitude or self._gps_monitor.latitude
        longitude = longitude or self._gps_monitor.longitude

        result = pvlib.solarposition.get_sun_rise_set_transit(dt, latitude, longitude)

        return SunriseSunset(sunrise=result.sunrise[0].to_pydatetime(),
                             sunset=result.sunset[0].to_pydatetime())

    def forecast(self, dt, latitude, longitude, altitude):
        """
        Calculates a forecasted sun angle for one moment in time
        :param datetime.datetime dt: The moment in time to use for the calculation
        :param float latitude: The GPS latitude to use for the calculation
        :param float longitude: The GPS longitude to use for the calculation
        :param float altitude: The GPS altitude (in meters) to use for the calculation
        :return: The forecasted sun angle
        :rtype: float
        """
        # pvlib wants pandas-style time, not time.time... convert!
        pandas_time = pd.to_datetime(dt)
        # print "pandas_time=" + str(pandas_time)

        result = pvlib.solarposition.spa_python(pandas_time, latitude, longitude, altitude)
        # print result
        # print result.azimuth
        # print result.apparent_elevation

        # There are some interesting design decisions in the pvlib code...
        # The fields that you might THINK are the numerical results are
        # actually smart objects, not just data. When you want JUST the
        # numbers (like we do here) you have to bypass the obvious (looking)
        # fields and go for the values "array of arrays""
        values_array = result.values[0]  # we only asked for one computation
        azimuth = values_array[2]
        elevation = values_array[0]  # Note that this is apparent elevation, not actual
        # print "azimuth = " + str(azimuth) + " elevation = " + str(elevation)

        # We don't care about the sun when it is below the horizon
        if elevation < 0.0:
            # 03/25/2019 update - we DO care to know if it is East versus West
            if azimuth >= 180.0: # Sproul page 1197: 180 degrees up to 360 is WEST
                sun_angle = 0.0 # this is our WEST
            else:
                sun_angle = 180.0 # this is our EAST
        else:
            sun_angle = self.compute_sun_angle(azimuth, elevation)

        # print "***** computed sun angle = " + str(sun_angle)
        return sun_angle

    def get_forecast(self, forecast_time):
        """
        Updates the binary string of forecasted sun positions
        :param datetime.datetime forecast_time: The moment to use as the starting point for the forecast
        :return: None
        """
        count = SunPositionTracker.FORECASTS_PER_BROADCAST
        forecast_string = ''
        while count > 0:
            angle = self.forecast(forecast_time, self._gps_monitor.latitude, self._gps_monitor.longitude, self._gps_monitor.altitude)
            # print angle
            # Convert into the format we need for the actual broadcast
            angle *= 10.0
            angle += 0.5
            angle = int(angle)
            # Append to the string, MSB first
            msb = (angle >> 8) & 0xFF
            lsb = angle & 0xFF
            forecast_string += chr(msb)
            forecast_string += chr(lsb)

            forecast_time += datetime.timedelta(seconds=SunPositionTracker.FORECAST_SPACING)
            count -= 1

        return forecast_string

    def get_local_time(self):
        """
        Calculates the local time using gps
        """
        latitude = self._gps_monitor.latitude
        longitude = self._gps_monitor.longitude

        tf = timezonefinderL.TimezoneFinder()
        timezone_str = tf.timezone_at(lat=latitude, lng=longitude)
        tz = pytz.timezone(timezone_str)
        local_time = datetime.datetime.now(tz)

        return local_time

#
# Make it so running module standalone results in something
#
if __name__ == "__main__":
    # Python 2 doesn't include UTC by default, but has example code:
    # https://docs.python.org/2/library/datetime.html#tzinfo-objects
    from datetime import tzinfo, timedelta
    ZERO = timedelta(0)
    class UTC(tzinfo):
        """UTC"""
        def utcoffset(self, dt):
            return ZERO
        def tzname(self, dt):
            return "UTC"
        def dst(self, dt):
            return ZERO

    # logging.basicConfig(level=logging.WARNING)
    # logging.basicConfig(level=logging.INFO)
    logging.basicConfig(level=logging.DEBUG)

    class FakeGpsMonitor:
        def __init__(self):
            self.latitude = 34.675581666
            self.longitude = -86.749965
            self.altitude = 0

    class FakeSite:
        def __init__(self):
            self.gps_lat = 34.675581666
            self.gps_lng = -86.749965
            self.gps_alt = 0

    sun_position_tracker = SunPositionTracker(FakeGpsMonitor(), FakeSite())

    # Test driving the algorithm under the debugger (or using print statements)
    # Choose a fixed moment in time a few hours before sunrise
    # We can then manually look at the forecast string and see the positions change
    moment = datetime.datetime(2017, 07, 11, 10, 40, 00, tzinfo=UTC())
    sun_position_tracker.get_forecast(moment)

    # Choose a fixed moment in time near the middle of the day
    moment = datetime.datetime(2017, 07, 11, 17, 40, 00, tzinfo=UTC())
    sun_position_tracker.get_forecast(moment)

    # Choose a fixed moment in time a few hours before sunset
    moment = datetime.datetime(2017, 07, 12, 00, 40, 00, tzinfo=UTC())
    sun_position_tracker.get_forecast(moment)

    # Later in the project we added "overnight shutdown", which caused us to
    # care about sunrise and sunset. The following was used to exercise the
    # sunrise/sunset calculation
    sunrise_sunset = sun_position_tracker.get_sun_rise_set(latitude=34.675581666, longitude=-86.749965)

    # These are in UTC
    print sunrise_sunset.sunrise
    print sunrise_sunset.sunset

    # Avoiding manual math. I am currently in CDT timezone, which is 5 hours earlier
    delta = datetime.timedelta(hours=5)
    print sunrise_sunset.sunrise - delta
    print sunrise_sunset.sunset - delta
