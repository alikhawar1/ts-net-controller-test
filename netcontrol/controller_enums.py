from enum import Enum


class TCStatusBits(Enum):
    """
    Tracker Controller Status Bits
    """
    OFFLINE =          0b00000001
    OVER_CURRENT =     0b00000010
    MOTOR_TIMEOUT =    0b00000100
    CHARGER_FAULT =    0b00001000
    MOTOR_CURRENT_LIMIT = 0b00010000
    BATTERY_LOW_MOVEMENT_RESTRICTED = 0b00100000  # Low enough to have triggered an auto-stow, or recharging afterwards
    MOTOR_TEMPERATURE_LIMIT = 0b01000000  # this original bit was for "too cold". See also TCTrackingStatus bit 0x40
    # Originally only NCCBs could set this bit, now Row Controllers can too.
    # It remains to be seen if Weather Stations will gain these buttons...
    ESTOP_IS_PRESSED = 0b10000000
    # The above marks the end of bits that exist in the REAL status byte
    # For UI purposes, we are duplicating some date between fields (see Asset.py)
    HIGH_TEMPERATURE_MOTOR_CUTOFF = 0x100 # This comes from the Row Box as tracking_status 0x40

    @property
    def description(self):
        if self == TCStatusBits.OFFLINE:
            return "NC has been declared offline by TC"
        if self == TCStatusBits.OVER_CURRENT:
            return "Motor Fault - over-current"
        if self == TCStatusBits.MOTOR_TIMEOUT:
            return "Motor Fault - timeout"
        if self == TCStatusBits.CHARGER_FAULT:
            return "Charger Fault"
        if self == TCStatusBits.MOTOR_CURRENT_LIMIT:
            return "Motor Current Limit Exceeded"
        if self == TCStatusBits.BATTERY_LOW_MOVEMENT_RESTRICTED:
            return "Battery Low - rack movement restricted"
        if self == TCStatusBits.MOTOR_TEMPERATURE_LIMIT:
            return "Motor Temperature Limit Exceeded"
        if self == TCStatusBits.ESTOP_IS_PRESSED:
            return "Emergency stop button is depressed"
        # The above marks the end of bits that exist in the REAL status byte
        # For UI purposes, we are duplicating some date between fields (see Asset.py)
        if self == TCStatusBits.HIGH_TEMPERATURE_MOTOR_CUTOFF:
            return "Motor shutdown due to high temperature"


class TCTrackingStatusBits(Enum):
    """
    Tracker Controller Tracking Status
    """
    MANUAL = 0
    AUTO_TRACK_TRACKING = 1
    AUTO_TRACK_BACKTRACKING = 3 # This is bit 0x01 + bit 0x02
    LOW_BATTERY_AUTO_STOW_INITIATED = 4
    LOCAL_ESTOP = 5  # This is synthesized, look at property tracking_status in Asset.py
    INDIVIDUAL_ROW_CONTROL = 8
    GROUP_ROW_CONTROL = 0x10
    HANDHELD_ROW_CONTROL = 0x20
    HIGH_TEMPERATURE_MOTOR_CUTOFF = 0x40
    ONE_BIT_STILL_TBD = 0x80


    @property
    def description(self):
        if self == TCTrackingStatusBits.MANUAL:
            return "Manual Control"  # IOW, NOT tracking
        if self == TCTrackingStatusBits.AUTO_TRACK_TRACKING:
            return "Auto-Tracking - Tracking"
        if self == TCTrackingStatusBits.AUTO_TRACK_BACKTRACKING:
            return "Auto-Tracking - Back-tracking"
        if self == TCTrackingStatusBits.LOW_BATTERY_AUTO_STOW_INITIATED:
            return "Auto-stow due to low battery"
        if self == TCTrackingStatusBits.HIGH_TEMPERATURE_MOTOR_CUTOFF:
            return "Motor shutdown due to high temperature"
        if self == TCTrackingStatusBits.LOCAL_ESTOP:
            return "Local Emergency Stop"
        if self == TCTrackingStatusBits.HANDHELD_ROW_CONTROL:
            return "Manual Row Control (HHC)"
        if self == TCTrackingStatusBits.INDIVIDUAL_ROW_CONTROL:
            return "Individual Row Control"
        if self == TCTrackingStatusBits.GROUP_ROW_CONTROL:
            return "Group Row Control"


class SensorConfigMode(Enum):
    """
    Tracker Controller Sensor ADC Mode
    """
    DISABLED = 0
    TEN_VOLT = 1
    FIVE_VOLT = 2
    FOUR_TWENTY_MA = 3

    @property
    def description(self):
        if self == SensorConfigMode.DISABLED:
            return "Disabled"
        if self == SensorConfigMode.TEN_VOLT:
            return "10 V"
        if self == SensorConfigMode.FIVE_VOLT:
            return "5 V"
        if self == SensorConfigMode.FOUR_TWENTY_MA:
            return "4-20 mA"


class SensorConfigFlags(Enum):
    """
    Tracker Controller Sensor Flags
    """
    EXTERNAL_POWER = 0b00000001
    ENABLE_DIN =     0b00000010

    @property
    def description(self):
        if self == SensorConfigFlags.EXTERNAL_POWER:
            return "Turn on External Sensor Power"
        if self == SensorConfigFlags.ENABLE_DIN:
            return "Enable Dry Contact Input"


class TCDeviceType(Enum):
    TC = 0
    NCCB = 1

    @property
    def description(self):
        if self == TCDeviceType.TC:
            return "Tracker Controller"
        if self == TCDeviceType.NCCB:
            return "Network Controller Companion Board"


class TrackerPresets(Enum):
    """
    Tracker Controller Preset Angles
    """
    STOW = 0  # intended to be the safest angle for panels in high winds
    FLAT = 1  # level / approx. horizontal / approx. 0.0 degrees
    CLEAN = 2  # intended to make the panels easier to reach for cleaning
    MOW = 3  # intended to make it easier to get underneath the panels
    RAIN_EAST = 4  # intended to maximize the cleaning effect of rainwater runoff
    RAIN_WEST = 5  # like RAIN_EAST but facing the opposite direction
    MIN_CW = 6  # places the panels at the minimum allowed angle
    MAX_CW = 7  # places the panels at the maximum allowed angle
    # STOW_NIGHT was split out from the original STOW on 03/05/2019 so that we could prefer
    # stowing to the East overnight but still be able to STOW *NEAREST* in an emergency
    STOW_NIGHT = 8

    @property
    def description(self):
        if self == TrackerPresets.STOW:
            return "Stow"
        if self == TrackerPresets.FLAT:
            return "Flat"
        if self == TrackerPresets.CLEAN:
            return "Clean"
        if self == TrackerPresets.MOW:
            return "Mowing"
        if self == TrackerPresets.RAIN_EAST:
            return "Rain East"
        if self == TrackerPresets.RAIN_WEST:
            return "Rain West"
        if self == TrackerPresets.MIN_CW:
            return "Minimum Angle"
        if self == TrackerPresets.MAX_CW:
            return "Maximum Angle"
        if self == TrackerPresets.STOW_NIGHT:
            return "Stow Night"

    def __str__(self):
        return self.description


class EventType(Enum):
    """
    Types of events reported to cloud.
    """
    SITE_OVERVIEW      = 0
    NETWORK_CONTROLLER = 1
    INDIVIDUAL_ASSET   = 2
    ACCESS_CONTROL     = 3
    FIRMWARE_UPDATE    = 4
    UNKNOWN            = 5

    @property
    def description(self):
        if self == EventType.SITE_OVERVIEW:
            return "Site Overview"
        if self == EventType.NETWORK_CONTROLLER:
            return "Network Controller Events"
        if self == EventType.INDIVIDUAL_ASSET:
            return "Individual Asset Events"
        if self == EventType.ACCESS_CONTROL:
            return "Access Control Events"
        if self == EventType.FIRMWARE_UPDATE:
            return "Firmware Updates"
        if self == EventType.UNKNOWN:
            return "Unknown"

    @property
    def _dict(self):
        return {'type': self}
    
class CommandSource(Enum):
    """
    Source which originated the command.
    """
    NC      = 0
    FCS     = 1
    MC      = 2
    UNKNOWN = 3

    @property
    def description(self):
        if self == CommandSource.NC:
            return "Network Controller"
        if self == CommandSource.FCS:
            return "Field Commissioning Software"
        if self == CommandSource.MC:
            return "Manual Control"
        if self == CommandSource.UNKNOWN:
            return "Unknown"


class CommandedRowState(Enum):
    PANEL_CMD_NONE = 0
    PANEL_CMD_V_ESTOP = 1
    PANEL_CMD_PAUSE = 2
    PANEL_CMD_MOVE_TO_ABS = 3
    PANEL_CMD_MOVE_BY_DELTA = 4
    PANEL_CMD_MOVE_TO_PRESET = 5
    PANEL_CMD_LOCAL_ESTOP = 6
    PANEL_CMD_NCCB_STOP = 7
    PANEL_CMD_HHC_STOP = 8
    PANEL_CMD_CONFIG_CHANGE_STOP = 9

    @property
    def description(self):
        if self == CommandedRowState.PANEL_CMD_NONE:
            return "Normal"
        if self == CommandedRowState.PANEL_CMD_V_ESTOP:
            return "Virtual Emergency Stop"
        if self == CommandedRowState.PANEL_CMD_PAUSE:
            return "Pause"
        if self == CommandedRowState.PANEL_CMD_MOVE_TO_ABS:
            return "Specific Angle"
        if self == CommandedRowState.PANEL_CMD_MOVE_BY_DELTA:
            return "Move By Delta"
        if self == CommandedRowState.PANEL_CMD_MOVE_TO_PRESET:
            return "Preset Angle"
        if self == CommandedRowState.PANEL_CMD_LOCAL_ESTOP:
            return "Local Emergency Stop"
        if self == CommandedRowState.PANEL_CMD_NCCB_STOP:
            return "NCCB Emergency Stop"
        if self == CommandedRowState.PANEL_CMD_HHC_STOP:
            return "HHC Stop"
        if self == CommandedRowState.PANEL_CMD_CONFIG_CHANGE_STOP:
            return "Configuration Change Stop"

    def __str__(self):
        return self.description

class CommandStatus(Enum):
    NEW = 0
    REQUESTED = 1
    ACKNOWLEDGED = 2
    COMPLETED = 3
    FAILED = 4
    UNKNOWN = 5
    DONE = 6
    
    @property
    def description(self):
        if self == CommandStatus.NEW:
            return "New Command Received"
        if self == CommandStatus.REQUESTED:
            return "Command Requested"
        if self == CommandStatus.ACKNOWLEDGED:
            return "Command Acknowledged"
        if self == CommandStatus.COMPLETED:
            return "Command Completed"
        if self == CommandStatus.FAILED:
            return "Command Failed"
        if self == CommandStatus.DONE:
            return "Done"
        if self == CommandStatus.UNKNOWN:
            return "Unknown"

    def __str__(self):
        return self.description

class CommandErrorCodes(Enum):
    CLEAR = 0
    PRESET_NOT_CONFIGURED = 1
    PRESET_NOT_AVAILABLE_YET = 2
    ASSET_OFFLINE = 3
    LOCAL_ESTOP_ENGAGED = 4
    NCCB_ESTOP_ENGAGED = 5
    UNDER_HHC_CONTROL = 6
    
    @property
    def description(self):
        if self == CommandErrorCodes.CLEAR:
            return "No Error"
        if self == CommandErrorCodes.PRESET_NOT_CONFIGURED:
            return "Preset Not Configured"
        if self == CommandErrorCodes.PRESET_NOT_AVAILABLE_YET:
            return "Preset angle not available yet"
        if self == CommandStatus.ASSET_OFFLINE:
            return "Asset Offline"
        if self == CommandStatus.ASSET_IN_LOCAL_ESTOP:
            return "Asset in Local ESTOP"
        if self == CommandStatus.NCCB_ESTOP_ENGAGED:
            return "NC ESTOP Engaged"
        if self == CommandStatus.UNDER_HHC_CONTROL:
            return "Asset is in HHC mode"

    def __str__(self):
        return self.description