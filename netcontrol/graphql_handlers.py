import datetime
import binascii
import logging
import os
import time
from controller_enums import EventType

from graphene.types.datetime import DateTime

from utils.SystemSettings import SYSTEM_CONFIG_DIRECTORY
from utils.SystemSettings import NC_UPGRADE_FILENAME, COMPOSITE_NC_CONFIG_FILENAME

from nc_update_manager import WATCH_FILE_DIRECTORY as NC_WATCH_FILE_DIRECTORY

# Save original version we are about to override
graphene_DateTime_serialize = DateTime.serialize

@staticmethod
def serialize_DateTime(dt):
    if isinstance(dt, float) or isinstance(dt, int):
        dt = datetime.datetime.utcfromtimestamp(dt)
    return graphene_DateTime_serialize(dt)

# Override built-in version to support floats
DateTime.serialize = serialize_DateTime

event_log = logging.getLogger('EventLog')


class GraphQLHandler(object):
    def __init__(self, schema,
                 my_site,
                 event_log_handler,
                 asset_mgr,
                 gps_mon,
                 modem_mon,
                 bridge_mon,
                 asset_mon,
                 weather_mon,
                 estop_handler,
                 asset_commander,
                 sun_tracker,
                 mqtt_client,
                 cloud_updater,
                 ntp_monitor,
                 system_manager,
                 asset_mover,
                 log_retriever,
                 asset_updater,
                 asset_broadcaster,
                 ethernet):
        """
        Connects the GraphQL resolvers to various managers in the system
        """
        self.event_log_handler = event_log_handler
        self.asset_mgr = asset_mgr
        self.my_site = my_site
        self.gps_mon = gps_mon
        self.modem_mon = modem_mon
        self.bridge_mon = bridge_mon
        self.asset_mon = asset_mon
        self.weather_mon = weather_mon
        self.estop_handler = estop_handler
        self.asset_commander = asset_commander
        self.sun_tracker = sun_tracker
        self.mqtt_client = mqtt_client
        self.cloud_updater = cloud_updater
        self.ntp_monitor = ntp_monitor
        self.system_manager = system_manager
        self.asset_mover = asset_mover
        self.log_retriever = log_retriever
        self.asset_updater = asset_updater
        self.asset_broadcaster = asset_broadcaster
        self.ethernet = ethernet

        # Replace default resolvers with our own
        schema.get_query_type().fields['config'].resolver = lambda *args: self.my_site
        schema.get_query_type().fields['systemStatus'].resolver = lambda *args: self.system_manager
        schema.get_query_type().fields['eventLog'].resolver = lambda *args: {}
        schema.get_query_type().fields['eventLog'].type.fields['eventLogEntries'].resolver = self.event_log_entries_resolver
        schema.get_query_type().fields['eventLog'].type.fields['entriesCount'].resolver = self.event_log_entries_count
        schema.get_query_type().fields['assets'].resolver = self.assets_resolver
        schema.get_query_type().fields['assets'].type.of_type.fields['configTimestamp'].resolver = self.assets_config_timestamp_resolver
        schema.get_mutation_type().fields['clearLogEntries'].resolver = self.clear_event_log_entry
        schema.get_mutation_type().fields['updateConfig'].resolver = self.update_site_config_resolver
        schema.get_query_type().fields['discoveredAssets'].resolver = self.discovered_assets_resolver
        schema.get_mutation_type().fields['activateDiscoveredAsset'].resolver = self.activate_discovered_asset_resolver
        schema.get_mutation_type().fields['deactivateActiveAsset'].resolver = self.deactivate_active_asset_resolver
        schema.get_mutation_type().fields['deleteDiscoveredAsset'].resolver = self.delete_discovered_asset_resolver
        schema.get_mutation_type().fields['getLogFromAsset'].resolver = self.get_log_from_asset_resolver
        schema.get_mutation_type().fields['clearFaults'].resolver = self.clear_faults_resolver
        schema.get_mutation_type().fields['forceUpload'].resolver = self.force_upload_resolver
        schema.get_query_type().fields['gps'].resolver = lambda *args: self.gps_mon
        schema.get_query_type().fields['cellModem'].resolver = lambda *args: self.modem_mon
        schema.get_query_type().fields['bridge'].resolver = lambda *args: self.bridge_mon
        schema.get_mutation_type().fields['updateBridgeConfig'].resolver = self.update_bridge_config
        schema.get_query_type().fields['mesh'].resolver = lambda *args: self.asset_mon
        schema.get_query_type().fields['ethernet'].resolver = lambda *args: self.ethernet
        schema.get_mutation_type().fields['updateEthernetConfig'].resolver = self.update_ethernet_config
        schema.get_query_type().fields['assetUpdater'].resolver = lambda *args: self.asset_updater
        schema.get_mutation_type().fields['updateAssetUpdaterConfig'].resolver = self.update_asset_updater_config
        schema.get_query_type().fields['weatherMonitor'].resolver = lambda *args: self.weather_mon
        schema.get_mutation_type().fields['updateWeatherMonitorConfig'].resolver = self.update_weather_monitor_config
        schema.get_mutation_type().fields['cancelSnowStow'].resolver = self.cancel_snow_stow
        schema.get_query_type().fields['emergencyStop'].resolver = lambda *args: self.estop_handler
        schema.get_mutation_type().fields['emergencyStop'].resolver = self.start_estop_test
        schema.get_query_type().fields['tracking'].resolver = lambda *args: {}
        schema.get_query_type().fields['tracking'].type.fields['calcPanelAngle'].resolver = lambda *args: self.sun_tracker.current_angle
        schema.get_query_type().fields['tracking'].type.fields['commandedState'].resolver = lambda *args: self.asset_commander.panel_state.value
        schema.get_mutation_type().fields['debug'].resolver = self.debug_resolver
        schema.get_mutation_type().fields['upgrade'].resolver = self.upgrade_resolver
        schema.get_mutation_type().fields['importConfig'].resolver = self.import_config_resolver
        schema.get_query_type().fields['cloud'].resolver = lambda *args: self.cloud_updater
        schema.get_query_type().fields['cloud'].type.fields['enabled'].resolver = lambda *args: self.cloud_updater.enabled
        schema.get_query_type().fields['cloud'].type.fields['connected'].resolver = lambda *args: self.mqtt_client.connected
        schema.get_query_type().fields['cloud'].type.fields['lastPublish'].resolver = lambda *args: self.mqtt_client.last_publish
        schema.get_query_type().fields['cloud'].type.fields['lastPublishResponse'].resolver = lambda *args: self.mqtt_client.last_publish_response
        schema.get_mutation_type().fields['updateCloudConfig'].resolver = self.update_cloud_config
        schema.get_query_type().fields['ntpinfo'].resolver = lambda *args: self.ntp_monitor
        schema.get_mutation_type().fields['shutDownWifi'].resolver = self.shut_down_wifi_resolver
        #####schema.get_query_type().fields['mover'].resolver = lambda *args: self.asset_mover

    def clear_event_log_entry(self, root, args, context, info):
        cleared_records = self.event_log_handler.set_record_cleared_flag(args['ids'], args.get('cleared', True))
        if cleared_records:
            self.cloud_updater.on_cleared_records(cleared_records)
            # We want have this log message have the same exact timestamp as when the records got cleared
            # To do this we need to manually create and add the record
            fn, lno, func = event_log.findCaller()
            record = event_log.makeRecord(event_log.name, logging.INFO, fn, lno, "User %s cleared event log entries" % context['current_user'], (), None, func, None)
            record.created = cleared_records[0].cleared - time.timezone  # The memory event log handler is about to add the timezone offset
            event_log.handle(record)

        return {
            'event_log_entries': cleared_records
        }

    def event_log_entries_count(self, root, args, context, info):
        levelno = args.get('levelno', logging.CRITICAL)
        count_cleared = args.get('countCleared', False)
        return self.event_log_handler.get_record_count(levelno, count_cleared)

    def event_log_entries_resolver(self, root, args, context, info):
        levelno = args.get('levelno', self.event_log_handler.level)
        cleared = args.get('cleared', None)
        created = 0
        if 'created' in args:
            # created arg comes in marked as UTC, even if it's not, so take off the timezone info
            created = (args['created'].replace(tzinfo=None) - datetime.datetime(1970, 1, 1)).total_seconds()

        records = [record
                   for record in self.event_log_handler.get_events()
                   if (record.levelno >= levelno and
                       record.created >= created and
                       (cleared is None or
                        ((not cleared and record.cleared is None) or
                         (cleared and record.cleared is not None))
                        ))]

        return records

    def assets_resolver(self, root, args, context, info):
        if 'id' in args:
            if args['id'] in self.asset_mgr.assets:
                return [self.asset_mgr.assets[args['id']]]
            return []
        if 'has_weather_sensor' in args:
            has_weather_sensor = args.get('has_weather_sensor')
            return [asset for asset in self.asset_mgr.assets.itervalues() if asset.has_weather_sensor == has_weather_sensor]

        return self.asset_mgr.assets.itervalues()

    @staticmethod
    def assets_config_timestamp_resolver(root, args, context, info):
        if root.config_timestamp:
            try:
                return datetime.datetime.fromtimestamp(root.config_timestamp / 1000)
            except ValueError:
                return datetime.datetime.utcfromtimestamp(0)

    def update_site_config_resolver(self, root, args, context, info):
        if 'siteImageFile' in args:
            self.my_site.replace_site_image(args['siteImageFile'], save_now=False)
        if 'configData' in args:
            self.my_site.update(args['configData'], save_now=False)
        if 'engineeringSpec' in args:
            self.my_site.replace_eng_spec_file(args['engineeringSpec'], save_now=False)
        self.my_site.save() # This is why we used save_now=False up above...
        return self.my_site

    def upgrade_resolver(self, root, args, context, info):
        received = False
        if 'upgradeFile' in args:
            got_file = args['upgradeFile']['filename']
            if NC_UPGRADE_FILENAME in got_file and os.path.splitext(got_file)[1] == ".whl":
                file_directory = NC_WATCH_FILE_DIRECTORY
                logging.getLogger('EventLog').log(logging.INFO, "Got NC Firmware upgrade file from SLUI" , extra=EventType.FIRMWARE_UPDATE._dict)
                with open(file_directory +"/"+ got_file, 'wb') as file_f:
                     file_f.write(args['upgradeFile']['body'])
                received = True
            else:
                logging.getLogger('EventLog').log(logging.INFO, "Got invalid NC Firmware upgrade file from SLUI" , extra=EventType.FIRMWARE_UPDATE._dict)

        return {'received': received}

    def import_config_resolver(self, root, args, context, info):
        received = False

        if 'configFile' in args:
            filename = args['configFile']['filename']
            config_file = os.path.join(SYSTEM_CONFIG_DIRECTORY, filename)
            if os.path.exists(config_file):
                os.remove(config_file)

            with open(config_file, 'wb') as f:
                f.write(args['configFile']['body'])
            received = self.my_site.digest_composite_config(filename)

        return {'received': received}

    def discovered_assets_resolver(self, root, args, context, info):
        if 'id' in args:
            if args['id'] in self.asset_mgr.discovered_assets:
                return [self.asset_mgr.discovered_assets[args['id']]]
            return []

        return self.asset_mgr.discovered_assets.itervalues()

    def activate_discovered_asset_resolver(self, root, args, context, info):
        asset_ids = self.asset_mgr.move_discovered_assets(args['ids'])
        return {'activated': asset_ids}

    def deactivate_active_asset_resolver(self, root, args, context, info):
        asset_ids = self.asset_mgr.move_back_to_discovered(args['ids'])
        return {'deactivated': asset_ids}

    def delete_discovered_asset_resolver(self, root, args, context, info):
        asset_ids = self.asset_mgr.delete_discovered_assets(args['ids'])
        return {'deleted': asset_ids}

    def get_log_from_asset_resolver(self, root, args, context, info):
        asset_ids = self.log_retriever.retrieve_logs(args['ids'])
        return {'requested': asset_ids}

    def clear_faults_resolver(self, root, args, context, info):
        asset_ids = self.asset_broadcaster.request_clear_faults(args['ids'])
        return {'requested': asset_ids}

    def force_upload_resolver(self, root, args, context, info):
        asset_ids = self.asset_updater.force_upload(args['ids'], args['uploadType'])
        return {'requested': asset_ids}

    def start_estop_test(self, root, args, context, info):
        self.estop_handler.start_estop_test(args.get('estopTestStart', 60))
        return {'emergency_stop': self.estop_handler}

    def update_bridge_config(self, root, args, context, info):
        # This used to just change the contents of a database
        # self.bridge_mon.update_config(args['configData'])
        # Now it actually triggers a multi-step process
        self.asset_mover.update_config(args['configData'])
        # TODO Will have to revisit the line below in light of the above changes
        return {"bridge": self.bridge_mon}

    def update_ethernet_config(self, root, args, context, info):
        self.ethernet.update_config(args['configData'])
        return {"ethernet": self.ethernet}

    def update_asset_updater_config(self, root, args, context, info):
        changes = False
        if 'bridgeScriptFile' in args:
            self.asset_updater.replace_bridge_script(args['bridgeScriptFile'], save_now=False)
            changes = True
        if 'bridgeFirmwareFile' in args:
            self.asset_updater.replace_bridge_firmware(args['bridgeFirmwareFile'], save_now=False)
            changes = True
        if 'assetScriptFile' in args:
            self.asset_updater.replace_asset_script(args['assetScriptFile'], save_now=False)
            changes = True
        if 'assetRadioFirmwareFile' in args:
            self.asset_updater.replace_asset_radio_firmware(args['assetRadioFirmwareFile'], save_now=False)
            changes = True
        if 'assetStm32FirmwareFile' in args:
            self.asset_updater.replace_asset_stm32_firmware(args['assetStm32FirmwareFile'], save_now=False)
            changes = True
        if 'configData' in args:
            self.asset_updater.update_config(args['configData'], save_now=False)
            changes = True
        if changes:
            self.asset_updater.save_config()
        return {"asset_updater": self.asset_updater}

    def update_weather_monitor_config(self, root, args, context, info):
        self.weather_mon.update_config(args['configData'])
        return {"weather_monitor": self.weather_mon}

    def update_cloud_config(self, root, args, context, info):
        self.cloud_updater.update_config(args['configData'])
        return {"cloud": self.cloud_updater}

    def cancel_snow_stow(self, root, args, context, info):
        result = self.weather_mon.cancel_snow_stow(context['current_user'])
        # For now, making a single GUI field clear BOTH types of stow...
        result |= self.weather_mon.cancel_panel_snow_stow(context['current_user'])
        return {'cancelled' : result}

    def shut_down_wifi_resolver(self, root, args, context, info):
        self.system_manager.request_wifi_shutdown(2)
        return {'shutdown': True}

    def debug_resolver(self, root, args, context, info):
        if 'asset_response' in args:
            self.asset_mon._snap_connect.packet_dispatcher.remote_addr = binascii.unhexlify(args['asset_response']['addr'])
            self.asset_mon.report(args['asset_response'].get('response', ''))
        if 'update_asset' in args:
            """
            # Example:
            mutation tester {
              debug(updateAsset: {addr: "ffdc36", params: [{name: "has_weather_sensor", intValue: 1}, {name: "peak_wind_speed", intValue: 0}]}) {
                success
              }
            }
            """
            for param in args['update_asset']['params']:
                setattr(self.asset_mgr.assets[args['update_asset']['addr']], param['name'],  param.get('str_value', None) or param.get('int_value', None))

        return {"success": True}
