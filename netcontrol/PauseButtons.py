"""
PauseButtons.py - This module was originally written to arbitrate wireless
communications between multiple objects, but could conceivably be used
for other purposes.
"""

class PauseButton(object):
    """
    By taking one of these you are agreeing to play by the rules of the
    PauseButtonManager:
    1) You will often check to see if you have been paused, and cease activity
    as soon as you see that you have been (try to be quick about it!)
    2) If you need to take priority, you will attempt_to_pause_others(),
    but you will check the return value before assuming you have control.
    IF YOU DO NOT GET CONTROL, KEEP QUIET!
    3) If you ever succeed in pausing others, you will complete your task as
    quickly as possible, and you will unpause_others() as soon as you can.
    THIS WHOLE SCHEME BREAKS DOWN IF SOMEONE KEEPS CONTROL FOREVER!!!
    """

    def __init__(self, parent, priority):
        """Just a constructor"""
        self._parent = parent
        self._priority = priority

        self.paused = False

    def attempt_to_pause_others(self):
        """
        Call this if you need to take precedence/priority.
        Note that you might not get it!
        """
        return self._parent._attempt_to_pause_others(self._priority)

    def unpause_others(self):
        """
        Call this to give up the priority/precedence previously obtained via
        a call to attempt_to_pause_others()
        """
        return self._parent._unpause_others(self._priority)


class PauseButtonManager(object):
    """
    If you want to "play nice with others", you request a PauseButton from this
    module and you keep it and pay attention to it going forward.

    If you need to take priority, you call the pauseOthers() function in that object
    (versus pressing your OWN pause button), but note that even then you have to wait
    for the requested pauses to take effect  - you may not be the highest priority thing
    going on at the moment.

    Also note that the programmer is responsible for assigning the priorities of the
    participating objects, with 0 being the highest priority and 1,2,3... each
    representing ever lower priorities.
    """

    MAX_PAUSIBLES = 12  # because there is a memory-allocation aspect to this
    # the above is also used as a sentinel value throughout this code

    __default_instance = None

    @staticmethod
    def get_default_instance():
        """Most code only needs one of these and so can use this one (inspired by tornado)"""
        if PauseButtonManager.__default_instance is not None:
            return PauseButtonManager.__default_instance
        else:
            PauseButtonManager.__default_instance = PauseButtonManager()
            return PauseButtonManager.__default_instance

    def __init__(self):
        self._pausibles = []
        # index and therefor priority of the holder IF THERE IS ONE
        self._pause_holder = PauseButtonManager.MAX_PAUSIBLES

    def get_pause_button(self, requested_priority):
        """Call this to get your very own PauseButton (factory method)"""
        if requested_priority < 0:
            return None

        if requested_priority >= PauseButtonManager.MAX_PAUSIBLES:
            return None

        # Handle the requests coming in out of order, or a few gaps
        while len(self._pausibles) <= requested_priority:
            self._pausibles.append(None)

        if self._pausibles[requested_priority] is not None:
            return None

        self._pausibles[requested_priority] = PauseButton(self, requested_priority)
        return self._pausibles[requested_priority]

    def give_up_pause_button(self, pause_button):
        """Added this in case an object needs to go away"""
        priority = pause_button._priority
        if priority >= len(self._pausibles):
            return
        if self._pausibles[priority] != pause_button:
            return

        if self._pause_holder == priority:
            self._unpause_others(priority)

        self._pausibles[priority] = None

    def _attempt_to_pause_others(self, priority):
        """Client code is supposed to go through the PauseButton object!"""
        if self._pause_holder < priority:
            return False
        else:
            self._pause_holder = priority
            while priority < len(self._pausibles):
                if self._pausibles[priority] != None:
                    self._pausibles[priority].paused = True
                priority += 1
            return True

    def _unpause_others(self, priority):
        """Client code is supposed to go through the PauseButton object!"""
        if self._pause_holder != priority:
            return False
        else:
            self._pause_holder = PauseButtonManager.MAX_PAUSIBLES
            while priority < len(self._pausibles):
                if self._pausibles[priority] != None:
                    self._pausibles[priority].paused = False
                priority += 1
            return True
