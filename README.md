# README - TerraSmart Network Controller #

You will need to clone the ts-common repo into ts_common at the root of this repo
You will also need to clone the pyfiria repo into pyfiria at the root of this repo (see https://bitbucket.org/firia/pyfiria)

If you want to configure you own logging levels you can create a file called `logging_config.py` in your config directory.
The format should be like
```python
import logging

logging.getLogger().setLevel(logging.DEBUG)

logging.getLogger('ModemMonitor').setLevel(logging.WARN)
logging.getLogger('GpsMonitor').setLevel(logging.WARN)
logging.getLogger('snap').setLevel(logging.INFO)
logging.getLogger('snaplib').setLevel(logging.INFO)
logging.getLogger('NetworkControllerDecoders').setLevel(logging.INFO)
logging.getLogger('AssetDiscovery').setLevel(logging.WARN)
logging.getLogger('AssetPoller').setLevel(logging.WARN)
logging.getLogger('tornado').setLevel(logging.INFO)
```


### Manufacturing ###
Moved to the `ts-manufacturing` repo.

### CLI Options ###
okay! While developing if you don't want the SLUI to serve HTTPS and automatically redirect, you can set the command line HTTPS port option to `--tls_port=0`.
Also if you prefer having HTTP listen on the normal development port you can do `--http_port=8888`

If don't have a serial connection to a NCCB you can set the `--nccb_port=None` argument.

To run without a SNAPconnect serial connection you can set the `--sc_port_type=None --sc_port=None` arguments

The AWS IoT MQTT client can be disabled with the `--disable_mqtt` argument