import os
import sys
import glob
import hashlib
import csv
import zipfile

ROWS_FILENAME = 'rows.csv'
MANIFEST_FILENAME = 'manifest'
ENGINEERING_SPEC_FILENAME = 'engineering.spec'

# Here are the fields of each row of the CSV file
CSV_SNAP_ADDRESS_INDEX = 0
CSV_CONFIG_LABEL_INDEX = 1
CSV_BLT_FILENAME_INDEX = 2
# The "config angles"...
# Note that index here refers to their step in the overall process
# In the actual "config angles" table, these are indices 0, 1, and 2
CSV_HORIZONTAL_OFFSET_ANGLE_INDEX = 3
CSV_MINIMUM_OFFSET_ANGLE_INDEX = 4
CSV_MAXIMUM_OFFSET_ANGLE_INDEX = 5
# PRESETS come from a separate file (instead of adding 32 (!) columns to this file)
CSV_PRESETS_FILENAME_INDEX = 6
CSV_INSTALL_FLAGS_INDEX = 7
CSV_WIND_DIR_OFFSET_INDEX = 8
CSV_SNOW_SENSOR_HEIGHT_INDEX = 9
CSV_SITE_LABEL_INDEX = 10
CSV_LATITUDE_INDEX = 11
CSV_LONGITUDE_INDEX = 12
CSV_LOCATION_INDEX = 13
CSV_BAP_FILENAME_INDEX = 14
# More will be added over time, and MUST go at the END of this list!
CSV_COLUMNS_COUNT = 15 # <- keep this up to date!


def main():
    extra_files = []
    missing_files = []

    prog = sys.argv[0]
    prog = os.path.split(prog)[1]
    if len(sys.argv) != 2:
        print "Usage: " + prog + ' <source directory>'
        exit()

    source_dir = sys.argv[1]
    if not os.path.exists(source_dir):
        print "The specified source directory " + source_dir + " does not exist, aborting."
        exit()

    original_cwd = os.getcwd()
    os.chdir(source_dir)

    try:
        with open(ROWS_FILENAME, 'rb') as csv_file:
            csv_reader = csv.reader(csv_file)
            for row_data in csv_reader:
                if len(row_data) >= CSV_COLUMNS_COUNT:
                    # Make sure referenced files are preset
                    blt_file = row_data[CSV_BLT_FILENAME_INDEX]
                    if os.path.isfile(blt_file):
                        if blt_file not in extra_files:
                            extra_files.append(blt_file)
                            # TODO Validate contents of BLT file
                    else:
                        if blt_file not in missing_files:
                            missing_files.append(blt_file)
                    presets_file = row_data[CSV_PRESETS_FILENAME_INDEX]
                    if os.path.isfile(presets_file):
                        if presets_file not in extra_files:
                            extra_files.append(presets_file)
                            # TODO Validate contents of PRESETS file
                    else:
                        if presets_file not in missing_files:
                            missing_files.append(presets_file)
                    bap_file = row_data[CSV_BAP_FILENAME_INDEX]
                    if os.path.isfile(bap_file):
                        if bap_file not in extra_files:
                            extra_files.append(bap_file)
                            # TODO Validate contents of BAP file
                    else:
                        if bap_file not in missing_files:
                            missing_files.append(bap_file)

        if missing_files != []:
            print "The following files are referenced by the " + ROWS_FILENAME + " file but are missing:"
            print missing_files
            print "Please correct as-needed and re-run this program."
            exit()

    except Exception, e:
        os.chdir(original_cwd)
        print "Invalid or missing " + ROWS_FILENAME + " file, aborting."
        exit()

    # Make a new manifest file
    filelist = [ROWS_FILENAME] + extra_files
    try:
        with open(MANIFEST_FILENAME, 'wb') as manifest_file:
            for filename in filelist:
                try:
                    with open(filename, 'rb') as f:
                        hash_md5 = hashlib.md5()
                        for chunk in iter(lambda: f.read(4096), b""):
                            hash_md5.update(chunk)
                        manifest_file.write(filename + ',' + hash_md5.hexdigest() + '\n')
                except Exception, e:
                    os.chdir(original_cwd)
                    print "Error reading " + filename + " file, aborting."
                    exit()

    except Exception, e:
        os.chdir(original_cwd)
        print "Error creating " + MANIFEST_FILENAME + " file, aborting."
        exit()

    # Now make the actual Engineering Specification file
    with zipfile.ZipFile(ENGINEERING_SPEC_FILENAME, 'w') as zf:
        filelist = [MANIFEST_FILENAME] + filelist
        for filename in filelist:
            zf.write(filename, filename)

    os.chdir(original_cwd)

    final_msg = "Engineering Specification file has been created and written to "
    final_msg += ENGINEERING_SPEC_FILENAME
    final_msg += " in the " + source_dir + " directory."
    print final_msg

if __name__ == '__main__':
    main()
