"""
Crude helper program to extract portions of the master "worksheet" file
into the format needed by build_spec_file.py.

To use:
Export the "SpecfileWork" tab of a master worksheet into a .csv file

For now - edit this program to control what rows get extracted
TODO - add a nice CLI to this program.

Run as python ingest_worksheet.py filename.csv

Manually inspect the output files for sanity.
"""

import os
import sys
import csv
import math

SNAP_ADDRESS_HEADER_TEXT = "Snap Address"
ROW_NUMBER_HEADER_TEXT = "Row Number"
LOCATION_HEADER_TEXT = "Location Text"
PANEL_WIDTH_HEADER_TEXT = "Panel Array Width\n (CM)"
#SPACING_EAST_HEADER_TEXT = "Spacing East\n (CM)"
#SPACING_WEST_HEADER_TEXT = "Spacing West\n (CM)"
#DELTA_HEIGHT_EAST_HEADER_TEXT = "Delta Height East\n (CM)"
#DELTA_HEIGHT_WEST_HEADER_TEXT = "Delta Height West\n (CM)"
#SPACING_EAST2_HEADER_TEXT = "Spacing East2\n (CM)"
#SPACING_WEST2_HEADER_TEXT = "Spacing West2\n (CM)"
#DELTA_HEIGHT_EAST2_HEADER_TEXT = "Delta Height East2\n (CM)"
#DELTA_HEIGHT_WEST2_HEADER_TEXT = "Delta Height West2\n (CM)"
# Spreadsheet headers got changed around the end of February
SPACING_EAST_HEADER_TEXT = "North Spacing East\n (CM)"
SPACING_WEST_HEADER_TEXT = "North Spacing West\n (CM)"
DELTA_HEIGHT_EAST_HEADER_TEXT = "North Delta Height East\n (CM)"
DELTA_HEIGHT_WEST_HEADER_TEXT = "North Delta Height West\n (CM)"
SPACING_EAST2_HEADER_TEXT = "South Spacing East\n (CM)"
SPACING_WEST2_HEADER_TEXT = "South Spacing West\n (CM)"
DELTA_HEIGHT_EAST2_HEADER_TEXT = "South Delta Height East\n (CM)"
DELTA_HEIGHT_WEST2_HEADER_TEXT = "South Delta Height West\n (CM)"

column_dictionary = {}

def fatal_error(msg):
    print msg
    exit()

def enforce_column(column_name):
    if column_name not in column_dictionary:
        fatal_error(column_name + "column is missing (or has been renamed)")

def process_header(row_data):
    global column_dictionary

    index = 0
    count = len(row_data)
    while index < count:
        column_dictionary[row_data[index]] = index
        index += 1
    print column_dictionary

    # Sanity check (will expand this as our usage of the worksheet data expands
    enforce_column(SNAP_ADDRESS_HEADER_TEXT)
    enforce_column(ROW_NUMBER_HEADER_TEXT)
    enforce_column(PANEL_WIDTH_HEADER_TEXT)
    enforce_column(SPACING_EAST_HEADER_TEXT)
    enforce_column(SPACING_WEST_HEADER_TEXT)
    enforce_column(DELTA_HEIGHT_EAST_HEADER_TEXT)
    enforce_column(DELTA_HEIGHT_WEST_HEADER_TEXT)
    enforce_column(SPACING_EAST2_HEADER_TEXT)
    enforce_column(SPACING_WEST2_HEADER_TEXT)
    enforce_column(DELTA_HEIGHT_EAST2_HEADER_TEXT)
    enforce_column(DELTA_HEIGHT_WEST2_HEADER_TEXT)
    enforce_column(LOCATION_HEADER_TEXT)

def passes_filter(row_data):
    # For the moment, filtering data by row number
    row_number = row_data[column_dictionary[ROW_NUMBER_HEADER_TEXT]]
    row_number = int(row_number)
    # This is the Rio Arriba build-out, which was done piece-meal
    #if row_number == 63:
    #    return True
    #if (row_number >= 65) and (row_number <= 81):
    #    return True
    #if (row_number >= 32) and (row_number <= 36):
    #    return True
    #if (row_number >= 37) and (row_number <= 38):
    #    return True
    #if row_number == 64:
    #    return True
    #if row_number in [41,42,43,44,45,46,49,51,52,53,55,56,57,58,61,62]:
    #    return True
    # Deliberate test of re-processing some rows. 
    #if (row_number >= 32) and (row_number <= 62):
    #    return True
    #if row_number in [5,6,10,11,13,14,17,18,21,23]:
    #    return True
    #if row_number in [15,19,24,25,27,28]:
    #    return True
    #if row_number in [16,22,26]:
    #    return True
    #if row_number in [2,3,7,12,20,29,30,31]:
    #    return True
    #if row_number in [9]:
    #    return True
    #if row_number in [1,4,8,20]:
    #    return True
    # On 03/02/2020 we actually had to re-process ALL rows
    #if (row_number >= 1) and (row_number <= 100):
    #    return True
    # On 03/05/2020 we had to re-process rows 32-62 due to new survey data
    #if (row_number >= 32) and (row_number <= 62):
    #    return True
    # On 04/07/2020 we actually had to re-process ALL rows
    #if (row_number >= 1) and (row_number <= 100):
    #    return True
    # Initial White Street work
    #if (row_number >= 1) and (row_number <= 152):
    #    return True
    # Initial Batavia work
    #if (row_number >= 88) and (row_number <= 94):
    #    return True
    # Initial Lime Light work
    #if (row_number >= 1) and (row_number <= 144):
    #    return True
    # Initial Mount Morris work
    #if (row_number >= 1) and (row_number <= 95):
    #    return True
    return True # False

def float_to_int_round_up(string_value):
    value = float(string_value)
    value = math.ceil(value)
    value = int(value)
    value = str(value)
    return value

def float_to_int_round_down(string_value):
    value = float(string_value)
    value = math.floor(value)
    value = int(value)
    value = str(value)
    return value

def float_to_int_round_closest(string_value):
    value = float(string_value)
    value = round(value, 0)
    value = int(value)
    value = str(value)
    return value

def create_bap_file(row_data):
    snap_address = row_data[column_dictionary[SNAP_ADDRESS_HEADER_TEXT]]
    snap_address = snap_address.replace('.', '')
    snap_address = snap_address.replace(':', '')
    snap_address = snap_address.lower()

    width = row_data[column_dictionary[PANEL_WIDTH_HEADER_TEXT]]
    # tweaking panel width
    width = float(width)
    width += 30.0 # (30 cm) - increasing by about a foot
    width = str(width)
    # end tweaking panel width
    width = float_to_int_round_up(width)

    spacing_east1 = row_data[column_dictionary[SPACING_EAST_HEADER_TEXT]]
    # tweaking spacing
    adjustment = 60.0 # 30.0 # cm
    spacing_east1 = float(spacing_east1)
    if spacing_east1 > adjustment:
        spacing_east1 -= adjustment
    spacing_east1 = str(spacing_east1)
    # end tweaking spacing
    spacing_east1 = float_to_int_round_down(spacing_east1)

    spacing_west1 = row_data[column_dictionary[SPACING_WEST_HEADER_TEXT]]
    # tweaking spacing
    spacing_west1 = float(spacing_west1)
    if spacing_west1 > adjustment:
        spacing_west1 -= adjustment
    spacing_west1 = str(spacing_west1)
    # end tweaking spacing
    spacing_west1 = float_to_int_round_down(spacing_west1)

    delta_height_east1 = row_data[column_dictionary[DELTA_HEIGHT_EAST_HEADER_TEXT]]
    delta_height_east1 = float_to_int_round_closest(delta_height_east1)

    delta_height_west1 = row_data[column_dictionary[DELTA_HEIGHT_WEST_HEADER_TEXT]]
    delta_height_west1 = float_to_int_round_closest(delta_height_west1)

    spacing_east2 = row_data[column_dictionary[SPACING_EAST2_HEADER_TEXT]]
    # tweaking spacing
    spacing_east2 = float(spacing_east2)
    if spacing_east2 > adjustment:
        spacing_east2 -= adjustment
    spacing_east2 = str(spacing_east2)
    # end tweaking spacing
    spacing_east2 = float_to_int_round_down(spacing_east2)

    spacing_west2 = row_data[column_dictionary[SPACING_WEST2_HEADER_TEXT]]
    # tweaking spacing
    spacing_west2 = float(spacing_west2)
    if spacing_west2 > adjustment:
        spacing_west2 -= adjustment
    spacing_west2 = str(spacing_west2)
    # end tweaking spacing
    spacing_west2 = float_to_int_round_down(spacing_west2)

    delta_height_east2 = row_data[column_dictionary[DELTA_HEIGHT_EAST2_HEADER_TEXT]]
    # Survive missing data
    if delta_height_east2 == '':
        delta_height_east2 = '0'
    delta_height_east2 = float_to_int_round_closest(delta_height_east2)

    delta_height_west2 = row_data[column_dictionary[DELTA_HEIGHT_WEST2_HEADER_TEXT]]
    # Survive missing data
    if delta_height_west2 == '':
        delta_height_west2 = '0'
    delta_height_west2 = float_to_int_round_closest(delta_height_west2)

    bap_filename = "bap" + snap_address + ".csv"

    try:
        with open(bap_filename, 'w') as bap_file:
            bap_file.write("0," + width + "," + spacing_east1 + "," + spacing_west1 + "," + delta_height_east1 + "," + delta_height_west1 + "\n")
            bap_file.write("1," + width + "," + spacing_east2 + "," + spacing_west2 + "," + delta_height_east2 + "," + delta_height_west2 + "\n")
    except Exception, e:
        os.chdir(original_cwd)
        print "Error creating " + bap_filename + " file, aborting."
        exit()

    return (snap_address, bap_filename)

def process_row(row_data):
    # For today, only trying to automate the creation of the bap files
    snap_address, bap_filename = create_bap_file(row_data)
    
    # Also a crude hack to make it easier to update rows.csv
    location_text = row_data[column_dictionary[LOCATION_HEADER_TEXT]]
    location_text = location_text.lstrip()
    location_text = location_text.rstrip()
    
    # White Street had no location_text filled in
    if location_text == '':
        location_text = 'Row ' + str(row_data[column_dictionary[ROW_NUMBER_HEADER_TEXT]])

    #print snap_address + ",Config2,generic.blt,0.0,-60.0,60.0,presets60.csv,0x0081,0.0,0.0,Rio Arriba,34.67522,-86.749888," + location_text + "," + bap_filename
    #print snap_address + ",Config1,generic.blt,0.0,-60.0,60.0,presets60.csv,0x0085,0.0,0.0,Batavia,34.67522,-86.749888," + location_text + "," + bap_filename
    #print snap_address + ",Config8,generic.blt,0.0,-60.0,60.0,presets60.csv,0x0085,0.0,0.0,White Street,34.67522,-86.749888," + location_text + "," + bap_filename
    #print snap_address + ",Config2,generic.blt,0.0,-60.0,60.0,presets60.csv,0x0085,0.0,0.0,Mount Morris,34.67522,-86.749888," + location_text + "," + bap_filename
    #print snap_address + ",Config4,generic.blt,0.0,-60.0,60.0,presets60.csv,0x0085,0.0,0.0,Lime Light,34.67522,-86.749888," + location_text + "," + bap_filename
    #print snap_address + ",Config3,generic.blt,0.0,-60.0,60.0,presets60.csv,0x0085,0.0,0.0,Mount Morris,34.67522,-86.749888," + location_text + "," + bap_filename
    print snap_address + ",Config0,generic.blt,0.0,-60.0,60.0,presets60.csv,0x00C5,0.0,0.0,Batavia,34.67522,-86.749888," + location_text + "," + bap_filename


def main():

    prog = sys.argv[0]
    prog = os.path.split(prog)[1]
    if len(sys.argv) != 3:
        print "Usage: " + prog + ' <source directory> <source file>'
        exit()

    source_dir = sys.argv[1]
    if not os.path.isdir(source_dir):
        print "The specified source directory " + source_dir + " does not exist, aborting."
        exit()

    original_cwd = os.getcwd()
    os.chdir(source_dir)

    source_file = sys.argv[2]
    if not os.path.isfile(source_file):
        print "The specified source file " + source_file + " does not exist, aborting."
        exit()

    try:
        with open(source_file, 'rb') as csv_file:
            csv_reader = csv.reader(csv_file)
            reading_header = True
            for row_data in csv_reader:
                #print row_data
                if reading_header:
                    process_header(row_data)
                    reading_header = False
                elif passes_filter(row_data):
                    process_row(row_data)

    except Exception, e:
        os.chdir(original_cwd)
        print "Invalid or missing " + source_file + " file, aborting."
        exit()


if __name__ == '__main__':
    main()
