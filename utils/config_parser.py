import os
import re

class ConfigFileParser(object):
    """  """
    def __init__(self, file=None):
        self._file = file
        self._config = {}
        if not os.path.exists(file) or self._file is None:
            self._config = None
        else:
            self.read(self._file)

    def read(self, file):
        separator = "="
        keys = {}
        try:
            with open(file, "rb") as f:
                for l in f:
                    if (not re.search("^(#|\.|=)", l)) and (separator in l):
                        name, value = l.split(separator, 1)
                        keys[name.strip()] = value.strip().strip('"')
                self._config = keys
            return keys
        except Exception as e:
            self._config = None
       
    
    def get_config(self):
        return self._config
