"""
PickleHelper.py - this class lets us reduce the amount of duplicated
code in classes using the xxxPersistedConfig paradigm. By containing
one of these helpers and initializing it once, you can avoid having
a lot of near-duplicate save() and load() routines in the codebase.
"""

import cPickle as pickle
import os

from SystemSettings import *


class PickleHelper(object):
    """
    Recurring code parameterized and pushed into a class.
    Instantiate one of these and have it handle save()/load()
    """
    def __init__(self, derived_object, config_class, config_filename):
        """
        Constructor - parameters include:
        derived_object - the object (derived from config_class) that we will be saving data for
        config_class - the xxxPersistableConfig class that defines what is to be persisted
        (we are drawing a distinction between "config" and "runtime" data)
        config_filename - the filename to pickle the config fields to/from
        """
        self._derived_object = derived_object
        self._config_class = config_class
        self._config_filename = config_filename

    def load_config(self):
        if not os.path.exists(SYSTEM_CONFIG_DIRECTORY):
            os.makedirs(SYSTEM_CONFIG_DIRECTORY)
            return False

        path = os.path.join(SYSTEM_CONFIG_DIRECTORY, self._config_filename)
        try:
            pkl_file = open(path, 'rb')
            config_only = pickle.load(pkl_file)
            pkl_file.close()

            for key in config_only.__dict__:
                setattr(self._derived_object, key, getattr(config_only, key))

        except Exception, e:
            return False
        return True

    def save_config(self):
        # I happen to know that load_config() always gets called first,
        # so I don't have to ensure the directory exists here TOO.
        path = os.path.join(SYSTEM_CONFIG_DIRECTORY, self._config_filename)
        try:

            config_only = self._config_class()
            for key in config_only.__dict__:
                setattr(config_only, key, getattr(self._derived_object, key))

            pkl_file = open(path, 'wb')
            # Pickle the list using the highest protocol available.
            pickle.dump(config_only, pkl_file, -1)
            pkl_file.close()
        except Exception, e:
            return False
        return True

    def update_single_entry_config(self, got_key, value):
        path = os.path.join(SYSTEM_CONFIG_DIRECTORY, self._config_filename)
        try:
            pkl_file = open(path, 'rb')
            config_only = pickle.load(pkl_file)
            pkl_file.close()
            for key in config_only.__dict__:
                if key == got_key:
                    setattr(config_only, key, value)
                    break
            pkl_file = open(path, 'wb')
            # Pickle the list using the highest protocol available.
            pickle.dump(config_only, pkl_file, -1)
            pkl_file.close()
        except Exception, e:
            return False
        return True