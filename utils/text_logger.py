"""
TextLogger - Responsible for writing custom log messages in a file
that persist in memory even system reboots.
"""


import os
import datetime
from enum import Enum
from PickleHelper import PickleHelper

class LogLevel(Enum):
    """
    Log levels.
    """
    INFO     = 0
    BEBUG    = 1
    WARNING  = 2
    CRITICAL = 3
    ERROR    = 4

    @property
    def description(self):
        if self == LogLevel.INFO:
            return " [INFO] "
        if self == LogLevel.BEBUG:
            return " [BEBUG] "
        if self == LogLevel.WARNING:
            return " [WARNING] "
        if self == LogLevel.CRITICAL:
            return " [CRITICAL] "
        if self == LogLevel.ERROR:
            return " [ERROR] "

LOG_CONFIG_FILE = 'log.pkl'
BASE_PATH = '/home/netctrl/logs'
MAX_NO_OF_FILES = 3


class LogPersistableConfig(object):
    """  """
    def __init__(self, file_name=None, max_size=None):
        if file_name:
            self.file_name = file_name.split('.')[0]
        else:
            self.file_name = 'nc_logs'
        if not os.path.exists(BASE_PATH):
            os.mkdir(BASE_PATH)
        self.log_file = os.path.join(BASE_PATH, self.file_name + '.log')
        self._file_number = 0
        self._level = LogLevel.BEBUG
        if max_size:
            self.max_size = max_size
        else:
            self.max_size = 1 # max log file size in MB

class TextLogger(LogPersistableConfig):
    """  """
    def __init__(self, file_name=None, max_size=None):
        LogPersistableConfig.__init__(self, file_name)
        self._pickle_helper = PickleHelper(self, LogPersistableConfig, LOG_CONFIG_FILE)
        if self.load_config():
            if self.file_name not in file_name:
                self.file_name = file_name
                self.save_config()
        else:
            self.save_config()

    def load_config(self):
        return self._pickle_helper.load_config()

    def save_config(self):
        return self._pickle_helper.save_config()

    def update_config(self, new_config_dict):
        """ """
        for key in new_config_dict:
            if hasattr(self, key):
                setattr(self, key, new_config_dict[key])
        self.save_config()

    @property
    def file_size(self):
        """ Returns current log file size in KB """
        if os.path.exists(self.log_file):
            return os.path.getsize(self.log_file) / (1024 * 1024)
        return 0
    
    def set_level(self, level):
        self._level = level
    
    def _normalize_file(self):
        if self._file_number >= MAX_NO_OF_FILES:
            self._file_number = 0
            file_name = self.file_name + '.log'
            self.log_file = os.path.join(BASE_PATH, file_name)
            open(self.log_file, 'w').close()
        else:
            file_name = self.file_name + "_" + str(self._file_number) + '.log'
            self._file_number += 1
            self.log_file = os.path.join(BASE_PATH, file_name)
        self.update_config(self.__dict__)

    def log(self, msg, level=None):
        if self.file_size >= self.max_size:
            self._normalize_file()
        if level:
            level = level.description
        else:
            level = self._level.description
        message = '[' + str(datetime.datetime.now()) + '] ' + level + msg
        try:
            log_file = open(self.log_file, 'a+')
            print >> log_file, message
            log_file.flush()
            log_file.close()
        except Exception as e:
            pass

    def log_mem_stats(self):
        tot_m, used_m, free_m, shared_m, buffer_m, cached_m = map(int, os.popen('free -t -m').readlines()[1].split()[1:])
        msg = "System memory information TOTAL: %i, USED: %i, FREE: %i, SHARED:  %i, BUFFER:  %i, CACHED:  %i" % (tot_m, used_m, free_m, shared_m, buffer_m, cached_m)
        self.log(msg)
    
    
if __name__ == '__main__':
    logger = TextLogger("tes_log.log")
    logger.set_level(LogLevel.BEBUG)
    logger.log("test message")
    logger.log("test message",LogLevel.ERROR)
    # logger.log_mem_stats()