"""
SystemSettings.py - the job of this module is to establish some
"system-wide" settings.
"""

import os

# We have agreed that we will use individual config files, stored
# in a common / shared directory. This default value can get overwritten
# in main.py since the user can supply his own path via command line args
SYSTEM_CONFIG_DIRECTORY = os.path.join(os.path.expanduser('~'),
                                       'terrasmart',
                                       'net-controller')

# Under SYSTEM_CONFIG_DIRECTORY are subdirectories containing different
# categories of files: one for pictures, one for software images (scripts and firmware)
# Not intended to be changed, just defining here so other modules can import.
SITE_IMAGES_SUBDIRECTORY = 'site_images'
SOFTWARE_IMAGES_SUBDIRECTORY = 'script_images'

# The "config" of a Network Controller is actually split across multiple files.
# The composite file referenced below is actually a ZIP file of those individual files.
# It is used solely for backup and restore of entire Network Controllers.
# NOTE - the composite file is only synthesized when the user asks to download it!
COMPOSITE_NC_CONFIG_FILENAME = 'nc.config'

MANIFEST_FILENAME = 'commissioning.manifest'

ENGINEERING_SPEC_FILENAME = 'engineering.spec'

PASSWD_FILENAME = 'passwd'

NC_UPGRADE_FILENAME = 'ts_net_controller'
