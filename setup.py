import os
import subprocess
import shutil

import distutils.cmd

from setuptools import setup

from pip.req import parse_requirements
from pip.download import PipSession

import versioneer

setup_dir = os.path.dirname(os.path.abspath(__file__))
slui_dir = os.path.join(setup_dir, 'slui')


def read_requirements(f):
    install_reqs = parse_requirements(f, session=PipSession())
    reqs = [str(r.req) for r in install_reqs]
    return reqs


class NpmCommand(distutils.cmd.Command):
    """A custom command to run Pylint on all Python source files."""

    description = 'run npm build on SLUI source files'
    user_options = [('npmpath=', None, 'path to NPM executable'), ]

    def initialize_options(self):
        """Set default values for options."""
        # Each user option must be listed here with their default value.
        self.npmpath = None

    def finalize_options(self):
        """Post-process options."""
        if self.npmpath is not None:
            assert os.path.exists(self.npmpath), ('NPM executable %s does not exist.' % self.npmpath)

    def run(self):
        """Run command"""
        # command = [self.npmpath or 'npm', '--prefix', slui_dir, 'install', slui_dir]
        command = [self.npmpath or 'npm', 'install']
        self.announce('executing cmd: %s' % str(command), level=distutils.log.INFO)
        subprocess.check_call(command, cwd=slui_dir)

        # command = [self.npmpath or 'npm', 'run-script', 'build', '--prefix', slui_dir]
        command = [self.npmpath or 'npm', 'run-script', 'build']
        self.announce('executing cmd: %s' % str(command), level=distutils.log.INFO)
        subprocess.check_call(command, cwd=slui_dir)

        self.announce('Moving build directory', level=distutils.log.INFO)
        if os.path.isdir(os.path.join(setup_dir, 'netcontrol', 'build')):
            shutil.rmtree(os.path.join(setup_dir, 'netcontrol', 'build'))
        shutil.copytree(os.path.join(setup_dir, 'slui', 'build'), os.path.join(setup_dir, 'netcontrol', 'build'))


commands = {
    'npm': NpmCommand,
}
commands.update(versioneer.get_cmdclass())

setup(
    cmdclass=commands,
    name="ts-net-controller",
    version=versioneer.get_version(),
    description="TerraSmart Network Controller",
    author="TerraSmart",
    author_email="support@terrasmart.com",
    url='http://www.terrasmart.com',
    packages=['netcontrol', 'netcontrol/graphql_schema', 'netcontrol/snappyImages', 'ts-common', 'pyfiria', 'pyfiria/gen', 'pyfiria/snap', 'pyfiria/net', 'utils'],
    package_data={'netcontrol': [
        os.path.join('static' ,'*'),                # /static/*
        os.path.join('build', '*'),                 # /build/*
        os.path.join('build', 'images', '*'),       # /build/images/*
        os.path.join('build', 'static', 'js', '*'), # /build/static/js/*
        os.path.join('build', 'static', 'css', '*') # /build/static/css/*
        ]},
    entry_points={
        'console_scripts': [
            'net-controller = netcontrol.main:main',
        ],
    },
    install_requires=read_requirements('requirements.txt')
)
