import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {gql, graphql, compose} from 'react-apollo'
import StatusTable from './StatusTable'
import {CardLoadingIndicator, CardErrorIndicator} from './LoadingCard'


class DashboardWeatherBase extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired  // 'data' is injected by GraphQL
  }

  render() {
    if (this.props.data.error) {
      return (<CardErrorIndicator apolloData={this.props.data}/>)
    } else if (this.props.data.loading) {
      return (<CardLoadingIndicator />)
    }

    if (this.props.data.assets.length === 0) {
      return (<div>There are no local weather devices configured</div>)
    }

    let weatherList = []
    this.props.data.assets.map((row, index) => (
      weatherList.push(
        {
          label: 'Station ' + row.id + ' - Wind Speed',
          value: row.windSpeed
        },
        {
          label: 'Station ' + row.id + ' - Wind Direction',
          value: row.windDirection.toString() + String.fromCharCode(0x00B0)
        },
        {
          label: 'Station ' + row.id + ' - Snow Depth',
          value: row.snowDepth
        },
      )
    ))

    return (
      <div>
        <StatusTable
          statusList={weatherList}
        />
      </div>
    )
  }
}


const weatherQuery = gql`
  query AssetWeatherQuery {
    assets(hasWeatherSensor: true) {
      id
      lastReported
      windSpeed
      windDirection
      averageWindSpeed
      peakWindSpeed
      snowDepth
    }
  }
`

// Bundle query and target component as Higher Order Component (HOC)
export const DashboardWeather = compose(
  graphql(weatherQuery, {
    options: {
      pollInterval: 5000,
    },
  })
)(DashboardWeatherBase);

export default DashboardWeather
