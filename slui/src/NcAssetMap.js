// AssetMap

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import MapPanel, {MapMarker} from './MapPanel'
import {gql, graphql} from 'react-apollo'
import TextField from 'material-ui/TextField'

import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import FlatButton from 'material-ui/FlatButton'
import ContentFilterIcon from 'material-ui/svg-icons/content/filter-list'


class AssetMapPrime extends Component {
  static propTypes ={
    data: PropTypes.any.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      overlaySet: ['map', 'aerial'],
    }

    this.markers = []
    this.filteredAssets = []
  }

  handleChangeMultiple = (event, value) => {
    this.setState({
      overlaySet: value,
    })
  }

  handleSearchTextChange = (ev) => {
    this.setState({searchText : ev.target.value})
  }

  filterAssets(event) {
    // Simple, client-side filtering of whole dataset
    this.filteredAssets = this.props.data.assets.filter(device => {
      const devStr = Object.values(device).join(' ').toLowerCase()
      return devStr.includes(this.state.searchText.toLowerCase())
    })

    // Now place markers
    this.markers = []
    this.props.data.assets.forEach((dev) => {
      if (dev.locationLat && dev.locationLng) {
        let marker = new MapMarker([dev.locationLat, dev.locationLng])
        // The following is not canon - mostly made up for an upcoming demo...
        marker.title = dev.device + ' ' + dev.id

        marker.text = dev.id + ' - ' + dev.device
        marker.text += '<br>Ref Des: ' + dev.locationText
        if (dev.device === 'Tracker') {
          marker.text += '<br>Mode: ' + dev.trackingStatus
          marker.text += '<br>Current Angle: ' + dev.currentAngle
          marker.text += '<br>Target Angle: ' + dev.requestedAngle
        }
        if (dev.hasWeatherSensor) {
          marker.text += '<br>Wind Dir: ' + dev.windDirection
          marker.text += '<br>Wind Speed: ' + dev.windSpeed
          marker.text += '<br>2 Min Avg: ' + dev.averageWindSpeed
          marker.text += '<br>1 Hr Gust: ' + dev.peakWindSpeed
        }
        this.markers.push(marker)
      }
    })
  }

  render() {
    if (this.props.data.error) {
      return (<div>{this.props.data.error.stack}</div>)
    } else if (!this.props.data.loading) {
      this.filterAssets()
    }


    return (
      <div style={{display: 'flex', flexDirection: 'column', height: '100%', width: '100%'}}>

        {/* Toolbar: Acknowledge | Alarms Only | Search */}
        <div style={{flexBasis: 50, display: 'flex', flexDirection: 'row'}}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              marginTop: '-2px',  // Not sure why it is off 2 pixels
            }}
          >
          <IconMenu
            iconButtonElement={
              <FlatButton
                label="Overlay"
                icon={<ContentFilterIcon />}
              />
            }
            onChange={this.handleChangeMultiple}
            value={this.state.overlaySet}
            multiple={true}
            selectedMenuItemStyle={{color: 'rgb(132, 189, 0)'}}
          >
            <MenuItem value="map" primaryText="Map" />
            <MenuItem value="plan" primaryText="Site Plan" />
            <MenuItem value="aerial" primaryText="Aerial View" />
          </IconMenu>          
          </div>
          <div
            style={{
              fontWeight: 'bold',
              marginRight: 'auto',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center'
            }}
          >
            {this.filteredAssets.length} Assets
          </div>


          {/* Search box */}
          <div
            style={{
              marginRight: '1em'
            }}
          >
            <TextField
              hintText="Search..."
              onChange={this.handleSearchTextChange}
            />
          </div>

        </div>

        <div style={{flex: 1}}>
          <MapPanel 
            curOverlay={this.state.overlaySet.includes('aerial') &&
                        'https://2zm7ur2bo6vm2xybs622ozaq-wpengine.netdna-ssl.com/wp-content/uploads/2014/06/Warren-C.jpg'}
            //curOverlay={configDb.imageDir + '/' + configDb.manifest.layers[0].imgFile}
            curBounds={[[34.665173, -86.751651], [34.673229, -86.736163]]}
            curLoc={[34.669649, -86.745583]}
            markers={this.markers}
            showStreets={this.state.overlaySet.includes('map')}
          >
          </MapPanel>
        </div>

      </div>
    )
  }

}

const assetMapQuery = gql`
  query AssetMapQuery {
    assets {
      id
      device
      locationText
      trackingStatus
      currentAngle
      requestedAngle
      locationLat
      locationLng
      hasWeatherSensor
      windSpeed
      windDirection
      averageWindSpeed
      peakWindSpeed
    }
  }
`
// Bundle query and target component as Higher Order Component (HOC)
const AssetMap = graphql(assetMapQuery, {
  options: { pollInterval: 5000 },
})(AssetMapPrime);

export default AssetMap
