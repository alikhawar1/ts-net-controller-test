import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom'
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import App from './App'
import './index.css'
import {PAGE_INDEXES} from './PageEnums'
import LoginPage from './Login'
import {isLoggedIn} from './AuthService'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'


//const muiTheme = getMuiTheme(darkBaseTheme)
export const muiTheme = getMuiTheme(darkBaseTheme, {
  palette: {
    //accent1Color: deepOrange500,
    canvasColor: '#303030',
  }
})


class DefaultPage extends Component {
  render() {
    if (isLoggedIn()) {
      return <Redirect to="/dashboard"/>
    }
    return <Redirect to="/login"/>
  }
}

export class Index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      blockDialogOpen: false,
      blockCallback: null,
      blockMessage: 'Leaving this page will result in ALL unsaved changes being lost. Are you sure you want to leave?',
    }
  }

  handleBlockDialogClose = (action) => {
    this.setState({
      blockDialogOpen: false
    })
    this.state.blockCallback(action)
  }

  renderIfLoggedIn = (pageIndex) => {
    if (isLoggedIn()) {
      return <App page={pageIndex}/>
    }
    return <Redirect to="/login"/>
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div>
          <Router
            getUserConfirmation={(message, callback) => {
              this.setState({
                blockDialogOpen: true,
                blockMessage: message,
                blockCallback: callback,
              })
            }}
          >
            <Switch>
              <Route path="/status" render={() => this.renderIfLoggedIn(PAGE_INDEXES.STATUS)}/>
              <Route path="/dashboard" render={() => this.renderIfLoggedIn(PAGE_INDEXES.DASHBOARD)} />
              <Route path="/events" render={() => this.renderIfLoggedIn(PAGE_INDEXES.EVENTS)} />
              <Route path="/asset-map" render={() => this.renderIfLoggedIn(PAGE_INDEXES.ASSETS_MAP)} />
              <Route path="/asset-list" render={() => this.renderIfLoggedIn(PAGE_INDEXES.ASSETS_LIST)} />
              <Route path="/config-files" render={() => this.renderIfLoggedIn(PAGE_INDEXES.CONFIG_FILES)} />
              <Route path="/config-assets" render={() => this.renderIfLoggedIn(PAGE_INDEXES.CONFIG_ASSETS)} />
              <Route path="/config-network" render={() => this.renderIfLoggedIn(PAGE_INDEXES.CONFIG_NETWORK)} />
              <Route path="/config-system" render={() => this.renderIfLoggedIn(PAGE_INDEXES.CONFIG_SYSTEM)} />
              <Route path="/config-updates" render={() => this.renderIfLoggedIn(PAGE_INDEXES.CONFIG_UPDATES)} />
              <Route path="/config-control" render={() => this.renderIfLoggedIn(PAGE_INDEXES.CONFIG_CONTROL)} />
              <Route path="/config-user" render={() => this.renderIfLoggedIn(PAGE_INDEXES.CONFIG_USER)} />
              <Route path="/login" component={LoginPage}/>
              <Route path="*" component={DefaultPage}/>
            </Switch>
          </Router>
          <Dialog
            title="There are unsaved changes on this page"
            actions={[
              <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={() => this.handleBlockDialogClose(false)}
              />,
              <FlatButton
                label="Leave page"
                primary={false}
                onTouchTap={() => this.handleBlockDialogClose(true)}
              />,
            ]}
            modal={true}
            open={this.state.blockDialogOpen}
          >
            {this.state.blockMessage}
          </Dialog>
        </div>
      </MuiThemeProvider>
    );
  }
}


ReactDOM.render(
  <Index />,
  document.getElementById('root')
)
