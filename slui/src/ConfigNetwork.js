// Network Config page for SLUI

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {gql, graphql, compose} from 'react-apollo'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import Checkbox from 'material-ui/Checkbox'
import Snackbar from 'material-ui/Snackbar'
import CircularProgress from 'material-ui/CircularProgress'
import validator from 'validator'
import Dialog from 'material-ui/Dialog'
import Paper from 'material-ui/Paper'
import * as util from './util'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {withRouter} from 'react-router-dom'
import {paperStyle, sectionStyle, titleStyle, descriptionStyle} from './styles'
import {CardLoadingIndicator, CardErrorIndicator} from './LoadingCard'
import {rootPaperStyle} from './styles'
import {loadingStyle} from './LoadingCard'
import {ConfigNetworkEventLog} from './DashboardEventLog'
import {ConfirmationDialog} from './ConfirmationDialog'


let muiTheme = getMuiTheme()


class ConfigNetwork extends Component {
  static propTypes = {
    ethernetConfig: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
    bridgeConfig: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
    match: PropTypes.object.isRequired,  // injected by react-router-dom withRouter
    location: PropTypes.object.isRequired,  // injected by react-router-dom withRouter
    history: PropTypes.object.isRequired  // injected by react-router-dom withRouter
  }

  constructor(props) {
    super(props)
    this.updateConfig = props.updateConfigs
    this.updateEthernetConfig = props.updateEthernetConfig
    this.meshChanged = false
    this.ethernetChanged = false
    this.updatedBridgeConfig = {}
    this.updatedEthernetConfig = {}
    this.state = {
      snackbarOpen: false,
      progressDisplay: 'none',
      meshUpdateButtonDisabled: false,
      bridgeChannelErrorText: '',
      bridgeNetworkIdErrorText: '',
      displayStaticIpFields: null,
      ethernetUpdateButtonDisabled: false,
      requestedStaticIpErrorText: '',
      requestedStaticSubnetErrorText: '',
      errorDialogOpen: false
    }
    this.history = props.history
    this.unblock = null
  }

  handleEthernetUpdate = () => {
    if (this.ethernetChanged) {
      this.setState({
        ethernetUpdateButtonDisabled: true,
        progressDisplay: 'inline'
      })
      this.updateEthernetConfig(this.updatedEthernetConfig).then((data) => {
        this.setState({
          snackbarOpen: true,
          ethernetUpdateButtonDisabled: false,
          progressDisplay: 'none'
        })
        this.ethernetChanged = false
        this.updatedEthernetConfig = {}
      }).catch((err) => {
        console.log(err)
        this.setState({
          errorDialogOpen: true,
          ethernetUpdateButtonDisabled: false,
          progressDisplay: 'none'
        })
      })
    }
  }

  handleEthernetCheckboxChange = (event) => {
    this.updatedEthernetConfig[event.target.name] = event.target.checked

    if (event.target.name === 'enableStaticIpAddress') {
      this.setState({
        displayStaticIpFields: event.target.checked
      })
    }

    this.ethernetChanged = true
    if (this.props.data) {
      this.props.data.refetch()
    }
  }

  handleMeshUpdate = () => {
    if (this.meshChanged) {
      this.setState({
        meshUpdateButtonDisabled: true,
        progressDisplay: 'inline'
      })
      this.updateConfig(this.updatedBridgeConfig).then((data) => {
        this.setState({
          snackbarOpen: true,
          meshUpdateButtonDisabled: false,
          progressDisplay: 'none'
        })
        this.meshChanged = false
        this.updatedBridgeConfig = {}
      }).catch((err) => {
        console.log(err)
        this.setState({
          errorDialogOpen: true,
          meshUpdateButtonDisabled: false,
          progressDisplay: 'none'
        })
      })
    }
  }

  // This is a cheat used to filter the messages on the "mini event log"
  filterFunc = (event) => {
      return event.message.toLowerCase().includes("all assets")
  }

  handleEthernetFieldChange = (event) => {
    this.updatedEthernetConfig[event.target.name] = event.target.value
    this.ethernetChanged = true
  }

  handleBridgeFieldChange = (event) => {
    this.updatedBridgeConfig[event.target.name] = event.target.value
    this.meshChanged = true
  }

  handleSnackbarRequestClose = () => {
    this.setState({
      snackbarOpen: false,
    });
  }

  validateInt = (e, min, max, errorTextName) => {
    let errorText = ""
    if (!validator.isInt(e.currentTarget.value, {min: min, max: max})) {
      errorText = "Must be a number between " + min + " and " + max
    }
    let stateUpdate = {}
    stateUpdate[errorTextName] = errorText
    this.setState(stateUpdate)
  }

  validateBridgeNetworkId = (e, min, max, errorTextName) => {
    let errorText = ""
    let temp = e.currentTarget.value
    // I am allowing decimal or hexadecimal input
    if ((temp.length >= 2) && (temp.substring(0, 2) === '0x')) {
      if (!validator.isHexadecimal(temp.substring(2))) {
        errorText = "Must be a valid hexadecimal number"
      }
      else {
        // OK, it is at least hexadecimal. We still have to perform the range checks
        temp = '' + parseInt(e.currentTarget.value, 16)
        if (!validator.isInt(temp, {min: min, max: max})) {
          errorText = "Must be a number between " + min + " and " + max
        } else {
          this.updatedBridgeConfig['bridgeNetworkId'] = temp
          this.meshChanged = true
        }
      }
    }
    else {
      if (!validator.isInt(e.currentTarget.value, {min: min, max: max})) {
        errorText = "Must be a number between " + min + " and " + max
      } else {
        this.updatedBridgeConfig['bridgeNetworkId'] = parseInt(e.currentTarget.value, 10)
        this.meshChanged = true
      }
    }
    let stateUpdate = {}
    stateUpdate[errorTextName] = errorText
    this.setState(stateUpdate)
  }

  validateFQDN = (e, errorTextName) => {
    let errorText = ""
    if (!validator.isFQDN(e.currentTarget.value)) {
      errorText = "Must be a valid domain name"
    }
    let stateUpdate = {}
    stateUpdate[errorTextName] = errorText
    this.setState(stateUpdate)
  }

  validateIPorNothing = (e, errorTextName) => {
    let errorText = ""
    if (!validator.isIP(e.currentTarget.value, 4)) {
      if (e.currentTarget.value !== "") {
        errorText = "Must be four valid octets www.xxx.yyy.zzz each 0-255, or an empty string"
      }
    }
    let stateUpdate = {}
    stateUpdate[errorTextName] = errorText
    this.setState(stateUpdate)
  }

  closeErrorDialg = () => {
    this.setState({errorDialogOpen: false})
  }

  componentWillMount = () => {
    this.unblock = this.history.block((location, action) => {
      if (this.meshChanged || this.ethernetChanged) {
        return 'Leaving this page will result in ALL unsaved changes being lost. Are you sure you want to leave?'
      }
    })
  }

  componentWillUnmount = () => {
    this.unblock()
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.state.displayStaticIpFields === null &&
      !nextProps.networkConfig.loading &&
      !nextProps.networkConfig.error) {
      this.setState({
        nightlyShutdownDisplay: nextProps.networkConfig.ethernet.enableStaticIpAddress,
      })
    }
  }

  render() {
    if (this.props.networkConfig.error) {
      return (<CardErrorIndicator apolloData={this.props.networkConfig} />)
    } else if (this.props.networkConfig.loading) {
      return (<CardLoadingIndicator />)
    }

    // Real data
    const bridgeConfig = this.props.networkConfig.bridge
    // Fake data for testing on my PC
    //const bridgeConfig = {}
    //bridgeConfig.bridgeChannel = 7

    // Real data
    const ethernetConfig = this.props.networkConfig.ethernet
    // Fake data for testing on my PC
    //const ethernetConfig = {}
    //ethernetConfig.enableStaticIpAddress = true
    //ethernetConfig.requestedStaticIpAddress = '192.168.1.42'



    const statTitleStyle = {
      fontSize: '1.15em',
      paddingBottom: '1em',
    }

    const widerPaperStyle = {}
    Object.assign(widerPaperStyle, rootPaperStyle)
    widerPaperStyle.width = 'auto'
    widerPaperStyle.overflow = 'auto'
    widerPaperStyle.margin = '0.8em'

    // So we can get the buttons closer to what they apply to
    const bottomlessSectionStyle = {}
    Object.assign(bottomlessSectionStyle, sectionStyle)
    delete bottomlessSectionStyle['borderBottom']
    delete bottomlessSectionStyle['paddingBottom']


    return (
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center'
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignContent: 'flex-start',
            flex: '1 1 100%'
          }}
        >
          <form>
            <div style={{
              fontSize: "1.2em",
              fontFamily: muiTheme.fontFamily,
              paddingTop: '2rem',
              paddingBottom: '2rem',
              paddingLeft: '2rem'
            }}>
              Network Configuration
            </div>

            <div style={bottomlessSectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Mesh Configuration</h5>
                <br/>
                Configuration parameters for the mesh network used by the tracker controllers
              </div>
              <Paper style={paperStyle}>
                <TextField name="bridgeChannel"
                           style={{width: '100%'}}
                           floatingLabelText="Channel"
                           onChange={this.handleBridgeFieldChange}
                           defaultValue={bridgeConfig.bridgeChannel}
                           onBlur={(e) => this.validateInt(e, 0, 15, 'bridgeChannelErrorText')}
                           errorText={this.state.bridgeChannelErrorText}
                />
                <TextField name="bridgeNetworkId"
                           style={{width: '100%'}}
                           floatingLabelText="Network ID"
                           defaultValue={util.snappyHex(bridgeConfig.bridgeNetworkId)}
                           onBlur={(e) => this.validateBridgeNetworkId(e, 0, 65535, 'bridgeNetworkIdErrorText')}
                           errorText={this.state.bridgeNetworkIdErrorText}
                />
                <TextField name="bridgeKey"
                           style={{width: '100%'}}
                           floatingLabelText="Security Key"
                           onChange={this.handleBridgeFieldChange}
                           defaultValue={bridgeConfig.bridgeKey}
                />
              </Paper>
              <Paper
                zDepth={1}
                rounded={false}
                style={widerPaperStyle}
              >
                <div style={loadingStyle}>
                  <div style={statTitleStyle}>
                    Progress Reports
                  </div>
                  <ConfigNetworkEventLog filterFunc={this.filterFunc} />
                </div>
              </Paper>
            </div>

            <div style={{
              maxWidth: '50em',
              margin: '0px auto 2rem'
            }}>
              <div style={{
                textAlign: 'right',
                display: 'flex',
                flexDirection: 'row-reverse',
                alignItems: 'center',
                paddingBottom: '1rem',
                borderBottom: '1px solid #d3dbe2'
              }}>
                <ConfirmationDialog title="Reconfigure mesh network?"
                                    text="This operation is service affecting for several minutes. Are you sure now is an appropriate time?"
                                    mainLabel="Update Mesh"
                                    cancelLabel="Cancel Request"
                                    confirmLabel="Reconfigure Network Now"
                                    onConfirm={() => this.handleMeshUpdate()}
                                    disabled={this.state.meshUpdateButtonDisabled} />
                <CircularProgress size={30} style={{
                  display: this.state.progressDisplay
                }}/>
              </div>
            </div>

            <div style={bottomlessSectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Ethernet Configuration</h5>
                <br/>
                Configuration parameters for use with the optional USB Ethernet interface
              </div>
              <Paper style={paperStyle}>
                <Checkbox
                  label="Enable an extra static IP Address"
                  name="enableStaticIpAddress"
                  onCheck={this.handleEthernetCheckboxChange}
                  defaultChecked={ethernetConfig.enableStaticIpAddress}
                />
                {(this.state.displayStaticIpFields !== null ? this.state.displayStaticIpFields : ethernetConfig.enableStaticIpAddress) &&
                <TextField name="requestedStaticIpAddress"
                           style={{width: '100%'}}
                           floatingLabelText="Requested Static IP Address"
                           onChange={this.handleEthernetFieldChange}
                           defaultValue={ethernetConfig.requestedStaticIpAddress}
                           onBlur={(e) => this.validateIPorNothing(e, 'requestedStaticIpErrorText')}
                           errorText={this.state.requestedStaticIpErrorText}
                />
                }
                {(this.state.displayStaticIpFields !== null ? this.state.displayStaticIpFields : ethernetConfig.enableStaticIpAddress) &&
                <TextField name="requestedStaticSubnetMask"
                           style={{width: '100%'}}
                           floatingLabelText="Requested Static Subnet Mask"
                           onChange={this.handleEthernetFieldChange}
                           defaultValue={ethernetConfig.requestedStaticSubnetMask}
                           onBlur={(e) => this.validateIPorNothing(e, 'requestedStaticSubnetErrorText')}
                           errorText={this.state.requestedStaticSubnetErrorText}
                />
                }
              </Paper>
            </div>

            <div style={{
              maxWidth: '50em',
              margin: '0px auto 2rem'
            }}>
              <div style={{
                textAlign: 'right',
                display: 'flex',
                flexDirection: 'row-reverse',
                alignItems: 'center',
                paddingBottom: '1rem',
                borderBottom: '1px solid #d3dbe2'
              }}>
                <ConfirmationDialog title="Reconfigure Ethernet network?"
                                    text="This operation might affect the Ethernet connection you are currently using. Are you sure now is an appropriate time?"
                                    mainLabel="Update Ethernet"
                                    cancelLabel="Cancel Request"
                                    confirmLabel="Reconfigure Network Now"
                                    onConfirm={() => this.handleEthernetUpdate()}
                                    disabled={this.state.ethernetUpdateButtonDisabled} />
                <CircularProgress size={30} style={{
                  display: this.state.progressDisplay
                }}/>
              </div>
            </div>

          </form>

          <Snackbar
            open={this.state.snackbarOpen}
            message="System configuration updated"
            autoHideDuration={4000}
            onRequestClose={this.handleSnackbarRequestClose}
            bodyStyle={{textAlign: 'center'}}
          />

          <Dialog
            actions={[
              <RaisedButton
                label="OK"
                primary={true}
                onTouchTap={this.closeErrorDialg}
              />,
            ]}
            modal={false}
            open={this.state.errorDialogOpen}
            onRequestClose={this.closeErrorDialg}
          >
            An error occurred while updating the settings. Please verify your values and try again
          </Dialog>

        </div>
      </div>
    )
  }
}


export const UPDATE_BRIDGE_CONFIG = gql`
  mutation updateBridgeConfig(
    $bridgeConfigData: BridgeConfigInput,
  ) {
    updateBridgeConfig(configData: $bridgeConfigData) {
      bridge {
        bridgeChannel
        bridgeNetworkId
        bridgeKey
      }
    }
  }
`

export const UPDATE_ETHERNET_CONFIG = gql`
  mutation updateEthernetConfig(
    $ethernetConfigData: EthernetConfigInput,
  ) {
    updateEthernetConfig(configData: $ethernetConfigData) {
      ethernet {
        enableStaticIpAddress
        requestedStaticIpAddress
        requestedStaticSubnetMask
      }
    }
  }
`

const configNetworkQuery = gql`
   query ConfigNetworkQuery {
     ethernet {
       enableStaticIpAddress
       requestedStaticIpAddress
       requestedStaticSubnetMask
     }
     bridge {
       bridgeChannel
       bridgeNetworkId
       bridgeKey
     }
   }
`

// Bundle query, mutation, and target component together as a Higher Order Component (HOC)
export const ConfigNetworkPage = compose(
  graphql(configNetworkQuery, {name: 'networkConfig'}),
  graphql(UPDATE_BRIDGE_CONFIG, {
    props: ({ownProps, mutate}) => ({
      updateConfigs: (updatedBridgeConfig) => {
        return mutate({
          variables: {
            bridgeConfigData: updatedBridgeConfig
          }
        })
      },
    }),
    options: {
      refetchQueries: [
        {query: configNetworkQuery},
      ],
    }
  }),
  graphql(UPDATE_ETHERNET_CONFIG, {
    props: ({ownProps, mutate}) => ({
      updateEthernetConfig: (updatedEthernetConfig) => {
        return mutate({
          variables: {
            ethernetConfigData: updatedEthernetConfig
          }
        })
      },
    }),
    options: {
      refetchQueries: [
        {query: configNetworkQuery},
      ],
    }
  })
)(withRouter(ConfigNetwork))

export default ConfigNetworkPage
