import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {List, ListItem, makeSelectable} from 'material-ui/List'
import SettingsIcon from 'material-ui/svg-icons/action/settings'
import ClipboardIcon from 'material-ui/svg-icons/action/assignment'
import RouterIcon from 'material-ui/svg-icons/hardware/router'
import TimerIcon from 'material-ui/svg-icons/av/av-timer'
import MapIcon from 'material-ui/svg-icons/maps/map'
import AssessIcon from 'material-ui/svg-icons/action/assessment'
import ListIcon from 'material-ui/svg-icons/action/list'
import ControlIcon from 'material-ui/svg-icons/image/tune'
import SystemIcon from 'material-ui/svg-icons/action/language'
import NetworkIcon from 'material-ui/svg-icons/image/leak-add'
import AssetsIcon from 'material-ui/svg-icons/image/blur-linear'
import FilesIcon from 'material-ui/svg-icons/action/description'
import {Link} from 'react-router-dom'
import {PAGE_INDEXES} from './PageEnums'
import {muiTheme} from './index'


let SelectableList = makeSelectable(List)


export class LeftNav extends Component {
  static propTypes = {
    page: PropTypes.oneOf(Object.values(PAGE_INDEXES)).isRequired
  }

  render() {
    return (
      <div style={{
        width: 256,
        minWidth: 256,
        backgroundColor: muiTheme.palette.canvasColor,
        height: '100vh',
        overflow: 'auto'
      }}>
        <SelectableList value={this.props.page}>
          <ListItem value={PAGE_INDEXES.DASHBOARD}
                    primaryText="Dashboard"
                    leftIcon={<TimerIcon />}
                    containerElement={<Link to="/dashboard"/>}
          />
          <ListItem value={PAGE_INDEXES.STATUS}
                    primaryText="Status"
                    leftIcon={<AssessIcon />}
                    containerElement={<Link to="/status"/>}
          />
          <ListItem value={PAGE_INDEXES.CONFIG_CONTROL}
                    primaryText="Control"
                    leftIcon={<ControlIcon />}
                    containerElement={<Link to="/config-control"/>}
          />
          <ListItem value={PAGE_INDEXES.EVENTS}
                    primaryText="Events"
                    leftIcon={<ClipboardIcon />}
                    containerElement={<Link to="/events"/>}
          />
          <ListItem primaryText="Assets"
                    leftIcon={<RouterIcon />}
                    primaryTogglesNestedList={true}
                    nestedItems={[
                      <ListItem value={PAGE_INDEXES.ASSETS_MAP}
                                primaryText="Map"
                                leftIcon={<MapIcon />}
                                containerElement={<Link to="/asset-map"/>}
                      />,
                      <ListItem value={PAGE_INDEXES.ASSETS_LIST}
                                primaryText="List"
                                leftIcon={<ListIcon />}
                                containerElement={<Link to="/asset-list"/>}
                      />,
                    ]}
          />
          <ListItem primaryText="Config"
                    leftIcon={<SettingsIcon />}
                    primaryTogglesNestedList={true}
                    nestedItems={[
                      <ListItem value={PAGE_INDEXES.CONFIG_SYSTEM}
                                primaryText="System"
                                leftIcon={<SystemIcon />}
                                containerElement={<Link to="/config-system"/>}
                      />,
                      <ListItem value={PAGE_INDEXES.CONFIG_NETWORK}
                                primaryText="Network"
                                leftIcon={<NetworkIcon />}
                                containerElement={<Link to="/config-network"/>}
                      />,
                      <ListItem value={PAGE_INDEXES.CONFIG_ASSETS}
                                primaryText="Assets"
                                leftIcon={<AssetsIcon />}
                                containerElement={<Link to="/config-assets"/>}
                      />,
                      <ListItem value={PAGE_INDEXES.CONFIG_FILES}
                                primaryText="Files"
                                leftIcon={<FilesIcon />}
                                containerElement={<Link to="/config-files"/>}
                      />,
                      <ListItem value={PAGE_INDEXES.CONFIG_UPDATES}
                                primaryText="Updates"
                                leftIcon={<SettingsIcon />}
                                containerElement={<Link to="/config-updates"/>}
                      />,
                    ]}
          />
        </SelectableList>
      </div>
    )
  }
}