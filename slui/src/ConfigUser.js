// User Config page for SLUI

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {compose} from 'react-apollo'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import Snackbar from 'material-ui/Snackbar'
import Dialog from 'material-ui/Dialog'
import Paper from 'material-ui/Paper'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {withRouter} from 'react-router-dom'
import {paperStyle, sectionStyle, titleStyle, descriptionStyle} from './styles'
import {rootPaperStyle} from './styles'
import {verify} from './AuthService'
import {change} from './AuthService'


let muiTheme = getMuiTheme()


class ConfigUser extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,  // injected by react-router-dom withRouter
    location: PropTypes.object.isRequired,  // injected by react-router-dom withRouter
    history: PropTypes.object.isRequired  // injected by react-router-dom withRouter
  }

  constructor(props) {
    super(props)
    this.updateConfig = props.updateConfigs
    this.hasChanged = false
    this.updatedBridgeConfig = {}
    this.state = {
      snackbarOpen: false,
      oldPassword: '',
      newPassword: '',
      newPasswordConfirmation: '',
      oldPasswordErrorText: '',
      newPasswordErrorText: '',
      newPasswordConfirmationErrorText: '',
      changePasswordButtonDisabled: true,

      progressDisplay: 'none',
      updateButtonDisabled: false,
      errorDialogOpen: false
    }
    this.history = props.history
    this.unblock = null
  }

  handleSnackbarRequestClose = () => {
    this.setState({
      snackbarOpen: false,
    });
  }

  handleChangePassword = (event) => {
    this.setState({
      changePasswordButtonDisabled: true
    })

    change(this.state.newPassword).then(() => {
      // Indicate success
      this.setState({
        snackbarOpen: true,
        oldPassword: '',
        newdPassword: '',
        newPasswordConfirmation: '',
      })
      console.log("Password changed successfully")
    }).catch(() => {
      console.log("Password change failed")
      this.setState({
        passwordErrorText: "Please check password",
      })
    })
  }

  handleOldPasswordChange = (event) => {
    this.setState({oldPassword: event.target.value});
  }

  handleNewPasswordChange = (event) => {
    this.setState({newPassword: event.target.value});
  }

  handleNewPasswordConfirmationChange = (event) => {
    this.setState({newPasswordConfirmation: event.target.value});
  }

  checkRulesForChangePasswordButton = () => {
    let ready_to_attempt = true // innocent until proven guilty...

    // Enough fields filled in to attempt a password change?
    if (this.state.oldPassword === "") {
      ready_to_attempt = false
    }
    if (this.state.newPassword === "") {
      ready_to_attempt = false
    }
    if (this.state.newPasswordConfirmation === "") {
      ready_to_attempt = false
    }
    // Enough fields filled in WITHOUT error to attempt a password change?
    if (this.state.oldPasswordErrorText !== "") {
      ready_to_attempt = false
    }
    if (this.state.newPasswordErrorText !== "") {
      ready_to_attempt = false
    }
    if (this.state.newPasswordConfirmationErrorText !== "") {
      ready_to_attempt = false
    }

    let stateUpdate = {}
    stateUpdate["changePasswordButtonDisabled"] = !ready_to_attempt
    this.setState(stateUpdate)
  }

  validateOldPassword = (e, errorTextName) => {
    let stateUpdate = {}

    if (this.state.oldPassword === "") {
      stateUpdate[errorTextName] = "Please enter your current password"
      stateUpdate["changePasswordButtonDisabled"] = true
      this.setState(stateUpdate)
      return
    }

    verify(this.state.oldPassword).then(() => {
      console.log("Current Password entered correctly")
      stateUpdate[errorTextName] = ""
      this.setState(stateUpdate)

      // Since THIS field is valid, check ALL of the fields
      this.checkRulesForChangePasswordButton()
    }).catch(() => {
      console.log("Current Password entered incorrectly")
      stateUpdate[errorTextName] = "Enter your current password correctly"
      stateUpdate["changePasswordButtonDisabled"] = true
      this.setState(stateUpdate)
    })
  }

  validateNewPasswords = (e, errorTextName) => {
    let errorText = ""
    let proposed = e.currentTarget.value
    if (proposed !== "") {
      if (proposed.length < 10) {
        errorText = "Illegal password - too short - must be at least 10 characters"
      } else if (!proposed.match(/[^A-Za-z0-9]/)) {
        errorText = "Illegal password - must contain at least one special character"
      } else if (!proposed.match(/[0-9]/)) {
        errorText = "Illegal password - must contain at least one digit"
      } else if (!proposed.match(/[A-Z]/)) {
        errorText = "Illegal password - must contain at least one uppercase character"
      } else if (!proposed.match(/[a-z]/)) {
        errorText = "Illegal password - must contain at least one lowercase character"
      }
      // If both "old" and "proposed"" have been entered, are they different?
      if ((errorText === "") && this.state.oldPassword !== "") {
        if (this.state.oldPassword === proposed) {
          errorText = "Proposed password has to be different from the current password"
        }
      }
      // If both "new" and "confirmation" have been entered, do they match?
      if ((errorText === "") && (this.state.newPassword !== "") && (this.state.newPasswordConfirmation !== "")) {
        if (this.state.newPassword !== this.state.newPasswordConfirmation) {
          errorText = "Proposed password not entered the same way twice! Re-enter this one"
        }
      }
    }

    let stateUpdate = {}
    if (errorText !== "") {
      stateUpdate[errorTextName] = errorText
      stateUpdate["changePasswordButtonDisabled"] = true
      this.setState(stateUpdate)
    } else {
      stateUpdate[errorTextName] = errorText
      this.setState(stateUpdate)

      this.checkRulesForChangePasswordButton()
    }
  }

  closeErrorDialg = () => {
    this.setState({errorDialogOpen: false})
  }

  componentWillMount = () => {
    this.unblock = this.history.block((location, action) => {
      if (this.hasChanged) {
        return 'Leaving this page will result in ALL unsaved changes being lost. Are you sure you want to leave?'
      }
    })
  }

  componentWillUnmount = () => {
    this.unblock()
  }

  render() {
    const widerPaperStyle = {}
    Object.assign(widerPaperStyle, rootPaperStyle)
    widerPaperStyle.width = 'auto'
    widerPaperStyle.overflow = 'auto'
    widerPaperStyle.margin = '0.8em'


    return (
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center'
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignContent: 'flex-start',
            flex: '1 1 100%'
          }}
        >
          <form>
            <div style={{
              fontSize: "1.2em",
              fontFamily: muiTheme.fontFamily,
              paddingTop: '2rem',
              paddingBottom: '2rem',
              paddingLeft: '2rem'
            }}>
              User Configuration
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Password Change</h5>
                <br/>
                Use this section of the page to change your password.
                Note that passwords are required to be at least 10 characters long,
                and must contain a mix of uppercase, lowercase, numeric, and special characters.
              </div>
              <Paper style={paperStyle}>
                <TextField name="oldpassword"
                           style={{width: '100%'}}
                           floatingLabelText="Enter your current password"
                           onChange={this.handleOldPasswordChange}
                           onBlur={(e) => this.validateOldPassword(e, 'oldPasswordErrorText')}
                           errorText={this.state.oldPasswordErrorText}
                           type="password"
                />
                <TextField name="newpassword"
                           style={{width: '100%'}}
                           floatingLabelText="Enter your requested new password"
                           onChange={this.handleNewPasswordChange}
                           onBlur={(e) => this.validateNewPasswords(e, 'newPasswordErrorText')}
                           errorText={this.state.newPasswordErrorText}
                           type="password"
                />
                <TextField name="newpasswordconfirmation"
                           style={{width: '100%'}}
                           floatingLabelText="Enter your requested new password again"
                           onChange={this.handleNewPasswordConfirmationChange}
                           onBlur={(e) => this.validateNewPasswords(e, 'newPasswordConfirmationErrorText')}
                           errorText={this.state.newPasswordConfirmationErrorText}
                           type="password"
                />
                <div style={{textAlign: 'right'}}>
                  <RaisedButton label="Change Password"
                                style={{margin: 14}}
                                primary={true}
                                onTouchTap={this.handleChangePassword}
                                disabled={this.state.changePasswordButtonDisabled}
                  />
                </div>
              </Paper>
            </div>

          </form>
          <Snackbar
            open={this.state.snackbarOpen}
            message="System configuration updated"
            autoHideDuration={4000}
            onRequestClose={this.handleSnackbarRequestClose}
            bodyStyle={{textAlign: 'center'}}
          />
          <Dialog
            actions={[
              <RaisedButton
                label="OK"
                primary={true}
                onTouchTap={this.closeErrorDialg}
              />,
            ]}
            modal={false}
            open={this.state.errorDialogOpen}
            onRequestClose={this.closeErrorDialg}
          >
            An error occurred while updating the settings. Please verify your values and try again
          </Dialog>
        </div>
      </div>
    )
  }
}


// Bundle query, mutation, and target component together as a Higher Order Component (HOC)
export const ConfigUserPage = compose(
)(withRouter(ConfigUser))

export default ConfigUserPage
