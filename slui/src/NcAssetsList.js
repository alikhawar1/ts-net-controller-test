// NcAssetList - Lists of Asset information
import React from 'react'
import SwipeableViews from 'react-swipeable-views'
import AssetListStatus from './AssetListStatus'
import AssetListTracking from './AssetListTracking'
import AssetListWeather from './AssetListWeather'
import AssetListPower from './AssetListPower'
import AssetListVersion from './AssetListVersion'
import {Tabs, Tab} from 'material-ui/Tabs'
import Paper from 'material-ui/Paper'
import getMuiTheme from 'material-ui/styles/getMuiTheme'


let muiTheme = getMuiTheme()
const STATUS_TAB = 0
const TRACKING_TAB = 1
const WEATHER_TAB = 2
const POWER_TAB = 3
const VERSION_TAB = 4


class NcAssetList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      slideIndex: 0,
    }
  }

  handleChange = (value) => {
    this.setState({
      slideIndex: value,
    })
  }

  render() {
    const tabHeight = 30
    const buttonStyle = {
      height: tabHeight,
      color: 'black',
      padding: '1em,'
    }

    return (
      <div
        style={{
          margin: '2em'
        }}
      >
        <div style={{
          fontSize: "1.2em",
          fontFamily: muiTheme.fontFamily,
          paddingBottom: '2rem',
        }}>
          Assets List
        </div>
        <Paper
          style={{
            width: '100%',
          }}
        >
          <div
            style={{
              paddingLeft: '1em',
              borderBottom: '1px solid #d3dbe2',
            }}>
            <Tabs
              onChange={this.handleChange}
              value={this.state.slideIndex}
              style={{
                margin: 0,
                width: '30em'
              }}
              tabItemContainerStyle={{
                backgroundColor: 'white',
                height: tabHeight,
                padding: '0.25em',
                margin: 0,
              }}
              inkBarStyle={{
                padding: '0.5px',
                backgroundColor: '#80bd03',
              }}
            >
              <Tab label="Status" buttonStyle={buttonStyle} value={STATUS_TAB}/>
              <Tab label="Tracking" buttonStyle={buttonStyle} value={TRACKING_TAB}/>
              <Tab label="Weather" buttonStyle={buttonStyle} value={WEATHER_TAB}/>
              <Tab label="Power" buttonStyle={buttonStyle} value={POWER_TAB}/>
              <Tab label="Version" buttonStyle={buttonStyle} value={VERSION_TAB}/>
            </Tabs>
          </div>

          <SwipeableViews
            enableMouseEvents={false}
            index={this.state.slideIndex}
            onChangeIndex={this.handleChange}
          >
            <div>
              {/* Status */}
              <AssetListStatus active={this.state.slideIndex === STATUS_TAB} />
            </div>
            <div>
              {/* Tracking */}
              <AssetListTracking active={this.state.slideIndex === TRACKING_TAB} />
            </div>
            <div>
              {/* Weather */}
              <AssetListWeather active={this.state.slideIndex === WEATHER_TAB} />
            </div>
            <div>
              {/* Power */}
              <AssetListPower active={this.state.slideIndex === POWER_TAB} />
            </div>
            <div>
              {/* Version */}
              <AssetListVersion active={this.state.slideIndex === VERSION_TAB} />
            </div>
          </SwipeableViews>

        </Paper>
      </div>
    )
  }
}

export default NcAssetList