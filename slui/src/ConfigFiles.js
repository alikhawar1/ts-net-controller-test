// Config => Files page for SLUI


import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {gql, graphql, compose} from 'react-apollo'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import Snackbar from 'material-ui/Snackbar'
import CircularProgress from 'material-ui/CircularProgress'
import validator from 'validator'
import Dialog from 'material-ui/Dialog'
import Paper from 'material-ui/Paper'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {withRouter} from 'react-router-dom'
import {paperStyle, sectionStyle, titleStyle, descriptionStyle} from './styles'
import Dropzone from 'react-dropzone'
import {CardLoadingIndicator, CardErrorIndicator} from './LoadingCard'


let muiTheme = getMuiTheme()
const defaultImageFileName = '<Drag file here or select image>'
const defaultSpecFileName = '<Drag spec file here or select file>'
const defaultConfigFileName = '<Drag config file here or select file>'
const defaultUpgradeFileName = '<Drag upgrade file here or select file>'


class ConfigFiles extends Component {
  static propTypes = {
    filesConfig: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
    match: PropTypes.object.isRequired,  // injected by react-router-dom withRouter
    location: PropTypes.object.isRequired,  // injected by react-router-dom withRouter
    history: PropTypes.object.isRequired  // injected by react-router-dom withRouter
  }

  constructor(props) {
    super(props)
    this.updateConfig = props.submit
    this.hasChanged = false
    this.updatedSiteConfigVals = {}
    this.state = {
      snackbarOpen: false,
      progressDisplay: 'none',
      updateButtonDisabled: false,
      llLngErrorText: '',
      llLatErrorText: '',
      ulLngErrorText: '',
      ulLatErrorText: '',
      errorDialogOpen: false,
      imageFileName: defaultImageFileName,
      imageFile: null,
      specFileName: defaultSpecFileName,
      specFile: null,
      configFileName: defaultConfigFileName,
      configFile: null,
      upgradeFileName: defaultUpgradeFileName,
      upgradeFile: null,
      errorMessage: null
    }
    this.history = props.history
    this.unblock = null
  }

  handleSubmit = (event) => {
    event.preventDefault()  // prevent the default form submit behaviour

    if (this.hasChanged) {
      this.setState({
        updateButtonDisabled: true,
        progressDisplay: 'inline'
      })
      this.updateConfig(this.state.imageFile, this.state.specFile, this.state.configFile, this.state.upgradeFile, this.updatedSiteConfigVals).then((data) => {
        // console.log("Updated site config")
        if (data.data.upgrade.received !== true && this.state.upgradeFile !== null) {
          this.setState({
            errorDialogOpen: true,
            updateButtonDisabled: false,
            progressDisplay: 'none',
            errorMessage: 'The provided upgrade file was not received. Please verify it is a valid ts_net_controller---.whl file'
          })
        } else if (data.data.importConfig.received !== true && this.state.configFile !== null) {
          this.setState({
            errorDialogOpen: true,
            updateButtonDisabled: false,
            progressDisplay: 'none',
            errorMessage: 'The provided config file was not received or was invalid. Please verify it is a valid config file from a previous download'
          })
        } else {
          this.setState({
            snackbarOpen: true,
            updateButtonDisabled: false,
            progressDisplay: 'none',
            imageFileName: defaultImageFileName,
            imageFile: null,
            specFileName: defaultSpecFileName,
            specFile: null,
            configFileName: defaultConfigFileName,
            configFile: null,
            upgradeFileName: defaultUpgradeFileName,
            upgradeFile: null
          })
          this.hasChanged = false
        }
      }).catch((err) => {
        console.log(err)
        if (this.state.errorMessage === null) {
          this.setState({
            errorMessage: 'An error occurred while updating the configuration. Please verify your values and try again',
            errorDialogOpen: true,
            updateButtonDisabled: false,
            progressDisplay: 'none'
          })
        }
      })
    }
  }

  handleFormChange = (event) => {
    this.updatedSiteConfigVals[event.target.name] = event.target.value
    this.hasChanged = true
  }

  handleSnackbarRequestClose = () => {
    this.setState({
      snackbarOpen: false,
    })
  }

  validateInt = (e, min, max, errorTextName) => {
    let errorText = ""
    if (!validator.isFloat(e.currentTarget.value, {min: min, max: max})) {
      errorText = "Must be a number between " + min + " and " + max
    }
    let stateUpdate = {}
    stateUpdate[errorTextName] = errorText
    this.setState(stateUpdate)
  }

  closeErrorDialg = () => {
    this.setState({errorDialogOpen: false})
  }

  componentWillMount = () => {
    this.unblock = this.history.block((location, action) => {
      if (this.hasChanged) {
        return 'Leaving this page will result in ALL unsaved changes being lost. Are you sure you want to leave?'
      }
    })
  }

  componentWillUnmount = () => {
    this.unblock()
  }

  render() {
    if (this.props.filesConfig.error) {
      return (<CardErrorIndicator apolloData={this.props.filesConfig}/>)
    } else if (this.props.filesConfig.loading) {
      return (<CardLoadingIndicator/>)
    }

    const filesConfig = this.props.filesConfig.config
    let dropzoneRef = null
    let specDropzoneRef = null
    let configDropzoneRef = null
    let upgradeDropzoneRef = null

    return (
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center'
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignContent: 'flex-start',
            flex: '1 1 100%'
          }}
        >
          <form onSubmit={this.handleSubmit}>
            <div style={{
              fontSize: "1.2em",
              fontFamily: muiTheme.fontFamily,
              paddingTop: '2rem',
              paddingBottom: '2rem',
              paddingLeft: '2rem'
            }}>
              Config Files
            </div>


            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Site Image</h5>
                <br/>
                Provide an aerial image of the site to use on the map and in the Field Commissioning Software
              </div>
              <Paper style={paperStyle}>
                <Dropzone
                  onDrop={(files) => {
                    this.setState({
                      imageFileName: files[0].name,
                      imageFile: files[0],
                    })
                    this.hasChanged = true
                  }}
                  ref={
                    (node) => {
                      dropzoneRef = node
                    }
                  }
                  disableClick={true}
                  activeStyle={{}}
                  multiple={false}
                >
                  <div>
                    <TextField
                      name="imageFile"
                      style={{
                        marginRight: '1em',
                        width: '17.5em'
                      }}
                      floatingLabelText="Site Image File"
                      floatingLabelStyle={{
                        color: 'black',
                      }}
                      value={this.state.imageFileName}
                      disabled={false}
                      inputStyle={{
                        color: 'rgba(0, 0, 0, 0.3)',
                        WebkitTextFillColor: 'rgba(0, 0, 0, 0.3)',
                        disabled: 'true'
                      }}
                      onClick={() => {
                        dropzoneRef.open()
                      }}
                      onFocus={(e) => {
                        e.target.blur()
                      }}
                    />
                    <RaisedButton
                      label='Select Image...'
                      primary={true}
                      onClick={
                        () => {
                          dropzoneRef.open()
                        }
                      }
                    />
                  </div>
                  <TextField
                    name="siteImageUlLat"
                    style={{width: '100%'}}
                    floatingLabelText="Image Upper Left Latitude"
                    onChange={this.handleFormChange}
                    defaultValue={filesConfig.siteImageUlLat}
                    onBlur={(e) => this.validateInt(e, -90, 90, 'ulLatErrorText')}
                    errorText={this.state.ulLatErrorText}
                  />
                  <TextField
                    name="siteImageUlLng"
                    style={{width: '100%'}}
                    floatingLabelText="Image Upper Left Longitude"
                    onChange={this.handleFormChange}
                    defaultValue={filesConfig.siteImageUlLng}
                    onBlur={(e) => this.validateInt(e, -180, 180, 'ulLngErrorText')}
                    errorText={this.state.ulLngErrorText}
                  />
                  <TextField
                    name="siteImageLlLat"
                    style={{width: '100%'}}
                    floatingLabelText="Image Lower Left Latitude"
                    onChange={this.handleFormChange}
                    defaultValue={filesConfig.siteImageLlLat}
                    onBlur={(e) => this.validateInt(e, -90, 90, 'llLatErrorText')}
                    errorText={this.state.llLatErrorText}
                  />
                  <TextField
                    name="siteImageLlLng"
                    style={{width: '100%'}}
                    floatingLabelText="Image Lower Left Longitude"
                    onChange={this.handleFormChange}
                    defaultValue={filesConfig.siteImageLlLng}
                    onBlur={(e) => this.validateInt(e, -180, 180, 'llLngErrorText')}
                    errorText={this.state.llLngErrorText}
                  />
                </Dropzone>
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Engineering Spec</h5>
                <br/>
                Set the engineering specifications for this site
              </div>
              <Paper style={paperStyle}>
                <Dropzone
                  onDrop={(files) => {
                    this.setState({
                      specFileName: files[0].name,
                      specFile: files[0],
                    })
                    this.hasChanged = true
                  }}
                  ref={
                    (node) => {
                      specDropzoneRef = node
                    }
                  }
                  disableClick={true}
                  activeStyle={{}}
                  multiple={false}
                >
                  <div>
                    <TextField
                      name="imageFile"
                      style={{
                        marginRight: '1em',
                        width: '18.5em'
                      }}
                      floatingLabelText="Engineering Spec File"
                      floatingLabelStyle={{
                        color: 'black',
                      }}
                      value={this.state.specFileName}
                      disabled={false}
                      inputStyle={{
                        color: 'rgba(0, 0, 0, 0.3)',
                        WebkitTextFillColor: 'rgba(0, 0, 0, 0.3)',
                        disabled: 'true'
                      }}
                      onClick={() => {
                        specDropzoneRef.open()
                      }}
                      onFocus={(e) => {
                        e.target.blur()
                      }}
                    />
                    <RaisedButton
                      label='Select File...'
                      primary={true}
                      onClick={
                        () => {
                          specDropzoneRef.open()
                        }
                      }
                    />
                  </div>
                </Dropzone>
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Network Controller Configuration</h5>
                <br/>
                Load a previously saved Network Controller configuration
              </div>
              <Paper style={paperStyle}>
                <Dropzone
                  onDrop={(files) => {
                    this.setState({
                      configFileName: files[0].name,
                      configFile: files[0],
                    })
                    this.hasChanged = true
                  }}
                  ref={
                    (node) => {
                      configDropzoneRef = node
                    }
                  }
                  disableClick={true}
                  activeStyle={{}}
                  multiple={false}
                >
                  <div>
                    <TextField
                      name="configFile"
                      style={{
                        marginRight: '1em',
                        width: '18.5em'
                      }}
                      floatingLabelText="Network Controller Configuration File"
                      floatingLabelStyle={{
                        color: 'black',
                      }}
                      value={this.state.configFileName}
                      disabled={false}
                      inputStyle={{
                        color: 'rgba(0, 0, 0, 0.3)',
                        WebkitTextFillColor: 'rgba(0, 0, 0, 0.3)',
                        disabled: 'true'
                      }}
                      onClick={() => {
                        configDropzoneRef.open()
                      }}
                      onFocus={(e) => {
                        e.target.blur()
                      }}
                    />
                    <RaisedButton
                      label='Select File...'
                      primary={true}
                      onClick={
                        () => {
                          configDropzoneRef.open()
                        }
                      }
                    />
                  </div>
                </Dropzone>
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>System Software Upgrade File</h5>
                <br/>
                Allows remote upgrading of the system software on this Network Controller
              </div>
              <Paper style={paperStyle}>
                <Dropzone
                  onDrop={(files) => {
                    this.setState({
                      upgradeFileName: files[0].name,
                      upgradeFile: files[0],
                    })
                    this.hasChanged = true
                  }}
                  ref={
                    (node) => {
                      upgradeDropzoneRef = node
                    }
                  }
                  disableClick={true}
                  activeStyle={{}}
                  multiple={false}
                >
                  <div>
                    <TextField
                      name="upgradeFile"
                      style={{
                        marginRight: '1em',
                        width: '18.5em'
                      }}
                      floatingLabelText="System Upgrade File"
                      floatingLabelStyle={{
                        color: 'black',
                      }}
                      value={this.state.upgradeFileName}
                      disabled={false}
                      inputStyle={{
                        color: 'rgba(0, 0, 0, 0.3)',
                        WebkitTextFillColor: 'rgba(0, 0, 0, 0.3)',
                        disabled: 'true'
                      }}
                      onClick={() => {
                        upgradeDropzoneRef.open()
                      }}
                      onFocus={(e) => {
                        e.target.blur()
                      }}
                    />
                    <RaisedButton
                      label='Select File...'
                      primary={true}
                      onClick={() => {
                        upgradeDropzoneRef.open()
                      }}
                    />
                  </div>
                </Dropzone>
              </Paper>
            </div>

            <div style={{
              maxWidth: '50em',
              margin: '0px auto 2rem'
            }}>
              <div style={{
                textAlign: 'right',
                display: 'flex',
                flexDirection: 'row-reverse',
                alignItems: 'center'
              }}>
                <RaisedButton
                  label="Update"
                  primary={true}
                  style={{margin: 14}}
                  type="submit"
                  disabled={this.state.updateButtonDisabled}
                />
                <CircularProgress
                  size={30}
                  style={{
                    display: this.state.progressDisplay
                  }}
                />
              </div>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Commissioning Manifest</h5>
                <br/>
                Download the commissioning manifest file to use with the Field Commissioning Software
              </div>
              <Paper style={paperStyle}>
                <div style={{
                  textAlign: 'right',
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: '1.25em'
                }}>
                  <div
                    style={{
                      width: '100%',
                      textAlign: 'left'
                    }}
                  >
                    Download commissioning manifest file
                  </div>
                  <a
                    href="commissioning.manifest"
                    download="commissioning.manifest"
                  >
                    <RaisedButton
                      label='Download'
                      primary={true}
                      style={{
                        width: '8em'
                      }}
                    />
                  </a>
                </div>
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Network Controller Configuration</h5>
                <br/>
                Download the complete configuration of this Network Controller for backup/restoral purposes
              </div>
              <Paper style={paperStyle}>
                <div style={{
                  textAlign: 'right',
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: '1.25em'
                }}>
                  <div
                    style={{
                      width: '100%',
                      textAlign: 'left'
                    }}
                  >
                    Download entire Network Controller configuration in a single file
                  </div>
                  <a
                    href="nc.config"
                    download="nc.config"
                  >
                    <RaisedButton
                      label='Download'
                      primary={true}
                      style={{
                        width: '8em'
                      }}
                    />
                  </a>
                </div>
              </Paper>
            </div>

          </form>

          <Snackbar
            open={this.state.snackbarOpen}
            message="Configuration updated"
            autoHideDuration={4000}
            onRequestClose={this.handleSnackbarRequestClose}
            bodyStyle={{textAlign: 'center'}}
          />

          <Dialog
            actions={[
              <RaisedButton
                label="OK"
                primary={true}
                onTouchTap={this.closeErrorDialg}
              />,
            ]}
            modal={false}
            open={this.state.errorDialogOpen}
            onRequestClose={this.closeErrorDialg}
          >
            {this.state.errorMessage}
          </Dialog>
        </div>
      </div>
    )
  }
}

const UPDATE_SITE_CONFIG_FILES = gql`
mutation updateSiteConfigFiles($image: FileUpload, $spec: FileUpload, $config: FileUpload, $upgrade: FileUpload, $data: SiteConfigInput) {
  updateConfig(siteImageFile: $image, engineeringSpec: $spec, configData: $data) {
    config {
      siteImageUlLat
      siteImageUlLng
      siteImageLlLat
      siteImageLlLng
      engineeringSpec
    }
  } upgrade(upgradeFile: $upgrade) {
    received
  } importConfig(configFile: $config) {
    received
  }
}
`

const configFilesQuery = gql`
   query ConfigFilesQuery {
     config {
      siteImageUlLat
      siteImageUlLng
      siteImageLlLat
      siteImageLlLng
      engineeringSpec
     }
   }
`

// Bundle query, mutation, and target component together as a Higher Order Component (HOC)
export const ConfigFilesPage = compose(
  graphql(configFilesQuery, {name: 'filesConfig'}),
  graphql(UPDATE_SITE_CONFIG_FILES, {
    props: ({ownProps, mutate}) => ({
      submit: (imageFile, specFile, configFile, upgradeFile, data) => mutate({
        variables: {
          image: imageFile,
          spec: specFile,
          config: configFile,
          upgrade: upgradeFile,
          data: data
        }
      }),
    }),
    options: {
      refetchQueries: [
        {query: configFilesQuery}
      ],
    }
  })
)(withRouter(ConfigFiles))

export default ConfigFilesPage
