import React, { Component } from 'react'
import PropTypes from 'prop-types'
import L from 'leaflet'
import 'leaflet/dist/leaflet.css'

// Point leaflet to local images for default markers
// -- no longer needed since we create custom icons...
//L.Icon.Default.imagePath =  'images/'

// Base class for custom Leaflet marker icons
class BaseIcon extends L.Icon {
  constructor(baseImage, imagePath) {
    const options = {
      iconUrl: imagePath + '/' + baseImage + '.png',
      iconRetinaUrl: imagePath + '/' + baseImage + '-2x.png',
      shadowUrl : imagePath + '/marker-shadow.png',
      iconSize : [25, 41],
      shadowSize : [41, 41],
      iconAnchor : [12, 41],
      popupAnchor : [1, -34],
      tooltipAnchor : [16, -28]
    }

    super(options)
  }
}

const greenIcon = new BaseIcon('marker-icon-grn', 'images')
const selectedIcon = new BaseIcon('marker-icon-grn-sel', 'images')
//const blueIcon = new BaseIcon('marker-icon', 'images')
const blueTransIcon = new BaseIcon('marker-icon-blutrans', 'images')

class MapMarker {
  constructor(latlon) {
    this.location = latlon
    this.text = ''
    this.icon = blueTransIcon
    this.draggable = false
  }

}

class MapPanel extends Component {
  static propTypes ={
    curLoc: PropTypes.array.isRequired,
    curBounds: PropTypes.array.isRequired,
    curOverlay: PropTypes.any,
    showStreets: PropTypes.bool,
  }

  constructor(props) {
    super(props)
    this.map = null
    this.imageOverlay = null
    this.streetsLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png')
    this.leafletMarkers = []
  }

  // The following is test code so I can easily grab GPS coordinates for simulated units
  onMapClick = (e) => {
    //alert("You clicked the map at " + e.latlng)
    L.popup()
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.latlng.toString())
        .openOn(this.map);  }

  baseLayer() {
    this.map = L.map('map', {
      minZoom: 12,
      maxZoom: 20,
      attributionControl: false,
    })
    
    this.map.setView(this.props.curLoc, 18);
    // The following is test code so I can easily grab GPS coordinates for simulated units
    this.map.on('click', this.onMapClick)
  }

  streets() {
    if (this.props.showStreets) {
	    this.streetsLayer.addTo(this.map);
    } else {
      this.streetsLayer.remove()
    }
  }

  overlay() {
    if (this.props.curOverlay) {
      this.imageOverlay =  L.imageOverlay(this.props.curOverlay, this.props.curBounds)
      this.imageOverlay.addTo(this.map)
    }
    else {
      this.imageOverlay.remove()
    }
  }

  erasePreviousMarkers() {
    for (const m of this.leafletMarkers) {
      this.map.removeLayer(m)
    }
    this.leafletMarkers = []
  }

  markers() {
    this.leafletMarkers = []
    // Drop some markers at each discovered device
    for (const m of this.props.markers) {
      let leafletMarker = L.marker(m.location, {
        draggable: m.draggable,
        icon: m.icon,
        title: m.title
      })
      leafletMarker.addTo(this.map).bindPopup(m.text)
      // We are keeping track of them so we can erase them later
      this.leafletMarkers.push(leafletMarker)
    }
  }

  locCursor() {
    // And a location circle
    L.circleMarker(this.props.curLoc, {radius: 5, fill: true, fillOpacity: 0.8}).addTo(this.map)
  }

  componentWillReceiveProps(nextProps) {
  }

  componentDidMount() {
    // code to run just after the component "mounts" / DOM elements are created
    this.baseLayer()
    this.streets()
    this.overlay()
    this.markers()
    this.locCursor()

  }

  componentWillUnmount() {
    // code to run just before unmounting the component
    // destroy the Leaflet map object & related event listeners
    if (this.map) {
      this.map.remove()
    }
  }

  componentDidUpdate(prevProps, prevState) {
    // code to run when the component receives new props or state
    // This is where we'd update map based on prop changes
    if (this.props.showStreets !== prevProps.showStreets) {
      this.streets()
    }

    if (this.props.curOverlay !== prevProps.curOverlay) {
      this.overlay()
    }

    if (this.props.curLoc !== prevProps.curLoc) {
      this.locCursor()
    }

    if (this.props.markers !== prevProps.markers) {
      this.erasePreviousMarkers()
      this.markers()
    }
  }

  render() {
    return (
      <div id="map" style={{width:'100%', height:'100%'}}>
      </div>
    )
  }
}

export default MapPanel
export {greenIcon, selectedIcon, MapMarker}
