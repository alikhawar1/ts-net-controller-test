// NcAssetList - Lists of Asset information
import React from 'react'
import PropTypes from 'prop-types'
import Paper from 'material-ui/Paper'
import Checkbox from 'material-ui/Checkbox'
import RaisedButton from 'material-ui/RaisedButton'
import CircularProgress from 'material-ui/CircularProgress'
import Snackbar from 'material-ui/Snackbar'
import Dialog from 'material-ui/Dialog'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import TextField from 'material-ui/TextField'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {gql, graphql, compose} from 'react-apollo'
import {refetchTimeout} from './LoadingCard'


let muiTheme = getMuiTheme()


class ConfigAssets extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
    activateMutation: PropTypes.func.isRequired,
    deactivateMutation: PropTypes.func.isRequired,
    deleteMutation: PropTypes.func.isRequired,
    getLogMutation: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      // Controls
      provisionButtonDisabled: true,
      unprovisionButtonDisabled: true,
      deleteButtonDisabled: true,
      getLogButtonDisabled: true,
      checkAllChecked: false,
      // Filters
      unprovisionedOnly: false,
      provisionedOnly: false,
      searchText: '',
      // Status
      progressDisplay: 'none',
      snackbarOpen: false,
      errorDialogOpen: false,
    }
    this.checkedIds = []
    this.activateMutation = this.props.activateMutation
    this.deactivateMutation = this.props.deactivateMutation
    this.deleteMutation = this.props.deleteMutation
    this.getLogMutation = this.props.getLogMutation
    this.errorTimer = null
  }

  handleSelectAll = (ev) => {
    const doSelect = ev.target.checked

    this.checkedIds.length = 0
    if (doSelect) {
      this.filteredAssets.forEach((item, index) => {
        this.checkedIds.push(item.id)
      })
    }

    this.setState({
      checkAllChecked: doSelect
    })

    this.updateButtonStates()
  }

  handleSearchTextChange = (ev) => {
    this.checkedIds.length = 0 // make any visibility changes clear any selections...
    this.setState({searchText: ev.target.value, checkAllChecked: false})
  }

  aggregateAndFilterAssets = (asset) => {
    // Simple, client-side aggregation and filtering of whole dataset

    let filteredDiscoveredAssets = []
    if (this.state.provisionedOnly === false) {
      filteredDiscoveredAssets = this.props.data.discoveredAssets.filter(asset => {
        const assetStr = Object.values(asset).join(' ').toLowerCase()
        return assetStr.includes(this.state.searchText.toLowerCase())
      })
    }

    let filteredAssets = []
    if (this.state.unprovisionedOnly === false) {
      filteredAssets = this.props.data.assets.filter(asset => {
        const evStr = Object.values(asset).join(' ').toLowerCase()
        return evStr.includes(this.state.searchText.toLowerCase())
      })
    }

    this.filteredAssets = filteredDiscoveredAssets.concat(filteredAssets)
  }

  updateButtonStates = () => {
    // If nothing checked, all buttons are disabled
    let new_provisionButtonDisabled_value = true
    let new_unprovisionButtonDisabled_value = true
    let new_deleteButtonDisabled_value = true
    let new_getLogButtonDisabled_value = true
    // If something checked, some buttons MAY get enabled
    if (this.checkedIds.length !== 0) {
      new_getLogButtonDisabled_value = false
      let discovered_only = true
      let active_only = true
      this.props.data.discoveredAssets.forEach((discovered_item, index1) => {
        this.checkedIds.forEach((checked_item_id, index2) => {
          if (discovered_item.id === checked_item_id) {
            active_only = false
          }
        })
      })
      this.props.data.assets.forEach((active_item, index1) => {
        this.checkedIds.forEach((checked_item_id, index2) => {
          if (active_item.id === checked_item_id) {
            discovered_only = false
          }
        })
      })
      if (discovered_only) {
        new_provisionButtonDisabled_value = false
        new_deleteButtonDisabled_value = false
      } else if (active_only) {
        new_unprovisionButtonDisabled_value = false
      }
    }

    this.setState({
      provisionButtonDisabled: new_provisionButtonDisabled_value,
      unprovisionButtonDisabled: new_unprovisionButtonDisabled_value,
      deleteButtonDisabled: new_deleteButtonDisabled_value,
      getLogButtonDisabled: new_getLogButtonDisabled_value,
    })
  }

  handleChecked = (event, asset) => {
    if (this.checkedIds.includes(asset.id)) {
      this.checkedIds = this.checkedIds.filter((id) => id !== asset.id)
    } else {
      this.checkedIds.push(asset.id)
    }

    this.updateButtonStates()
  }

  closeErrorDialg = () => {
    this.setState({errorDialogOpen: false})
  }

  handleProvision = (event) => {
    if (this.checkedIds.length > 0) {
      this.setState({
        provisionButtonDisabled: true,
        deleteButtonDisabled: true,
        getLogButtonDisabled: true,
        progressDisplay: 'inline'
      })
      this.activateMutation(this.checkedIds).then((data) => {
        this.setState({
          snackbarOpen: true,
          progressDisplay: 'none'
        })
        this.props.data.refetch()
        this.checkedIds.length = 0
      }).catch((err) => {
        console.log(err)
        this.setState({
          errorDialogOpen: true,
          progressDisplay: 'none'
        })
        this.checkedIds.length = 0
      })
    }
  }

  handleUnprovision = (event) => {
    if (this.checkedIds.length > 0) {
      this.setState({
        unprovisionButtonDisabled: true,
        getLogButtonDisabled: true,
        progressDisplay: 'inline'
      })
      this.deactivateMutation(this.checkedIds).then((data) => {
        this.setState({
          snackbarOpen: true,
          progressDisplay: 'none'
        })
        this.props.data.refetch()
        this.checkedIds.length = 0
      }).catch((err) => {
        console.log(err)
        this.setState({
          errorDialogOpen: true,
          progressDisplay: 'none'
        })
        this.checkedIds.length = 0
      })
    }
  }

  handleDelete = (event) => {
    if (this.checkedIds.length > 0) {
      this.setState({
        provisionButtonDisabled: true,
        deleteButtonDisabled: true,
        getLogButtonDisabled: true,
        progressDisplay: 'inline'
      })
      this.deleteMutation(this.checkedIds).then((data) => {
        this.setState({
          snackbarOpen: true,
          progressDisplay: 'none'
        })
        this.props.data.refetch()
        this.checkedIds.length = 0
      }).catch((err) => {
        console.log(err)
        this.setState({
          errorDialogOpen: true,
          progressDisplay: 'none'
        })
        this.checkedIds.length = 0
      })
    }
  }

  handleGetLog = (event) => {
    if (this.checkedIds.length > 0) {
      this.setState({
        provisionButtonDisabled: true,
        unprovisionButtonDisabled: true,
        deleteButtonDisabled: true,
        getLogButtonDisabled: true,
        progressDisplay: 'inline'
      })
      this.getLogMutation(this.checkedIds).then((data) => {
        this.setState({
          snackbarOpen: true,
          progressDisplay: 'none'
        })
        this.props.data.refetch()
        this.checkedIds.length = 0
      }).catch((err) => {
        console.log(err)
        this.setState({
          errorDialogOpen: true,
          progressDisplay: 'none'
        })
        this.checkedIds.length = 0
      })
    }
  }

  render() {
    const columns = [
      {
        Header: (
          <Checkbox
            style={{margin: -3, marginLeft: -12}}
            onClick={this.handleSelectAll}
            checked={this.state.checkAllChecked}
          />
        ),
        width: 35,
        sortable: false,
        resizable: false,
        Cell: (row) => (
          <Checkbox
            onCheck={(event) => this.handleChecked(event, row.original)}
            checked={this.checkedIds.includes(row.original.id)}  // Needed because how react reuses DOM elements?
          />
        )
      },
      {
        Header: 'Device ID',
        accessor: 'id',
        width: 95,
      }, {
        Header: 'Config Status',
        id: 'configStatus',
        width: 120,
        accessor: (data) => {
          let config_status = 'Unprovisioned'
          let filteredAssets = this.props.data.assets.filter(asset => {
            return data.id === asset.id
          })
          if (filteredAssets.length !== 0) {
            config_status = 'Provisioned'
          }
          return config_status
        }
      }, {
        Header: 'Type',
        accessor: 'device',
        width: 100,
      }, {
        Header: 'Location',
        accessor: 'locationText',
      }, {
        Header: 'Poll Responses',
        id: 'pollResponses',
        width: 130,
        accessor: (data) => {
          let radioResponses = 0
          if (data.radioPollsSent) {
            radioResponses = data.radioPollResponses / data.radioPollsSent
          }
          return radioResponses.toFixed(2)
        }
      }
    ]

    if (this.props.data.error) {
      if (this.errorTimer === null) {
        this.errorTimer = setTimeout(() => {
          this.errorTimer = null
          this.props.data.refetch()
        }, refetchTimeout)
      }
    } else if (!this.props.data.loading) {
      this.aggregateAndFilterAssets()
    }

    return (
      <div
        style={{
          margin: '2em'
        }}
      >
        <div style={{
          fontSize: "1.2em",
          fontFamily: muiTheme.fontFamily,
          paddingBottom: '2rem',
        }}>
          Configure Assets
        </div>
        <Paper
          style={{
            width: '100%',
          }}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              paddingLeft: '1em',
              paddingRight: '1em',
              paddingBottom: '1em',
            }}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'row',
                // justifyContent: 'flex-end'
              }}
            >
              <div
                style={{
                  justifyContent: 'center',
                  display: 'flex',
                  flexDirection: 'column',
                  minWidth: '7em'
                }}
              >
                <RaisedButton
                  label="Provision"
                  primary={true}
                  onClick={() => this.handleProvision()}
                  disabled={this.state.provisionButtonDisabled}
                />
              </div>
              <div
                style={{
                  paddingLeft: '1em',
                  justifyContent: 'center',
                  display: 'flex',
                  flexDirection: 'column',
                  minWidth: '8em'
                }}
              >
                <RaisedButton
                  label="Unprovision"
                  primary={true}
                  onClick={() => this.handleUnprovision()}
                  disabled={this.state.unprovisionButtonDisabled}
                />
              </div>
              <div
                style={{
                  paddingLeft: '1em', // because there is another button to the left of this one...
                  justifyContent: 'center',
                  display: 'flex',
                  flexDirection: 'column',
                  minWidth: '5em'
                }}
              >
                <RaisedButton
                  label="Delete"
                  primary={true}
                  onClick={() => this.handleDelete()}
                  disabled={this.state.deleteButtonDisabled}
                />
              </div>
              <div
                style={{
                  paddingLeft: '1em', // because there is another button to the left of this one...
                  justifyContent: 'center',
                  display: 'flex',
                  flexDirection: 'column',
                  minWidth: '6em'
                }}
              >
                <RaisedButton
                  label="Get Log"
                  primary={true}
                  onClick={() => this.handleGetLog()}
                  disabled={this.state.getLogButtonDisabled}
                />
              </div>
              <div
                style={{
                  marginRight: 'auto',
                  justifyContent: 'center',
                  display: 'flex',
                  flexDirection: 'column',
                  paddingLeft: '1em',
                }}
              >
                <CircularProgress
                  size={30}
                  style={{
                    display: this.state.progressDisplay
                  }}
                />
              </div>
              {/* Unprovisioned-only filter */}
              <div style={{marginTop: 15, marginRight: 40}}>
                <Checkbox
                  style={{whiteSpace: 'nowrap'}}
                  label="Unprovisioned only"
                  onClick={(ev) => {
                    this.checkedIds.length = 0 // make any visibility changes clear any selections...
                    this.setState({unprovisionedOnly: ev.target.checked, checkAllChecked: false})
                  }}
                />
              </div>
              {/* Provisioned-only filter */}
              <div style={{marginTop: 15, marginRight: 40}}>
                <Checkbox
                  style={{whiteSpace: 'nowrap'}}
                  label="Provisioned only"
                  onClick={(ev) => {
                    this.checkedIds.length = 0 // make any visibility changes clear any selections...
                    this.setState({provisionedOnly: ev.target.checked, checkAllChecked: false})
                  }}
                />
              </div>
              {/* Search box */}
              <div
                style={{
                  marginTop: 2,
                  paddingRight: '1em',
                }}
              >
                <TextField
                  hintText="Search..."
                  onChange={this.handleSearchTextChange}
                />
              </div>
            </div>
            {/* Table of Assets */}
            <div
              style={{
                backgroundColor: 'white',
              }}
            >
              <ReactTable
                className="-striped -highlight"
                loading={this.props.data.loading || this.props.data.error}
                data={this.filteredAssets}
                columns={columns}
                NoDataComponent={() => {
                  return (
                    <Paper
                      className='rt-noData'
                    >
                      <div>No matching assets found</div>
                    </Paper>
                  )
                }}
              />
            </div>
          </div>

        </Paper>
        <Snackbar
          open={this.state.snackbarOpen}
          message="Configuration changed"
          autoHideDuration={4000}
          onRequestClose={this.handleSnackbarRequestClose}
          bodyStyle={{textAlign: 'center'}}
        />
        <Dialog
          actions={[
            <RaisedButton
              label="OK"
              primary={true}
              onTouchTap={this.closeErrorDialg}
            />,
          ]}
          modal={false}
          open={this.state.errorDialogOpen}
          onRequestClose={() => this.setState({errorDialogOpen: false})}
        >
          An error occurred while updating the settings. Please verify your values and try again
        </Dialog>
      </div>
    )
  }
}

const statusQuery = gql`
  query AssetStatusQuery {
    discoveredAssets {
      id
      lastReported
      model
      device
      statuses
      locationText
      radioPollsSent
      radioPollResponses
    }
    assets {
      id
      lastReported
      model
      device
      statuses
      locationText
      radioPollsSent
      radioPollResponses
    }
  }
`

const activateMutation = gql`
mutation move($assetIDs: [ID])  {
  activateDiscoveredAsset(ids: $assetIDs) {
    activated
  }
}
`

const deactivateMutation = gql`
mutation move($assetIDs: [ID])  {
  deactivateActiveAsset(ids: $assetIDs) {
    deactivated
  }
}
`

const deleteMutation = gql`
mutation delete($assetIDs: [ID])  {
  deleteDiscoveredAsset(ids: $assetIDs) {
    deleted
  }
}
`

const getLogMutation = gql`
mutation getLog($assetIDs: [ID])  {
  getLogFromAsset(ids: $assetIDs) {
    requested
  }
}
`

// Bundle query and target component as Higher Order Component (HOC)
export const ConfigAssetsPage = compose(
  graphql(statusQuery, {
    options: {pollInterval: 5000},
  }),
  graphql(activateMutation, {
    props: ({ownProps, mutate}) => ({
      activateMutation: (ids) => mutate({
        variables: {assetIDs: ids},
      }),
    }),
  }),
  graphql(deactivateMutation, {
    props: ({ownProps, mutate}) => ({
      deactivateMutation: (ids) => mutate({
        variables: {assetIDs: ids},
      }),
    }),
  }),
  graphql(deleteMutation, {
    props: ({ownProps, mutate}) => ({
      deleteMutation: (ids) => mutate({
        variables: {assetIDs: ids},
      }),
    }),
  }),
  graphql(getLogMutation, {
    props: ({ownProps, mutate}) => ({
      getLogMutation: (ids) => mutate({
        variables: {assetIDs: ids},
      }),
    }),
  })
)(ConfigAssets);

export default ConfigAssetsPage