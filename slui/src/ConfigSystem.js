// System Config page for SLUI


import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {gql, graphql, compose} from 'react-apollo'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import Checkbox from 'material-ui/Checkbox'
import Snackbar from 'material-ui/Snackbar'
import CircularProgress from 'material-ui/CircularProgress'
import validator from 'validator'
import Dialog from 'material-ui/Dialog'
import Paper from 'material-ui/Paper'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {headerQuery} from './NcHeaderInfo'
import {withRouter} from 'react-router-dom'
import {paperStyle, sectionStyle, indentStyle, titleStyle, descriptionStyle} from './styles'
import {CardLoadingIndicator, CardErrorIndicator} from './LoadingCard'


let muiTheme = getMuiTheme()


class ConfigSystem extends Component {
  static propTypes = {
    siteConfig: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
    match: PropTypes.object.isRequired,  // injected by react-router-dom withRouter
    location: PropTypes.object.isRequired,  // injected by react-router-dom withRouter
    history: PropTypes.object.isRequired  // injected by react-router-dom withRouter
  }

  constructor(props) {
    super(props)
    this.updateConfig = props.submit
    this.hasChanged = false
    this.updatedSiteConfigVals = {}
    this.updatedWeatherMonitorVals = {}
    this.updatedAssetUpdaterVals = {}
    this.updatedCloudVals = {}
    this.state = {
      snackbarOpen: false,
      progressDisplay: 'none',
      updateButtonDisabled: false,
      gpsLatErrorText: '',
      gpsLngErrorText: '',
      gpsAltErrorText: '',
      powerOffErrorText: '',
      powerOnErrorText: '',
      snowDepthThresholdErrorText: '',
      panelSnowDepthThresholdErrorText: '',
      errorDialogOpen: false,
      windAvgDisplay: null,
      windGustDisplay: null,
      // Portions of the UI are conditionally hidden
      snowDepthStowDisplay: null,
      panelSnowDepthStowDisplay: null,
      autoResumePanelStowDisplay: null,
      autoResumeStowDisplay: null,
      nightlyShutdownDisplay: null,
      lowPowerShutdownDisplay: null,
    }
    this.history = props.history
    this.unblock = null
  }

  handleSubmit = (event) => {
    event.preventDefault()  // prevent the default form submit behaviour
    if (this.hasChanged) {
      this.setState({
        updateButtonDisabled: true,
        progressDisplay: 'inline'
      })
      this.updateConfig(this.updatedSiteConfigVals,
                        this.updatedWeatherMonitorVals,
                        this.updatedAssetUpdaterVals,
                        this.updatedCloudVals).then((data) => {
        // console.log("Updated site config")
        this.setState({
          snackbarOpen: true,
          updateButtonDisabled: false,
          progressDisplay: 'none'
        })
        this.hasChanged = false
        this.updatedSiteConfigVals = {}
        this.updatedWeatherMonitorVals = {}
        this.updatedAssetUpdaterVals = {}
        this.updatedCloudVals = {}
      }).catch((err) => {
        console.log(err)
        this.setState({
          errorDialogOpen: true,
          updateButtonDisabled: false,
          progressDisplay: 'none'
        })
      })
    }
  }

  handleCheckboxChange = (event) => {
    this.updatedSiteConfigVals[event.target.name] = event.target.checked
    this.hasChanged = true

    if (event.target.name === 'enableNightlyShutdown') {
      this.setState({
        nightlyShutdownDisplay: event.target.checked
      })
    }

    if (event.target.name === 'enableLowPowerShutdown') {
      this.setState({
        lowPowerShutdownDisplay: event.target.checked
      })
    }
  }

  handleWeatherCheckboxChange = (event) => {
    this.updatedWeatherMonitorVals[event.target.name] = event.target.checked
    this.hasChanged = true

    if (event.target.name === 'enableWindSpeedStow') {
      this.setState({
        windAvgDisplay: event.target.checked
      })
    } else if (event.target.name === 'enableWindGustStow') {
      this.setState({
        windGustDisplay: event.target.checked
      })
    } else if (event.target.name === 'enableSnowDepthStow') {
      this.setState({
        snowDepthStowDisplay: event.target.checked
      })
    } else if (event.target.name === 'enablePanelSnowDepthStow') {
      this.setState({
        panelSnowDepthStowDisplay: event.target.checked
      })
    } else if (event.target.name === 'enableAutoResumeTrackingSnow') {
      //console.log("enableAutoResumeTrackingSnow = ", event.target.checked)
      if (event.target.checked){
        this.updatedWeatherMonitorVals['resumeTrackingAfterSnowTimeout'] = 1
        this.setState({['resumeTrackingAfterSnowTimeout']: 1})
      }else {
        this.updatedWeatherMonitorVals['resumeTrackingAfterSnowTimeout'] = 0
        this.setState({['resumeTrackingAfterSnowTimeout']: 0})
      }
        //console.log("resumeTrackingAfterSnowTimeout = ", this.updatedWeatherMonitorVals['resumeTrackingAfterSnowTimeout'])
       const weatherMonitorconfig = this.props.siteConfig.weatherMonitor
       if (weatherMonitorconfig.snowDepthLowThreshold == null ){
            this.updatedWeatherMonitorVals['snowDepthLowThreshold'] =  weatherMonitorconfig.snowDepthThreshold
        }
        else{
            this.updatedWeatherMonitorVals['snowDepthLowThreshold'] =  weatherMonitorconfig.snowDepthLowThreshold
        }
      this.setState({
        autoResumeStowDisplay: event.target.checked
      })
    } else if (event.target.name === 'enableAutoResumeTrackingPanelSnow') {
        if (event.target.checked){
            this.updatedWeatherMonitorVals['resumeTrackingAfterPanelSnowTimeout'] = 1
            this.setState({['resumeTrackingAfterPanelSnowTimeout']: 1})
      }else {
            this.updatedWeatherMonitorVals['resumeTrackingAfterPanelSnowTimeout'] = 0
            this.setState({['resumeTrackingAfterPanelSnowTimeout']: 0})
      }
            const weatherMonitorconfig = this.props.siteConfig.weatherMonitor
            if (weatherMonitorconfig.panelSnowDepthLowThreshold == null ){
                this.updatedWeatherMonitorVals['panelSnowDepthLowThreshold'] =  weatherMonitorconfig.panelSnowDepthThreshold
            }
            else{
                this.updatedWeatherMonitorVals['panelSnowDepthLowThreshold'] =  weatherMonitorconfig.panelSnowDepthLowThreshold
            }
      this.setState({
        autoResumePanelStowDisplay: event.target.checked
      })
    }

  }

  handleFormChange = (event) => {
    this.updatedSiteConfigVals[event.target.name] = event.target.value
    this.hasChanged = true
  }

  handleWeatherFormChange = (event) => {
    const {name, value}= event.target;
    this.setState({[name]: value})
    this.updatedWeatherMonitorVals[event.target.name] = event.target.value
    this.hasChanged = true
  }

  handleAssetUpdaterFormChange = (event) => {
    const {name, value}= event.target;
    this.setState({[name]: value})
    this.updatedAssetUpdaterVals[event.target.name] = event.target.value
    this.hasChanged = true
  }

  handleCloudFormChange = (event) => {
    const {name, value}= event.target;
    this.setState({[name]: value})
    this.updatedCloudVals[event.target.name] = event.target.value
    this.hasChanged = true
  }

  handleSnackbarRequestClose = () => {
    this.setState({
      snackbarOpen: false,
    });
  }

  validateInt = (e, min, max, errorTextName) => {
    let errorText = ""
    if (!validator.isFloat(e.currentTarget.value, {min: min, max: parseFloat(max)})) {
      errorText = "Must be a number between " + min + " and " + max
    }
    let stateUpdate = {}
    stateUpdate[errorTextName] = errorText
    this.setState(stateUpdate)
  }

  closeErrorDialg = () => {
    this.setState({errorDialogOpen: false})
  }

  componentWillMount = () => {
    this.unblock = this.history.block((location, action) => {
      if (this.hasChanged) {
        return 'Leaving this page will result in ALL unsaved changes being lost. Are you sure you want to leave?'
      }
    })
  }

  componentWillUnmount = () => {
    this.unblock()
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.state.nightlyShutdownDisplay === null &&
      this.state.lowPowerShutdownDisplay === null &&
      !nextProps.siteConfig.loading &&
      !nextProps.siteConfig.error) {
      this.setState({
        nightlyShutdownDisplay: nextProps.siteConfig.config.enableNightlyShutdown,
        lowPowerShutdownDisplay: nextProps.siteConfig.config.enableLowPowerShutdown,
        windAvgDisplay: nextProps.siteConfig.weatherMonitor.enableWindSpeedStow,
        windGustDisplay: nextProps.siteConfig.weatherMonitor.enableWindGustStow,
        snowDepthStowDisplay: nextProps.siteConfig.weatherMonitor.enableSnowDepthStow,
        panelSnowDepthStowDisplay: nextProps.siteConfig.weatherMonitor.enablePanelSnowDepthStow,
        autoResumeStowDisplay: nextProps.siteConfig.weatherMonitor.enableAutoResumeTrackingSnow,
        autoResumePanelStowDisplay: nextProps.siteConfig.weatherMonitor.enableAutoResumeTrackingPanelSnow,
        minimumStationsRequired: nextProps.siteConfig.weatherMonitor.minimumStationsRequired,
        doUploadBridgeScript: nextProps.siteConfig.assetUpdater.doUploadBridgeScript,
        doUploadBridgeFirmware: nextProps.siteConfig.assetUpdater.doUploadBridgeFirmware,
        doUploadAssetScripts: nextProps.siteConfig.assetUpdater.doUploadAssetScripts,
        doUploadAssetRadioFirmware: nextProps.siteConfig.assetUpdater.doUploadAssetRadioFirmware,
        doUploadAssetStm32Firmware: nextProps.siteConfig.assetUpdater.doUploadAssetStm32Firmware,
        doUploadAssetConfigs: nextProps.siteConfig.assetUpdater.doUploadAssetConfigs,
        windReportingClosePercentage: nextProps.siteConfig.cloud.windReportingClosePercentage,
        windReportingCloseInterval: nextProps.siteConfig.cloud.windReportingCloseInterval,
        windReportingOverInterval: nextProps.siteConfig.cloud.windReportingOverInterval,
        increasedWeatherReportingTimeout: nextProps.siteConfig.cloud.increasedWeatherReportingTimeout,
        snowReportingClosePercentage: nextProps.siteConfig.cloud.snowReportingClosePercentage,
        snowReportingCloseInterval: nextProps.siteConfig.cloud.snowReportingCloseInterval,
        snowReportingOverInterval: nextProps.siteConfig.cloud.snowReportingOverInterval
      })
    }
  }

  render() {
    if (this.props.siteConfig.error) {
      return (<CardErrorIndicator apolloData={this.props.siteConfig} />)
    } else if (this.props.siteConfig.loading) {
      return (<CardLoadingIndicator />)
    }

    const siteConfig = this.props.siteConfig.config
    const weatherMonitorConfig = this.props.siteConfig.weatherMonitor
    const assetUpdaterConfig = this.props.siteConfig.assetUpdater
    const cloudConfig = this.props.siteConfig.cloud

    return (
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center'
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignContent: 'flex-start',
            flex: '1 1 100%'
          }}
        >
          <form onSubmit={this.handleSubmit}>
            <div style={{
              fontSize: "1.2em",
              fontFamily: muiTheme.fontFamily,
              paddingTop: '2rem',
              paddingBottom: '2rem',
              paddingLeft: '2rem'
            }}>
              System Configuration
            </div>


            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Site</h5>
                <br />
                Descriptive information about this site
              </div>
              <Paper style={paperStyle}>
                <TextField name="siteName"
                           style={{width: '100%'}}
                           floatingLabelText="Site Name"
                           onChange={this.handleFormChange}
                           defaultValue={siteConfig.siteName}
                />
                <TextField name="siteContact"
                           style={{width: '100%'}}
                           floatingLabelText="Site Contact"
                           onChange={this.handleFormChange}
                           defaultValue={siteConfig.siteContact}
                           hintText="Name, number, and email"
                />
                <TextField name="siteOrganization"
                           style={{width: '100%'}}
                           floatingLabelText="Organization"
                           onChange={this.handleFormChange}
                           defaultValue={siteConfig.siteOrganization}
                           hintText="Example, LLC."
                />
              </Paper>
            </div>


            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Automatic "Stow" (Offline WX Units)</h5>
                <br/>
                Based on loss of communications with one or more weather stations, all rows can be automatically commanded to
                move into their "stow" position as a precautionary measure
              </div>
              <Paper style={paperStyle}>
                <div
                  style={{
                    marginTop: '1em',
                  }}
                >
                  Minimum online weather stations required
                </div>
                <TextField name="minimumStationsRequired"
                           style={indentStyle}
                           floatingLabelStyle={{width: '100%'}}
                           floatingLabelText='Less than this amount will cause Automatic "Stow"'
                           onChange={this.handleWeatherFormChange}
                           defaultValue={weatherMonitorConfig.minimumStationsRequired}
                           onBlur={(e) => this.validateInt(e, 0, 100, 'minimumStationsRequiredErrorText')}
                           errorText={this.state.minimumStationsRequiredErrorText}
                />
              </Paper>
            </div>


            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Automatic "Stow" (Wind)</h5>
                <br/>
                Based on wind conditions all rows can be automatically commanded to
                move into their "stow" position
              </div>
              <Paper style={paperStyle}>
                <Checkbox
                  label="Enable 1-minute wind speed average"
                  name="enableWindSpeedStow"
                  style={{
                    marginLeft: '-2px',  // Checkbox SVG has 2px border
                  }}
                  onCheck={this.handleWeatherCheckboxChange}
                  defaultChecked={weatherMonitorConfig.enableWindSpeedStow}
                />
                {this.state.windAvgDisplay &&
                <div>
                  <TextField name="windSpeedThreshold"
                             style={indentStyle}
                             floatingLabelText="Maximum average wind speed (MPH)"
                             onChange={this.handleWeatherFormChange}
                             defaultValue={weatherMonitorConfig.windSpeedThreshold}
                             onBlur={(e) => this.validateInt(e, 1, 200, 'stowAvgMphErrorText')}
                             errorText={this.state.stowAvgMphErrorText}
                  />
                  < TextField name="windSpeedDurationRequired"
                              style={indentStyle}
                              floatingLabelText="Minimum duration of wind (minutes)"
                              onChange={this.handleWeatherFormChange}
                              defaultValue={weatherMonitorConfig.windSpeedDurationRequired / 60}
                              onBlur={(e) => this.validateInt(e, 0, 60, 'stowAvgDurationText')}
                              errorText={this.state.stowAvgDurationText}
                  />
                </div>
                }
                <Checkbox
                  label="Enable 1-hour wind gust"
                  name="enableWindGustStow"
                  style={{
                    marginTop: '1em',
                    marginLeft: '-2px',
                  }}
                  onCheck={this.handleWeatherCheckboxChange}
                  defaultChecked={weatherMonitorConfig.enableWindGustStow}
                />
                {this.state.windGustDisplay &&
                <TextField name="windGustThreshold"
                           style={indentStyle}
                           floatingLabelText="Maximum gust wind speed (MPH)"
                           onChange={this.handleWeatherFormChange}
                           defaultValue={weatherMonitorConfig.windGustThreshold}
                           onBlur={(e) => this.validateInt(e, 1, 200, 'gustMphErrorText')}
                           errorText={this.state.gustMphErrorText}
                />
                }
                <div
                  style={{
                    marginTop: '1em',
                  }}
                >
                  Resume tracking
                </div>
                <TextField name="resumeTrackingAfterWindTimeout"
                           style={indentStyle}
                           floatingLabelStyle={{width: '100%'}}
                           floatingLabelText='Minutes before resume tracking after "stow" conditions'
                           onChange={this.handleWeatherFormChange}
                           defaultValue={weatherMonitorConfig.resumeTrackingAfterWindTimeout / 60}
                           onBlur={(e) => this.validateInt(e, 0, 60, 'resumeAfterWindMinErrorText')}
                           errorText={this.state.resumeAfterWindMinErrorText}
                />
              </Paper>
            </div>


            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Automatic "Stow" (Snow)</h5>
                <br/>
                Based on snow depth all rows can be automatically commanded to
                move into their "stow" position. Note that snow on the ground
                and snow on the solar panels are measured and treated separately
              </div>
              <Paper style={paperStyle}>
                <Checkbox
                  label="Enable Stow on Ground snow Depth (Weather Station)"
                  name="enableSnowDepthStow"
                  style={{
                    marginLeft: '-2px',  // Checkbox SVG has 2px border
                  }}
                  onCheck={this.handleWeatherCheckboxChange}
                  defaultChecked={weatherMonitorConfig.enableSnowDepthStow}
                />
                {this.state.snowDepthStowDisplay &&
                <TextField name="snowDepthThreshold"
                  style={indentStyle}
                  floatingLabelStyle={{width: '122%'}}
                  floatingLabelText="Go to snow stow when snow depth is greater than (meters)..."
                  onChange={this.handleWeatherFormChange}
                  defaultValue={weatherMonitorConfig.snowDepthThreshold}
                  onBlur={(e) => this.validateInt(e, 0, 5, 'snowDepthThresholdErrorText')}
                  errorText={this.state.snowDepthThresholdErrorText}
                />
                }
                {this.state.snowDepthStowDisplay &&
                <Checkbox
                  label="Automatically resume tracking based on snow measurements"
                  name="enableAutoResumeTrackingSnow"
                  style={indentStyle}
                  style={{
                    paddingTop: '19px',
                    paddingLeft: '32px',
                  }}
                  onCheck={this.handleWeatherCheckboxChange}
                  defaultChecked={this.state.autoResumeStowDisplay}
                />
                }
                {this.state.snowDepthStowDisplay && ! this.state.autoResumeStowDisplay &&
                <div style={{fontSize: '13px', lineHeight: '24px', width: '362px', height: '72px',
                display: 'inline-block', position: 'relative', backgroundColor: 'transparent',
                fontFamily: 'Roboto, sans-serif', paddingLeft: '5.39em', opacity: '0.4',}}>
               If left disabled, the system will remain in weather stow until the event  is cleared by a user in the SLUI.
              </div>
                }
                {this.state.snowDepthStowDisplay && this.state.autoResumeStowDisplay &&
                <TextField name="snowDepthLowThreshold"
                  style={indentStyle}
                  floatingLabelStyle={{width: '122%'}}
                  floatingLabelText="Remain in snow stow until snow depth is less than (meters)..."
                  onChange={this.handleWeatherFormChange}
                  defaultValue={(weatherMonitorConfig.snowDepthLowThreshold ? weatherMonitorConfig.snowDepthLowThreshold : weatherMonitorConfig.snowDepthThreshold) }
                  onBlur={(e) => this.validateInt(e, 0, (this.state.snowDepthThreshold ? this.state.snowDepthThreshold : weatherMonitorConfig.snowDepthThreshold), 'snowDepthThresholdErrorText')}
                  errorText={this.state.snowDepthThresholdErrorText}
                />
                }
                {this.state.snowDepthStowDisplay && this.state.autoResumeStowDisplay&&
                <TextField name="resumeTrackingAfterSnowTimeout"
                           style={indentStyle}
                           floatingLabelStyle={{width: '122%'}}
                           floatingLabelText='Wait X minutes after snow stow clears before resuming tracking.'
                           onChange={this.handleWeatherFormChange}
                           defaultValue={this.state.resumeTrackingAfterSnowTimeout ? this.state.resumeTrackingAfterSnowTimeout : weatherMonitorConfig.resumeTrackingAfterSnowTimeout / 60}
                           onBlur={(e) => this.validateInt(e, 0, 60, 'resumeAfterSnowMinErrorText')}
                           errorText={this.state.resumeAfterSnowMinErrorText}
                />
                }
                {this.state.snowDepthStowDisplay &&
                <Checkbox
                  label="Enable Averaging Across Multiple Weather Stations"
                  name="enableSnowDepthAveraging"
                  style={indentStyle}
                  onCheck={this.handleWeatherCheckboxChange}
                  defaultChecked={weatherMonitorConfig.enableSnowDepthAveraging}
                />
                }

                <Checkbox
                  label="Enable Stow on Panel Snow Depth (Row Box)"
                  name="enablePanelSnowDepthStow"
                  style={{
                    marginLeft: '-2px',  // Checkbox SVG has 2px border
                    paddingTop: '15px',
                  }}
                  onCheck={this.handleWeatherCheckboxChange}
                  defaultChecked={weatherMonitorConfig.enablePanelSnowDepthStow}
                />
                {this.state.panelSnowDepthStowDisplay &&
                <TextField name="panelSnowDepthThreshold"
                  style={indentStyle}
                  floatingLabelStyle={{width: '122%'}}
                  floatingLabelText="Go to snow stow when snow depth is greater than (meters)..."
                  onChange={this.handleWeatherFormChange}
                  defaultValue={weatherMonitorConfig.panelSnowDepthThreshold}
                  onBlur={(e) => this.validateInt(e, 0, 5, 'panelSnowDepthThresholdErrorText')}
                  errorText={this.state.panelSnowDepthThresholdErrorText}
                />
                }
                {this.state.panelSnowDepthStowDisplay &&
                <Checkbox
                  label="Automatically resume tracking based on panel snow measurements"
                  name="enableAutoResumeTrackingPanelSnow"
                  style={indentStyle}
                  style={{
                    paddingTop: '19px',
                    paddingLeft: '32px',
                  }}
                  onCheck={this.handleWeatherCheckboxChange}
                  defaultChecked={this.state.autoResumePanelStowDisplay}
                />
                }
                {this.state.panelSnowDepthStowDisplay && ! this.state.autoResumePanelStowDisplay &&
                <div style={{fontSize: '13px', lineHeight: '24px', width: '362px', height: '72px',
                display: 'inline-block', position: 'relative', backgroundColor: 'transparent',
                fontFamily: 'Roboto, sans-serif', paddingLeft: '5.39em', opacity: '0.4',}}>
               If left disabled, the system will remain in weather stow until the event  is cleared by a user in the SLUI.
              </div>
                }
                {this.state.panelSnowDepthStowDisplay && this.state.autoResumePanelStowDisplay &&
                <TextField name="panelSnowDepthLowThreshold"
                  style={indentStyle}
                  floatingLabelStyle={{width: '122%'}}
                  floatingLabelText="Remain in snow stow until snow depth is less than (meters)..."
                  onChange={this.handleWeatherFormChange}
                  defaultValue={(weatherMonitorConfig.panelSnowDepthLowThreshold ? weatherMonitorConfig.panelSnowDepthLowThreshold : weatherMonitorConfig.panelSnowDepthThreshold)}
                  onBlur={(e) => this.validateInt(e, 0, (this.state.panelSnowDepthThreshold ? this.state.panelSnowDepthThreshold : weatherMonitorConfig.panelSnowDepthThreshold), 'panelSnowDepthThresholdErrorText')}
                  errorText={this.state.panelSnowDepthThresholdErrorText}
                />
                }
                {this.state.panelSnowDepthStowDisplay && this.state.autoResumePanelStowDisplay &&
                <TextField name="resumeTrackingAfterPanelSnowTimeout"
                           style={indentStyle}
                           floatingLabelStyle={{width: '122%'}}
                           floatingLabelText='Wait X minutes after panel snow stow clears before resuming tracking.'
                           onChange={this.handleWeatherFormChange}
                           defaultValue={this.state.resumeTrackingAfterPanelSnowTimeout ? this.state.resumeTrackingAfterPanelSnowTimeout : weatherMonitorConfig.resumeTrackingAfterPanelSnowTimeout / 60}
                           onBlur={(e) => this.validateInt(e, 0, 60, 'resumeAfterPanelSnowMinErrorText')}
                           errorText={this.state.resumeAfterPanelSnowMinErrorText}
                />
                }
                {this.state.panelSnowDepthStowDisplay &&
                <Checkbox
                  label="Enable Averaging Across Multiple Row Boxes"
                  name="enablePanelSnowDepthAveraging"
                  style={indentStyle}
                  onCheck={this.handleWeatherCheckboxChange}
                  defaultChecked={weatherMonitorConfig.enablePanelSnowDepthAveraging}
                />
                }
              </Paper>
            </div>


            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Cloud Weather Reporting</h5>
                <br/>
                Weather reports to the cloud can be increased in certain conditions, at the
                expense of increasing cellular data usage. Configure your preferences here
              </div>
              <Paper style={paperStyle}>
                <div
                  style={{marginTop: '1em',}}
                >
                  % of Wind Stow Threshold to declare "getting close"
                </div>
                <TextField name="windReportingClosePercentage"
                           style={indentStyle}
                           floatingLabelStyle={{width: '100%'}}
                           floatingLabelText='Percentage of the Wind Stow Threshold'
                           onChange={this.handleCloudFormChange}
                           defaultValue={cloudConfig.windReportingClosePercentage}
                           onBlur={(e) => this.validateInt(e, 1, 100, 'windReportingClosePercentageErrorText')}
                           errorText={this.state.windReportingClosePercentageErrorText}
                />
                <div
                  style={{marginTop: '1em',}}
                >
                  Reporting Interval when "getting close" to Wind Stow
                </div>
                <TextField name="windReportingCloseInterval"
                           style={indentStyle}
                           floatingLabelStyle={{width: '100%'}}
                           floatingLabelText='Reporting Interval in Minutes'
                           onChange={this.handleCloudFormChange}
                           defaultValue={cloudConfig.windReportingCloseInterval}
                           onBlur={(e) => this.validateInt(e, 1, 60, 'windReportingCloseIntervalErrorText')}
                           errorText={this.state.windReportingCloseIntervalErrorText}
                />
                <div
                  style={{marginTop: '1em',}}
                >
                  Reporting Interval when above Wind Stow Threshold
                </div>
                <TextField name="windReportingOverInterval"
                           style={indentStyle}
                           floatingLabelStyle={{width: '100%'}}
                           floatingLabelText='Reporting Interval in Minutes'
                           onChange={this.handleCloudFormChange}
                           defaultValue={cloudConfig.windReportingOverInterval}
                           onBlur={(e) => this.validateInt(e, 1, 60, 'windReportingOverIntervalErrorText')}
                           errorText={this.state.windReportingOverIntervalErrorText}
                />
                <div
                  style={{marginTop: '1em',}}
                >
                  Increased Weather Reporting Timeout
                </div>
                <TextField name="increasedWeatherReportingTimeout"
                           style={indentStyle}
                           floatingLabelStyle={{width: '100%'}}
                           floatingLabelText='Timeout in Minutes'
                           onChange={this.handleCloudFormChange}
                           defaultValue={cloudConfig.increasedWeatherReportingTimeout}
                           onBlur={(e) => this.validateInt(e, 0, 60, 'increasedWeatherReportingTimeoutErrorText')}
                           errorText={this.state.increasedWeatherReportingTimeoutErrorText}
                />
                <div
                  style={{marginTop: '1em',}}
                >
                  % of Snow Stow Threshold to declare "getting close"
                </div>
                <TextField name="snowReportingClosePercentage"
                           style={indentStyle}
                           floatingLabelStyle={{width: '100%'}}
                           floatingLabelText='Percentage of the Snow Stow Threshold'
                           onChange={this.handleCloudFormChange}
                           defaultValue={cloudConfig.snowReportingClosePercentage}
                           onBlur={(e) => this.validateInt(e, 1, 100, 'snowReportingClosePercentageErrorText')}
                           errorText={this.state.snowReportingClosePercentageErrorText}
                />
                <div
                  style={{marginTop: '1em',}}
                >
                  Reporting Interval when "getting close" to Snow Stow
                </div>
                <TextField name="snowReportingCloseInterval"
                           style={indentStyle}
                           floatingLabelStyle={{width: '100%'}}
                           floatingLabelText='Reporting Interval in Minutes'
                           onChange={this.handleCloudFormChange}
                           defaultValue={cloudConfig.snowReportingCloseInterval}
                           onBlur={(e) => this.validateInt(e, 1, 60, 'snowReportingCloseIntervalErrorText')}
                           errorText={this.state.snowReportingCloseIntervalErrorText}
                />
                <div
                  style={{marginTop: '1em',}}
                >
                  Reporting Interval when above Snow Stow Threshold
                </div>
                <TextField name="snowReportingOverInterval"
                           style={indentStyle}
                           floatingLabelStyle={{width: '100%'}}
                           floatingLabelText='Reporting Interval in Minutes'
                           onChange={this.handleCloudFormChange}
                           defaultValue={cloudConfig.snowReportingOverInterval}
                           onBlur={(e) => this.validateInt(e, 1, 60, 'snowReportingOverIntervalErrorText')}
                           errorText={this.state.snowReportingOverIntervalErrorText}
                />
              </Paper>
            </div>


            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Site Fallback GPS</h5>
                <br />
                GPS location and altitude to use if the internal GPS is unavailable for any reason
              </div>
              <Paper style={paperStyle}>
                <TextField name="gpsLat"
                           style={{width: '100%'}}
                           floatingLabelText="Fallback GPS Latitude"
                           onChange={this.handleFormChange}
                           defaultValue={siteConfig.gpsLat}
                           onBlur={(e) => this.validateInt(e, -90, 90, 'gpsLatErrorText')}
                           errorText={this.state.gpsLatErrorText}
                />
                <TextField name="gpsLng"
                           style={{width: '100%'}}
                           floatingLabelText="Fallback GPS Longitude"
                           onChange={this.handleFormChange}
                           defaultValue={siteConfig.gpsLng}
                           onBlur={(e) => this.validateInt(e, -180, 180, 'gpsLngErrorText')}
                           errorText={this.state.gpsLngErrorText}
                />
                <TextField name="gpsAlt"
                           style={{width: '100%'}}
                           floatingLabelText="Fallback GPS Altitude"
                           onChange={this.handleFormChange}
                           defaultValue={siteConfig.gpsAlt}
                           onBlur={(e) => this.validateInt(e, -100, 20000, 'gpsAltErrorText')}
                           errorText={this.state.gpsAltErrorText}
                />
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Nightly Shutdown</h5>
                <br />
                Enabling nightly shutdown conserves the internal battery but
                makes the Network Controller unavailable during the specified
                interval between sunset and sunrise.
              </div>
              <Paper style={paperStyle}>
                <Checkbox
                  label="Enable nightly shutdown of Network Controller"
                  name="enableNightlyShutdown"
                  onCheck={this.handleCheckboxChange}
                  defaultChecked={siteConfig.enableNightlyShutdown}
                />
                {this.state.nightlyShutdownDisplay &&
                <div>
                  <TextField name="powerOff"
                             floatingLabelText="Minutes after sunset before powering off"
                             style={indentStyle}
                             onChange={this.handleFormChange}
                             defaultValue={siteConfig.powerOff}
                             onBlur={(e) => this.validateInt(e, 0, 1440, 'powerOffErrorText')}
                             errorText={this.state.powerOffErrorText}
                  />
                  <TextField name="powerOn"
                             floatingLabelText="Minutes prior to sunrise to power on"
                             style={indentStyle}
                             onChange={this.handleFormChange}
                             defaultValue={siteConfig.powerOn}
                             onBlur={(e) => this.validateInt(e, 0, 1440, 'powerOnErrorText')}
                             errorText={this.state.powerOnErrorText}
                  />
                  <br />&nbsp;
                  <br />&nbsp;
                </div>
                }
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Battery Stretching</h5>
                <br />
                If there is insufficient sunlight for battery charging to keep pace with
                battery utilization, the system can take measures to compensate.
                <br /><br />
                The first potential compromise is turning off the cell modem, which takes
                away data reporting to the cloud as well as disables remote access. It does allow
                the controller to broadcast solar forecasts longer.
                <br /><br />
                In extreme low power situations, the controller can shut everything off until
                the next morning, hoping for sunnier conditions.
                <br /><br />
                The Row Boxes will auto-stow when they no longer hear from the controller.
              </div>
              <Paper style={paperStyle}>
                <Checkbox
                  label="Enable low power shutdowns of sub-systems"
                  name="enableLowPowerShutdown"
                  onCheck={this.handleCheckboxChange}
                  defaultChecked={siteConfig.enableLowPowerShutdown}
                />
                {this.state.lowPowerShutdownDisplay &&
                <div>
                  <TextField name="cellModemWarningVoltage"
                             floatingLabelText="Cell modem cutoff WARNING Voltage"
                             style={indentStyle}
                             onChange={this.handleFormChange}
                             defaultValue={siteConfig.cellModemWarningVoltage}
                             onBlur={(e) => this.validateInt(e, 0, 25, 'cellModemWarningVoltageErrorText')}
                             errorText={this.state.cellModemWarningVoltageErrorText}
                  />
                  <TextField name="cellModemCutoffVoltage"
                             floatingLabelText="Cell modem CUTOFF Voltage"
                             style={indentStyle}
                             onChange={this.handleFormChange}
                             defaultValue={siteConfig.cellModemCutoffVoltage}
                             onBlur={(e) => this.validateInt(e, 0, 25, 'cellModemCutoffVoltageErrorText')}
                             errorText={this.state.cellModemCutoffVoltageErrorText}
                  />
                  <TextField name="cellModemCutonVoltage"
                             floatingLabelText="Cell modem REACTIVATION Voltage"
                             style={indentStyle}
                             onChange={this.handleFormChange}
                             defaultValue={siteConfig.cellModemCutonVoltage}
                             onBlur={(e) => this.validateInt(e, 0, 25, 'cellModemCutonVoltageErrorText')}
                             errorText={this.state.cellModemCutonVoltageErrorText}
                  />
                  <TextField name="gatewayWarningVoltage"
                             floatingLabelText="Controller cutoff WARNING Voltage"
                             style={indentStyle}
                             onChange={this.handleFormChange}
                             defaultValue={siteConfig.gatewayWarningVoltage}
                             onBlur={(e) => this.validateInt(e, 0, 25, 'gatewayWarningVoltageErrorText')}
                             errorText={this.state.gatewayWarningVoltageErrorText}
                  />
                  <TextField name="gatewayCutoffVoltage"
                             floatingLabelText="Controller CUTOFF Voltage"
                             style={indentStyle}
                             onChange={this.handleFormChange}
                             defaultValue={siteConfig.gatewayCutoffVoltage}
                             onBlur={(e) => this.validateInt(e, 0, 25, 'gatewayCutoffVoltageErrorText')}
                             errorText={this.state.gatewayCutoffVoltageErrorText}
                  />
                  <br />&nbsp;
                  <br />&nbsp;
                </div>
                }
              </Paper>
            </div>

            <div style={{
              maxWidth: '50em',
              margin: '0px auto 2rem'
            }}>
              <div style={{
                textAlign: 'right',
                display: 'flex',
                flexDirection: 'row-reverse',
                alignItems: 'center'
              }}>
                <RaisedButton label="Update" primary={true} style={{margin: 14}} type="submit"
                              disabled={this.state.updateButtonDisabled}/>
                <CircularProgress size={30} style={{
                  display: this.state.progressDisplay
                }}/>
              </div>
            </div>
          </form>
          <Snackbar
            open={this.state.snackbarOpen}
            message="System configuration updated"
            autoHideDuration={4000}
            onRequestClose={this.handleSnackbarRequestClose}
            bodyStyle={{textAlign: 'center'}}
          />
          <Dialog
            actions={[
              <RaisedButton
                label="OK"
                primary={true}
                onTouchTap={this.closeErrorDialg}
              />,
            ]}
            modal={false}
            open={this.state.errorDialogOpen}
            onRequestClose={this.closeErrorDialg}
          >
            An error occurred while updating the settings. Please verify your values and try again
          </Dialog>
        </div>
      </div>
    )
  }
}

// Having one big mutation is a consequence of having a single Update button on the page
// Would a button per sub-panel be better?
export const UPDATE_SYSTEM_PAGE_CONFIG = gql`
  mutation updateSystemPageConfig(
    $configData: SiteConfigInput,
    $weatherMonitorConfigData: WeatherMonitorConfigInput,
    $assetUpdaterConfigData: AssetUpdaterConfigInput,
    $cloudConfigData: CloudConfigInput,
  ) {
    updateConfig(configData: $configData) {
      config {
        siteName
      }
    }
    updateWeatherMonitorConfig(configData: $weatherMonitorConfigData) {
      weatherMonitor {
        enableWindSpeedStow
        windSpeedThreshold
        windSpeedDurationRequired
        enableWindGustStow
        windGustThreshold
        enableSnowDepthStow
        enableSnowDepthAveraging
        snowDepthThreshold
        snowDepthLowThreshold
        enableAutoResumeTrackingSnow
        enablePanelSnowDepthStow
        enablePanelSnowDepthAveraging
        panelSnowDepthThreshold
        panelSnowDepthLowThreshold
        enableAutoResumeTrackingPanelSnow
        resumeTrackingAfterWindTimeout
        resumeTrackingAfterSnowTimeout
        resumeTrackingAfterPanelSnowTimeout
        minimumStationsRequired
      }
    }
    updateAssetUpdaterConfig(configData: $assetUpdaterConfigData) {
      assetUpdater {
        doUploadBridgeScript
        doUploadBridgeFirmware
        doUploadAssetScripts
        doUploadAssetRadioFirmware
        doUploadAssetStm32Firmware
        doUploadAssetConfigs
      }
    }
    updateCloudConfig(configData: $cloudConfigData) {
      cloud {
        windReportingClosePercentage
        windReportingCloseInterval
        windReportingOverInterval
        increasedWeatherReportingTimeout
        snowReportingClosePercentage
        snowReportingCloseInterval
        snowReportingOverInterval
      }
    }
  }
`

const configSystemQuery = gql`
   query ConfigSystemQuery {
     config {
       siteName
       siteContact
       siteOrganization
       gpsLat
       gpsLng
       gpsAlt
       enableNightlyShutdown
       powerOff
       powerOn
       enableLowPowerShutdown
       cellModemWarningVoltage
       cellModemCutoffVoltage
       cellModemCutonVoltage
       gatewayWarningVoltage
       gatewayCutoffVoltage
     }
     weatherMonitor {
       enableWindSpeedStow
       windSpeedThreshold
       windSpeedDurationRequired
       enableWindGustStow
       windGustThreshold
       enableSnowDepthStow
       enableSnowDepthAveraging
       snowDepthThreshold
       snowDepthLowThreshold
       enableAutoResumeTrackingSnow
       enablePanelSnowDepthStow
       enablePanelSnowDepthAveraging
       panelSnowDepthThreshold
       panelSnowDepthLowThreshold
       enableAutoResumeTrackingPanelSnow
       resumeTrackingAfterWindTimeout
       resumeTrackingAfterSnowTimeout
       resumeTrackingAfterPanelSnowTimeout
       minimumStationsRequired
     }
     assetUpdater {
       doUploadBridgeScript
       doUploadBridgeFirmware
       doUploadAssetScripts
       doUploadAssetRadioFirmware
       doUploadAssetStm32Firmware
       doUploadAssetConfigs
     }
     cloud {
       windReportingClosePercentage
       windReportingCloseInterval
       windReportingOverInterval
       increasedWeatherReportingTimeout
       snowReportingClosePercentage
       snowReportingCloseInterval
       snowReportingOverInterval
     }
   }
`

// Bundle query, mutation, and target component together as a Higher Order Component (HOC)
export const ConfigSystemPage = compose(
  graphql(configSystemQuery, {name: 'siteConfig'}),
  graphql(UPDATE_SYSTEM_PAGE_CONFIG, {
    props: ({ownProps, mutate}) => ({
      submit: (siteConfigData, updatedWeatherMonitorConfig, updatedAssetUpdaterConfig, updatedCloudConfig) => {
        // UI displays some values in minutes that are really in seconds
        // Convert the data as it "exits the view"
        if ('windSpeedDurationRequired' in updatedWeatherMonitorConfig) {
          updatedWeatherMonitorConfig['windSpeedDurationRequired'] = '' + parseInt(updatedWeatherMonitorConfig['windSpeedDurationRequired'], 10) * 60
        }
        if ('resumeTrackingAfterWindTimeout' in updatedWeatherMonitorConfig) {
          updatedWeatherMonitorConfig['resumeTrackingAfterWindTimeout'] = '' + parseInt(updatedWeatherMonitorConfig['resumeTrackingAfterWindTimeout'], 10) * 60
        }
        if ('resumeTrackingAfterSnowTimeout' in updatedWeatherMonitorConfig) {
          updatedWeatherMonitorConfig['resumeTrackingAfterSnowTimeout'] = '' + parseInt(updatedWeatherMonitorConfig['resumeTrackingAfterSnowTimeout'], 10) * 60
        }
        if ('resumeTrackingAfterPanelSnowTimeout' in updatedWeatherMonitorConfig) {
          updatedWeatherMonitorConfig['resumeTrackingAfterPanelSnowTimeout'] = '' + parseInt(updatedWeatherMonitorConfig['resumeTrackingAfterPanelSnowTimeout'], 10) * 60
        }
        return mutate({
          variables: {
            configData: siteConfigData,
            weatherMonitorConfigData: updatedWeatherMonitorConfig,
            assetUpdaterConfigData: updatedAssetUpdaterConfig,
            cloudConfigData: updatedCloudConfig,
          },
        })
      },
    }),
    options: {
      refetchQueries: [
        {query: headerQuery},
        {query: configSystemQuery}
      ],
    }
  })
)(withRouter(ConfigSystem))

export default ConfigSystemPage
