// Assets Lists Weather Stations

import React from 'react'
import PropTypes from 'prop-types'
import TextField from 'material-ui/TextField'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {gql, graphql} from 'react-apollo'
import {AssetInfoDialog} from './AssetInfo'
import {refetchTimeout} from './LoadingCard'
import {formatTimestampAsDateAndTime} from './util'


class AssetWeatherPrime extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
  }

  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
    }
    this.errorTimer = null
  }

  handleSearchTextChange = (ev) => {
    this.setState({searchText: ev.target.value})
  }

  filterAssets = (event) => {
    // Simple, client-side filtering of whole dataset
    // First apply "alarms only" filter, then search phrase
    this.filteredAssets = this.props.data.assets.filter(event => {
      const evStr = Object.values(event).join(' ').toLowerCase()
      return evStr.includes(this.state.searchText.toLowerCase())
    })
  }

  render() {
    if (this.props.active) {
      this.props.data.refetch()  // Go ahead a get data now and then schedule poll
      this.props.data.startPolling(5000)
    } else {
      this.props.data.stopPolling()
    }

    const columns = [
      {
        Header: 'Location',
        accessor: 'locationText',
        width: 200,
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "left"
            }
          }
        }
      }, {
        Header: 'Address',
        accessor: 'id',
        width: 80,
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "left"
            }
          }
        }
      }, {
        Header: 'Last Reported',
        id: 'lastReported',
        width: 200,
        //resizable: false,
        accessor: (d) => {
          return formatTimestampAsDateAndTime(d.lastReported)
        },
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "left"
            }
          }
        }
      }, {
        Header: 'Wind Direction',
        accessor: 'windDirection',
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "left"
            }
          }
        }
      }, {
        Header: 'Wind Speed',
        accessor: 'windSpeed',
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        },
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        }
      }, {
        Header: 'Avg Wind Speed',
        accessor: 'averageWindSpeed',
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        },
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        }
      }, {
        Header: 'Peak Wind Speed',
        accessor: 'peakWindSpeed',
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        },
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        }
      }, {
        Header: 'Snow Sensor Temperature',
        accessor: 'snowSensorTemperature',
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        },
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        }
      }, {
        Header: 'Snow Depth',
        accessor: 'snowDepth',
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        },
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        }
      }
    ]

    if (this.props.data.error) {
      if (this.errorTimer === null) {
        this.errorTimer = setTimeout(() => {
          this.errorTimer = null
          this.props.data.refetch()
        }, refetchTimeout)
      }
    } else if (this.props.data.assets) {
      this.filterAssets()
    }

    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        {/* Toolbar: Acknowledge | Alarms Only | Filter */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end'
          }}
        >
          {/* Search box */}
          <div
            style={{
              marginTop: 2,
              paddingRight: '1em',
            }}
          >
            <TextField
              hintText="Filter..."
              onChange={this.handleSearchTextChange}
            />
          </div>
        </div>
        {/* Table of Events */}
        <div
          style={{
            backgroundColor: 'white',
            padding: '1em'
          }}
        >
          <ReactTable
            className="-striped -highlight"
            loading={this.props.data.loading || this.props.data.error}
            data={this.filteredAssets}
            columns={columns}
            getTdProps={(state, rowInfo, column, instance) => {
              return {
                onClick: e => {
                  if (rowInfo && rowInfo.row.id) {
                    this.nodeInfoDlg.getWrappedInstance().handleOpen(rowInfo.row.id)
                  }
                }
              }
            }}
          />
        </div>
        <AssetInfoDialog ref={(refToDlg) => (this.nodeInfoDlg = refToDlg)}/>
      </div>
    )
  }
}

const
  weatherQuery = gql`
  query AssetWeatherQuery {
    assets(hasWeatherSensor: true) {
      locationText
      id
      lastReported
      windSpeed
      windDirection
      averageWindSpeed
      peakWindSpeed
      snowSensorTemperature
      snowDepth
    }
  }
`
// Bundle query and target component as Higher Order Component (HOC)
const
  AssetListWeather = graphql(weatherQuery, {
    options: {pollInterval: 5000},
  })(AssetWeatherPrime);

export
default
AssetListWeather
