import React, {Component} from 'react'
import TextField from 'material-ui/TextField'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Paper from 'material-ui/Paper'
import RaisedButton from 'material-ui/RaisedButton'
import {login} from './AuthService'
import {muiLightTheme} from './App'


const loginBackground = 'images/login-background.jpg'
const logo = 'images/ts_logo.png'


class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      usernameErrorText: '',
      passwordErrorText: '',
      loginButtonDisabled: false
    };
  }

  handleSubmit = (event) => {
    // console.log("login submitted")
    event.preventDefault()  // prevent the default for submit behaviour
    this.setState({
      loginButtonDisabled: true
    })

    login(this.state.username, this.state.password).then(() => {
      // console.log("login promise then")
      this.props.history.push("/dashboard")
    }).catch(() => {
      // console.log("login promise CATCH")
      this.setState({
        usernameErrorText: "Please check username",
        passwordErrorText: "Please check password",
        loginButtonDisabled: false
      })
    })
  }

  handleUsernameChange = (event) => {
    this.setState({username: event.target.value});
  }

  handlePasswordChange = (event) => {
    this.setState({password: event.target.value});
  }

  render() {
    const style = {
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      margin: 'auto',
      width: '400px',
      height: '452px',
    }

    return (
      <div id="background"
           style={{
             position: "fixed",
             width: "100%",
             height: "100%",
             left: 0,
             top: 0,
             backgroundImage: "url(" + loginBackground + ")",
             backgroundSize: "cover",
             zIndex: 10
           }}>
        <MuiThemeProvider muiTheme={muiLightTheme}>
          <div style={style}>
            <Paper style={{padding: 20}}
                   zDepth={2}>
              <img src={logo}
                   alt={"TerraSmart Logo"}
              />
              <form onSubmit={this.handleSubmit}>
                <div>
                  <TextField name="username"
                             style={{width: '100%'}}
                             floatingLabelText="Enter your username"
                             onChange={this.handleUsernameChange}
                             errorText={this.state.usernameErrorText}
                  />
                </div>
                <div>
                  <TextField name="password"
                             style={{width: '100%'}}
                             floatingLabelText="Enter your password"
                             onChange={this.handlePasswordChange}
                             errorText={this.state.passwordErrorText}
                             type="password"
                  />
                </div>
                <div style={{textAlign: 'right'}}>
                  <RaisedButton label="Login"
                                style={{margin: 14}}
                                type="submit"
                                primary={true}
                                disabled={this.state.loginButtonDisabled}
                  />
                </div>
              </form>
            </Paper>
          </div>
        </MuiThemeProvider>
      </div>
    )
  }
}

export default LoginPage
