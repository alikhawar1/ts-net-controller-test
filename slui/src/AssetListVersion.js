// AssetListVersion

import React from 'react'
import PropTypes from 'prop-types'
import Checkbox from 'material-ui/Checkbox'
import TextField from 'material-ui/TextField'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {gql, graphql, compose} from 'react-apollo'
import * as util from './util'
import {AssetInfoDialog} from './AssetInfo'
import {refetchTimeout} from './LoadingCard'
import {formatTimestampAsDateAndTime} from './util'
import {formatSNAPversion} from './util'
import {formatVersion} from './util'
import {ConfirmationDialog} from './ConfirmationDialog'

const BRIDGE_SPY_FILE_UPLOAD = 0
const ASSET_SPY_FILE_UPLOAD = 1
const BRIDGE_RADIO_FIRMWARE_UPLOAD = 2
const ASSET_RADIO_FIRMWARE_UPLOAD = 3
const TC_WX_FIRMWARE_UPLOAD = 4
//const NCCB_FIRMWARE_UPLOAD = 5
const TC_CONFIG_UPLOAD = 6

class AssetListVersionPrime extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
    forceUploadMutation: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      // Controls
      checkAllChecked: false,
      forceScriptUploadButtonDisabled: true,
      forceSnapUploadButtonDisabled: true,
      forceStm32UploadButtonDisabled: true,
      forceConfigUploadButtonDisabled: true,
      // Filters
      searchText: '',
      // Pop-ip
      rowClickAssetId: null
    }
    this.checkedIds = []
    this.forceUploadMutation = this.props.forceUploadMutation
    this.errorTimer = null
  }

  handleSelectAll = (ev) => {
    const doSelect = ev.target.checked

    this.checkedIds.length = 0
    if (doSelect) {
      this.filteredAssets.forEach((item, index) => {
        this.checkedIds.push(item.id)
      })
    }

    this.setState({
      checkAllChecked: doSelect
    })

    this.updateButtonStates()
  }

  handleSearchTextChange = (ev) => {
    this.checkedIds.length = 0 // make any visibility changes clear any selections...
    this.setState({searchText: ev.target.value, checkAllChecked: false})
  }

  filterAssets = (event) => {
    // Simple, client-side filtering of whole dataset
    // Here we are only using the search phrase
    this.filteredAssets = this.props.data.assets.filter(event => {
      const evStr = Object.values(event).join(' ').toLowerCase()
      return evStr.includes(this.state.searchText.toLowerCase())
    })
  }

  updateButtonStates = () => {
    // If nothing checked, all "row-specific" buttons are disabled
    let new_forceScriptUploadButtonDisabled_value = true
    let new_forceSnapUploadButtonDisabled_value = true
    let new_forceStm32UploadButtonDisabled_value = true
    let new_forceConfigUploadButtonDisabled_value = true
    // If something checked, some buttons MAY get enabled
    if (this.checkedIds.length !== 0) {
      new_forceScriptUploadButtonDisabled_value = false
      new_forceSnapUploadButtonDisabled_value = false
      new_forceStm32UploadButtonDisabled_value = false
      new_forceConfigUploadButtonDisabled_value = false
    }

    this.setState({
      forceScriptUploadButtonDisabled: new_forceScriptUploadButtonDisabled_value,
      forceSnapUploadButtonDisabled: new_forceSnapUploadButtonDisabled_value,
      forceStm32UploadButtonDisabled: new_forceStm32UploadButtonDisabled_value,
      forceConfigUploadButtonDisabled: new_forceConfigUploadButtonDisabled_value,
    })
  }

  handleChecked = (event, asset) => {
    if (this.checkedIds.includes(asset.id)) {
      this.checkedIds = this.checkedIds.filter((id) => id !== asset.id)
    } else {
      this.checkedIds.push(asset.id)
    }

    this.updateButtonStates()
  }

  handleForceUpload = (uploadType) => {
    if (this.checkedIds.length > 0) {
      this.setState({
        forceScriptUploadButtonDisabled: true,
        forceSnapUploadButtonDisabled: true,
        forceStm32UploadButtonDisabled: true,
        forceConfigUploadButtonDisabled: true,
        progressDisplay: 'inline'
      })
      this.forceUploadMutation(this.checkedIds, uploadType).then((data) => {
        this.setState({
          snackbarOpen: true,
          progressDisplay: 'none'
        })
        this.props.data.refetch()
        this.checkedIds.length = 0
      }).catch((err) => {
        console.log(err)
        this.setState({
          errorDialogOpen: true,
          progressDisplay: 'none'
        })
        this.checkedIds.length = 0
      })
    }
  }

  handleForceBridgeUpload = (uploadType) => {
    this.setState({
      progressDisplay: 'inline'
    })
    this.forceUploadMutation(["bridge"], uploadType).then((data) => {
      this.setState({
        snackbarOpen: true,
        progressDisplay: 'none'
      })
      this.props.data.refetch()
    }).catch((err) => {
      console.log(err)
      this.setState({
        errorDialogOpen: true,
        progressDisplay: 'none'
      })
    })
  }

  render() {
    if (this.props.active) {
      this.props.data.refetch()  // Go ahead a get data now and then schedule poll
      this.props.data.startPolling(5000)
    } else {
      this.props.data.stopPolling()
    }

    const bridge = this.props.data.bridge

    let bridgeScriptVersion = '?'
    let bridgeVersion = '?'
    if (bridge !== undefined) {
      if (bridge.bridgeScriptVersion) {
        bridgeScriptVersion = bridge.bridgeScriptVersion
      }
      if (bridge.bridgeVersion) {
        bridgeVersion = bridge.bridgeVersion
      }
    }

    const columns = [
    {
      Header: (
        <Checkbox
          onClick={this.handleSelectAll}
          checked={this.state.checkAllChecked}
        />
      ),
      id: 'selector',
      width: 35,
      sortable: false,
      resizable: false,
      Cell: (row) => (
        <Checkbox
          onCheck={(event) => this.handleChecked(event, row.original)}
          checked={this.checkedIds.includes(row.original.id)}  // Needed because how react reuses DOM elements?
        />
      )
    },
    {
      Header: 'Location',
      accessor: 'locationText',
      width: 200,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "left"
          }
        }
      }
    },
    {
      Header: 'Address',
      accessor: 'id',
      width: 80,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "left"
          }
        }
      }
    },
    {
      Header: 'Online',
      id: 'online',
      width: 80,
      accessor: (d) => {
        if (d.offline) {
          return 'No'
        } else if (d.trackingStatus === 'HANDHELD_ROW_CONTROL') {
          return '(HHC)'
        } else if (d.offlineMinutes === util.MANUAL_CONTROL_INDICATOR) {
          return '(IRC)'
        } else if (d.offlineMinutes > 0) {
          return '(FCS)'
        } else {
          return 'Yes'
        }
      },
    },
    {
      Header: 'Last Reported',
      id: 'lastReported',
      width: 200,
      accessor: (d) => {
        return d.lastReported ? formatTimestampAsDateAndTime(d.lastReported) : 'Never'
      },
    },
    {
      Header: 'Type',
      accessor: 'device',
      width: 100,
    },
    {
      Header: 'Uptime',
      id: 'uptime',
      width: 100,
      accessor: (d) => {
        return util.uptimeStr(d.uptime)
      }
    },
    {
      Header: 'Script',
      id: 'radioScriptVersion',
      width: 100,
      accessor: (d) => {
        return formatVersion(d.radioScriptVersion)
      }
    },
    {
      Header: 'SNAP',
      id: 'radioFirmware',
      width: 100,
      accessor: (d) => {
        return formatSNAPversion(d.radioFirmware)
      }
    },
    {
      Header: 'STM32',
      id: 'firmwareRev',
      width: 100,
      accessor: (d) => {
        return formatVersion(d.firmwareRev)
      }
    },
    {
      Header: 'Config',
      width: 100,
      accessor: 'configLabel'
    },
    ]

    if (this.props.data.error) {
      if (this.errorTimer === null) {
        this.errorTimer = setTimeout(() => {
          this.errorTimer = null
          this.props.data.refetch()
        }, refetchTimeout)
      }
    } else if (this.props.data.assets) {
      this.filterAssets()
    }

    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        {/* Toolbar: Bridge Script Ver | [Upload] | Bridge SNAP Ver | [Upload] | [Script Upload] [SNAP Upload] [STM32 Upload] [Config Upload] | Separator | Filter */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start'
          }}
        >

          <TextField name="bridgeScriptVersion"
                     style={{
                       marginLeft: '1em',
                       width: '6em'
                     }}
                     floatingLabelText="Bridge Script"
                     floatingLabelStyle={{
                       color: 'black',
                     }}
                     defaultValue={bridgeScriptVersion === "?" ? bridgeScriptVersion : formatVersion(bridgeScriptVersion)}
                     disabled={true}
          />

          {/* Upload (Bridge Script) button */}
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              paddingLeft: '1em',
              paddingRight: '1em',
              paddingTop: '1em',
            }}
          >
            <ConfirmationDialog title="Force upload of bridge script?"
                                text="This operation is service affecting for several minutes. Are you sure now is an appropriate time?"
                                mainLabel="UPLOAD"
                                cancelLabel="Cancel Request"
                                confirmLabel="Upload Bridge Script Now"
                                onConfirm={() => this.handleForceBridgeUpload(BRIDGE_SPY_FILE_UPLOAD)}
                                disabled={false} />
          </div>

          <TextField name="bridgeVersion"
                     style={{
                       marginLeft: '1em',
                       width: '6em'
                     }}
                     floatingLabelText="Bridge SNAP"
                     floatingLabelStyle={{
                       color: 'black',
                     }}
                     defaultValue={bridgeVersion}
                     disabled={true}
          />

          {/* Upload (Bridge SNAP) button */}
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              paddingLeft: '1em',
              paddingRight: '1em',
              paddingTop: '1em',
            }}
          >
            <ConfirmationDialog title="Force upload of bridge firmware?"
                                text="This operation is service affecting for several minutes. Are you sure now is an appropriate time?"
                                mainLabel="UPLOAD"
                                cancelLabel="Cancel Request"
                                confirmLabel="Upload Bridge SNAP Firmware Now"
                                onConfirm={() => this.handleForceBridgeUpload(BRIDGE_RADIO_FIRMWARE_UPLOAD)}
                                disabled={false} />
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              flexGrow: 1,
            }}
          >
          </div>

          <div
            style={{
              paddingLeft: '1em', // because there is another button to the left of this one...
              justifyContent: 'center',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <ConfirmationDialog title="Force upload of radio script?"
                                text="This operation is service affecting for several minutes. Are you sure now is an appropriate time?"
                                mainLabel="SCRIPT"
                                cancelLabel="Cancel Request"
                                confirmLabel="Upload Radio Script Now"
                                onConfirm={() => this.handleForceUpload(ASSET_SPY_FILE_UPLOAD)}
                                disabled={this.state.forceScriptUploadButtonDisabled} />
          </div>

          <div
            style={{
              paddingLeft: '1em', // because there is another button to the left of this one...
              justifyContent: 'center',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <ConfirmationDialog title="Force upload of radio firmware?"
                                text="This operation is service affecting for several minutes. Are you sure now is an appropriate time?"
                                mainLabel="SNAP"
                                cancelLabel="Cancel Request"
                                confirmLabel="Upload SNAP Firmware Now"
                                onConfirm={() => this.handleForceUpload(ASSET_RADIO_FIRMWARE_UPLOAD)}
                                disabled={this.state.forceSnapUploadButtonDisabled} />
          </div>

          <div
            style={{
              paddingLeft: '1em', // because there is another button to the left of this one...
              justifyContent: 'center',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <ConfirmationDialog title="Force upload of STM32 firmware?"
                                text="This operation is service affecting for several minutes. Are you sure now is an appropriate time?"
                                mainLabel="STM32"
                                cancelLabel="Cancel Request"
                                confirmLabel="Upload STM32 Firmware Now"
                                onConfirm={() => this.handleForceUpload(TC_WX_FIRMWARE_UPLOAD)}
                                disabled={this.state.forceStm32UploadButtonDisabled} />
          </div>

          <div
            style={{
              paddingLeft: '1em', // because there is another button to the left of this one...
              justifyContent: 'center',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <ConfirmationDialog title="Force upload of configuration?"
                                text="This operation is service affecting for several minutes. Are you sure now is an appropriate time?"
                                mainLabel="CONFIG"
                                cancelLabel="Cancel Request"
                                confirmLabel="Upload Configuration Now"
                                onConfirm={() => this.handleForceUpload(TC_CONFIG_UPLOAD)}
                                disabled={this.state.forceConfigUploadButtonDisabled} />
          </div>

          {/* Separator between Buttons and Filter box */}
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              flexGrow: 1,
            }}
          >
          </div>

          {/* Filter box */}
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              marginTop: 2,
              paddingRight: '1em',
            }}
          >
            <TextField
              hintText="Filter..."
              onChange={this.handleSearchTextChange}
            />
          </div>
        </div>
        {/* Table of Assets */}
        <div
          style={{
            backgroundColor: 'white',
            padding: '1em'
          }}
        >
          <ReactTable
            className="-striped -highlight"
            loading={this.props.data.loading || this.props.data.error}
            data={this.filteredAssets}
            columns={columns}
            getTdProps={(state, rowInfo, column, instance) => {
              return {
                onClick: e => {
                  // We have added a "selector" checkbox in the first column
                  // We want to exclude that column from the "click on the row" behavior
                  if (column.id !== 'selector') {
                    if (rowInfo && rowInfo.row.id) {
                        this.nodeInfoDlg.getWrappedInstance().handleOpen(rowInfo.row.id)
                    }
                  }
                }
              }
            }}
            getTheadThProps={(state, rowInfo, column, instance) => {
              return {
                style: {
                  textAlign: "left"
                }
              }
            }}
          />
        </div>
        <AssetInfoDialog ref={(refToDlg) => (this.nodeInfoDlg = refToDlg)}/>
      </div>
    )
  }
}

const statusQuery = gql`
  query AssetStatusQuery {
    assets {
      locationText
      id
      lastReported
      device
      uptime
      offline
      offlineMinutes
      radioScriptVersion
      radioFirmware
      firmwareRev
      configLabel
      trackingStatus
    }
    bridge {
      bridgeVersion
      bridgeScriptVersion
    }
  }
`

const forceUploadMutation = gql`
mutation forceUpload($assetIDs: [ID], $uploadTypeInt: Int)  {
  forceUpload(ids: $assetIDs, uploadType: $uploadTypeInt) {
    requested
  }
}
`

// Bundle query and target component as Higher Order Component (HOC)
const AssetListVersion = compose(
  graphql(statusQuery, {
    options: {pollInterval: 5000},
  }),
  graphql(forceUploadMutation, {
    props: ({ownProps, mutate}) => ({
      forceUploadMutation: (ids, uploadType) => mutate({
        variables: {assetIDs: ids, uploadTypeInt: uploadType},
      }),
    }),
  })
)(AssetListVersionPrime);

export default AssetListVersion
