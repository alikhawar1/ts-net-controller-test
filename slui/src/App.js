// Entry point for React application using "create-react-app" build pipeline (https://github.com/facebookincubator/create-react-app)


import React, {Component} from 'react'
import {ApolloClient, ApolloProvider} from 'react-apollo'
import { createNetworkInterface } from 'apollo-upload-client'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import injectTapEventPlugin from 'react-tap-event-plugin'
import {NcStatusWithData} from './NcStatus'
import AppHeader from './AppHeader'
import {PAGE_INDEXES} from './PageEnums'
import Cookies from 'universal-cookie'
import {NcEventsWithData} from './NcEvents'
import {ConfigSystemPage} from './ConfigSystem'
import {ConfigControlPage} from './NcControl'
import ConfigNetworkPage from './ConfigNetwork'
import ConfigAssets from './ConfigAssets'
import ConfigFilesPage from './ConfigFiles'
import ConfigUpdatesPage from './ConfigUpdates'
import ConfigUserPage from './ConfigUser'
import NcAssetsList from './NcAssetsList'
import NcAssetMap from './NcAssetMap'
import NcDashboard from './NcDashboard'
import {cyan500} from 'material-ui/styles/colors';
import {LeftNav} from './LeftNav'


const cookies = new Cookies();

export const muiLightTheme = getMuiTheme(lightBaseTheme, {
  palette: {
    primary1Color: 'rgb(132, 189, 0)',
    accent1Color: cyan500,
  }
})

const apolloNetworkInterface = createNetworkInterface({
  uri: '/graphql',
  opts: {
    credentials: 'same-origin'
  }
})

apolloNetworkInterface.use([{
  applyMiddleware(req, next) {
    if (!req.options.headers) {
      req.options.headers = {};  // Create the header object if needed.
    }
    req.options.headers['X-Xsrftoken'] = cookies.get('_xsrf');

    next();
  }
}]);
apolloNetworkInterface.useAfter([{
  applyAfterware({response}, next) {
    if (response.status === 401 || response.status === 403) {
      // TODO: Redirect to login page and show some message
    }
    next();
  }
}]);

// Create Apollo GraphQL client
const client = new ApolloClient({
  networkInterface: apolloNetworkInterface
})

// Note: In DEV mode, the Webpack config is setup to proxy to 'http://localhost:8888/graphql'

// Needed for onTouchTap, required by Material-UI library (http://www.material-ui.com)
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin()


class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        {/* Overall page */}
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          height: '100vh'
        }}>
          <AppHeader />
          <div style={{
            display: 'flex',
            flexDirection: 'row',
            height: '100vh',
            overflow: 'hidden',
          }}>
            <LeftNav page={this.props.page}/>
            <MuiThemeProvider muiTheme={muiLightTheme}>
              <div
                style={{
                  overflow: 'auto',
                  width: '100vw',
                  backgroundColor: 'rgb(242, 244, 248)'
                }}
              >
                {this.renderSelectedPage()}
              </div>
            </MuiThemeProvider>
          </div>
        </div>
      </ApolloProvider>
    );
  }

  renderSelectedPage() {
    switch (this.props.page) {
      case PAGE_INDEXES.DASHBOARD:
        return <NcDashboard />
      case PAGE_INDEXES.STATUS:
        return <NcStatusWithData />
      case PAGE_INDEXES.EVENTS:
        return <NcEventsWithData />
      case PAGE_INDEXES.ASSETS_LIST:
        return <NcAssetsList />
      case PAGE_INDEXES.ASSETS_MAP:
        return <NcAssetMap />
      case PAGE_INDEXES.CONFIG_SYSTEM:
        return <ConfigSystemPage />
      case PAGE_INDEXES.CONFIG_CONTROL:
        return <ConfigControlPage />
      case PAGE_INDEXES.CONFIG_NETWORK:
        return <ConfigNetworkPage />
      case PAGE_INDEXES.CONFIG_ASSETS:
        return <ConfigAssets />
      case PAGE_INDEXES.CONFIG_FILES:
        return <ConfigFilesPage />
      case PAGE_INDEXES.CONFIG_USER:
        return <ConfigUserPage />
      case PAGE_INDEXES.CONFIG_UPDATES:
        return <ConfigUpdatesPage />
      default:
        return <p>{`TODO: page index ${this.props.page}`}</p>
    }
  }
}

export default App
