// Node Info dialog for SLUI

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import StatusTable from './StatusTable'
import {gql, withApollo} from 'react-apollo'
import ApolloClient from 'apollo-client'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {snappyHex, formatMacAddr, uptimeStr} from './util'
import {dateLocaleName, dateLocaLOptions} from './util'


let muiTheme = getMuiTheme()

const col1Width = '15em'


export class AssetInfoDialogBase extends Component {
  static propTypes = {
    client: PropTypes.instanceOf(ApolloClient).isRequired,
  }

  constructor(props) {
    super(props)
    this.screenRefresher = null
    this.state = {
      open: false,
      assetID: null,
      statusBitsEnums: null,
      trackingStatusEnums: null,
      statusList: [],
      systemList: [],
      meshList: [],
      sensorList: [],
      configList: []
    }
  }

  computeStateFromData = (asset) => {
    let status = ''
    if (asset.statuses.length === 0) {
      status = 'Normal'
    } else {
      asset.statuses.forEach((statusEnum) => {
        let a_status = this.state.statusBitsEnums.find((e) => e.name === statusEnum)
        if (a_status) {
          a_status = a_status.description
        } else {
          a_status = statusEnum
        }
        if (status.length !== 0) {
          status += ' '
        }
        status += a_status
      })
    }

    let trackingStatus = this.state.trackingStatusEnums.find((e) => e.name === asset.trackingStatus)
    if (trackingStatus) {
      trackingStatus = trackingStatus.description
    } else {
      trackingStatus = asset.trackingStatus
    }

    const lastReported = new Date(asset.lastReported)
    let systemList = [
      {label: 'Asset ID', value: asset.id, col1Style: {width: col1Width}},
      {label: 'Last Reported', value: asset.lastReported ? lastReported.toLocaleString(dateLocaleName, dateLocaLOptions) : 'Never'},
      {label: 'Statuses', value: status},
      {label: 'Tracking Status', value: trackingStatus},
      {label: 'Current Angle', value: asset.currentAngle},
      {label: 'Requested Angle', value: asset.requestedAngle},
      {label: 'Model', value: asset.model},
      {label: 'Device', value: asset.device},
      {label: 'Hardware Rev', value: asset.hardwareRev},
      {label: 'Firmware Rev', value: asset.firmwareRev},
      {label: 'Uptime', value: uptimeStr(asset.uptime)},
      {label: 'Downtime', value: uptimeStr(asset.downtime)},
      {label: 'Offline Minutes', value: asset.offlineMinutes}
    ]

    const configTimestamp = new Date(asset.configTimestamp)
    let configList = [
      {label: 'Config Label', value: asset.configLabel, col1Style: {width: col1Width}},
      {label: 'Config Timestamp', value: asset.configTimestamp ? configTimestamp.toLocaleString(dateLocaleName, dateLocaLOptions) : 'Unknown'},
      {label: 'Site Name', value: asset.siteName},
      {label: 'Location Lat', value: asset.locationLat},
      {label: 'Location Lng', value: asset.locationLng},
      {label: 'Location Text', value: asset.locationText},
      {label: 'Panel Horizontal Cal Angle', value: asset.panelHorizontalCalAngle},
      {label: 'Panel Min Cal Angle', value: asset.panelMinCalAngle},
      {label: 'Panel Max Cal Angle', value: asset.panelMaxCalAngle},
      {label: 'Config Flags', value: snappyHex(asset.configFlags)},
    ]

    let statusList = [
      {label: 'Battery Voltage', value: asset.batteryVoltage, col1Style: {width: col1Width}},
      {label: 'External Input 1 Voltage', value: asset.solarVoltage},
      {label: 'External Input 2 Voltage', value: asset.externalInput2Voltage},
      {label: 'Charger Current', value: asset.chargerCurrent},
      {label: 'Unit Temperature', value: asset.unitTemperature},
      {label: 'Battery Temperature', value: asset.batteryTemperature},
      {label: 'Battery Current', value: asset.batteryCurrent},
      {label: 'External Input 1 Current', value: asset.solarCurrent},
      {label: 'External Input 2 Current', value: asset.externalInput2Current},
      {label: 'Charger Voltage', value: asset.chargerVoltage},
      {label: 'Motor Current', value: asset.motorCurrent},
      {label: 'Heater Temperature', value: asset.heaterTemperature},
      {label: 'Battery Charged', value: asset.batteryCharged},
      {label: 'Battery Health', value: asset.batteryHealth},
      {label: 'External Input 1 Power Current Hour', value: asset.solarPowerCurrentHour},
      {label: 'External Input 1 Power Previous Hour', value: asset.solarPowerPreviousHour},
      {label: 'External Input 1 Power Current Day', value: asset.solarPowerCurrentDay},
      {label: 'External Input 1 Power Previous Day', value: asset.solarPowerPreviousDay},
      {label: 'External Input 2 Power Current Hour', value: asset.externalInput2PowerCurrentHour},
      {label: 'External Input 2 Power Previous Hour', value: asset.externalInput2PowerPreviousHour},
      {label: 'External Input 2 Power Current Day', value: asset.externalInput2PowerCurrentDay},
      {label: 'External Input 2 Power Previous Day', value: asset.externalInput2PowerPreviousDay},
      {label: 'Battery Power Current Hour', value: asset.batteryPowerCurrentHour},
      {label: 'Battery Power Previous Hour', value: asset.batteryPowerPreviousHour},
      {label: 'Battery Power Current Day', value: asset.batteryPowerCurrentDay},
      {label: 'Battery Power Previous Day', value: asset.batteryPowerPreviousDay},
      {label: 'Charger Power Current Hour', value: asset.chargerPowerCurrentHour},
      {label: 'Charger Power Previous Hour', value: asset.chargerPowerPreviousHour},
      {label: 'Charger Power Current Day', value: asset.chargerPowerCurrentDay},
      {label: 'Charger Power Previous Day', value: asset.chargerPowerPreviousDay},
      {label: 'Motor Power Current Hour', value: asset.motorPowerCurrentHour},
      {label: 'Motor Power Previous Hour', value: asset.motorPowerPreviousHour},
      {label: 'Motor Power Current Day', value: asset.motorPowerCurrentDay},
      {label: 'Motor Power Previous Day', value: asset.motorPowerPreviousDay},
      {label: 'Average Angular Error Current Hour', value: asset.averageAngularErrorCurrentHour},
      {label: 'Average Angular Error Previous Hour', value: asset.averageAngularErrorPreviousHour},
      {label: 'Average Angular Error Current Day', value: asset.averageAngularErrorCurrentDay},
      {label: 'Average Angular Error Previous Day', value: asset.averageAngularErrorPreviousDay},
      {label: 'Hours Since Accumulators Reset', value: asset.hoursSinceAccumulatorsReset},
      {label: 'Misc. status bits', value: snappyHex(asset.miscStatusBits)},
      {label: 'More status bits', value: snappyHex(asset.moreStatusBits)},
    ]

    let meshList = [
      {label: 'Radio Mac Addr', value: formatMacAddr(asset.radioMacAddr)},
      {label: 'Radio Channel', value: asset.radioChannel},
      {label: 'Radio Network ID', value: snappyHex(asset.radioNetworkId)},
      {label: 'Radio Forwarding Mask', value: snappyHex(asset.radioForwardingMask)},
      {label: 'Radio Repeats Data', value: asset.isARepeater ? "Yes" : "No"},
      {label: 'Radio Firmware', value: asset.radioFirmware},
      {label: 'Radio Script Version', value: asset.radioScriptVersion},
      {label: 'Radio Script CRC', value: snappyHex(asset.radioScriptCrc)},
      {label: 'Radio Link Quality', value: asset.radioLinkQuality, col1Style: {width: col1Width}},
      {label: 'Radio Mesh Depth', value: asset.radioMeshDepth},
      {label: 'Radio Polls Sent', value: asset.radioPollsSent},
      {label: 'Radio Poll Responses', value: asset.radioPollResponses}
    ]

    let sensorList = [
      {label: 'Has Weather Sensor', value: asset.hasWeatherSensor ? "Yes" : "No", col1Style: {width: col1Width}}
    ]
    if (asset.hasWeatherSensor) {
      sensorList = sensorList.concat([
        {label: 'Wind Speed', value: asset.windSpeed},
        {label: 'Wind Direction', value: asset.windDirection},
        {label: 'Wind Direction Offset', value: asset.windDirOffset},
        {label: 'Average Wind Speed', value: asset.averageWindSpeed},
        {label: 'Peak Wind Speed', value: asset.peakWindSpeed},
        {label: 'Snow Sensor Temperature', value: asset.snowSensorTemperature},
        {label: 'Snow Sensor Height', value: asset.snowSensorHeight},
        {label: 'Snow Sensor Distance', value: asset.snowSensorDistance},
        {label: 'Snow Depth', value: asset.snowDepth}
      ])
    }

    asset.sensors.forEach((sensor) => {
      if (sensor.sensorAdcMode) {
        sensorList = sensorList.concat([
          {label: 'Sensor Number', value: sensor.sensorNum},
          {label: 'Sensor ADC Mode', value: sensor.sensorAdcMode},
          {label: 'Sensor Flags', value: sensor.sensorFlags},
          {label: 'Sensor ADC Value', value: sensor.sensorAdcValue},
          {label: 'Sensor Digital Value', value: sensor.sensorDigitalInputValue}
        ])
      }
    })

    asset.segments.forEach((segment) => {
      if (segment.segmentNum !== null) {
        configList = configList.concat([
          {label: 'Row End Number', value: segment.segmentNum},
          {label: 'Panel Array Width', value: segment.panelArrayWidth},
          {label: 'Spacing To East', value: segment.spacingToEast},
          {label: 'Spacing To West', value: segment.spacingToWest},
          {label: 'Delta Height East', value: segment.deltaHeightEast},
          {label: 'Delta Height West', value: segment.deltaHeightWest}
        ])
      }
    })

    asset.presetAngles.forEach((angle) => {
      if (angle.presetAngle !== null) {
        configList = configList.concat([
          {label: 'Preset Number', value: angle.presetNum},
          {label: 'Preset Nearest Enabled', value: angle.nearestEnabled ? "Yes" : "No"},
          {label: 'Preset Angle Degrees', value: angle.presetAngle}
        ])
      }
    })

    this.setState({
      statusList: statusList,
      systemList: systemList,
      meshList: meshList,
      sensorList: sensorList,
      configList: configList
    })

  }

  handleOpen = (assetId) => {
    this.setState({
      open: true,
      assetID: assetId
    })

    // console.log(assetId)
    // console.log(this.state.assetID)

    let my_variables =  {
      assetID: assetId
    }

    if (this.screenRefresher) {
      this.screenRefresher.stopPolling()  // having trouble getting Apollo to shut up!
      this.screenRefresher.setVariables(my_variables)
      this.screenRefresher.startPolling(5000)
    } else {
      this.screenRefresher = this.props.client.watchQuery(
        {
          query: assetInfoQuery,
          fetchPolicy: 'network-only',
          pollInterval: 5000,
          variables: my_variables
        }
      )
      this.screenRefresher.subscribe(
        {
          next: ({ data }) => {
            let asset = data.assets[0]
            this.computeStateFromData(asset)
          }
        }
      )
    }
  }

  handleClose = () => {
    // Try to make Apollo stop querying for data
    if (this.screenRefresher) {
      this.screenRefresher.stopPolling()
    }
    this.setState({
      open: false,
      assetID: null
    })
  }

  componentDidMount() {
    if (!this.state.statusBitsEnums || this.state.statusBitsEnums.length === 0) {
      this.props.client.query({
        query: assetEnumsQuery,
      }).then((results) => {
        this.setState({
          trackingStatusEnums: results.data.assetEnums.fields.find(
            (field) => field.name === "trackingStatus"
          ).type.enumValues,
          statusBitsEnums: results.data.assetEnums.fields.find(
            (field) => field.name === "statuses"
          ).type.ofType.enumValues
        })
      }).catch(() => {
        console.log("An error occurred while trying to retrieve the ENUMs")
      })
    }
  }

  render() {
    const actions = [
      <FlatButton
        label="Ok"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleClose}
      />,
    ]

    if (this.state.statusList.length === 0) {
      return null
    }

    return (
      <div>
        <Dialog
          title="Asset Info"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <div style={{
            fontSize: "1.2em",
            fontFamily: muiTheme.fontFamily,
            paddingBottom: '1em',
            paddingTop: '1em',
            color: 'black'
          }}>
            System
          </div>
          <StatusTable
            statusList={this.state.systemList}
            co1Style={{width: 100}}
          />

          <div style={{
            fontSize: "1.2em",
            fontFamily: muiTheme.fontFamily,
            paddingBottom: '1em',
            paddingTop: '1em',
            color: 'black'
          }}>
            Mesh
          </div>
          <StatusTable
            statusList={this.state.meshList}
          />

          <div style={{
            fontSize: "1.2em",
            fontFamily: muiTheme.fontFamily,
            paddingBottom: '1em',
            paddingTop: '1em',
            color: 'black'
          }}>
            Config
          </div>
          <StatusTable
            statusList={this.state.configList}
          />

          <div style={{
            fontSize: "1.2em",
            fontFamily: muiTheme.fontFamily,
            paddingBottom: '1em',
            paddingTop: '1em',
            color: 'black'
          }}>
            Status
          </div>

          <StatusTable
            statusList={this.state.statusList}
          />

          <div style={{
            fontSize: "1.2em",
            fontFamily: muiTheme.fontFamily,
            paddingBottom: '1em',
            paddingTop: '1em',
            color: 'black'
          }}>
            Sensors
          </div>
          <StatusTable
            statusList={this.state.sensorList}
          />
        </Dialog>
      </div>
    )
  }
}

const assetInfoQuery = gql`
query AssetInfo($assetID: ID) {
  assets(id: $assetID){
    id
    lastReported
    segments {
      segmentNum
      panelArrayWidth
      spacingToEast
      spacingToWest
      deltaHeightEast
      deltaHeightWest
    }
    sensors {
      sensorNum
      sensorAdcMode
      sensorFlags
      sensorAdcValue
      sensorDigitalInputValue
    }
    presetAngles {
      presetNum
      nearestEnabled
      presetAngle
    }
    configFlags
    panelHorizontalCalAngle
    panelHorizontalCalAngle
    panelMinCalAngle
    panelMaxCalAngle
    configLabel
    configTimestamp
    siteName
    locationLat
    locationLng
    locationText
    model
    device
    hardwareRev
    firmwareRev
    hasWeatherSensor
    hasTrackerHardware
    statuses
    uptime
    downtime
    offlineMinutes
    currentAngle
    requestedAngle
    batteryVoltage
    solarVoltage
    chargerCurrent
    unitTemperature
    batteryTemperature
    batteryCurrent
    solarCurrent
    chargerVoltage
    motorCurrent
    heaterTemperature
    batteryCharged
    batteryHealth
    trackingStatus
    solarPowerCurrentHour
    solarPowerPreviousHour
    solarPowerCurrentDay
    solarPowerPreviousDay
    batteryPowerCurrentHour
    batteryPowerPreviousHour
    batteryPowerCurrentDay
    batteryPowerPreviousDay
    chargerPowerCurrentHour
    chargerPowerPreviousHour
    chargerPowerCurrentDay
    chargerPowerPreviousDay
    motorPowerCurrentHour
    motorPowerPreviousHour
    motorPowerCurrentDay
    motorPowerPreviousDay
    radioLinkQuality
    radioMeshDepth
    radioChannel
    radioNetworkId
    radioForwardingMask
    isARepeater
    radioMacAddr
    radioScriptVersion
    radioScriptCrc
    radioFirmware
    radioPollsSent
    radioPollResponses
    windSpeed
    windDirection
    windDirOffset
    averageWindSpeed
    peakWindSpeed
    snowSensorTemperature
    snowSensorDistance
    snowSensorHeight
    snowDepth
    averageAngularErrorCurrentHour
    averageAngularErrorPreviousHour
    averageAngularErrorCurrentDay
    averageAngularErrorPreviousDay
    hoursSinceAccumulatorsReset
    externalInput2Voltage
    externalInput2Current
    externalInput2PowerCurrentHour
    externalInput2PowerPreviousHour
    externalInput2PowerCurrentDay
    externalInput2PowerPreviousDay
    miscStatusBits
    moreStatusBits
  }
}
`

const assetEnumsQuery = gql`
query AssetEnums {
  assetEnums: __type(name: "TCAsset") {
    fields {
      name
      type {
        ofType {
          name
          enumValues{
            name
            description
          }
        }
        enumValues {
          name
          description
        }
        fields{
          name
        }
      }
    }
	}
}
`
export const AssetInfoDialog = withApollo(AssetInfoDialogBase, {withRef: true})

export default AssetInfoDialog
