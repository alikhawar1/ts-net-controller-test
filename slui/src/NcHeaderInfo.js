// NcHeaderInfo - site name, notifications, etc. for AppHeader


import React from 'react'
import PropTypes from 'prop-types'
import {gql, graphql} from 'react-apollo'
import {refetchTimeout} from './LoadingCard'


class HeaderSiteName extends React.Component {
  static propTypes = {
    data: PropTypes.any.isRequired
  }

  constructor(props) {
    super(props)
    this.errorTimer = null
  }

  render() {
    let siteName = '...'

    if (this.props.data.error) {
      if (this.errorTimer === null) {
        this.errorTimer = setTimeout(() => {
          this.errorTimer = null
          this.props.data.refetch()
        }, refetchTimeout)
      }
    } else if (!this.props.data.loading) {
      siteName = this.props.data.config.siteName
    }

    return (
      <div
        style={{
          display: 'flex',
          margin: 16,
          fontSize: 20,
          fontStyle: 'normal',
          color: 'black',
          whiteSpace: 'nowrap'
        }}>
        <div
          style={{
            fontWeight: 'bold',
            marginRight: 10
          }}
        >
          Site:
        </div>
        <div style={{fontWeight: 'normal'}}>
          {siteName}
        </div>
      </div>
    )
  }
}

export const headerQuery = gql`
  query HeaderQuery {
    config {
      siteName
    }
  }
`

// Bundle query and target component as Higher Order Component (HOC)
export const NcHeaderSiteName = graphql(headerQuery)(HeaderSiteName)
