import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table'
import {gql, graphql} from 'react-apollo'
import {criticalLevelNo} from './NcEvents'
import {CardLoadingIndicator, CardErrorIndicator} from './LoadingCard'
import {formatTimestampAsDateAndTime} from './util'


const rowStyle = {
  height: 0,
  padding: 0
}

const timeColStyle = {
  width: '15em'
}
Object.assign(timeColStyle, rowStyle)

const timeHeaderStyle = {
  width: '16.2em'  // Not sure why this needs to be wider than timeColStyle
}
Object.assign(timeHeaderStyle, rowStyle)


class DashboardEventLogEntry extends PureComponent {
  static propTypes = {
    time: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired
  }

  render() {
    return (
      <TableRow style={rowStyle}>
        <TableRowColumn style={timeColStyle}>{formatTimestampAsDateAndTime(this.props.time)}</TableRowColumn>
        <TableRowColumn style={rowStyle}>{this.props.message}</TableRowColumn>
      </TableRow>
    )
  }
}


class DashboardEventLogTarget extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
    filterFunc: PropTypes.func,  // optional callback function that returns a boolean
  }

  matchAll = (event) => { return true }

  static defaultProps = {
    filterFunc: this.matchAll,
  }

  getTableRows = () => {
    return this.filteredEvents.map((entry) => {
      return (
        <DashboardEventLogEntry time={entry.created} message={entry.message} key={entry.id} />
      )
    })
  }

  filterEvents() {
    this.filteredEvents = this.props.data.eventLog.eventLogEntries.filter(event => {
      // Given that I am specifying defaultProps up above, I have no idea why the
      // following if statement is necessary, but it is...
      if (this.props.filterFunc === undefined) {
        return true
      } else {
        return this.props.filterFunc(event)
      }
    })
  }

  render() {
    if (this.props.data.error) {
      return (<CardErrorIndicator apolloData={this.props.data} />)
    } else if (this.props.data.loading) {
      return (<CardLoadingIndicator />)
    } else if (this.props.data.eventLog.eventLogEntries.length === 0) {
      return (<div>There are currently no unacknowledged event log entries</div>)
    }

    this.filterEvents()

    return (
      <Table>
        <TableHeader
          displaySelectAll={false}
          adjustForCheckbox={false}
        >
          <TableRow style={rowStyle}>
            <TableHeaderColumn style={timeHeaderStyle}>Time</TableHeaderColumn>
            <TableHeaderColumn style={rowStyle}>Event</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody
          displayRowCheckbox={false}
        >
          {this.getTableRows()}
        </TableBody>
      </Table>
    )
  }
}

const configNetworkQuery = gql`
query ConfigNetworkQuery {
  eventLog {
    eventLogEntries(cleared: false) {
      id
      message
      created
    }
  }
}
`
// Bundle query and target component as Higher Order Component (HOC)
export const ConfigNetworkEventLog = graphql(configNetworkQuery, {
  options: {
    pollInterval: 5000,
    variables: {
      cleared: false
    }
  },
})(DashboardEventLogTarget);

const dashboardQuery = gql`
query DashboardQuery($level: Int) {
  eventLog {
    eventLogEntries(levelno: $level, cleared: false) {
      id
      message
      created
    }
  }
}
`
// Bundle query and target component as Higher Order Component (HOC)
export const DashboardEventLog = graphql(dashboardQuery, {
  options: {
    pollInterval: 5000,
    variables: {
      level: criticalLevelNo
    }
  },
})(DashboardEventLogTarget);

export default DashboardEventLog

