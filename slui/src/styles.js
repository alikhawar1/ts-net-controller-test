export const sectionStyle = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  justifyContent: 'left',
  maxWidth: '50em',
  margin: '0 auto 2rem',
  paddingBottom: '2rem',
  borderBottom: '1px solid #d3dbe2',
  paddingLeft: '1rem',
}

export const descriptionStyle = {
  display: 'flex',
  flexDirection: 'column',
  paddingRight: 64,
  maxWidth: '15em',
  flex: '1 1 24rem',
  marginBottom: '1em',
}

export const rootPaperStyle = {
  paddingBottom: '2em',
  paddingTop: '1em',
  paddingLeft: '1em',
  paddingRight: '1em',
}

export const paperStyle = {
  display: 'flex',
  flexDirection: 'column',
  flex: '2 1 30rem',
  maxWidth: '30em',
}
// Copy in all rootPaperStyle properties
Object.assign(paperStyle, rootPaperStyle)

export const titleStyle = {
  marginBottom: 4,
  marginTop: 0,
}

export const indentStyle = {
  paddingLeft: '2.25em',
  width: '23.7em',
}
