import React from 'react'
import PropTypes from 'prop-types'
import {Table, TableBody, TableRow, TableRowColumn} from 'material-ui/Table'

// Compact table which displays props.statusList
class StatusTable extends React.Component {
  static propTypes ={
    statusList: PropTypes.array.isRequired,
    style: PropTypes.object,
  }

  getRowStyle = (index) => {
    return {
      height: '1.5em',
      background: index % 2 ? 'rgba(132, 189, 0, 0.02)' : 'inherit',
    }
  }

  render() {
    const col1Style = {height: 'auto', fontWeight: 'bold'}
    const col2Style = {height: 'auto'}
    const tableStyle = Object.assign({tableLayout: 'auto', width: '100%'}, this.props.style)  // Merge with passed-in style

    return (
      <Table 
        selectable={false}
        showCheckboxes={false}
        style={tableStyle}
      >
        <TableBody displayRowCheckbox={false} stripedRows={false}>

          {this.props.statusList.map( (row, index) => (
            <TableRow
                key={index}
                style={this.getRowStyle(index)}
            >
              <TableRowColumn style={Object.assign(col1Style, row.col1Style)}>{row.label}</TableRowColumn>
              <TableRowColumn style={Object.assign(col2Style, row.col2Style)}>{row.value}</TableRowColumn>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    )
  }
}

export default StatusTable
