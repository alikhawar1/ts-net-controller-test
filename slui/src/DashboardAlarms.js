import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {gql, graphql} from 'react-apollo'
import {DashboardEventLog} from './DashboardEventLog'
import {CardLoadingIndicator, CardErrorIndicator} from './LoadingCard'

// This file may seem a little strange - it used to contain more entities.
// We pulled the "alarm states" out of this "card" and they became individual
// "ribbons" on the parent Dashboard. Keeping this file for a bit to see if
// we move something else into here. If we don't, bypassing this file and
// having the Dashboard pull in DashboardEventLog directly is a possibility.

class DashboardAlarmsTarget extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
  }

  render() {
    if (this.props.data.error) {
      return (<CardErrorIndicator apolloData={this.props.data} />)
    } else if (this.props.data.loading) {
      return (<CardLoadingIndicator />)
    }

    return (
      <div>
        <DashboardEventLog />
      </div>
    )
  }
}


const DashboardMeshQuery = gql`
query DashboardMeshQuery {
  mesh {
    latestPoll
  }
}
`

// Bundle query and target component as Higher Order Component (HOC)
export const DashboardAlarms = graphql(DashboardMeshQuery, {
  options: {
    pollInterval: 5000,
  },
})(DashboardAlarmsTarget);

export default DashboardAlarms
