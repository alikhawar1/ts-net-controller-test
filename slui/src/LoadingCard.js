import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import CircularProgress from 'material-ui/CircularProgress'


export const loadingStyle = {
  display: 'flex',
  flexDirection: 'column',
  height: '100%'
}


export const refetchTimeout = 10000


export class CardErrorIndicator extends PureComponent {
  static propTypes = {
    apolloData: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)
    this.errorTimer = null
  }

  render() {
    if (this.errorTimer === null) {
      this.errorTimer = setTimeout(() => {
        this.errorTimer = null
        this.props.apolloData.refetch()
      }, refetchTimeout)
    }

    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%'
        }}
      >
        An error occurred while trying to retrieve the data, retrying...
      </div>
    )
  }
}

export class CardLoadingIndicator extends PureComponent {
  render() {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%'
        }}
      >
        <CircularProgress
          size={30}
          style={{
            display: 'block',
          }}
        />
      </div>
    )
  }
}

export default CardLoadingIndicator
