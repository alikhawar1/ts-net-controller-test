// Status page for SLUI

import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import Paper from 'material-ui/Paper'
import StatusTable from './StatusTable'
import {gql, graphql} from 'react-apollo'
import * as util from './util'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {rootPaperStyle} from './styles'
import {CardLoadingIndicator, CardErrorIndicator} from './LoadingCard'
import {formatTimestampAsDateAndTime} from './util'


let muiTheme = getMuiTheme()


class NcStatus extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
  }

  constructor(props, context) {
    super(props)
  }

  formatPowerSource(asset) {
    if (asset.solarCurrent > asset.batteryCurrent) {
      return "SOLAR"
    } else if (asset.batteryCurrent > 0) {
      return "BATTERY"
    } else {
      return "AUX"
    }
  }

  formatPercentage(percentage) {
    // This only comes into play when running on a PC, but we have a demo coming up
    let percent = (percentage === null) ? 0 : percentage
    return '' + percent + '%'
  }

  formatChargerStatus(asset) {
    if (asset.chargerCurrent > 0) {
      return 'CHARGING'
    } else {
      return 'IDLE'
    }
  }

  render() {
    if (this.props.data.error) {
      return (<CardErrorIndicator apolloData={this.props.data} />)
    } else if (this.props.data.loading) {
      return (<CardLoadingIndicator />)
    }

    const statTitleStyle = {
      fontSize: '1.15em',
      paddingBottom: '1em',
    }
    const tableStyle = {width: '95%', marginLeft: 10}
    const siteConfig = this.props.data.config
    const ethernet = this.props.data.ethernet
    const gps = this.props.data.gps
    const cellModem = this.props.data.cellModem
    const mesh = this.props.data.mesh
    const bridge = this.props.data.bridge
    const asset = this.props.data.assets[0]
    const cloud = this.props.data.cloud
    const ntpinfo = this.props.data.ntpinfo

    const paperStyle = {
      width: '25rem',
      height: '21rem',
      margin: '0.7em'
    }
    // Copy in all rootPaperStyle properties
    Object.assign(paperStyle, rootPaperStyle)

    const widePaperStyle = {
      // double the width of the "standard"" cards
      // width: '51.5rem',
      // wide enough for the longest expected content
      // (currently this is the Gateway OS Build)
      width: '35rem',
      height: '21rem',
      margin: '0.7em'
    }
    // Copy in all rootPaperStyle properties
    Object.assign(widePaperStyle, rootPaperStyle)

    let roaming = ""
    if (cellModem.linkStatus) {
      roaming = cellModem.roaming ? "True" : "False"
    }

    let meshLatestPoll = ''
    if (mesh.latestPoll) {
      meshLatestPoll = formatTimestampAsDateAndTime(mesh.latestPoll)
    }

    let meshLatestResponse = ''
    if (mesh.latestResponse) {
      meshLatestResponse = formatTimestampAsDateAndTime(mesh.latestResponse)
    }

    let gpsLastFixTime = ''
    if (gps.fixTime) {
      gpsLastFixTime = formatTimestampAsDateAndTime(gps.fixTime)
    }

    let cloudLastPublish = ''
    if (cloud.lastPublish) {
      cloudLastPublish = formatTimestampAsDateAndTime(cloud.lastPublish)
    }

    let cloudLastPublishResponse = ''
    if (cloud.lastPublishResponse) {
      cloudLastPublishResponse = formatTimestampAsDateAndTime(cloud.lastPublishResponse)
    }

    return (
      <div
        style={{
          margin: '2em'
        }}
      >
        <div style={{
          fontSize: "1.2em",
          fontFamily: muiTheme.fontFamily,
          paddingBottom: '1em',
        }}>
          Status
        </div>
        <div style={{display: 'flex', flexDirection: 'column'}}>

          {/* Cards */}
          <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>

            {/* System */}
            <Paper
              zDepth={1}
              rounded={false}
              style={widePaperStyle}
            >
              <div style={statTitleStyle}>
                System
              </div>
              <StatusTable
                style={tableStyle}
                statusList={[
                  {label: 'Site Name', value: siteConfig.siteName},
                  {label: 'Organization', value: siteConfig.siteOrganization},
                  {label: 'Contact', value: siteConfig.siteContact},
                  {label: 'Time Zone', value: 'UTC'}, // <- only time zone allowed is UTC until post-MVP
                  {label: 'Uptime', value: util.uptimeStr(siteConfig.uptime)},
                  {label: 'Gateway Platform', value: siteConfig.gatewayPlatform},
                  {label: 'Gateway OS Version', value: siteConfig.gatewayOsVersion},
                  {label: 'Gateway OS Build', value: siteConfig.gatewayOsBuild},
                  {label: 'Software Version', value: siteConfig.softwareVersion},
                ]}
              />
            </Paper>

            {/* Power */}
            <Paper
              zDepth={1}
              rounded={false}
              style={paperStyle}
            >
              <div style={statTitleStyle}>
                Power
              </div>
              <StatusTable
                style={tableStyle}
                statusList={[
                  {label: 'Power Source', value: this.formatPowerSource(asset)},
                  {label: 'Battery Charge', value: this.formatPercentage(asset.batteryCharged)},
                  {label: 'Battery Health', value: this.formatPercentage(asset.batteryHealth)},
                  {label: 'Charger Status', value: this.formatChargerStatus(asset)},
                ]}
              />
            </Paper>

            {/* Mesh (Mesh Info) */}
            <Paper
              zDepth={1}
              rounded={false}
              style={paperStyle}>
              <div style={statTitleStyle}
              >
                Mesh Network
              </div>
              <StatusTable
                style={tableStyle}
                statusList={[
                  {label: 'Assets Reporting', value: util.sprintf("%d/%d", mesh.assetsReporting, mesh.totalAssets)},
                  {label: 'Latest Poll', value: meshLatestPoll},
                  {label: 'Latest Response', value: meshLatestResponse},
                  {label: 'Mesh Depth', value: mesh.latestPoll ? mesh.maxMeshDepth : ''},  // Only show if we've actually polled
                  {label: 'Bridge Address', value: bridge.bridgeAddress},
                  {label: 'Bridge Version', value: bridge.bridgeVersion},
                  {label: 'Bridge Script CRC', value: bridge.bridgeScriptCrc ? util.snappyHex(bridge.bridgeScriptCrc) : ''},
                  {label: 'Configured Channel', value: bridge.bridgeChannel},
                  {label: 'Configured Net ID', value: bridge.bridgeNetworkId ? util.snappyHex(bridge.bridgeNetworkId) : ''},
                  {label: 'Bridge Radio Script Version', value: bridge.bridgeScriptVersion}
                ]}
              />
            </Paper>

            {/* GPS */}
            <Paper
              zDepth={1}
              rounded={false}
              style={paperStyle}
            >
              <div style={statTitleStyle}>
                {gps.isResponding ? 'GPS' : 'GPS is currently not responding!'}
              </div>
              <StatusTable
                style={tableStyle}
                statusList={[
                  {label: 'Fix Acquired', value: gps.quality}, /* TODO: bool */
                  {label: 'Last Fix Time', value: gpsLastFixTime},
                  {label: 'Satellites', value: gps.numSats},
                  {label: 'Latitude', value: gps.latitude ? gps.latitude.toFixed(6) : ''},
                  {label: 'Longitude', value: gps.latitude ? gps.longitude.toFixed(6) : ''},
                  {label: 'Altitude', value: gps.altitude},
                  {label: 'Using Fallback', value: gps.usingFallback ? 'True' : 'False'},
                  {label: 'Fallback Latitude', value: siteConfig.gpsLat ? siteConfig.gpsLat.toFixed(6) : ''},
                  {label: 'Fallback Longitude', value: siteConfig.gpsLng ? siteConfig.gpsLng.toFixed(6) : ''},
                  {label: 'Fallback Altitude', value: siteConfig.gpsAlt},
                ]}
              />
            </Paper>

            {/* Cell Modem */}
            <Paper
              zDepth={1}
              rounded={false}
              style={paperStyle}
            >
              <div style={statTitleStyle}>
                {cellModem.isResponding ? 'Cell Modem' : 'Cell Modem is currently not responding!'}
              </div>
              <StatusTable
                style={tableStyle}
                statusList={[
                  {label: 'Link Status', value: cellModem.linkStatus ? cellModem.linkStatus : 'Unknown'},
                  {label: 'Signal Strength', value: cellModem.rssiDbm ? util.sprintf("%d dBm", cellModem.rssiDbm) : ''},
                  {label: 'WAN IP Address', value: cellModem.wanIp},
                  {label: 'LAN IP Address', value: cellModem.lanIp},
                  {
                    label: 'Uptime',
                    value: cellModem.uptime? util.uptimeStr(cellModem.uptime) : ''
                  },
                  {label: 'Data Usage (24hr)', value: cellModem.txDataUsage ? util.sprintf("%d bytes", cellModem.txDataUsage) : ''},
                  {label: 'IMEI', value: cellModem.imei},
                  {label: 'MDN', value: cellModem.mdn},
                  {label: 'Tower ID', value: cellModem.towerId},
                  {label: 'Roaming', value: roaming},
                ]}
              />
            </Paper>

            {/* TerraWmart Cloud */}
            <Paper
              zDepth={1}
              rounded={false}
              style={paperStyle}>
              <div style={statTitleStyle}
              >
                TerraSmart Cloud
              </div>
              <StatusTable
                style={tableStyle}
                statusList={[
                  {label: 'Enabled', value: cloud.enabled ? 'True' : 'False'},
                  {label: 'Connected', value: cloud.connected ? 'True' : 'False'},
                  {label: 'Latest Report', value: cloudLastPublish},
                  {label: 'Latest Response', value: cloudLastPublishResponse},
                ]}
              />
            </Paper>

            {/* NTP Info */}
            <Paper
              zDepth={1}
              rounded={false}
              style={paperStyle}>
              <div style={statTitleStyle}
              >
                NTP Time Synchronization
              </div>
              <StatusTable
                style={tableStyle}
                statusList={[
                  {label: 'Synchronized', value: ntpinfo.timeSynchronized ? 'True' : 'False'},
                  {label: 'NTP Server', value: ntpinfo.timeSynchronized ? ntpinfo.timeServer : ''},
                  {label: 'Offset In Seconds', value: ntpinfo.timeSynchronized ? ntpinfo.timeOffset : ''},
                ]}
              />
            </Paper>

            {/* System */}
            <Paper
              zDepth={1}
              rounded={false}
              style={widePaperStyle}
            >
              <div style={statTitleStyle}>
                Ethernet
              </div>
              <StatusTable
                style={tableStyle}
                statusList={[
                  {label: 'Ethernet DHCP IP', value: ethernet.dhcpIpAddress},
                  {label: 'Ethernet Static IP', value: ethernet.staticIpAddress},
                  {label: 'Ethernet Subnet Mask', value: ethernet.dhcpSubnetMask},
                ]}
              />
            </Paper>

          </div>
        </div>
      </div>
    )
  }
}

const statusQuery = gql`
   query StatusQuery {
     config {
       siteName
       siteContact
       siteOrganization
       gpsLat
       gpsLng
       gpsAlt
       softwareVersion
       gatewayPlatform
       gatewayOsVersion
       gatewayOsBuild
       uptime
     }
     ethernet {
       dhcpIpAddress
       dhcpSubnetMask
       staticIpAddress
     }
     gps {
       isResponding
       latitude
       longitude
       altitude
       numSats
       quality
       fixTime
       usingFallback
     }
     cellModem {
       isResponding
       rssiDbm
       imei
       roaming
       mdn
       lanIp
       wanIp
       linkStatus
       uptime
       txDataUsage
       rxDataUsage
       towerId
     }
     mesh {
       assetsReporting
       totalAssets
       latestPoll
       latestResponse
       maxMeshDepth
     }
     bridge {
       bridgeChannel
       bridgeNetworkId
       bridgeAddress
       bridgeVersion
       bridgeScriptCrc
       bridgeScriptVersion
     }
     assets(id: "serial") {
       solarCurrent
       batteryCurrent
       chargerCurrent
       batteryCharged
       batteryHealth
     }
     cloud {
       enabled
       connected
       lastPublish
       lastPublishResponse
     }
     ntpinfo {
       timeSynchronized
       timeServer
       timeOffset
     }
   }
`
// Bundle query and target component as Higher Order Component (HOC)
export const NcStatusWithData = graphql(statusQuery, {
  options: {pollInterval: 5000},
})(NcStatus);

export default NcStatus
