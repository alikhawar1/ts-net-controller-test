// Utility functions
import sprintf from './sprintf'

function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals)
}

function distanceStr(meters) {
  // Return localized distance string
  const ftDec = meters * 3.28084
  const ft = Math.trunc(ftDec)
  const inches = 12 * (ftDec - ft)
  return sprintf("%d ft %d in", ft, inches)
}

function uptimeStr(uptimeSecs) {
  const rem_secs = uptimeSecs % 60
  const mins = Math.trunc(uptimeSecs / 60)
  const rem_mins = mins % 60
  const hours = Math.trunc(mins / 60)
  const rem_hours = hours % 24
  const days = Math.trunc(hours / 24)
  const rem_days = days % 365
  const years = Math.trunc(days / 365)

  let yd = ''
  if (years) {
    yd = `${years}y ${rem_days}d `
  }
  else if (rem_days) {
    yd = `${rem_days}d `
  }

  return sprintf("%s%02d:%02d:%02d", yd, rem_hours, rem_mins, rem_secs)
}

// SNAPpy often returns values that are REALLY unsigned 16-bit
// values in a signed 16-bit form. This routine compensates for that.
function snappyHex(value) {
  value = value >>> 0  // This gets us back to unsigned values
  value &= 0xFFFF  // This gets us back to 16-bits
  return sprintf("0x%04X", value)
}

function formatMacAddr(macStr) {
  if (macStr) {
    return macStr.toUpperCase().match(/.{1,2}/g).join(':')
  } else {
    return "unknown"
  }
}

function formatTimestampAsDateAndTime(timestamp)
{
  const date = new Date(timestamp)
  let formattedString = date.toLocaleString(dateLocaleName, dateLocaLOptions)
  formattedString = formattedString.replace(",", "") // I don't like a comma between date and time...
  formattedString += ' UTC' // we said this would be explicit
  return formattedString
}

function linkQualitydBmToPercent(linkQualitydBm)
{
  // The following formula is based on information obtained from the Synapse Support Forum
  const linkQualityPercent = Math.min(99, Math.max(0, Math.floor(100 - 100.0 * (linkQualitydBm - 18) / (95 - 18))))
  return linkQualityPercent
}

function formatSNAPversion(version_offset) {
  if (version_offset === null) {
    return "?"
  }
  let version = 281 + parseInt(version_offset, 10)
  const major = Math.trunc(version / 100)
  version = version % 100
  const minor = Math.trunc(version / 10)
  const build = version % 10

  return sprintf("%d.%d.%d", major, minor, build)
}

function formatVersion(version) {
  if (version === null) {
    return "?"
  } else if (version === 0) {
    return "?"
  } else if (version === '0') {
    return "?"
  } else {
    const major = Math.trunc(version / 10)
    const minor = version % 10

    return sprintf("%d.%d", major, minor)
  }
}

export const dateLocaleName = 'en-US'
export const dateLocaLOptions = {
  hour12: false
}
export const MANUAL_CONTROL_INDICATOR = 0x7fff

export {sprintf, round, distanceStr, uptimeStr, snappyHex, formatMacAddr, formatTimestampAsDateAndTime,
        linkQualitydBmToPercent, formatSNAPversion, formatVersion}
