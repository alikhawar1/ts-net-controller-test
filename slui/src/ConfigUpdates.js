// Config => Updates page for SLUI


import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {gql, graphql, compose} from 'react-apollo'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import Checkbox from 'material-ui/Checkbox'
import Snackbar from 'material-ui/Snackbar'
import CircularProgress from 'material-ui/CircularProgress'
import validator from 'validator'
import Dialog from 'material-ui/Dialog'
import Paper from 'material-ui/Paper'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {headerQuery} from './NcHeaderInfo'
import {withRouter} from 'react-router-dom'
import {paperStyle, sectionStyle, titleStyle, descriptionStyle} from './styles'
import Dropzone from 'react-dropzone'
import {CardLoadingIndicator, CardErrorIndicator} from './LoadingCard'
import * as util from './util'


let muiTheme = getMuiTheme()
const defaultBridgeScriptFileName = '<Drag new file here or select file>'
const defaultBridgeFirmwareFileName = '<Drag new file here or select file>'
const defaultAssetScriptFileName = '<Drag new file here or select file>'
const defaultAssetRadioFirmwareFileName = '<Drag new file here or select file>'
const defaultAssetStm32FirmwareFileName = '<Drag new file here or select file>'


class ConfigUpdates extends Component {
  static propTypes = {
    configData: PropTypes.object.isRequired,  // injected by GraphQL
    match: PropTypes.object.isRequired,  // injected by react-router-dom withRouter
    location: PropTypes.object.isRequired,  // injected by react-router-dom withRouter
    history: PropTypes.object.isRequired  // injected by react-router-dom withRouter
  }

  constructor(props) {
    super(props)
    this.updateConfig = props.submit
    this.hasChanged = false
    this.updatedAssetUpdaterVals = {}
    this.state = {
      snackbarOpen: false,
      progressDisplay: 'none',
      updateButtonDisabled: false,
      errorDialogOpen: false,
      bridgeScriptFileName: defaultBridgeScriptFileName,
      bridgeScriptFile: null,
      bridgeFirmwareFileName: defaultBridgeFirmwareFileName,
      bridgeFirmwareFile: null,
      assetScriptFileName: defaultAssetScriptFileName,
      assetScriptFile: null,
      assetRadioFirmwareFileName: defaultAssetRadioFirmwareFileName,
      assetRadioFirmwareFile: null,
      assetStm32FirmwareFileName: defaultAssetStm32FirmwareFileName,
      assetStm32FirmwareFile: null,
      errorMessage: null
    }
    this.history = props.history
    this.unblock = null
  }

  handleSubmit = (event) => {
    event.preventDefault()  // prevent the default form submit behaviour
    if (this.hasChanged) {
      this.setState({
        updateButtonDisabled: true,
        progressDisplay: 'inline'
      })

      // UI has some standalone fields, the real data comes from Graphene Lists
      // Requires some manual fixups... first we see if the fixups will be needed
      const special_cases = ["assetScriptUploadBatchSize",
                             "assetRadioFirmwareUploadBatchSize",
                             "stm32FirmwareUploadBatchSize"]
      let uploadBatchSizesFixupNeeded = false
      for (const key of Object.keys(this.updatedAssetUpdaterVals)) {
          if (special_cases.includes(key)) {
              uploadBatchSizesFixupNeeded = true
          }
      }
      const special_cases2 = ["bridgeScriptUploadRetries",
                              "assetScriptUploadRetries",
                              "bridgeRadioFirmwareUploadRetries",
                              "assetRadioFirmwareUploadRetries",
                              "stm32FirmwareUploadRetries",
                              "configUploadRetries"]
      let uploadRetriesFixupNeeded = false
      for (const key of Object.keys(this.updatedAssetUpdaterVals)) {
          if (special_cases2.includes(key)) {
              uploadRetriesFixupNeeded = true
          }
      }
      const special_cases3 = ["bridgeScriptUploadPollsRequired",
                              "assetScriptUploadPollsRequired",
                              "bridgeRadioFirmwareUploadPollsRequired",
                              "assetRadioFirmwareUploadPollsRequired",
                              "stm32FirmwareUploadPollsRequired",
                              "configUploadPollsRequired"]
      let uploadPollsRequiredFixupNeeded = false
      for (const key of Object.keys(this.updatedAssetUpdaterVals)) {
          if (special_cases3.includes(key)) {
              uploadPollsRequiredFixupNeeded = true
          }
      }

      if (uploadBatchSizesFixupNeeded) {
          // Grab all the original values
          this.updatedAssetUpdaterVals.uploadBatchSizes = this.props.configData.assetUpdater.uploadBatchSizes
          // Now update those array values individually
          for (const [key, value] of Object.entries(this.updatedAssetUpdaterVals)) {
              if (key === "assetScriptUploadBatchSize") {
                  this.updatedAssetUpdaterVals.uploadBatchSizes[1] = parseInt(value, 10)
              } else if (key === "assetRadioFirmwareUploadBatchSize") {
                  this.updatedAssetUpdaterVals.uploadBatchSizes[3] = parseInt(value, 10)
              } else if (key === "stm32FirmwareUploadBatchSize") {
                  this.updatedAssetUpdaterVals.uploadBatchSizes[4] = parseInt(value, 10)
              }
          }
          // Now get rid of the entries we no longer need (the ones replaced by .uploadBatchSizes[])
          for (const entry of special_cases){
              if (entry in this.updatedAssetUpdaterVals) {
                  delete this.updatedAssetUpdaterVals[entry]
              }
          }
      }

      if (uploadRetriesFixupNeeded) {
          // Grab all the original values
          this.updatedAssetUpdaterVals.uploadRetries = this.props.configData.assetUpdater.uploadRetries
          // Now update those array values individually
          for (const [key, value] of Object.entries(this.updatedAssetUpdaterVals)) {
              if (key === "bridgeScriptUploadRetries") {
                  this.updatedAssetUpdaterVals.uploadRetries[0] = parseInt(value, 10)
              } else if (key === "assetScriptUploadRetries") {
                  this.updatedAssetUpdaterVals.uploadRetries[1] = parseInt(value, 10)
              } else if (key === "bridgeRadioFirmwareUploadRetries") {
                  this.updatedAssetUpdaterVals.uploadRetries[2] = parseInt(value, 10)
              } else if (key === "assetRadioFirmwareUploadRetries") {
                  this.updatedAssetUpdaterVals.uploadRetries[3] = parseInt(value, 10)
              } else if (key === "stm32FirmwareUploadRetries") {
                  this.updatedAssetUpdaterVals.uploadRetries[4] = parseInt(value, 10)
              } else if (key === "configUploadRetries") {
                  this.updatedAssetUpdaterVals.uploadRetries[6] = parseInt(value, 10)
              }
          }
          // Now get rid of the entries we no longer need (the ones replaced by .uploadRetries[])
          for (const entry of special_cases2){
              if (entry in this.updatedAssetUpdaterVals) {
                  delete this.updatedAssetUpdaterVals[entry]
              }
          }
      }

      if (uploadPollsRequiredFixupNeeded) {
          // Grab all the original values
          this.updatedAssetUpdaterVals.uploadPollsRequired = this.props.configData.assetUpdater.uploadPollsRequired
          // Now update those array values individually
          for (const [key, value] of Object.entries(this.updatedAssetUpdaterVals)) {
              if (key === "bridgeScriptUploadPollsRequired") {
                  this.updatedAssetUpdaterVals.uploadPollsRequired[0] = parseInt(value, 10)
              } else if (key === "assetScriptUploadPollsRequired") {
                  this.updatedAssetUpdaterVals.uploadPollsRequired[1] = parseInt(value, 10)
              } else if (key === "bridgeRadioFirmwareUploadPollsRequired") {
                  this.updatedAssetUpdaterVals.uploadPollsRequired[2] = parseInt(value, 10)
              } else if (key === "assetRadioFirmwareUploadPollsRequired") {
                  this.updatedAssetUpdaterVals.uploadPollsRequired[3] = parseInt(value, 10)
              } else if (key === "stm32FirmwareUploadPollsRequired") {
                  this.updatedAssetUpdaterVals.uploadPollsRequired[4] = parseInt(value, 10)
              } else if (key === "configUploadPollsRequired") {
                  this.updatedAssetUpdaterVals.uploadPollsRequired[6] = parseInt(value, 10)
              }
          }
          // Now get rid of the entries we no longer need (the ones replaced by .uploadPollsRequired[])
          for (const entry of special_cases3){
              if (entry in this.updatedAssetUpdaterVals) {
                  delete this.updatedAssetUpdaterVals[entry]
              }
          }
      }

      // NOW we can try and apply the updates
      this.updateConfig(this.state.bridgeScriptFile,
                        this.state.bridgeFirmwareFile,
                        this.state.assetScriptFile,
                        this.state.assetRadioFirmwareFile,
                        this.state.assetStm32FirmwareFile,
                        this.updatedAssetUpdaterVals).then((data) => {
        this.setState({
          snackbarOpen: true,
          updateButtonDisabled: false,
          progressDisplay: 'none',
          bridgeScriptFileName: defaultBridgeScriptFileName,
          bridgeScriptFile: null,
          bridgeFirmwareFileName: defaultBridgeFirmwareFileName,
          bridgeFirmwareFile: null,
          assetScriptFileName: defaultAssetScriptFileName,
          assetScriptFile: null,
          assetRadioFirmwareFileName: defaultAssetRadioFirmwareFileName,
          assetRadioFirmwareFile: null,
          assetStm32FirmwareFileName: defaultAssetStm32FirmwareFileName,
          assetStm32FirmwareFile: null,
        })
        this.hasChanged = false
        this.updatedAssetUpdaterVals = {}
      }).catch((err) => {
        console.log(err)
        this.setState({
          errorMessage: 'An error occurred while updating the configuration. Please verify your values and try again',
          errorDialogOpen: true,
          updateButtonDisabled: false,
          progressDisplay: 'none'
        })
      })
    }
  }

  handleAssetUpdaterCheckboxChange = (event) => {
    this.updatedAssetUpdaterVals[event.target.name] = event.target.checked
    this.hasChanged = true
  }

  handleAssetUpdaterFormChange = (event) => {
    const {name, value }= event.target;
    this.setState({[name]: value})
    this.updatedAssetUpdaterVals[event.target.name] = event.target.value
    this.hasChanged = true
  }

  handleSnackbarRequestClose = () => {
    this.setState({
      snackbarOpen: false,
    });
  }

  validateInt = (e, min, max, errorTextName) => {
    let errorText = ""
    if (!validator.isFloat(e.currentTarget.value, {min: min, max: parseFloat(max)})) {
      errorText = "Must be a number between " + min + " and " + max
    }
    let stateUpdate = {}
    stateUpdate[errorTextName] = errorText
    this.setState(stateUpdate)
  }

  closeErrorDialg = () => {
    this.setState({errorDialogOpen: false})
  }

  componentWillMount = () => {
    this.unblock = this.history.block((location, action) => {
      if (this.hasChanged) {
        return 'Leaving this page will result in ALL unsaved changes being lost. Are you sure you want to leave?'
      }
    })
  }

  componentWillUnmount = () => {
    this.unblock()
  }

  render() {
    if (this.props.configData.error) {
      return (<CardErrorIndicator apolloData={this.props.configData} />)
    } else if (this.props.configData.loading) {
      return (<CardLoadingIndicator />)
    }

    const assetUpdaterConfig = this.props.configData.assetUpdater
    let bridgeScriptDropzoneRef = null
    let bridgeFirmwareDropzoneRef = null
    let assetScriptDropzoneRef = null
    let assetRadioFirmwareDropzoneRef = null
    let assetStm32FirmwareDropzoneRef = null

    return (
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center'
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignContent: 'flex-start',
            flex: '1 1 100%'
          }}
        >
          <form onSubmit={this.handleSubmit}>
            <div style={{
              fontSize: "1.2em",
              fontFamily: muiTheme.fontFamily,
              paddingTop: '2rem',
              paddingBottom: '2rem',
              paddingLeft: '2rem'
            }}>
              Updates Configuration
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Automatic Software Updates</h5>
                <br/>
                These checkboxes allow the Network Controller to perform different types
                of software updates. Normally these should be enabled unless there is a
                reason to postpone one or more categories of uploads.
              </div>
              <Paper style={paperStyle}>
                <Checkbox
                  label="Enable upload of Bridge script"
                  name="doUploadBridgeScript"
                  style={{
                    marginLeft: '-2px',  // Checkbox SVG has 2px border
                  }}
                  onCheck={this.handleAssetUpdaterCheckboxChange}
                  defaultChecked={assetUpdaterConfig.doUploadBridgeScript}
                />
                <Checkbox
                  label="Enable upload of Bridge firmware"
                  name="doUploadBridgeFirmware"
                  style={{
                    marginLeft: '-2px',  // Checkbox SVG has 2px border
                  }}
                  onCheck={this.handleAssetUpdaterCheckboxChange}
                  defaultChecked={assetUpdaterConfig.doUploadBridgeFirmware}
                />
                <Checkbox
                  label="Enable upload of Asset scripts"
                  name="doUploadAssetScripts"
                  style={{
                    marginLeft: '-2px',  // Checkbox SVG has 2px border
                  }}
                  onCheck={this.handleAssetUpdaterCheckboxChange}
                  defaultChecked={assetUpdaterConfig.doUploadAssetScripts}
                />
                <Checkbox
                  label="Enable upload of Asset Radio firmware"
                  name="doUploadAssetRadioFirmware"
                  style={{
                    marginLeft: '-2px',  // Checkbox SVG has 2px border
                  }}
                  onCheck={this.handleAssetUpdaterCheckboxChange}
                  defaultChecked={assetUpdaterConfig.doUploadAssetRadioFirmware}
                />
                <Checkbox
                  label="Enable upload of Asset STM32 firmware"
                  name="doUploadAssetStm32Firmware"
                  style={{
                    marginLeft: '-2px',  // Checkbox SVG has 2px border
                  }}
                  onCheck={this.handleAssetUpdaterCheckboxChange}
                  defaultChecked={assetUpdaterConfig.doUploadAssetStm32Firmware}
                />
                <Checkbox
                  label="Enable upload of Asset configurations"
                  name="doUploadAssetConfigs"
                  style={{
                    marginLeft: '-2px',  // Checkbox SVG has 2px border
                  }}
                  onCheck={this.handleAssetUpdaterCheckboxChange}
                  defaultChecked={assetUpdaterConfig.doUploadAssetConfigs}
                />
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Bridge Script</h5>
                <br/>
                Set the Bridge SNAPpy Script update settings for this site
              </div>
              <Paper style={paperStyle}>
                <Dropzone
                  onDrop={(files) => {
                    this.setState({
                      bridgeScriptFileName: files[0].name,
                      bridgeScriptFile: files[0],
                    })
                    this.hasChanged = true
                  }}
                  ref={
                    (node) => {
                      bridgeScriptDropzoneRef = node
                    }
                  }
                  disableClick={true}
                  activeStyle={{}}
                  multiple={false}
                >
                  <div>
                    <TextField name="configuredBridgeScriptName"
                               style={{width: '100%'}}
                               floatingLabelText="Currently Configured Bridge Script Name"
                               onChange={this.handleAssetUpdaterFormChange}
                               value={assetUpdaterConfig.configuredBridgeScriptName + ".spy"}
                               disabled={true}
                    />
                    <TextField name="configuredBridgeScriptCrc"
                               style={{width: '100%'}}
                               floatingLabelText="Currently Configured Bridge Script CRC"
                               onChange={this.handleAssetUpdaterFormChange}
                               value={util.snappyHex(assetUpdaterConfig.configuredBridgeScriptCrc)}
                               disabled={true}
                    />
                    <TextField
                      name="bridgeScriptFile"
                      style={{
                        marginRight: '1em',
                        width: '18.5em'
                      }}
                      floatingLabelText="New Bridge Script File"
                      floatingLabelStyle={{
                        color: 'black',
                      }}
                      value={this.state.bridgeScriptFileName}
                      disabled={false}
                      inputStyle={{
                        color: 'rgba(0, 0, 0, 0.3)',
                        WebkitTextFillColor: 'rgba(0, 0, 0, 0.3)',
                        disabled: 'true'
                      }}
                      onClick={() => {
                        bridgeScriptDropzoneRef.open()
                      }}
                      onFocus={(e) => {
                        e.target.blur()
                      }}
                    />
                    <RaisedButton
                      label='Select File...'
                      primary={true}
                      onClick={
                        () => {
                          bridgeScriptDropzoneRef.open()
                        }
                      }
                    />
                  </div>
                </Dropzone>
                <TextField name="bridgeScriptUploadRetries"
                           style={{width: '100%'}}
                           floatingLabelText="Bridge Script Upload Retries"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 1, 10, 'bridgeScriptUploadRetriesErrorText')}
                           errorText={this.state.bridgeScriptUploadRetriesErrorText}
                           defaultValue={assetUpdaterConfig.uploadRetries[0]}
                           disabled={false}
                />
                <TextField name="bridgeScriptUploadPollsRequired"
                           style={{width: '100%'}}
                           floatingLabelText="Polls Required before Bridge Script Upload"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 0, 300, 'bridgeScriptUploadPollsErrorText')}
                           errorText={this.state.bridgeScriptUploadPollsErrorText}
                           defaultValue={assetUpdaterConfig.uploadPollsRequired[0]}
                           disabled={false}
                />
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Bridge SNAP Firmware</h5>
                <br/>
                Set the Bridge SNAP Firmware update settings for this site
              </div>
              <Paper style={paperStyle}>
                <Dropzone
                  onDrop={(files) => {
                    this.setState({
                      bridgeFirmwareFileName: files[0].name,
                      bridgeFirmwareFile: files[0],
                    })
                    this.hasChanged = true
                  }}
                  ref={
                    (node) => {
                      bridgeFirmwareDropzoneRef = node
                    }
                  }
                  disableClick={true}
                  activeStyle={{}}
                  multiple={false}
                >
                  <div>
                    <TextField name="configuredBridgeRadioFirmwareName"
                               style={{width: '100%'}}
                               floatingLabelText="Currently Configured Bridge Firmware Name"
                               onChange={this.handleAssetUpdaterFormChange}
                               value={assetUpdaterConfig.configuredBridgeRadioFirmwareFilename + ".sfi"}
                               disabled={true}
                    />
                    <TextField name="configuredBridgeRadioFirmwareVersion"
                               style={{width: '100%'}}
                               floatingLabelText="Currently Configured Bridge Firmware Version"
                               onChange={this.handleAssetUpdaterFormChange}
                               value={assetUpdaterConfig.configuredBridgeRadioFirmwareVersion}
                               disabled={true}
                    />
                    <TextField
                      name="bridgeFirmwareFile"
                      style={{
                        marginRight: '1em',
                        width: '18.5em'
                      }}
                      floatingLabelText="New Bridge Firmware File"
                      floatingLabelStyle={{
                        color: 'black',
                      }}
                      value={this.state.bridgeFirmwareFileName}
                      disabled={false}
                      inputStyle={{
                        color: 'rgba(0, 0, 0, 0.3)',
                        WebkitTextFillColor: 'rgba(0, 0, 0, 0.3)',
                        disabled: 'true'
                      }}
                      onClick={() => {
                        bridgeFirmwareDropzoneRef.open()
                      }}
                      onFocus={(e) => {
                        e.target.blur()
                      }}
                    />
                    <RaisedButton
                      label='Select File...'
                      primary={true}
                      onClick={
                        () => {
                          bridgeFirmwareDropzoneRef.open()
                        }
                      }
                    />
                  </div>
                </Dropzone>
                <TextField name="bridgeRadioFirmwareUploadRetries"
                           style={{width: '100%'}}
                           floatingLabelText="Bridge Firmware Upload Retries"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 1, 10, 'bridgeFirmwareUploadRetriesErrorText')}
                           errorText={this.state.bridgeFirmwareUploadRetriesErrorText}
                           defaultValue={assetUpdaterConfig.uploadRetries[2]}
                           disabled={false}
                />
                <TextField name="bridgeRadioFirmwareUploadPollsRequired"
                           style={{width: '100%'}}
                           floatingLabelText="Polls Required before Bridge Firmware Upload"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 0, 300, 'bridgeFirmwareUploadPollsErrorText')}
                           errorText={this.state.bridgeFirmwareUploadPollsErrorText}
                           defaultValue={assetUpdaterConfig.uploadPollsRequired[2]}
                           disabled={false}
                />
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Asset Script</h5>
                <br/>
                Set the Asset SNAPpy Script update settings for this site
              </div>
              <Paper style={paperStyle}>
                <Dropzone
                  onDrop={(files) => {
                    this.setState({
                      assetScriptFileName: files[0].name,
                      assetScriptFile: files[0],
                    })
                    this.hasChanged = true
                  }}
                  ref={
                    (node) => {
                      assetScriptDropzoneRef = node
                    }
                  }
                  disableClick={true}
                  activeStyle={{}}
                  multiple={false}
                >
                  <div>
                    <TextField name="configuredAssetScriptName"
                               style={{width: '100%'}}
                               floatingLabelText="Currently Configured Asset Script Name"
                               onChange={this.handleAssetUpdaterFormChange}
                               value={assetUpdaterConfig.configuredAssetScriptName + ".spy"}
                               disabled={true}
                    />
                    <TextField name="configuredAssetScriptCrc"
                               style={{width: '100%'}}
                               floatingLabelText="Currently Configured Asset Script CRC"
                               onChange={this.handleAssetUpdaterFormChange}
                               value={util.snappyHex(assetUpdaterConfig.configuredAssetScriptCrc)}
                               disabled={true}
                    />
                    <TextField
                      name="assetScriptFile"
                      style={{
                        marginRight: '1em',
                        width: '18.5em'
                      }}
                      floatingLabelText="New Asset Script File"
                      floatingLabelStyle={{
                        color: 'black',
                      }}
                      value={this.state.assetScriptFileName}
                      disabled={false}
                      inputStyle={{
                        color: 'rgba(0, 0, 0, 0.3)',
                        WebkitTextFillColor: 'rgba(0, 0, 0, 0.3)',
                        disabled: 'true'
                      }}
                      onClick={() => {
                        assetScriptDropzoneRef.open()
                      }}
                      onFocus={(e) => {
                        e.target.blur()
                      }}
                    />
                    <RaisedButton
                      label='Select File...'
                      primary={true}
                      onClick={
                        () => {
                          assetScriptDropzoneRef.open()
                        }
                      }
                    />
                  </div>
                </Dropzone>
                <TextField name="assetScriptUploadBatchSize"
                           style={{width: '100%'}}
                           floatingLabelText="Asset Script Upload Batch Size"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 1, 10, 'scriptBatchSizeErrorText')}
                           errorText={this.state.scriptBatchSizeErrorText}
                           defaultValue={assetUpdaterConfig.uploadBatchSizes[1]}
                           disabled={false}
                />
                <TextField name="assetScriptUploadRetries"
                           style={{width: '100%'}}
                           floatingLabelText="Asset Script Upload Retries"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 1, 10, 'assetScriptUploadRetriesErrorText')}
                           errorText={this.state.assetScriptUploadRetriesErrorText}
                           defaultValue={assetUpdaterConfig.uploadRetries[1]}
                           disabled={false}
                />
                <TextField name="assetScriptUploadPollsRequired"
                           style={{width: '100%'}}
                           floatingLabelText="Polls Required before Asset Script Upload"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 0, 300, 'assetScriptUploadPollsErrorText')}
                           errorText={this.state.assetScriptUploadPollsErrorText}
                           defaultValue={assetUpdaterConfig.uploadPollsRequired[1]}
                           disabled={false}
                />
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Asset SNAP Firmware</h5>
                <br/>
                Set the Asset SNAP Firmware update settings for this site
              </div>
              <Paper style={paperStyle}>
                <Dropzone
                  onDrop={(files) => {
                    this.setState({
                      assetRadioFirmwareFileName: files[0].name,
                      assetRadioFirmwareFile: files[0],
                    })
                    this.hasChanged = true
                  }}
                  ref={
                    (node) => {
                      assetRadioFirmwareDropzoneRef = node
                    }
                  }
                  disableClick={true}
                  activeStyle={{}}
                  multiple={false}
                >
                  <div>
                    <TextField name="configuredAssetRadioFirmwareName"
                               style={{width: '100%'}}
                               floatingLabelText="Currently Configured Asset SNAP Firmware Name"
                               onChange={this.handleAssetUpdaterFormChange}
                               value={assetUpdaterConfig.configuredAssetRadioFirmwareFilename + ".sfi"}
                               disabled={true}
                    />
                    <TextField name="configuredAssetRadioFirmwareVersion"
                               style={{width: '100%'}}
                               floatingLabelText="Currently Configured Asset SNAP Firmware Version"
                               onChange={this.handleAssetUpdaterFormChange}
                               value={assetUpdaterConfig.configuredAssetRadioFirmwareVersion}
                               disabled={true}
                    />
                    <TextField
                      name="assetRadioFirmwareFile"
                      style={{
                        marginRight: '1em',
                        width: '18.5em'
                      }}
                      floatingLabelText="New Asset SNAP File"
                      floatingLabelStyle={{
                        color: 'black',
                      }}
                      value={this.state.assetRadioFirmwareFileName}
                      disabled={false}
                      inputStyle={{
                        color: 'rgba(0, 0, 0, 0.3)',
                        WebkitTextFillColor: 'rgba(0, 0, 0, 0.3)',
                        disabled: 'true'
                      }}
                      onClick={() => {
                        assetRadioFirmwareDropzoneRef.open()
                      }}
                      onFocus={(e) => {
                        e.target.blur()
                      }}
                    />
                    <RaisedButton
                      label='Select File...'
                      primary={true}
                      onClick={
                        () => {
                          assetRadioFirmwareDropzoneRef.open()
                        }
                      }
                    />
                  </div>
                </Dropzone>
                <TextField name="assetRadioFirmwareUploadBatchSize"
                           style={{width: '100%'}}
                           floatingLabelText="Asset SNAP Upload Batch Size"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 1, 10, 'radioFirmwareBatchSizeErrorText')}
                           errorText={this.state.radioFirmwareBatchSizeErrorText}
                           defaultValue={assetUpdaterConfig.uploadBatchSizes[3]}
                           disabled={false}
                />
                <TextField name="assetRadioFirmwareUploadRetries"
                           style={{width: '100%'}}
                           floatingLabelText="Asset SNAP Firmware Upload Retries"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 1, 10, 'assetFirmwareUploadRetriesErrorText')}
                           errorText={this.state.assetFirmwareUploadRetriesErrorText}
                           defaultValue={assetUpdaterConfig.uploadRetries[3]}
                           disabled={false}
                />
                <TextField name="assetRadioFirmwareUploadPollsRequired"
                           style={{width: '100%'}}
                           floatingLabelText="Polls Required before Asset SNAP Firmware Upload"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 0, 300, 'assetFirmwareUploadPollsErrorText')}
                           errorText={this.state.assetFirmwareUploadPollsErrorText}
                           defaultValue={assetUpdaterConfig.uploadPollsRequired[3]}
                           disabled={false}
                />
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Asset STM32 Firmware</h5>
                <br/>
                Set the Asset STM32 Firmware update settings for this site
              </div>
              <Paper style={paperStyle}>
                <Dropzone
                  onDrop={(files) => {
                    this.setState({
                      assetStm32FirmwareFileName: files[0].name,
                      assetStm32FirmwareFile: files[0],
                    })
                    this.hasChanged = true
                  }}
                  ref={
                    (node) => {
                      assetStm32FirmwareDropzoneRef = node
                    }
                  }
                  disableClick={true}
                  activeStyle={{}}
                  multiple={false}
                >
                  <div>
                    <TextField name="configuredAssetStm32FirmwareName"
                               style={{width: '100%'}}
                               floatingLabelText="Currently Configured Asset STM32 Firmware Name"
                               onChange={this.handleAssetUpdaterFormChange}
                               value={assetUpdaterConfig.configuredAssetStm32FirmwareFilename + ".hex"}
                               disabled={true}
                    />
                    <TextField name="configuredAssetStm32FirmwareVersion"
                               style={{width: '100%'}}
                               floatingLabelText="Currently Configured Asset STM32 Firmware Version"
                               onChange={this.handleAssetUpdaterFormChange}
                               value={assetUpdaterConfig.configuredAssetStm32FirmwareVersion}
                               disabled={true}
                    />
                    <TextField
                      name="assetStm32FirmwareFile"
                      style={{
                        marginRight: '1em',
                        width: '18.5em'
                      }}
                      floatingLabelText="New Asset STM32 File"
                      floatingLabelStyle={{
                        color: 'black',
                      }}
                      value={this.state.assetStm32FirmwareFileName}
                      disabled={false}
                      inputStyle={{
                        color: 'rgba(0, 0, 0, 0.3)',
                        WebkitTextFillColor: 'rgba(0, 0, 0, 0.3)',
                        disabled: 'true'
                      }}
                      onClick={() => {
                        assetStm32FirmwareDropzoneRef.open()
                      }}
                      onFocus={(e) => {
                        e.target.blur()
                      }}
                    />
                    <RaisedButton
                      label='Select File...'
                      primary={true}
                      onClick={
                        () => {
                          assetStm32FirmwareDropzoneRef.open()
                        }
                      }
                    />
                  </div>
                </Dropzone>
                <TextField name="stm32FirmwareUploadBatchSize"
                           style={{width: '100%'}}
                           floatingLabelText="Asset STM32 Upload Batch Size"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 1, 10, 'stm32FirmwareBatchSizeErrorText')}
                           errorText={this.state.stm32FirmwareBatchSizeErrorText}
                           defaultValue={assetUpdaterConfig.uploadBatchSizes[4]}
                           disabled={false}
                />
                <TextField name="stm32FirmwareUploadRetries"
                           style={{width: '100%'}}
                           floatingLabelText="Asset STM32 Firmware Upload Retries"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 1, 10, 'stm32FirmwareUploadRetriesErrorText')}
                           errorText={this.state.stm32FirmwareUploadRetriesErrorText}
                           defaultValue={assetUpdaterConfig.uploadRetries[4]}
                           disabled={false}
                />
                <TextField name="stm32FirmwareUploadPollsRequired"
                           style={{width: '100%'}}
                           floatingLabelText="Polls Required before Asset STM32 Firmware Upload"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 0, 300, 'stm32FirmwareUploadPollsErrorText')}
                           errorText={this.state.stm32FirmwareUploadPollsErrorText}
                           defaultValue={assetUpdaterConfig.uploadPollsRequired[4]}
                           disabled={false}
                />
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Asset Configuration</h5>
                <br/>
                Set the Asset Configuration (engineering.spec) update settings for this site
              </div>
              <Paper style={paperStyle}>
                <TextField name="configUploadRetries"
                           style={{width: '100%'}}
                           floatingLabelText="Asset Configuration Upload Retries"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 1, 10, 'configUploadRetriesErrorText')}
                           errorText={this.state.configUploadRetriesErrorText}
                           defaultValue={assetUpdaterConfig.uploadRetries[6]}
                           disabled={false}
                />
                <TextField name="configUploadPollsRequired"
                           style={{width: '100%'}}
                           floatingLabelText="Polls Required before Asset Configuration Upload"
                           onChange={this.handleAssetUpdaterFormChange}
                           onBlur={(e) => this.validateInt(e, 0, 300, 'configUploadPollsErrorText')}
                           errorText={this.state.configUploadPollsErrorText}
                           defaultValue={assetUpdaterConfig.uploadPollsRequired[6]}
                           disabled={false}
                />
              </Paper>
            </div>

            <div style={{
              maxWidth: '50em',
              margin: '0px auto 2rem'
            }}>
              <div style={{
                textAlign: 'right',
                display: 'flex',
                flexDirection: 'row-reverse',
                alignItems: 'center'
              }}>
                <RaisedButton label="Update" primary={true} style={{margin: 14}} type="submit"
                              disabled={this.state.updateButtonDisabled}/>
                <CircularProgress size={30} style={{
                  display: this.state.progressDisplay
                }}/>
              </div>
            </div>
          </form>
          <Snackbar
            open={this.state.snackbarOpen}
            message="System configuration updated"
            autoHideDuration={4000}
            onRequestClose={this.handleSnackbarRequestClose}
            bodyStyle={{textAlign: 'center'}}
          />
          <Dialog
            actions={[
              <RaisedButton
                label="OK"
                primary={true}
                onTouchTap={this.closeErrorDialg}
              />,
            ]}
            modal={false}
            open={this.state.errorDialogOpen}
            onRequestClose={this.closeErrorDialg}
          >
            {this.state.errorMessage}
          </Dialog>
        </div>
      </div>
    )
  }
}

export const CONFIG_UPDATES_MUTATION = gql`
  mutation changeUpdatesPageConfig(
    $bridgeScript: FileUpload,
    $bridgeFirmware: FileUpload,
    $assetScript: FileUpload,
    $assetRadioFirmware: FileUpload,
    $assetStm32Firmware: FileUpload,
    $assetUpdaterConfigData: AssetUpdaterConfigInput,
  ) {
    updateAssetUpdaterConfig(bridgeScriptFile: $bridgeScript,
                             bridgeFirmwareFile: $bridgeFirmware,
                             assetScriptFile: $assetScript,
                             assetRadioFirmwareFile: $assetRadioFirmware,
                             assetStm32FirmwareFile: $assetStm32Firmware,
                             configData: $assetUpdaterConfigData) {
      assetUpdater {
        doUploadBridgeScript
        doUploadBridgeFirmware
        doUploadAssetScripts
        doUploadAssetRadioFirmware
        doUploadAssetStm32Firmware
        doUploadAssetConfigs
        configuredBridgeScriptName
        configuredBridgeScriptCrc
        configuredBridgeRadioFirmwareFilename
        configuredBridgeRadioFirmwareVersion
        configuredAssetScriptName
        configuredAssetScriptCrc
        configuredAssetRadioFirmwareFilename
        configuredAssetRadioFirmwareVersion
        configuredAssetStm32FirmwareFilename
        configuredAssetStm32FirmwareVersion
        uploadBatchSizes
        uploadRetries
        uploadPollsRequired
      }
    }
  }
`

const CONFIG_UPDATES_QUERY = gql`
  query ConfigUpdatesQuery {
    assetUpdater {
      doUploadBridgeScript
      doUploadBridgeFirmware
      doUploadAssetScripts
      doUploadAssetRadioFirmware
      doUploadAssetStm32Firmware
      doUploadAssetConfigs
      configuredBridgeScriptName
      configuredBridgeScriptCrc
      configuredBridgeRadioFirmwareFilename
      configuredBridgeRadioFirmwareVersion
      configuredAssetScriptName
      configuredAssetScriptCrc
      configuredAssetRadioFirmwareFilename
      configuredAssetRadioFirmwareVersion
      configuredAssetStm32FirmwareFilename
      configuredAssetStm32FirmwareVersion
      uploadBatchSizes
      uploadRetries
      uploadPollsRequired
    }
  }
`

// Bundle query, mutation, and target component together as a Higher Order Component (HOC)
export const ConfigUpdatesPage = compose(
  graphql(CONFIG_UPDATES_QUERY, {name: 'configData', options: {pollInterval: 5000}}),
  graphql(CONFIG_UPDATES_MUTATION, {
    props: ({ownProps, mutate}) => ({
      submit: (bridgeScriptFile,
               bridgeFirmwareFile,
               assetScriptFile,
               assetRadioFirmwareFile,
               assetStm32FirmwareFile,
               updatedAssetUpdaterConfig) => {
        return mutate({
          variables: {
            bridgeScript: bridgeScriptFile,
            bridgeFirmware: bridgeFirmwareFile,
            assetScript: assetScriptFile,
            assetRadioFirmware: assetRadioFirmwareFile,
            assetStm32Firmware: assetStm32FirmwareFile,
            assetUpdaterConfigData: updatedAssetUpdaterConfig,
          },
        })
      },
    }),
    options: {
      refetchQueries: [
        {query: headerQuery},
        {query: CONFIG_UPDATES_QUERY}
      ],
    }
  })
)(withRouter(ConfigUpdates))

export default ConfigUpdatesPage
