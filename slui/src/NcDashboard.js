// Dashboard page for SLUI

import React, {PureComponent} from 'react'
import Paper from 'material-ui/Paper'
import {DashboardAlarms} from './DashboardAlarms'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {rootPaperStyle} from './styles'
import {DashboardTrackingWithData} from './DashboardTracking'
import {DashboardWeather} from './DashboardWeather'
import {loadingStyle} from './LoadingCard'
import PropTypes from 'prop-types'
import {gql, graphql} from 'react-apollo'
import {CardLoadingIndicator, CardErrorIndicator} from './LoadingCard'


let muiTheme = getMuiTheme()


const alarmStyle = {
  color: 'red'
}

const ribbonStyle = {
  width: '100%',
  height: '3rem',
  margin: '0.7em'
}
// Copy in all rootPaperStyle properties (I am only doing this because paperStyle (below) did...)
Object.assign(ribbonStyle, rootPaperStyle)


class ClockAlarm extends PureComponent {
  static propTypes = {
    gps: PropTypes.object.isRequired,
  }

  render() {
    // Return either an empty div or a div representing an "alarm ribbon"
    return (
      <div>
        {(this.props.gps.systemClockQuestionable) &&
        <Paper
          zDepth={1}
          rounded={false}
          style={ribbonStyle}
        >
          <div style={alarmStyle}>
            The System Clock seems questionable compared to the GPS clock
          </div>
        </Paper>
        }
      </div>
    )
  }
}


class NtpAlarm extends PureComponent {
  static propTypes = {
    ntpinfo: PropTypes.object.isRequired,
  }

  render() {
    // Return either an empty div or a div representing an "alarm ribbon"
    return (
      <div>
        {(!this.props.ntpinfo.timeSynchronized) &&
        <Paper
          zDepth={1}
          rounded={false}
          style={ribbonStyle}
        >
          <div style={alarmStyle}>
            This Network Controller is not time synchronized to a NTP Server
          </div>
        </Paper>
        }
      </div>
    )
  }
}


class AssetsReporting extends PureComponent {
  static propTypes = {
    mesh: PropTypes.object.isRequired,
  }

  render() {
    // Return either an empty div or a div representing an "alarm ribbon"
    return (
      <div>
        {this.props.mesh.assetsReporting !== this.props.mesh.totalAssets &&
        <Paper
          zDepth={1}
          rounded={false}
          style={ribbonStyle}
        >
          <div style={alarmStyle}>
            Only {this.props.mesh.assetsReporting} assets are reporting out
            of {this.props.mesh.totalAssets}
          </div>
        </Paper>
        }
      </div>
    )
  }
}


class GpsAlarm extends PureComponent {
  static propTypes = {
    gps: PropTypes.object.isRequired,
  }

  render() {
    let reason = "the GPS modem is reporting a state of " + this.props.gps.quality
    if (!this.props.gps.isResponding) {
      reason = "because the GPS is not responding"
    }

    // Return either an empty div or a div representing an "alarm ribbon"
    return (
      <div>
        {(this.props.gps.quality === "FIX_UNAVAILABLE" || !this.props.gps.isResponding) &&
        <Paper
          zDepth={1}
          rounded={false}
          style={ribbonStyle}
        >
          <div style={alarmStyle}>
            The GPS based location is currently unavailable {reason}
          </div>
        </Paper>
        }
      </div>
    )
  }
}


class ModemAlarm extends PureComponent {
  static propTypes = {
    modem: PropTypes.object.isRequired,
  }

  render() {
    let reason = "reporting a status of " + this.props.modem.linkStatus
    if (!this.props.modem.isResponding) {
      reason = "not responding"
    }
    // console.log(this.props.modem.linkStatus)

    return (
      <div>
        {(this.props.modem.linkStatus === "PPP Link is down" || !this.props.modem.isResponding) &&

        <Paper
          zDepth={1}
          rounded={false}
          style={ribbonStyle}
        >
          <div style={alarmStyle}>
            The internet connection is currently down and the cell modem is {reason}
          </div>
        </Paper>
        }
      </div>
    )
  }
}


class ModemPowerAlarm extends PureComponent {
  static propTypes = {
    systemStatus: PropTypes.object.isRequired,
  }

  render() {
    let message = ''
    if (this.props.systemStatus.cellModemPoweredOff) {
        message = 'Power so low the cell modem has been voluntarily powered down'
    } else if (this.props.systemStatus.cellModemPowerLow) {
        message = 'Power is low enough the cell modem may have to be powered down soon'
    }

    // Return either an empty div or a div representing an "alarm ribbon"
    return (
      <div>
        {(message !== '') &&
        <Paper
          zDepth={1}
          rounded={false}
          style={ribbonStyle}
        >
          <div style={alarmStyle}>
            {message}
          </div>
        </Paper>
        }
      </div>
    )
  }
}


class SystemPowerAlarm extends PureComponent {
  static propTypes = {
    systemStatus: PropTypes.object.isRequired,
  }

  render() {
    let message = ''
    if (this.props.systemStatus.gatewayPowerLow) {
        message = 'Power is low enough the entire controller may power-down soon'
    }

    // Return either an empty div or a div representing an "alarm ribbon"
    return (
      <div>
        {(message !== '') &&
        <Paper
          zDepth={1}
          rounded={false}
          style={ribbonStyle}
        >
          <div style={alarmStyle}>
            {message}
          </div>
        </Paper>
        }
      </div>
    )
  }
}


class EmergencyStop extends PureComponent {
  static propTypes = {
    emergencyStop: PropTypes.object.isRequired,
  }

  render() {
    return (
      <div>
        {this.props.emergencyStop.inEstop &&
        <Paper
          zDepth={1}
          rounded={false}
          style={ribbonStyle}
        >
          <div style={alarmStyle}>
            The emergency stop button is currently pressed
          </div>
        </Paper>
        }
      </div>
    )
  }
}


class WeatherSourcesReporting extends PureComponent {
  static propTypes = {
    weatherMonitor: PropTypes.object.isRequired,
  }

  render() {
    let message = "No weather sources are reporting in"
    if (this.props.weatherMonitor.stationsReporting > 0) {
      message = "Only " + this.props.weatherMonitor.stationsReporting
      message += " weather sources out of " + this.props.weatherMonitor.totalStations
      message += " are reporting in"
    }
    // Return either an empty div or a div representing an "alarm ribbon"
    return (
      <div>
        {this.props.weatherMonitor.stationsReporting < this.props.weatherMonitor.totalStations &&
        <Paper
          zDepth={1}
          rounded={false}
          style={ribbonStyle}
        >
          <div style={alarmStyle}>
            {message}
          </div>
        </Paper>
        }
      </div>
    )
  }
}


class NcDashboardTarget extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
  }

  render() {
    const statTitleStyle = {
      fontSize: '1.15em',
      paddingBottom: '1em',
    }

    const paperStyle = {
      width: '25rem',
      height: '21rem',
      margin: '0.7em'
    }
    // Copy in all rootPaperStyle properties
    Object.assign(paperStyle, rootPaperStyle)

    const widerPaperStyle = {}
    Object.assign(widerPaperStyle, paperStyle)
    widerPaperStyle.width = '40rem'
    widerPaperStyle.overflow = 'auto'

    if (this.props.data.error) {
      return (<CardErrorIndicator apolloData={this.props.data} />)
    } else if (this.props.data.loading) {
      return (<CardLoadingIndicator />)
    }

    return (
      <div
        style={{
          margin: '2em'
        }}
      >
        <div style={{
          fontSize: "1.2em",
          fontFamily: muiTheme.fontFamily,
          paddingBottom: '1em',
        }}>
          Dashboard
        </div>
        <div style={{display: 'flex', flexDirection: 'column'}}>

          {/* Alarm "ribbons" at top of dashboard, each only visible when the alarm is active */}

          {/* "System Power Issues" Ribbon */}
          <SystemPowerAlarm systemStatus={this.props.data.systemStatus}/>

          {/* "Clock Issues" Ribbon */}
          <ClockAlarm gps={this.props.data.gps}/>

          {/* "NTP Issues" Ribbon */}
          <NtpAlarm ntpinfo={this.props.data.ntpinfo}/>

          {/* "Asset Issues" Ribbon */}
          <AssetsReporting mesh={this.props.data.mesh}/>

          {/* "GPS Issues" Ribbon */}
          <GpsAlarm gps={this.props.data.gps}/>

          {/* "Cell Modem Issues" Ribbon */}
          <ModemAlarm modem={this.props.data.cellModem}/>

          {/* "Cell Modem Power Issues" Ribbon */}
          <ModemPowerAlarm systemStatus={this.props.data.systemStatus}/>

          {/* "Emergency Stop Issues" Ribbon */}
          <EmergencyStop emergencyStop={this.props.data.emergencyStop} />

          {/* "Weather Stations Reporting" Ribbon */}
          <WeatherSourcesReporting weatherMonitor={this.props.data.weatherMonitor} />

          {/* Cards */}
          <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>

            {/* Alarms */}
            <Paper
              zDepth={1}
              rounded={false}
              style={widerPaperStyle}
            >
              <div style={loadingStyle}>
                <div style={statTitleStyle}>
                  Alarms
                </div>
                <DashboardAlarms />
              </div>
            </Paper>

            {/* Tracking */}
            <Paper
              zDepth={1}
              rounded={false}
              style={paperStyle}
            >
              <div style={loadingStyle}>
                <div style={statTitleStyle}>
                  Solar Tracking
                </div>
                <DashboardTrackingWithData />
              </div>
            </Paper>

            {/* Weather */}
            <Paper
              zDepth={1}
              rounded={false}
              style={paperStyle}
            >
              <div style={loadingStyle}>
                <div style={statTitleStyle}>
                  Weather
                </div>
                <DashboardWeather />
              </div>
            </Paper>

          </div>
        </div>
      </div>
    )
  }
}

const DashboardQuery = gql`
query DashboardQuery {
  mesh {
    assetsReporting
    totalAssets
  }
  gps {
    quality
    isResponding
    systemClockQuestionable
  }
  cellModem {
    linkStatus
    isResponding
  }
  emergencyStop {
    inEstop
  }
  ntpinfo {
    timeSynchronized
  }
  weatherMonitor {
    stationsReporting
    minimumStationsRequired
    totalStations
  }
  systemStatus {
    cellModemPowerLow
    cellModemPoweredOff
    gatewayPowerLow
  }
}
`

// Bundle query and target component as Higher Order Component (HOC)
export const NcDashboard = graphql(DashboardQuery, {
  options: {
    pollInterval: 5000,
  },
})(NcDashboardTarget);

export default NcDashboard
