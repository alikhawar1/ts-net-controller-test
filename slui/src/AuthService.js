const tokenUrl = "/token"
const verifyUrl = "/verify"
const changeUrl = "/change"
const signOutUrl = "/signout"


export function login(username, password) {
  // console.log("authservice login")
  localStorage.setItem('tokenExpiry', null)

  return new Promise((resolve, reject) => {
    fetch(tokenUrl, {
      method: 'POST',
      credentials: "same-origin",
      cache: 'no-cache',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: username,
        password: password,
      })
    }).then(function (response) {
      return response.json()
    }).then(function (tokenResponse) {
      if (tokenResponse.success) {
        localStorage.setItem('tokenExpiry', tokenResponse.expires)
      }
      tokenResponse.success ? resolve() : reject()
    }).catch(function (err) {
      console.log(err)
      reject()
    })
  })
}

export function verify(password) {
  // console.log("authservice verify")

  return new Promise((resolve, reject) => {
    fetch(verifyUrl, {
      method: 'POST',
      credentials: "same-origin",
      cache: 'no-cache',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        password: password,
      })
    }).then(function (response) {
      return response.json()
    }).then(function (tokenResponse) {
      tokenResponse.success ? resolve() : reject()
    }).catch(function (err) {
      console.log(err)
      reject()
    })
  })
}

export function change(password) {
  // console.log("authservice change")

  return new Promise((resolve, reject) => {
    fetch(changeUrl, {
      method: 'POST',
      credentials: "same-origin",
      cache: 'no-cache',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        password: password,
      })
    }).then(function (response) {
      return response.json()
    }).then(function (tokenResponse) {
      tokenResponse.success ? resolve() : reject()
    }).catch(function (err) {
      console.log(err)
      reject()
    })
  })
}

export function logout() {
  localStorage.setItem('tokenExpiry', null)
  fetch(signOutUrl, {
    method: 'POST',
    credentials: "same-origin",
    cache: 'no-cache'
  })
}

export function isLoggedIn() {
  let now = new Date()
  let tokenExpiry = new Date(0)
  tokenExpiry.setUTCSeconds(localStorage.getItem('tokenExpiry'))
  // getItem returns null if the item doesn't exist which is ok to pass to setUTCSeconds

  return tokenExpiry > now
}
