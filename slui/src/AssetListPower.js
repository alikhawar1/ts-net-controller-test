// Assets Lists Weather Stations

import React from 'react'
import PropTypes from 'prop-types'
import TextField from 'material-ui/TextField'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {gql, graphql} from 'react-apollo'
import {AssetInfoDialog} from './AssetInfo'
import {refetchTimeout} from './LoadingCard'


class AssetListPowerPrime extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
    active: PropTypes.bool.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
    }
    this.errorTimer = null
  }

  handleSearchTextChange = (ev) => {
    this.setState({searchText: ev.target.value})
  }

  filterPowerAssets = (event) => {
    // Simple, client-side filtering of whole dataset
    // First apply "alarms only" filter, then search phrase
    this.filteredAssets = this.props.data.assets.filter(event => {
      const evStr = Object.values(event).join(' ').toLowerCase()
      return evStr.includes(this.state.searchText.toLowerCase())
    })
  }

  render() {
    if (this.props.active) {
      this.props.data.refetch()  // Go ahead a get data now and then schedule poll
      this.props.data.startPolling(5000)
    } else {
      this.props.data.stopPolling()
    }

    const columns = [
      {
        Header: 'Location',
        accessor: 'locationText',
        width: 200,
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "left"
            }
          }
        }
      },
      {
        Header: 'Address',
        accessor: 'id',
        width: 80,
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "left"
            }
          }
        }
      },
      {
        Header: 'Power Source',
        id: 'batteryCurrent',
        accessor: (d) => {
          if ((d.solarCurrent > d.batteryCurrent) && (d.solarCurrent >= d.externalInput2Current)) {
            return 'External 1'
          } else if ((d.externalInput2Current > d.batteryCurrent) && (d.externalInput2Current >= d.solarCurrent)) {
            return 'External 2'
          } else {
            return 'Battery'
          }
        },
        width: 120,
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "left"
            }
          }
        }
      },
      {
        Header: 'Battery %',
        accessor: 'batteryCharged',
        width: 100,
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        },
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        }
      },
      {
        Header: 'Charge Status',
        id: 'chargeStatus',
        accessor: (data) => {
          if (data.chargerVoltage) {
            return 'Charging'
          } else if (data.batteryCurrent) {
            return 'Discharging'
          }
          return 'Charged'
        },
        width: 120,
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "left"
            }
          }
        }
      },
      {
        Header: 'Heater',
        id: 'heaterStatus',
        accessor: (data) => {
          if (data.miscStatusBits & 0x0001) {
            return 'Enabled'
          } else {
            return 'Disabled'
          }
        },
        width: 120,
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "left"
            }
          }
        }
      },
      {
        Header: 'Battery Health',
        accessor: 'batteryHealth',
        width: 120,
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        },
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        }
      },
      {
        Header: 'Ext Power 1 Voltage',
        accessor: 'solarVoltage',
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        },
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        }
      },
      {
        Header: 'Ext Power 2 Voltage',
        accessor: 'externalInput2Voltage',
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        },
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        }
      },
      {
        Header: 'Ext Power 1 (Today)',
        accessor: 'solarPowerCurrentDay',
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        },
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        }
      },
      {
        Header: 'Ext Power 2 (Today)',
        accessor: 'externalInput2PowerCurrentDay',
        getHeaderProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        },
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textAlign: "right"
            }
          }
        }
      }
    ]

    if (this.props.data.error) {
      if (this.errorTimer === null) {
        this.errorTimer = setTimeout(() => {
          this.errorTimer = null
          this.props.data.refetch()
        }, refetchTimeout)
      }
    } else if (this.props.data.assets) {
      this.filterPowerAssets()
    }

    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        {/* Toolbar: Acknowledge | Alarms Only | Filter */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end'
          }}
        >
          {/* Search box */}
          <div
            style={{
              marginTop: 2,
              paddingRight: '1em',
            }}
          >
            <TextField
              hintText="Filter..."
              onChange={this.handleSearchTextChange}
            />
          </div>
        </div>
        {/* Table of Events */}
        <div
          style={{
            backgroundColor: 'white',
            padding: '1em'
          }}
        >
          <ReactTable
            className="-striped -highlight"
            loading={this.props.data.loading || this.props.data.error}
            data={this.filteredAssets}
            columns={columns}
            getTdProps={(state, rowInfo, column, instance) => {
              return {
                onClick: e => {
                  if (rowInfo && rowInfo.row.id) {
                    this.nodeInfoDlg.getWrappedInstance().handleOpen(rowInfo.row.id)
                  }
                }
              }
            }}
          />
        </div>
        <AssetInfoDialog ref={(refToDlg) => (this.nodeInfoDlg = refToDlg)}/>
      </div>
    )
  }
}

const powerQuery = gql`
query AssetPowerQuery {
  assets {
    locationText
    id
    batteryCurrent
    batteryCharged
    miscStatusBits
    batteryHealth
    chargerVoltage
    solarVoltage
    solarCurrent
    externalInput2Voltage
    externalInput2Current
    solarPowerCurrentDay
    externalInput2PowerCurrentDay
  }
}
`

// Bundle query and target component as Higher Order Component (HOC)
const AssetListPower = graphql(powerQuery)(AssetListPowerPrime);

export default AssetListPower
