// AssetListStatus

import React from 'react'
import PropTypes from 'prop-types'
import Checkbox from 'material-ui/Checkbox'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {gql, graphql, compose} from 'react-apollo'
import * as util from './util'
import {AssetInfoDialog} from './AssetInfo'
import {refetchTimeout} from './LoadingCard'
import {formatTimestampAsDateAndTime} from './util'


class AssetListStatusPrime extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
    clearFaultsMutation: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      // Controls
      checkAllChecked: false,
      clearFaultsButtonDisabled: true,
      // Filters
      searchText: '',
      // Pop-ip
      rowClickAssetId: null
    }
    this.checkedIds = []
    this.clearFaultsMutation = this.props.clearFaultsMutation
    this.errorTimer = null
  }

  handleSelectAll = (ev) => {
    const doSelect = ev.target.checked

    this.checkedIds.length = 0
    if (doSelect) {
      this.filteredAssets.forEach((item, index) => {
        this.checkedIds.push(item.id)
      })
    }

    this.setState({
      checkAllChecked: doSelect
    })

    this.updateButtonStates()
  }

  handleSearchTextChange = (ev) => {
    this.checkedIds.length = 0 // make any visibility changes clear any selections...
    this.setState({searchText: ev.target.value, checkAllChecked: false})
  }

  filterAssets = (event) => {
    // Simple, client-side filtering of whole dataset
    // Here we are only using the search phrase
    this.filteredAssets = this.props.data.assets.filter(event => {
      const evStr = Object.values(event).join(' ').toLowerCase()
      return evStr.includes(this.state.searchText.toLowerCase())
    })
  }

  updateButtonStates = () => {
    // If nothing checked, all buttons are disabled
    let new_clearFaultsButtonDisabled_value = true
    // If something checked, some buttons MAY get enabled
    if (this.checkedIds.length !== 0) {
      new_clearFaultsButtonDisabled_value = false
    }

    this.setState({
      clearFaultsButtonDisabled: new_clearFaultsButtonDisabled_value,
    })
  }

  handleChecked = (event, asset) => {
    if (this.checkedIds.includes(asset.id)) {
      this.checkedIds = this.checkedIds.filter((id) => id !== asset.id)
    } else {
      this.checkedIds.push(asset.id)
    }

    this.updateButtonStates()
  }

  handleClearFaults = (event) => {
    if (this.checkedIds.length > 0) {
      this.setState({
        clearFaultsButtonDisabled: true,
        checkAllChecked: false,
        progressDisplay: 'inline'
      })
      this.clearFaultsMutation(this.checkedIds).then((data) => {
        this.setState({
          snackbarOpen: true,
          progressDisplay: 'none'
        })
        this.props.data.refetch()
        this.checkedIds.length = 0
      }).catch((err) => {
        console.log(err)
        this.setState({
          errorDialogOpen: true,
          progressDisplay: 'none'
        })
        this.checkedIds.length = 0
      })
    }
  }

  render() {
    if (this.props.active) {
      this.props.data.refetch()  // Go ahead a get data now and then schedule poll
      this.props.data.startPolling(5000)
    } else {
      this.props.data.stopPolling()
    }

    const columns = [
    {
      Header: (
        <Checkbox
          onClick={this.handleSelectAll}
          checked={this.state.checkAllChecked}
        />
      ),
      id: 'selector',
      width: 35,
      sortable: false,
      resizable: false,
      Cell: (row) => (
        <Checkbox
          onCheck={(event) => this.handleChecked(event, row.original)}
          checked={this.checkedIds.includes(row.original.id)}  // Needed because how react reuses DOM elements?
        />
      )
    },
    {
      Header: 'Location',
      accessor: 'locationText',
      width: 200,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "left"
          }
        }
      }
    },
    {
      Header: 'Address',
      accessor: 'id',
      width: 80,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "left"
          }
        }
      }
    },
    {
      Header: 'Online',
      id: 'online',
      width: 80,
      accessor: (d) => {
        if (d.offline) {
          return 'No'
        } else if (d.trackingStatus === 'HANDHELD_ROW_CONTROL') {
          return '(HHC)'
        } else if (d.offlineMinutes === util.MANUAL_CONTROL_INDICATOR) {
          return '(IRC)'
        } else if (d.offlineMinutes > 0) {
          return '(FCS)'
        } else {
          return 'Yes'
        }
      },
    },
    {
      Header: 'Last Reported',
      id: 'lastReported',
      width: 200,
      //resizable: false,
      accessor: (d) => {
        return d.lastReported ? formatTimestampAsDateAndTime(d.lastReported) : 'Never'
      },
    },
    {
      Header: 'Type',
      accessor: 'device',
      width: 100,
    },
    {
      Header: 'Uptime',
      id: 'uptime',
      width: 100,
      //resizable: false,
      accessor: (d) => {
        return util.uptimeStr(d.uptime)
      }
    },
    {
      Header: 'Signal',
      id: 'radioLinkQuality',
      accessor: (d) => d.radioLinkQuality ? util.linkQualitydBmToPercent(d.radioLinkQuality) + '%' : '--',
      //resizable: false,
      width: 80,
    },
    {
      Header: 'Repeater',
      id: 'isARepeater',
      width: 80,
      accessor: (data) => {
        return data.isARepeater ? 'Yes' : 'No'
      }
    },
    {
      Header: 'Status',
      id: 'status',
      accessor: (d) => d.statuses.join(' | '),
      //width: 200,
      // Since this is the last column, and sometimes it needs to show multiple
      // status values at the same time, give it the remaining width
      minWidth: 200,
    },
    ]

    if (this.props.data.error) {
      if (this.errorTimer === null) {
        this.errorTimer = setTimeout(() => {
          this.errorTimer = null
          this.props.data.refetch()
        }, refetchTimeout)
      }
    } else if (this.props.data.assets) {
      this.filterAssets()
    }

    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        {/* Toolbar: Clear Faults | Separator | Filter */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start'
          }}
        >
          {/* Clear Faults button */}
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              paddingLeft: '1em',
              paddingRight: '1em',
              paddingTop: '1em',
            }}
          >
            <RaisedButton
              label="Clear Faults"
              primary={true}
              onClick={() => this.handleClearFaults()}
              disabled={this.state.clearFaultsButtonDisabled}
            />
          </div>

          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              flexGrow: 1,
            }}
          >
          </div>

          {/* Filter box */}
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              marginTop: 2,
              paddingRight: '1em',
            }}
          >
            <TextField
              hintText="Filter..."
              onChange={this.handleSearchTextChange}
            />
          </div>
        </div>
        {/* Table of Assets */}
        <div
          style={{
            backgroundColor: 'white',
            padding: '1em'
          }}
        >
          <ReactTable
            className="-striped -highlight"
            loading={this.props.data.loading || this.props.data.error}
            data={this.filteredAssets}
            columns={columns}
            getTdProps={(state, rowInfo, column, instance) => {
              return {
                onClick: e => {
                  // We have added a "selector" checkbox in the first column
                  // We want to exclude that column from the "click on the row" behavior
                  if (column.id !== 'selector') {
                    if (rowInfo && rowInfo.row.id) {
                        this.nodeInfoDlg.getWrappedInstance().handleOpen(rowInfo.row.id)
                    }
                  }
                }
              }
            }}
            getTheadThProps={(state, rowInfo, column, instance) => {
              return {
                style: {
                  textAlign: "left"
                }
              }
            }}
          />
        </div>
        <AssetInfoDialog ref={(refToDlg) => (this.nodeInfoDlg = refToDlg)}/>
      </div>
    )
  }
}

const statusQuery = gql`
  query AssetStatusQuery {
    assets {
      locationText
      id
      lastReported
      model
      device
      uptime
      radioLinkQuality
      statuses
      isARepeater
      offline
      offlineMinutes
      trackingStatus
    }
  }
`
const clearFaultsMutation = gql`
mutation clearFaults($assetIDs: [ID])  {
  clearFaults(ids: $assetIDs) {
    requested
  }
}
`

// Bundle query and target component as Higher Order Component (HOC)
const AssetListStatus = compose(
  graphql(statusQuery, {
    options: {pollInterval: 5000},
  }),
  graphql(clearFaultsMutation, {
    props: ({ownProps, mutate}) => ({
      clearFaultsMutation: (ids) => mutate({
        variables: {assetIDs: ids},
      }),
    }),
  }),
)(AssetListStatusPrime);

export default AssetListStatus
