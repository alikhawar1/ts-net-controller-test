//
// ConfirmationDialog looks like a RaisedButton BUT
// it pops up a (customizable) confirm/cancel dialog
// when you click on it. Note that you have to specify
// a onConfirm method (not an onClick method). Has
// defaults for most properties but you should usually
// override them to make the UI clearer for the user.
//
import React from 'react';
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';

export class ConfirmationDialog extends React.Component {
  static propTypes = {
    mainLabel: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
    cancelLabel: PropTypes.string,
    confirmLabel: PropTypes.string,
    onConfirm: PropTypes.func.isRequired,
    disabled: PropTypes.bool
  }

  static defaultProps = {
    mainLabel: "UPDATE",
    title: "",
    text: "",
    cancelLabel: "CANCEL",
    confirmLabel: "CONFIRM",
    disabled: false
  }

  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true})
  };

  handleClose = () => {
    this.setState({open: false})
  };

  handleConfirm = () => {
    this.props.onConfirm()
    this.handleClose()
  };

  render() {
    const actions = [
      <RaisedButton
        label={this.props.cancelLabel}
        primary={true}
        onClick={this.handleClose}
      />,
      <RaisedButton
        style={{
          paddingLeft: '1em', // because there is another button to the left of this one...
        }}
        label={this.props.confirmLabel}
        primary={true}
        onClick={this.handleConfirm}
      />
    ];

    return (
      <div>
        <RaisedButton
          label={this.props.mainLabel}
          primary={true}
          style={{margin: 14}}
          onClick={this.handleOpen}
          disabled={this.props.disabled}
        />
        <Dialog
          title={this.props.title}
          actions={actions}
          modal={true}
          open={this.state.open}
        >
          {this.props.text}
        </Dialog>
      </div>
    );
  }
}

export default ConfirmationDialog
