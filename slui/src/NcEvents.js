// NcEvents - User interface for event log management


import React from 'react'
import PropTypes from 'prop-types'
import FlatButton from 'material-ui/FlatButton'
import Checkbox from 'material-ui/Checkbox'
import DoneAllcon from 'material-ui/svg-icons/action/done-all'
import TextField from 'material-ui/TextField'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {gql, graphql, compose} from 'react-apollo'
import Paper from 'material-ui/Paper'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {refetchTimeout} from './LoadingCard'
import {formatTimestampAsDateAndTime} from './util'


let muiTheme = getMuiTheme()

export const criticalLevelNo = 50


class CheckRow extends React.Component {
  static propTypes = {
    onCheck: PropTypes.func.isRequired,
    checked: PropTypes.bool.isRequired,
    row: PropTypes.any.isRequired,
    recordId: PropTypes.string.isRequired,
  }

  handleCheck = (ev, isChecked) => {
    this.props.onCheck(this.props.row, isChecked)
  }

  render() {
    return (
      <Checkbox
        style={{margin: -3, transform: 'scale(0.75)'}}
        onCheck={this.handleCheck}
        checked={this.props.checked}
      />
    )
  }
}

class NcEvents extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
    clearRecordsMutation: PropTypes.func.isRequired,  // injected by GraphQL
  }

  constructor(props) {
    super(props)
    this.pageSize = 20
    this.state = {
      alarmsOnly: false,
      selectedItems: new Array(this.pageSize).fill(false),
      searchText: '',
      checkAllChecked: false,
    }
    this.checkedIds = []
    this.clearRecordsMutation = this.props.clearRecordsMutation
    this.errorTimer = null
  }

  handleAckAlarms = () => {
    if (this.checkedIds.length > 0) {
      this.clearRecordsMutation(this.checkedIds).then((data) => {
        this.props.data.refetch()
        this.checkedIds.length = 0
        this.setState({
          selectedItems: new Array(this.pageSize).fill(false),
          checkAllChecked: false
        })
      }).catch((err) => {
        console.log(err)
      })
    }
    // console.log("Ack alarms")
  }

  handlePageSizeChange = (pageSize, pageIndex) => {
    this.pageSize = pageSize
    this.setState({selectedItems: new Array(this.pageSize).fill(false)})
    this.checkedIds.length = 0
  }

  handleSelectRow = (row, isChecked, record) => {
    const sel = this.state.selectedItems
    sel[row] = isChecked
    this.setState({selectedItems: sel})

    if (this.checkedIds.includes(record.id)) {
      this.checkedIds = this.checkedIds.filter((id) => id !== record.id)
    } else {
      this.checkedIds.push(record.id)
    }
  }

  isRowSelected = (row) => {
    return this.state.selectedItems[row.viewIndex]
  }

  handleSelectAll = (ev) => {
    const doSelect = ev.target.checked
    this.setState({
      selectedItems: new Array(this.pageSize).fill(doSelect),
      checkAllChecked: doSelect
    })

    this.checkedIds.length = 0
    if (doSelect) {
      this.checkedIds.push.apply(this.checkedIds, this.props.data.eventLog.eventLogEntries.map((record) => record.id))
    }
  }

  handleSearchTextChange = (ev) => {
    this.setState({searchText: ev.target.value})
  }

  filterEvents(event) {
    // Simple, client-side filtering of whole dataset
    // First apply "alarms only" filter, then search phrase
    this.filteredEvents = this.props.data.eventLog.eventLogEntries.filter(event => {
      if (this.state.alarmsOnly && event.levelno < criticalLevelNo) {
        return false
      }
      const evStr = Object.values(event).join(' ').toLowerCase()
      return evStr.includes(this.state.searchText.toLowerCase())
    })
  }

  render() {
    // Also look at overridden styles in index.css
    const columns = [{
      Header: (
        <Checkbox
          style={{margin: -3, marginLeft: -12, transform: 'scale(0.75)'}}
          onClick={this.handleSelectAll}
          checked={this.state.checkAllChecked}
        />
      ),
      width: 35,
      sortable: false,
      resizable: false,
      Cell: (cellInfo) => (
        <CheckRow
          onCheck={(row, isChecked) => this.handleSelectRow(row, isChecked, cellInfo.original)}
          checked={this.isRowSelected(cellInfo)}
          row={cellInfo.viewIndex}
          recordId={cellInfo.original.id}
        />
      ),
    }, {
      Header: 'Time',
      id: 'created',
      accessor: (d) => {
        return formatTimestampAsDateAndTime(d.created)
      },
      filterable: false,
      width: 200,
    }, {
      Header: 'Event',
      id: 'message',
      accessor: (d) => {
        return (
          <div>
            {d.message}
            {d.cleared && d.levelno >= criticalLevelNo ? <br /> : null}
            {d.cleared && d.levelno >= criticalLevelNo ? "Cleared: " + formatTimestampAsDateAndTime(d.cleared) : null}
          </div>
        )
      },
      minWidth: 200,
    }, {
      Header: 'Level',
      accessor: 'levelname',
      width: 100
    }]

    if (this.props.data.error) {
      if (this.errorTimer === null) {
        this.errorTimer = setTimeout(() => {
          this.errorTimer = null
          this.props.data.refetch()
        }, refetchTimeout)
      }
    } else if (!this.props.data.loading) {
      this.filterEvents()
    }

    return (
      <div
        style={{
          margin: '2em'
        }}
      >
        <div style={{
          fontSize: "1.2em",
          fontFamily: muiTheme.fontFamily,
          paddingBottom: '2rem',
        }}>
          Events
        </div>
        <Paper
          style={{
            width: '100%',
          }}
        >
          <div style={{display: 'flex', flexDirection: 'column'}}>
            {/* Toolbar: Acknowledge | Alarms Only | Filter */}
            <div style={{display: 'flex', flexDirection: 'row'}}>

              {/* Alarm-cancel button */}
              <div
                style={{
                  marginRight: 'auto',
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                }}
              >
                {this.state.selectedItems.includes(true) ?
                  <div style={{textAlign: 'center'}}>
                    <FlatButton
                      label="Acknowledge Selected Alarms"
                      //style={{whiteSpace: 'nowrap'}}
                      icon={<DoneAllcon />}
                      primary={true}
                      onClick={this.handleAckAlarms}
                    />

                  </div>
                  :
                  <div style={{}}>
                  </div>
                }
              </div>

              {/* Alarms-only filter */}
              <div style={{marginTop: 15, marginRight: 40}}>
                <Checkbox
                  style={{whiteSpace: 'nowrap'}}
                  label="Alarms only"
                  onClick={(ev) => this.setState({alarmsOnly: ev.target.checked})}
                />
              </div>
              {/* Search box */}
              <div style={{marginTop: 2}}>
                <TextField
                  hintText="Filter..."
                  onChange={this.handleSearchTextChange}
                />
              </div>

            </div>

            {/* Table of Events */}
            <div style={{flex: 1, backgroundColor: 'white'}}>
              <div style={{margin: 'auto'}}>
                <ReactTable
                  className="-striped -highlight"
                  loading={this.props.data.loading || this.props.data.error}
                  data={this.filteredEvents}
                  columns={columns}
                  defaultPageSize={this.pageSize}
                  onPageSizeChange={this.handlePageSizeChange}
                  //filterable={true}
                  getPaginationProps={(state) => {
                    return {
                      style: {
                        color: 'green'
                      }
                    }
                  }}
                  getTrProps={(state, rowInfo, column, instance) => {
                    // if (rowInfo) console.log(rowInfo)
                    return {
                      style: {
                        color: rowInfo && !rowInfo.original.cleared && rowInfo.original.levelno >= criticalLevelNo ? 'red' : 'black'
                      }
                    }
                  }}
                  getTdProps={(state, rowInfo, column, instance) => {
                    // if (rowInfo) console.log(rowInfo)
                    return {
                      style: {
                        whiteSpace: 'normal'
                      }
                    }
                  }}
                />
              </div>
            </div>

          </div>
        </Paper>
      </div>
    )
  }
}

const eventsQuery = gql`
  query EventsQuery {
    eventLog {
      eventLogEntries {
        id
        message
        levelno
        levelname
        created
        cleared
      }
    }
  }
`

const clearRecordsMutation = gql`
mutation clearRecords($recordIDs: [ID]) {
  clearLogEntries(ids: $recordIDs) {
    eventLogEntries {
      id
    }
  }
}

`

// Bundle query and target component as Higher Order Component (HOC)
export const NcEventsWithData = compose(
  graphql(eventsQuery, {
    options: {pollInterval: 5000},
  }),
  graphql(clearRecordsMutation, {
    props: ({ownProps, mutate}) => ({
      clearRecordsMutation: (ids) => mutate({
        variables: {recordIDs: ids},
      }),
    }),
  })
)(NcEvents);

export default NcEventsWithData
