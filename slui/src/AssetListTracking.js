// AssetListTracking

import React from 'react'
import PropTypes from 'prop-types'
import TextField from 'material-ui/TextField'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import {gql, graphql} from 'react-apollo'
import {AssetInfoDialog} from './AssetInfo'
import {refetchTimeout} from './LoadingCard'


class AssetListTrackingPrime extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
  }

  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
    }
    this.errorTimer = null
  }

  handleSearchTextChange = (ev) => {
    this.setState({searchText: ev.target.value})
  }

  filterTrackingAssets = (event) => {
    // Simple, client-side filtering of whole dataset
    // First apply "alarms only" filter, then search phrase
    this.filteredTrackingAssets = this.props.data.assets.filter(event => {
      if (event.device !== 'Tracker') {
        return false
      }

      const evStr = Object.values(event).join(' ').toLowerCase()
      return evStr.includes(this.state.searchText.toLowerCase())
    })
  }

  render() {
    if (this.props.active) {
      this.props.data.refetch()  // Go ahead a get data now and then schedule poll
      this.props.data.startPolling(5000)
    } else {
      this.props.data.stopPolling()
    }

    const columns = [{
      Header: 'Location',
      accessor: 'locationText',
      width: 200,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "left"
          }
        }
      }
    }, {
      Header: 'Address',
      accessor: 'id',
      width: 80,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "left"
          }
        }
      }
    }, {
      Header: 'Mode',
      accessor: 'trackingStatus',
      width: 250,  // AUTO_TRACK_BACKTRACKING is why this is so wide...
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "left"
          }
        }
      }
    }, {
      Header: 'Cur Angle',
      accessor: 'currentAngle',
      width: 90,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      },
      getProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      }
    }, {
      Header: 'Tgt Angle',
      accessor: 'requestedAngle',
      width: 80,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      },
      getProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      }
    }, {
      Header: 'Avg Err (Today)',
      accessor: 'averageAngularErrorCurrentDay',
      width: 130,  // increased for header width, not value width
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      },
      getProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      }
    }, {
      Header: 'Motor Amps',
      accessor: 'motorCurrent',
      width: 120,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      },
      getProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      }
    }, {
      Header: 'Peak Inrush',
      accessor: 'peakMotorInrushCurrent',
      width: 120,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      },
      getProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      }
    }, {
      Header: 'Peak Amps',
      accessor: 'peakMotorCurrent',
      width: 120,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      },
      getProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      }
    }, {
      Header: 'Average Amps',
      accessor: 'averageMotorCurrent',
      width: 120,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      },
      getProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      }
    }, {
      Header: 'Ending Amps',
      accessor: 'endingMotorCurrent',
      width: 120,
      getHeaderProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      },
      getProps: (state, rowInfo, column) => {
        return {
          style: {
            textAlign: "right"
          }
        }
      }
    }]

    if (this.props.data.error) {
      if (this.errorTimer === null) {
        this.errorTimer = setTimeout(() => {
          this.errorTimer = null
          this.props.data.refetch()
        }, refetchTimeout)
      }
    } else if (this.props.data.assets) {
      this.filterTrackingAssets()
    }

    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        {/* Toolbar: Acknowledge | Alarms Only | Filter */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end'
          }}
        >
          {/* Search box */}
          <div
            style={{
              marginTop: 2,
              paddingRight: '1em',
            }}
          >
            <TextField
              hintText="Filter..."
              onChange={this.handleSearchTextChange}
            />
          </div>
        </div>
        {/* Table of Events */}
        <div
          style={{
            backgroundColor: 'white',
            padding: '1em'
          }}
        >
          <ReactTable
            className="-striped -highlight"
            loading={this.props.data.loading || this.props.data.error}
            data={this.filteredTrackingAssets}
            columns={columns}
            getTdProps={(state, rowInfo, column, instance) => {
              return {
                onClick: e => {
                  if (rowInfo && rowInfo.row.id) {
                    this.nodeInfoDlg.getWrappedInstance().handleOpen(rowInfo.row.id)
                  }
                }
              }
            }}
          />
        </div>
        <AssetInfoDialog ref={(refToDlg) => (this.nodeInfoDlg = refToDlg)}/>
      </div>
    )
  }
}

const trackingQuery = gql`
  query AssetTrackingQuery {
    assets {
      id
      device
      locationText
      trackingStatus
      currentAngle
      requestedAngle
      averageAngularErrorCurrentDay
      motorCurrent
      peakMotorInrushCurrent
      peakMotorCurrent
      averageMotorCurrent
      endingMotorCurrent
    }
  }
`
// Bundle query and target component as Higher Order Component (HOC)
const AssetListTracking = graphql(trackingQuery, {
  options: {pollInterval: 5000},
})(AssetListTrackingPrime);

export default AssetListTracking
