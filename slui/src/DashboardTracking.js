import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {gql, graphql, compose} from 'react-apollo'
import StatusTable from './StatusTable'
import {CardLoadingIndicator, CardErrorIndicator} from './LoadingCard'


class DashboardTracking extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
    trackingEnums: PropTypes.object.isRequired  // injected by GraphQL
  }

  render() {
    if (this.props.data.error) {
      return (<CardErrorIndicator apolloData={this.props.data} />)
    } else if (this.props.trackingEnums.error) {
      return (<CardErrorIndicator apolloData={this.props.trackingEnums} />)
    } else if (this.props.data.loading || this.props.trackingEnums.loading) {
      return (<CardLoadingIndicator />)
    }

    // In my GraphQL query I'm using an introspection query to get all of the ENUM values so I can find the description here
    let commandedStateDesc = this.props.trackingEnums.commandStates.fields.find(
      (field) => field.name === "commandedState"
    ).type.enumValues.find(
      (e) => e.name === this.props.data.tracking.commandedState
    ).description
    let opModeDesc = this.props.data.config.operationalMode
    if (opModeDesc) {
      opModeDesc = this.props.trackingEnums.operationalModes.fields.find(
        (field) => field.name === "operationalMode"
      ).type.enumValues.find(
        (e) => e.name === this.props.data.config.operationalMode
      ).description
    }

    if (this.props.data.tracking.commandedState === "NORMAL") {
      // When in the "normal" state we follow whatever the user has set as his desired state
      commandedStateDesc = this.props.data.config.trackingEnable ? "Tracking" : opModeDesc
    }

    return (
      <div>
        <StatusTable
          statusList={[
            {
              label: 'Calculated Sun Angle',
              value: Math.round(this.props.data.tracking.calcPanelAngle).toString() + String.fromCharCode(0x00B0)
            },
            {
              label: 'Commanded Panel State',
              value: commandedStateDesc
            },
            {
              label: 'Desired Operational Mode',
              value: this.props.data.config.trackingEnable ? "Tracking" : opModeDesc
            },
            {
              label: 'Backtracking Enabled',
              value: this.props.data.config.backtrackingEnable ? "Enabled" : "Disabled"
            }
          ]}
        />
      </div>
    )
  }
}


const DashboardTrackingQuery = gql`
query DashboardTrackingQuery {
  tracking {
    calcPanelAngle
    commandedState
  }
  config {
    operationalMode
    trackingEnable
    backtrackingEnable
  }
}
`

const trackingEnumsQuery = gql`
query TrackingEnums {
  commandStates: __type(name: "TrackingStatus") {
    fields {
      name
      type {
        enumValues {
          name
          description
        }
      }
    }
	}
	operationalModes: __type(name: "SiteConfig") {
    fields {
      name
      type {
        enumValues {
          name
          description
        }
      }
    }
	}
}
`

// Bundle query and target component as Higher Order Component (HOC)
export const DashboardTrackingWithData = compose(
  graphql(trackingEnumsQuery, {
    name: 'trackingEnums',
  }),
  graphql(DashboardTrackingQuery, {
    options: {
      pollInterval: 5000,
    },
  })
)(DashboardTracking);

export default DashboardTrackingWithData
