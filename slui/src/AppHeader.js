// AppHeader - contains info/controls across top of screen


import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {gql, graphql, compose} from 'react-apollo'
import {TsLogo} from './TsLogo'
import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import IconButton from 'material-ui/IconButton'
import Divider from 'material-ui/Divider'
import Person from 'material-ui/svg-icons/social/person'
import {logout} from './AuthService'
import {withRouter} from 'react-router-dom'
import Badge from 'material-ui/Badge';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications'
import {NcHeaderSiteName} from './NcHeaderInfo'
import {criticalLevelNo} from './NcEvents'
import {refetchTimeout} from './LoadingCard'


class AppHeader extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,   // 'data' is injected by GraphQL
    history: PropTypes.object.isRequired,  // 'history' is injected by react-router
    shutDownWifiMutation: PropTypes.func.isRequired
}

  constructor(props) {
    super(props)
    this.state = {
      alarmsCount: 0
    }
    this.errorTimer = null
    this.shutDownWifiMutation = this.props.shutDownWifiMutation
  }

  userSettingsClick = () => {
    if (this.props.history.location.pathname !== "/config-user") {
      this.props.history.push("/config-user")
    }
  }

  signoutClick = () => {
    logout()
    this.props.history.push("/login")
  }

  signoutWiFiClick = () => {
    this.shutDownWifiMutation(0).then((data) => {
      this.signoutClick()
    }).catch((err) => {
      console.log(err)
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data.error && nextProps.data.error.networkError && nextProps.data.error.networkError.message.includes("403")) {
      // Token expired or is considered invalid
      this.signoutClick()
    }
  }

  render() {
    let alarmsCount = 0
    let displayAlarmCount = 'none'

    if (this.props.data.error) {
      if (this.errorTimer === null) {
        this.errorTimer = setTimeout(() => {
          this.errorTimer = null
          this.props.data.refetch()
        }, refetchTimeout)
      }
      alarmsCount = '?'
      displayAlarmCount = 'flex'
    } else if (!this.props.data.loading && !this.props.data.error) {
      alarmsCount = this.props.data.eventLog.entriesCount
      if (alarmsCount > 0) {
        displayAlarmCount = 'flex'
      }
    }

    return (
      <div
        style={{
          display: 'flex',
          height: 56,
          backgroundColor: 'rgba(128, 189, 1, 1)'
        }}
      >
        <div
          style={{
            paddingLeft: 16,
            paddingTop: 13,
            paddingBottom: 13,
            width: 240,
            minWidth: 240
          }}
        >
          <TsLogo width={173} height={30}/>
        </div>
        <div>
          <NcHeaderSiteName />
        </div>
        <div style={{marginRight: 'auto'}}/>
        <div>
          <Badge
            badgeContent={alarmsCount}
            primary={true}
            badgeStyle={{
              top: 4,
              right: 0,
              width: 15,
              height: 15,
              fontSize: 12,
              color: 'white',
              backgroundColor: '#bd3e01',
              display: displayAlarmCount
            }}
            style={{display: 'flex', padding: 0}}
            onClick={() => {
              if (this.props.history.location.pathname !== "/events") {
                this.props.history.push("/events")
              }
            }}
          >
            <IconButton
              tooltip="Notifications"
              iconStyle={{
                width: 30,
                height: 30
              }}
            >
              <NotificationsIcon color='black'/>
            </IconButton>
          </Badge>
        </div>
        <div
          style={{
            marginTop: -1,
            marginRight: 4
          }}>
          <IconMenu
            iconButtonElement={
              <IconButton>
                <Person />
              </IconButton>
            }
            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
            targetOrigin={{horizontal: 'left', vertical: 'top'}}
            iconStyle={{height: 32, width: 32, color: 'black'}}
          >
            <MenuItem
              onClick={this.userSettingsClick}
              value="settings"
              primaryText="User Settings"/>
            <Divider />
            <MenuItem
              onClick={this.signoutWiFiClick}
              value="signoutWiFi"
              primaryText="Sign out+WiFi OFF"/>
            <Divider />
            <MenuItem
              onClick={this.signoutClick}
              value="signout"
              primaryText="Sign out"/>
          </IconMenu>
        </div>
      </div>
    )
  }
}

export const headerQuery = gql`
  query AlarmQuery($level: Int) {
    eventLog {
      entriesCount(levelno: $level)
    }
  }
`

const shutDownWifiMutation = gql`
mutation shutDownWifi($dummyInt: Int)  {
  shutDownWifi(dummy: $dummyInt) {
    shutdown
  }
}
`


// Bundle query and target component as Higher Order Component (HOC)
export const AppHeaderWithData = compose(
  graphql(headerQuery, {
    options: {
      pollInterval: 5000,
      variables: {
        level: criticalLevelNo
      }
    }
  }),
  graphql(shutDownWifiMutation, {
    props: ({ownProps, mutate}) => ({
      shutDownWifiMutation: (dummyInt) => mutate({
        variables: {dummy: dummyInt},
      }),
    }),
  })
)(withRouter(AppHeader))

export default AppHeaderWithData
