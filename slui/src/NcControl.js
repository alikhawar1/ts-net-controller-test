// Control page for SLUI

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {gql, graphql, compose} from 'react-apollo'
import RaisedButton from 'material-ui/RaisedButton'
import Checkbox from 'material-ui/Checkbox'
import Snackbar from 'material-ui/Snackbar'
import CircularProgress from 'material-ui/CircularProgress'
import Dialog from 'material-ui/Dialog'
import Paper from 'material-ui/Paper'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {paperStyle, sectionStyle, titleStyle, descriptionStyle} from './styles'
import {CardLoadingIndicator, CardErrorIndicator} from './LoadingCard'


let muiTheme = getMuiTheme()

const ESTOP_TEST_LEN = 60


class EmergencyCountDown extends Component {
  static propTypes = {
    countDown: PropTypes.number.isRequired
  }

  render() {
    if (this.props.countDown > 0) {
      return (
        <div>
          Test time remaining {this.props.countDown.toString()} seconds
          <br />&nbsp;
        </div>
      )
    }

    return (
      <span />
    )
  }
}


class ConfigControl extends Component {
  static propTypes = {
    controlConfig: PropTypes.object.isRequired,
    startTest: PropTypes.func.isRequired,
    updateConfig: PropTypes.func.isRequired,
    siteConfigEnums: PropTypes.object.isRequired,
    cancelSnowStowMutation: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)
    this.updateConfig = props.updateConfig
    this.startTest = props.startTest
    this.hasChanged = false
    this.updatedSiteConfigVals = {}
    this.cancelSnowStowMutation = this.props.cancelSnowStowMutation
    this.state = {
      snackbarOpen: false,
      progressDisplay: 'none',
      updateButtonDisabled: false,
      errorDialogOpen: false,
      stowAvgMphErrorText: '',
      stowAvgDurationText: '',
      gustMphErrorText: '',
      resumeMinErrorText: '',
      desiredMode: 1,
      eStopButtonDisabled: false,
      snowStowCancelButtonDisabled: true,
      snackbarMessage: ''
    }
  }

  handleSubmit = (event) => {
    event.preventDefault()  // prevent the default form submit behaviour
    if (this.hasChanged) {
      this.setState({
        updateButtonDisabled: true,
        progressDisplay: 'inline'
      })
      this.updateConfig(this.updatedSiteConfigVals).then((data) => {
        // console.log("Updated site config")
        this.setState({
          snackbarOpen: true,
          snackbarMessage: "Control configuration updated",
          updateButtonDisabled: false,
          progressDisplay: 'none'
        })
        this.hasChanged = false
      }).catch((err) => {
        console.log(err)
        this.setState({
          errorDialogOpen: true,
          updateButtonDisabled: false,
          progressDisplay: 'none'
        })
      })
    }
  }

  handleCheckboxChange = (event) => {
    this.updatedSiteConfigVals[event.target.name] = event.target.checked
    this.hasChanged = true
  }

  handleFormChange = (event) => {
    this.updatedSiteConfigVals[event.target.name] = event.target.value
    this.hasChanged = true
  }

  handleSnackbarRequestClose = () => {
    this.setState({
      snackbarOpen: false,
    });
  }

  handleModeChange = (event, index, value) => {
    // SelectField makes you manually set the state for the component
    this.setState({desiredMode: value})
    if (value === "TRACK") {
      this.updatedSiteConfigVals['trackingEnable'] = true
    } else {
      this.updatedSiteConfigVals['trackingEnable'] = false
      this.updatedSiteConfigVals['operationalMode'] = value
    }
    this.hasChanged = true
  }

  closeErrorDialg = () => {
    this.setState({errorDialogOpen: false})
  }

  handleEmergencyStop = (e) => {
    this.setState({eStopButtonDisabled: true})
    this.startTest(ESTOP_TEST_LEN).then((data) => {
      // console.log('Emergency Stop Test Button)
      this.setState({
        snackbarOpen: true,
        snackbarMessage: "Emergency Stop Button Test Started",
        eStopButtonDisabled: false,
        progressDisplay: 'none'
      })
      this.hasChanged = false
      this.props.controlConfig.startPolling(1000)
    }).catch((err) => {
      console.log(err)
      this.setState({
        errorDialogOpen: true,
        eStopButtonDisabled: false,
        progressDisplay: 'none'
      })
    })
  }

  handleSnowStowCancellation = (e) => {
    // console.log('Cancel button pressed')
    this.cancelSnowStowMutation(0).then((data) => {
      // console.log(data)
      // console.log(data.data)
      // console.log(data.data.cancelSnowStow)
      // console.log(data.data.cancelSnowStow.cancelled)
    }).catch((err) => {
      console.log(err)
    })
  }

  handleVisuallyVerified = (event) => {
    this.setState({snowStowCancelButtonDisabled: !event.target.checked})
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.controlConfig.loading) {
      if (nextProps.controlConfig.config.trackingEnable) {
         this.setState({desiredMode: "TRACK"})
      } else {
        this.setState({desiredMode: nextProps.controlConfig.config.operationalMode})
      }
    }
  }

  getModes = () => {
    return this.props.siteConfigEnums.__type.fields.find(
      (field) => field.name === "operationalMode"
    ).type.enumValues.map((mode) => {
      return (
       <MenuItem value={mode.name} primaryText={mode.description} key={mode.name} />
      )
    })
  }

  render() {
    if (this.props.controlConfig.error) {
      return (<CardErrorIndicator apolloData={this.props.controlConfig} />)
    } else if (this.props.siteConfigEnums.error) {
      return (<CardErrorIndicator apolloData={this.props.siteConfigEnums} />)
    } else if (this.props.controlConfig.loading || this.props.siteConfigEnums.loading) {
      return (<CardLoadingIndicator />)
    }

    const controlConfig = this.props.controlConfig.config
    const estopStatus = this.props.controlConfig.emergencyStop

    if (estopStatus.estopTestRemaining > 0) {
      this.props.controlConfig.startPolling(1000)
    } else {
      this.props.controlConfig.startPolling(5000)
    }

    return (
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: 'center'
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignContent: 'flex-start',
            flex: '1 1 100%'
          }}
        >
          <form onSubmit={this.handleSubmit}>
            <div style={{
              fontSize: "1.2em",
              fontFamily: muiTheme.fontFamily,
              paddingTop: '2rem',
              paddingBottom: '2rem',
              paddingLeft: '2rem'
            }}>
              Control
            </div>


            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Operational Mode</h5>
                <br />
                Controls the operational mode for all panels in the field
              </div>
              <Paper style={paperStyle}>
                <SelectField
                  fullWidth={true}
                  floatingLabelText="Desired Mode"
                  floatingLabelStyle={{marginTop: -16}}
                  underlineStyle={{
                    marginTop: -16,
                    position: "relative"
                  }}
                  menuStyle={{marginTop: -4}}
                  style={{marginBottom: -16}}
                  value={this.state.desiredMode}
                  onChange={this.handleModeChange}
                  selectedMenuItemStyle={{color: 'rgb(132, 189, 0)'}}
                >
                  <MenuItem value="TRACK" primaryText="Tracking"/>
                  {this.getModes()}
                </SelectField>
                <Checkbox
                  label="Enable backtracking"
                  name="backtrackingEnable"
                  onCheck={this.handleCheckboxChange}
                  defaultChecked={controlConfig.backtrackingEnable}
                />
              </Paper>
            </div>

            <div style={{
              maxWidth: '50em',
              margin: '0px auto 2rem'
            }}>
              <div style={{
                textAlign: 'right',
                display: 'flex',
                flexDirection: 'row-reverse',
                alignItems: 'center'
              }}>
                <RaisedButton label="Update" primary={true} style={{margin: 14}} type="submit"
                              disabled={this.state.updateButtonDisabled}/>
                <CircularProgress size={30} style={{
                  display: this.state.progressDisplay
                }}/>
              </div>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Manually Clear Snow Stow</h5>
                <br />
                Once panels have been automatically stowed due to snow depth,
                 manual intervention is required to resume tracking
              </div>
              <Paper style={paperStyle}>
                <RaisedButton
                  label="Cancel Snow Depth Auto-Stow"
                  primary={true}
                  style={{
                    marginTop: "4px",
                    marginBottom: "4px"
                  }}
                  onClick={this.handleSnowStowCancellation}
                  disabled={this.state.snowStowCancelButtonDisabled}
                />
                <Checkbox
                  label="I have visually verified all panel movement-arcs are sufficiently clear of snow"
                  name="visuallyVerified"
                  onCheck={this.handleVisuallyVerified}
                  defaultChecked={false}
                />
              </Paper>
            </div>

            <div style={sectionStyle}>
              <div style={descriptionStyle}>
                <h5 style={titleStyle}>Emergency Stop Test</h5>
                <br />
                Emergency Stop Button Test Procedure:
                <br/><br/>
                1. Press the "Test Emergency Stop" button on this page.
                <br/><br/>
                2. A timer will now be displayed indicating how much time is left for the test. During this time pressing the emergency stop button on the network controller will not affect panel movement.
                <br/><br/>
                2. Press down on the red emergency stop button on the network controller box to engage the emergency stop
                <br/><br/>
                3. Verify that the page shows that the last emergency stop test has passed
                <br/><br/>
                4. Pull up on the red emergency stop button on the network controller box to disengage the emergency stop
              </div>
              <Paper style={paperStyle}>
                <div>
                  The emergency stop button is currently <b>{estopStatus.estopIsPressed ? "active" : "inactive"}</b>
                </div>
                <br />
                <div>
                  The last emergency stop test <b>{estopStatus.estopTestPassed ? "passed" : "failed"}</b>
                </div>
                <br />
                <EmergencyCountDown countDown={estopStatus.estopTestRemaining}/>
                <RaisedButton
                  label="Test Emergency Stop"
                  primary={true}
                  style={{
                    marginTop: "4px",
                    marginBottom: "4px"
                  }}
                  onClick={this.handleEmergencyStop}
                  disabled={this.state.eStopButtonDisabled}
                />
              </Paper>
            </div>

          </form>
          <Snackbar
            open={this.state.snackbarOpen}
            message="System configuration updated"
            autoHideDuration={4000}
            onRequestClose={this.handleSnackbarRequestClose}
            bodyStyle={{textAlign: 'center'}}
          />
          <Dialog
            actions={[
              <RaisedButton
                label="OK"
                primary={true}
                onTouchTap={this.closeErrorDialg}
              />,
            ]}
            modal={false}
            open={this.state.errorDialogOpen}
            onRequestClose={this.closeErrorDialg}
          >
            An error occurred while updating the settings. Please verify your values and try again
          </Dialog>
        </div>
      </div>
    )
  }
}

const UPDATE_CONTROL_CONFIG = gql`
  mutation updateSiteConfig($configData: SiteConfigInput) {
    updateConfig(configData: $configData) {
      config {
        siteName
      }
    }
  }
`

const START_ESTOP_TEST = gql`
  mutation estopTest($testLen: Int) {
    emergencyStop(estopTestStart: $testLen)
    {
      emergencyStop {
        inEstop
        estopIsPressed
        estopTestPassed
        estopTestRemaining
      }
    }
  }
`

const configControlQuery = gql`
  query ConfigControlQuery {
    config {
      operationalMode
      backtrackingEnable
      trackingEnable
    }
    emergencyStop {
      inEstop
      estopIsPressed
      estopTestPassed
      estopTestRemaining
    }
  }
`

const configEnumsQuery = gql`
query ConfigEnums {
  __type(name: "SiteConfig") {
    fields {
      name
      type {
        enumValues {
          name
          description
        }
      }
    }
	}
}
`

const cancelSnowStowMutation = gql`
mutation cancelSnowStow($dummyInt: Int)  {
  cancelSnowStow(dummy: $dummyInt) {
    cancelled
  }
}
`

// Bundle query, mutation, and target component together as a Higher Order Component (HOC)
export const ConfigControlPage = compose(
  graphql(configControlQuery, {
    name: 'controlConfig',
  }),
  graphql(configEnumsQuery, {
    name: 'siteConfigEnums',
  }),
  graphql(UPDATE_CONTROL_CONFIG, {
    props: ({ownProps, mutate}) => ({
      updateConfig: (siteConfigData) => mutate({
        variables: {configData: siteConfigData}
      }),
    }),
  }),
  graphql(START_ESTOP_TEST, {
    props: ({ownProps, mutate}) => ({
      startTest: (estopTestLen) => mutate({
        variables: {$testLen: estopTestLen}
      }),
    }),
    options: {pollInterval: 5000},
  }),
  graphql(cancelSnowStowMutation, {
    props: ({ownProps, mutate}) => ({
      cancelSnowStowMutation: (dummyInt) => mutate({
        variables: {dummy: dummyInt},
      }),
    }),
  })
)(ConfigControl);

export default ConfigControlPage
