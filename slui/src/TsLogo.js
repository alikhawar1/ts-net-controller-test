import React, {Component} from 'react'


export class TsLogo extends Component {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="-0.4 0 1055.3817 181.67726" height="181.67726" width="1055.3817"
           xmlSpace="preserve" id="svg5206" version="1.1" {...this.props}>
        <defs id="defs5210">
          <clipPath id="clipPath5238" clipPathUnits="userSpaceOnUse">
            <path id="path5236" d="M 0,792 H 612 V 0 H 0 Z"/>
          </clipPath>
        </defs>
        <g transform="matrix(1.3333333,0,0,-1.3333333,-0.0551365,618.86181)" id="g5214">
          <g transform="matrix(1.4641,0,0,1.4641,-104.53547,-173.75681)" id="g6091">
            <path id="path5218"
                  style={{fill: '#ffffff', stroke: '#ffffff', strokeWidth: 0.75, strokeLinecap: 'round', strokeLinejoin: 'round', strokeMiterlimit: 10, strokeDasharray: 'none', strokeOpacity: 1}}
                  d="m 234.2891,368.7217 h -3.343 v 26.193 h -9.816 v 3.05 h 22.975 v -3.05 h -9.816 v -26.193"/>
            <path id="path5226"
                  style={{fill: '#ffffff', fillOpacity: 1, stroke: '#ffffff', strokeWidth: 0.75, strokeLinecap: 'round', strokeLinejoin: 'round', strokeMiterlimit: 10, strokeDasharray: 'none', strokeOpacity: 1}}
                  d="m 279.7847,394.957 h -17.837 v -9.984 h 15.958 v -3.008 h -15.958 v -10.236 h 18.046 v -3.007 h -21.346 v 29.243 h 21.137 v -3.008"/>
            <path id="path5250" style={{fill: '#ffffff', fillOpacity: 1, fillRule: 'nonzero', stroke: '#ffffff', strokeOpacity:1}}
                  d="m 307.3203,394.915 h -9.024 v -11.864 h 8.982 c 4.387,0 7.52,2.256 7.52,6.057 0,3.634 -2.758,5.807 -7.478,5.807 m 7.812,-26.193 -8.481,11.363 h -8.355 v -11.363 h -3.3 v 29.243 h 12.575 c 6.433,0 10.569,-3.468 10.569,-8.732 0,-4.887 -3.342,-7.728 -7.938,-8.563 l 8.982,-11.948 z"/>
            <path id="path5262" style={{fill: '#ffffff', fillOpacity: 1, fillRule: 'nonzero', stroke: '#ffffff', strokeOpacity:1}}
                  d="m 345.8828,394.915 h -9.024 v -11.864 h 8.981 c 4.387,0 7.521,2.256 7.521,6.057 0,3.634 -2.758,5.807 -7.478,5.807 m 7.812,-26.193 -8.481,11.363 h -8.355 v -11.363 h -3.3 v 29.243 h 12.574 c 6.434,0 10.569,-3.468 10.569,-8.732 0,-4.887 -3.341,-7.728 -7.937,-8.563 l 8.982,-11.948 z"/>
            <path id="path5274" style={{fill: '#ffffff', fillOpacity: 1, fillRule: 'nonzero', stroke: '#ffffff', strokeOpacity:1}}
                  d="m 384.4443,394.3301 -6.685,-14.914 h 13.328 z m 14.914,-25.608 h -3.552 l -3.425,7.727 h -15.916 l -3.468,-7.727 h -3.383 l 13.326,29.452 h 3.091 z"/>
            <path id="path5278"
                  style={{fill: '#ffffff', fillOpacity: 1, stroke: '#ffffff', strokeWidth: 0.75, strokeLinecap: 'round', strokeLinejoin: 'round', strokeMiterlimit: 10, strokeDasharray: 'none', strokeOpacity: 1}}
                  d="m 423.2588,368.3037 c -4.429,0 -8.899,1.546 -12.408,4.68 l 3.802,4.552 c 2.631,-2.171 5.388,-3.55 8.731,-3.55 2.631,0 4.219,1.044 4.219,2.756 v 0.084 c 0,1.63 -1.003,2.465 -5.891,3.719 -5.89,1.504 -9.691,3.132 -9.691,8.94 v 0.084 c 0,5.304 4.261,8.814 10.235,8.814 4.262,0 7.896,-1.337 10.86,-3.719 l -3.34,-4.845 c -2.591,1.797 -5.138,2.882 -7.604,2.882 -2.465,0 -3.76,-1.127 -3.76,-2.548 v -0.083 c 0,-1.922 1.254,-2.549 6.308,-3.844 5.933,-1.545 9.274,-3.676 9.274,-8.774 v -0.083 c 0,-5.807 -4.427,-9.065 -10.735,-9.065 z"/>
            <path id="path5286"
                  style={{fill: '#ffffff', fillOpacity: 1, stroke: '#ffffff', strokeWidth: 0.75, strokeLinecap: 'round', strokeLinejoin: 'round', strokeMiterlimit: 10, strokeDasharray: 'none', strokeOpacity: 1}}
                  d="m 470.8037,368.7217 v 19.091 l -8.23,-12.49 h -0.166 l -8.148,12.365 v -18.966 h -6.309 v 29.243 h 6.936 l 7.687,-12.366 7.686,12.366 h 6.936 v -29.243 h -6.392"/>
            <path id="path5302" style={{fill: '#ffffff', fillOpacity: 1, fillRule: 'nonzero', stroke: '#ffffff', strokeOpacity:1}}
                  d="m 505.4814,390.4453 -3.885,-9.483 h 7.771 z m 8.855,-21.724 -2.673,6.559 h -12.365 l -2.673,-6.559 h -6.559 l 12.532,29.453 h 5.932 l 12.533,-29.453 z"/>
            <path id="path5314" style={{fill: '#ffffff', fillOpacity: 1, fillRule: 'nonzero', stroke: '#ffffff', strokeOpacity:1}}
                  d="m 551.8135,387.9805 c 0,2.757 -1.922,4.178 -5.054,4.178 h -6.392 v -8.398 h 6.517 c 3.133,0 4.929,1.672 4.929,4.22 m -0.125,-19.259 -6.267,9.358 h -5.054 v -9.358 h -6.434 v 29.243 h 13.368 c 6.893,0 11.029,-3.634 11.029,-9.65 0,-4.805 -2.548,-7.77 -6.266,-9.148 l 7.144,-10.445 z"/>
            <path id="path5318"
                  style={{fill: '#ffffff', fillOpacity: 1, stroke: '#ffffff', strokeWidth: 0.75, strokeLinecap: 'round', strokeLinejoin: 'round', strokeMiterlimit: 10, strokeDasharray: 'none', strokeOpacity: 1}}
                  d="m 585.7393,392.0322 v -23.311 h -6.433 v 23.311 h -8.898 v 5.933 h 24.23 v -5.933 h -8.899"/>
            <path id="path5326"
                  style={{fill: 'none', fillOpacity: 1, stroke: '#ffffff', strokeWidth: 0.75, strokeLinecap: 'round', strokeLinejoin: 'round', strokeMiterlimit: 10, strokeDasharray: 'none', strokeOpacity: 1}}
                  d="m 606.0811,374.5488 c 0,0.059 0.047,0.111 0.11,0.111 h 1.58 c 0.698,0 1.267,-0.552 1.267,-1.243 0,-0.535 -0.354,-0.976 -0.859,-1.18 l 0.796,-1.475 c 0.04,-0.075 0,-0.168 -0.099,-0.168 h -0.453 c -0.047,0 -0.082,0.028 -0.094,0.052 l -0.772,1.539 h -0.877 v -1.481 c 0,-0.058 -0.052,-0.11 -0.111,-0.11 h -0.378 c -0.063,0 -0.11,0.052 -0.11,0.11 z"/>
            <path id="path5330"
                  style={{fill: 'none', stroke: '#ffffff', strokeWidth: 0.75, strokeLinecap: 'round', strokeLinejoin: 'round', strokeMiterlimit: 10, strokeDasharray: 'none', strokeOpacity: 1}}
                  d="m 607.7256,372.6846 c 0.377,0 0.708,0.319 0.708,0.721 0,0.377 -0.331,0.69 -0.708,0.69 h -1.034 v -1.411 z"/>
            <path id="path5338"
                  style={{fill: 'none', stroke: '#ffffff', strokeWidth: 0.75, strokeLinecap: 'round', strokeLinejoin: 'round', strokeMiterlimit: 10, strokeDasharray: 'none', strokeOpacity: 1}}
                  d="m 607.5586,368.3037 c -2.383,0 -4.321,1.939 -4.321,4.323 0,2.383 1.938,4.323 4.321,4.323 2.383,0 4.324,-1.94 4.324,-4.323 0,-2.384 -1.941,-4.323 -4.324,-4.323 z"/>
            <path id="path5342"
                  style={{fill: 'none', fillOpacity: 1, stroke: '#ffffff', strokeWidth: 0.75, strokeLinecap: 'round', strokeLinejoin: 'round', strokeMiterlimit: 10, strokeDasharray: 'none', strokeOpacity: 1}}
                  d="m 607.5586,376.3711 c -2.063,0 -3.743,-1.68 -3.743,-3.744 0,-2.066 1.68,-3.746 3.743,-3.746 2.064,0 3.746,1.68 3.746,3.746 0,2.064 -1.682,3.744 -3.746,3.744 z"/>
          </g>
          <path
            d="m 156.5903,422.6694 c -1.706,4.278 -5.355,4.693 -8.574,0.875 l -14.992,-19.1 -25.767,52.224 c -4.352,8.819 -10.939,10.018 -17.463,0.23 l -89.678,-128.635 68.222,18.603 -41.551,-0.615 68.372,79.084 35.095,-58.301 21.005,22.232 42.545,-60.889 v 0.008 z"
            style={{fill: '#ffffff', fillOpacity: 1, stroke: '#ffffff', strokeWidth: 0.75, strokeLinecap: 'round', strokeLinejoin: 'round', strokeMiterlimit: 10, strokeDasharray: 'none', strokeOpacity: 1}}
            id="path5350"/>
          <path
            d="m 62.0767,349.6807 29.218,0.432 -29.844,-8.138 c 7.235,-4.883 16.262,-7.132 25.551,-5.58 11.134,1.858 20.15,8.746 25.179,17.934 -7.741,7.056 -18.507,10.64 -29.64,8.781 -4.762,-0.795 -9.133,-2.517 -12.975,-4.939 0.124,0.283 0.261,0.557 0.369,0.852 1.651,4.524 0.99,9.32 -1.364,13.119 -4.248,-1.39 -7.842,-4.634 -9.493,-9.158 -1.483,-4.065 -1.096,-8.346 0.704,-11.93 1.569,3.308 5.47,11.938 5.47,11.938 z"
            style={{fill: '#ffffff', fillOpacity: 1, stroke: '#ffffff', strokeWidth: 0.75, strokeLinecap: 'round', strokeLinejoin: 'round', strokeMiterlimit: 10, strokeDasharray: 'none', strokeOpacity: 1}}
            id="path5358"/>
        </g>
      </svg>
    )
  }
}
