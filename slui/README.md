Project setup
----------------------------------------------------------------
1. Clone the repo
2. cd to the base directory containing package.json
3. npm install

This gets you to the point where you can run the "production" or "development" steps below.

Building for production
----------------------------------------------------------------
npm run build           # React packaging
npm run deploy          # Copies build to ..\netcontrol\static

For Development
----------------------------------------------------------------
npm start   # starts Webpack development server

* You'll need to run the 'netcontrol' python application separately
* The SLUI backend connects to netcontrol on localhost:8888/graphql
 
