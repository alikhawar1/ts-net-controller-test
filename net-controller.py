"""
net-controller.py - the purpose of this file is to mimic the run-time
environment of the code when run on an actual NC (E12) Gateway.
"""

import sys

from netcontrol.main import main

sys.path.insert(0, "netcontrol")

if __name__ == '__main__':
    sys.exit(main())

